using Base.Iterators
using CSV
using Pipe

chemin_attaques = "attaques.txt"
chemin_rimes = "rimes.txt"

# Récupération brute des attaques.
texte_attaques = read(chemin_attaques, String)
modèle_a2 = r"\\deux{(?<forme>\w+?)}"
modèle_a3 = r"\\trois{(?<forme>\w+?)}"
attaques2 = @pipe eachmatch(modèle_a2, texte_attaques) |> collect .|> _[:forme] |> unique
attaques3 = @pipe eachmatch(modèle_a3, texte_attaques) |> collect .|> _[:forme] |> unique
attaques = vcat(attaques3..., attaques2...)

println(attaques)

# Détection d’ambiguïtés pour les substitutions.
lignes = readlines(chemin_attaques)
modèle_phonétique = r"\\(deux|trois){(?<forme>\w+?)}"
modèle_orthographique = r"\\(japhug|forme){(?<forme>.+?)}"
ambiguïtés = Dict()
ambiguïtés_groupes = Dict()
for (position, ligne) ∈ lignes |> enumerate
    bilan_phonétique = match(modèle_phonétique, ligne)
    bilan_orthographique = match(modèle_orthographique, ligne)
    if !isnothing(bilan_phonétique)
        forme_phonétique = bilan_phonétique["forme"]
        exemple_orthographique = bilan_orthographique["forme"]
        if !occursin(forme_phonétique, exemple_orthographique)
            println("Ligne $position : $forme_phonétique non présent dans $exemple_orthographique")
            # Cas où le caractère ambigu est en premier.
            caractère_ambigu = first(forme_phonétique)
            sousforme_phonétique = last(forme_phonétique, length(forme_phonétique) - 1)
            modèle_sousphonétique = "(?<caractère>\\w)$sousforme_phonétique" |> Regex
            bilans_sousphonétiques = eachmatch(modèle_sousphonétique, exemple_orthographique) |> collect
            for bilan_sousphonétique ∈ bilans_sousphonétiques
                println(caractère_ambigu, bilan_sousphonétique)
                push!(get!(ambiguïtés_groupes, forme_phonétique, Set()), bilan_sousphonétique.match)
                push!(get!(ambiguïtés, caractère_ambigu, Set()), first(bilans_sousphonétiques)["caractère"] |> first)
            end
            # Cas où le caractère ambigu est en dernier.
            caractère_ambigu = last(forme_phonétique)
            sousforme_phonétique = first(forme_phonétique, length(forme_phonétique) - 1)
            modèle_sousphonétique = "$sousforme_phonétique(?<caractère>\\w)" |> Regex
            bilans_sousphonétiques = eachmatch(modèle_sousphonétique, exemple_orthographique) |> collect
            for bilan_sousphonétique ∈ bilans_sousphonétiques
                println(caractère_ambigu, bilan_sousphonétique)
                push!(get!(ambiguïtés_groupes, forme_phonétique, Set()), bilan_sousphonétique.match)
                push!(get!(ambiguïtés, caractère_ambigu, Set()), first(bilans_sousphonétiques)["caractère"] |> first)
            end
        end
    end
end

println(ambiguïtés)
println(ambiguïtés_groupes)

# Substitutions des attaques ambiguës.
substitutions = Dict(source => cible |> first for (source, cible) ∈ ambiguïtés_groupes)
attaques_à_jour = [get(substitutions, groupe, groupe) for groupe ∈ attaques]

println(attaques_à_jour)

# open("attaques_avant.txt", "w") do fichier
#     données = @pipe attaques |> join(_, "\n")
#     write(fichier, données)
# end
# open("attaques_après.txt", "w") do fichier
#     données = @pipe attaques_à_jour |> join(_, "\n")
#     write(fichier, données)
# end

# Récupération brute des rimes.
texte_rimes = read(chemin_rimes, String)
modèle_r = r"\\ipa{(?<forme>\w+?)}"
rimes = @pipe eachmatch(modèle_r, texte_rimes) |> collect .|> _[:forme] |> unique |> sort(_, by=length, rev=true)

# Génération des combinaisons théoriques.
attaques_complètes = vcat(attaques_à_jour, [""])
combinaisons = product(attaques_complètes, rimes) .|> join

# open("combinaisons.txt", "w") do fichier
#     données = @pipe combinaisons |> permutedims |> vec |> join(_, '\n') * "\n"
#     write(fichier, données)
# end

# Génération en colonne pour LaTeX.
# open("syllabes colonnes.txt", "w") do fichier
#     données = @pipe combinaisons |> permutedims |> vec |> join(_, "1\n") * "1\n"
#     write(fichier, données)
# end

# Génération en tableau pour LaTeX.
tableau_texte = ""

for ligne ∈ eachrow(combinaisons)
    global tableau_texte *= join(ligne, "1 ") * "1\n"
end

diacritiques_combinants = 0x300:0x338 .|> Char
for caractère ∈ diacritiques_combinants
    global tableau_texte *= ' ' * caractère * "1"
end

tableau_texte *= "\n"

open("../syllabes.txt", "w") do fichier
    write(fichier, tableau_texte)
end
