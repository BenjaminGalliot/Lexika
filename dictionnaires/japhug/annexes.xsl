<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:template name="ajouter_introduction">
\chapter*{Introduction}

Ce dictionnaire décrit le lexique de la langue japhug (\pjya{kɯrɯ skɤt}), parlée dans la région de Japhug (\pcmn{茶堡}, \pjya{tɕɤpʰɯ}) au district de Mbarkhams (\pcmn{马尔康县}), préfecture de Rngaba (\pcmn{阿坝州}) au Sichuan en Chine, dans les cantons de Gdongbrgyad (\pcmn{龙尔甲乡}, \pjya{ʁdɯrɟɤt}), Gsarrdzong (\pcmn{沙尔宗乡}, \pjya{sarndzu}) de Datshang (\pcmn{大藏乡}, \pjya{tatshi}). Il est basé sur les matériaux recueillis à Mbarkhams par l’auteur auprès de Tshendzin (\emph{Chenzhen} \pcmn{陈珍}) et Dpalcan (\emph{Baierqing} \pcmn{柏尔青}) depuis juillet 2002.

Seul le dialecte de Kamnyu (\pcmn{干木鸟村}, \pjya{kɤmɲɯ}) est représenté dans ce dictionnaire. Cette variété a fait l'objet d'une description grammaticale \parencite{jacques21grammar} et un corpus d'enregistrements transcrits est inclus dans la collection Pangloss (\url{https://pangloss.cnrs.fr/corpus/Japhug}). Ce dictionnaire recense tous les mots qui apparaissent dans les enregistrements de cette collection, à l'exception des emprunts au chinois.
Il contient <xsl:value-of select="count(//EntréesLexicales/EntréeLexicale)"/> entrées (dont <xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^v.*')])"/> verbes et <xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^n.*')])"/> noms).

Chaque entrée du dictionnaire contient une définition en français et en chinois ainsi que la partie du discours du mot, parmi les suivantes :

\begin{itemize}
\item \emph{adv} : adverbe
\item \emph{clf} : classificateur
\item \emph{idph} : idéophone
\item \emph{intj} : interjection
\item \emph{n} : nom
\item \emph{np} : nom inaliénablement possédé
\item \emph{postp} : postposition
\item \emph{pro} : pronom
\item \emph{vi} : verbe intransitif
\item \emph{vinh} : verbe intransitif sans sujet humain
\item \emph{vi-t} : verbe semi-transitif
\item \emph{vs} : verbe statif intransitif
\item \emph{vt} : verbe transitif
\end{itemize}

Dans le cas des prédicats complexes, l'entrée principale contient les formes des deux éléments séparés par une virgule, suivi de chaque élément avec sa partie du discours propre. Par exemple \pjya{loʁ,tɯ-ʑi} \symbolediamant{} \pjya{tɯ-ʑi} np. \symbolediamant{} \pjya{loʁ} vs. ‘avoir la nausée’ signifie que l’élément \pjya{loʁ} est morphologiquement un verbe statif, et \pjya{tɯ-ʑi} un nom possédé, même si les deux ne peuvent pas s'employer de façon indépendante.

Le numéro qui suit \emph{idph} correspond au patron idéophonique selon la classification décrite dans \cite[428]{jacques21grammar}.

Les verbes contiennent après \emph{dir} le ou les préfixes directionnels utilisés pour former les tiroirs verbaux \parencite[1082]{jacques21grammar}. Le symbole \tiretbas{} est employé pour les verbes orientables \parencite[632]{jacques21grammar}. Pour les verbes irréguliers (tels que \pjya{ɕe} ‘aller’ ou \pjya{ɣɤʑu} ‘exister’), les formes non prévisibles (thème du passé, seconde personne ou générique) sont toutes indiquées.

Les dérivations verbales sont indiquées par les abréviations suivantes, identiques à celles utilisées dans les gloses des exemples dans \cite{jacques21grammar}:

\begin{itemize}
\item \textsc{acaus} : anticausatif
\item \textsc{apass} : antipassif
\item \textsc{appl} : applicatif
\item \textsc{auto} : autif (autobénéfactif-spontané)
\item \textsc{caus} : causatif
\item \textsc{comp} : composé
\item \textsc{deidph} : déidéophonique
\item \textsc{denom} : dénominal
\item \textsc{distr} : action distribuée
\item \textsc{facil} : facilitatif
\item \textsc{incorp} : incorporation
\item \textsc{pass} : passif
\item \textsc{prop} : propriétif
\item \textsc{recip} : réciproque
\item \textsc{refl} : réfléchi
\item \textsc{trop} : tropatif
\item \textsc{vert} : vertitif
\end{itemize}

Ce dictionnaire a bénéficié des corrections de nombreux collègues et étudiants, en particulier Giorgio Arcodia, Gong Xun, Peng Guozhen, Zev Handel, Alexis Michaud, Laurent Sagart, Shuai Yanchen et Zhang Shuya. Je remercie également Benjamin Galliot, Rémy Bonnet, Céline Buret, Séverine Guillaume et Thomas Pellard pour leur aide avec les scripts de conversion du format MDF vers \LaTeX.

Ce travail a été financé par le projet ANR HimalCo (ANR-12-CORP-0006) et est en relation avec le projet de recherche LR-4.11 \emph{Automatic Paradigm Generation and Language Description} du Labex EFL (fondé par l’ANR/CGI).
</xsl:template>

</xsl:stylesheet>
