<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:variable name="inclureaudio" select="true()"/>
<xsl:param name="dossieraudio"/>
<xsl:param name="formataudio"/>

<xsl:import href="latex%20alphabétique.xsl"/>

<xsl:template name="ajouter_audio">
\RequirePackage{totcount}
\newcounter{compteur}
\setcounter{compteur}{0}
\regtotcounter{compteur}
\RequirePackage{media9}
\addmediapath{<xsl:value-of select="$dossieraudio"/>}
\RenewDocumentCommand \vedette { m } {\addtocounter{compteur}{1}\pjya{\textbf{#1}}}
\RenewDocumentCommand \sousvedette { m } {\addtocounter{compteur}{1}\pjya{\textbf{#1}}}
\NewDocumentEnvironment {notesaudio} {} {\ignorespaces} {\ignorespacesafterend}
\NewDocumentCommand \noteaudio { m m } {\notemargespéciale{a:\arabic{compteur}}{\footnotesize\écouter{#1}{#2}}}
\NewDocumentCommand \écouter { m m } {\csname écouterqualité#2\endcsname{#1}}
\NewDocumentCommand \écouterqualitéélevée { m } {%
    \IfFileExists{<xsl:value-of select="$dossieraudio"/>/#1.<xsl:value-of select="$formataudio"/>}{%
        \includemedia[activate=onclick,addresource=#1.<xsl:value-of select="$formataudio"/>,flashvars={source=#1.<xsl:value-of select="$formataudio"/>&amp;autoPlay=true&amp;autoRewind=true&amp;loop=false&amp;hideBar=true&amp;volume=1.0&amp;balance=0.0}]{\symbolesonfort}{APlayer.swf}%
    }{\symbolesansson}%
}
\NewDocumentCommand \écouterqualitéfaible { m } {%
    \IfFileExists{<xsl:value-of select="$dossieraudio"/>/8_#1.<xsl:value-of select="$formataudio"/>}{%
        \includemedia[activate=onclick,addresource=8_#1.<xsl:value-of select="$formataudio"/>,flashvars={source=8_#1.<xsl:value-of select="$formataudio"/>&amp;autoPlay=true&amp;autoRewind=true&amp;loop=false&amp;hideBar=true&amp;volume=1.0&amp;balance=0.0}]{\symbolesonfaible}{APlayer.swf}%
    }{\symbolesansson}%
}
\NewDocumentCommand \symbolesonfort {} {\textcolor{Turquoise}{\textesymbolique{🔉}}}
\NewDocumentCommand \symbolesonfaible {} {\textcolor{JungleGreen}{\textesymbolique{🔉}}}
\NewDocumentCommand \symbolesansson {} {}
</xsl:template>

<xsl:template match="Multimédia">
    <xsl:if test="$inclureaudio">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\begin{notesaudio}</xsl:text>
        <xsl:for-each select="Audios/Audio">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() mod 3 = 0">
                <xsl:text>\linebreak</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\end{notesaudio}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="Audio">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\noteaudio</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="CheminDAccès"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Qualité"/>
    <xsl:text>}%</xsl:text>
</xsl:template>

</xsl:stylesheet>
