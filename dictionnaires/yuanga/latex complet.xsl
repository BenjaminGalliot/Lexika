<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:import href="en-tête.xsl"/>
<xsl:import href="latex%20alphabétique.xsl"/>
<xsl:import href="latex%20thématique.xsl"/>
<xsl:import href="latex%20inverse.xsl"/>

<xsl:template match="RessourceLexicale">
<xsl:call-template name="ajouter_entête_générale"/>
<xsl:call-template name="ajouter_entête_introduction"/>

\begin{document}

<xsl:call-template name="ajouter_première_de_couverture"/>

\pagefantôme

\pagenumbering{roman} % Éviter \frontmatter parce qu’il y aura des chapitres numérotés dans cette longue introduction.

<xsl:call-template name="ajouter_premières_pages"/>

\begin{introduction}
<xsl:call-template name="ajouter_introduction"/>
\end{introduction}

\bibliographie

<xsl:call-template name="ajouter_entête_alphabétique"/>
\begin{dictionnaire}[\textefra{Dictionnaire alphabétique}]
<xsl:apply-templates select="InformationsLexicographiques" mode="alphabétique"/>
\end{dictionnaire}

<xsl:call-template name="ajouter_entête_thématique"/>
\begin{dictionnaire}[\textefra{Dictionnaire thématique}]
<xsl:apply-templates select="InformationsLexicographiques" mode="thématique"/>
\end{dictionnaire}

<xsl:call-template name="ajouter_entête_inverse"/>
\begin{dictionnaire}[\textefra{Dictionnaire inverse}]
<xsl:apply-templates select="InformationsLexicographiques" mode="inverse"/>
\end{dictionnaire}

\backmatter

\begin{postface}
<xsl:call-template name="ajouter_postface"/>
\end{postface}

\tabledesmatières

\cleardoublepage
\pagefantôme

<xsl:call-template name="ajouter_quatrième_de_couverture"/>

\end{document}

</xsl:template>

</xsl:stylesheet>
