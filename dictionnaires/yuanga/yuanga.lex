\_sh v3.0  1030 kanak_MDFalt

\lx a
\hm 1
\dialx GOs
\va ò
\dialx BO PA
\is déplacement
\ps v
\ge partir ; aller
\se a-da
\sge monter
\se a-du
\sge descendre
\se a-mi
\sge venir (vers le locuteur)
\se a-e
\sge s'éloigner (du locuteur)
\se a-mwã-mi
\sge revenir
\se a-mwã-e
\sge repartir
\se a hayu
\sge aller n'importe où
\se ò hayu
\dialx BO PA
\sge aller n'importe où
\se ò maya
\dialx BO PA
\sge aller doucement, ralentir
\ng |lx{ò} |dialx{BO PA}: forme de |lx{a} en composition en |dialx{BO PA}
\dt 27/Jan/2022

\lx a
\hm 2
\dialx GOs
\va al
\dialx PA BO
\is astre
\ps n
\ge soleil ; beau temps
\xv ulu a
\dialx GOs
\xn le soleil se couche
\se gòòn-a
\dialx GO
\va gòòn-al
\dialx PA
\sge midi, zénith
\xv waya hinõ al ?
\dialx PA
\xn quelle heure est-il?
\et *qaco, *qaso
\eg soleil
\el POc
\dt 08/Jan/2022

\lx a
\hm 3
\dialx GOs
\va hai, ha, ai
\dialx PA BO
\is grammaire_conjonction
\ps CNJ
\ge ou bien
\xv inu a içö
\dialx GO
\xn moi ou toi
\xv za u naa-du, vwo e threi duu-je a, bwaa-je a
\dialx GO
\xn il frappe (lit. met (la hache) en bas) pour lui couper soit le dos, soit la tête
\xv yhaamwa e trõne nai kani a yhaamwa e trõne nai ti
\dialx GOs
\xn on ne sait pas s'il l'a entendu (venant) du canard ou de qui d'autre
\xv i baxòòl ha i phõng ?
\dialx BO
\xn il est droit ou tordu ? (BM)
\dt 04/Nov/2021

\lx a
\hm 4
\dialx BO
\is grammaire_conjonction
\ps REL ou DEM
\ge que
\xv eje we a nu kido-le kõnõbwõn
\xn voici l'eau que j'ai bue hier
\xv i ra tòne a dili a i nyama
\xn il entend la terre qui bouge
\dt 08/Oct/2021

\lx a
\hm 5
\dialx PA BO
\is grammaire_voyelle_euphonique
\ps voyelle euphonique
\ge voyelle euphonique
\dn parfois réalisée schwa
\xv ho-ny (a) laviã
\dialx BO
\xn ma viande
\xv ho-ny (a) nõ lã-nã
\dialx BO
\xn ce sont mes poissons à manger
\xv mî ru na wo yala-n (a) Kaavwo
\dialx PA
\xn nous allons lui donner pour nom Kaavwo
\dt 08/Feb/2025

\lx a-
\hm 1
\dialx GO PA
\va aa-
\is grammaire_dérivation
\ps PREF.NMZ (n. d'agent)
\ge agent (préfixe des noms d')
\xv i aa-puyòl i nu
\dialx PA
\xn c'est ma cuisinière
\se a-vhaa
\sge bavard
\se a-palu
\sge avare
\se a-puunõ
\dialx GO
\sge orateur
\se a-wovwa
\dialx GO
\sge guerrier, bagarreur
\se aa-punòl
\dialx PA
\sge orateur
\dt 03/Feb/2025

\lx a-
\hm 2
\dialx GOs PA
\va aa-
\is classificateur_numérique
\ps CLF.NUM (animés)
\ge CLF des animés
\xv a-xè, a-tru, a-kò, a-pa, a-ni, a-ni-ma-xè kuau
\dialx GOs
\xn un, deux, trois, quatre, cinq, six chiens
\cf po-, wè-
\ce classificateurs
\dt 22/Feb/2025

\lx -a
\dialx GOs BO PA
\is grammaire_relateur_possessif
\ps REL.POSS
\va -w-a
\va -xa
\ge relateur possessif: de
\xv wòòdro-w-a êgu
\dialx GOs
\xn les discussions des gens
\xv mõlò-w-a êgu
\dialx GOs
\xn les coutumes des gens
\xv nobwò-w-a êgu
\dialx GOs
\xn les tâches des gens
\xv dra-w-a yai
\dialx GOs
\xn les cendres du feu
\xv dra-w-a dröö
\dialx GOs
\xn la suie sur la marmite
\xv doo-a-ny
\dialx PA
\xn ma marmite
\xv loto-w-a (la) êgu
\dialx GOs
\xn la voiture des gens
\xv loto i nu
\dialx GOs
\xn ma voiture
\xv loto i lã êgu
\dialx GOs
\xn la voiture de ces gens
\dt 05/Jan/2023

\lx ã
\dialx GOs WEM WE
\is grammaire_démonstratif
\ps DET ou PRO.DEICT
\ge ce …-ci
\xv cö za phe-zoo-ni ã pò-nu
\dialx GO
\xn tu prends cette noix de coco avec précaution
\xv e khila hovwo vwo la phoo mwã ã wõ
\dialx GO
\xn il cherche à manger pour qu'ils remplissent ce bateau
\xv li chôô-da mwa ã-li-ê
\dialx GO
\xn ces deux-là abordent
\se ã êni
\sge celui-là (DX2)
\se ã êba
\sge celui-là (DX2 sur le côté, éloigné mais visible)
\se ã õli
\sge celui-là (DX3)
\se ã êdu mu
\sge celui-là derrière
\se ã êda
\sge celui-là en haut
\se ã êdu
\sge celui-là en bas
\se ã êbòli
\sge celui-là loin en bas
\dt 26/Feb/2023

\lx -ã
\dialx GOs PA
\is grammaire_démonstratif
\ps CLIT.DEIC.1
\ge ce …-ci
\xv nye êgu-ã
\dialx PA
\xn cet homme-ci
\xv ã êgu-ã
\xn cet homme-ci
\xv êgu-mãli-ã
\xn ces deux personnes-ci
\cf -õli
\ce là (déict. distal)
\cf -ba
\ce là (déict., latéralement, visible)
\cf -ò
\ce là-bas (anaph.)
\dt 06/Mar/2023

\lx a waya ?
\dialx GOs
\is déplacement
\ps INT
\ge aller vers où ?
\dt 23/Jan/2018

\lx -ãã
\ph ɛ̃:
\dialx GOs PA BO
\va -ã
\is grammaire_pronom
\ps PRO 1° pers.PL.INCL (OBJ ou POSS)
\ge nous ; nos
\dt 06/Nov/2021

\lx aa-baatro
\ph 'a:ba:ɽo
\dialx GOs
\is caractéristiques_personnes
\va 'a-baaro
\dialx WEM BO PA
\ps n
\ge paresseux ; fainéant
\sy kônôô
\st paresseux
\dt 02/Jan/2022

\lx ããbe
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge croire tout savoir
\xv e za ããbe òri !
\xn il croit tout savoir !
\xv kêbwa ããbe !
\xn arrête de faire celui qui sait tout !
\xv çö za hine ããbe !
\xn tu crois tout savoir ! tu sais toujours tout !
\dt 20/Mar/2023

\lx aaleni
\dialx PA
\va halèèni
\dialx BO [Corne]
\is action
\ps v.t.
\ge dévier qqch.
\xv nu halèèni we
\dialx BO
\xn je dévie l'eau
\dt 29/Mar/2022

\lx aamu
\dialx GOs
\va ããmu
\dialx PA BO
\sn 1
\is insecte
\ps n
\ge mouche (grosse) |dialx{GOs}
\cf ne phû
\ce mouche bleue
\sn 2
\is insecte
\ps n
\ge abeille |dialx{PA, BO}
\se pi-ããmu, we-ããmu
\dialx PA BO
\sge miel
\dt 05/Jan/2022

\lx aa-palu
\ph a:'palu
\dialx GOs
\is échanges
\ps NMZ
\ge avare ; égoïste
\dt 22/Feb/2025

\lx aa-pwebwe wè-ce
\dialx GOs
\is poisson
\ps n
\ge poisson "million"
\sc Pœcilia reticulata
\scf Pœciliidés
\dt 05/Jan/2023

\lx aari
\dialx PA
\va hari
\dialx PA
\is nourriture
\ps n
\ge riz
\bw riz (FR)
\dt 26/Jan/2019

\lx aava
\ph 'a:va
\dialx GOs
\sn 1
\is plantes_processus
\ps v.stat
\ge vert ; pas mûr
\ge jeune (fruit)
\an zenô
\at mûr
\sn 2
\is caractéristiques_personnes
\ps v.stat
\ge fragile (nourrisson)
\xv e aava phãgoo-je
\xn son corps est fragile (se dit d'un nourrisson)
\dt 03/Feb/2025

\lx aavwê
\ph a:βẽ
\dialx GO(s)
\va aavhe
\dialx PA BO
\sn 1
\is société
\ps n
\ge étranger ; inconnu (personnes ou objets)
\xv aavwê-ã
\dialx GO
\xn elle est étrangère pour nous (lit. notre étrangère)
\xv mèni ka aavwê
\dialx GO
\xn un oiseau inconnu, jamais vu
\sn 2
\is société_organisation
\ps n
\ge clans venus de l'extérieur (pour des cérémonies)
\cf apoxapenu, awoxavhenu
\ce personnes ou clans qui accueillent les |lx{aavwê}
\dt 22/Feb/2025

\lx aazi
\ph a:ði
\dialx GOs
\is poisson
\ps n
\ge "bossu doré"
\sc Lethrinus atkinsoni
\scf Lethrinidés
\dt 27/Aug/2021

\lx aazo
\ph a:ðo
\dialx GOs PA
\is société_organisation
\ps n
\ge chef ; Grand Chef
\dt 24/Feb/2025

\lx ãbaa
\ph ɛ̃'mba:
\hm 1
\dialx GOs
\va ãbaa-n
\dialx BO PA
\is grammaire_quantificateur_mesure
\ps n.QNT
\ge autre (un)
\ge un bout de
\ge certains ; quelques
\xv ãbaa-la
\dialx GO
\xn certains d'entre eux
\xv ge le xa ãbaa-we na e zoma a iò ne thrõbo
\dialx GOs
\xn certains d'entre vous trois partiront ce soir (tout à l'heure au soir)
\xv la a, novwö la ãbaa, la tree yu
\dialx GOs
\xn ils sont partis, mais les autres sont restés
\xv ãbaa-la êgu
\dialx PA
\xn certaines de ces personnes
\xv koèn-xa ãbaa êgu
\dialx PA
\xn certaines personnes sont absentes
\xv koèn-xa ãbaa wony
\dialx PA
\xn certains bateaux ont disparu
\xv ge le xa ãbaa wony a kòen
\dialx PA
\xn il y a certains bateaux qui ont disparu
\xv ge le ãbaa wõ xa la kòi-ò
\dialx GOs
\xn il y a quelques bateaux qui ont disparu là-bas
\xv i phe ãbaa-la ko
\dialx BO
\xn il a pris un des poulets
\xv thu ãbaa mwani
\xn ajoute plus d'argent
\xv na ãbaa mwani
\xn donne plus d'argent
\dt 24/Feb/2025

\lx ãbaa
\hm 2
\ph ɛ̃'mba:
\dialx GOs
\is grammaire_quantificateur_mesure
\ps QNT
\ge en complément ; en plus
\ge autres
\xv nu phe-mi hovwo vwö ãbaa xa lhã-ã çö pavwange
\xn j'ai apporté de la nourriture en complément de/pour ce que tu as préparé
\xv kêbwa na la nõõ-me xo la ãbaa
\xn d'autres ne doivent pas nous voir
\dt 16/Feb/2025

\lx ãbaa-
\ph ɛ̃'mba:
\dialx GOs PA BO
\is parenté
\ps n
\ge frère
\ge sœur
\ge cousins parallèles (enfants de sœur de mère, enfants de frère de père)
\xv pe-ãbaa-la
\dialx PA
\xn ils sont frères et sœurs
\xv ãbaa-kêê-ny
\dialx PA
\xn le frère de mon père
\et *apə
\ea Lynch
\dt 08/Oct/2021

\lx ãbaa êmwê
\dialx GOs
\va ãbaa êmwèn
\dialx PA
\is parenté
\ps n
\ge frère
\xv ãbaa-nu êmwê
\dialx GO
\xn mon frère
\xv ãbaa-ny êmwèn
\dialx PA
\xn mon frère
\dt 26/Aug/2021

\lx ãbaa êmwê whamã
\dialx GOs
\va ãbaa êmwèn whamã
\dialx PA BO
\is parenté
\ps n
\ge frère aîné
\xv ãbaa-ny êmwèn whamã
\dialx PA
\xn mon frère aîné
\dt 26/Aug/2021

\lx ãbaa thoomwã
\dialx GOs PA
\va ãbaa-doomwã
\dialx PA
\is parenté
\ps n
\ge sœur
\xv ãbaa-nu thoomwã
\dialx GO
\xn ma sœur
\xv ãbaa-ny thoomwã
\dialx PA
\xn ma sœur
\dt 26/Aug/2021

\lx ãbaa thoomwã whamã
\dialx GOs PA
\va ãbaa-doomwã whamã
\dialx PA
\is parenté
\ps n
\ge sœur aînée
\dt 26/Aug/2021

\lx a-baxoo
\dialx GOs
\is déplacement
\ps v
\ge aller tout droit
\dt 23/Jan/2018

\lx a-bwaayu
\dialx WEM
\is caractéristiques_personnes
\ps NMZ
\ge travailleur ; courageux
\xv novwu ã i a-bwayu, i yu mwã mòn
\dialx PA WEM
\xn quant à celui qui est travailleur, il reste là
\dt 16/Oct/2021

\lx a-çabò
\ph a'ʒabɔ
\dialx GOs
\va ayabòl
\dialx PA
\va ayabwòl
\dialx BO (Corne)
\is parenté
\ps n
\ge maternels ; parenté ou famille côté maternel
\xv a-çabò i hã
\xn notre parenté maternelle
\cf cabo
\ce sortir, naître
\dt 02/Jan/2022

\lx a-chòmu
\dialx GOs
\is fonctions_intellectuelles
\ps n
\ge enseignant
\dt 05/Jan/2023

\lx a-da
\ph ãnda
\nph nasalisation régressive
\dialx GOs
\is déplacement
\is grammaire_direction
\ps v.DIR
\ge monter
\ge entrer dans une maison
\ge aller vers l'intérieur du pays
\ge aller en amont d'un cours d'eau ; sortir de l'eau
\ge aller vers le sud
\se a-daa-mi
\dialx GOs
\sge monter vers ici
\se a-da-ò
\dialx GOs
\sge monter en s'éloignant du locuteur
\xv nu a-da Numia
\dialx GOs
\xn je vais à Nouméa
\xv nu a-da na Frâ
\dialx GOs
\xn je reviens de France
\xv nu a-da na bwabu
\dialx GOs
\xn je reviens d'en bas (= de France)
\dt 22/Feb/2025

\lx a-du
\ph ãndu
\nph nasalisation régressive
\dialx GOs PA
\is grammaire_direction
\is déplacement
\ps v.DIR
\ge descendre
\ge sortir (de la maison)
\ge aller vers la mer, vers le large
\ge aller en aval d'un cours d'eau ; entrer dans l'eau
\ge aller vers le nord
\se a-du-mi
\sge descendre vers le locuteur
\se a-du-ò
\sge descendre en s'éloignant du locuteur
\xv nu a-du Frâs
\dialx PA
\xn je vais en France
\xv nu a-du Pum
\dialx GOs
\xn je vais à Poum
\xv nu a-du Aramwa
\dialx GOs
\xn je vais à Arama
\xv e a-du pwa
\xn il est sorti (de la maison)
\xv nu weena khõbwe e a-du ênè
\dialx GOs
\xn je pense qu'il est passé en bas là
\an a-da
\at entrer (dans la maison)
\dt 22/Feb/2025

\lx a-e
\ph aɛ ae
\nph la 2e voyelle est semi-ouverte et varie entre |ph{ɛ} et |ph{e}
\dialx GOs
\is déplacement
\is grammaire_direction
\ps v.DIR
\ge aller dans une direction transverse ; passer par
\xv e a-e
\xn il va dans une direction transverse
\xv nu a-e Pwebo
\xn je vais à Pouébo
\xv la a-e Hiengen
\xn ils sont allés à Hienghiène
\dt 24/Feb/2025

\lx ã-e
\ph ɛ̃ɛ ɛ̃e
\nph la 2e voyelle est semi-ouverte et varie entre |ph{ɛ} et |ph{e}
\dialx GOs
\is grammaire_démonstratif
\ps PRON.SG DEIC.1
\ge ce …-ci
\xv e phe ã-e kînu
\xn il prend cette ancre
\xv e khõbwe xo ã-e
\xn celui-ci dit
\xv e a-daa-mi ã-e kaze
\xn la marée remonte
\dt 24/Feb/2025

\lx aeke
\dialx GOs WE
\is discours_interjection
\ps INTJ
\ge salut !
\dn se dit quand on est loin
\dt 08/Feb/2025

\lx aguko
\dialx BO PA
\is maison
\ps n
\ge solive verticale
\dn pièce de bois réunissant la poutre maîtresse à la poutre de faîtage des maisons carrées (selon Dubois)
\dt 22/Oct/2021

\lx a-hêbu
\dialx GOs
\is déplacement
\ps v
\ge marcher en tête ; passer devant
\dt 05/Jan/2023

\lx a-hòò
\dialx GOs
\va a-ò
\dialx GO(s)
\is déplacement
\ps v.DIR
\ge éloigner (s')
\ge partir
\ge aller (s'en)
\xv a-hòò na inu !
\xn éloigne-toi de moi !
\xv a-ò !
\xn va-t'en ! (en s'éloignant du locuteur)
\dt 24/Feb/2025

\lx a-hovwo
\dialx GOs PA
\is nourriture
\ps n
\ge gourmand ; vorace
\dt 25/Aug/2021

\lx a-hu-da
\dialx GOs
\is grammaire_direction
\ps v
\ge aller vers le haut
\xv a-hu-mi-da
\xn viens ici en haut
\dt 27/Jan/2022

\lx a-hu-du
\dialx GOs
\is grammaire_direction
\ps v
\ge aller en descendant
\xv a-hu-mi-du
\xn viens ici en bas
\xv a-hu-du-ò
\xn continue à avancer (dans l'eau, en s'éloignant du bord)
\dt 27/Jan/2022

\lx a-hu-mi
\nph légère nasalisation régressive: |ph{a-hũ-mi}
\dialx GOs
\is grammaire_direction
\ps v
\ge approcher (s')
\xv a-(h)u-mi !
\xn approche !
\cf huri
\ce suivre
\dt 27/Jan/2022

\lx a-hu-ò
\dialx GOs
\is grammaire_direction
\ps v
\ge pousse-toi (en s'éloignant du locuteur)
\dt 27/Jan/2022

\lx ai-
\dialx GOs PA BO
\va awi-
\sn 1
\is sentiments
\ps n
\ge cœur ; amour ; volonté ; envie de
\xv ai-je
\dialx GO
\xn son cœur
\xv ai-nu
\dialx GO
\xn mon cœur
\xv phwe-ai-n
\dialx PA
\xn son cœur
\xv mhã ai-ny
\dialx PA
\xn je suis malheureux
\sn 2
\is grammaire_modalité
\ps PRED.NOM
\ge envie (avoir) ; vouloir
\xv ai-nu nye wõ-ã
\dialx GO
\xn j'ai envie de ce bateau (ou) j'aime bien ce bateau
\xv ai-nu vwo pe-mhe-õ
\dialx GO
\xn je voudrais y aller avec vous (je voudrais que nous trois y allions ensemble)
\xv la êgu ai-la nye wõ-ã
\dialx GO
\xn les gens aiment ce bateau
\xv ai ã-mãlã-ò mèni (p)u la a-du kale
\dialx PA
\xn les oiseaux veulent descendre à la pêche
\xv ai-n
\dialx PA
\xn il veut
\xv ai-m da ? avu-yu phe nya hèlè ?
\dialx PA
\xn que veux-tu ? désires-tu prendre ce couteau ?
\xv ai-ny u nu a
\dialx PA
\xn je veux partir
\xv ai-m da ?
\dialx PA
\xn que veux-tu ?
\se kixa ai
\sge pas dressé (animal) (lit. qui n'a pas de cœur) ; qui n'a pas l'âge de raison (enfant) (lit. qui n'a pas de cœur)
\dt 24/Feb/2025

\lx ai xa
\dialx GOs
\va a-xa
\dialx GOs PA
\is sentiments
\ps v (+ objet indéfini spécifique)
\ge vouloir ; envie de (avoir)
\xv ai xa mõõ-nu mã a-vwö nu marie
\dialx GO
\xn j'ai envie d'une épouse et je veux me marier
\xv ai xa wõjo-nu
\dialx GO
\xn j'ai envie d'un bateau
\xv a(i) xa wõjo-nu na wame nye
\dialx GO
\xn j'ai envie d'un bateau qui soit comme ça
\xv a(i) xa i yo
\dialx PA
\xn ta volonté, ton désir
\ng |lx{ai xa} est raccourci sous la forme |lx{axa} et |lx{aa} |dialx{PA}
\dt 20/Mar/2023

\lx ai-vwö
\ph aβω
\dialx GOs
\va a-vwö, a(i) po
\dialx GO(s)
\va a(i)-wu-; a(i)vho-
\dialx PA
\is sentiments
\ps v
\ge envie de (avoir)
\ge vouloir
\xv ai-nu vwö/po nu …
\dialx GOs
\xn je veux … que/de
\xv ai-nu vwö nu kûdo
\dialx GOs
\xn j'ai envie de boire (lit. désir-mon boire)
\xv ai vwö-nu kûdo
\dialx GOs
\xn j'ai envie de boire
\xv ai-nu vwö nu imã
\dialx GOs
\xn j'ai envie d'uriner
\xv a(i)-vwö me tròòli xa mhenõ-yuu
\xn nous voulons retrouver des lieux
\xv ai-nu phe ai-m meni kòi-m
\dialx PA
\xn je veux prendre ton cœur et ton foie
\ng |lx{a-} est la forme courte de |lx{ai} cœur, volonté ; |lx{a-vwö} est la forme contractée et jugée incorrecte de |lx{ai-vwö} vouloir que
\dt 17/Feb/2025

\lx a-kããle
\dialx GOs PA
\is médecine
\ps n
\ge médecin
\dn (lit. celui qui soigne)
\xv a-kããle-ã
\xn celui qui nous soigne
\dt 08/Feb/2025

\lx a-kai
\dialx GOs BO
\is déplacement
\ps v
\ge suivre
\ge accompagner
\dn (lit. aller suivre)
\xv ai-nu vwö nu a-kai-we
\dialx GO
\xn je voudrais/j'ai envie de vous suivre/accompagner
\xv nu a-kai-çö
\dialx GO
\xn je te suis
\xv nu ru a-kai-m
\dialx BO
\xn j'irai avec toi ; je t'accompagnerai
\se a-kai-ne
\sge ensemble
\cf höze dè
\ce suivre un chemin
\dt 20/Feb/2025

\lx a-kalu
\dialx GOs
\is parenté
\ps n
\ge maternels
\dn terme utilisé dans les cérémonies coutumières, réfère à ceux qui reçoivent les dons coutumiers lors des cérémonies
\dt 08/Feb/2025

\lx a-kaze
\dialx GOs
\va a-kale
\dialx PA
\is déplacement
\is pêche
\ps v
\ge aller à la pêche (à la mer)
\ge pêcher à la mer
\dt 25/Jan/2019

\lx a-kênõ
\dialx GOs
\is déplacement
\ps v
\ge contourner ; faire le tour
\xv e kha-kênõ
\xn il se retourne
\dt 05/Jan/2023

\lx a-kênõge
\dialx GOs
\is déplacement
\ps v.t.
\ge encercler
\ge entourer
\xv la thrê a-kênõge nõ
\xn ils courent en encerclant les poissons
\dt 22/Aug/2021

\lx a-kò
\dialx GOs
\va a-kòn
\dialx BO PA
\is grammaire_numéral
\ps NUM (animés)
\ge trois (animés)
\dt 31/Jan/2022

\lx a-kò êgu
\dialx GOs
\is grammaire_numéral
\ps NUM
\ge soixante
\dn (lit. trois hommes)
\dt 09/Feb/2025

\lx a-kò êgu bwa truuçi
\dialx GOs
\is grammaire_numéral
\ps NUM
\ge soixante-dix
\dn (lit. trois hommes et 10)
\dt 09/Feb/2025

\lx akònòbòn
\dialx BO [BM]
\is temps
\ps ADV
\ge passé ; dernier
\xv je ka akònòbòn
\dialx BO
\xn l'année dernière [BM]
\dt 26/Aug/2021

\lx a-kha-kuçaxo
\dialx GOs
\is déplacement
\ps v
\ge rendre visite à un malade
\dt 06/Feb/2019

\lx a-kha-pwiò
\dialx GOs
\is pêche
\is déplacement
\ps v
\ge aller pêcher à la senne
\dn (lit. aller lancer le filet)
\dt 09/Feb/2025

\lx a-khazia
\dialx GOs
\is déplacement
\ps v
\ge passer près de
\dt 17/May/2024

\lx ala
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge maladroit ; gauche
\xv ala la mwêêjè-je
\dialx GO
\xn il a des manières gauches ;  il est maladroit
\xv e ala òri êgu-ba !
\dialx GO
\xn ce qu'il est maladroit cet homme-là ! (lit. maladroit fou)
\dt 22/Feb/2025

\lx ala-
\dialx GOs PA BO
\sn 1
\is corps
\ps n ; n.loc
\ge face ; visage ; avant
\xv bwa ala-mè-ny
\dialx BO
\xn devant moi
\sn 2
\is configuration
\ps n ; PREF. sémantique
\ge façade ; surface
\se bwa ala-mwa ; bwa ala-mè mwa
\sge devant la maison ; devant la façade de la maison
\se ala-hi
\dialx GO
\sge paume de la main
\se ala-kòò-n
\dialx BO
\sge plante du pied ; chaussure
\sn 3
\is préfixe_sémantique
\ps PREF. sémantique
\ge contenant vide
\se ala-gu
\dialx BO
\sge valve de coquillage
\se ala-nu
\dialx BO
\sge coquille de coco vide
\se ala-we
\dialx BO
\sge calebasse
\se ala-dau
\dialx BO
\sge îlot inhabité
\sn 4
\is caractéristiques_personnes
\ge apparence ; aspect
\et *qadop
\eg face, devant
\el POc
\dt 16/Feb/2025

\lx alaaba
\dialx GOs BO PA
\is feu
\ps n
\ge braise
\dt 26/Jan/2019

\lx alaal
\dialx BO
\is poisson
\ps n
\ge picot rayé
\sc Siganus lineatus
\scf Siganidés
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx alabo
\dialx GOs WEM
\is corps
\ps n
\ge côté (du corps)
\se alabo bwa mhõ
\sge côté gauche
\dt 30/Jan/2019

\lx alabo-
\dialx PA
\is classificateur_numérique
\ps CLF.NUM
\ge quart ou moitié de tortue ou de bœuf
\dn compte les quarts ou moitiés de tortue ou de bœuf dans les échanges coutumiers
\cf mhãi-xè
\dialx PA WEM
\ce morceau de viande, de tortue, etc.
\dt 10/Jan/2022

\lx ala-hi
\dialx GOs BO PA
\wr A
\is corps
\ps n
\ge paume
\xv ala-hii-n
\dialx PA BO
\xn sa paume (ou) la paume de la main
\xv ala-hii-je
\dialx GO
\xn sa paume de main
\wr B
\is caractéristiques_personnes
\ps v
\ge habile
\xv çö za ala-hi
\dialx GO
\xn tu es habile ; tu es adroit
\xv ala-hii-je
\dialx GO
\xn il est adroit/habile
\xv za kavwö çö ala-hi
\dialx GO
\xn tu ne sais pas t'y prendre ; tu es maladroit
\dt 22/Feb/2025

\lx ala-kò
\dialx GOs PA BO
\va ala-xò
\dialx GO(s)
\sn 1
\is corps
\ps n
\ge plante de pied
\xv ala-xòò-je
\dialx GO
\xn sa plante de pied ; ses chaussures
\sn 2
\is habillement
\ps n
\ge chaussures
\xv ala-xòò-nu
\dialx GO
\xn mes chaussures
\xv ala-kòò-ny
\dialx PA
\xn mes chaussures
\se ala-xò thi
\sge claquettes
\se ala-xò-thixò
\sge chaussure à talon
\dt 22/Feb/2025

\lx ala-me
\dialx GOs PA BO
\sn 1
\is corps
\ps n
\ge visage ; face
\xv tûûne ala-me-çö !
\dialx GO
\xn essuie ton visage !
\xv ala-mee-n
\dialx PA BO
\xn son visage
\xv bwa ala-mee-ny
\dialx BO
\xn sur mon visage
\sn 2
\is configuration
\ps n.loc
\ge devant
\se ala-me-kò
\sge le devant de la jambe
\se ala-me mwa
\sge le devant de la maison
\dt 03/Feb/2025

\lx alamwi
\dialx BO PA
\sn 1
\is coutumes_objet
\ps n
\ge corde de monnaie de coquillage 
\dn selon [Dubois]
\sn 2
\is cordes
\ps n
\ge fibre de jeune rejet de |lx{phuleng}
\dn fibre lavée et séchée qui sert à faire des cordes pour les frondes, les doigtiers de sagaie ou les ceintures de guerre |lx{wa-bwanu} (selon Charles Pebu-Polae)
\dt 09/Feb/2025

\lx alavwu
\dialx GOs WEM PA BO
\va alapu
\dialx arch.
\is fonctions_naturelles
\ps v.stat
\ge faim (avoir)
\dt 03/Feb/2025

\lx alawe
\dialx GOs PA
\va olawe, olaè, olaa
\dialx BO
\is discours_interjection
\ps n.INTJ ; v
\ge au revoir !
\xv alawe i we
\dialx GO
\xn au revoir ! (à plusieurs personnes)
\xv alawe i çò
\dialx GO
\xn au revoir à vous deux !
\xv alawe-m
\dialx PA
\xn au revoir à toi !
\xv olaè-m
\dialx BO
\xn au revoir à toi !
\xv olaa-m
\dialx BO
\xn au revoir à toi
\se ba-alawe
\dialx GO
\sge coutume d'au revoir !
\dt 05/Jan/2023

\lx alaxe
\dialx GOs PA BO
\sn 1
\is position
\ps v.stat
\ge côté (sur le)
\ge travers (de) ; pas droit
\xv tre-alaxe dröö-a-çö
\dialx GO
\xn ta marmite est posée de travers (i.e. penche, ou n'est pas au milieu)
\se kô-alaxe
\dialx GO PA
\sge couché de travers
\sn 2
\is grammaire_modalité
\ps v.stat
\ge travers (de) ; mal fait
\xv la nee-alaxee-ni la mõgu i ã
\dialx GO
\xn ils ont mal fait notre travail
\xv la ne-alaxee-ni la nyama i ã
\dialx PA
\xn ils ont mal fait notre travail
\an baxòòl
\at droit
\dt 22/Feb/2025

\lx aleleang
\dialx PA
\is insecte
\ps n
\ge cigale
\dn petite et rouge
\dt 08/Feb/2025

\lx a-lixee
\dialx GOs
\is déplacement
\ps v
\ge aller à flanc de montagne
\dt 23/Jan/2018

\lx alö
\dialx GOs BO
\va alu
\dialx PA
\is fonctions_naturelles
\ps v.i
\ge regarder ; observer ; guetter
\xv e alöe-nu
\xn il me regarde
\xv e alöe-je
\xn il le/la regarde
\ng v.t. |lx{alöe} + pronom objet; |lx{alö-le} + objet nominal
\gt regarder qqch.
\dt 21/Feb/2025

\lx alobo
\dialx GOs PA
\is fonctions_naturelles
\ps v.i
\ge fixer du regard ; dévisager
\xv kêbwa alobo !
\xn ne fixe pas du regard !
\dt 03/Feb/2025

\lx alö-le
\dialx GOs BO
\va alu
\dialx PA
\is fonctions_naturelles
\ps v.t.
\ge regarder ; observer ; guetter
\xv e alö-le loto
\xn il regarde la voiture
\xv e alö-le thoomwã-ã
\xn il regarde cette femme
\xv e alö-le hênua êmwê-ã
\xn il regarde la photo de cet homme
\xv ciia ! alö-le zine !
\xn poulpe ! regarde le rat !
\xv e za alu-le
\dialx PA
\xn il le regarde
\ng v.t. |lx{alöe} + pronom objet; |lx{alö-le} + objet nominal
\gt regarder qqch.
\dt 21/Feb/2025

\lx ãmã
\ph ɛ̃mɛ̃
\dialx GOs
\sn 1
\is corps
\ps n
\ge palais (bouche)
\sn 2
\is son
\ps n
\ge brouhaha des voix
\xv ãmã-la
\xn leur brouhaha
\dt 15/Sep/2021

\lx amaèk
\dialx BO
\is plantes
\ps n
\ge mimosa (faux)
\sc Leucæna glauca
\scf Fabacées
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ã-mãla-
\dialx GOs PA BO
\is grammaire_démonstratif
\ps PRON.PL DEIC ou ANAPH
\ge les ; ces
\xv nu whizi mwã ã-mãlã ê
\dialx GOs
\xn j'ai mangé les cannes à sucre
\xv vhaa i ã-mãlã-ã
\dialx GOs
\xn les paroles de ceux-ci
\xv ã-mãlã-ã
\xn ces/ceux-ci
\xv ã-mãlã-na
\xn ces/ceux-là
\xv ã-mãlã-òli
\xn ces/ceux-là
\dt 19/Feb/2025

\lx ã-mãla-e
\dialx GOs
\is grammaire_démonstratif
\ps PRON.PL DEIC.2
\ge ces …-là
\dt 03/Feb/2025

\lx ã-mãla-na
\dialx GOs PA BO
\is grammaire_démonstratif
\ps PRON.PL
\ge ces …-là
\dt 03/Feb/2025

\lx ã-mãla-ò
\dialx GOs PA BO
\is grammaire_démonstratif
\ps PRON.PL DEIC.3
\ge ces … là-bas
\dt 03/Feb/2025

\lx ã-mãla-òli
\dialx GOs
\is grammaire_démonstratif
\ps PRON.PL DEIC.3
\ge ces … là-bas
\dt 03/Feb/2025

\lx ã-mãli-na
\dialx GOs BO
\is grammaire_démonstratif
\is grammaire_interpellation
\ps PRON.duel (DX ou ANAPH)
\ge ces deux-là (pas loin) ; hé ! les deux hommes-là !
\dt 03/Feb/2025

\lx ã-mani
\dialx BO
\is société_organisation
\ps n
\ge porte-parole
\nt selon BM
\dt 26/Mar/2022

\lx ãmatri
\ph ɛ̃maɽi
\dialx GOs
\va ãmari
\dialx GO(s)
\is bananier
\ps n
\ge banane "amérique"
\dt 05/Mar/2019

\lx a-maxo
\dialx GOs
\is société_organisation
\ps n
\ge porte-parole (du chef) ; émissaire
\dt 27/Jan/2019

\lx a-mi
\ph ɛ̃mi
\nph nasalisation régressive
\dialx GOs
\va ô-mi
\dialx BO
\is déplacement
\is grammaire_direction
\ps v.DIR
\ge aller vers le locuteur
\xv nu a-mi na bwa phwamwa
\xn je reviens du champ
\xv e a-mi ênè
\xn il est venu ici
\dt 05/Jan/2023

\lx a-mi na
\dialx GOs
\is grammaire_conjonction
\ps LOC.CONJ
\ge cause (à) de moi
\xv e gi a-mi na nu
\xn il pleure à cause de moi
\se a-mi na (ni)
\sge à cause de (provenir de)
\xv li pe-hivwine-li a-mi na (ni) nye ba-êgu
\xn ils s'ignorent à cause de cette femme
\dt 20/Jan/2022

\lx a-mõnu
\dialx GOs BO
\is déplacement
\ps v.DIR
\ge aller près de
\ge approcher
\xv a-hu-mi mõnu
\xn approche-toi tout près
\dt 05/Jan/2023

\lx amwidra
\dialx GOs
\va aamwida
\dialx BO
\is mollusque
\ps n
\ge bernacle ; anatife (chapeau chinois, clovisse)
\sc Atactodea striata
\scf Mesodesmatidés
\dt 26/Jan/2022

\lx amhee
\dialx GOs
\is société
\ps n
\ge compagnon
\xv amhee-nu
\dialx GO
\xn (c'est) mon compagnon/ma compagne
\xv amhee-nu Brigit
\dialx GO
\xn Brigit est ma compagne
\xv ge ea amhee-çö ?
\dialx GO
\xn où est ton compagnon/ta compagne ?
\sy bala
\st partenaire
\sy thilò
\st paire, l'autre d'une paire
\dt 22/Feb/2025

\lx ã-na
\ph ɛ̃ɳɛ̃
\dialx GOs BO
\is grammaire_démonstratif
\ps PRON.MASC.SG DEIC
\ge celui-là
\xv mwa ã-na
\dialx BO
\xn la maison de cet homme
\dt 03/Feb/2025

\lx ã-na !
\ph ɛ̃ɳɛ̃
\dialx GOs PA
\is grammaire_interpellation
\is grammaire_démonstratif
\ps INTJ
\ge hé ! toi!; hé l'homme-là !
\xv ã-na ! na yo wãã-na
\xn hé ! toi fais ainsi
\cf ijèè !
\ce hé ! la femme !
\cf ã-mãli-na !
\ce hé ! les (duel) hommes-là !
\dt 24/Feb/2025

\lx a-ni êgu
\dialx GOs
\va a-nim êgu
\dialx PA
\is grammaire_numéral
\ps NUM
\ge cent
\dn (lit. cinq hommes)
\dt 09/Feb/2025

\lx a-ni-ma-xe
\dialx GOs
\va a-nim-a-xe
\dialx BO PA
\is grammaire_numéral
\ps NUM (animés)
\ge six
\dt 29/Jan/2019

\lx a-niza ?
\dialx GOs
\va a-nira ?
\dialx PA BO
\is grammaire_interrogatif
\ps INT.QNT (animés)
\ge combien? (animés)
\dt 31/Jan/2022

\lx a-nûû
\dialx GOs
\is déplacement
\ps v
\ge aller à la pêche ou la chasse à la torche
\xv e a-nûû kula
\xn aller à la pêche aux crevettes à la torche
\xv e a-nûû peenã
\xn aller à la pêche aux anguilles à la torche
\xv e a-nûû drube
\xn aller à la chasse au cerf à la torche
\dt 22/Mar/2023

\lx aò
\dialx BO PA
\is société_organisation
\ps v
\ge sorcier ("emboucaneur")
\dt 27/Mar/2022

\lx a-õ
\dialx GOs BO
\is habillement
\ps v
\ge nu
\xv e pe-a-õ
\xn il va tout nu, sans rien (sur lui)
\dt 05/Jan/2023

\lx ã-ò
\dialx GOs BO
\is grammaire_démonstratif
\ps PRON.MASC.SG ANAPH
\ge celui-là (en question)
\xv i nee xo ã-ò
\xn celui-là (en question) l'a fait
\dt 03/Feb/2025

\lx a-ò !
\dialx GOs PA
\is maison
\ps INTJ
\ge va-t'en ! ; pars ! ; vas-y !
\ge appel utilisé lors de la ligature de la paille sur le toit
\dn prononcé par la personne qui est à l'intérieur de la maison à l'adresse de celui qui est sur le toit pour que ce dernier pique l'alène vers lui, i.e. vers le bas
\cf cabòl !
\ce sors ! (appel inverse de celui qui est sur le toit à l'adresse de celui qui est à l'intérieur de la maison)
\dt 19/Feb/2025

\lx ã-òòli
\dialx GOs
\is grammaire_démonstratif
\ps PRON.MASC.SG DEIC.3
\ge lui là-bas
\dt 03/Feb/2025

\lx a-pa êgu
\dialx GOs
\va aa-pa êgu
\dialx BO
\is grammaire_numéral
\ps NUM
\ge quatre-vingt
\dn (lit. quatre hommes)
\dt 09/Feb/2025

\lx a-pa êgu bwa truuçi
\dialx GOs
\is grammaire_numéral
\ps NUM
\ge quatre-vingt-dix
\dn (lit. quatre hommes et 10)
\dt 19/Feb/2025

\lx a-pe-haze
\dialx GOs
\va a-ve-hale
\dialx PA BO
\is déplacement
\ps v
\ge aller chacun de son côté ; divorcer
\xv li u a-da mwã a-ve-hale na li-na phwamwa
\dialx PA
\xn ils s'en retournent dans leur terroir respectif
\xv la a-pe-haze, la a-ve-haze
\dialx GO
\xn ils se dispersent
\xv la pe-a pe-ale
\dialx BO
\xn ils se dispersent
\dt 05/Jan/2023

\lx a-pe-hê-kòlò
\dialx BO
\is parenté_couple
\is parenté
\ps couple PAR
\ge tante et neveux/nièces
\dn (enfants de frère)
\ge enfant de frère et de cousins (femme parlant) (parenté réciproque)
\ge famille ; allié
\xv li a-pe-hê-kòlò-li
\dialx BO
\xn ils sont tante et neveux/nièces (enfants de frère)
\dt 08/Feb/2025

\lx a-pènô
\dialx GOs
\is échanges
\ps n
\ge voleur
\dt 05/Jan/2023

\lx a-pe-paçaxai
\dialx GOs
\va a-pe-pha-caaxai
\dialx PA
\is caractéristiques_personnes
\ps n
\ge farceur ; qui joue des tours ; turbulent
\dt 21/Aug/2021

\lx a-pii
\dialx GOs
\is crustacés
\ps n
\ge crabe en train de muer
\dt 08/Feb/2025

\lx apoxapenu
\dialx GOs
\va avhoxavhenu
\dialx PA
\is société_organisation
\is parenté_alliance
\ps n
\ge clan allié du clan paternel
\dn dans les cérémonies coutumières
\ge clan paternel
\dn dans les cérémonies de deuil
\dn ce clan vient soutenir le clan paternel pour offrir les dons coutumiers à ceux qui viennent d'ailleurs (|lx{aavhe}), lors des cérémonies, de deuil par exemple
\ge personnes connues |dialx{PA}
\cf aavhe
\dialx PA
\ce clan extérieur (venus pour des cérémonies)
\cf a-xalu, a-kalu
\dialx GO
\ce clan maternel (dans les cérémonies de deuil)
\cf a-kalu waniri
\dialx PA
\ce clan maternel (dans les cérémonies de deuil)
\dt 08/Feb/2025

\lx a-poxe
\dialx GOs PA
\is déplacement
\ps v
\ge aller ensemble
\dn (lit. aller un)
\xv la a-poxe
\dialx GO
\xn ils vont ensemble (lit. aller un)
\dt 09/Feb/2025

\lx a-puunõ
\dialx GOs
\va a-puunol
\dialx PA
\is discours
\ps n
\ge orateur
\dt 28/Jan/2019

\lx a-phe-vhaa
\dialx GOs PA
\is société_organisation
\ps n
\ge messager
\dn (lit. celui qui apporte le message)
\xv a-phe-vhaa i aazo
\dialx PA
\xn le messager du chef
\dt 08/Feb/2025

\lx a-phònò
\dialx GOs
\va a-phònòng
\dialx PA
\is société_organisation
\ps n
\ge sorcier ("emboucaneur")
\cf phònò
\ce sorcellerie ("boucan")
\dt 10/Jan/2022

\lx ara-hoogo
\dialx PA
\is topographie
\ps n
\ge flanc de la montagne
\xv ni ara-hoogo
\dialx PA
\xn sur le flanc de la montagne
\dt 31/Oct/2021

\lx aru
\dialx PA BO
\is cultures
\ps n
\ge butte de terre de la tarodière
\dn orientée vers le versant de la montagne, à terre moins remuée que le |lx{penu}, et qui donne de moins bons taros (selon Dubois et Charles Pebu-Polae)
\cf peenu
\ce tarodière
\dt 05/Jan/2022

\lx atibuda
\dialx GOs
\is oiseau
\ps n
\ge oiseau (petit et noir)
\se atibunu
\sge oiseau (à ventre rouge)
\dt 22/Feb/2025

\lx a-thu drõgò
\dialx GOs
\is société
\is bois_travail
\ps n
\ge sculpteur
\dn (lit. celui qui fait des masques)
\dt 09/Feb/2025

\lx a-thu kônya
\dialx GOs
\is interaction
\ps n
\ge pitre (qui fait le)
\dt 08/Feb/2025

\lx atree thrõbò
\ph aʈe: ʈʰõmbɔ
\dialx GOs
\va te-thõbwõn, te-a thõbwõn
\dialx BO
\is astre
\ps n
\ge étoile du soir ; Vénus
\dt 05/Jan/2023

\lx atrilò
\ph aʈilɔ
\dialx GOs BO PA
\is corps
\ps n
\ge glotte
\dt 24/Jan/2019

\lx a-tru êgu
\dialx GOs
\ph aʈu
\va a-ru êgu
\dialx PA
\va aa-ru êgu
\dialx BO
\is grammaire_numéral
\ps NUM
\ge quarante
\dn (lit. deux hommes)
\dt 09/Feb/2025

\lx a-tru êgu bwa truuçi
\dialx GOs
\is grammaire_numéral
\ps NUM
\ge cinquante
\dn (lit. deux hommes et 10)
\dt 09/Feb/2025

\lx au mwaji-n
\dialx PA
\is temps
\ps n
\ge retard (son) ; il est en retard
\dt 15/Aug/2021

\lx auva
\dialx GOs BO
\is mammifères_marins
\ps n
\ge dugong ; vache marine
\dt 29/Jan/2019

\lx a-uxi-cee
\dialx GOs
\is insecte
\ps n
\ge termite
\dn (lit. qui perce le bois)
\dt 08/Feb/2025

\lx a-vwe-bulu
\ph aβebulu
\dialx GOs
\is déplacement
\ps v.COLL
\ge en groupe ; ensemble (aller)
\xv la a-vwe-bulu
\dialx GO
\xn ils vont ensemble
\xv la a-vwe-bulu vwö la a hovwo
\dialx GO
\xn ils vont ensemble pour aller manger
\xv li pe-a-vwe-bulu
\dialx GO
\xn ils se déplacent ensemble
\ng |lx{a-vwe bulu} vient de |lx{a-pe-bulu} aller ensemble
\cf pe-bulu
\ce en groupe/ensemble
\dt 22/Feb/2025

\lx a-vwe-da
\ph aβenda
\dialx GOs
\is déplacement
\ps v.DIR
\ge aller en montant (ensemble)
\ng |lx{a-vwe-da} vient de |lx{a-pe-da}
\cf a-po-xè
\ce aller ensemble (lit. aller comme un)
\dt 05/Jan/2022

\lx a-vwe-du
\ph aβendu
\dialx GOs
\is déplacement
\ps v.DIR
\ge aller en descendant ensemble
\ng |lx{a-vwe-du} vient de |lx{a-pe-du}
\dt 05/Jan/2022

\lx a-vwe-e
\ph aβe-e
\dialx GOs
\is déplacement
\ps v.DIR
\ge aller sur le côté ; aller dans une direction transverse
\ng |lx{a-vwe-e} vient de |lx{a-pe-e}
\dt 05/Jan/2022

\lx avwi-
\ph aβi
\dialx GOs
\va avhi-
\dialx PA
\va api
\dialx GO(s)
\is parenté
\ps n
\ge affins du côté maternel (maternels parlant)
\dn terme d'évitement pour référer au frère ou à la sœur quand on est de sexe opposé
\xv avwi-la i nu
\dialx GO
\xn mes maternels
\xv api-nu
\dialx GO
\xn ma sœur
\xv i avhi-ny
\dialx PA
\xn il est de mon clan maternel
\cf ayabòl
\dialx PA
\ce affins du côté maternel
\dt 10/Jan/2022

\lx a-vwö
\dialx GOs
\va ai-vwö
\dialx GOs
\is nourriture
\ps v
\ge appétissant ; beau à voir (légumes)
\xv a-vwö-nu kûûńi
\xn j'ai envie de le manger
\dt 17/Feb/2025

\lx a-vwö kûdo
\ph a-βω kûndo
\dialx GOs
\va aa po kûdo
\dialx GO(s) arch.
\is fonctions_naturelles
\ps v
\ge soif (avoir)
\dn (lit. vouloir boire)
\xv a(i)-vwö-nu kûdo
\dialx GO
\xn j'ai envie de boire
\dt 09/Feb/2025

\lx avwõnõ
\dialx GOs PA BO
\ph aβɔ̃ɳɔ̃
\va apõnõ
\dialx arch.
\sn 1
\is maison
\ps n
\ge maison ; demeure
\sn 2
\is habitat
\ps n
\ge village ; ensemble de maisons
\xv avwõnõõ-n
\dialx PA
\xn sa demeure ; son village
\sn 3
\is nom_locatif
\ps n.loc
\ge chez
\dt 22/Feb/2025

\lx a-wãã-da
\dialx GOs
\is grammaire_direction
\ps v.DIR
\ge monter (direction approximative)
\se e a-wãã-da
\sge il est allé vers le haut
\se a-wãã-vwe-da
\sge monter comme ça en groupe (sans destination précise)
\ng |lx{-wãã-} indique une direction sans destination précise
\dt 27/Jan/2022

\lx a-wãã-du
\dialx GOs
\is grammaire_direction
\ps v.DIR
\ge descendre (direction approximative)
\xv e a-wãã-du Kaavwo kòlò we-za
\dialx GO
\xn Kaavwo part du côté de la mer
\xv e a-wãã-du xo Kaavwo ni kòlò we-za
\dialx GO
\xn Kaavwo part du côté de la mer (contrastif sur les personnes)
\xv e a-wãã-du ni kòlò we-za xo Kaavwo
\dialx GO
\xn Kaavwo part du côté de la mer
\se a-wãã-vwe-du
\sge descendre comme ça en groupe (sans destination précise)
\dt 05/Jan/2023

\lx a-wãã-e
\dialx GOs
\is grammaire_direction
\ps v.DIR
\ge aller dans une direction transverse (direction approximative)
\xv e a-wãã-e
\xn il est allé dans une direction transverse
\dt 27/Jan/2022

\lx a-wãã-mi
\dialx GOs
\is grammaire_direction
\ps v.DIR
\ge aller/venir vers ici
\xv e a-wãã-mi
\xn il est venu vers ici (direction approximative)
\dt 31/Jan/2022

\lx a-wãã-ò
\dialx GOs
\is grammaire_direction
\ps v.DIR
\ge aller en s'éloignant (direction approximative)
\xv e a-wãã-ò
\xn il est allé vers là-bas
\dt 27/Jan/2022

\lx awaze
\dialx GOs
\is discours_interjection
\ps v
\ge dégage ! ; sors de là !
\dt 28/Jan/2019

\lx a-wi?
\dialx GOs PA
\is déplacement
\ps INT.LOC (dynamique)
\ge où (aller) ? ; aller où ?
\xv çö a-wi ?
\xn où vas-tu ?
\xv la a-wi ?
\xn où vont-ils ?
\xv e a-wi kuau ?
\xn où va le chien ?
\xv e a-wi loto-ã ?
\xn où va cette voiture ?
\dt 05/Jan/2023

\lx a-wône-vwo
\dialx GOs
\is interaction
\ps NMZ
\ge remplaçant
\dt 18/Sep/2021

\lx a-whili
\dialx GOs
\va a-huli
\dialx PA
\is interaction
\ps n
\ge guide
\dt 27/Jan/2019

\lx a-whili paa
\dialx GOs PA
\is société_organisation
\is guerre
\ps n
\ge chef de guerre
\dt 13/Jan/2022

\lx a-whili pò
\dialx GOs
\is poisson
\ps n
\ge poisson "sabre"
\sc Chirocentrus dorab
\scf Chirocentridés
\dt 27/Aug/2021

\lx a-xalu
\dialx GOs
\va a-kalu
\dialx GO(s)
\is parenté_alliance
\is société_organisation
\ps n
\ge clan maternel (dans les cérémonies de deuil)
\cf apoxapenu
\ce clan paternel (dans les cérémonies de deuil)
\dt 18/Mar/2021

\lx axe
\dialx GO PA
\va haxe
\dialx GO PA
\is grammaire_conjonction
\ps CNJ
\ge et ; mais
\xv haxe kavwö jaxa vwo çö zòò
\dialx GO
\xn mais il ne sera pas possible que tu nages
\dt 20/Oct/2021

\lx a-xè
\dialx GOs PA BO
\va a-kè
\dialx GOs
\is grammaire_numéral
\ps NUM (animés)
\ge un (animé)
\xv a-ru ; a-tru
\dialx GO
\xn deux
\xv a-kò
\dialx GO
\xn trois
\xv a-xe (h)ada pòi-n
\dialx PA
\xn il n'a qu'un enfant
\xv a-xè pwaje
\dialx GO
\xn un crabe (vivant ou mort)
\dt 08/Feb/2025

\lx a-xè êgu
\dialx GOs PA
\va a-xe êgu
\dialx PA
\va aa-xe ãgu
\dialx BO
\is grammaire_numéral
\ps NUM
\ge vingt
\dn (lit. un homme)
\xv e u uvwi a-xè êgu jivwa po-mã
\dialx GOs
\xn il a acheté vingt mangues
\xv e u uvwi a-xè êgu jivwa po-mã
\dialx GOs
\xn il a acheté vingt mangues
\xv a-xè êgu jivwa ci-kãbwa
\dialx GOs
\xn vingt tissus
\dt 09/Feb/2025

\lx a-xè êgu bwa truuçi
\dialx GO
\va aa-xe ãgu bwa truji
\dialx BO
\is grammaire_numéral
\ps NUM
\ge trente
\dn (lit. un homme et 10)
\dt 09/Feb/2025

\lx a-xè êgu jivwa êgu
\dialx GOs PA
\is grammaire_numéral
\ps NUM
\ge vingt personnes
\dt 05/Jan/2023

\lx axe poxe hõ ma
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge mais seulement
\dt 29/Jan/2019

\lx axe poxee
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge mais en revanche
\dt 29/Jan/2019

\lx a-yu
\dialx GOs
\is société
\ps NMZ
\ge habitant (être) de
\xv e a-yu Gome
\xn il vit à Gomen
\cf êgu Gome
\ce les habitants de Gomen
\dt 05/Jan/2023

\lx a-zala
\dialx GOs
\ph aðala
\sn 1
\is pêche
\ps v
\ge aller à la pêche (sur le platier)
\sn 2
\is chasse
\ps v
\ge aller à la chasse (au cerf)
\dt 19/Feb/2025

\lx azi
\ph aði
\dialx GOs
\va khâli
\dialx PA
\va kâli
\dialx BO
\va we-khâli
\dialx PA
\is corps
\ps n
\ge bile ; fiel
\ge vésicule biliaire
\et *qasu
\el POc
\dt 08/Oct/2021

\lx azoo
\ph aðo:
\dialx GOs
\va alòò- ; alu
\dialx PA BO
\is parenté
\ps n
\ge époux ; mari
\xv azoo-nu
\dialx GO
\xn mon mari
\xv i alòò-ny, i alu-ny
\dialx PA
\xn c'est mon mari
\cf hazoo
\dialx GOs
\ce se marier
\cf halòòn
\dialx PA
\ce se marier
\et *qasawa
\el POc
\dt 20/Mar/2023

\lx ba
\hm 1
\ph mba
\dialx GOs BO PA
\sn 1
\is poisson
\ps n
\ge loche ; sardine
\sn 2
\is poisson
\ps n
\ge gobie
\sc Awaous guamensis
\scf Gobiidés
\dt 19/Feb/2025

\lx ba
\hm 2
\dialx BO (Corne)
\is maison
\ps n
\ge plateforme ; place aménagée devant la case
\nt non vérifié
\dt 26/Jan/2019

\lx ba
\hm 3
\dialx PA WEM
\is cultures
\ps n
\ge mur de soutènement de la tarodière |dialx{PA}
\ge barrage pour dévier l'eau vers la tarodière |dialx{WEM}
\se ba-khia
\sge mur de soutènement du champ d'igname
\se ba-peenu
\sge mur de soutènement de la tarodière irriguée
\se ba-mwa
\sge mur de soutènement de la maison
\et *mpaa
\el POc
\dt 26/Mar/2022

\lx ba-
\dialx GOs PA
\va baa-
\dialx GOs PA
\is grammaire_dérivation
\ps PREF.NMZ (instrumental)
\ge instrument à ; qui sert à
\dn dérive les noms d'outils, instruments, ustensiles
\xv baa-pe-nhuã-õ
\dialx GO
\xn geste d'au revoir entre nous
\se ba-iazo
\sge pierre d'herminette (adze blade)
\se ba-hal
\dialx BO
\sge rame
\se ba-he
\dialx BO
\sge allume-feu
\se ba-tabi
\sge mortier (food-pounder)
\se ba-tenge
\dialx BO
\sge fil
\se ba-thi-puu
\dialx BO
\sge perche (pour bateau)
\se ba-tu da ?
\sge pourquoi faire ? (Dubois)
\sge éventail
\dt 19/Feb/2025

\lx -ba
\dialx GOs
\is grammaire_démonstratif
\ps DEM.DEIC.2 médial
\ge celui-là (latéralement, visible)
\xv nye mwa ê-ba
\xn la maison à côté
\xv çö cö-da bwa nu-ba
\xn grimpe sur le cocotier là
\xv ijè-ba
\xn celle-là (femme)
\xv ã(ã)-ba
\xn celui-là (homme)
\dt 03/Feb/2025

\lx baa
\hm 1
\dialx GOs PA BO
\is grammaire_quantificateur_mesure
\ps PREF.COLL
\ge ensemble (dans les interpellations)
\se baa-roomwã!
\sge les femmes !
\se baa-êmwên !
\sge les hommes !
\dt 16/Feb/2025

\lx baa
\hm 2
\dialx GOs
\va baang
\dialx PA
\va baan
\dialx BO
\is couleur
\ps v.stat
\ge noir ; noirci
\xv baan u pubu yaai
\dialx BO
\xn noirci par la fumée du feu
\se dili baa
\dialx GO
\sge terre noire
\dt 03/Feb/2025

\lx baa
\hm 3
\dialx GOs
\is action_corps
\ps v
\ge frapper
\xv lha baa-nu
\xn ils m'ont frappé
\xv i baa Kawèngwa
\xn il a frappé Kawèngwa (ogre)
\xv e baa (hai)vwo
\xn il en tue beaucoup
\ng v.t. |lx{baani}
\gt frapper, tuer qqn. ou qqch.
\dt 21/Feb/2025

\lx bãã
\dialx BO
\is poisson
\ps n
\ge silure (de rivière)
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx baaba
\dialx GOs BO PA
\is corps_doigt
\ps n
\ge annulaire
\dt 24/Jan/2019

\lx baa-bwò
\dialx GOs
\is chasse
\ps v
\ge chasser la roussette
\ng v.t. |lx{baani}
\gt chasser
\dt 05/Jan/2023

\lx baadò
\dialx GOs PA
\is corps
\va baadò-n
\dialx PA BO
\ps n
\ge menton
\xv baadò-nu
\dialx GO
\xn mon menton
\xv baadò-n
\dialx PA
\xn son menton
\dt 05/Jan/2023

\lx baalaba
\dialx PA BO
\is maison_objet
\ps n
\ge lit
\dt 26/Jan/2019

\lx ba-alawe
\dialx GOs
\is coutumes
\ps n
\ge coutume de départ
\dt 23/Jan/2018

\lx baani
\ph mba:ɳi
\dialx GOs PA BO
\is action_corps
\ps v.t.
\ge tuer
\ge frapper (de haut en bas)
\ge abattre (animal)
\ge tuer (les moustiques)
\xv mi baani nhye êmwê=e
\dialx GO
\xn nous avons tué ce jeune-homme
\ng v.i. |lx{baa}
\dt 08/Feb/2025

\lx baaro
\dialx GOs WEM BO PA
\ph ba:ɽω
\va baatrö
\dialx GO(s)
\is caractéristiques_personnes
\ps v.stat
\ge paresseux (humain)
\se a-baaro, a-baatrö
\sge un paresseux; un fainéant
\xv e a-baatrö
\xn il est paresseux
\cf kônôô
\ce animal domestique; paresseux (dort toute la journée comme un animal domestique)
\an a-bwaayu
\dialx WEM
\at travailleur
\dt 22/Feb/2025

\lx baaròl
\dialx PA BO
\is poisson
\ps n
\ge loche blanche de rivière
\dt 29/Jan/2019

\lx baatro
\dialx GOs
\ph ba:ɽo
\va baaro
\dialx GO(s)
\is poisson
\ps n
\ge lochon
\dn petit poisson d'eau douce
\sc Eleotris fusca
\scf Éléotridés
\cf thraanõ
\ce lochon (taille adulte)
\dt 17/Feb/2025

\lx ba-a-tru
\ph ba.aʈu
\dialx GOs
\is grammaire_ordinal
\ps ORD
\ge deuxième (être animé)
\xv ba-a-tru ẽnõ
\xn deuxième enfant
\dt 18/Aug/2021

\lx baaxò
\ph ba:ɣɔ
\dialx GOs WEM WE
\va baaxòl
\dialx PA
\va baxòòl
\ph baɣɔ:l
\dialx BO
\sn 1
\is fonctions_intellectuelles
\ps n
\ge droit ; droiture
\xv ra thu(-xa) baxòòl i nu ?
\dialx BO
\xn ai-je le droit ? (BM)
\sn 2
\is position
\ps v.stat
\ge droit (être) ; vertical ; d'aplomb
\se a-baaxò
\dialx GO
\sge marcher tout droit
\se kô-baaxòl
\dialx PA
\sge être couché en long
\ng v.t. |lx{baxòòle}
\an phòng
\dialx BO
\at tordu
\dt 03/Feb/2025

\lx baazò
\dialx GOs
\is position
\ps v.stat
\ge en travers
\xv e kô-baazò cee bwa dè
\xn l'arbre est couché en travers de la route
\an ku-gòò
\at être droit
\an baaxò
\at être droit, vertical
\dt 03/Feb/2025

\lx ba-cabi
\dialx GOs PA
\va marto
\dialx GOs
\is outils
\ps n
\ge marteau ; instrument pour taper
\dt 27/Jan/2019

\lx ba-còxe
\dialx GOs
\is instrument
\ps n
\ge ciseaux
\dt 20/Mar/2023

\lx ba-êgu
\dialx GOs
\is société
\ps n
\ge femme (terme respectueux)
\dt 27/Jan/2019

\lx ba-hã
\dialx GOs
\is instrument
\ps n
\ge volant (voiture)
\dt 27/Jan/2019

\lx ba-ja
\dialx GOs
\sn 1
\is société_organisation
\ps n
\ge loi ; règle
\sn 2
\is grammaire_dérivation
\ps n
\ge balance
\dt 20/Mar/2023

\lx ba-kudo
\dialx GOs
\va ba-xudo
\dialx GO(s)
\va ba-kido
\dialx PA BO
\is ustensile
\ps n
\ge verre ; bol
\dt 26/Jan/2019

\lx ba-kûûni
\ph bakû:ɳi
\dialx GOs
\va ba-kuuni
\dialx BO
\is grammaire_aspect
\ps n
\ge finalement ; en guise de fin ; fin
\dt 20/Mar/2023

\lx ba-kha-da
\dialx PA
\is instrument
\ps n
\ge échelle
\dt 27/Jan/2019

\lx ba-kham thô
\dialx PA
\is ustensile
\ps n
\ge louche
\dn (lit. écope fermée)
\dt 08/Feb/2025

\lx ba-khee-vwo phwa
\dialx GOs
\va ba-kham phwa
\dialx PA BO
\is ustensile
\ps n
\ge écumoire
\dn (lit. écope à trou)
\dt 08/Feb/2025

\lx ba-khee-vwo thô
\dialx GOs
\va ba-kham thô
\dialx PA
\va ba-kam thõn
\dialx BO
\is ustensile
\ps n
\ge louche
\dn (lit. écope fermée)
\cf thô
\ce fermer
\dt 08/Feb/2025

\lx bala
\hm 1
\dialx GOs BO
\is interaction
\ps n
\ge partenaire ; coéquipier ; complice
\dn réfère à un groupe de plus de deux personnes
\xv bala-nu
\dialx GOs
\xn mon coéquipier
\xv pe-bala-î
\dialx GOs
\xn nous sommes coéquipiers
\xv pe-bala-lò
\dialx GOs
\xn ils (triel) font équipe
\se bala-yu
\sge compagnon de résidence ; serviteur
\dt 24/Feb/2025

\lx bala
\hm 2
\dialx GOs
\va bala-n
\dialx PA BO
\is configuration
\ps n
\ge limite ; bout ; fin
\xv balaa mwa
\xn les extrémités de la maison
\xv la phe bala
\xn ils prennent la suite de/poursuivent (une œuvre)
\xv haxe kixa bala bwèdò-hii-n
\dialx PA
\xn mais (elle) n’avait pas de bouts de doigts
\dt 20/Feb/2025

\lx bala
\hm 3
\dialx GOs
\sn 1
\is grammaire_modalité
\ps PTCL.MODAL (adversatif, hypothétique)
\ge adversatif ; incertain
\xv nòme çö bala a, çö thomã-nu
\dialx GOs
\xn si jamais tu t'en vas, tu m'appelles (au cas où tu t'en irais)
\xv avwö-nu a-da bwa kavwegu, xa nye bala mudra hõbwoli-nu pune digöö
\dialx GOs
\xn je voulais aller à la chefferie, mais malheureusement ma robe a été déchirée par les "cassis"
\xv e bala uça Kumwa, xa nye e ci thraa loto i je
\dialx GOs
\xn il est arrivé à Kumac, alors même que sa voiture marchait très mal
\xv e zaxòe vwo e kibao mèni, axe e bala tha
\dialx GOs
\xn il tentait de tuer l'oiseau, mais il l'a raté
\xv bala trûã !
\dialx GOs
\xn pas de chance (|lx{trûã} faire semblant); cette expression s'emploie lorsqu'on fait quelque chose qui échoue, et qu'on prétend ne pas avoir voulu faire ; la personne elle-même ou quelqu'un d'autre peut le dire
\sn 2
\is grammaire_modalité
\ps PTCL.MODAL (contrastif)
\ge contrastif
\xv çö bala yuu ?
\dialx GOs
\xn mais alors tu es resté ? (alors que tu devais partir)
\xv e bala a
\dialx GOs
\xn elle poursuit son chemin (malgré les appels)
\sn 3
\is grammaire_modalité
\ps PTCL.MODAL
\ge complètement
\xv e bala tua mwã khô-chòva-ò
\dialx GOs
\xn la corde du cheval en question s'est complètement détachée
\xv u bala thraa
\dialx GOs
\xn c'est complètement fichu !
\xv cii bala kô-raa !
\dialx GOs
\xn c'est vraiment impossible !
\xv bala mii dili
\dialx GOs
\xn la terre devient toute rouge
\sn 4
\is grammaire_modalité
\ps PTCL.MODAL-ASP
\ge pour toujours ; à jamais ; révolu
\xv ezoma bala mõgu
\dialx GOs
\xn elle va travailler là tout le temps (pour toujours)
\xv e za u a vwo bala mwã jenã
\dialx GOs
\xn elle est partie à jamais là-bas (hélas)
\dt 21/Mar/2023

\lx bala
\hm 4
\dialx GOs BO PA
\sn 1
\is préfixe_sémantique
\ps n
\ge morceau (allongé) ; partie ; moitié
\se bala-cee
\dialx BO
\sge bout de bois
\se bala-mãda
\dialx PA
\sge morceau d'étoffe
\se bala-paang
\dialx PA
\sge partie désherbée
\se bala-gò
\sge jumelles ; longue-vue (le nom provient de sa ressemblance avec un bout de bambou)
\sn 2
\is classificateur_numérique
\ps CLF.NUM
\ge morceau de canne à sucre, de bois
\se bala-xè, bala-tru, etc.
\sge un morceau de bois, deux morceaux de bois
\dt 22/Feb/2025

\lx balaa-mãã
\dialx GOs
\is eau_marée
\ps n
\ge marée haute à l'aurore
\dt 29/Jan/2019

\lx bala-khazia
\dialx GOs
\is action
\ps v
\ge trouver qqch. par hasard
\xv nu bala-khazia nye cee
\xn j'ai trouvé cet arbre par hasard
\cf khazia
\ce (être) à côté ; à proximité de
\dt 22/Feb/2025

\lx bala-khò
\dialx PA
\is configuration
\ps n
\ge limite du labour
\dn là où on s'est arrêté
\dt 08/Feb/2025

\lx bala-n
\dialx PA
\is grammaire_modalité
\ps MODAL
\ge à la suite ; dans la foulée
\xv i ra u a wu bala-n
\dialx PA
\xn il est parti à tout jamais/définitivement
\xv li hovwo, jo li bala mããni ?
\xn ils ont mangé et ils sont restés dormir (à la suite)
\xv i bala kool mwa
\dialx PA
\xn il est resté (alors qu'il était sur le point de partir)
\xv i havha kêê-n, jo nu bala khõbwe
\dialx PA
\xn son père est arrivé et je lui en ai parlé (en profitant de l'occasion)
\xv nu bala kha-phe-je
\dialx PA
\xn je l'ai pris en route
\dt 22/Feb/2025

\lx bala-nyama
\dialx PA
\is action
\ps n
\ge travail interrompu, non fini
\dt 29/Jan/2019

\lx bala-pho
\dialx GOs
\is tressage
\ps n
\ge limite du tressage
\dn là où on s'est arrêté
\dt 08/Feb/2025

\lx bala-xè
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM (morceaux de bois)
\ge un (morceau de bois)
\xv bala-xè, bala-tru, etc.
\xn un, deux morceau(x), etc.
\dt 29/Jan/2019

\lx bale
\dialx GOs
\is instrument
\ps v ; n
\ge balayer ; balai
\bw balai (FR)
\xv bale goo
\xn un balai (fait avec la nervure centrale des folioles de palmes de cocotier)
\dt 25/Aug/2021

\lx balevhi
\ph baleβi
\dialx GA
\is topographie
\ps n
\ge talus
\dt 28/Jan/2019

\lx balexa
\dialx GOs BO
\is bananier
\ps n
\ge goût pâteux d'une banane (|lx{minyô})
\dn cette banane colle à la langue quand elle n'est pas mûre (par extension, réfère à tous les fruits qui ont la même consistance)
\dt 13/Jan/2022

\lx bali-cee
\dialx GOs
\is instrument_ponts
\ps n
\ge pont en bois (sur une rivière)
\dt 31/Jan/2022

\lx ba-nhõî
\ph baɳʰɔ̃î
\dialx GOs PA
\is cordes
\ps n
\ge lien
\dn (lit. qui sert à attacher)
\dt 09/Feb/2025

\lx ba-õgine
\dialx GOs BO
\va ba-õgine-n
\dialx PA
\sn 1
\is grammaire_ordinal
\ps ORD
\ge dernier (le)
\sn 2
\is grammaire_dérivation
\ps NMZ
\ge fin
\xv ni ba-õgine mwhããnu
\dialx BO
\xn à la fin du mois
\xv ba-õgine-n
\dialx PA
\xn pour terminer ; pour finir
\dt 22/Feb/2025

\lx ba-õ-xè
\dialx GOs
\is grammaire_ordinal
\ps ORD
\ge première fois
\xv ba-õ-xè xa nu nõõ-je
\dialx GOs
\xn c'est la première fois que je le vois
\cf ba-õ-tru, ba-õ-kò, etc.
\ce deuxième fois, troisième fois, etc.
\dt 24/Feb/2025

\lx ba-paaba
\dialx GOs
\is navigation
\ps n
\ge rame
\dt 23/Jan/2018

\lx ba-pase
\dialx GOs
\is instrument
\ps n
\ge tamis
\bw passer (FR)
\se ba-pase õ
\sge tamis à sable
\se ba-pase kafe
\sge filtre à café
\se ba-pase nu
\sge passoire (lit. qui sert à passer le coco)
\dt 25/Aug/2021

\lx ba-pe-ravhi
\dialx PA
\is soin
\is instrument
\ps n
\ge rasoir
\dt 08/Oct/2021

\lx ba-pe-thra
\ph bapeʈʰa
\dialx GOs
\is soin
\is instrument
\ps n
\ge rasoir
\cf pe-thra
\ce raser (se)
\dt 02/Jan/2022

\lx ba-phe-ẽnõ
\dialx PA
\is coutumes
\ps n
\ge don lors d'une adoption
\dn (lit. sert à prendre l'enfant)
\dt 08/Jan/2022

\lx ba-phe-vwo
\dialx GOs
\is portage
\ps n
\ge bât (cheval)
\ge porte-bagages
\dt 26/Aug/2021

\lx ba-thaavwu
\dialx GOs
\va ba-thaavwun
\dialx PA
\is grammaire_aspect
\ps ASP
\ge pour commencer ; début
\dt 17/Oct/2021

\lx ba-thiçe
\ph batʰiʒe
\dialx GOs
\is feu
\is instrument
\ps n
\ge tisonnier
\dt 02/Jan/2022

\lx ba-thi-halelewa
\dialx GO PA BO
\va ce ba-thi halelewa
\dialx GOs
\is instrument
\ps n
\ge bâton à glu pour attraper les cigales
\dn jeu d'enfants pratiqué avec la glu du gommier
\dt 22/Oct/2021

\lx ba-thu-khia
\dialx PA BO
\is société_organisation
\is cultures
\ps v ; n
\ge labourer le champ d'igname du chef
\ge ouverture du champ d'igname du chef
\dt 18/Aug/2021

\lx ba-thu-pwaalu
\dialx GOs
\is coutumes
\ps n
\ge geste de respect
\dt 28/Jan/2019

\lx ba-trabwa
\ph baɽabwa
\dialx GOs BO PA
\va ba-rabwa
\dialx GO(s)
\is maison_objet
\ps n
\ge chaise ; banc ; chaise ; siège
\se ba-tabwa cova, ba-tabwa cova
\dialx PA
\sge selle
\se mhenõ-(t)rabwa
\sge siège (tout ce qui sert à s'asseoir)
\dt 02/Jan/2022

\lx ba-tröi
\ph baɽωi, baɽui
\dialx GOs
\va ba-rui
\dialx PA
\is ustensile
\ps n
\ge cuillère
\se ba-tröi pòńõ
\dialx GOs
\sge petite cuillère
\se ba-tröi whaa
\dialx GOs
\sge grande cuillère
\cf tröi
\dialx GOs
\ce puiser
\cf truu
\dialx PA
\ce plonger
\dt 10/Jan/2022

\lx ba-thrôbo
\ph baʈʰôbo
\dialx GOs
\is caractéristiques_objets
\ps n
\ge talus
\dt 02/Jan/2022

\lx ba-u
\dialx GOs
\va ba-ul
\dialx WEM WE BO PA
\is feu
\is instrument
\ps n
\ge éventail
\dn en feuille de cocotier, utilisé pour le feu
\cf ula
\ce éventer
\dt 08/Feb/2025

\lx ba-uxi-cee
\ph bauɣicɨ:
\dialx GOs
\is outils
\ps n
\ge perceuse
\dt 02/Jan/2022

\lx bavala
\dialx PA BO
\is position
\ps v
\ge aligné
\ge côte à côte
\xv u tee-bavala xo li
\dialx PA BO
\xn elles restent côte à côte
\se a-bavala
\dialx PA
\sge marcher côte à côte
\se ku-bavala
\dialx PA
\sge debout en ligne ; côte à côte
\se tee-bavala
\dialx PA
\sge assis côte à côte
\se ta-bavala
\dialx BO
\sge assis en ligne [BM]
\dt 22/Feb/2025

\lx ba-zo
\dialx GOs
\va ba-zòl
\dialx PA
\va ba-yòl
\dialx BO
\is ustensile
\ps n
\ge grattoir
\dt 26/Jan/2019

\lx ba-zo cee
\ph bazo cɨ:
\dialx GOs PA
\va ba-yo
\dialx BO
\is outils
\ps n
\ge scie
\dt 02/Jan/2022

\lx ba-… (le)
\dialx GOs PA BO
\va na-
\dialx BO arch.
\is grammaire_ordinal
\ps ORD
\ge énième
\xv e phe nye ba-wè-tru-le
\dialx GO
\xn elle prend le deuxième (objet long: bout de bois)
\xv ba-po-xè, ba-po-tru
\dialx GO
\xn premier, deuxième (inanimé)
\xv ba-õ-kò xa nu nõõ-je ni tree
\dialx GO
\xn c'est la troisième fois que je le vois dans la journée
\xv ba-pwò-ru, ba-po-ru
\dialx PA
\xn deuxième
\xv ba-axè êgu
\xn vingtième
\xv ba-õ-xè xa nu nõõ-je
\dialx GO
\xn c'est la première fois que je le vois
\xv na-po-xe
\dialx BO
\xn premier (inanimé) (Dubois)
\dt 19/Feb/2025

\lx be
\dialx GOs PA BO
\is insecte
\ps n
\ge ver de terre
\et *mpasa, *mpaya
\eg ver de terre
\el POc
\dt 01/Feb/2019

\lx bè
\hm 1
\dialx GOs
\is arbre
\ps n
\ge banian
\dn à racines aériennes
\sc Ficus sp.
\scf Moracées
\cf bumi
\ce banian
\et *baqa
\el PSO (Proto-South Oceanic)
\ea Geraghty
\dt 08/Feb/2025

\lx bè
\hm 2
\dialx GOs
\va bèn
\dialx BO PA
\is oiseau
\ps n
\ge râle (oiseau) ; bécasse
\sc Rallus philippensis swindellsi
\scf Rallidés
\dt 27/Aug/2021

\lx bea
\dialx BO
\is igname
\ps n
\ge igname blanche
\nt selon Dubois
\dt 26/Mar/2022

\lx bee
\hm 1
\dialx GOs
\va been
\dialx PA BO
\is caractéristiques_objets
\ps v.stat
\ge mouillé ; humide
\xv dròò-chaamwa bee
\dialx GO
\xn des feuilles fraîches de bananier
\xv nuu-pho bee
\dialx GO
\xn des fibres fraîches de pandanus
\ng causatif: |lx{pa-beene} |dialx{BO}
\gt mouiller
\an mãû
\at sec
\dt 03/Feb/2025

\lx bee
\hm 2
\dialx PA
\is plantes_processus
\ps v.stat
\ge vert (pour des tubercules et des fruits)
\an te
\at commencer à mûrir
\dt 22/Feb/2025

\lx bee-
\dialx GOs PA BO
\is parenté_alliance
\ps n
\ge parenté par alliance
\ge sœur du mari ; frère d'épouse ; mari de sœur
\ge sœur ou frère du beau-frère ; sœur ou frère de la belle-sœur (désigne aussi "homme parlant" les cousins parallèles de l'épouse: fils de frère de père, fils de sœur de mère et les cousins croisés de l'épouse: fils de frère de mère, fils de sœur de père)
\dn parents par alliance; anciennement |lx{bee-} ne faisait référence qu'aux hommes, (aux beaux-frères) et |lx{phalawu-} référait aux femmes
\xv bee-nu thoomwã
\dialx GO
\xn ma belle-sœur
\xv bee-nu êmwê
\dialx GO
\xn mon beau-frère
\xv bee-ny doomwã, bee-ny thoomwã
\dialx PA
\xn ma belle-sœur
\cf mõû-
\ce sœur de l'épouse
\dt 10/Jan/2022

\lx beela
\dialx GOs PA BO
\sn 1
\is mouvement
\ps v
\ge ramper (enfant) ; marcher à quatre pattes (enfant)
\sn 2
\is mouvement
\ps v
\ge monter aux arbres (en serrant le tronc)
\xv e beela bwa cee
\xn il grimpe à l'arbre
\dt 08/Nov/2021

\lx bèèxu
\dialx BO
\is fonctions_naturelles_animaux
\ps v
\ge en rut
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx beloo
\dialx GOs PA
\sn 1
\is santé
\ps v.stat
\ge faible ; fragile
\sn 2
\is caractéristiques_personnes
\ps v.stat
\ge mou ; sans énergie
\ge lent ; indolent
\xv e mõgu beloo
\xn il a travaillé mollement
\an thani
\at dynamique
\dt 03/Feb/2025

\lx bemãbe
\dialx GOs
\va bemêbe
\ph bemɛ̃mbe
\is caractéristiques_personnes
\ps v.stat
\ge courageux ; travailleur ; débrouillard
\an kônôô
\at paresseux
\an baatro, baaro
\at paresseux
\dt 03/Feb/2025

\lx benõõ
\ph benɔ̃:
\dialx PA BO WEM
\is natte
\ps n
\ge natte
\dn de palmes de cocotier tressées
\dt 08/Feb/2025

\lx bèsè
\dialx GOs
\is ustensile
\ps n
\ge bassine ; cuvette
\bw bassine (FR)
\dt 26/Jan/2019

\lx bèxè
\dialx GOs
\is santé
\ps n
\ge bourbouille
\dt 25/Jan/2019

\lx be-yaza
\dialx GOs
\va be-ala-
\dialx PA
\va be-(y)ala
\dialx WEM WE
\is société
\ps v
\ge homonyme (être)
\dn porter le même nom
\xv nu be-yaza gee
\dialx GO
\xn j'ai le même nom que grand-mère
\xv pe-be-yaza-bî
\dialx GO
\xn nous avons le même nom; nous sommes homonymes
\xv pe-be-yaza-li
\dialx GO
\xn ils (duel) sont homonymes
\xv i be-ala-ny
\dialx PA
\xn c'est mon homonyme
\xv pe-be-(y)ala-bî
\dialx PA
\xn nous avons le même nom
\xv li evhi be-alaa-n ; li evhe be-alaa-n
\dialx PA
\xn ils portent le même nom
\xv ã-be-(y)ala-ny nyòli
\dialx WEM
\xn celui-là a le même nom que moi
\dt 24/Feb/2025

\lx bi
\hm 1
\dialx GOs
\va biny
\dialx PA BO WE
\is santé
\ps v.stat
\ge maigre
\ge dégonflé
\xv e bi bwoo
\dialx GO
\xn le ballon est dégonflé
\ng causatif: |lx{phaa-bi-ni}
\gt dégonfler
\dt 03/Feb/2025

\lx bi
\hm 2
\dialx GOs
\is jeu
\ps n
\ge bille (de petite taille ; jeu)
\bw bille (FR)
\dt 15/Aug/2021

\lx bî
\hm 3
\dialx GOs PA
\is grammaire_pronom
\ps PRO 1° pers. duel.EXCL. (sujet, OBJ ou POSS)
\ge nous deux (excl.) ; notre
\dt 26/Mar/2023

\lx -bî
\dialx GOs
\va -bin
\dialx PA BO
\is grammaire_pronom
\ps PRO 1° pers. duel.EXCL. (OBJ ou POSS)
\ge nous deux (excl.) ; notre
\dt 12/Nov/2021

\lx bibi
\dialx GOs PA BO
\is parenté_adresse
\ps n
\ge cousin (croisé de même sexe)
\cf ebiigi
\ce cousin croisé de même sexe (terme de désignation)
\dt 22/Feb/2025

\lx biça
\ph biʒa
\dialx WEM
\sn 1
\is déplacement
\ps v
\ge dépasser
\xv nu biça i je
\xn je la dépasse
\xv va pe-thu-mhenõ, nu kha-biça i je
\xn nous nous promenons, je la dépasse
\sn 2
\is grammaire_quantificateur_degré
\ps v.QNT
\ge trop
\ge plus encore
\xv la ra u mara thu-wöloo, mara u biça mwa !
\xn ils se sont mis à troubler l'eau et elle l'était encore plus
\xv ra u biça halelewa !
\xn il y a énormément de cigales
\xv biça mwani i je
\xn il a énormément d'argent
\dt 07/May/2024

\lx biçô
\ph biʒô
\dialx GOs
\sn 1
\is action_corps
\ps v
\ge accrocher (s') ; suspendre (se)
\xv e za u biçôô-je
\dialx GO
\xn elle s'accroche à lui
\sn 2
\is position
\ps v
\ge accroché ; suspendu
\cf côô
\ce suspendu
\dt 25/Jan/2019

\lx bii
\hm 1
\dialx GOs
\is caractéristiques_objets
\ps v ; n
\ge enfoncé ; cabossé
\ge choc ; impact
\xv e bii wãge
\xn il s'est enfoncé la cage thoracique
\xv e bii phalaxe-je
\xn il a les côtes fêlées
\se tha-bii loto
\sge cabosser la voiture
\dt 20/Mar/2023

\lx bii
\hm 2
\dialx GOs PA
\is cultures
\ps n
\ge conduite d'eau en bambou ; canalisation
\dn conduit l'eau de la rivière à la tarodière
\xv lha thu bii
\xn elles font une conduite d'eau
\dt 22/Oct/2021

\lx biibo
\dialx GOs
\va bibwo
\dialx GO(s)
\is action_tête
\ps v
\ge embrasser ; faire un baiser
\dt 20/Mar/2023

\lx biije
\ph bi:ɲɟe
\dialx GOs PA BO
\is nourriture
\ps v
\ge mâcher (pour extraire le jus) ; mastiquer (des fibres)
\dn telles que l'écorce du bourao et du magnania
\cf hovwo
\ce manger (général)
\dt 22/Feb/2025

\lx bile
\dialx GOs BO
\va bire
\dialx GOs
\is action
\is cordes
\ps v ; n
\ge rouler un toron
\dn un toron de corde sur la cuisse
\ge enrouler
\ge filer
\ge corde à torons tordus
\se bile wa
\sge rouler un toron
\se wa-bile
\sge fil de filet
\et *piri
\eg plait a cord, twist
\el POc
\ea Blust
\dt 08/Feb/2025

\lx bilòò
\dialx GOs BO PA
\is action_corps
\ps v
\ge tourner
\ge tordre ; enrouler (une corde)
\ge moudre
\ge essorer
\ge (dé)visser
\xv e kaa-bilòò kòò-je
\xn elle s'est tordu la cheville (lit. appuyer tordre)
\xv e kaa-bilòò hii-je
\xn elle s'est tordu le poignet (en tombant et en appuyant dessus)
\se bilòò kafe
\sge moudre le café (en tournant le moulin)
\se bilòò-du
\sge visser
\se bilòò-da
\sge dévisser
\dt 20/Mar/2023

\lx biluu
\dialx GOs PA BO
\is mollusque
\ps n
\ge escargot de terre ; bulime
\sc Placostylus fibratus
\scf Bulimulidés
\dt 27/Aug/2021

\lx bîni
\ph bîɳi
\dialx GOs
\va biini
\dialx PA
\va bîni
\dialx BO
\is action
\ps v.t.
\ge plier (linge)
\ge enrouler (tissu, natte)
\xv nu bî hõbwò
\xn j'ai plié le linge
\xv i bîni hõbwòn
\xn j'ai plié le linge
\dt 09/Mar/2023

\lx bizi
\dialx GOs
\va biri
\dialx BO
\is action_corps
\ps v
\ge étrangler (avec les mains)
\xv i biri nõõ-n
\dialx BO
\xn il l'étrangle (lit. lui serre le cou)
\dt 25/Jan/2019

\lx bizigi
\dialx GOs
\va birigi
\dialx GO(s) WE
\va bitigi
\dialx PA BO
\sn 1
\is caractéristiques_objets
\ps v.stat
\ge collé
\ge collant (sous la dent) |dialx{PA}
\xv i bitigi nye kui
\dialx PA
\xn cette igname est pâteuse/collante
\cf tigi
\ce être pris (dans un filet)
\ng causatif: |lx{pha-bizigi-ni}
\gt coller qqch.
\sn 2
\is matière
\ps n ; v.stat
\ge résine |dialx{PA}
\dn de sapin, kaori
\ge collant comme de la résine
\dt 22/Feb/2025

\lx bo
\ph mbo
\dialx GOs PA BO
\va bwo
\dialx GO(s)
\va bon, bwon
\dialx BO
\sn 1
\is fonctions_naturelles
\ps n
\ge odeur
\xv bo-çö
\dialx GO
\xn ton odeur
\sn 2
\is fonctions_naturelles
\ps v.i
\ge sentir (odeur) ; avoir une odeur
\xv e bo yaai hõbòli-nu
\xn ma robe sent le feu
\se bo-raa; bo traa
\dialx GO
\sge sentir mauvais
\se bo zo
\sge sentir bon
\se me-bo zo
\dialx BO
\sge bonne odeur
\ng v.t. |lx{bole}
\et *bona
\el POc
\dt 03/Feb/2025

\lx bò
\hm 1
\dialx GOs
\va bòl
\dialx BO
\is action_corps
\ps v
\ge taper avec un bâton
\xv e aa-bò
\xn il frappe souvent
\xv e bòzi-nu
\dialx GO
\xn il m'a tapé
\se ba-bò
\sge bâton
\ng v.t. |dialx{GOs} |lx{bòzi} ; |dialx{PA} |lx{bòli}
\gt frapper qqch.
\dt 21/Feb/2025

\lx bò
\hm 2
\dialx GOs
\va bòng
\ph bɔŋ
\dialx BO PA
\is topographie
\ps n
\ge pente (de montagne) ; ravin
\xv kòlò bò
\dialx GO
\xn le bord du ravin
\xv kòlò bòng
\dialx BO
\xn le bord du ravin ; le talus
\dt 22/Feb/2025

\lx bò
\hm 3
\dialx GOs
\va bòng
\dialx BO [BM]
\is plantes_processus
\ps v
\ge faner
\ge roussi (par le feu)
\dn se dit par ex. des feuilles d'arbre roussies par le feu
\cf mènõ
\ce faner naturellement
\dt 26/Aug/2021

\lx bö
\dialx WE BO PA
\va bwö
\dialx BO
\is feu
\ps v
\ge éteindre (s') (un feu)
\xv u bö yaai
\dialx BO
\xn le feu s'est éteint [BM]
\xv i bööni yaai
\dialx WEM
\xn il a éteint le feu
\xv i bö nuua-n
\dialx BO
\xn sa torche s'est éteinte
\ng v.t. |lx{b(w)ööni} |dialx{BO}
\gt éteindre qqch.
\dt 21/Feb/2025

\lx bòdra
\ph bɔɖa
\dialx GOs
\va bòda
\dialx BO
\sn 1
\is couleur
\ps n
\ge teinture noire
\sn 2
\is plantes
\ps n
\ge Alphitonia
\dn arbuste dont l'écorce a une odeur camphrée
\sc Alphitonia neocaledonica
\scf Rhamnacées
\dt 08/Feb/2025

\lx boe
\dialx GOs
\is temps_atmosphérique
\ps v.stat
\ge nuageux
\xv e  boe dònò
\xn le ciel est nuageux
\dt 03/Feb/2025

\lx böji-hi
\dialx GOs
\va buyi ni hi
\dialx WEM
\va böyil
\dialx PA
\is corps
\ps n
\ge coude ; l'articulation de (tout) le bras
\ge poignet
\dt 21/Aug/2021

\lx böji-kò
\dialx GOs
\va buyi ni kò
\dialx WEM
\va böyil
\dialx PA
\is corps
\ps n
\ge genou ; articulation de (toute) la jambe
\dt 21/Aug/2021

\lx bole
\dialx GOs PA BO
\is fonctions_naturelles
\ps v.t.
\ge humer ; sentir
\xv e bole mû-cee
\dialx GO
\xn il hume (le parfum de) la fleur (en la prenant)
\dt 19/May/2024

\lx böleõne bo
\ph bωleõɳe bo
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge humer ; sentir
\dn volontairement
\cf trõne bo
\dialx GOs
\ce sentir (sans intention)
\dt 08/Feb/2025

\lx bòli
\dialx GOs
\is grammaire_direction
\ps DIR
\ge là-bas en bas
\xv nõõ-je bòli !
\xn regarde-le/là là-bas en bas !
\xv novwö ili-bòli, kêê Mwani-mii
\xn quant à eux deux là-bas en bas, les parents de Mwani-mii
\dt 22/Feb/2025

\lx bòlòmaxaò
\dialx GOs
\va boloxaò
\dialx GO(s)
\va vaaci
\dialx WE
\va vaci
\dialx PA
\is mammifères
\ps n
\ge bétail
\bw bull and cow (GB)
\dt 25/Aug/2021

\lx bò-na, bwò-na
\dialx GOs
\ph bɔɳa bwɔɳa
\va bwòn-na
\dialx PA BO
\va bòna
\dialx BO
\is temps_deixis
\ps ADV
\ge après-demain ; le lendemain
\ge à l'avenir
\se bò-na ô-xe
\sge après-après-demain; dans trois jours
\dt 24/Feb/2025

\lx böö
\ph mbω
\dialx GOs
\is santé
\ps v
\ge réaction cutanée (avoir une) à qqch. ; s'infecter avec la sève de l'acajou
\xv e böö phwa-je xo pò-mãã
\dialx GO
\xn sa bouche a eu une réaction cutanée à cause de la mangue
\dt 29/Mar/2022

\lx booli
\dialx PA WEM BO
\va bwooli
\dialx WEM BO
\is arbre
\ps n
\ge arbre
\dn son bois dur fait des étincelles en brûlant et n'est donc pas utilisé pour le feu de nuit
\sc Acronychia lævis
\scf Rutacées
\nt selon Corne
\dt 08/Feb/2025

\lx bööni
\ph mbω:ɳi
\dialx GOs PA BO
\is maison
\ps v.t.
\ge construire (un mur) ; faire un mur
\ng v.i. |lx{bö}
\gt construire
\dt 05/Jan/2022

\lx bosu
\dialx GOs
\is interaction
\ps v
\ge saluer (se)
\bw bonjour (FR)
\xv mo pe-bosu
\xn nous nous saluons
\dt 25/Aug/2021

\lx boxola
\dialx PA
\is plantes
\ps n
\ge herbe coupante et malodorante
\dt 29/Jan/2019

\lx bòzi
\dialx GOs
\va bòli
\dialx BO PA
\is action_corps
\ps v
\ge fouetter
\ge corriger ; châtier
\xv e bòzi-nu
\dialx GO
\xn il m'a tapé
\dt 06/Jan/2022

\lx bozo
\dialx GOs
\va bolo
\dialx PA WE
\is corps
\ps n
\ge cordon ombilical
\ge nom de l'arbre planté à la naissance
\dn et sous lequel on enterrait le cordon ombilical
\xv bozo-nu
\xn mon cordon ombilical
\se phwe-bozo
\sge nombril
\et *mpusos
\el POc
\dt 08/Feb/2025

\lx bu
\hm 1
\dialx GOs PA BO
\is nourriture
\is plantes_processus
\ps v.stat
\ge vert (fruit) ; pas mûr
\an mii
\at mûr
\dt 03/Feb/2025

\lx bu
\hm 2
\dialx GOs PA
\is cultures
\ps n
\ge talus
\ge tertre
\ge tas
\ge billon
\xv bu dili
\dialx PA
\xn billon de terre
\et *apu
\eg mound for house site
\el PEOc (Proto-Eastern Oceanic)
\ea Blust
\dt 22/Aug/2021

\lx bu
\hm 3
\dialx GOs
\va bul
\dialx BO PA
\is mouvement
\is navigation
\ps v
\ge sombrer ; couler ; noyer (se)
\ge chavirer ; retourner (se)
\xv i bul (e) wony
\dialx PA
\xn le bateau a coulé
\xv nu pa-bu-ni wõ
\dialx GO
\xn j'ai fait chavirer le bateau
\ng causatif: |lx{pa-bu-ni}
\gt faire chavirer
\dt 05/Jan/2022

\lx bu
\hm 4
\dialx GOs PA
\is action_corps
\ps v
\ge frapper ; taper (une cible)
\xv e bu-i je
\dialx GO
\xn il l'a frappé ; il est entré en collision avec lui
\xv e bu ni cee xo loto
\dialx GO
\xn la voiture est entrée dans l'arbre
\xv li pe-bu-i li
\dialx GO
\xn ils sont entrés en collision
\xv e bu hòò
\dialx GO
\xn ça a tapé loin
\se jige bu hòò
\dialx GO
\sge fusil à longue portée (lit. qui tape loin)
\ng v.t. |lx{bule} + objet nominal (nom commun)
\gt frapper, cogner qqch. ou qqn.
\dt 22/Feb/2025

\lx bu
\hm 5
\dialx GOs
\is reptile_marin
\ps n
\ge tortue "bonne écaille"
\dt 29/Jan/2019

\lx bu
\hm 6
\dialx PA
\is sentiments
\ps v
\ge refuser ; ne pas vouloir
\xv bu je ne i zage-je ko/vwo nyamã
\dialx PA
\xn il refuse de l'aider à travailler
\xv bu je yaai na nûû
\dialx PA
\xn il ne veut pas que le feu éclaire
\dt 25/Dec/2021

\lx bu-
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge billon (d'igname) ; monticule
\xv bu-xè bu kui, bu-ru bu kui
\dialx PA
\xn une, deux buttes d'igname
\xv bu-ru bu õn
\dialx PA
\xn deux tas de sable
\dt 22/Feb/2025

\lx bua
\ph bua
\dialx GOs PA BO
\is interaction
\ps v
\ge héler ; faire signe
\ge crier (pour annoncer sa présence)
\xv u bua na le, a khibi mwã na bwe Kaòla
\dialx PA
\xn (il) crie là, et cela résonne sur Kaòla (montagne)
\dt 26/Jan/2022

\lx buaõ
\ph buaɔ̃
\dialx GOs
\va bwaô
\dialx PA
\va buaôn
\dialx BO
\is reptile_marin
\ps n
\ge serpent de mer (tricot rayé) ; plature
\cf bwaa
\ce serpent de mer (gris)
\dt 26/Aug/2021

\lx bubu
\dialx BO PA
\is couleur
\ps v.stat
\ge vert ; bleu
\dt 22/Feb/2025

\lx bue
\ph buɨ
\dialx PA
\is action_tête
\ps v
\ge embrasser
\dt 26/Jan/2019

\lx bu-êgu
\dialx PA
\is étapes_vie
\ps n
\ge tombe
\dt 27/Jan/2019

\lx bui
\dialx BO PA
\is habillement
\ps n
\ge bracelet
\dn formé d'un seul coquillage taillé
\nt selon Dubois ms.
\dt 08/Feb/2025

\lx bûkû
\dialx GOs
\va bûûwû
\dialx BO [BM]
\is fonctions_naturelles
\ps v
\ge sentir mauvais (odeur de chair)
\xv li bûkû hii-nu
\dialx GO
\xn mes mains sentent le poisson
\cf bû nõ
\ce odeur de poisson ; sentir le poisson
\dt 22/Feb/2025

\lx bu-kui
\dialx GOs PA
\is igname
\is cultures
\ps n
\ge butte d'igname
\dt 26/Jan/2019

\lx bulago
\dialx GOs
\is caractéristiques_objets
\ps v
\ge pourri ; effriter (s')
\xv e bulago kòli phwè-mwa
\dialx GOs
\xn le linteau de la porte s'effrite (il est pourri ou mangé par les termites)
\dt 22/Feb/2025

\lx bulaivi
\dialx GOs PA BO
\is armes
\ps n
\ge casse-tête (générique)
\dt 23/Jan/2018

\lx bule
\dialx GOs PA
\is mouvement
\ps v.t.
\ge toucher une cible
\xv e bule mèni
\dialx GO
\xn il a touché l'oiseau
\ng v.i. |lx{bu}
\dt 06/Jan/2022

\lx buleony
\dialx BO [Corne, BM]
\va buleon
\is arbre_cocotier
\ps n
\ge spathe de cocotier
\dn cette feuille enveloppe l'inflorescence, ou est à la base du pédoncule floral utilisé pour envelopper la monnaie traditionnelle
\dt 22/Oct/2021

\lx buli
\dialx GOs BO
\is société
\ps v.stat
\ge décimé ; sans descendance
\dt 03/Feb/2025

\lx bulu
\dialx GOs PA BO
\wr A
\is grammaire_quantificateur_mesure
\ps n
\ge assemblage ; réunion ; ensemble
\ge groupe |dialx{PA BO}
\ge bande (oiseaux, enfants) |dialx{PA BO}
\ge tas |dialx{PA BO}
\xv yu nòòle lã-nã bulu paa ?
\dialx BO
\xn tu vois ce tas de cailloux ? [BM]
\se bulu ẽnõ
\sge bande d'enfants
\se bulu mèni
\sge bande d'oiseaux
\se bulu paa
\dialx BO
\sge tas de pierre
\wr B
\is grammaire_quantificateur_mesure
\ps COLL.ADV
\ge ensemble
\xv li a bulu
\xn ils partent ensemble
\xv e thaivwi-bulu-õ mwã
\xn il nous rassemble ici
\xv pe-na-bulu-ni
\xn mets-les ensemble
\xv tha-bulu-ni ja!
\xn rassemble les saletés !
\se naa bulu
\sge mettre ensemble
\se thaivwi bulu
\dialx GO
\sge entasser ; ramasser
\cf a pò-xè
\ce partir ensemble (lit. partir un ; i.e., partir comme un)
\wr C
\is grammaire_quantificateur_mesure
\ps v.COLL
\ge être ensemble
\se p(h)a-bulu-ni
\sge entasser
\se nee pe-bulu-ni
\sge faire ensemble
\ng causatif: |lx{pa-bulu-ni}
\gt rassembler
\dt 22/Feb/2025

\lx bumi
\dialx GOs PA BO
\va bumîî
\dialx BO
\sn 1
\is arbre
\ps n
\ge banian
\sc Ficus obliqua
\scf Moracées
\sn 2
\is arbre
\ps n
\ge balassor ; arbre à tapa
\sc Broussonetia
\scf Moracées
\dn les nœuds dans les bandes de tapa permettaient de transmettre des messages d'un groupe à un autre; les bagayous étaient faits en tapa
\cf bè
\ce banian
\dt 19/Feb/2025

\lx bumira
\dialx PA BO
\is corps
\ps n
\ge péritoine
\dt 24/Jan/2019

\lx bu-mwa
\dialx GOs BO
\is maison
\ps n
\ge tertre ; emplacement d'une maison disparue
\xv bwa bu-mwa
\xn à l'emplacement de la maison
\cf kêê-mwa
\ce emplacement de la case ; terrassement
\dt 22/Feb/2025

\lx bunu
\dialx GOs
\is terrain_terre
\ps n
\ge terre rouge ; latérite
\dt 08/Mar/2019

\lx bunya
\dialx GOs
\is nourriture
\ps n
\ge bounia
\dn préparation culinaire
\dt 25/May/2024

\lx buò
\dialx GOs WEM PA BO
\is arbre
\ps n
\ge bois "pétrole"
\sc Fagræa schlechteri
\scf Gentianacées
\dt 20/Mar/2023

\lx burali
\dialx BO
\is son
\ps v
\ge détonner avec éclat ; tomber et se briser avec bruit
\nt selon Corne
\dt 26/Mar/2022

\lx burei
\dialx GO PA
\is ustensile
\ps n
\ge bouteille
\bw bouteille (FR)
\dt 12/Nov/2021

\lx burò
\hm 1
\dialx GOs
\ph buɽɔ
\va buròn
\ph burɔn
\dialx BO PA WEM
\va bwòn
\ph bwɔn
\dialx BO
\sn 1
\is lumière
\ps v.stat. ; n
\ge sombre ; obscur ; noir
\xv e burò ni mee-je
\xn il est évanoui (lit. ses yeux sont dans l'obscurité)
\sn 2
\is lumière
\ps n
\ge obscurité
\dt 03/Feb/2025

\lx burò
\hm 2
\dialx GOs
\is poisson
\ps n
\ge "planqueur"
\sc Lethrinus sp.
\scf Lethrinidés
\dt 27/Aug/2021

\lx butrö
\ph buɽω
\dialx GOs
\is poisson
\ps n
\ge poisson "baleinier"
\sc Sillago ciliata
\scf Sillaginidés
\dt 27/Aug/2021

\lx butrõ
\ph buɽõ
\dialx GOs
\va burõ
\dialx GO(s)
\va buròm
\dialx WEM PA BO
\is soin
\ps v
\ge baigner (se) ; laver (se)
\xv e pe-burõ
\dialx GO
\xn il se baigne
\xv e pe-buròm (e) Kaavo
\dialx PA
\xn Kaavo se baigne
\xv e burõ Kaavwo
\dialx GO
\xn Kaavo se baigne
\ng causatif: |lx{pa-burõ-ni}
\gt baigner qqn.; donner un bain
\cf chavwoo, jamwe
\dialx GOs
\ce se laver
\dt 22/Feb/2025

\lx buu
\hm 1
\dialx GOs BO PA
\is corps
\ps n
\ge épaule
\xv buu-je
\dialx GO
\xn son épaule
\xv buu-n
\dialx PA
\xn son épaule
\xv bu hii-n
\xn le haut du bras
\dt 24/Jan/2019

\lx buu
\hm 2
\dialx PA
\is plantes
\ps n
\ge croton
\dn cette plante protège les maisons et les êtres humains
\sc Codiæum variegatum
\scf Euphorbiacées
\dt 08/Feb/2025

\lx buubu
\dialx GO PA
\is santé
\ps n
\ge tache décolorée (sur peau)
\dt 27/Oct/2021

\lx buu-dili
\dialx GOs WEM WE
\is terrain_terre
\ps n
\ge tas de terre ; monticule de terre
\dt 28/Jan/2019

\lx buuni
\ph bu:ɳi
\dialx GOs
\va buuni
\dialx BO
\is cultures
\ps v
\ge butter (autour des tubercules)
\dt 22/Aug/2021

\lx buvaa
\dialx GOs BO PA WEM
\va bupaa
\dialx GO arch.
\is arbre
\ps n
\ge oranger sauvage ; faux oranger
\sc Citrus macroptera
\scf Rutacées
\dn un jeu de balle pratiqué dans le pays des morts avec le fruit de l'oranger sauvage, ce jeu permet de distinguer l'esprit d'un vivant de celui d'un mort
\se pu-buvaa
\sge pied d'oranger sauvage
\dt 19/Feb/2025

\lx buzo-kò
\dialx GOs
\is corps
\va bulo-kò
\dialx PA BO
\ps n
\ge mollet
\xv bulo-kòò-ny
\dialx PA
\xn mon mollet
\dt 23/Jan/2018

\lx bwa
\dialx GOs BO
\sn 1
\is corps
\ps n
\ge tête
\xv bwaa-nu
\dialx GO
\xn ma tête
\xv bwaa-n
\dialx BO PA
\xn sa tête
\xv bwa(a) chòvwa
\xn la tête du cheval
\sn 2
\is grammaire_préposition_locative
\ps LOC
\ge dessus ; au-dessus de ; sur
\xv bwa cee
\xn sur l'arbre
\xv a bwa wamwa !
\dialx GO
\xn va au champ !
\xv nu a-mi na bwa wamwa
\dialx GO
\xn je reviens du champ
\se na bwa
\dialx BO
\sge au-dessus de
\et *bwatu
\el POc
\dt 08/Nov/2021

\lx bwa-
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge CLF desbottes d'herbes et des paquets de feuilles
\se bwa-xè, bwa-tru, bwa-kò, bwa-pa, bwa-ni
\sge une botte, deux, trois, quatre, cinq, etc.
\xv bwa-xè bwalò-pho
\xn une botte de feuilles de pandanus
\xv bwa-xè mãe
\xn une botte d'Imperata cylindrica
\dt 22/Feb/2025

\lx bwa dree
\dialx GOs PA BO
\va bwa dèèn
\dialx PA BO
\is navigation
\ps LOC
\ge au vent ; vent debout
\dt 20/Mar/2023

\lx bwa gu-hi
\dialx PA
\va bwa gu-i
\dialx WEM
\va bwa mhwãã
\dialx GO
\is grammaire_locatif
\ps LOC
\ge droite
\dt 20/Mar/2023

\lx bwa mè
\dialx GOs BO
\is grammaire_préposition_locative
\ps LOC
\ge face à ; présence (en) de
\xv nu kò bwa mèè-we
\dialx GO
\xn je suis face à vous
\dt 14/Oct/2021

\lx bwa mõ
\dialx GOs
\va bwa mol
\dialx BO
\is navigation
\is topographie
\ps LOC
\ge à terre
\dn (lit. sec)
\xv li cö-da bwa mõ
\xn ils remontent à terre
\dt 08/Feb/2025

\lx bwa mhõ
\ph bwa mʰɔ̃
\dialx GOs WE PA
\va mò
\ph mɔ̃
\dialx BO
\is grammaire_locatif
\ps LOC
\ge gauche
\se alabo bwa mhõ
\dialx GO WEM PA
\sge côté gauche
\dt 27/Feb/2023

\lx bwa mhwãã
\dialx GOs
\ph bwa mʰwɛ̃:
\is grammaire_locatif
\ps LOC
\ge droite
\se hii-je bwa mhwãã
\dialx GO
\sge sa main droite
\an bwa mhõ
\at à gauche
\dt 20/Mar/2023

\lx bwaa
\hm 1
\dialx GOs PA BO
\va bwaamwa
\dialx GOs PA BO
\is grammaire_interpellation
\ps vocatif ; adresse honorifique
\ge ô vous !
\xv bwaa ! me phweexu chãnã
\dialx GO
\xn ô vous ! nous bavardons sans fin
\dt 14/May/2024

\lx bwaa
\hm 2
\dialx GOs PA
\is reptile_marin
\ps n
\ge serpent de mer (gris)
\cf bwaô
\dialx PA
\ce serpent "tricot rayé"
\dt 10/Jan/2022

\lx bwaaçu
\dialx GOs
\va bwaayu
\dialx WEM WE BO PA
\is caractéristiques_personnes
\ps v.stat
\ge courageux ; travailleur
\se e a-bwaayu
\sge il est travailleur
\cf bemãbe
\dialx GOs
\ce courageux ; travailleur
\dt 22/Feb/2025

\lx bwaado
\dialx GOs PA BO
\is oiseau
\ps n
\ge Martin-pêcheur
\sc Halcyon Sanctus canacorum
\scf Alcédinidés
\gb Sacred Kingfisher
\dt 22/Feb/2025

\lx bwaadu
\dialx BO [BM]
\is sentiments
\ps v
\ge dégoûté
\xv nu u bwaadu
\dialx BO
\xn je suis dégoûté
\dt 27/Jan/2019

\lx bwaare
\dialx WEM WE
\va bware
\dialx PA BO
\is santé
\ps v ; n
\ge épuisement
\ge épuisé ; fatigué
\xv nu bwaare wa nyamã
\xn je suis épuisé à cause du travail
\se pa-bwaare
\sge fatiguer ; épuiser qqn.
\cf mora
\ce épuisé ; éreinté
\dt 22/Feb/2025

\lx bwaa-xe
\dialx PA
\is classificateur_numérique
\ps CLF.NUM
\ge un fagot ; un paquet de feuilles, de paille
\dt 21/Aug/2021

\lx bwabòzö
\dialx GOs
\is cultures
\ps n
\ge mur de soutènement d'une cuvette
\dn destinée à laver l'igname |lx{dimwa} pour la rendre comestible
\dt 05/Jan/2022

\lx bwabu
\dialx GOs PA BO
\is grammaire_locatif
\ps LOC
\ge au-dessous ; par terre ; en bas
\xv trabwa bwabu
\dialx GO
\xn assieds-toi par terre
\xv i ã-du bwabu (Frâs)
\dialx PA
\xn elle va en France
\dt 28/Jan/2019

\lx bwaçu
\dialx GOs
\va bwaju
\dialx WEM WE
\va bwayu
\dialx PA
\sn 1
\is nourriture
\ps v
\ge manger (terme respectueux)
\dn est employé pour s'adresser ou parler d'un chef)
\cf hovwo
\ce manger (général)
\sn 2
\is coutumes
\ps v
\ge festoyer
\dt 22/Feb/2025

\lx bwadraa
\ph bwaɖa:
\dialx GOs
\va bwadaa
\dialx PA BO
\is topographie
\ps n
\ge plaine cultivable
\ge plateau ; ouverture de la vallée
\dt 16/Aug/2021

\lx bwadreo
\dialx GOs
\ph bwaɖeo
\va bwadeo
\dialx BO (Corne)
\is habillement
\ps n
\ge turban
\dt 25/Jan/2019

\lx bwaè
\dialx GOs
\va bwaalek
\dialx PA
\is oiseau
\ps n
\ge buse de mer
\sc Pandion haliætus melvillensis
\scf Accipitridés
\dt 27/Aug/2021

\lx bwange
\hm 1
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge dévier (eau)
\dt 29/Jan/2019

\lx bwange
\hm 2
\dialx GOs
\is déplacement
\ps v
\ge tourner ; revenir ; retourner (s'en)
\xv bwange-cö !
\xn retourne-toi !
\cf pwaa
\dialx PA
\dt 10/Jan/2022

\lx bwagili
\dialx GOs PA WEM
\va bwagil
\dialx BO
\is corps
\ps n
\ge genou
\se du-bwagili
\dialx GO
\sge os du genou
\se thi-bwagil
\dialx BO
\sge être à genoux
\xv bwagili(i)-m
\dialx PA
\xn ton genou
\xv bwagili-je
\dialx GO
\xn son genou
\et *bwaqu
\eg genou
\el reflet de PSO
\et *bwa-turu 
\eg tête-genou
\el POc *tunu 'genou', est reflété dans les langues du S Vanuatu
\dt 22/Feb/2025

\lx bwagiloo
\dialx GOs PA
\sn 1
\is action_corps
\ps v
\ge courber (se)
\ge baisser la tête
\sn 2
\is religion
\ps v
\ge prosterner (se)
\ge incliner la tête
\cf kiluu
\dialx GOs BO
\ce courber (se)
\dt 10/Jan/2022

\lx bwaida
\dialx GOs
\is mollusque
\ps n
\ge palourde
\sc Anadara antiquata
\scf Arcidés
\dt 27/Aug/2021

\lx bwa-kaça
\ph bwaɣaca, bwa-ɣaʒa
\dialx GOs
\va bwa-xaça
\dialx GO(s)
\is configuration
\ps n
\ge dessus ; dos (de la main, du pied)
\xv bwa-xaça hii-je
\xn le dessus de sa main
\xv bwa-xaça kò-je
\xn le dessus de son pied
\xv bwa-xaça za
\xn le dos de l'assiette
\an kaça
\at arrière
\dt 22/Feb/2025

\lx bwa-kaça hi
\ph bwaɣacʰa bwaɣaʒa
\dialx GOs
\va bwa-xaça
\dialx GO(s)
\is corps
\ps n
\ge dessus ; dos (de la main)
\xv bwa-xaça hii-je
\xn le dessus de sa main
\xv bwa-xaça kò-je
\xn le dessus de son pied
\dt 22/Feb/2025

\lx bwa-kazi-
\dialx GO
\va kaji-n
\dialx BO (Corne)
\is corps
\ps n
\ge pubis ; partie antérieure des os de la hanche
\xv kaji-n
\xn son pubis (Dubois)
\dt 31/Jan/2019

\lx bwakitra-hi
\ph bwakiɽahi, bwaɣiɽahi
\dialx GOs
\va bwagira-hi
\dialx GO(s) BO
\is corps
\ps n
\ge coude
\xv bwagira hi-n
\dialx BO
\xn son coude
\cf böji hi
\dialx GO
\ce poignet (lit. articulation de la main)
\dt 17/May/2024

\lx bwa-kitra-me
\ph bwaɣiɽame
\dialx GOs
\va bwa-kira-me, bwa-gira-mè
\dialx GO(s) PA BO
\va bwagila-me
\dialx BO
\is corps
\ps n
\ge arcade sourcilière
\xv bwakira mee-nu
\dialx GO
\xn mon arcade sourcilière
\xv bwagila me-ny
\dialx BO
\xn mon arcade sourcilière
\se pu-bwa-kira-me
\sge sourcils
\cf me
\ce œil
\dt 02/Jan/2022

\lx bwala
\dialx BO PA
\is cultures_champ
\ps n
\ge tarodière irriguée en terrasse
\dn la tarodière |lx{bwala} est de taille supérieure à la tarodière |lx{penu} ; les pieds de taros sont dans l'eau et non plantés sur un billon |lx{aru}, comme c'est le cas pour la tarodière sèche |lx{penu}
\se kêê-bwala
\sge tarodière irriguée
\dt 05/Jan/2022

\lx bwalò
\dialx GOs PA BO
\va pwalò
\dialx GO(s)
\wr A
\is configuration
\ps n
\ge fagot (bois, canne à sucre)
\ge tas (feuilles, coco)
\se bwalò-cee
\dialx GO PA
\sge fagot de bois
\se bwalòò-paa
\dialx GO
\sge tas de pierres
\se bwalò-ê
\dialx GO
\sge fagot de canne à sucre
\se bwalò-pò-cee
\dialx PA
\sge tas de fruits
\se bwalò-mãe
\dialx PA
\sge fagot de paille
\wr B
\is classificateur_numérique
\ps CLF.NUM
\ge tas de
\xv bwalò-xè, bwalò-tru, bwalò-kò
\dialx GOs
\xn un, deux, trois tas
\dt 08/Nov/2021

\lx bwa-mwa
\dialx GOs PA
\is maison
\ps n
\ge toit
\dt 25/Aug/2021

\lx bwana
\dialx GO
\is igname
\ps n
\ge côté mâle du massif d'ignames
\nt selon Haudricourt ; mot inconnu des locuteurs actuels
\dt 27/Mar/2022

\lx bwanamwa
\ph 'bwaɳamwa
\dialx GOs
\va bwaamwa
\dialx GO(s) PA WEM
\va bwaa
\dialx PA
\is discours_interjection
\ps INTJ (pitié, affection)
\ge hélas ; pauvre ! ; cher !
\xv bwaa dòny !
\dialx PA
\xn pauvre buse !
\xv bwanamwa hoo-î !
\dialx GOs
\xn hélas pour notre repas (perdu)
\xv bwanamwa ! azoo-nu !
\dialx GOs
\xn que ce serait bien ! il pourrait devenir mon époux
\cf gaanamwa
\ce (jugé moins correct)
\dt 15/Mar/2023

\lx bwane
\dialx GOs
\va bwea
\dialx WE PA
\va bwani
\dialx BO
\is maison_objet
\ps n
\ge oreiller ; appuie-tête
\xv bwani-ny
\dialx BO
\xn mon oreiller
\dt 26/Jan/2019

\lx bwaô
\dialx GOs
\is poisson
\ps n
\ge carangue noire
\dn de très grosse taille
\sc Caranx lugubris ou Caranx ignobilis
\scf Carangidées
\cf kûxû (juvénile), dròò-kibö (moyen), putrakou (plus gros), bwaô (adulte)
\ce les quatre noms de la même carangue à des tailles différentes
\dt 08/Feb/2025

\lx bwaole
\dialx GOs PA BO
\is oiseau
\ps n
\ge aigle pêcheur ; balbuzard pêcheur ; aigle siffleur
\sc Haliastur sphenurus
\scf Accipitridés
\gb Whistling Eagle-Kite
\dt 27/Aug/2021

\lx bwaòle
\ph bwaɔle
\dialx PA BO WEM
\sn 1
\is mouvement
\ps v
\ge rouler
\ge tourner une roue
\cf bwarao
\dialx GOs
\ce rouler
\sn 2
\is instrument
\ps n
\ge roue
\dt 10/Jan/2022

\lx bwarabo
\dialx BO
\is caractéristiques_objets
\ps v.stat
\ge rond
\nt selon BM
\dt 03/Feb/2025

\lx bwarao
\ph bwaɽao
\dialx GOs
\va bwaòl
\dialx WEM
\is mouvement
\ps v
\ge rouler
\ge faire des galipettes (sur le côté)
\dt 25/Aug/2021

\lx bwarele
\ph bwaɽele
\dialx GOs PA WEM BO
\va bwatrele
\dialx GO(s)
\ph bwaɽele
\is oiseau
\ps n
\ge "collier blanc" ; pigeon à gorge blanche
\sc Columba vitiensis hypœnochroa
\scf Columbidés
\gb White-throated Pigeon
\dt 27/Aug/2021

\lx bwaroe
\ph bwaɽoe
\dialx GOs BO
\is portage
\ps v
\ge porter un enfant dans les bras (contre la poitrine)
\dt 25/Jan/2019

\lx bwatra
\dialx GOs
\ph bwaɽa
\va bwara
\dialx GO(s)
\is poisson
\ps n
\ge "crocro" ; perche argentée
\sc Pomadasys argenteus
\scf Hémulidés
\dt 19/Feb/2025

\lx bwatratra
\dialx GOs
\ph bwaɽaɽa
\is instrument
\ps n
\ge maillet à poisson
\dt 27/Jan/2019

\lx bwatrû
\ph bwaʈũ bwaɽũ
\dialx GOs
\va bwarû
\dialx PA
\va bwarong, bwarô
\dialx BO
\is oiseau
\ps n
\ge nid (oiseau)
\xv bwatrû meni
\xn nid d'oiseau
\xv bwarô mèèni (ou) bwaarong mèni
\dialx BO
\xn nid d'oiseau
\dt 25/Aug/2021

\lx bwaû-bwara
\dialx GOs
\is poisson
\ps n
\ge loche castex
\sc Diagramma pictum
\scf Hémulidés
\dt 27/Aug/2021

\lx bwaû-wããdri
\dialx GOs
\is poisson
\ps n
\ge castex
\sc Plectorhinchus picus
\scf Hémulidés
\dt 27/Aug/2021

\lx bwavwa
\dialx GOs BO
\va bwapa
\dialx GO(s)
\is mollusque
\ps n
\ge bénitier géant (gastéropode)
\sc Tridacna derasa
\scf Tridacnidés
\dt 27/Aug/2021

\lx bwavwaida
\dialx GOs
\va bwaivwada
\dialx GO(s) BO
\va bwapaida
\dialx arch.
\is oiseau
\ps n
\ge oiseau de proie ; aigle
\dt 24/Dec/2021

\lx bwavwòlo
\dialx GOs
\va bweevwòlo
\dialx PA BO
\is cultures
\ps n
\ge barrage sur une rivière 
\dn où les femmes lavent le |lx{dimwa}
\ge barrage (pour l'irrigation)
\xv li thòni jè bweevwòlo
\dialx PA
\xn elles construisent ce barrage
\xv li thi-da la bi ni jè-ò bweevwòlo-li
\dialx PA
\xn elles plantent un tuyau dans leur barrage
\dt 08/Feb/2025

\lx bwavwu-we
\dialx GOs BO PA
\va bwevwu-we
\dialx GOs
\va pwe-we
\dialx PA
\is cultures
\ps n
\ge source
\ge barrage ; vanne de canal de tarodière
\dt 27/Mar/2022

\lx bwawe
\dialx GOs
\is action_corps
\ps v
\ge démêler
\xv e bwawe-zoo-ni
\xn il les démêle (tuyau, corde)
\dt 23/Jan/2018

\lx bwaxala
\dialx BO
\is navigation
\ps n
\ge pirogue
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx bwaxe
\dialx PA BO
\is santé
\ps v
\ge fatigué ; faible
\xv nu pò bwaxe
\xn je suis un peu fatigué
\dt 23/Jan/2022

\lx bwaxeni
\dialx GOs WEM BO
\is maison
\ps n
\ge tertre
\xv bwaxeni mwa
\xn tertre de la maison
\dt 26/Jan/2019

\lx bwaxixi
\dialx GOs
\va bwaxii
\is fonctions_naturelles
\ps v
\ge regarder (se) (dans un miroir)
\cf zido
\dialx GOs
\ce se mirer
\dt 10/Jan/2022

\lx bwaxuli
\dialx WEM WE
\is instrument
\ps n
\ge chaîne
\dt 27/Jan/2019

\lx bwe
\dialx BO
\va bwee
\dialx BO
\is plantes_partie
\ps n
\ge tronc ; branche ; bout
\xv bwee-pòò
\dialx GO
\xn un tronc de bourao
\xv bwe laloe
\dialx BO
\xn tige d'aloès
\xv bwe cee
\dialx BO
\xn bâton ; bout de bois
\dt 22/Feb/2025

\lx bwe-
\dialx GOs BO PA
\sn 1
\is configuration
\ps n (composition)
\ge tête ; sommet
\se bwe-kui
\sge tête d'igname
\se bwèè-mwã
\dialx PA
\sge sommet de la maison
\sn 2
\is topographie
\ps n (composition)
\ge sommet de ; dessus de
\dt 23/Jan/2022

\lx bwèdò
\ph mbwɛndɔ
\dialx GOs PA
\is corps_doigt
\ps n
\ge doigt
\xv bwèdò-hii-je
\dialx GO
\xn son doigt
\xv bwèdò kòò-n
\xn orteils
\dt 12/Jan/2022

\lx bwèdo-hi
\dialx GOs PA BO
\is corps_doigt
\ps n
\ge doigt
\dt 12/Mar/2023

\lx bwèdò-hi êmwèn
\dialx PA
\is corps_doigt
\ps n
\ge index
\dt 12/Mar/2023

\lx bwèdo-hi thoomwã
\dialx GOs PA
\is corps_doigt
\va bwèdo-hi thooma
\dialx BO
\ps n
\ge pouce
\dt 12/Mar/2023

\lx bwèdò-kò
\dialx GOs PA BO
\va bwèdò-xò
\dialx GO(s) PA BO
\is corps_doigt
\ps n
\ge orteil
\se bwèdò-kò thoomwã
\dialx PA
\sge son gros orteil
\dt 08/Jan/2022

\lx bwèèdrò
\ph mbwɛ:nɖɔ
\dialx GOs
\va bwèèdò
\dialx PA BO
\va bwèdòl
\dialx BO
\is corps
\ps n
\ge front
\xv bwèèdrò-nu
\dialx GO
\xn mon front
\xv bwèdòò-n
\dialx PA
\xn son front
\dt 06/Feb/2019

\lx bwèèdrö
\ph mbwɛ:nɖω
\dialx GOs
\va bwèèdo
\dialx PA BO
\is terrain_terre
\ps n
\ge terre ; pays ; sol ; Terre
\xv na bwèèdrö
\xn par terre ; au sol
\dt 22/Feb/2025

\lx bweena
\dialx GOs PA BO
\is reptile
\ps n
\ge lézard
\dt 29/Jan/2019

\lx bwèèra
\ph bwè:ɽa
\dialx GOs PA
\va bwèèrao
\dialx BO (Corne)
\is feu
\ps n
\ge pierres de support
\dn souvent au nombre de trois servant de support à marmite
\ge chenets ; foyer ; rails du feu
\se bwèèra paa
\sge les pierres servant de support aux marmites
\dt 17/Feb/2025

\lx bweeravac
\dialx PA BO
\is vent
\ps n
\ge vent soufflant du sud au nord
\dt 28/Jan/2019

\lx bweetroe
\ph bwe:ɽoe
\dialx GOs
\va bweeroe
\dialx GO(s)
\is poisson
\ps n
\ge loche (en général)
\sc Epinephelus sp.
\scf Serranidés
\cf poxa-zaaja
\ce loche (de grande taille)
\dt 27/Aug/2021

\lx bwèèvaça kò
\ph bwe:vadʒa
\dialx GOs
\va bwèèvao kò-ã
\dialx PA
\va bwèèva kò
\dialx BO WEM
\is corps
\ps n
\ge talon
\xv bwèèva kòò-n
\dialx BO
\xn son talon
\dt 24/Jan/2019

\lx bwèèxò
\dialx GOs PA WEM BO
\is déplacement
\ps n
\ge traces ; empreintes 
\dn d'hommes ou d'animaux
\xv bwèèxòò-n
\dialx PA
\xn ses traces
\xv bwèèxò loto
\xn les traces de voiture
\dt 21/Feb/2025

\lx bweeye
\dialx PA
\is temps_saison
\ps n
\ge saison chaude et sèche
\dt 15/Aug/2021

\lx bwe-hoogo
\dialx GOs PA
\va bwe-hogo
\dialx BO
\is topographie
\ps n
\ge sommet de la montagne
\ge crête de la montagne
\dt 27/Mar/2022

\lx bwehulo
\dialx GOs
\va bwexulo
\dialx GOs
\is corps
\ps v.stat
\ge amputé ; manquer
\xv bwehulo hii-je
\dialx GO
\xn son bras est (partiellement) amputé
\xv bwehulo kòò-je
\dialx GO
\xn sa jambe est amputée
\xv bwehulo bwedòò hii-je
\dialx GO
\xn il lui manque un doigt
\cf paxu
\ce manquer
\dt 08/Feb/2025

\lx bwe-kui
\dialx GOs PA
\is igname
\is cultures
\ps n
\ge tête de l'igname
\dn replantée
\ge bouture d'igname
\an thô-kui
\dialx GOs
\at extrémité inférieure de l'igname
\dt 08/Feb/2025

\lx bwè-mwa
\dialx GOs PA BO
\is maison
\ps n
\ge paille (dernière rangée de paille sur le toit)
\dn elle forme un bourrelet qui ferme le faîtage
\cf phu
\ce paille (première rangée de paille sur le toit)
\dt 25/Aug/2021

\lx bwe-nõ
\dialx GOs
\is santé
\ps n
\ge torticolis (avoir le)
\xv e tròò-nu xo bwe-nõ
\xn j'ai le torticolis (lit. le torticolis m'a trouvé/affecté)
\dt 20/Mar/2023

\lx bweo
\dialx PA BO
\is vent
\ps n
\ge vent d'ouest
\dt 28/Jan/2019

\lx bwe-pai
\dialx GOs PA
\va bwe-vai
\dialx GO(s) PA
\is fonctions_intellectuelles
\ps n
\ge résultat ; conséquence (bénéfique)
\dt 08/Sep/2021

\lx bwè-po
\dialx GO
\is armes
\va bwepon
\dialx BO PA
\ps n
\ge casse-tête à bout en bec d'oiseau
\dt 23/Jan/2018

\lx bwe-phwamwa
\dialx GOs
\is cultures
\ps n
\ge extrémité du champ
\dt 05/Jan/2023

\lx bwevwu
\dialx GOs PA BO
\va bweevwu
\sn 1
\is plantes_partie
\ps n
\ge tronc ; pied ; base ; bout de
\se bwevwu ci-pai
\dialx GO
\sge pied d'arbre à pain
\se bweevwu nu
\sge au pied du/sous le cocotier
\se bwevwu gò
\dialx BO
\sge au pied du bambou [BM]
\sn 2
\is temps_découpage
\ps n
\ge origine
\xv bwevwuu-n
\xn son origine
\mr |lx{bwe(e)vwu} vient de |lx{bwa-puu-n} "à la base"
\dt 20/Feb/2025

\lx bwevwu-cee
\dialx GOs
\is arbre
\is plantes_partie
\ps n
\ge souche ; pied ; base de l'arbre
\dt 24/Jan/2022

\lx bwi
\dialx GOs PA BO
\is santé
\ps v.stat
\ge aveugle
\ge conjonctivite
\xv bwi mèè-n
\dialx BO
\xn il est aveugle (lit. ses yeux sont aveugles)
\dt 03/Feb/2025

\lx bwihin
\dialx BO
\va bwiin
\dialx PA
\is igname
\ps n
\ge igname (violette)
\dt 17/Feb/2025

\lx bwili
\dialx GOs
\va dixoo-bwa-n
\dialx PA
\is corps_animaux
\ps n
\ge corne
\se bwili dube
\sge cornes du cerf
\se bwili kava
\sge corne du dawa (poisson)
\et *mpuji
\el POc
\dt 23/Jan/2022

\lx bwi-nu
\dialx BO
\is ustensile
\ps n
\ge calebasse ; noix de coco vide
\et *bwilo
\eg coconut shell used as liquid container
\el POc
\ea Blust
\dt 26/Jan/2019

\lx bwiri
\ph bwiɽi
\dialx GOs
\va bwirik
\dialx PA BO
\is cordes
\ps n
\ge bride
\se pò-bwiri
\dialx GO
\va pwò-bwirik
\dialx BO
\sge mors
\se khô-bwiri
\dialx GO
\va khô-bwirik
\dialx BO
\sge rênes
\dt 13/Jan/2022

\lx bwixuu
\dialx GOs
\va bwixu
\dialx PA
\va bwivu
\dialx BO (Corne)
\is mammifères
\ps n
\ge chauve-souris (petite)
\dt 29/Jan/2019

\lx bwo
\dialx GOs BO
\va bo
\dialx PA BO
\is nourriture
\is caractéristiques_objets
\ps v
\ge pourri (être) ; sentir
\dt 01/Feb/2019

\lx bwò
\hm 1
\ph bwɔ
\dialx GOs PA BO
\va bò, bo
\dialx BO
\is mammifères
\ps n
\ge roussette
\sc Pteropus sp.
\scf Pteropodidés
\gb fruit-bat
\et *mpeka
\el POc
\dt 27/Aug/2021

\lx bwò
\hm 2
\dialx GOs
\is poisson
\ps n
\ge poisson-papillon ; poisson-lune
\sc Platax teira
\scf Ephippidés
\cf thrimavwo
\ce picot de palétuvier ; poisson-papillon
\dt 27/Aug/2021

\lx bwò
\hm 3
\dialx GOs
\va bò
\dialx GO(s)
\va bwòn
\dialx PA BO
\is temps
\ps n
\ge jour fixé ; date convenue
\xv poniza bò mhwããnu ?
\dialx GO
\xn quelle est la date ?
\xv bwò u-da ni chòmu
\dialx GO
\xn le jour de la rentrée scolaire
\xv bò phããde kui
\dialx GO
\xn la date pour montrer l'igname
\xv bò khî kui
\dialx GO
\xn la date pour griller l'igname
\xv bòni-ã mõnõ
\dialx GO
\xn le moment que nous nous sommes fixés (notre date) demain
\se bò thi-cee, bò thi-yee
\dialx PA
\sge jour fixé pour une fête (on plante un bois pour fixer la date)
\et *mpoŋi
\eg night
\el POc
\dt 28/May/2024

\lx bwòivhe
\dialx GOs BO
\va boive
\dialx PA
\is insecte
\ps n
\ge papillon
\dt 08/Feb/2025

\lx bwò-kabu
\dialx GOs
\va bò-kabun
\dialx BO PA
\is temps_jours
\ps n
\ge dimanche (lit. jour sacré)
\dt 08/Oct/2021

\lx bwòò
\dialx GOs
\va bool
\dialx WE
\is jeu
\ps n
\ge ballon ; balle
\bw ball (GB)
\dt 15/Aug/2021

\lx bwoom
\ph bwɔ:m
\dialx PA BO
\sn 1
\is caractéristiques_personnes
\ps v.stat
\ge calme ; paisible ; humble (personne)
\sn 2
\is caractéristiques_objets
\ps n
\ge ombre ; endroit ombragé
\nt selon BM, Corne
\dt 03/Feb/2025

\lx bwòvwô
\dialx GOs BO
\va bòòvwô, bòpô
\dialx GO(s)
\is santé
\ps v
\ge fatigué (être)
\ge épuisé
\xv kavwa e bwòvwô pune la mõgu i je dròrò ?
\xn n'est-il pas fatigué de son travail hier ?
\xv Ôô ! e bwòvwô !
\xn Oui ! il en est fatigué !
\dt 23/Jan/2022

\lx ca
\hm 1
\ph ca
\dialx GOs BO
\is caractéristiques_objets
\ps v
\ge coupant ; bien aiguisé
\xv u ca hèlè
\xn le couteau est coupant
\cf yazoo
\ce affûter
\dt 26/Jan/2019

\lx ca
\ph ca
\hm 2
\dialx GOs PA BO
\sn 1
\is chasse
\ps v
\ge toucher (une cible à la sagaie)
\xv e a-pha-ca !
\dialx GO
\xn il est adroit (ne rate pas la cible)
\xv u ca !
\dialx GO PA
\xn touché ! dans le mille !
\sn 2
\is action
\ps v
\ge avoir lieu (pour un événement fixé)
\xv u ca !
\dialx GO PA
\xn ça s'est réalisé ! ; c'est le jour "j" ; c'est le jour fixé
\dt 22/Feb/2025

\lx ca
\hm 3
\ph ca
\dialx GOs WEM
\va ya, yaa, yai
\dialx PA
\sn 1
\is grammaire_préposition
\ps PREP
\ge vers
\xv uvwa ca ênêdu
\dialx WEM
\xn ça sort vers le bas
\xv kavwö jaxa-cö vwö cö zòò ça bwa drau
\dialx GO
\xn tu ne pourras pas nager jusqu'à l'île
\sn 2
\is grammaire_préposition
\ps PREP
\ge à ; pour
\dn (destinataire)
\xv xa e wãã mwa cai (i)je xo ã kani
\dialx GO
\xn et le canard lui répond
\xv i khõbwe ya i je
\dialx PA
\xn il lui dit
\xv u kõbwe wo Thomaxim ya kòlò Thonòòl
\dialx PA
\xn Thomaxim dit à Thonòòl
\xv i kôbwe yaa i nu
\dialx BO
\xn il me dit
\xv yu kôbwe da yaani daalèn ?
\dialx BO
\xn qu'as-tu dit aux Européens ?
\xv nu kôbwe ya-i ije nye (ou) nu kôbwe nye ya-i ije
\dialx BO
\xn je lui ai dit cela
\xv nu uvi yaa hi-n
\dialx BO
\xn je le lui ai acheté
\ng |lx{ca-i} est suivi d'un nom singulier ; |lx{ca-ni} est suivi d'un nom pluriel
\dt 08/Feb/2025

\lx ça
\ph ʒa
\dialx GOs PA
\va ka
\dialx GO(s)
\va ca, ce, je, ye
\dialx BO PA
\is grammaire_IS
\ps THEM ; SEQ
\ge thématisation
\xv yaza aponoo-va ça wêwêne
\dialx GO
\xn le nom de chez nous, c'est Wêwêne
\xv novwo hãgana ca mi a-du mwa paawa
\dialx PA
\xn mais aujourd'hui, nous allons désherber
\cf ka
\ce et
\dt 06/Aug/2023

\lx ça bwa
\ph ʒa bwa
\dialx GOs
\is grammaire_préposition_locative
\ps PREP
\ge jusqu'à ; vers
\xv kavwö jaxa-çö vwo çö zòò ça bwa drau
\dialx GO
\xn tu ne pourras pas nager jusqu'à l'île
\dt 20/Oct/2021

\lx ca èa ?
\ph ca
\dialx GOs
\va ça èa ?
\ph ʒa ɛa
\is grammaire_interrogatif
\ps INT.LOC (dynamique)
\ge vers où ? ; jusqu'où ?
\xv e a ça èa loto-ã ?
\xn jusqu'où va cette voiture ?
\dt 19/Jan/2025

\lx caa-
\dialx GO PA
\va ca-
\dialx BO [Corne, BM]
\is nourriture
\ps n
\ge accompagnement (des féculents)
\xv caa xa da ? – Caa xa lai
\dialx GO
\xn c'est l'accompagnement de/pour quoi ? – C'est l'accompagnement des/pour le riz
\xv caa xa cè-ã
\dialx GO
\xn c'est l'accompagnement pour/de nos féculents
\xv caa xa kui
\dialx GO
\xn c'est l'accompagnement de/pour l'igname
\cf caai
\ce accompagnement (des féculents)
\et *kani
\el POc
\dt 24/Feb/2025

\lx caaça
\ph ca:ʒa ca:dʒa
\dialx GOs
\va caaya, caya
\ph ca:ja
\dialx PA BO
\sn 1
\is parenté
\ps n
\ge père ; oncle paternel
\xv caaça-nu
\dialx GO
\xn mon père
\xv caayè-ny
\dialx PA
\xn mon père
\sn 2
\is parenté_adresse
\ge papa ; tonton
\dt 18/Aug/2021

\lx caai
\hm 1
\dialx GOs PA
\is nourriture
\ps n
\ge condiment (pour accompagner les féculents)
\dn utilisé comme terme général à Gomen
\xv kixa caai
\dialx GO
\xn il n'y a rien comme condiment
\dt 08/Sep/2021

\lx caai
\hm 2
\dialx GOs PA BO
\va caak, caai
\dialx BO
\va samelõ
\dialx GO(s)
\sn 1
\is arbre
\ps n
\ge jamelonier ; jamblon
\sc Syzygium cuminii
\scf Myrtacées
\sn 2
\is arbre
\ps n
\ge pommier canaque ; jambosier rouge
\sc Syzygium malaccense; Eugenia malaccensis
\scf Myrtacées
\sn 3
\is arbre
\ps n
\ge pomme-rose |dialx{PA} ; jambrosade
\sc Syzygium jambos
\scf Myrtacées
\xv pò-cai (ou) pò-caai
\xn "pomme canaque" ; fruit du jamelonier
\et *kapika
\el POc
\dt 22/Feb/2025

\lx caan
\dialx PA
\is poisson
\ps n
\ge carpe (grosse)
\dt 29/Jan/2019

\lx caanô
\dialx PA BO
\is plantes
\ps n
\ge salsepareille ; liane
\sc Smilax purpurata
\scf Smilacacées
\nt selon Corne
\dt 27/Mar/2022

\lx caave
\dialx GOs
\is oiseau
\ps n
\ge frégate (petite)
\sc Fregata ariel ariel
\scf Frégatidés
\dt 27/Aug/2021

\lx caaxai
\dialx BO
\is interaction
\ps v
\ge menacer
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx caaxò
\dialx GOs BO PA
\va kyaaxo
\dialx BO
\va caawo
\dialx BO
\is grammaire_manière
\ps v.stat
\ge cachette (en) ; sans bruit; doux ; par surprise (faire)
\xv i vhaa caaxò
\dialx GO
\xn il parle doucement
\xv nu a kha-caaxò
\dialx GO
\xn j'y suis allé en cachette
\xv thala-çaaxò-ni phwèmwa !
\dialx GO
\xn ouvre la porte sans bruit !
\se mõ-caa(x)ò
\dialx GO
\sge maison des femmes (où les femmes se retiraient lors de la période des règles)
\xv va kyaaxo !
\dialx BO
\xn parlez doucement ! [BM]
\dt 03/Feb/2025

\lx caaxö
\dialx GOs PA
\is discours
\ps v
\ge acquiescer ; répondre
\dt 28/Jan/2019

\lx caaxô
\ph ca:ɣõ
\dialx GOs
\va caxõõl
\dialx PA BO
\va cawhûûl
\dialx BO
\sn 1
\is son
\ps v
\ge grommeler ; gronder ; murmurer (de mécontentement)
\xv e vhaa-caaxô
\dialx GO
\xn il parle entre ses dents
\sn 2
\is discours
\ps v
\ge plaindre (se) constamment
\dt 26/Aug/2021

\lx caaya
\dialx PA
\is parenté_adresse
\ps n
\ge père ; papa
\dt 13/Sep/2021

\lx caay-ò! 
\dialx GO PA
\is parenté_adresse
\is grammaire_vocatif
\ps vocatif
\ge papa !
\se caayo !
\dialx BO (Corne)
\sge papa !
\va caayu !
\dialx BO (Corne)
\ge papa !
\dt 16/Feb/2025

\lx cabeng
\dialx PA BO
\is bananier
\ps n
\ge bananier ; variété de banane-chef
\cf pòdi, puyai
\dt 08/Feb/2025

\lx cabi
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge frapper
\ge heurter
\xv cabi phweemwa !
\dialx PA
\xn tape à la porte !
\sn 2
\is action_corps
\ps v
\ge cogner ; taper ; gifler
\se ba-cabi
\sge marteau
\se pha-cabi
\sge taper pour enfoncer
\sn 3
\is mouvement
\ps v
\ge buter sur qqch.
\sn 4
\is musique
\ps v
\ge battre en rythme ; battre (cloche)
\se ba-cabi kaa
\sge tambour
\et *tapi
\eg frapper avec la main
\el POc
\dt 19/Feb/2025

\lx cabicabi
\dialx BO
\is temps_saison
\ps n
\ge époque où l'on choisit les ignames qu'on va consommer et semer
\dn d'octobre à novembre
\nt selon Dubois
\cf pwebae
\ce époque où les ignames commencent à mûrir
\dt 08/Feb/2025

\lx cabo
\dialx GOs
\va cabwòl, cabòl
\dialx PA BO WEM
\sn 1
\is fonctions_naturelles
\ps v
\ge lever (se)
\ge réveiller (se)
\sn 2
\is mouvement
\ps v
\ge surgir ; monter
\ge apparaître
\ge émerger
\xv e cabo a
\dialx GO
\xn le soleil se lève
\xv i cabòl al
\dialx BO
\xn le soleil se lève
\ng causatif: |lx{pa-cabo-ni, pa-çabo-ni}
\gt révéler qqch.
\sn 3
\is parenté
\ps v
\se a-çabo
\dialx GO
\sge famille maternelle
\se a-yabòl
\dialx PA
\sge neveux côté maternel
\sn 4
\is plantes_processus
\ps v ; n
\ge repousse des feuilles
\ge reverdir
\xv e cabo kîbwoo-cee
\dialx GO
\xn les bourgeons repoussent
\dt 21/Feb/2025

\lx cabo kò
\dialx GOs
\va cabwo kò
\dialx GO(s)
\va cabwò kòl
\dialx BO
\is action_corps
\ps v
\ge mettre debout (se)
\xv cabwo kò !
\xn lève-toi !
\dt 25/Jan/2019

\lx cabòl !
\dialx PA BO
\is maison
\ps INTJ
\ge sortir ; sors ! 
\dn terme utilisé lors de la ligature de la paille et de la couverture du toit; ces appels sont prononcés par la personne qui est sur le toit à l'adresse de celui qui est à l'intérieur de la maison, indiquant à ce dernier qu'il doit piquer l'alène à travers la paille vers le haut et la pousser vers l'extérieur (|lx{cabòl!}); ensuite on crie |lx{a-ò!}, pour récupérer l'alène
\cf a-ò !
\ce pars !
\dt 22/Feb/2025

\lx cabul
\dialx PA BO [BM]
\is action_eau_liquide_fumée
\ps v
\ge déborder
\cf phu
\dialx GOs
\ce déborder
\dt 10/Jan/2022

\lx cabwau
\dialx BO
\is igname
\ps n
\ge igname (violette)
\nt selon Dubois
\dt 17/Feb/2025

\lx caçai
\ph caʒai
\dialx GOs
\va cayaai
\dialx PA
\is nourriture
\ps v
\ge mâcher (en écrasant)
\et *kaRati
\eg mordre, tenir entre les dents
\el POc
\dt 26/Jan/2019

\lx cai
\ph cai
\dialx GOs WEM
\va çai
\ph ʒaɨ
\dialx GO(s)
\va jai, yai
\dialx WEM PA BO
\is grammaire_préposition
\ps PREP (objet indirect)
\ge à ; pour
\dn (destinataire animé)
\xv i khõbwe cai la
\dialx GO
\xn il leur dit
\xv e zaba cai nu
\dialx GO
\xn il m’a répondu
\xv e zixô cai la pòi-je xo õ ẽnõ-ã
\dialx GO
\xn la mère de ces enfants raconte une histoire à ses petits
\xv i khõbwe jai/yai la
\dialx BO
\xn il leur dit
\xv ole jai çö
\dialx WEM
\xn merci à toi
\xv e içu-ni cai Kaawo a-kò pwaji xo õã-nu
\xn ma mère a vendu à Kaawo trois crabes
\xv li pe-pha-nõnõmi cai li la mwêêje ẽgõgò
\dialx GOs
\xn elles se rappellent mutuellement les coutumes d'antan
\xv e khilapuu çai nu
\xn il m'a provoqué
\cf ca
\ce à ; vers ; pour
\dt 22/Feb/2025

\lx caigoo
\ph caiŋgo:
\dialx GOs BO
\is action_tête
\ps v
\ge attraper avec les dents
\ge tenir avec les dents
\ge couper (avec les dents)
\dt 20/Mar/2023

\lx caivwo
\dialx GOs PA BO
\va caipo
\dialx GO arch.
\is mammifères
\ps n
\ge mulot ; souris
\cf ciibwin, zine
\ce rat
\dt 24/Dec/2021

\lx ca-khã
\dialx GOs
\va ca-kã
\is mouvement
\ps v
\ge ricocher
\ge effleurer
\dt 20/Mar/2023

\lx caladraa
\dialx GOs
\is société
\ps n
\ge gendarme
\bw gendarme (FR)
\dt 20/Mar/2023

\lx calaru
\dialx PA
\is insecte
\ps n
\ge sauterelle (marron, petite)
\dt 29/Jan/2019

\lx cale
\dialx GOs BO
\is feu
\ps v
\ge allumer
\dn feu, lampe, cigarette, briquet
\se cale pwaip
\dialx BO
\sge allumer une pipe
\se cale yaai
\dialx GO
\sge allumer le feu
\se cale kîbi
\dialx BO
\sge allumer le four
\dt 08/Feb/2025

\lx calii
\dialx GOs PA BO
\is plantes
\ps n
\ge magnania
\dn petit tubercule sauvage
\sc Pueraria sp.
\scf Fabacées
\dt 08/Feb/2025

\lx calo
\dialx GO
\is corps
\ps n
\ge fontanelle
\nt selon Haudricourt ; non reconnu par les locuteurs actuels
\dt 26/Mar/2022

\lx calò
\dialx BO
\is poisson
\ps n
\ge carangue jaune (à l'âge adulte)
\sc Carangoides gilberti et Gnathanodon speciosus
\scf Carangidées
\nt selon Corne, non vérifié
\dt 26/Mar/2022

\lx ca-ma
\dialx PA BO
\sn 1
\is fonctions_naturelles
\ps n
\ge bave de mort
\dn (lit. nourriture des morts)
\sn 2
\is santé
\ps n
\ge épilepsie
\xv ca-ma-n
\xn bave (épilepsie)
\dt 09/Feb/2025

\lx camadi
\ph camandi
\dialx GOs PA
\is sentiments
\ps v
\ge mauvaise conscience (avoir)
\xv e camadi
\xn il a mauvaise conscience
\se i a-camadi
\dialx PA
\sge prédicateur
\dt 08/Jan/2022

\lx camhãã
\dialx PA BO
\va kyamhãã
\dialx BO [Corne]
\is arbre
\ps n
\ge ficus
\sc Ficus sp.
\sc Ficus scabra Forster
\scf Moracées
\dt 16/Jan/2024

\lx canabwe
\dialx BO
\is taro
\ps n
\ge taro (clone de) de terrain sec
\nt selon Dubois
\dt 27/Mar/2022

\lx cani
\dialx BO
\is nourriture
\ps v
\ge manger (féculents)
\xv i thiò dimwa ma wu i ra un cani
\dialx BO
\xn il gratte son igname pour la manger
\et *kani
\el POc
\dt 26/Jan/2019

\lx cani
\dialx GOs
\va çani
\ph ʒanɨ
\dialx GO(s)
\va yani
\is grammaire_préposition
\ps PREP (objet indirect)
\ge à ; pour
\dn (destinataire animé pluriel)
\xv nu ne hivwine nee cani la-ã pòi-nu
\dialx GO
\xn je n'ai jamais su le raconter à mes enfants (lit. souvent pas su faire)
\xv e naa vhaa mwã cani la êgu i je, e khõbwe cai la khõbwe ...
\dialx GO
\xn il donne des instructions (lit. donne une parole) à ses sujets, il leur dit
\xv e khõbwe cani lhã-ã yabwee i je
\dialx GO
\xn il dit à ses sujets
\xv yu kôbwe da yani daalèn ?
\dialx BO
\xn qu'as-tu dit aux Européens ?
\dt 08/Feb/2025

\lx caro
\dialx BO
\va caaro
\dialx BO
\is danse
\ps n
\ge danse des morts
\dn effectuée par les femmes
\cf citèèn
\ce danse des morts (effectuée par les hommes)
\nt selon Corne ; non vérifié
\dt 08/Feb/2025

\lx carû
\ph caɽû
\dialx GOs
\va carun
\dialx PA BO
\is feu
\ps v
\ge attiser ; pousser le feu
\dn en ajoutant du bois
\xv carûni yaai !
\dialx PA
\xn pousse le feu !
\xv paa carun
\dialx BO
\xn pierres pour le four enterré
\cf tha-carûni
\dialx PA
\ce pousser le feu
\ng v.t. |lx{carûni}
\dt 08/Feb/2025

\lx cauvala
\dialx PA
\is sentiments
\ps v
\ge exprimer son mécontentement
\dn se dit de gens qu'on entend de loin, sans entendre le détail de leurs paroles
\xv la cauvala
\xn ils sont mécontents
\dt 22/Feb/2025

\lx cavwe
\ph caβe
\dialx GOs
\is grammaire_quantificateur_mesure
\ps COLL ; QNT
\ge tous ensemble
\xv la cavwe khai wa
\dialx PA
\xn ils tirent tous ensemble la corde
\xv mõ cavwe a, kixa ne yu !
\dialx GO
\xn partons tous trois ensemble, personne ne reste !
\xv mõ cavwe wa !
\dialx GO
\xn chantons tous ensemble !
\xv la cavwe wa
\dialx GO
\xn ils chantent tous ensemble
\xv lò cavwe pe-be-yaza
\dialx GO
\xn ils (triel) ont tous le même nom
\dt 16/Feb/2025

\lx cawane
\dialx GOs
\va cawan
\dialx BO
\is navigation
\ps n
\ge mât
\dt 16/Jan/2024

\lx caxòò
\dialx GOs
\is mouvement
\ps v
\ge monter aux arbres à quatre pattes
\dn en écartant le corps du tronc
\dt 08/Feb/2025

\lx cayae
\dialx PA
\va ceyai
\dialx BO [BM]
\is nourriture
\ps v
\ge mâcher (en général)
\dt 26/Jan/2019

\lx cazae
\dialx GOs
\is action
\ps v
\ge esquinter ; abîmer
\dt 26/Mar/2022

\lx cè-
\dialx GOs
\va ca(a)-
\dialx PA BO
\is classificateur_possessif_nourriture
\ps n ; CLF.POSS
\ge nourriture de base (féculents)
\xv cèè-nu kui
\dialx GOs
\xn ma part d'igname
\xv cèè-çö lhã-ã
\dialx GOs
\xn cela est ta nourriture
\xv cè-ã kuru
\dialx GO
\xn notre part de taro
\xv caa-ny kui
\dialx PA
\xn ma part d'igname
\xv na-mi ca-ã bwa tap !
\dialx BO
\xn apporte nos féculents sur la table ! (BM)
\cf kûû
\ce manger (fruits)
\et *ka
\el POc
\dt 30/Dec/2021

\lx çe
\dialx GOs
\va je, ça
\dialx GOs
\va ye, ca
\dialx PA
\is grammaire_IS
\ps THEM
\ge thématisation
\xv da yaza-cu ? – Yaza-nu Kaawo  (ou) yaza-nu çe/je Kaawo
\dialx GO
\xn quel est ton nom ? – Je m'appelle Kaawo (ou) mon nom, c'est Kaawo
\dt 24/Feb/2025

\lx ce-baalu
\dialx GOs
\va cibaalu
\dialx GOs
\is instrument_ponts
\ps n
\ge pont en bois ; passerelle ; planche servant de pont
\dn sert à traverser une rivière ou est simplement posée sur la boue pour traverser un terrain détrempé
\dt 31/Jan/2022

\lx cebaèp
\dialx BO
\is vent
\ps n
\ge vent d'est
\dt 28/Jan/2019

\lx cebòn
\dialx BO
\is anguille
\ps n
\ge anguille (variété d')
\nt selon BM
\dt 27/Mar/2022

\lx ce-b(w)ò
\ph cɨ-mbɔ
\dialx GOs
\va ce-bòn
\dialx WEM
\va ce-b(w)òn
\dialx BO
\va cee bòn, ci-bòn
\dialx PA
\is feu
\ps n
\ge bûche de nuit
\dn qui brûle toute la nuit
\dn cette grosse bûche est portée par les hommes
\se ce-bò yaai
\dialx GO
\sge bûche pour le feu de nuit
\se kô-pa-ce-bò
\dialx GO
\sge couché près du feu
\dt 08/Feb/2025

\lx ce-cia
\dialx BO
\is danse
\ps n
\ge poteaux de danse
\nt selon Corne
\dt 26/Mar/2022

\lx cee
\dialx GOs PA BO
\ph cɨ:
\va ce
\dialx PA
\is matière
\is arbre
\ps n
\ge bois
\ge arbre
\se ce-cia
\dialx GO
\sge poteau de danse
\se ce-doo
\dialx PA BO
\sge bois pour la cuisine (lit. bois marmite)
\et *kai
\el POc
\dt 08/Jan/2022

\lx cêê
\dialx GOs
\va cê
\dialx BO
\is corps
\ps n
\ge sexe (femme) ; vulve ; vagin
\se kumèè-cêê
\sge clitoris
\se pu-cêê
\sge poils de pubis
\et *kala
\eg parties génitales
\el POc
\dt 31/Jan/2019

\lx cego
\dialx PA
\is coutumes_objet
\ps n
\ge perches plantées devant les portes des cases
\nt selon Dubois
\dt 26/Mar/2022

\lx ce-he
\ph cɨ he
\dialx GOs PA BO
\is feu
\ps v
\ge bois frotté pour faire du feu
\cf yaa-he
\ce feu allumé par friction
\cf he
\ce frotter
\dt 08/Feb/2025

\lx ce-kabun
\dialx PA BO
\is coutumes_objet
\ps n
\ge perche signalant un interdit
\cf ce-nõbu
\dt 08/Jan/2022

\lx ce-ka, ce-xa
\dialx GOs
\va ce-kam
\dialx BO (Corne)
\is plantes
\ps n
\ge arbuste ; bagayou des vieux
\sc Polyscias scutelaria (N.L. Burm.) Fosberg
\scf Araliacées
\dt 22/Oct/2021

\lx ce-kiyai
\dialx GOs BO
\va ce-xiyai
\is feu
\ps n
\ge bois pour la cuisine
\ge bois pour le feu 
\dn allumé dehors pour se réchauffer
\cf ce-bò ; ce bwo
\ce bois pour la nuit
\dt 08/Feb/2025

\lx ce-ko
\dialx GO
\va cee-xòò
\dialx PA
\va ce-kòòl
\dialx PA BO
\is coutumes_objet
\ps n
\ge perches plantées devant la porte
\dn (lit. bois debout)
\dt 09/Feb/2025

\lx ce-kui
\dialx GOs
\va ce-xui
\dialx GO(s)
\is plantes
\ps n
\ge nom de plante
\sc Mimusops parvifolia
\scf Sapotacées
\dt 13/Sep/2021

\lx ce-kura
\ph cɨ kura
\dialx GOs PA
\va ce-kutra
\ph cɨ kuʈa, cɨ kuɽa
\dialx GO(s)
\sn 1
\is plantes
\ps n
\ge bois de sang
\sc Cocconerion balansæ
\scf Euphorbiacées
\sn 2
\is plantes
\ps n
\ge "sang dragon"
\sc Pterocarpus indicus Willdenow
\scf Fabacées, Cæsalpinioidées
\dt 15/Feb/2025

\lx celèng
\ph celɛŋ
\dialx PA BO [BM]
\is action
\ps n
\ge effleurer
\dt 29/Jan/2019

\lx ce-mããni
\dialx GOs
\is plantes
\ps n
\ge sensitive
\dn cette plante se ferme quand on la touche, d'où |lx{mããni} dormir ; elle a une tige rouge, des feuilles vert-gris et est dotée de petites épines
\sc Mimosa pudica L.
\scf Fabacées
\dt 10/Jan/2022

\lx ce-mwa
\dialx PA BO
\is maison
\ps n
\ge chevrons ; solives
\dn les solives suivent la pente du toit et soutiennent les gaulettes qui retiennent la paille
\cf ce-nuda
\ce solives
\dt 08/Jan/2022

\lx cèni
\ph 'cɛɳi
\dialx GOs
\va cani
\dialx BO PA
\is nourriture
\ps v
\ge manger (des féculents)
\xv i cani pò-wha
\xn il mange des figues sauvages
\xv cèni kha-maaçe-ni cè-çö lai
\dialx GOs
\xn mange ton riz lentement
\xv i aa-cani
\dialx BO PA
\xn il est gourmand
\cf cè-
\dialx GOs
\ce part de féculents
\cf ca-
\dialx PA BO
\ce part de féculents
\cf hovwo
\ce manger (général)
\et *kani
\el POc
\dt 10/Jan/2022

\lx ce-nõbu
\dialx GO BO
\is coutumes_objet
\ps n
\ge perche signalant un interdit
\cf ce-kabun
\ce perche signalant un interdit
\dt 08/Jan/2022

\lx ce-nuda
\dialx PA BO
\is maison
\ps n
\ge solives
\dn les solives soutiennent les gaulettes qui retiennent les écorces de niaouli et la paille recouvrant la maison ; ces solives sont plus petites que les solives |lx{ce-mwa} d'après Dubois
\cf orèi, zabo
\dt 05/Jan/2022

\lx ce-pexoli
\dialx GOs
\is plantes
\ps n
\ge aubergine sauvage
\sc Solanum torvum
\scf Solanacées
\dt 27/Oct/2023

\lx cetil
\dialx BO
\is armes
\ps n
\ge plumet de fronde
\dn en fibre d'aloès
\cf thila
\ce plumet de fronde plus court
\nt selon Corne ; non vérifié
\dt 08/Feb/2025

\lx ce-tha
\dialx GOs WEM
\is maison
\ps n
\ge charpente (maison)
\ge poutre faîtière
\dn cette poutre rejoint le |lx{pwabwani} au sommet du toit
\dt 17/Feb/2025

\lx ce-thîni
\dialx GOs
\is maison
\ps n
\ge poteaux de barrière
\dt 23/Jan/2018

\lx ce-thri
\dialx GOs
\is coutumes_objet
\ps n
\ge perche qui annonce le décès d'un chef
\dt 27/Jan/2019

\lx ce-vada
\dialx GOs PA
\va ce-pada
\dialx GO(s)
\is arbre
\ps n
\ge flamboyant
\sc Serianthes calycina Benth
\scf Fabacées
\dt 27/Aug/2021

\lx cèvèro
\dialx BO PA
\is mammifères
\ps n
\ge cerf
\dt 16/Aug/2021

\lx ce-xou
\dialx GOs PA
\va hup
\dialx PA
\is arbre
\ps n
\ge houp
\sc Montrouziera sp.
\scf Guttifères
\dt 27/Aug/2021

\lx ci
\dialx GOs
\va cin
\dialx WEM WE BO
\is arbre
\ps n
\ge papayer
\sc Carica papaya L.
\scf Caricacées
\se po-ci
\dialx GO
\va po-cin
\dialx BO
\sge papaye
\et *kulu(R)
\el POc
\dt 13/Jan/2022

\lx ci-
\dialx GO PA
\is plantes_partie
\ps n
\ge écorce ; enveloppe
\se ci-ce, ci-cee
\dialx GO PA
\sge écorce d'arbre
\et *kupit
\eg bark, peelings
\el POc
\dt 08/Jan/2022

\lx cî
\dialx GOs PA BO
\is crustacés
\ps n
\ge crabe de palétuvier
\dn plus petit que |lx{ji, jim}, de couleur noire, il s'enfouit dans la terre
\dt 08/Feb/2025

\lx ci pojo
\dialx BO
\is insecte
\ps n
\ge punaise
\nt selon Corne ; non vérifié
\dt 08/Feb/2025

\lx cia
\ph cɨ.a
\dialx GOs BO PA
\is danse
\ps v ; n
\ge danser ; danse
\dt 02/Feb/2019

\lx cibò
\dialx GOs
\is corps_animaux
\ps n
\ge queue (poisson)
\se cibò nõ
\sge queue de poisson
\dt 22/Aug/2021

\lx ci-ciò
\dialx GOs
\is instrument
\ps n
\ge tôle
\dn (lit. peau/couverture en tôle)
\cf ci-
\ce peau
\dt 08/Feb/2025

\lx ci-chaamwa
\ph cɨ-cʰa:mwa
\dialx GOs
\is bananier
\ps n
\ge enveloppe de tronc de bananier
\dt 08/Jan/2022

\lx cii
\hm 1
\dialx GOs PA BO
\is corps
\ps n
\ge peau
\se ci-phwa-n
\sge ses lèvres
\xv cii-n
\dialx PA BO
\xn peau (sa)
\et *kuli(t)
\el POc
\ng forme courte en composition |lx{ci-N}
\dt 22/Feb/2025

\lx cii
\hm 2
\dialx GOs PA BO
\va cee
\dialx PA
\is grammaire_quantificateur_degré
\is grammaire_modalité
\ps DEGRE ; INTENS
\ge très ; vraiment
\se cii êgu
\dialx GO
\sge homme mûr, dans la force de l'âge, de grande taille
\xv e cii alavwu
\dialx GO
\xn il a très faim
\xv i xau cii êgu !
\dialx PA
\xn il est vraiment très costaud
\xv e cii êgu nai je
\dialx GO
\xn il est vraiment plus grand qu'elle
\xv lha cii thu fe
\dialx GO
\xn ils font une grande fête
\xv e cii gi
\dialx GO
\xn il pleure fort
\xv nu cii vha
\dialx PA
\xn je parle sérieusement
\xv e za cii hòò
\dialx GO
\xn c'est vraiment très loin
\dt 26/Aug/2024

\lx cii êgu
\dialx GOs
\is société
\ps n
\ge homme mûr ; dans la force de l'âge
\dt 27/Jan/2019

\lx ciia
\ph ci:a
\dialx GOs BO PA
\va ciiya
\dialx BO
\is céphalopode
\ps n
\ge poulpe ; pieuvre
\et *kuRita
\el POc
\dt 02/Feb/2019

\lx ciia hulò hailò hulò
\dialx GOs
\is poisson
\ps n
\ge seiche
\dn (lit. pieuvre sans bout)
\dt 08/Feb/2025

\lx ciibwin
\dialx PA BO
\va cibwi
\dialx BO
\is mammifères
\ps n
\ge rat
\cf zine
\dialx GOs
\ce rat
\et *ka(n)supe
\el POc
\dt 10/Jan/2022

\lx cii-du
\ph cɨ:-ndu
\dialx GOs
\is corps
\ps n
\ge colonne vertébrale
\xv cii-duu-nu
\xn ma colonne vertébrale
\cf du
\ce os
\dt 06/Feb/2019

\lx cii-hoogo
\ph cɨ:ho:ŋgo
\dialx PA
\is topographie
\ps n
\ge crête de la montagne
\dt 02/Jan/2022

\lx cii-vhaa
\dialx GOs
\is discours
\ps n
\ge vraie parole
\dt 28/Jan/2019

\lx ciiza
\dialx GOs
\va ciilaa
\dialx BO
\is insecte
\ps n
\ge pou de corps ; morpion
\dt 29/Jan/2019

\lx ci-kãbwa
\dialx GOs
\va ci-xãbwa
\dialx GO(s)
\is habillement
\ps n
\ge étoffe ; tissu ; vêtement
\dn (lit. peau du diable)
\dt 09/Feb/2025

\lx ci-kui
\dialx GOs
\is igname
\ps n
\ge peau de l'igname
\dt 08/Jan/2022

\lx cili
\dialx PA BO
\is action_corps
\ps v
\ge secouer
\dt 25/Jan/2019

\lx ciluu
\dialx BO
\is mouvement
\ps v
\ge incliner (s')
\nt selon BM
\dt 26/Mar/2022

\lx ci-me
\dialx GOs
\is corps
\ps n
\ge paupière
\xv ci-mee-nu
\xn ma paupière
\dt 05/Jul/2022

\lx cimic
\dialx PA
\is habillement
\ps n
\ge chemise
\xv i udale cimiy-i ye
\dialx PA
\xn il met sa chemise
\bw chemise (FR)
\dt 09/Jul/2022

\lx cimwî
\dialx GOs PA
\va khimwi
\dialx BO [BM]
\is action_corps
\ps v
\ge tenir ferme ; saisir
\xv li pe-cimwî hii, li pe-cimwî hii-li
\dialx PA
\xn ils se serrent la main
\se te-yimwî
\dialx GO
\sge attraper, saisir
\et *kumi
\el POc
\dt 02/Nov/2021

\lx cin
\dialx BO
\is arbre
\ps n
\ge papayer
\sc Carica papaya L.
\scf Caricacées
\xv po cin
\xn papaye
\et *kulu(R)
\el POc
\dt 27/Aug/2021

\lx cińevwö
\ph cineβω
\dialx GOs BO
\is caractéristiques_personnes
\ps v.stat
\ge important
\xv ci nevo
\dialx BO
\xn très important [BM]
\xv e je-nã ńya e cińevwö na ni mõlò i ã
\dialx GOs
\xn voilà ce qui est important dans nos vies
\dt 03/Feb/2025

\lx ci-nu
\ph cɨɳu
\dialx GOs
\is arbre_cocotier
\ps n
\ge bourre de coco
\dt 31/Dec/2021

\lx ci-pòò
\dialx GOs
\is plantes_partie
\ps n
\ge écorce de bourao
\dn sert à la confection des jupes anciennes
\dt 08/Feb/2025

\lx ci-phãgoo
\dialx GOs PA
\is corps
\ps n
\ge peau (humain) ; enveloppe corporelle
\xv ci-phãgoo-je
\dialx GOs
\xn sa peau
\xv la phããde ci-phãgo
\dialx PA
\xn ils ont fait acte de présence
\cf ci-chova
\ce la peau du cheval
\dt 06/Nov/2021

\lx ci-phai
\dialx GOs
\va cin-phai
\dialx PA BO
\is arbre
\ps n
\ge arbre à pain
\dn son fruit est bouilli
\sc Artocarpus altilis (Park.) Fosb.
\scf Moracées
\dt 08/Feb/2025

\lx ci-phwa
\dialx GOs
\va ci-phwa-n
\dialx BO PA
\is corps
\ps n
\ge lèvres
\dt 24/Jan/2019

\lx cirara
\ph ciɽaɽa, cirara
\dialx GOs
\va citratra
\dialx GO(s)
\is caractéristiques_objets
\ps v.stat
\ge fin ; mince
\dt 03/Feb/2025

\lx ciròvwe
\ph cirɔβe
\dialx GOs
\va ciròvhe
\dialx PA BO
\is action
\ps v
\ge retourner ; mettre à l'envers (linge)
\dt 22/Feb/2025

\lx citèèn
\dialx BO
\is danse
\ps n
\ge danse des morts
\dn effectuée par les hommes
\nt selon Corne ; non vérifié
\cf caro
\ce danse des morts (effectuée par les femmes)
\dt 08/Feb/2025

\lx civwi
\ph ciβi
\dialx GOs
\is fonctions_naturelles
\ps v
\ge toucher ; agacer (une blessure)
\dt 22/Feb/2025

\lx cixa
\dialx GOs
\is caractéristiques_personnes
\ps v.stat
\ge craintif
\dt 03/Feb/2025

\lx cixè
\dialx GOs
\is société
\ps n
\ge même génération ; même tranche d'âge
\xv pe-cixèè-la
\dialx GO
\xn ils sont d'une même tranche d'âge, d'une même génération
\xv pe-poxe cixèè-li
\dialx PA
\xn ils sont d'une même tranche d'âge, d'une même génération
\xv cixè ẽnõ
\dialx GO
\xn enfants d'une même tranche d'âge, d'une même génération
\xv cixè whamã
\dialx GO
\xn vieilles personnes d'une même tranche d'âge, d'une même génération
\dt 27/Jan/2019

\lx ci.i
\ph ci:i
\dialx GOs WE PA
\va chi.i
\dialx BO
\is insecte
\ps n
\ge pou
\ge puce
\xv çö ã-daa-mi pa-çi-nu
\dialx GOs WE PA
\xn viens ici fouiller mes cheveux
\se pa-ci ; pa-çi
\sge épouiller; fouiller les cheveux
\et *kutu
\eg pou
\el POc
\dt 12/Jan/2022

\lx cò
\dialx GOs BO
\is corps
\ps n
\ge pénis (grossier) ; sexe (de l'homme)
\xv wee cò-je
\dialx GO
\xn son sperme
\xv còò-n
\dialx BO
\xn son sexe ; son pénis
\cf phi, pi
\ce testicules
\et *kau, *kayu
\eg penis
\el POc
\dt 22/Feb/2025

\lx cö
\ph cω
\dialx GOs
\va còòl
\dialx PA BO
\va cul, cu
\dialx PA
\is mouvement
\ps v
\ge sauter
\ge débarquer
\xv novwö ã uvilu, je cöö-ò cöö-mi na bwa ã-da yòò
\dialx GO
\xn mais cet oiseau, il sautille de-ci de-là sur ce bois de fer
\xv nu cu-da bwa mòl
\dialx PA
\xn je débarque sur la terre ferme ; je sors de l'eau
\xv nu cu-du ni we
\dialx PA
\xn je saute à l'eau
\dt 22/Feb/2025

\lx çò
\ph ʒɔ
\dialx GO
\va yò
\dialx PA
\is grammaire_pronom
\ps PRO 2° pers. duel (sujet)
\ge vous deux
\dt 29/Mar/2022

\lx -çò
\ph ʒɔ
\dialx GOs
\is grammaire_pronom
\ps PRO 2° pers. duel (OBJ ou POSS)
\ge vous deux ; vos
\dt 29/Mar/2022

\lx cobo
\dialx GOs
\is action_corps
\ps v
\ge tapoter
\dt 23/Jan/2018

\lx coboe
\dialx GOs
\va cobwoi
\dialx BO (Corne, BM)
\is action_corps
\ps v
\ge pincer entre les doigts
\dt 25/Jan/2019

\lx coçovwa
\ph coʒoβa
\dialx GOs
\va cocopa
\dialx GO(s)
\va cocovwa
\dialx PA BO
\sn 1
\is grammaire_quantificateur_mesure
\ps COLL ; QNT
\ge tous ; chaque
\ge tous ensemble
\se coçovwa êgu
\sge les hommes ensemble
\se coçovwa tree
\sge tous les jours
\sn 2
\is grammaire_quantificateur_mesure
\ps COLL ; QNT
\ge tout ; toutes sortes de
\xv haivwö la coçovwa pwaixe xa lò yue
\dialx GO
\xn il y a beaucoup de sortes d'animaux (lit. choses) qu'ils élèvent
\xv za kò salad, mãni tròòmwa, mãni la coçovwa
\dialx GO
\xn il pousse des salades et des tomates et d'autres sortes (de plantes)
\dt 22/Feb/2025

\lx cò-chaamwa
\ph cɔ-cʰa:mwa
\dialx GOs
\is bananier
\is plantes_partie
\ps n
\ge bout de l'inflorescence de bananier
\dt 29/Jan/2019

\lx cö-da
\ph cω-nda
\dialx GOs WEM
\va cu-da
\dialx PA
\is déplacement
\ps v
\ge monter ; grimper
\ge traverser
\xv nu cö-da bwa loto
\dialx GO
\xn je monte dans la voiture
\xv cö-da
\dialx GO
\xn monter (au cocotier en se poussant des pieds)
\xv cö-du
\dialx GO
\xn sauter à bas
\dt 04/Nov/2021

\lx cö-e
\ph cωe
\dialx GOs BO
\va cu-e
\dialx BO
\is déplacement
\ps v
\ge traverser (rivière)
\xv e cö-e nõgò
\dialx GO
\xn il traverse la rivière
\dt 25/Jan/2019

\lx cöi
\ph cωi
\dialx GOs PA BO
\is action_corps
\ps v.t.
\ge creuser (trou)
\ge gratter (terre)
\ge creuser (pour contrôler l'état des tubercules)
\xv e cöi phwe pwaji
\dialx GO
\xn elle creuse le trou du crabe
\xv e cöi dili
\dialx GO
\xn elle creuse la terre
\et *k(a/e)li
\el POc
\dt 13/Oct/2021

\lx côî
\ph côî
\dialx GOs PA
\va chôî
\dialx BO (Corne)
\is action_corps
\ps v.t.
\ge suspendre
\ge prendre au collet
\xv e côî pu-nõõ-je
\dialx GO
\xn il l'attrape par le collet
\xv nu kha-côî kee-nu
\dialx GO
\xn je marche avec mon sac au bras ou à la main
\xv i pa-yôî-du-mi êne ni nõõ-n
\dialx PA
\xn il le suspend à son cou
\dt 05/Nov/2021

\lx cò-mhwêdin
\dialx BO
\is corps
\ps n
\ge partie du nez entre les deux narines
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx cöńi
\ph cωni
\dialx GOs BO PA
\va cööni
\dialx PA
\sn 1
\is santé
\ps v
\ge souffrir
\ge endurer
\sn 2
\is sentiments
\ps v
\ge affligé
\ge retenir ses larmes (enfant)
\ge retenir son souffle |dialx{PA}
\sn 3
\is interaction
\ps v
\ge pleurer un mort
\ge deuil (être en)
\xv i cöni
\dialx BO
\xn il est en deuil (Corne)
\cf mõõdim
\dialx WEM WE BO PA
\ce deuil
\cf giul
\ce pleurer un mort (pour les hommes)
\dt 10/Jan/2022

\lx cöńi-vwo
\dialx GOs
\is santé
\ps n
\ge souffrance
\dt 25/Jan/2019

\lx côô
\ph cõ:
\dialx GOs
\va cô
\dialx PA BO
\sn 1
\is position
\ps v.i
\ge suspendu
\ge accroché
\xv e kô-côô
\dialx GO
\xn elle dort suspendue (roussette)
\se paa-côôe
\dialx GO
\va pa-yôôi
\dialx BO
\sge suspendre, accrocher qqch.  (BM)
\sn 2
\is action_corps
\ps v
\ge soulever (un objet léger)
\xv côô-e-da !
\dialx GO
\xn soulève-le !
\ng v.t. |lx{côô-e}
\sn 3
\is navigation
\ps v.i
\ge accoster (bateau)
\xv e côô wõ
\dialx GO
\xn le bateau a accosté
\dt 21/Feb/2025

\lx cöö-du
\ph cω:ndu
\dialx GOs WEM
\va cu-du, cul-du
\dialx PA
\is déplacement
\ps v
\ge descendre ; sauter en bas
\ge plonger
\cf u-du
\ce entrer (dans une maison)
\dt 19/Dec/2021

\lx cooge
\dialx GOs BO
\va coge
\dialx BO
\is action_corps
\ps v
\ge lever ; soulever (des choses lourdes) ; élever
\ge ramasser
\ge soutenir (avec la paume de la main)
\se cooge-da
\sge soulever en l'air
\dt 20/Aug/2021

\lx coo-kêni
\dialx GO PA
\is corps
\ps n
\ge lobe de l'oreille
\xv coo-kênii-n
\dialx PA
\xn le lobe de son oreille
\dt 21/Aug/2021

\lx côôni
\dialx PA
\is action_corps
\ps v.t.
\ge toucher à qqch.
\xv i côôni u ri ?
\xn qui l'a touché ? (et abîmé)
\xv kêbwa côôni hèlè
\xn ne touche pas au couteau
\dt 29/Mar/2022

\lx cöö-ò cöö-mi
\ph cω:ɔ cω:mi
\dialx GOs
\va cu-ò cu-mi
\dialx BO
\is déplacement
\ps v
\ge sauter de-ci de-là
\xv i pe-cu-ò cu-mi
\dialx BO
\xn il saute de-ci de-là
\dt 04/Nov/2021

\lx cou
\dialx GOs PA WEM WE
\ph co.u
\sn 1
\is nourriture
\ps v
\ge dur (taro, igname et manioc)
\ge immangeable (taro et manioc)
\dn se dit du taro et du manioc mal cuit
\sn 2
\is discours
\ps v
\ge zozoter
\dn sens figuré
\xv e cou phwa-je
\dialx GO
\xn il zozote
\dt 22/Feb/2025

\lx co-xa-da
\dialx PA
\is navigation
\ps v
\ge accoster ; échouer
\xv u co-xa-da je wony
\dialx PA
\xn ce bateau accoste
\dt 12/Jun/2024

\lx còxe
\ph cɔɣe
\dialx GOs BO PA
\va còòxe
\dialx PA
\va còge
\ph cɔŋge
\dialx BO PA arch.
\is soin
\ps v
\ge couper (avec des ciseaux)
\dn par ex. cheveux, barbe, etc.
\ge tailler (barbe)
\xv i còòxe pi-hii-n
\dialx PA
\xn il se coupe les ongles
\xv i còòxe pu-bwaa-n
\dialx PA
\xn il s'est coupé les cheveux
\xv nu pe-còòxe pu
\dialx PA
\xn je me rase les poils
\xv li pe-còòxe
\dialx PA
\xn ils sont occupés à se couper les cheveux (cette action n'est pas obligatoirement réciproque ; elle peut simplement référer à deux personnes impliquées dans ce processus, dont l'une est celle qui fait l'action de couper)
\xv ã-du mwa còòxe
\dialx PA
\xn (ils) entrent pour le tuer (lit. le couper)
\et *koti
\el POc
\dt 22/Feb/2025

\lx cö, çö, yö
\ph ʒω jω
\dialx GOs
\va co, yo
\dialx PA
\va cu, yu
\dialx PA BO
\is grammaire_pronom
\ps PRO 2° pers. SG (sujet ou OBJ)
\ge tu ; te
\dt 02/Jan/2022

\lx cu
\dialx BO
\va cuu
\is grammaire_quantificateur_degré
\ps QNT ; DEGRE
\ge trop
\xv u mha cu mwã la peenã i phe
\dialx BO
\xn il a trop pris d'anguilles
\nt selon Corne
\dt 27/Mar/2022

\lx cubii
\dialx PA
\is action_tête
\ps v
\ge déchirer avec les dents
\cf kaobi
\ce casser avec les dents
\dt 26/Jan/2019

\lx cubu
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge déborder
\xv e cubu we
\xn l'eau déborde
\dt 29/Jan/2019

\lx cuk
\dialx PA BO
\is nourriture
\ps n
\ge sucre
\bw sucre (FR)
\dt 26/Jan/2019

\lx cuka
\dialx BO
\is nourriture
\ps n
\ge pomme 
\dn selon [BM]
\ge banane sucre
\dn selon [Corne]
\bw sugar (GB)
\dt 09/Feb/2025

\lx cul (a) kao
\dialx PA
\va col (a) kao
\dialx BO
\is action_eau_liquide_fumée
\ps v
\ge déborder (lit. sauter inondation)
\dt 08/Jan/2022

\lx cura
\dialx BO
\is habitat
\ps v
\ge habiter ; demeurer
\dn terme de registre élevé
\xv li a cura bwa ènè-da Phaja
\xn ils habitent à cet endroit en haut à Phaja
\dt 08/Feb/2025

\lx cuxi
\ph cuɣi
\dialx GO PA BO
\va cuki
\dialx GO(s)
\va cugi
\dialx BO
\sn 1
\is santé
\ps v.stat
\ge fort ; vigoureux ; résistant
\sn 2
\is caractéristiques_personnes
\ps v.stat ; n
\ge courage ; courageux
\xv e zo na çö cuxi nani mõlõõ-jö
\dialx GO
\xn il faut que tu sois courageux dans ta vie
\dt 03/Feb/2025

\lx cha
\ph cʰa
\dialx PA
\is caractéristiques_objets
\ps v
\ge aiguisé ; coupant
\xv nõõli cha hèlè !
\xn regarde les traces de machette (dans la brousse)
\dt 14/Aug/2021

\lx chaa
\ph cʰa:
\dialx GOs
\is eau_topographie
\ps n
\ge récif
\et *sakaRu
\el POc
\dt 29/Jan/2019

\lx chaaçee
\ph cʰa:ʒe:
\dialx GOs PA
\is position
\ps ADV
\ge travers (de) ; penché sur un côté
\xv e no-chaaçee
\xn elle regarde la tête penchée sur le côté
\xv ku-chaaçee nhye cee mwã
\xn l'arbre est penché sur le côté
\xv kô-chaaçee loto
\xn la voiture est de travers (penchée sur le bas-côté)
\dt 10/Mar/2023

\lx chaamwa
\ph cʰa:mwa
\dialx GOs PA BO WE
\sn 1
\is plantes_fruits
\ps n
\ge banane (générique)
\sn 2
\is bananier
\ps n
\ge bananier
\sc Musa sp.
\scf Musacées
\se cò-chaamwa
\sge bout de l'inflorescence de bananier
\se thò-chaamwa
\sge régime de bananes
\dt 20/Feb/2025

\lx chaamwa we-ê
\dialx GOs
\is bananier
\ps n
\ge banane sucrée
\dn de petite taille
\dt 08/Feb/2025

\lx chaavwa-dili
\ph cʰa:βa-ndili
\dialx GOs
\va cava dili
\dialx BO
\is terrain_terre
\ps v.stat
\ge boue
\dn (lit. terre molle)
\xv we cava dili
\dialx BO
\xn eau boueuse
\xv tomènô ni cavaa dili
\dialx BO
\xn marcher dans la boue
\dt 08/Feb/2025

\lx chan
\dialx BO
\is discours
\ps v
\ge bégayer ; bredouiller
\nt selon BM ; non vérifié
\dt 27/Mar/2022

\lx chãnã
\hm 1
\ph cʰɛ̃ɳɛ̃
\dialx GOs WE
\va chãnã, cãnã
\ph cʰɛ̃nɛ̃
\dialx PA BO
\sn 1
\is fonctions_naturelles
\ps v ; n
\ge respirer
\ge souffle
\ge respiration
\xv chãnã-õ
\dialx GO
\xn notre souffle
\xv kixa c(h)ãnã-n
\dialx PA
\xn il est essoufflé/à bout de souffle
\xv c(h)ãnã-n
\dialx PA
\xn son souffle
\sn 2
\is fonctions_naturelles
\ps v ; n
\ge reposer (se) ; repos
\dt 22/Feb/2025

\lx chãnã
\hm 2
\ph cʰɛ̃ɳɛ̃
\dialx GOs
\is grammaire_aspect
\ps ASP persistif
\ge sans cesse ; sans arrêt ; constamment ; toujours ; à répétition
\xv e gi chãnã ẽnõ-ã
\xn cet enfant-là pleure sans cesse
\xv e vhaa chãnã
\xn il parle sans cesse ; il s'obstine à parler
\xv e za ge ni bwaa-nu chãnã nhye zixô
\xn ce conte est toujours dans ma tête
\xv novwo kêê-Jae, ca lhi za gi chãna mã õã-je
\xn quant au père de Jae, il pleure toujours là avec la mère
\xv e zòò chãnã xa za phee-je du xo we
\xn il nage sans arrêt mais le courant l'emporte
\xv e zòò da xa za phee-je du chãnã xo we
\xn il nage vers la rive, mais le courant l'emporte constamment vers le large
\xv e cani chãnã kuru
\xn il ne mange que du taro
\xv mo phweexu chãnã
\xn nous bavardons sans arrêt
\cf haa
\dialx PA
\dt 22/Feb/2025

\lx chãnã waa
\dialx PA
\is fonctions_naturelles
\ps v
\ge respirer la bouche ouverte
\ge haleter
\dt 23/Aug/2021

\lx chavwi
\dialx GOs
\ph cʰaβi
\va cabwi
\dialx PA
\is nourriture
\ps n
\ge disette ; famine
\dt 26/Jan/2019

\lx chavwo
\bw savon (FR)
\ph cʰaβo
\dialx GOs
\va chapo
\dialx GO(s) arch.
\va cavo
\dialx PA
\va caavu
\dialx BO (Corne)
\is soin
\ps v ; n
\ge savon
\ge laver (au savon)
\xv e chavwoo mee xo Kavwo
\dialx GO
\xn Kavwo se lave le visage
\xv e chavwoo-ni mee-je xo chavwo
\dialx GO
\xn elle se lave le visage avec du savon
\xv i cavo-ni mee-n u Kavwo
\dialx PA
\xn Kavwo se lave le visage (ou) Kavwo lui lave le visage
\xv i cavo-ni mee Kavwo
\dialx PA
\xn Kavwo se lave le visage (ou) elle lave le visage de Kaawo
\xv i cavo-ni mee-n
\dialx PA
\xn elle se lave le visage (seule interprétation possible)
\xv e chavwo-ni hõbwò
\dialx GO
\xn elle lave les vêtements
\ng v.t. |lx{chavwoo-ni} + objet nominal
\cf jaamwe
\ce laver
\dt 06/Jan/2022

\lx chawe
\ph cʰawe
\dialx GOs
\is préparation_aliments
\ps v
\ge faire du bouillon, de la soupe
\dt 25/Jan/2019

\lx chaxe
\ph cʰaɣe
\dialx PA BO [BM]
\is action_corps
\ps v
\ge étirer (s')
\dt 25/Jan/2019

\lx chèèvwe
\ph cʰɛ:βe
\dialx GOs
\va chèèbwe
\va cewe
\dialx BO
\is bananier
\ps n
\ge banane
\dn à peau grise, elle a la forme d'une pirogue
\dt 08/Feb/2025

\lx chele
\ph cʰele
\dialx GOs PA
\is action_corps
\ps v
\ge toucher
\ge effleurer (qqch.) ; frôler
\xv kêbwa na çö nee vwo kha-chelee-ni gòò-nu
\dialx GO
\xn il ne faut pas que tu lui fasses toucher le tronc du cocotier
\ng v.t. |lx{chelee-ni}
\dt 17/Feb/2025

\lx chèńi
\ph 'cʰɛni
\dialx GOs
\is poisson
\ps n
\ge mulet de rivière
\dt 29/Jan/2019

\lx chiçô
\ph cʰiʒõ
\dialx GOs
\va cicô
\dialx PA
\is instrument
\ps n
\ge ciseaux
\bw ciseaux (FR)
\dt 08/Feb/2019

\lx chińõ
\ph cʰinɔ̃
\dialx GOs PA
\va cinõ
\dialx BO
\wr A
\is caractéristiques_objets
\ps n
\ge grosseur ; épaisseur
\ge taille ; circonférence
\xv pe-poxe chińõ-li
\xn ils sont de même taille (en largeur)
\xv e whaya chińõ ?
\dialx GO
\xn quelle taille ? ; quelle grandeur ?
\xv e whaya chinõ-n ?
\dialx PA
\xn quelle taille ? ; quelle grandeur ?
\xv cinõ-n
\dialx BO
\xn sa taille
\an pònõ
\at petit
\wr B
\is caractéristiques_objets
\ps n ; v
\ge occuper tout l'espace
\xv cinõ-we
\dialx BO
\xn le lit du fleuve
\xv phe chińõ je we
\dialx GO
\xn elle (une anguille) faisait/prenait toute la taille de ce trou d'eau
\wr C
\is grammaire_verbe_locatif
\ps v
\ge en plein dans
\xv ge-li ne pe-chińõ we
\dialx GO
\xn ils sont en pleine mer
\wr D
\is grammaire_quantificateur_mesure
\ps QNT
\ge tous ; totalité
\xv chińõ phwe-meevwu çò phweexoe êni
\dialx GO
\xn tous les clans que vous avez nommés là
\xv chinõ mwa
\dialx PA
\xn la totalité de la maison
\xv chinõ êgu
\dialx PA GO
\xn tous les gens
\xv la pevwe tõne-nu ko chinõ êgu
\dialx PA
\xn tous les gens m'entendent
\dt 22/Feb/2025

\lx chińõõ
\ph cʰinɔ̃:
\dialx GOs PA
\is action_corps
\ps v
\ge essorer
\ge frotter (linge, mains, etc.)
\ge tripoter (un objet) ; toucher
\dt 20/Aug/2021

\lx chiò
\ph cʰiɔ
\dialx GOs BO PA
\is ustensile
\ps n
\ge seau
\bw seau (FR)
\dt 26/Jan/2019

\lx chira, chiira
\ph cʰira, cʰiɽa
\dialx GOs PA
\is pêche
\ps n
\ge encoche de l'hameçon
\ge barbeau de la sagaie |dialx{BO, BM}
\dt 25/Mar/2022

\lx chivi
\ph cʰivi
\dialx BO
\va chivi, civi
\dialx BO
\is mouvement
\ps v
\ge écarter ; chasser (animal)
\dt 25/Jan/2019

\lx chiwe
\dialx GOs PA BO
\ph cʰiwe
\is fonctions_naturelles
\ps v
\ge éternuer
\dt 25/Jan/2019

\lx chîxi
\ph cʰîɣî
\dialx GOs PA
\va chîngi
\ph cʰiŋi
\dialx GO(s) BO
\is grammaire_manière
\ps v
\ge contraire ; contraire (faire le)
\ge envers (à l') ; faire à l'envers
\ge faire l'inverse (de tout le monde)
\xv e a-chîngi
\dialx GO
\xn il est gauche ; il fait tout à l'envers
\xv i aa-chîxî
\dialx PA
\xn c'est quelqu'un qui fait tout à l'inverse
\xv chîngi mwêêje-je
\dialx GO
\xn ses façons de faire sont à l'envers de tout le monde
\xv nu a-chîngi-da
\xn je monte (en sens opposé de toi qui descend)
\xv e nee-chîngi-ni
\dialx GO
\xn il l'a fait à l'envers
\xv i a chîxî
\dialx PA
\xn il part en sens inverse
\ng v.t. |lx{V-chîxi-ni} dans une construction verbale complexe
\dt 22/Feb/2025

\lx chö
\ph cʰω
\dialx GOs
\va coho
\dialx BO (Corne)
\is mammifères_marins
\ps n
\ge baleine ; cachalot
\dt 16/Aug/2021

\lx chôã
\ph cʰôɛ̃
\dialx GOs PA BO WEM WE
\sn 1
\is jeu
\ps v ; n
\ge jouer ; s'amuser ; jeu
\xv la pe-chôã
\dialx GO
\xn ils jouent ensemble ; ils se jouent des tours
\se pe-chôã
\dialx GO
\sge jouer ensemble ; se disputer
\se chôã-nu
\dialx GOs
\sge mes jeux
\sn 2
\is interaction
\ps v ; n
\ge jouer des tours ; embêter
\xv kêbwa na cö zo(ma) chôã-la
\xn il ne faut pas que tu les déranges
\se chôã-raa
\dialx GO
\sge jouer des mauvais tours
\ng v.t. |lx{chôã} + objet pronominal; |lx{chôã-ni} + objet nominal
\gt jouer avec qqch.
\dt 22/Feb/2025

\lx chôã-ni
\ph cʰôɛ̃ɳi
\dialx GOs WEM WE
\is jeu
\ps v.t.
\ge jouer à qqch.
\ge embêter
\xv e chôã-ni loto
\dialx GO
\xn il joue avec sa voiture
\xv kêbwa chôãni nye-na
\dialx GO
\xn ne joue pas avec ça
\ng v.t. |lx{chôã} + objet pronominal; |lx{chôã-ni} + objet nominal
\dt 29/Mar/2022

\lx chòmu
\dialx GOs BO
\ph cʰɔmu
\va comu
\dialx PA
\is fonctions_intellectuelles
\ps v
\ge apprendre ; étudier
\ge lire |dialx{PA BO}
\xv nu chòmuu-ni yuanga
\dialx BO
\xn j'apprends le yuanga
\se a(a)-chòmu
\sge enseignant
\se ba-chòmu
\sge livre
\se mo-chòmu
\sge école
\se pha-chòmu(u)-ni
\sge enseigner
\ng v.t. |lx{chòmu(u)-ni}
\gt apprendre
\cf phinãã
\dialx GOs
\ce lire ; compter
\dt 22/Feb/2025

\lx chòvwa
\ph cʰɔβa, cʰɔ:βa, cʰɔva
\dialx GOs
\va còval
\dialx WE BO
\va cova
\dialx PA
\is mammifères
\ps n
\ge cheval
\xv chòvwa i nu
\xn mon cheval
\bw cheval (FR)
\dt 06/Feb/2019

\lx chue
\dialx GOs WEM WE
\ph cʰue
\is musique
\ps v
\ge jouer
\dn de la guitare, aux cartes, jeu de balle, sport
\bw jouer (FR)
\dt 08/Feb/2025

\lx da
\ph nda
\dialx GOs PA BO
\sn 1
\is grammaire_suff_directionnel
\ps DIR
\ge en haut
\ge en amont
\se ã-da-mi !
\sge monte ici !
\an ã-du !
\at descends ! (ou va vers le nord)
\sn 2
\is grammaire_suff_directionnel
\ps DIR
\ge vers le sud ; vers l'est
\sn 3
\is grammaire_suff_directionnel
\ps DIR
\ge vers la terre ; vers le fond de la vallée ou l'intérieur du pays
\sn 4
\is grammaire_suff_directionnel
\ps DIR
\ge à l'intérieur de la maison ; vers le fond de la maison
\xv e mããni-da bwaa-je nõ mwa xo kòò-je du ni phwee-mwa
\dialx GO
\xn il dort la tête vers l'intérieur de la maison et les pieds vers la porte
\sn 5
\is grammaire_suff_directionnel
\ps DIR
\ge dans/vers le passé
\xv e tho kutra-da na ni tree ni pe-waaça
\xn le sang a coulé dans le passé pendant les jours de lutte
\et *sake
\el POc
\dt 22/Feb/2025

\lx da ?
\ph nda
\dialx GOs PA BO
\va ta ?
\dialx GO(s)
\is grammaire_interrogatif
\ps INT
\ge quoi ?
\xv da nye hu-çö ?
\dialx GO
\xn qu'est-ce qui t'a mordu ?
\xv e hu-çö xo da ? ; hu-çö da ?
\dialx GO
\xn qu'est-ce qui t'a mordu ?
\xv da ? thoomwã o êmwê ?
\dialx GO
\xn qu'est-ce ? une fille ou un garçon ?
\xv i mõgu ni da ?
\dialx GO
\xn que fait-il ? (quel travail?)
\xv da i çö ?
\dialx GO
\xn qu'est-il pour toi ? (dans la parenté)
\xv da yaza-çö ?
\dialx GO
\xn quel est ton nom?
\xv yaza da "chö" ?
\dialx GO
\xn que signifie "chö"? (lit. c'est le nom de quoi "chö")?
\xv ba-thu da ?
\xn à quoi ça sert ?
\xv çö thrõbo ni da ?
\dialx GO
\xn quand es-tu né ?
\xv da ê? da nyè ?
\dialx GO
\xn qu'est-ce que c'est?
\xv da phwe-mèèvwu i çö ?
\dialx GO
\xn quel est ton clan ?
\xv pò da nye ?
\dialx GO
\xn c'est un fruit de quoi ?
\xv i nõõli da ?
\dialx PA
\xn que regarde-t-il?
\xv çö kiiga da ?
\dialx GO
\xn de quoi ris-tu ?
\cf ra ?; za ?
\ce quoi ? (position postposée)
\cf dajâ?
\ce quoi ?
\et *sapa
\eg what?
\el POc
\dt 20/Feb/2025

\lx daal
\dialx PA
\is bananier
\ps n
\ge banane
\dn non comestible, dont la sève rouge foncé est utilisée comme peinture de danse
\dt 08/Feb/2025

\lx daawe
\ph nda:we
\dialx PA BO
\is vent
\ps n
\ge vent froid du sud-ouest ; alizés du sud-ouest
\dt 02/Feb/2019

\lx dabò
\dialx GOs
\is navigation
\ps n
\ge flotteur de balancier
\dt 26/Jan/2019

\lx dada
\ph danda
\dialx GOs PA
\is insecte
\ps n
\ge cigale (très petite)
\dt 29/Jan/2019

\lx dagi
\dialx GOs
\va daginy
\dialx WEM BO PA
\sn 1
\is oiseau
\ps n
\ge lève-queue ; passereau
\sc Rhipidura spilodera verreauxi
\scf Rhipiduridés
\gb Streaked Fantail
\sn 2
\is société_organisation
\ps n
\ge messager du Grand Chef
\dt 21/Feb/2025

\lx dagi phwamwa
\dialx GOs
\va daginy pwemwa
\dialx WEM
\is société_organisation
\ps n
\ge médiateur de la chefferie
\dt 05/Jan/2023

\lx dagony
\dialx PA
\is insecte
\ps n
\ge libellule
\dt 29/Jan/2019

\lx dalaèèn
\dialx BO
\ph nda:laɛ:n
\va daalèn ; dalaèn
\dialx BO
\va dalaan
\dialx PA
\is société
\ps n
\ge étranger ; blanc ; européen
\dt 13/Oct/2021

\lx daluça mã
\dialx GOs
\va ba-òginen
\dialx PA BO
\is coutumes_objet
\ps n
\ge bouquet de plante (cérémonie de deuil)
\dn contenant une monnaie et entouré d'un lien de paille, il est lancé au sol et constitue le dernier geste coutumier pour le défunt
\dt 08/Feb/2025

\lx dame
\dialx GOs BO
\va dam
\dialx BO
\is action_corps
\ps v
\ge tasser (en frappant)
\bw damer (FR)
\se ba-dam
\sge outil pour damer
\cf kha
\ce écraser (avec le pied) ; appuyer
\dt 22/Feb/2025

\lx dao
\ph ndao
\dialx GOs
\va daòn
\ph ndaɔn
\dialx PA BO
\sn 1
\is temps_atmosphérique
\ps n
\ge vapeur
\ge brouillard
\sn 2
\is fonctions_naturelles
\ps n
\ge haleine ; buée
\dn ou vapeur faite par le souffle
\dt 08/Feb/2025

\lx dauliõ
\dialx PA
\va dawuliõ
\dialx BO
\is vent
\ps n
\ge tourbillon (d'air)
\dt 28/Jan/2019

\lx de
\dialx GOs BO PA
\ph nde
\sn 1
\is instrument
\ps n
\ge fourchette
\sn 2
\is pêche
\ps n
\ge sagaie de pêche
\dt 02/Feb/2019

\lx de-
\dialx GOs PA
\ph nde
\is classificateur_numérique
\ps CLF.NUM
\ge main de bananes
\xv de-xè, de-tru, de-kò, etc.
\xn une, deux, trois main(s) de bananes, etc.
\cf thò-xè
\ce dénombre les régimes
\dt 22/Feb/2025

\lx dè
\ph dɛ
\dialx GOs
\va dèn
\ph dɛn
\dialx BO PA
\sn 1
\is déplacement_moyen
\ps n
\ge chemin ; sentier ; route
\xv dèè-nu
\dialx GO
\xn mon chemin
\xv dèè we gò
\dialx GO
\xn chenal/aqueduc en bambou
\se phè dè-khibwaa
\dialx GO
\sge prendre un raccourci (lit. chemin coupé)
\se ce-dèn
\dialx BO PA
\sge grande route
\sn 2
\is société_organisation
\ps n
\ge chemin coutumier
\xv pe-dè îbi
\dialx GO
\xn notre chemin (relation de parenté duelle réciproque par alliance)
\et *(n)sala(n), *njala(n)
\el POc
\dt 08/Nov/2021

\lx deang
\dialx WEM PA BO
\is pêche
\ps n
\ge épuisette ; haveneau ; nasse (à crevette)
\dt 26/Aug/2021

\lx de-du
\dialx GOs
\va degu, dego
\dialx BO [Corne]
\is maison
\ps n
\ge toiture en paille
\dn dite "racines dehors"
\xv e yaa de-du
\xn il fait la toiture en paille racines vers l'extérieur
\dt 08/Feb/2025

\lx dee
\hm 1
\dialx PA BO
\is corps
\ps n
\ge côtes
\xv dee-n
\dialx PA
\xn ses côtes
\xv deein
\dialx BO
\xn ses côtes
\dt 24/Jan/2019

\lx dee
\hm 2
\dialx GOs
\va deeny
\dialx PA BO
\is anguille
\ps n
\ge anguille de creek (rouge)
\dt 29/Jan/2019

\lx dèè
\dialx GOs
\is instrument
\ps n
\ge roue
\xv dèè-loto
\xn la roue de la voiture
\dt 27/Jan/2019

\lx dee-chaamwa
\dialx GOs BO
\is bananier
\ps n
\ge main de banane
\dt 29/Jan/2019

\lx deeny
\hm 1
\dialx BO PA
\is vent
\ps v
\ge vent du sud
\dt 28/Jan/2019

\lx deeny
\hm 2
\dialx PA BO
\is anguille
\ps n
\ge anguille de creek et de forêt
\dt 29/Jan/2019

\lx dèè-we
\dialx GOs BO PA
\is cultures
\ps n
\ge conduite d'eau pour les cultures ; aqueduc d'irrigation
\ge fossé d'écoulement |lx{PA}
\dn sur le bord du champ d'igname
\se dèè-we gò
\dialx BO
\sge canalisation en bambou
\cf pwang
\dialx BO
\dt 08/Feb/2025

\lx degam
\dialx BO
\ph ndeŋgam
\is mollusque
\ps n
\ge huître
\nt selon Corne, BM ; non vérifié
\dt 26/Mar/2022

\lx dei
\dialx PA BO [Corne]
\va deei
\is action_corps
\ps v
\ge couper (faire)
\cf threi
\dt 25/Jan/2019

\lx dèl
\dialx PA
\is arbre
\ps n
\ge arbre et bois de santal
\sc Santalum austro-caledonicum
\scf Santalacées
\dt 27/Aug/2021

\lx dèn kha-jöe
\dialx PA
\is déplacement
\ps n
\ge raccourci
\dt 23/Jan/2018

\lx dèxavi
\ph ndɛɣavi
\dialx GOs
\va dèè-xavi
\dialx BO [Corne]
\is vent
\ps n
\ge tourbillon
\dt 06/Feb/2019

\lx di
\ph ndi
\dialx GOs PA BO
\is plantes
\ps n
\ge cordyline (symbole masculin)
\sc Cordyline fruticosa (L.) A. Chev.
\scf Agavacées
\et *siRi, *jiRi
\eg cordyline
\el POc
\ea Ross
\dt 27/Aug/2021

\lx dibee
\ph ndibe:
\dialx GOs
\is nourriture
\ps n
\ge beurre
\bw du beurre (FR)
\dt 26/Jan/2019

\lx didi
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v.stat
\ge profond
\nt selon Corne
\sn 2
\is couleur
\ps v.stat
\ge vert
\nt non vérifié
\dt 03/Feb/2025

\lx digo
\ph ndiŋgo
\dialx GOs WEM WE BO
\sn 1
\is configuration
\ps n
\ge fourche (arbre)
\ge bananes jumelles
\dn elles sont dans une même enveloppe
\sn 2
\is corps_animaux
\ps n
\ge cornes |dialx{WEM BO}
\xv digo bwaa-n
\xn ses cornes
\et *saŋa
\el POc
\ea Blust
\dt 08/Feb/2025

\lx digöö
\ph ndiŋgω:
\dialx GOs
\va digoony
\ph ndiŋgo:ɲ
\dialx BO PA
\is plantes
\ps n
\ge "cassis" (arbuste épineux)
\ge épine
\dt 26/Aug/2021

\lx dii nu
\ph di: ɳu
\dialx GOs BO
\is arbre_cocotier
\ps n
\ge fibre de coco
\dt 17/Aug/2021

\lx diiri
\dialx GOs WEM
\is oiseau
\ps n
\ge cagou
\sc Rhynochetos jubatus
\scf Rhynochetidés
\dt 27/Aug/2021

\lx diiru
\dialx BO
\is anguille
\ps n
\ge anguille (tachetée bleu et blanc)
\nt selon Corne
\dt 27/Mar/2022

\lx diixe
\dialx GOs PA
\is action
\ps v
\ge ceindre ; serrer ; attacher (avec une corde) ; tendre (corde)
\dt 29/Jan/2019

\lx dilee
\dialx PA
\is maison
\ps v
\ge crépir ; faire un mur en torchis
\xv la dilee mwa
\dialx GO
\xn ils ont faire le mur en torchis ; ils ont crépi la maison
\dt 22/Feb/2025

\lx dili
\ph ndili
\dialx GOs BO PA
\is terrain_terre
\ps n
\ge terre ; torchis
\se dili mii
\sge terre rouge
\se dili phozo
\dialx GO
\sge terre blanche ; chaux
\se dili pulo
\dialx BO
\sge terre blanche ; chaux
\se bu dili
\dialx BO PA
\sge butte de terre
\se nõ dili
\sge sous terre
\cf dilee
\ce crépir ; faire (un mur) en torchis
\dt 22/Feb/2025

\lx dili baa
\dialx GOs
\va dili baang
\dialx PA BO
\is terrain_terre
\ps n
\ge terre noire
\dt 19/Oct/2021

\lx dimòm
\dialx BO [BM]
\ph dimɔm
\va dimwò-n
\dialx BO
\is fonctions_naturelles
\ps n
\ge morve
\xv dimwò-n
\xn sa morve
\cf têi
\dialx GOs
\ce morve
\dt 10/Jan/2022

\lx dimwã
\hm 1
\ph dimwɛ̃
\dialx GOs PA BO
\is igname
\ps n
\ge igname sauvage
\dn pour préparer une purée de cette igname râpée, il faut râper le |lx{dimwa} dans l'eau de la rivière (voir |lx{bwevòlò}) pour en enlever l'amertume, puis on recueille la chair lavée dans un panier |lx{keruau}; elle est ensuite séchée puis cuite  et se déguste de préférence sucrée
\sc Dioscorea bulbifera
\scf Dioscoréacées
\dt 08/Feb/2025

\lx dimwã
\hm 2
\dialx GOs
\is poisson
\ps n
\ge poisson-perroquet
\dt 29/Jan/2019

\lx dimwãã ko
\dialx GOs
\va dimwãã diwe-ko
\dialx BO PA
\is corps_animaux
\ps n
\ge crête de coq
\dt 24/Jan/2019

\lx diva
\dialx GO
\va diia
\dialx PA BO
\sn 1
\is mollusque
\ps n
\ge huître perlière
\dn servait à couper l'igname (selon Charles Pebu-Polae)
\sc Pinctada margaritifera
\scf Ptériidés
\sn 2
\is instrument
\ps n
\ge couteau pour igname 
\dn selon [Dubois]
\dt 20/Feb/2025

\lx dive
\dialx BO
\is corps
\ps n
\ge hanche
\xv divè-n
\dialx BO
\xn sa hanche
\nt selon Haudricourt, Corne ; non vérifié
\dt 26/Mar/2022

\lx divhii
\dialx GOs
\is oiseau
\ps n
\ge bécasse ; courlis corlieu
\dt 27/Aug/2021

\lx dixa-cee
\dialx GOs PA
\is plantes_partie
\ps n
\ge résine ; sève d'arbre
\xv thu dixa-n
\dialx PA
\xn y avoir de la sève
\et *suRuq
\el POC
\dt 08/Nov/2021

\lx dixa-nu
\dialx GOs PA BO
\va dika-nu
\dialx GOs BO
\is arbre_cocotier
\ps n
\ge lait de coco ; huile de coco
\dt 08/Sep/2021

\lx dixoo
\dialx GOs BO
\is corps_animaux
\ps n
\ge cornes
\xv dixoo bwa-n
\dialx PA
\xn ses cornes
\xv dixoo bwa cèvèro
\dialx PA
\xn les cornes du cerf
\se dixoo drube
\dialx GO
\va dixoo cèvèro
\dialx BO
\sge cornes de cerf
\dt 08/Jan/2022

\lx do
\ph ndo
\dialx GOs PA BO
\sn 1
\is armes
\ps n
\ge sagaie
\se do teò
\sge sagaie de pêche
\se do we-xè
\sge une sagaie
\xv doo-n
\dialx BO PA
\xn sa sagaie
\sn 2
\is jeu
\ps n
\ge figure de jeu de ficelle "la sagaie"
\et *sao(t)
\el POc
\dt 13/Sep/2021

\lx dö
\dialx BO
\is corps
\ps n
\ge côtes
\xv dö-n
\xn sa côte
\dt 24/Jan/2019

\lx do-a
\ph ndo.a
\dialx GOs
\va do-al
\dialx PA
\is astre
\ps n
\ge rayon de soleil
\cf do
\ce sagaie
\dt 06/Feb/2019

\lx doau
\dialx PA
\is crustacés
\ps n
\ge crabe
\dn de creek et de forêt, très petit, mesure quelques cm
\dt 20/Feb/2025

\lx döbe
\ph dωmbe
\dialx GOs WEM BO
\va dube
\ph dumbe
\dialx GA BO PA
\is discours
\ps v
\ge dire des bêtises ; dérailler ; faire qqch. sans sérieux ; faire des bêtises
\dt 29/Mar/2022

\lx do-bubu
\dialx BO
\is topographie
\ps n
\ge plaine verte
\nt selon Corne
\dt 26/Mar/2022

\lx dobwa
\dialx BO
\is taro
\ps n
\ge taro d'eau (clone)
\nt selon Dubois
\dt 27/Mar/2022

\lx do-de
\ph dode
\dialx GOs
\is armes
\ps n
\ge sagaie à trois pointes (trident)
\dn (lit. sagaie fourchette)
\dt 27/Mar/2022

\lx do-jitrua
\dialx GOs
\is armes
\ps n
\ge flèche
\dt 07/Oct/2018

\lx dòlògò
\ph dɔlɔŋgɔ
\dialx GOs
\va dològòm
\dialx PA BO
\is plantes
\ps n
\ge brède pariétaire ; feuille d'Aramanthus ; épinard (sorte d')
\dn herbe à feuilles comestibles
\sc Amaranthus interruptus R. Br.
\scf Amaranthacées
\dt 08/Feb/2025

\lx dom
\dialx PA
\va dum
\dialx WE WEM
\is caractéristiques_objets
\ps v
\ge pointu
\dt 26/Jan/2019

\lx dòmã
\ph ndɔmã
\dialx PA BO
\is couleur
\ps v
\ge noir
\cf baa
\dialx GOs
\ce noir
\dt 10/Jan/2022

\lx dòmògèn
\ph dɔmɔŋgɛn
\dialx BO [BM]
\is oiseau
\ps n
\ge râle de forêt (gros oiseau)
\dt 16/Aug/2021

\lx dõńi
\dialx GOs PA BO
\is grammaire_locatif
\ps LOC
\ge parmi ; entre
\xv i phe aa-xe na ni dõni la-ã ko
\dialx BO
\xn il a pris l'un parmi ces poulets
\dt 16/Feb/2025

\lx dònò
\ph dɔɳɔ, dɔɳɔ̃
\dialx GOs
\va dòòn
\ph ndɔ:n
\dialx PA BO
\is astre
\ps n
\ge ciel ; cieux
\dn a un sens religieux en |dialx{PA}
\se dòò-va
\dialx BO
\sge ciel clair, dégagé
\se dòò ni pwa
\dialx BO
\sge ciel nuageux
\cf phwa
\dialx PA
\ce ciel
\dt 08/Feb/2025

\lx dõõgo
\ph dɔ̃:ŋgo
\dialx GOs
\is crustacés
\ps n
\ge crabe vide
\dt 29/Jan/2019

\lx dòòla
\dialx BO
\is son
\ps n
\ge bruissement
\xv dòòla dòò-cee
\dialx BO
\xn le bruissement des feuilles
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx döölia
\ph dω:lia
\dialx GOs PA BO
\va dolia
\dialx PA BO
\is plantes_partie
\ps n
\ge épine de
\se döölia orã
\sge épine d'oranger
\et *suRi
\el POc
\dt 06/Feb/2019

\lx döölia thra
\dialx GOs
\is plantes_partie
\ps n
\ge épines ; piquants de la nervure centrale de la feuille de pandanus
\dt 22/Feb/2025

\lx doo-pe
\ph do:pe, dω:pe
\dialx GOs PA
\is corps_animaux
\ps n
\ge dard de la raie
\dn (lit. sagaie de la raie)
\dt 09/Feb/2025

\lx dòò-phwa
\dialx PA
\is corps
\ps n
\ge lèvres
\dn (lit. feuille-bouche)
\xv dòò-phwa-n
\xn ses lèvres
\dt 22/Feb/2025

\lx doori
\dialx PA BO
\is maison
\ps n
\ge bord inférieur de la toiture
\dn dépasse de la sablière
\ge premier rang de paille
\dt 08/Feb/2025

\lx dopweza
\dialx PA BO
\sn 1
\is coutumes_objet
\ps n
\ge feuille de bananier qui enveloppe la monnaie |lx{weem}
\dn selon [Charles Pebu-Polae]
\ge monnaie coutumière
\dn selon [Dubois], 1 |lx{dopweza} de 2,5 m vaut 20 francs
\sn 2
\is bananier
\ps n
\ge feuille de bananier (voir |lx{pweza})
\cf pwãmwãnu ; weem; yòò
\dt 20/Feb/2025

\lx dou
\hm 1
\dialx GOs PA
\is échanges
\ps n
\ge don ; offrande
\ge tas de vivres
\dn à donner dans les échanges
\xv dou-nu
\xn mes dons
\xv lha ra u phaò dou-la
\dialx PA
\xn elles préparent les tas de vivres
\dt 08/Feb/2025

\lx dou
\hm 2
\dialx GOs PA
\va deü
\dialx BO
\sn 1
\is corps
\ps n
\ge enveloppe
\ge enveloppe (corporelle)
\xv dowa hêgi (< dou-a hêgi)
\dialx PA
\xn l'enveloppe de la monnaie
\sn 2
\is fonctions_naturelles_animaux
\ps n
\ge dépouille de mue
\xv dou-n
\dialx PA
\xn son enveloppe
\xv deü pwaji
\dialx BO
\xn carapace de crabe (vide)
\xv deü-n
\dialx BO
\xn carapace de crabe (vide)
\dt 23/Jan/2022

\lx dö-vwiã
\ph dωβiã
\dialx GOs
\va dö-piã
\dialx GO(s)
\va du-piã
\dialx BO
\sn 1
\is oiseau
\ps n
\ge Diamant psittaculaire ; "rouge-gorge"
\sc Erythrura psittacea
\scf Estrildidés
\gb Red-throated Parrotfinch
\sn 2
\is oiseau
\ps n
\ge colibri ; "Sucrier écarlate" ; "cardinal"
\sc Myzomela cardinalis et Myzomela dibapha
\scf Méliphagidés
\gb New Caledonian Myzomela
\dt 22/Feb/2025

\lx du
\hm 1
\ph ndu
\dialx GOs BO PA
\sn 1
\is corps
\ps n
\ge os
\se du-kòò-je
\dialx GO
\sge son tibia
\se duu bwa-n
\dialx BO
\sge crâne
\xv duu-n
\dialx PA
\xn son os
\sn 2
\is corps
\ps n
\ge dos
\xv e khinu duu-nu
\dialx GO
\xn j'ai mal au dos
\xv e ciia duu-je du mu ca la ẽnõ
\dialx GO
\xn il danse le dos (tourné) derrière les enfants
\se ki-duu-ny
\dialx PA
\sge ma colonne vertébrale
\sn 3
\is poisson
\ps n
\ge arête (poisson)
\se du-nõ
\sge arête de poisson
\sn 4
\is cultures
\ps n
\ge tuteur à igname (grand)
\se du-kui
\sge tuteur à igname
\cf tha
\ce tuteur (petit)
\et *suRi
\el POc
\dt 09/Jul/2022

\lx du
\hm 2
\ph ndu
\dialx GOs PA
\sn 1
\is grammaire_suff_directionnel
\ps DIR
\ge en bas
\se ã-du !
\sge descends !
\sn 2
\is grammaire_suff_directionnel
\ps DIR
\ge vers le nord ; vers l'ouest
\sn 3
\is grammaire_suff_directionnel
\ps DIR
\ge vers la mer ; en aval
\sn 4
\is grammaire_suff_directionnel
\ps DIR
\ge à l'extérieur de la maison ; vers la porte
\xv e mããni kòò-je du ni phwee-mwa
\dialx GO
\xn il dort les pieds vers la porte
\et *(n)sipo
\el POc
\dt 22/Feb/2025

\lx du dròò-chaamwa
\ph ndu ɖɔ:
\dialx GOs
\is bananier
\ps n
\ge nervure dorsale de la feuille de bananier
\dt 29/Jan/2019

\lx dua
\dialx BO PA
\is topographie
\ps n
\ge grotte ; caverne
\ge ravinement sur les routes |dialx{PA}
\nt non vérifié
\dt 27/Mar/2022

\lx dubila
\dialx GOs PA BO WEM WE
\va döbela, dubela
\dialx GA GO(s)
\is soin
\ps n
\ge peigne
\xv dubilaa-nu
\dialx GO
\xn mon peigne
\xv dubila-n
\dialx PA
\xn son peigne
\se döbela gò
\sge peigne en bambou
\dt 02/Feb/2019

\lx du-bwò
\ph ndumbwɔ
\dialx GOs PA BO
\va duu-bò
\dialx BO
\sn 1
\is couture
\ps n
\ge aiguille
\dn (lit. os de roussette)
\sn 2
\is pêche
\ps n
\ge navette à filet
\et *(n)saRu
\el POc
\dt 09/Feb/2025

\lx du-hêgi
\dialx GOs
\is coutumes_objet
\ps n
\ge ossature de la monnaie
\dt 22/Aug/2021

\lx du-hoogo
\dialx PA
\va do-hoogo
\is topographie
\ps n
\ge ligne de crête ; pente de la montagne
\dn (lit. os de la montagne)
\dt 08/Feb/2025

\lx du-kai
\dialx GOs
\ph ndukai
\va du-kaè-n
\ph du-kaɛn
\dialx PA BO
\is corps
\ps n
\ge colonne vertébrale
\xv du-kai-nu
\dialx GO
\xn ma colonne vertébrale
\dt 02/Jan/2022

\lx du-kò
\dialx GOs
\is corps
\ps n
\ge tibia
\dt 24/Jan/2019

\lx du-mi
\dialx GOs PA
\is grammaire_suff_directionnel
\ps DIR
\ge approcher (s') en descendant
\xv a mwã-è wo Thonòòl ra u whili-je-mwã-mi, [...] khõbwe : "a-duu-mi !"
\dialx PA
\xn alors, Thonòòl part, l'amène ici et dit "sors et viens ici!"
\dt 22/Feb/2025

\lx dra
\hm 1
\ph nɖa
\dialx GOs
\va da
\ph nda
\dialx BO PA
\is feu
\ps n
\ge cendres ; poudre
\ge suie
\xv dra-(w)a yaai
\dialx GO
\xn les cendres du feu
\xv dra-(w)a dröö
\dialx GO
\xn la suie de la marmite
\ng forme déterminée: |lx{dra-w-a}
\gt poudre de
\et *ɖapu
\el POc
\dt 12/Jul/2022

\lx dra
\hm 2
\ph ɖa
\dialx GOs
\is son
\ps v
\ge éclater
\xv e dra dèè loto
\xn la roue de la voiture a éclaté
\ng v.t. |lx{drale}
\dt 05/Jan/2022

\lx draa
\hm 1
\ph ɖa:
\dialx GOs
\va daa
\dialx PA BO
\is topographie
\ps n
\ge plaine
\cf bwadraa
\ce plaine; plateau
\dt 28/Jan/2019

\lx draa
\hm 2
\ph ɖa:
\dialx GOs
\va daa
\dialx BO
\sn 1
\is grammaire_modalité
\ps MODIF anticausatif
\ge action spontanée (sans cause)
\xv e draa khõbwe
\dialx GOs
\xn il a dit cela sans savoir ; il a inventé
\xv e draa kalu
\dialx GOs
\xn il est tombé tout seul
\xv la daa pe-kule pò-mãã
\dialx PA
\xn les mangues tombent d’elles-mêmes (toutes seules)
\xv e draa nee
\dialx GOs
\xn il l'a fait tout seul ; il l'a fait sans penser au résultat
\xv i daa nee
\dialx BO
\xn il l'a fait tout seul ; il l'a fait sans penser au résultat
\sn 2
\is grammaire_modalité
\ps MODIF INTENS
\ge seul ; de/par soi-même
\xv nu draa a khila-je
\dialx GO
\xn je suis parti moi-même la chercher
\xv e draa uça !
\dialx GOs
\xn il est revenu tout seul
\xv nu daa nye
\dialx BO
\xn je l'ai fait tout seul
\cf draa pu nee
\ce volontairement
\dt 22/Feb/2025

\lx draa
\hm 3
\ph ɖa:
\dialx GOs
\va daa
\dialx PA BO
\sn 1
\is grammaire_contraste
\ps PRE.VB de contraste
\ge contraste
\dn indique une opposition entre des actions, des agents
\xv çö yu (ê)nè, ma nu dra(a) ã-du bòli
\dialx GOs
\xn reste ici, car je vais descendre là-bas en bas
\xv nòme kavwö huu pwe, çö dra(a) ã-du
\dialx GOs
\xn si cela ne mord pas, vas donc plus bas
\sn 2
\is grammaire_contraste
\ps PRE.VB
\ge tour de (au)
\xv draa içö !
\dialx GOs
\xn à ton tour !
\xv çö dra(a) mõgu, ma nu tree-çãnã
\dialx GOs
\xn à ton tour de travailler, car je vais me reposer
\xv i daa khõbwe !
\dialx PA
\xn il a fini par avouer/par le dire !
\xv cu khõbwe cu Teã-ma je-na, daa bwa-m-du bwabu
\dialx PA
\xn tu dis que tu es le Grand Chef comme cela, alors que tu as la tête vers le bas
\cf mwaa içö !
\ce à ton tour !
\dt 22/Feb/2025

\lx draa pu nee
\ph ɖa: puɳe:
\dialx GOs
\is grammaire_modalité
\ps ADV
\ge faire exprès, volontairement
\dn dénote une action spontanée, inattendue, sans cause externe ; le sens peut-être contrastif ou adversatif
\xv e draa pu nee vwo zòi hii-je
\dialx GO
\xn elle s'est coupé le bras toute seule
\xv e za draa zòi
\dialx GO
\xn elle s'est coupée
\xv e kaavwö draa pu nee
\dialx GO
\xn il ne l'a pas fait de lui-même (ou) volontairement
\xv li (za) draa pu nee xo/vwo phãde-nu
\dialx GO
\xn ils me l'ont montré d'eux-mêmes
\xv kavwö li draa pu nee
\dialx GO
\xn ils ne l'ont pas fait d'eux-même (ou) volontairement
\dt 20/Feb/2025

\lx draaçi
\ph ɖa:dʒi
\dialx GOs
\va dacim
\dialx PA
\is vent
\ps n
\ge vent alizé du sud-ouest
\dt 28/Jan/2019

\lx draadro
\ph ɖa:ɖo
\dialx GOs
\va dadeng
\dialx BO (Corne)
\va daadòng
\dialx BO (Dubois, Corne)
\sn 1
\is arbre
\ps n
\ge arbuste de bord de mer ; gattilier
\dn ses feuilles sont grises ; il est utilisé comme plante de guerre
\sc Vitex trifolia
\scf Lamiacées
\sn 2
\is arbre
\ps n
\ge arbuste 
\dn utilisé comme produit anti-puce
\sc Vitex rotundifolia
\scf Lamiacées
\dt 22/Feb/2025

\lx draalai
\dialx GOs
\ph ɖa:lai
\is société
\ps n
\ge Blanc ; européen
\dn (lit. poudre riz)
\dt 08/Feb/2025

\lx draańi
\ph ɖa:ni
\dialx GOs
\is poisson
\ps n
\ge "bec de canne"
\sc Lethrinus sp.
\scf Lethrinidés
\dt 27/Aug/2021

\lx draa-phwalawa
\dialx GOs
\is nourriture
\ps n
\ge farine
\dn (lit. poudre pain)
\dt 08/Feb/2025

\lx draba
\ph ɖaba
\dialx GOs
\is eau_topographie
\ps n
\ge îlot d'alluvion
\dt 29/Jan/2019

\lx drabu
\ph ɖabu
\dialx GOs
\is reptile_marin
\ps n
\ge tortue de mer "grosse tête"
\dt 29/Jan/2019

\lx drale
\hm 1
\ph ɖale
\dialx GOs
\va dale, daale
\dialx PA BO
\is action_corps
\ps v
\ge fendre ; casser
\se khi-drale
\sge fendre
\et *saRi
\el POc
\cf daale
\dialx langue nêlêmwa
\ce fendre ; casser
\dt 10/Jan/2022

\lx drale
\hm 2
\ph ɖale
\dialx GOs
\va daala
\dialx BO [Corne]
\is crustacés
\ps n
\ge crabe de rivière
\dt 02/Feb/2019

\lx drapwê
\dialx GOs
\va damwê
\dialx GOs
\is étapes_vie
\is parenté
\ps n
\ge veuf ; veuve
\xv drapwela Pwacili
\xn veuf du clan Pwacili
\dt 26/Aug/2021

\lx drau
\ph nɖa.u
\dialx GOs
\va dau
\ph nda.u
\dialx PA BO
\is eau_topographie
\ps n
\ge île ; platier
\et *sakaRu
\el POc
\dt 19/Feb/2025

\lx drava-dröö
\dialx GOs
\is préparation_aliments
\ps n
\ge vapeur de la marmite
\dt 02/Feb/2019

\lx drawa yaai
\ph ɖawa ya:i
\dialx GOs
\is feu
\ps n
\ge cendres du feu
\cf dra
\ce cendres
\dt 23/Aug/2021

\lx drawa-dröö
\dialx GOs
\va dawa doo
\dialx BO PA
\is feu
\ps n
\ge suie sur la marmite
\cf dra
\ce cendres
\dt 08/Oct/2021

\lx drawalu
\ph 'nɖawalu
\dialx GOs
\is plantes
\ps n
\ge herbe ; pelouse
\dt 29/Jan/2019

\lx dra-wawe
\ph nɖawawe
\dialx GOs
\va dra-wapwe
\dialx arch.
\va da-whaawe
\dialx BO (Corne)
\is arbre
\ps n
\ge érythrine "peuplier"
\sc Erythrina pyramidalis
\scf Fabacées
\et *(n)ɖaɖap
\el POc
\dt 30/Mar/2022

\lx dre
\hm 1
\ph ɖe
\dialx GOs WEM
\is maison
\ps n
\ge lianes (pour la construction)
\dn servant à attacher le bois de la charpente
\dt 08/Feb/2025

\lx dre
\hm 2
\ph ɖe
\dialx GOs
\is poisson
\ps n
\ge poisson "cochon"
\sc Gazza minuta et Leiognathus equulus
\scf Leiognathidés
\dt 27/Aug/2021

\lx dree
\ph ɖe:
\dialx GOs
\va dèèn
\ph ndɛ:n
\dialx BO PA
\is vent
\ps n
\ge vent ; air ; atmosphère ; cyclone
\xv i pha dèèn
\dialx PA
\xn le vent souffle
\xv dèèn nî we-za
\dialx PA
\xn brise de mer
\se dree xa wîî
\dialx GO
\sge vent fort
\se pa-dree
\dialx GO
\sge coup de vent ; cyclone
\se bwa dree
\dialx GO
\sge au vent ; vent debout
\dt 22/Feb/2025

\lx dree bwa pwa
\ph ɖe:
\dialx GOs
\is vent
\ps n
\ge vent annonciateur de pluie
\dt 20/Mar/2023

\lx dree kaze
\dialx GOs
\is vent
\ps n
\ge brise de mer
\dt 16/Aug/2021

\lx dree ni we
\ph ɖe:
\dialx GOs
\is vent
\ps n
\ge vent de mer
\dt 26/Aug/2021

\lx dreebò
\dialx GOs
\is temps_atmosphérique
\ps n ; v.stat.
\ge nuageux ; gros nuage
\dt 28/Jan/2019

\lx dree-bwamõ
\ph ɖe:
\dialx GOs
\va dèèn-bwa-mòl
\dialx PA BO [Corne]
\is vent
\ps n
\ge vent de terre ; brise de terre ; vent d'est
\dt 16/Aug/2021

\lx dree-bwava
\ph ɖe:
\dialx GOs
\is vent
\ps n
\ge vent alizé du sud-est
\dt 27/Mar/2022

\lx drele-ma-drele
\ph ɖele
\dialx GOs
\va dele-ma-dele
\dialx BO
\sn 1
\is parenté
\ps n
\ge arrière-arrière-petits-enfants |dialx{GOs}
\sn 2
\is société_organisation
\ps n
\ge ascendants (de la lignée) |dialx{BO}
\xv dele-ma-dele a Mateo
\dialx BO
\xn les ascendants de Mateo [Corne]
\dt 20/Feb/2025

\lx drewaa
\ph ɖewa:
\dialx GOs
\va dea
\dialx WE
\va deang
\dialx PA BO
\ph ndea:ŋ
\is pêche
\ps n
\ge nasse
\dn en forme de poche pour fouiller les berges
\ge épuisette à crevettes
\se drewaa kula
\dialx GO
\sge épuisette à crevette
\se drewaa pwaji
\sge épuisette à crabe
\cf kevalu
\ce épuisette (plus grande que |lx{deaang})
\dt 08/Feb/2025

\lx driluu
\ph ɖilu:
\dialx GOs
\va dilu, dilo
\dialx PA BO
\va diluuc
\dialx BO (Corne)
\is plantes
\ps n
\ge hibiscus
\sc Hibiscus rosa sinensis L.
\scf Malvacées
\dt 27/Aug/2021

\lx drò
\dialx GOs
\ph ɖɔ
\va dòny
\dialx PA BO
\ph dɔɲ
\is oiseau
\ps n
\ge buse ; émouchet ; faucon
\dn petit faucon avec des panaches rougeâtres sous le ventre ; nom donné à deux oiseaux
\sc Accipiter fasciatus vigilax
\scf Accipitridés
\sc Circus approximans approximans
\scf Accipitridés
\gb Brown Goshawk ou Swamp Harrier
\dt 27/Aug/2021

\lx drõbö
\ph ɖɔ̃:bω
\dialx GOs
\va dòbo
\dialx BO PA
\sn 1
\is mouvement
\ps v ; n
\ge éboulement ; écrouler (s')
\xv e drõbö mwa
\dialx GO
\xn la maison s'est écroulée
\xv e drõbö dili
\dialx GO
\xn la terre s'éboule
\xv nu drõbö-ni mwa
\dialx GO
\xn j'ai démoli la maison
\sn 2
\is topographie
\ps n
\ge érosion ; ravinement ; terre ravinée
\xv e drõbö hoogo
\dialx GO
\xn la montagne se ravine/s'érode
\dt 22/Feb/2025

\lx drõgò
\ph ɖɔ̃ŋgɔ
\dialx GOs WEM
\va dõgò
\dialx PA
\sn 1
\is coutumes_objet
\ps n
\ge masque
\dn comprenant le tissu/vêtement qui accompagne le masque
\sn 2
\is maison
\ps n
\ge chambranles
\ge sculpture faîtière
\dt 22/Oct/2021

\lx dròò
\ph ɖɔ:
\dialx GOs
\va dòò
\dialx PA BO
\is plantes_partie
\ps n
\ge feuille
\se dròò-cee
\dialx GO
\sge feuille d'arbre ; rameaux
\se dòò cee
\dialx PA
\sge feuilles; "médicament"; "boucan"
\se dròò kò
\dialx GO
\sge petite plante à feuilles comestibles
\se dròò nu
\sge palme de cocotier
\xv dòò-n
\dialx PA BO
\xn sa feuille
\et *nɖau(n)
\el POc
\dt 22/Feb/2025

\lx dröö
\ph ɖω:
\dialx GOs
\va doo
\ph ndo:
\dialx BO PA
\is ustensile
\ps n
\ge marmite
\dn originellement en poterie
\ge argile de poterie
\xv phuu dröö-a-wa !
\dialx GO
\xn retirez votre marmite de nourriture (du feu) !
\xv doo-a-n
\dialx BO
\xn sa marmite
\se dröö-a kui
\dialx GO
\sge une marmite d'ignames
\se dröö-a nõ
\dialx GO
\sge une marmite de poisson
\se doo-togi
\dialx BO
\sge marmite en fonte
\se kivwa doo-a-ny
\dialx PA
\sge le couvercle de ma marmite
\se whaa doo-a-nõ
\dialx PA
\sge une grande marmite de poisson
\xv whaa dooa-ny (a) nõ
\dialx PA
\xn ma grande marmite de poisson
\ng forme déterminée: |lx{doo-a-}
\et *daRoq
\eg argile (clay)
\el POc
\dt 08/Feb/2025

\lx dròò ê
\ph ɖɔ: ê
\dialx GOs
\va dòò èm
\dialx PA
\is plantes_partie
\ps n
\ge feuille de canne à sucre
\dt 06/Feb/2019

\lx droo-du
\ph ɖɔ:-du
\dialx GOs
\va doori
\dialx PA BO
\is maison
\ps n
\ge bord inférieur de la toiture
\dt 25/Aug/2021

\lx dròò-kêni
\ph ɖɔ:kêɳi
\dialx GOs
\va dròò-xêni
\dialx GO(s)
\va dòò-kêni
\dialx BO
\va dòò-va-jêni
\dialx WEM
\is corps
\ps n
\ge pavillon de l'oreille
\xv drò-kênii-je
\dialx GO
\xn le pavillon de son oreille
\xv dòò-kênii-n
\dialx BO
\xn le pavillon de son oreille
\dt 08/Jan/2022

\lx dròò-kibö
\ph ɖɔ:kibω
\dialx GOs
\va dròò-xibö
\ph ɖɔ:ɣibω
\is poisson
\ps n
\ge carangue (taille moyenne)
\dn (lit. feuille de palétuvier)
\cf kûxû
\ce carangue (la même, de taille juvénile)
\dt 08/Feb/2025

\lx dròò-ko
\ph ɖɔ:ko
\dialx GOs
\va dòò-ko
\dialx BO
\is plantes
\ps n
\ge brède à feuilles comestibles ; morelle noire
\sc Solanum nigrum L.
\scf Solanacées
\gb Black nightshade
\dt 02/Jan/2022

\lx droo-mwa
\ph ɖo:mwa
\dialx GOs
\va doori
\dialx PA BO
\is maison
\ps n
\ge bord inférieur de la toiture
\dn il dépasse de la sablière des maisons carrées
\dt 02/Jan/2022

\lx dròò-mhwêêdi
\dialx GOs
\is corps
\ps n
\ge ailes du nez
\dt 08/Mar/2019

\lx dròò-phê
\ph ɖɔ:phê
\dialx GOs
\va dròò-phoã
\dialx GOs
\is plantes
\ps n
\ge laiteron ; feuille de "pissenlit"
\sc Sonchus oleraceus L.
\scf Astéracées
\dt 02/Jan/2022

\lx dròòrò
\ph ɖɔ:ɽɔ, ɖɔ:rɔ
\dialx GOs
\va dròrò
\dialx GO(s)
\is temps_deixis
\ps LOC
\ge hier
\se kabu dròòrò
\sge la semaine dernière
\se kabu ni hêbu
\sge la semaine d'avant (il y a longtemps) ; les semaines passées
\dt 22/Feb/2025

\lx dròò-xè
\ph ɖɔ:-ɣɛ
\dialx GO
\va dòò-
\dialx PA
\is classificateur_numérique
\ps CLF.NUM (feuilles)
\ge un paquet de (feuilles)
\xv dròò-xè; dro-tru
\xn un ; deux paquets de feuilles
\dt 02/Sep/2021

\lx dròòxi
\ph ɖɔ:ɣi
\dialx GOs
\va dòki
\dialx BO
\is religion
\ps n
\ge magie
\ge esprit
\dn qui se manifeste par une boule de feu dans la nuit
\dt 08/Feb/2025

\lx dròò-yòò
\ph ɖɔ:yɔ:
\dialx GOs
\is poisson
\ps n
\ge poisson
\dn (lit. feuille de bois de fer)
\dt 08/Feb/2025

\lx drope
\ph ɖope
\dialx GOs
\is couleur
\ps v
\ge multicolore
\dt 26/Jan/2019

\lx drò-uva
\ph ɖɔuva
\dialx GOs
\va dò-uva
\dialx BO
\is taro
\ps n
\ge feuille de taro d'eau
\cf drò
\dialx GOs
\ce feuille de taro d'eau
\dt 10/Jan/2022

\lx dròvivińi
\ph ɖɔβiβini
\dialx GOs
\va dopipini
\dialx GO
\va dovivini
\dialx BO
\is temps_découpage
\ps n
\ge crépuscule
\dt 28/Jan/2019

\lx drovwe
\ph ɖoβe
\dialx GOs PA BO
\va do-phe
\dialx PA BO
\is terrain_terre
\ps n
\ge terre laissée par l'inondation ; terre d'alluvions
\dt 28/Jan/2019

\lx drövwiçu
\ph ɖωβidʒu
\dialx GOs
\va dopicu, dovio, duvio
\dialx BO PA
\is outils
\ps n
\ge clou
\dt 20/Mar/2023

\lx drua-ko
\dialx GOs PA
\is déplacement
\va dua-ko
\dialx PA
\ps v
\ge frayer (se) un chemin dans la brousse
\dn pour se sauver
\ge sauver (se) dans la brousse
\dt 08/Feb/2025

\lx druali
\dialx GOs
\is mollusque
\ps n
\ge coquillage long
\dn qui s'enfonce dans le sable
\dt 08/Feb/2025

\lx drube
\dialx GOs
\va dube
\dialx PA
\va cèvèro
\dialx WEM
\is mammifères
\ps n
\ge cerf
\dt 16/Aug/2021

\lx drudruu
\ph ɖuɖu:
\dialx GOs
\is mollusque
\ps n
\ge porte-montre
\sc Chicoreus ramosus
\scf Muricidés
\dt 27/Aug/2021

\lx e
\dialx GOs
\is grammaire_modalité
\ps v
\ge convenir ; être bien
\xv ezoma e, nòme zo tree mònõ
\dialx GO
\xn ce serait bien s'il fait beau demain
\xv kavwö e
\dialx GO
\xn ce n'est pas ainsi; cela ne convient pas
\dt 18/Oct/2021

\lx e-
\dialx BO
\is parenté_couple
\ps PREF (couple PAR)
\ge parenté duelle (préfixe de)
\se e-peebu
\sge grand-père et petit-fils
\se e-pòi-n
\dialx BO
\sge père et fils, mère et enfant (BM)
\se epòi epè-toma kolo-li
\sge tante paternelle et neveu ou nièce (BM)
\dt 08/Jan/2022

\lx -e
\dialx GOs PA
\va -è
\dialx WEM
\is grammaire_suff_directionnel
\ps DIR (transverse)
\ge en s'éloignant du locuteur
\dn sur un axe transverse en allant d'une vallée à l'autre, ou pour traverser un cours d'eau
\xv e na-e pòi-je ce-la lai
\dialx GO
\xn elle donne à ses enfants du riz
\xv e za kô-raa mwã na cö-mwã-e
\dialx GO
\xn ne peut passer de l'autre côté
\xv ma novwö na cö kala-mwã-e, [...] ca (e)zoma me naa mwã kudoo-cö
\dialx GO
\xn quand tu repartiras là-bas, [...] alors, nous te donnerons à boire pour là-bas
\xv e tigi mwã na a-mwã-e
\dialx GO
\xn c'est trop épais pour traverser
\xv a mi a-mwã-è thaa-mwã-mi xa we-î ?
\dialx PA
\xn et si nous allions creuser notre (conduite d') eau vers ici ?
\xv li ra u a, hava-mwã-è ênõli ni nyòli we
\dialx PA
\xn elles partent, arrivent enfin là-bas à cette rivière
\xv phe mwa nye deang, thi-mwã-è ni phwe-keala
\dialx WEM
\xn elle prend l'épuisette, la met en travers de l'ouverture du panier
\dt 06/May/2024

\lx -è
\dialx GOs
\is grammaire_démonstratif
\ps DEIC.1
\ge ce …-ci
\xv ẽnõ-è
\xn cet enfant-ci
\xv aazo-è
\xn ce chef-ci
\xv ni khabu-è
\dialx GOs
\xn cette semaine
\xv we kaavwu mwa-è
\dialx GOs
\xn vous êtes les gardiens de cette maison
\xv we kòòla êni xo ime mãlò-è ce me za kòòla êne
\dialx GOs
\xn vous qui êtes debout là et nous qui sommes debout ici
\xv e za kô-raa mwã na cö mwã-e  xo ijè-è õã-je
\dialx GOs
\xn celle-là sa mère ne peut passer de l'autre côté
\xv e za khõbwe xo ã êmwê khõbwe novwö ijè-è ca e za Hiixe
\dialx GOs
\xn cet homme pense que celle-ci est Hiixe
\dt 20/Feb/2025

\lx ê
\hm 1
\dialx GOs
\is grammaire_démonstratif
\ps ANAPH
\ge là ; là-bas (anaphorique)
\dn réfère à des inanimés absents, mais connus des interlocuteurs
\cf -ò
\ce là Dx2
\cf òli
\ce Dx3
\xv kavwö ne hine khõbwe ge-le xa êmwê-ê
\xn elle ne savait pas qu'il existait un tel homme
\dt 26/Mar/2022

\lx ê
\hm 2
\ph ê
\dialx GOs
\va èm
\ph ɛ̃m
\dialx PA BO
\is plantes
\ps n
\ge canne à sucre
\sc Saccharum officinarum
\scf Graminées, Panicoidées
\se tha ê
\dialx GO
\sge cueillir la canne à sucre
\se whizi ê
\dialx GO
\sge manger de la canne à sucre
\se whili èm
\dialx PA
\sge manger de la canne à sucre
\dt 27/Aug/2021

\lx e zo !
\dialx GOs
\is discours_interjection
\ps INTJ
\ge bien fait !
\cf i no-n !
\dialx PA
\ce bien fait pour lui ! (lit. sa crotte)
\dt 19/Apr/2024

\lx èa ?
\dialx GOs
\va ia ?
\dialx GO(s)
\va ia, ya
\dialx BO
\is grammaire_interrogatif
\ps INT
\ge où ?
\xv kò èa ?
\dialx GOs
\xn dans quelle forêt ?
\xv e yu èa mwã ?
\dialx GOs
\xn où vit-il ?
\xv cö tròò na èa mõû-cö ?
\dialx GOs
\xn où as-tu trouvé ton épouse ?
\xv çö ã-da èa mwã ?
\dialx GOs
\xn où montes-tu ?
\xv çö uça na èa mwã ?
\dialx GOs
\xn d'où arrives-tu ?
\xv ge èa mwã nye ẽnõ ?
\dialx GOs
\xn où est cet enfant ?
\xv ge èa loto i çö ?
\dialx GOs
\xn où est ta voiture ?
\xv ge èa thoomwã ã ?
\dialx GOs
\xn où est cette femme ?
\xv ge nu èa mwã ?
\dialx GOs
\xn où/à quel endroit suis-je ?
\xv ge èa hèlè ?
\dialx GOs
\xn où se trouve le couteau ?
\xv ge èa mò-çö ?
\dialx GOs
\xn où se trouve ta maison ?
\xv e yu èa mwã ?
\dialx GOs
\xn où vit-il ?
\xv a-yu èa mwã ?
\dialx GOs
\xn c'est un habitant d'où ?
\xv ge-je èa caaja ?
\dialx GOs
\xn où est Papa ?
\xv ge-çö èa ?
\dialx GOs
\xn où es-tu ?
\xv ge-la èa ?
\dialx GOs
\xn où sont-ils ?
\xv e a ca/ça èa loto-ã ?
\dialx GOs
\xn jusqu'où va cette voiture ?
\xv e trê ca/ça èa ?
\dialx GOs
\xn jusqu'où court-il ?
\xv la trê na èa mwã ?
\dialx GOs
\xn où sont-ils allés courir ?
\cf ia ?
\ce où ?
\cf ça èa ? ja èa?
\ce jusqu'où ?
\dt 20/Feb/2025

\lx êba
\dialx GOs PA
\is grammaire_locatif
\ps ADV.DIR
\ge là-bas latéralement
\xv ge êba
\dialx PA
\xn il est là-bas (latéralement)
\xv novwö êba ça e mõ
\dialx GO
\xn et là sur le côté, cela s'est vidé
\ng forme courte de |lx{ênè-ba}
\dt 05/Jan/2022

\lx ebe
\dialx GOs
\va mõõ-n
\dialx PA
\is parenté_alliance
\ps n
\ge beau-frère
\xv ebee-nu
\dialx GO
\xn mon beau-frère
\dt 27/Jan/2019

\lx ebe ba-êgu
\dialx GOs
\va mõõ-n thòòmwã, mõõ-n dòòmwã
\dialx PA
\is parenté_alliance
\ps n
\ge belle-sœur
\dt 26/Aug/2021

\lx ebiigi
\dialx GOs BO
\va biigi, bii
\dialx PA BO WEM
\is parenté
\ps n
\ge cousin croisé de même sexe (aîné ou cadet): fils/fille de sœur de père
\ge fils/fille de frère de mère
\xv la pe-ebiigi
\dialx GO
\xn ils sont cousins
\xv ebiigi-nu
\dialx GO
\xn c'est mon cousin
\xv i biigi-ny
\dialx PA
\xn c'est mon cousin
\xv tho-mi xo bii Suzan (= biigi)
\dialx WEM
\xn cousine Suzanne l'appelle
\cf bibi
\ce terme d'adresse
\dt 05/Nov/2021

\lx êbòli
\dialx GOs PA BO
\is grammaire_direction
\ps ADV.LOC.DIR
\ge là-bas en bas ; là-bas vers la mer
\dn visible ou non
\xv e a-du êbòli kòli we za
\dialx GO
\xn elle est descendue à la mer
\xv e a-du êbòli kòli kaze
\dialx GO
\xn elle est descendue à la mer
\xv èńiza nye çö thaavwu pîînã-du êbòli bwabu ?
\dialx GO
\xn quand es-tu allée en France pour la première fois ?
\xv e a-du êbòli bwabu
\dialx PA
\xn elle est partie en France
\dt 08/Feb/2025

\lx êda
\dialx GO
\is grammaire_direction
\ps ADV.DIR
\ge là-bas en haut ; devant
\dt 31/Dec/2021

\lx êdime
\dialx GOs
\is mollusque
\ps n
\ge "bigorneau"
\sc Turbo petholatus
\scf Turbinidés
\dt 27/Aug/2021

\lx êdi-me
\dialx GOs
\is corps
\ps n
\ge œil
\dt 24/Jan/2019

\lx êdoa
\ph ẽ'ndo.a
\dialx GOs
\va êdo
\dialx GO(s)
\is plantes_partie
\ps n
\ge graine
\ge noyau ; pépin
\dt 26/Aug/2021

\lx êdu
\dialx GO
\is grammaire_direction
\ps ADV.DIR
\ge là-bas en bas ; derrière
\dt 31/Dec/2021

\lx êê-
\dialx GOs PA BO [BM]
\sn 1
\is cultures
\ps n
\ge plants
\xv êê-nu
\dialx GOs
\xn mes plants
\xv êê-ny
\dialx BO
\xn mes plants
\sn 2
\is classificateur_possessif
\ps CLF.POSS (plants)
\ge CLF des plants
\xv êê-nu khô-kumwala
\xn mes boutures de patate douce
\dt 22/Feb/2025

\lx êgi
\dialx GOs
\is action_corps
\ps v
\ge attraper (qqch. en mouvement)
\xv e ne la-ã pwaixe vwö kêbwa na mi êgi-li mwã
\xn elle fait tout cela pour nous empêcher de les attraper
\xv çö êgi kamwêlè ? – Hê-pwe
\xn tu les as attrapés comment? – À la ligne
\dt 24/Feb/2025

\lx êgo
\ph ẽŋgo
\dialx GOs
\va pi-ko
\dialx PA
\is oiseau
\is poisson
\ps n
\ge œuf (poule, poisson, crustacé)
\se êgo ko
\dialx GOs
\sge œuf de poule
\dt 26/Aug/2021

\lx êgo dili
\dialx GOs
\is cultures
\ps n
\ge motte de terre
\dt 24/Jan/2018

\lx êgo mebo
\dialx GOs
\is insecte
\ps n
\ge nid de guêpes
\dt 20/Feb/2025

\lx êgo ulò
\dialx GOs
\is insecte
\ps n
\ge larve de sauterelle
\dt 16/Aug/2021

\lx ẽgõgò
\ph ɛ̃ŋgɔ̃ŋgɔ
\dialx GOs
\va êgòl
\ph ɛ̃ŋgɔl
\dialx PA BO WEM WE
\va êgògòn
\dialx BO
\is grammaire_temps
\ps ADV
\ge autrefois ; il y a longtemps ; avant
\xv ẽgõgò xa kavwa hine
\dialx GOs
\xn il ne le savait pas auparavant
\xv e gi nõnõmi nya tree na ẽgõgò
\dialx GOs
\xn il pleure en pensant à ce jour passé
\se whamã ẽgõgò
\sge les vieux d'antan
\dt 25/Feb/2023

\lx êgòl
\ph ẽŋgɔl
\dialx PA BO WEM WE
\va êgõgò
\dialx GO
\is grammaire_temps
\ps ADV
\ge autrefois ; il y a longtemps ; avant
\xv êgòl êgòl
\dialx PA
\xn il y a très longtemps
\dt 26/Aug/2021

\lx egu
\dialx GO
\va eku
\dialx GO
\is grammaire_agent
\ps AGT
\ge sujet ; agent
\dn (marque archaïque)
\dt 08/Feb/2025

\lx êgu
\hm 1
\dialx GOs BO
\is société
\sn 1
\ps n
\ge homme ; personne
\xv yo êgu va ?
\dialx BO
\xn d'où es-tu?
\se êgu hayu
\sge un homme quelconque
\se êgu polo
\dialx BO
\sge albinos
\se êgu ni nipa
\sge un homme fou
\sn 2
\ps n
\ge corps ; main-d'œuvre
\xv haxe novwo êgu-n, ca ka mhwã nõõli
\dialx PA
\xn mais quant à sa personne/son corps, nous ne la voyons pas
\xv mhwã a-mi na êgu-n mwã nee mhenõ-nyama
\dialx PA
\xn nous amenons de la main-d'œuvre pour que nous fassions les travaux
\dt 20/Feb/2025

\lx êgu
\hm 2
\dialx GO
\is caractéristiques_personnes
\ps v
\ge sobre
\xv la za peve êgu
\xn ils sont tous sobres
\dt 23/Jan/2022

\lx êgu-zo
\dialx GOs
\va ayò
\dialx BO PA
\is caractéristiques_personnes
\ps v.stat
\ge joli ; beau (personne, chose)
\xv ne-xo egu-zo ! (ou) ne-vwo egu-zo !
\xn fais-le joli !
\dt 03/Feb/2025

\lx èjè
\ph ɛjɛ
\dialx GOs PA BO
\sn 1
\is grammaire_démonstratif
\ps DEM
\ge cette femme-ci
\xv jo novwö jè-ò mõû-m, ca ra u èjè jènã
\dialx PA
\xn bon et ton épouse, la voilà c'est celle-là
\se èjè--ã
\sge cette femme-ci (DX1)
\se èjè êni
\sge cette femme-là (DX2)
\se èjè-ba
\sge cette femme-là (DX2 sur le côté, éloignée mais visible)
\se èjè-òli
\sge cette femme là-bas (DX3)
\se èjè-du mu
\sge cette femme-là derrière
\se èjè êda
\sge cette femme là-haut
\se èjè êdu
\sge cette femme-là en bas
\se èjè-bòli
\sge cette femme-là loin en bas
\sn 2
\is grammaire_interpellation
\ge hé ! la femme !
\dt 24/Feb/2025

\lx èjè-ni
\dialx GOs PA BO
\is grammaire_démonstratif
\ps DEM.FEM. DEICT2
\ge cette femme-là
\ng forme courte de |lx{èjè-êni}
\dt 05/Jan/2022

\lx èla-ã
\dialx GOs PA
\is grammaire_démonstratif
\ps DEM.PL.DEIC.1 proximal
\ge celles-ci
\se èla-êni
\sge celles-là (DX2)
\se èla-êba
\sge celles-là (DX2 sur le côté)
\se èla-òli
\sge celles-là là-bas (DX3)
\se èla-êdu mu
\sge celles-là derrière
\se èla-êda
\sge celles-là là-haut
\se èla-êdu
\sge celles-là en bas
\se èla-êbòli
\sge celles-là en bas loin
\dt 20/Feb/2025

\lx èla-òli
\dialx GOs PA
\is grammaire_démonstratif
\ps DEM.PL.DEIC.1
\ge celles-là là-bas (DX3)
\dt 20/Feb/2025

\lx ele
\dialx GOs BO
\is action_corps
\ps v
\ge pousser (qqch. avec la main)
\xv pò ele-mi
\dialx GO
\xn pousse-le un peu vers moi
\xv pò ele-ò
\dialx GO
\xn pousse-le un peu vers là
\dt 17/Feb/2025

\lx èli-ã
\dialx GOs
\is grammaire_démonstratif
\ps DEM.DEIC.1 proximal
\ge ces deux-ci
\se èli êni
\sge ces deux-là (DX2)
\se èli êba
\sge ces deux-là (DX2 sur le côté)
\se èli-òli
\sge ces deux là-bas (DX3)
\se èli êda
\sge ces deux-là en haut
\se èli êdu mu
\sge ces deux-là derrière
\se èli êdu
\sge ces deux-là en bas
\se èli êbòli
\sge ces deux-là loin en bas
\dt 20/Feb/2025

\lx èlò
\dialx GO PA BO
\sn 1
\is grammaire_assertif
\ps INTJ
\ge oui
\sn 2
\is discours
\ps v
\ge accepter ; dire oui
\xv i elòge
\dialx PA
\xn il l'a accepté ; il a acquiescé
\an ai
\at non
\dt 22/Feb/2025

\lx èlò-ã
\dialx GOs
\is grammaire_démonstratif
\ps DEM.DEIC.1 proximal
\ge ces trois-ci
\se èlò êni
\sge ces trois-là (DX2)
\se èlò êba
\sge ces trois-là (DX2 sur le côté)
\se èlò-òli
\sge ces trois là-bas (DX3)
\se èlò êda
\sge ces trois là-haut
\se èlò êdu mu
\sge ces trois-là derrière
\se èlò êdu
\sge ces trois-là en bas
\se èlò êbòli
\sge ces trois-là loin en bas
\dt 24/Feb/2025

\lx eloe
\dialx GA
\is préparation_aliments
\ps v
\ge couper en lamelle
\dt 26/Jan/2019

\lx èlò-è !
\dialx GOs
\is grammaire_interpellation
\is grammaire_démonstratif
\ps INTJ
\ge hé ! vous !
\dn (emploi triel, paucal)
\dt 21/Feb/2025

\lx emãli
\dialx GOs PA
\sn 1
\is grammaire_démonstratif
\ps DEM.duel
\ge ces deux-là
\xv ra u cabòl-da-mi emãli-ò maama
\dialx PA
\xn ces deux diables là-bas se dressent
\sn 2
\is grammaire_interpellation
\ps DEM.duel
\ge hé vous deux !
\dt 21/Feb/2025

\lx emãlo
\dialx GOs
\is grammaire_démonstratif
\ps DEM.triel
\ge ces trois-là
\xv i emãlo
\dialx GO
\xn où sont les trois autres ?
\dt 08/Jan/2022

\lx è-mõõ
\dialx GOs
\va mõõn
\dialx PA
\is parenté_couple
\ps couple PAR
\ge beaux-parents: beau-père (d'épouse ou de mari) ; belle-mère (d'épouse ou de mari)
\ge beau-père et beau-fils
\xv mõõn i je
\dialx PA
\xn ses beaux-parents
\xv li è-mõõn
\dialx PA
\xn ils sont beaux-parents et beaux-enfants
\dt 13/Sep/2021

\lx è-mõû
\dialx GOs
\va è-mõû-n
\dialx PA BO
\is parenté_couple
\ps couple PAR
\ge époux (mari et femme)
\xv li è-mõû-li
\dialx GO
\xn ils sont mari et femme
\xv li è-mõû-n
\dialx PA
\xn ils sont mari et femme
\dt 26/Aug/2021

\lx êmwê
\hm 1
\dialx GOs
\va êmwèn
\dialx PA BO
\is société
\ps n
\ge homme ; mâle
\se ẽnõ êmwê
\dialx GOs
\va ẽnõ êmwèn
\dialx PA
\sge garçon
\se ãbaa-ny êmwèn
\dialx PA
\sge mon frère
\dt 08/Jan/2022

\lx êmwê
\hm 2
\dialx GOs BO
\va êmwèn
\dialx PA
\is corps_doigt
\ps n
\ge index (main)
\se hii-n êmwèn
\dialx PA
\sge index
\cf hii-n thoomwã
\dialx PA
\ce pouce
\cf thizii, thiri, tiri
\ce auriculaire
\dt 10/Jan/2022

\lx êmwèn kòlò-
\dialx PA BO
\is parenté
\ps n
\ge fils de frère ; neveu
\dn (lit. garçon de mon côté)
\dt 08/Feb/2025

\lx ênã
\ph ẽɳɛ̃
\dialx GOs
\va ènan
\dialx BO [BM]
\va kaalu
\dialx GO(s)
\is santé
\ps v ; n
\ge blessé
\xv i thu ènan
\dialx BO
\xn il a eu un accident [BM]
\dt 03/Dec/2021

\lx êńa
\ph ẽna
\dialx GOs PA BO
\is grammaire_locatif
\ps ADV.LOC (spatio-temporel)
\ge là (pas loin)
\xv na êńa
\dialx GOs
\xn à cet endroit (pas loin)
\xv çö yu êńa ?
\dialx GOs
\xn tu habites à cet endroit (pas loin)?
\xv kòòl na êna phwaxilo
\dialx PA
\xn elle se tient là à la porte
\dt 05/Nov/2021

\lx ène
\is grammaire_démonstratif
\dialx GO
\va èna
\dialx GO
\ps PRON.DEM.ANAPH
\ge cela ; ce …-là
\xv õgi xa ène, ca e khõbwe cai je
\xn quand il a fini cela, elle lui dit
\xv èna ca khõbwe cai je
celle-là lui dit
\dt 03/Feb/2025

\lx ênè
\hm 1
\ph ẽɳɛ
\dialx GOs BO
\is grammaire_locatif
\ps n.loc
\ge endroit où
\xv iru a ênè no kobwe
\dialx BO
\xn il ira là où je dis
\xv nu hivwine ênè e a le
\dialx GOs
\xn j'ignore l'endroit où il est allé
\xv nu porome ênè e yu (le)
\dialx GOs
\xn j'ai oublié où il habite
\dt 03/Feb/2025

\lx ênè
\hm 2
\ph ẽɳɛ
\dialx GOs BO PA
\va enã
\dialx PA
\is grammaire_locatif
\ps ADV.LOC
\ge ici
\dt 25/Aug/2023

\lx ênè-ba
\ph ẽɳɛba
\dialx GOs
\va èba
\dialx GO(s)
\is grammaire_direction
\ps ADV.LOC.DEIC.2
\ge là (sur le côté, latéralement)
\dt 02/Jan/2022

\lx ênê-da
\ph ẽɳêda
\dialx GOs
\va ênîda
\dialx WEM PA BO
\is grammaire_direction
\ps ADV.DIR
\ge vers le haut ; en amont ; vers la montagne
\ge vers la terre
\ge au sud ; à l'est
\xv tha-ò-da mwã avwõnõ na ênîda
\dialx PA
\xn ils arrivent enfin chez eux là-bas en haut
\dt 02/Jan/2022

\lx ênê-du
\ph ẽɳêdu
\dialx GOs
\is grammaire_direction
\ps ADV.LOC.DIR
\ge bas (en) ; vers le bas ; en aval ; vers la mer
\ge au nord ; à l'ouest
\xv ênê-du mwã
\xn très loin en aval
\dt 02/Jan/2022

\lx ênè-ò
\ph ẽɳɛɔ
\dialx GOs
\is grammaire_locatif
\ps n.loc ANAPH
\ge endroit-là
\dn mentionné et connu des locuteurs
\dt 08/Feb/2025

\lx êni
\dialx GOs
\ph ẽɳi
\va enim
\dialx PA
\is grammaire_locatif
\ps ADV.LOC.DEIC.3
\ge là (peut avoir un sens péjoratif)
\xv yu ra kòòl na ênim pwa, u yu ru ra mã le
\dialx PA
\xn tu vas rester là dehors, et tu vas y mourir
\xv êni-da bwe Kaòla
\xn là-haut au sommet de Kaòla
\an ênè
\at ici
\dt 05/May/2024

\lx èńiza ?
\ph ɛńiða
\nph /n/ dental
\dialx GOs
\va ènira ?
\dialx WEM WE PA
\va inira ?
\dialx BO
\is grammaire_interrogatif
\ps INT
\ge quand? (passé et futur)
\xv yo a-mi inira ?
\dialx BO
\xn quand arrives-tu ?
\et *ŋainsa
\el POc
\dt 27/Oct/2021

\lx èńiza mwã
\ph ɛńiða
\dialx GO
\va inira mwã
\dialx BO
\is grammaire_temps
\ps ADV
\ge indéfiniment ; un jour
\dt 02/Jan/2022

\lx ẽnõ
\hm 1
\ph ɛ̃ɳɔ̃
\dialx GOs
\va ênõ
\dialx PA BO
\sn 1
\is étapes_vie
\ps n ; v
\ge jeune ; petit ; enfant
\xv pòi-nu ẽnõ
\xn mon dernier enfant, mon benjamin
\xv nu pò ẽnõ nai çö
\dialx GOs
\xn je suis un peu plus jeune que toi
\se ẽnõ-êmwê
\dialx GO
\va ênõ-êmwèn
\dialx PA
\sge garçon
\se ẽnõ thoomwã, ẽnõ-zoomwã
\dialx GO
\va ẽnõ-roomwã
\dialx WEM
\sge fille
\se ẽnõ-ba-êgu
\dialx GO
\sge fille
\dt 27/May/2024

\lx ẽnõ
\hm 2
\ph ɛ̃ɳɔ̃
\dialx GOs
\va ènõ
\dialx PA
\is parenté
\ps n
\ge tante paternelle ("tantine") ; sœur de père
\ge épouse du frère de mère
\ge cousine du père
\ge épouse des cousins de mère
\ge neveu ou nièce de la sœur du père
\xv ẽnõ i nu
\xn ma tante paternelle
\dt 22/Sep/2021

\lx ẽnõ ni gò
\ph ɛ̃ɳɔ̃ ni gɔ
\dialx GO PA
\is parenté
\ps n
\ge puîné
\dn (lit. enfant du milieu)
\dt 08/Feb/2025

\lx ẽnõ xa pogabe
\ph ɛ̃ɳɔ̃
\dialx GOs
\is étapes_vie
\ps n
\ge nourrisson ; nouveau-né
\dn (lit. enfant qui est très petit)
\dt 09/Feb/2025

\lx ẽnõbau
\ph ɛ̃ɳɔ̃bau
\dialx GOs
\is caractéristiques_objets
\ps v
\ge agréable ; beau ; merveilleux 
\dn terme lié à la religion
\dt 08/Feb/2025

\lx ênõli
\ph ẽɳɔ̃li
\dialx GOs
\va ènõli
\dialx PA BO
\is grammaire_direction
\ps ADV.LOC
\ge là-bas ; au-delà
\xv ge je ênõli
\xn il est là-bas
\xv ge je ênõli mwã
\xn il est loin là-bas
\xv li ra u a, hava-mwã-è ênõli ni nyõli we
\dialx PA
\xn elles partent, arrivent enfin là-bas à cette rivière
\dt 31/Dec/2021

\lx èńoma
\ph ɛńoma
\dialx PA BO [BM]
\is grammaire_quantificateur_mesure
\ps PRON.QNT
\ge les autres
\xv ge vhaa èńoma
\xn les autres parlent
\dt 16/Feb/2025

\lx ênuda
\ph 'ẽɳuda
\dialx GOs BO
\is grammaire_direction
\ps ADV.DIR
\ge là-haut ; en haut (sur la montagne) ; au-dessus
\xv mo za thrõbo-du mwã na (ê)nuda
\dialx GO
\xn nous descendions de là-haut
\xv e za thrêê bwa mõ, thrêê je ênuda
\dialx GO
\xn elle court sur le rivage, elle court là-bas (vers la terre)
\xv e a=daa mwã vwo hovwa mwã ênuda
\dialx GO
\xn il monte et arrive là-haut (chez lui)
\xv nu nõõli mwã ãmãlã nu ênuda mwã bwa mõ
\dialx GO
\xn j'ai vu des cocotiers là-bas en haut sur la terre
\xv gele-xa thrii ênuda Bweye
\dialx GO
\xn il y avait une levée de deuil là-haut à Bweyen
\xv ênuda mwã
\dialx BO
\xn là-bas en haut au loin
\dt 05/Aug/2023

\lx e-nye
\dialx GOs PA BO
\is grammaire_présentatif
\ps DEM
\ge voici (le)
\dt 29/Jan/2019

\lx e-nye-ba
\dialx PA
\is grammaire_présentatif
\ps DEM
\ge voilà (le) là
\dn on le montre et on regarde dans sa direction
\dt 05/Mar/2019

\lx e-nye-bòli
\dialx PA
\is grammaire_présentatif
\ps DEM.DEIC.3 distal
\ge voilà (le) là-bas
\dn à un endroit plus bas que là où on est
\dt 08/Feb/2025

\lx e-nyoli
\dialx GOs PA
\is grammaire_démonstratif
\is grammaire_présentatif
\ps DEM
\ge voilà (le) ; celui-là (visible)
\dt 29/Jan/2019

\lx e-nyu-da
\dialx PA
\is grammaire_présentatif
\ps DEM.LOC
\ge voilà (le) en haut
\dt 14/Aug/2021

\lx e-nyu-du
\dialx PA
\is grammaire_présentatif
\ps DEM.LOC
\ge voilà (le) en bas
\dt 25/Oct/2021

\lx epè-
\dialx GO
\is parenté_couple
\ps PREF (couple PAR)
\ge parenté duelle
\xv epè-be-yaza
\xn ils sont deux homonymes
\xv li epè-me
\xn ils sont frère et sœur
\xv li epè-è-mõõ
\xn ils sont belle-mère et gendre
\xv li epè-è-pööni
\xn oncle utérin et neveu
\dn |lx{epè-} indique un couple hétérogène en relation duelle (selon Haudricourt, Leenhardt)
\dt 05/Jan/2022

\lx è-peebu
\dialx GOs
\ph ɛβe:bu
\va è-veebu
\dialx GO(s)
\va è-veebu-n
\dialx PA
\is parenté_couple
\ps couple PAR
\ge grand-père ou grand-mère et petit-fils ou petite-fille
\xv bî è-veebu
\dialx GO
\xn nous sommes grand-père (ou) grand-mère et petit-fils (ou) petite-fille
\xv li è-peebu
\dialx GO
\xn ils sont grand-père et petit-fils
\xv li è-veebu-n
\dialx PA
\xn ils sont grand-père (ou) grand-mère et petit-fils (ou) petite-fille
\dt 08/Jan/2022

\lx epè-thoomwã kòlò
\dialx GOs
\va ebe-thoomwã kòlò
\dialx GO
\is parenté_couple
\ps couple PAR
\ge tante paternelle ("tantine") et nièce ou neveu
\xv ebè-thoomwã kòlò-nu
\dialx GO
\xn ma tante paternelle
\xv epè-thoomwã kòlò-li
\dialx GO
\xn ils sont en relation de tante paternelle et nièce ou neveu
\dt 08/Feb/2025

\lx è-pòi
\ph ɛpɔi
\dialx GOs
\va è-vwòi
\ph ɛβɔi
\dialx GO(s)
\va è-pòi-n
\dialx PA BO
\sn 1
\is parenté_couple
\ps couple PAR
\ge père et fils (ou) fille
\ge mère et fils (ou) fille
\xv li è-pòi
\dialx GO
\xn ils sont père (ou) mère et fils (ou) fille
\xv li è-pòi-n
\dialx PA
\xn ils sont père (ou) mère et fils (ou) fille
\sn 2
\is parenté_couple
\ps couple PAR
\ge sœur de la mère et neveu (ou) nièce
\ge frère du père et neveu (ou) nièce
\xv li è-vwòi
\dialx GO
\xn ils sont tante et neveu/nièce
\xv li è-pòi-n
\dialx PA
\xn ils sont tante et neveu/nièce
\dt 08/Jan/2022

\lx è-pööni
\ph ɛpω:ɳi
\dialx GOs
\va è-pööni-n
\ph ɛpω:nin
\dialx PA
\is parenté_couple
\ps n ; couple PAR
\ge oncle maternel et neveu/nièce maternel
\xv li pe-è-pööni / li pe-è-vwööni
\dialx GOs
\xn ils sont en relation d'oncle et neveu/nièce maternel
\xv li è-pööni-n
\dialx PA
\xn ils sont en relation d'oncle et neveu/nièce maternel
\dn |dialx{PA} n'utilise |lx{è-pööni-n} que pour la parenté réciproque, |lx{pööni-n} désigne l'oncle maternel ; en |dialx{GO}, |lx{è-pööni, è-vwööni} désigne l'oncle maternel, la forme de parenté duelle est marquée par le préfixe réciproque : |lx{pe-è-pööni}
\dt 20/Feb/2025

\lx eu
\dialx PA
\is discours_interjection
\ps INTJ
\ge bah !
\xv Eu ! ye memee mèni na êneda=ò bwe Kaòla
\dialx PA
\xn bah! ce sont sans doute des oiseaux en haut là-bas sur Kaòla
\dn Minorise une mise en garde avec le sens de 'ce n'est rien'
\dt 17/Mar/2023

\lx eva ?
\dialx BO PA
\is grammaire_interrogatif
\ps INT.LOC (statique)
\ge où ?
\xv ge je eva ?
\xn où est-elle ?
\xv i ivi ja eva ?
\xn où a-t-elle ramassé les saletés ?
\dt 16/Feb/2025

\lx evadan
\dialx BO
\is igname
\ps n
\ge igname (clone)
\nt selon Dubois ; non vérifié
\dt 26/Mar/2022

\lx èvwööni
\ph ɛβω:ɳi
\dialx GOs
\is parenté
\ps n
\ge oncle maternel
\dn en |dialx{GO}, |lx{è-pööni, è-vwööni} désigne l'oncle maternel
\dt 08/Jan/2022

\lx evhe
\dialx PA BO
\va epe
\is grammaire_quantificateur_mesure
\ps COLL ; QNT
\ge tous ; ensemble
\xv li evhe be-alaa-n
\dialx PA
\xn ils portent tous le même nom
\dt 16/Feb/2025

\lx exa
\dialx PA BO
\va eka
\dialx BO
\is grammaire_conjonction
\ps CNJ
\ge quand ; lorsque
\dn référence passée
\xv novwo exa waang
\dialx PA
\xn au matin ; quand c'est le matin
\cf novw-exa
\dialx PA
\ce quand ; lorsque
\cf novwo exa
\dialx BO
\ce quand ; lorsque
\xv eka goon-al
\dialx BO
\xn à midi
\dt 22/Feb/2025

\lx ezoma
\dialx GOs
\va zoma
\dialx GOs
\va ruma
\dialx PA
\is grammaire_temps
\ps FUT
\ge futur
\xv ezoma li ubò mõnõ ? – Hai ! kò (neg) li zoma ubò mõnõ, ezoma li yu avwõnõ
\dialx GOs
\xn vont-ils sortir demain ? – Non ! ils ne vont pas sortir demain, ils vont rester à la maison
\xv ezoma a nhye !
\xn celui-ci va partir tout de suite
\dt 22/Feb/2025

\lx è-…-n
\dialx PA
\is parenté_couple
\ps couple PAR
\ge parenté duelle
\xv è-pòi-n
\xn père et fils
\xv è-mõû-n
\xn les époux ; le couple
\dt 22/Feb/2025

\lx fari
\dialx GOs
\is nourriture
\ps n
\ge farine
\bw farine (FR)
\dt 26/Jan/2019

\lx fè
\dialx GOs
\is interaction
\ps n
\ge fête
\bw fête (FR)
\dt 27/Jan/2019

\lx gaa
\hm 1
\dialx GOs PA BO
\va ga
\dialx BO
\is grammaire_aspect
\ps ASP duratif
\ge encore en train de ; toujours en train de
\xv e gaa vhaa gò
\dialx GO
\xn il continue à parler (ça n'en finit plus)
\xv e gaa vhaa gòl
\dialx PA
\xn il continue à parler
\xv gaa waang
\dialx PA
\xn (c'est) encore tôt le matin
\xv gaa ge-la-daa-mi ni dèn
\dialx PA
\xn ils sont en route vers ici
\xv li gaa ẽnõ
\dialx PA
\xn ils sont encore jeunes ; ce sont encore des enfants
\xv i gaa ẽnõ
\dialx BO
\xn il est encore petit
\xv gaa mãã
\dialx PA
\xn (c'est) encore cru (pas encore cuit)
\cf gò
\dialx GOs
\ce encore; continuer à
\cf gòl
\dialx PA
\ce encore; continuer à
\dt 22/Feb/2025

\lx gaa
\hm 2
\ph ga:
\dialx GOs BO
\va gee
\ph ge:
\dialx BO
\va gèèn
\dialx BO [Corne]
\sn 1
\is fonctions_naturelles
\ps n
\ge voix
\sn 2
\is son
\ps n
\ge son ; bruit (de paroles)
\xv gee tho=eli
\xn le son de cet appel
\sn 3
\is musique
\ps n
\ge musique ; air (d'une chanson) ; mélodie
\se gee-wa
\sge mélodie ; air du chant
\dt 22/Feb/2025

\lx gaa kòò gò
\dialx GOs
\va gaa kòòl
\dialx PA BO
\is discours_interjection
\ps v
\ge attendez ! (respect)
\dn (lit. restez debout)
\dt 09/Feb/2025

\lx gaa mara
\dialx PA
\is grammaire_aspect
\ps ASP
\ge venir tout juste de
\xv nu ra gaa mara khõbwe
\dialx PA
\xn c'est ce que je viens de dire
\xv i ra gaa mara a-du-ò
\dialx PA
\xn il vient juste de partir
\xv gaa mara mò-n
\dialx PA
\xn c'est sa première maison
\dt 14/Aug/2021

\lx gaa … gòl
\dialx PA
\is grammaire_aspect
\ps ASP
\ge toujours en train de
\ge faire le premier ; faire d'abord
\xv i gaa mããni gòl
\dialx PA
\xn il est encore en train de dormir
\xv gaa waang gòl
\dialx PA
\xn c'est encore tôt
\xv gaa meã gòl
\dialx PA
\xn ce n'est toujours pas cuit ; c'est encore cru
\xv na ru gaa vhaa gòl
\dialx PA
\xn je vais parler en premier
\xv na ru gaa khõbwe gòl jena nu nõnõmi
\dialx PA
\xn je vais d'abord dire ce que je pense
\xv na ru gaa huu gòl nye nõ
\dialx PA
\xn je me réserve ces poissons (c'est moi qui vais en manger d'abord)
\xv yu gaa kòò gòl, ma nu gaa a kole we ni mûû
\dialx PA
\xn attends un peu, je vais aller arroser les fleurs
\dt 22/Feb/2025

\lx gaa … hô
\dialx PA
\is grammaire_aspect
\ps ASP
\ge venir juste de
\xv i gaa hô uvhi loto
\dialx PA
\xn il vient d'acheter une voiture
\xv i ra gaa hô a-du-ò
\dialx PA
\xn il vient juste de partir
\dt 29/Jan/2019

\lx gaajò
\ph ga:ɲɟɔ
\dialx GOs
\va gaajòn
\dialx PA BO WEM
\is fonctions_intellectuelles
\ps v.i
\ge surpris (être) ; sursauter ; étonner (s') ; étonné (être)
\xv e gaajò-ni êê
\dialx GOs
\xn il s'étonne de cela
\xv nu pa-gaajò-ni-je
\dialx GOs
\xn je l'ai fait sursauter
\xv ra u cabòl-da-mi emãli-ò maama, ra u khii pha-jò-gaajòn-i Teã-mã
\dialx PA
\xn ces deux diables-là se dressent et d'un coup font sursauter Teã-mã
\ng causatif: |lx{pa-gaajò-ni} ; l'autre forme est |lx{pha-jò-gaajòn} ou la forme longue |lx{pha-còòl-gaajòn}
\gt faire sursauter
\cf hããmal
\ce admirer
\dt 22/Feb/2025

\lx gaaò
\dialx PA BO WE
\va ha
\dialx GO(s)
\sn 1
\ps v
\is action
\ge casser (verre) ; éclater
\xv nu gaaò ver i nu
\dialx BO
\xn j'ai cassé mon verre
\xv e gaaò za
\dialx WEM
\xn il a cassé l'assiette
\sn 2
\is caractéristiques_objets
\ge fêlé ; cassé
\dt 20/Aug/2021

\lx gaa-vhaa
\dialx GOs
\va gaa-fhaa
\dialx GA
\is son
\ps n
\ge bruit des voix ; brouhaha
\xv gaa-vhaa i êgu
\xn le bruit des voix des gens
\dt 26/Jan/2019

\lx gaa-we
\dialx GOs PA BO
\va thò-we
\dialx GO(s)
\is eau
\ps n
\ge cascade
\dn (lit. mélodie de l'eau), désigne une cascade bruyante
\dt 26/Mar/2022

\lx gaayi
\dialx PA
\is discours_interjection
\ps INTJ
\ge nom d'un cri
\dn annonce l'interruption ou la fin d'une danse
\dt 21/Feb/2025

\lx gaga
\dialx GOs
\is mammifères
\ps n
\ge roussette
\dn avec du blanc sur le cou
\dt 08/Feb/2025

\lx ngãngi
\ph ŋãŋi
\dialx GOs
\is nourriture_goût
\ps v.stat
\ge acide ; amer
\dt 03/Feb/2025

\lx gala-mii
\dialx GOs
\is plantes_processus
\ps v
\ge mûr (fruits)
\dt 29/Jan/2019

\lx galö
\dialx GOs
\va galo
\dialx PA
\is caractéristiques_objets
\ps v
\ge croquant (sous la dent)
\dn se dit d'un fruit encore vert, tel que la mangue ou la papaye
\xv e galo nye pò-mãã
\dialx PA
\xn la mangue croque sous la dent
\dt 20/Mar/2023

\lx ngãmãã
\ph ŋãmã:
\dialx GOs
\va ngamããm
\dialx PA BO [BM]
\is nourriture
\ps n
\ge restes de nourriture
\dt 26/Jan/2019

\lx gana
\dialx GOs BO
\is plantes
\ps n
\ge pois d'angole ; ambrevade
\sc Cajanus indicus
\scf Fabacées, Légumineuses
\dt 27/Aug/2021

\lx gaò
\dialx GOs PA
\sn 1
\is plantes_processus
\ps v
\ge éclore
\xv u gaò muu
\dialx PA
\xn la fleur a éclos
\sn 2
\is fonctions_naturelles_animaux
\ps v
\ge sortir du cocon (papillon)
\dt 24/Jan/2018

\lx gapavwu-
\dialx GOs
\va gavwu-
\dialx PA
\sn 1
\is préfixe_sémantique
\ps CLF
\ge préfixe des touffes de bambous ou de bananiers
\sn 2
\is classificateur_numérique
\ps CLF.NUM
\ge CLF des touffes de bambous ou de bananiers
\se gapavwu-xè
\sge une touffe de bambous ou de bananiers
\dt 22/Feb/2025

\lx garuã
\dialx GOs PA BO
\is caractéristiques_personnes
\ps v
\ge ruser
\se a-garuã
\sge quelqu'un qui ruse ; un rusé
\xv i ra u garuãu !
\dialx PA
\xn il est rusé !
\xv yu garuãi ni la tèèn mãla-ò.
\dialx PA
\xn tu as rusé tous ces jours passés
\cf truã
\ce mentir ; jouer des tours
\dt 22/Feb/2025

\lx gase ?
\dialx GOs PA
\is discours_interjection
\ps LOCUT
\ge on y va ?
\dn se dit de deux personnes ou plus
\dt 08/Feb/2025

\lx gawee
\dialx GOs
\is plantes_processus
\ps v
\ge fleurir
\dt 20/Mar/2023

\lx ge
\hm 1
\dialx GOs BO PA
\sn 1
\is grammaire_verbe_locatif
\ps v.loc ; progressif
\ge trouver (se) à ; être dans un endroit
\ge près de (être)
\xv ge nu Gome
\dialx GO
\xn je suis à Gomen
\xv ge çö Gome
\dialx GO
\xn tu es à Gomen
\xv ge je Numia
\dialx GO
\xn il est à Nouméa
\xv ge je ênõli
\dialx GO
\xn il est là-bas
\xv ge le da ?
\dialx GO
\xn que se passe-t-il ? ; qu'est-ce qu'il y a ?
\xv ge va hèlè i nu ?
\dialx GO
\xn où est mon couteau?
\xv ge je va ?
\dialx BO
\xn d'où est-il ?
\xv pwòmò-m ge kê-kui ?
\dialx PA
\xn est-ce ton champ d'ignames?
\se ge na
\dialx BO PA
\sge il est ici
\se ge nòli
\dialx BO PA
\sge il est là-bas
\sn 2
\is grammaire_aspect
\ps ASP progressif
\ge être en train de
\xv novwö ne ge je ne mhenõ mããni çe me zoma baa-je (ou) ça me zoma baa-je
\dialx GO
\xn quand elle sera en train de dormir, nous trois la frapperons
\xv kavwö ge je pwaa ce, mã ge je mããni !
\dialx GO
\xn il n'est pas en train de couper du bois, il est en train de dormir
\dt 20/Feb/2025

\lx ge
\hm 2
\dialx PA BO
\is grammaire_conjonction
\ps CNJ ; THEM
\ge et alors
\xv pwòmò-m ge kêê-kui ?
\dialx BO
\xn ton champ est-il un champ d'ignames ?
\dt 27/Aug/2023

\lx ge ni xa
\dialx GOs PA
\is grammaire_verbe_locatif
\ps v.loc INDEF
\ge être quelque part
\xv ge ni xa
\xn il est quelque part
\xv kavwö lha hine khõbwe ge-je ni xa
\xn ils ne savaient pas qu'il était quelque part
\xv ra u a Teã-ma, a gaa ge ni xa tèèn
\dialx PA
\xn Teã-ma part avant le lever du jour (lit. quand le jour est encore quelque part)
\dt 16/Feb/2025

\lx ge … bwa
\dialx GOs
\is grammaire_comparaison
\ps v
\ge plus haut que (être)
\xv ge je bwa na ni mwa-nu
\xn il est plus haut que ma maison
\xv ge je bwa na i nu
\xn il est plus haut que moi (mon supérieur hiérarchique)
\dt 29/Jan/2019

\lx ge … mhenõõ
\dialx GOs PA
\is grammaire_aspect
\ps ASP
\ge en train de
\xv ge je mhenõõ-burom
\dialx PA
\xn il est en train de se baigner/laver
\xv gaa ge je mhenõõ burom
\dialx PA
\xn il est encore en train de se baigner/laver
\dt 14/Oct/2021

\lx gèa
\dialx GOs BO
\wr A
\is caractéristiques_corps
\ps v
\ge loucher
\xv gèa mee-je
\dialx GO
\xn il louche (lit. ses yeux louchent)
\xv êgu xa gèa mee-je
\dialx GO
\xn quelqu'un qui louche
\wr B
\is santé
\ps n
\ge voile blanc de la pupille (maladie de l'œil)
\dt 23/Jan/2022

\lx gee
\dialx GOs
\va gèèng
\dialx BO PA
\va gèènk
\dialx BO arch.
\sn 1
\is caractéristiques_objets
\ps v.stat
\ge sale
\ge taché
\xv i gèèn
\dialx PA
\xn il est sale
\ng causatif: |lx{pa-geene}
\gt salir
\sn 2
\is caractéristiques_objets
\ps n
\ge saleté ; détritus
\ge tache
\xv gee-je
\dialx GO
\xn sa crasse
\xv geenga-n
\dialx PA
\xn sa crasse
\an zo
\dialx GOs
\at propre, bien, bon
\et *tanuq
\eg poussiéreux, sale
\el POc
\dt 03/Feb/2025

\lx gèè
\ph gɛ:
\dialx GOs PA BO
\is parenté
\ps n
\ge grand-mère (maternelle ou paternelle)
\dn terme de désignation et d'adresse
\ge sœur de grand-mère
\ge cousine de grand-mère
\xv gèè i nu
\dialx GO PA
\xn ma grand-mère
\dt 08/Feb/2025

\lx gèè-thraa, gè-raa
\dialx GOs PA
\va gèè-mãmã, gè-raa
\dialx PA WEM WE
\is parenté
\ps n
\ge arrière-grand-mère
\dt 08/Oct/2021

\lx gee-vha
\dialx GOs PA BO
\is musique
\ps n
\ge son ; mélodie de la voix
\dt 22/Feb/2025

\lx gee-wal
\dialx BO
\is musique
\ps n
\ge air (chant, musique)
\dt 28/Jan/2019

\lx ge-le
\dialx GOs PA BO
\is grammaire_existentiel
\ps v.loc
\ge il y (en) a
\xv ge-le da ?
\dialx GO
\xn qu'est-ce qu'il y a ?
\xv ge-le phwa
\dialx BO
\xn il y a des trous
\dt 16/Feb/2025

\lx ge-le xa
\dialx GOs BO
\is grammaire_existentiel
\ps v
\ge il y a (indéfini non spécifique)
\xv ge-le da ?
\dialx GO
\xn qu'est-ce qu'il y a ?
\xv ge-le-xa da ?
\dialx PA
\xn qu'est-ce qu'il y a ? que se passe-t-il ?
\xv ge-le xa po ênê-da
\dialx GO
\xn il y a qqch. là-haut
\xv kavwö cö trõne khõbwe ge-le xa ..., kavwö cö nõõli xa thoomwã na ... khõbwe Mwani-mii ?
\dialx GO
\xn n'as-tu pas entendu dire qu'il y aurait ..., n'as-tu pas vu une femme qu'on appelle Mwani-mi ?
\dt 22/Feb/2025

\lx gènè
\dialx GOs
\va gèn
\dialx PA BO
\is couleur
\ps n
\ge couleur |dialx{GOs}
\ge couleur ; dessin ; maquillage pour danser |dialx{PA BO}
\xv gènè-je
\dialx GO
\xn sa couleur
\xv whaya gènè-n ?
\dialx BO PA
\xn quelle couleur ?
\xv meevwu gènè-n ?
\dialx PA
\xn de toutes les couleurs
\se gèène jopo
\dialx BO PA
\sge dessins géométriques sur les chambranles
\ng forme déterminée de |lx{gè > gène}
\dt 23/Jan/2022

\lx gèny
\dialx BO
\is santé
\ps v.stat
\ge stérile
\nt selon BM
\dt 03/Feb/2025

\lx gèrè
\dialx GOs PA BO
\is nourriture
\ps n
\ge graisse ; huile
\xv mini-gèrèè
\xn résidu de saindoux ; grattons
\bw graisse (FR)
\dt 22/Feb/2025

\lx gè-thaa
\dialx GOs BO PA
\is soin
\ps v ; n
\ge tatouer ; tatouage
\dn (lit. couleur-piquer)
\dt 08/Feb/2025

\lx ge-yai
\dialx GOs BO PA
\is soin
\ps n
\ge tatouage
\dn pratiqué avec des côtes de palmes de cocotier appointées et incandescentes
\dt 20/Feb/2025

\lx gi
\ph ŋgi
\dialx GO PA BO
\is fonctions_naturelles
\ps v
\ge pleurer ; gémir
\xv e gi ã ẽnõ
\dialx GO
\xn cet enfant pleure
\xv e gi mèni
\dialx GO
\xn l'oiseau pleure
\ng causatif: |lx{pa-gi}
\gt faire pleurer
\cf giul
\ce pleurer un mort (pour les hommes)
\cf cöńi
\ce pleurer un mort; être en deuil
\et *taŋis
\el POc
\dt 05/Jan/2022

\lx gibwa
\dialx GOs
\sn 1
\is action_corps
\ps v
\ge tirer d'un coup sec
\sn 2
\is pêche
\ps v
\ge ferrer un poisson
\dt 09/Oct/2018

\lx giçaò
\ph ŋgiʒaɔ, ŋgidʒaɔ
\dialx GOs
\is interaction
\ps v
\ge repentir (se) ; demander pardon ; confesser (se)
\dt 06/Feb/2019

\lx gii-we
\dialx GOs
\is eau
\ps n
\ge courant de l'eau
\dt 28/Jan/2019

\lx gi-mã
\dialx GOs
\is interaction
\ps v
\ge pleurer un mort
\dt 08/Mar/2019

\lx giner
\dialx PA
\is plantes
\ps n
\ge "herbe à éléphant"
\dt 29/Jan/2019

\lx gitaa
\dialx GOs
\is musique
\ps n
\ge guitare
\bw guitare (FR)
\dt 20/Mar/2023

\lx giul
\dialx BO
\is interaction
\ps v
\ge pleurer un mort (pour les hommes)
\cf gi, cöńi
\ce pleurer
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx gò
\hm 1
\dialx GOs BO PA
\sn 1
\is plantes
\ps n
\ge bambou
\se gò-hûgu
\dialx PA
\sge sorte de bambou (utilisé comme percussion)
\sn 2
\is outils
\ps n
\ge couteau de bambou ; couteau à subincision
\sn 3
\is musique
\ps n
\ge musique ; appareil de musique
\dn (lit. son de la flûte en bambou)
\xv e tho gò
\dialx GO
\xn la musique joue
\et *qauR
\el POc
\dt 08/Feb/2025

\lx gò
\hm 2
\dialx GOs
\va gòl
\dialx PA
\is grammaire_aspect
\ps ASP (post-verbal) duratif, persistif
\ge continuer à ; sans interruption ; encore en train de
\xv e mããni gò !
\xn il dort encore
\xv e mãyã gò !
\xn c'est encore cru (mal cuit) !
\xv ge je mããni gò !
\xn il est/était encore en train de dormir
\xv wazaò me ẽnõ gò !
\xn quand nous étions encore enfant
\xv e uça ? – Hai ! kavwö uça gò !
\xn il est arrivé ? – Non ! il n'est pas encore arrivé !
\xv çö hovwo gò !
\xn continue à manger
\xv e mha whaa gò
\dialx GO
\xn c'est encore trop tôt
\xv kixa gò we
\dialx GO
\xn il n'y avait pas encore l'eau (courante)
\se kavwö … gò
\sge pas encore
\dt 22/Feb/2025

\lx gò-êgu
\dialx GOs
\is étapes_vie
\ps n
\ge personne d'âge moyen, d'âge mûr
\cf ẽnõ
\ce enfant
\cf whamã
\ce vieillard
\dt 22/Feb/2025

\lx gò-hii
\dialx PA
\sn 1
\is corps
\ps n
\ge avant-bras
\sn 2
\is richesses
\ps n
\ge monnaie kanak
\dn de la longueur d'un avant-bras, selon Charles Pebu-Polae
\dt 08/Feb/2025

\lx gò-khai
\dialx GOs
\is musique
\ps n
\ge accordéon
\cf khai
\ce tirer
\dt 28/Jan/2019

\lx gò-niza ?
\dialx GO
\va gò-nira ?
\dialx BO
\is grammaire_interrogatif
\ps INT
\ge combien de morceaux ? (bois, poisson, anguilles)
\dt 22/Feb/2025

\lx goo
\dialx GOs
\va goony
\dialx PA BO
\is arbre_cocotier
\ps n
\ge nervure centrale des folioles de palmes de cocotier
\dn elles sont utilisées pour faire des petits balais
\se goo-nu ; goo-a dròò-nu
\dialx GO
\va goony-a-nu
\dialx PA
\sge nervure centrale des folioles de palmes de cocotier
\dt 06/Mar/2023

\lx gòò
\dialx GOs PA BO
\wr A
\ps n
\sn 1
\is grammaire_quantificateur_mesure
\ps n
\ge morceau ; partie
\se gò mãe
\dialx BO
\sge bout de paille
\sn 2
\is plantes_partie
\ps n
\ge tronc
\sn 3
\is corps
\ps n
\ge taille
\dn (lit. milieu)
\xv gòò-n
\dialx PA
\xn sa taille
\sn 4
\is configuration
\ps n
\ge milieu
\xv gòò-n
\dialx BO
\xn son milieu
\xv ni gòò-n
\dialx PA
\xn au milieu
\xv ge ni gòò
\dialx GO
\xn c'est au milieu ; à moitié plein
\se gò êgu
\dialx GOs
\sge homme d'âge mûr/moyen
\se ẽnõ ni gò
\dialx PA
\sge le puîné (enfant du milieu)
\wr B
\is classificateur_numérique
\ps CLF.NUM  [pas à Gomen]
\ge morceau (ou section médiane) |dialx{PA}
\xv gò-xe, gò-ru, gò-kòn, gò-ruuyi (= go-tuuyi)
\dialx PA
\xn un, deux, trois, dix morceaux
\xv gò-ruuji bwa gò-ru
\dialx PA BO
\xn douze morceaux (Dubois)
\et *qau
\eg milieu
\el PNNC
\ea Hollyman
\dt 22/Feb/2025

\lx gòò-bwòn
\dialx PA BO
\va gò-b(w)òn
\dialx BO
\is temps_découpage
\ps n
\ge minuit
\nt selon Corne, BM
\dt 26/Mar/2022

\lx gòò-cee
\dialx GOs
\is plantes_partie
\ps n
\ge tronc d'arbre
\ge morceau de bois
\dt 19/Dec/2021

\lx gòò-kui
\dialx PA
\is igname
\ps n
\ge corps de l'igname
\dt 29/Jan/2019

\lx gòò-mwa
\dialx GOs
\va gò-mwê
\dialx GO
\is maison
\ps n
\ge mur
\se phwa ni gòò-mwa
\sge fenêtre
\dt 26/Jan/2019

\lx gòòn-a
\dialx GOs WE
\va gòòn-al
\dialx BO PA WEM
\is temps_atmosphérique
\ps n
\ge midi ; zénith
\ge soleil irradiant
\xv gòòn-a thrõbo
\dialx GO
\xn le soleil décline
\xv i mããni gòòn-al
\dialx PA
\xn il a fait la sieste
\dt 28/May/2024

\lx gòò-ui
\dialx GOs BO
\is musique
\ps n
\ge flûte ; harmonica
\xv la ui gò
\dialx GO
\xn ils jouent de la flûte
\cf ui
\ce souffler (|lx{gòò-ui}: lit. bambou souffler)
\et *kopi
\eg bamboo (flute)
\el POc
\ea Blust
\dt 07/Jan/2022

\lx goovwû
\ph ŋgo:βû
\dialx GOs
\va gobu
\dialx GO(s)
\is temps
\ps v
\ge retard (en)
\xv e uça goovwû
\dialx GO
\xn il est arrivé en retard
\an uça hêbu
\dialx GO
\at arrivé en avance
\dt 15/Aug/2021

\lx gò-pwãu
\dialx GO PA
\va gò-pwêûp
\dialx BO
\is plantes
\ps n
\ge bambou
\dn sert de percussion lors des danses ; est aussi utilisé pour gratter les |lx{dimwa} dans l'eau
\cf bwevòlò
\dt 08/Feb/2025

\lx gorolo
\dialx BO [BM, Corne]
\va gòròlò
\dialx BO [BM]
\is plantes
\ps n
\ge plante
\sc Triumphetta rhomboïdea
\scf Malvacées
\gb thistle
\dt 27/Aug/2021

\lx goronui
\dialx GOs PA BO
\is reptile
\ps n
\ge grenouille
\bw grenouille (FR)
\dt 20/Mar/2023

\lx gòrui
\dialx GOs
\is matière
\ps n
\ge fer
\dt 29/Jan/2019

\lx gò-tröxi
\is matière
\dialx GOs WE
\ps n
\ge bout de métal
\dn (lit. tronc de métal)
\dt 08/Feb/2025

\lx gò-wõ
\dialx GOs
\va gò-wony
\dialx BO
\is navigation
\ps n
\ge milieu du bateau
\dt 25/Aug/2021

\lx gu
\hm 1
\dialx GOs
\va gun
\dialx PA WEM
\is oiseau
\ps n
\ge pigeon vert
\dn petit pigeon à tête pourpre, vert du dos à la poitrine, jaune sous la queue, taches pourpres sur le ventre
\sc Drepanoptila holosericea
\scf Columbidés
\gb green pigeon
\dt 27/Aug/2021

\lx gu
\hm 2
\dialx GOs BO
\sn 1
\is pêche
\ps n
\ge filoche
\ge brochette
\xv gua-n
\dialx PA
\xn sa filoche
\se gua-nõ
\dialx GOs
\sge filoche de poissons
\sn 2
\is cordes
\ge liane (utilisée pour enfiler)
\ge lacet
\se gua-alaxò
\dialx GOs
\sge lacet de chaussure
\ng forme déterminée: |lx{gua}
\dt 20/Feb/2025

\lx gu
\hm 3
\dialx GOs
\va gun
\dialx BO PA
\is son
\ps v ; n
\ge bruit sourd
\ge faire du bruit
\xv hã gu tõne gunè-n (ou) gunò-n
\dialx PA
\xn nous entendons le bruit qu'il fait (lit. son bruit)
\xv novwö gunè-n, e gun whã nhyô
\dialx PA
\xn quant à son grondement, il retentit comme le tonnerre
\se thu gu
\sge faire du bruit
\se guna loto
\dialx GO BO
\sge le bruit de la voiture
\se gu ne chiò
\dialx GO
\sge le bruit du seau
\se gunò wony
\dialx PA
\sge le bruit du bateau
\se gunò nhyô
\dialx PA
\sge le bruit du tonnerre
\se gunò da ?
\dialx PA
\sge le bruit de quoi ?
\ng forme possédée: |dialx{PA} |lx{gunò-n}, |dialx{BO} |lx{gunè-n}
\gt son bruit
\dt 06/Jan/2022

\lx gu
\hm 4
\dialx GOs BO
\is grammaire_modalité
\ps v.stat.
\ge vrai ; droit ; véritable
\ge autochtone
\xv gu ma hangai
\dialx BO
\xn c'est vraiment grand [Corne]
\se gu hi
\dialx GO
\sge main droite
\se gu we
\dialx GO
\sge eau potable
\et *(n)tuqu
\el POc
\dt 22/Feb/2025

\lx gu
\hm 5
\dialx GOs
\va ku
\dialx PA
\is grammaire_modalité
\ps ASS
\ge ordre ; injonction
\xv gu jaxa
\dialx GOs
\xn ça suffit !
\xv gu himi phwaa-çö
\dialx GOs
\xn tais-toi !
\xv gu nee la nu khõbwe çai çö (prononcé kûçaa-çö)
\dialx GOs
\xn fais ce que je te dis !
\dt 28/Mar/2024

\lx gu
\hm 6
\dialx GOs
\va ku
\dialx PA
\is grammaire_modalité
\ps RESTR
\ge rester à ; ne faire que
\xv la hû, kavwö la vhaa mwa, la gu traabwa wãã na
\xn elles se taisent, elles ne parlent plus, elles restent assises comme ça
\xv nu gu trõne vajama wãã-na
\xn j'ai juste entendu cette histoire comme cela
\se gu-trabwa
\sge assis sans bouger
\dt 02/Nov/2021

\lx gu-
\dialx GOs PA
\sn 1
\is classificateur_numérique
\ps CLF.NUM
\ge filoche de poissons |dialx{GOs PA}
\xv la pe-gu-xè
\xn ils font une filoche de poissons
\sn 2
\is classificateur_numérique
\ps CLF.NUM
\ge file de voitures |dialx{PA}
\dt 22/Feb/2025

\lx gu kiya
\dialx PA
\is grammaire_négation_existentiel
\ps v.EXS
\ge il n'y a pas le moindre
\xv gu kiya cee le
\xn il n'y avait pas le moindre bout de bois
\dt 26/Jan/2022

\lx gu traabwa
\dialx GOs
\is position
\ps v
\ge assis sans bouger
\dt 10/Feb/2023

\lx gu we
\dialx GOs
\is eau
\ps n
\ge eau potable
\dt 16/Aug/2021

\lx gu-hi-n
\dialx PA BO
\va gu-yi-n
\dialx BO
\is corps
\ps n
\ge main droite
\se yi-ny gu-(y)i
\dialx BO
\sge ma main droite
\se gu-(y)i-ny
\dialx BO
\sge ma main droite
\se keni-ny gui (< gu-(y)i)
\dialx BO
\sge mon oreille droite (lit. mon oreille à main droite)
\xv cu tabwa bwa gu-yi-ny
\dialx BO
\xn tu es assis à ma droite
\et *taqu
\eg right
\el POc
\dt 21/Jan/2022

\lx gu-kui
\dialx GOs BO
\va gu-kui, gu-xui
\dialx PA
\is igname
\ps n
\ge igname du chef
\dn comporte deux espèces: une variété blanche |lx{pwalamu} et une violette |lx{pwang} (selon Dubois)
\cf pwalamu, pwang
\dt 05/Jan/2022

\lx gumãgu
\dialx GOs PA BO
\is interaction
\ps v ; n
\ge vrai ; vérité
\ge dire la vérité ; avoir raison
\xv gumãguu-je
\dialx GO
\xn il a raison
\xv gu-m ã gu-m
\dialx PA
\xn tu as raison
\xv ra gumãgu
\dialx BO
\xn c'est vrai
\xv pha(a)-gumãgu !
\dialx PA
\xn fais en sorte que cela soit vrai !
\dt 25/Aug/2023

\lx gu-mwa
\dialx GOs PA BO
\is maison
\ps n
\ge maison ronde ; grande maison
\xv gu mwa i ã na ni mõ Treã Gome
\dialx GOs
\xn notre grande maison dans la chefferie de Treã Gomen
\dt 06/Nov/2021

\lx gunebwa
\dialx PA BO
\is religion
\ps n
\ge lézard
\dn être mythologique qui protège les cultures
\nt selon Corne
\dt 08/Feb/2025

\lx guni
\dialx GOs
\va gunim
\dialx BO PA
\is santé
\ps v
\ge infecté ; infecter (s')
\xv e guni kòò-nu
\dialx GO
\xn j'ai la jambe infectée
\dt 25/Jan/2019

\lx guya
\hm 1
\dialx GOs
\va gwayal
\dialx PA
\is arbre
\ps n
\ge goyavier
\sc Psidium guajava L.
\scf Myrtacées
\dt 27/Aug/2021

\lx guya
\hm 2
\dialx GOs
\is insecte
\ps n
\ge ver de bancoul
\dn long avec des pattes
\dt 08/Feb/2025

\lx ha
\hm 1
\dialx GOs PA
\is mammifères
\ps n
\ge roussette (petite)
\dn vivant dans les rochers
\dt 08/Feb/2025

\lx ha
\hm 2
\dialx GOs
\is action
\ps v.i
\ge cassé (verre)
\xv e ha za
\xn l'assiette est cassée
\ng v.t. |lx{hale}
\gt casser qqch.
\dt 21/Feb/2025

\lx ha
\hm 3
\dialx PA BO
\is grammaire_pronom
\ps PRO 2° pers. PL (sujet)
\ge vous
\xv ha u nòòl !
\xn réveillez-vous !
\dt 25/Aug/2023

\lx hã
\hm 1
\ph hɛ̃
\dialx GOs
\va hããny
\dialx BO
\is navigation
\ps v ; n
\ge gouvernail
\ge pagaie
\ge diriger ; conduire
\ng v.t. |lx{hãnge}
\gt diriger qqch.
\dt 21/Feb/2025

\lx hã
\hm 2
\dialx PA BO
\va hangai
\dialx PA BO
\is caractéristiques_objets
\ps v
\ge gros ; grand
\dt 26/Jan/2019

\lx hã
\hm 3
\dialx BO
\is grammaire_pronom
\ps PRO 1° pers.PL.INCL
\ge nous (plur.)
\dt 24/Aug/2021

\lx haa
\hm 1
\dialx GOs PA BO
\va haa
\dialx GOs PA
\is grammaire_aspect
\ps ASP
\ge sans arrêt ; sans cesse
\xv i ra haa kîga
\dialx PA
\xn il ne fait que rire
\xv i haa-vhaa
\dialx PA
\xn il parle sans cesse
\se haa-nonòm
\dialx PA
\sge penser sans arrêt
\dt 29/Aug/2023

\lx haa
\hm 2
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge CLF des tissus et étoffes végétales
\xv haa-xè ci-xãbwa, haa-xè thrô, haa-xè murò
\dialx GOs
\xn une étoffe, une natte, une couverture
\xv haa-xè, haa-tru, haa-kò, haa-pa, haa-ni ci-xãbwa, etc.
\dialx GOs
\xn un, deux, trois, quatre, cinq étoffes, etc.
\xv haa-ru maû ci-xãbwa
\dialx PA
\xn deux rouleaux d'étoffe
\xv e tree-kuzaò haa-tru mãdra
\dialx GO
\xn il reste deux pièces de tissu en plus
\dt 22/Feb/2025

\lx haal
\dialx PA BO
\is navigation
\ps v
\ge ramer
\xv nu haale wony
\dialx BO
\xn je fais avancer le bateau à la rame
\se a-haal
\dialx BO
\sge rameur
\se ba-haal
\dialx BO
\sge rame
\ng v.i. |lx{haale}
\gt conduire qqch. à la rame
\et *xalo(r)
\el PSO
\ea Geraghty
\cf paaba
\ce ramer
\dt 21/Feb/2025

\lx hããma
\dialx GOs
\va hããmal
\dialx PA BO
\is sentiments
\ps v
\ge émerveiller (s') ; émerveillé (être) ; admirer ; admiratif
\xv nu hããmali-je
\dialx PA
\xn je l'admire
\ng v.t. |lx{hããmali}
\cf gaajò ; gaajòn
\ce s'étonner, être surpris
\dt 20/Mar/2023

\lx hããni
\dialx BO
\is société
\is fonctions_naturelles
\ps v
\ge voir (respectueux)
\dn registre respectueux pour le Grand Chef
\xv nu hãã-je
\dialx BO
\xn je l'ai vu
\ng |lx{hãã-} + suffixe pronominal objet ; |lx{hããni} + objet nominal
\nt selon BM
\dt 16/Feb/2025

\lx hããny
\dialx PA BO
\is navigation
\ps v ; n
\ge diriger (voiture)
\ge barrer (bateau)
\ge gouvernail ; volant
\xv i phe hããny
\xn il prend le gouvernail
\dt 25/Aug/2021

\lx haa-uva
\dialx GOs PA BO
\is taro
\ps n
\ge extrémité inférieure du pétiole de taro d'eau
\dn il est pressé dans la bouche du nouveau-né par la mère
\dt 02/Feb/2019

\lx haavwu
\ph 'ha:βu
\dialx GOs PA BO
\va haapu
\is cultures_champ
\ps n
\ge jardin (au bord d'une rivière) ; massif de culture humide (comme les taros)
\ge terre alluvionnaire
\dn au bord de rivière et au pied des montagnes
\dt 17/Feb/2025

\lx haaxa
\dialx BO
\sn 1
\is instrument
\ps n
\ge baguette ; canne
\sn 2
\is chasse
\ps n
\ge armature de piège
\nt selon Corne
\dt 21/Feb/2025

\lx hããxa
\ph hɛ̃:ɣa
\dialx GOs PA BO
\va haaka
\dialx BO
\is interaction
\ps v
\ge craintif ; craindre
\ge peureux ; avoir peur
\ge hésiter
\xv nu hããxa ne nu a-da-ò
\dialx GOs
\xn j'ai peur de monter (vers toi)
\xv nu hããxa na i poxèè thûã
\dialx PA
\xn j'ai peur qu'il ne joue des tours
\xv i hããxe-je
\dialx BO
\xn il a peur de lui
\ng v.t. |lx{hããxe}
\gt craindre qqn.
\dt 21/Feb/2025

\lx hããxe
\ph hɛ̃:ɣe
\dialx GOs BO
\is interaction
\ps v.t.
\ge craindre (qqn., qqch.)
\xv kêbwa (çö) hããxe-bî mã ãbaa-nu
\dialx GOs
\xn n'aie pas peur de moi et de mon frère
\se pha-hããxe
\sge effrayer, faire peur à qqn.
\se phaza-hããxe
\sge menacer qqn., faire très peur à qqn.
\dt 21/Feb/2025

\lx haaxo
\dialx GOs PA BO
\is insecte
\ps n
\ge ver de bancoul
\dn gros, blanc et comestible
\dt 08/Feb/2025

\lx hãba
\dialx PA BO
\sn 1
\is grammaire_préposition
\ps PREP.BENEF
\ge pour (nécessaire à)
\xv ha-ò wony ja ne-wo hãba i jo
\dialx BO
\xn ce bateau, nous l'avons fait pour toi
\xv ka phe-mi cee o hãba o yaai !
\dialx BO
\xn apporte du bois pour le feu
\sn 2
\is grammaire_préposition
\ps PREP.BENEF
\ge nécessaire à qqn.
\xv na yu phe xa hãba i yo, phe-xa hãba i yo Teã-mã !
\dialx PA
\xn prends ce qu'il te faut (pour te restaurer), prends ce qu'il te faut Teã-mã
\dt 11/Jun/2024

\lx hãbaö
\dialx GOs
\va hãba
\dialx PA
\is grammaire_modalité
\ps v.MODAL ; n
\ge utile ; nécessaire ; falloir
\xv kixa na hãbaö vwo çö mhã phe-uça lã-na
\dialx GO
\xn il était inutile d'apporter toutes ces choses
\xv kixa na hãbaö ẽnõ-ã !
\dialx GO
\xn cet enfant est inutile (= il n'aide pas)
\dt 11/Jun/2024

\lx hãbe
\dialx GOs PA BO
\is corps
\ps n
\ge aisselle
\xv phwe-hãbee-je
\dialx GOs
\xn son aisselle
\xv hãbee-n
\dialx PA
\xn son aisselle
\se pu-hãbe
\sge poils des aisselles
\dt 15/Sep/2021

\lx hãbira
\dialx GOs
\is portage
\ps v
\ge porter sous le bras
\xv nu phe ci-xãbwa po nu kha-hãbira
\xn je prends l'étoffe et je l'emporte sous le bras
\dt 24/Jan/2018

\lx hãbo
\dialx GOs
\is discours
\ps v
\ge jurer ; promettre
\ge prêter serment
\dt 15/Aug/2021

\lx habwa
\dialx GOs
\is action_corps
\ps v
\ge soulever (lentement)
\xv habwa hii-çö
\xn lève ton bras
\dt 20/Aug/2021

\lx hãda
\dialx GOs PA BO
\va (h)ada
\is grammaire_restrictif
\ps ADV ; QNT
\ge seul ; tout seul ; seulement
\xv ili hãda
\xn eux deux seuls
\xv lhi zo(ma) xa yuu hãda mwã lhi
\dialx GO
\xn ils (duel) vivront seuls
\xv hai ! za inu hãda mã ãbaa-nu nhye bî a
\dialx GO
\xn non! c'est seulement ma sœur et moi qui sommes parties
\xv hai ! inu mã ãbaa-nu hãda nhye bî a
\dialx GO
\xn non! c'est seulement ma sœur et moi qui sommes parties
\xv a-xè (h)ãda pòi-n
\dialx PA
\xn il n'a qu'un enfant
\xv axe za hine-(h)ãdaa-ni xo kani
\dialx GO
\xn mais seul le canard le savait
\xv nu tròòli-(h)ãdaa-ni thixèè ala-xòò-nu
\dialx GO
\xn je n'ai trouvé qu'une seule chaussure
\ng v.t. |lx{V-hãdaa-ni} dans une construction verbale complexe
\dt 24/Feb/2025

\lx hãgana
\ph 'hɛ̃ŋgaɳa, 'hɛ̃ŋgana
\dialx GOs BO PA
\is temps_deixis
\ps ADV
\ge maintenant ; aujourd'hui ; tout à l'heure
\xv hãgana na mi hovwo
\dialx GO
\xn tout à l'heure quand nous mangerons
\xv hãgana novwö kui zèèno
\dialx GO
\xn maintenant que l'igname est mûre
\se hãgana hâ
\sge maintenant
\se bala hãgana
\sge jusqu'à maintenant (Dubois)
\dt 21/Feb/2025

\lx hãnge
\hm 1
\dialx PA
\is action_corps
\ps v
\ge lever le bras en menaçant
\dt 25/Jan/2019

\lx hãnge
\hm 2
\dialx GOs
\is navigation
\ps v
\ge diriger le bateau/voiture ; barrer ; conduire
\xv e hãnge wõ
\xn il dirige le bateau
\se ba-hãnge wõ
\sge gouvernail ; barre
\ng v.i. |lx{hã}
\gt barrer, diriger
\dt 22/Feb/2025

\lx hãgu
\ph 'hɛ̃ŋgu
\dialx GOs
\va hêgu
\dialx GOs
\is plantes
\ps n
\ge roseau
\ge bambou
\dn petit, pousse au bord de la rivière
\dt 08/Feb/2025

\lx hai
\hm 1
\dialx PA BO
\va ha, ai
\dialx PA BO
\va (h)a
\dialx GO(s)
\is grammaire_conjonction
\ps CNJ
\ge ou bien
\xv e hivwine khõbwe e za mõlò a u mã
\dialx GO
\xn il ne sait s'il est encore vivant ou s'il est mort
\xv i baxòòl ha i phõng ?
\dialx BO
\xn il est droit ou tordu ? (BM)
\dt 22/Dec/2021

\lx hai
\hm 2
\dialx GOs PA BO
\va hayai
\dialx GO(s)
\is grammaire_assertif
\ps NEG
\ge non (en réponse à une question)
\an èlò
\at oui
\dt 15/Sep/2021

\lx hai
\hm 3
\dialx GOs BO PA
\is grammaire_quantificateur_degré
\ps QNT
\ge très ; trop
\xv hai nèèng
\dialx BO
\xn il y a des nuages ; c'est très nuageux
\cf haivwö
\ce il y en a
\cf ge-le
\dialx GOs
\ce il y a
\cf haivwö
\ce être nombreux
\dt 22/Feb/2025

\lx hai-khãbu
\dialx GOs
\va hai'xãbu
\dialx GO(s)
\is température
\ps v.stat
\ge très froid
\dt 03/Feb/2025

\lx hai-trirê
\ph hai'ʈiɽẽ
\dialx GOs
\va haitritrê
\dialx GO(s)
\is fonctions_naturelles
\ps v
\ge suer ; transpirer
\dn (lit. très chaud)
\dt 09/Feb/2025

\lx haivwö
\dialx GOs BO PA
\va hai
\dialx BO
\va haipo
\dialx arch.
\is grammaire_quantificateur_mesure
\ps v.QNT
\ge beaucoup ; nombreux ; trop
\xv haivwö chaamwa i nu
\dialx GO
\xn j'ai beaucoup de bananes
\xv haivwö la mãla phwe-meevwu Maluma xa la uça
\dialx GO
\xn beaucoup de gens du clan Maluma sont venus
\xv lha cii haivwö
\xn ils sont très nombreux
\xv lha mhã haivwö
\xn ils sont trop nombreux
\xv lha pò haivwö nai la
\xn ils sont un peu plus nombreux qu'eux
\an hoxèè
\at peu
\dt 16/Feb/2025

\lx hajaa-wõ
\dialx GOs
\is navigation
\ps n
\ge balancier de pirogue
\gb outrigger boom
\dt 26/Jan/2019

\lx hala
\dialx GOs
\va halan
\dialx PA BO [BM]
\is caractéristiques_objets
\ps v
\ge gâté ; abîmé
\xv i halan (a) kui
\dialx PA BO
\xn cette igname est gâtée (trop vieux)
\dt 20/Mar/2023

\lx halalaò
\dialx PA
\is caractéristiques_objets
\ps v
\ge lâche (être) ; grand
\xv i (mha) halalaò jene patolõ
\dialx PA
\xn il est lâche ce pantalon
\dt 26/Jan/2019

\lx halaò
\dialx GOs
\is santé
\ps v
\ge peler (peau)
\xv e halaò ci-hii-nu ko/xo we tòò
\dialx GO
\xn la peau de ma main pèle à cause de l'eau brûlante
\xv e halaò ci-hii-je ko we tòò
\xn la peau de ma main pèle à cause de l'eau brûlante
\dt 10/Jan/2022

\lx hale
\dialx GOs
\is action
\ps v.t.
\ge casser (verre, assiette)
\xv e hale za
\xn elle a cassé l'assiette
\ng v.i. |lx{ha}
\gt être cassé
\dt 05/Jan/2022

\lx halelewa
\ph 'hale'lewa
\dialx GOs
\va haleleang
\dialx PA WE
\va alelewa, halelea
\dialx PA
\is insecte
\ps n
\ge cigale (petite et verte)
\se ba-thi-halelewa
\sge bâton à glu (sur lequel on colle les cigales)
\dt 29/Jan/2019

\lx hane
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge sans
\xv yo kûûni pwa-caai hane yo tilòò-ny ?
\xe tu manges les pommes canaques sans me l’avoir demandé ?
\dt 27/Aug/2023

\lx haòm
\dialx PA
\va aom
\dialx PA BO
\is caractéristiques_objets
\ps v.stat
\ge léger
\an pwalu, pwaalu
\at lourd
\dt 03/Feb/2025

\lx hau
\hm 1
\dialx PA
\is habillement
\ps n
\ge coiffure (tout type)
\xv hau-n
\dialx PA
\xn son chapeau
\xv i phu hau-n
\dialx PA
\xn il enlève son chapeau
\dt 26/Jan/2019

\lx hau
\hm 2
\dialx PA
\is plantes
\ps n
\ge liane (pour la construction)
\dn utilisée pour attacher les gaulettes de la maison
\dt 08/Feb/2025

\lx haulaa
\ph 'haula:
\dialx GOs PA BO
\is caractéristiques_personnes
\ps v
\ge faire l'intéressant ; faire le malin ; hautain
\xv i haulaa
\dialx PA
\xn il est hautain/orgueilleux
\dt 22/Feb/2025

\lx hauva
\dialx GOs PA BO
\is coutumes
\ps n
\ge don du clan paternel au clan maternel de l'épouse ou de l'époux
\dn coutume cérémonielle de deuil
\xv hovwo za ? – Hovwo ponga hauva
\xn quel type de nourriture ? – De la nourriture pour la levée de deuil
\se thîni hauva
\dialx GO
\sge enclos pour y déposer les biens coutumiers
\cf mãû bweera
\ce cérémonie coutumière de deuil pour une femme (don du mari au clan paternel de l'épouse)
\dt 24/Feb/2025

\lx havaa-n
\dialx PA BO
\va hapa
\dialx PA
\sn 1
\is topographie
\ps n.loc
\ge bord
\ge extrémité (champ, etc.)
\sn 2
\is cultures
\ps n
\ge billon ; côté femelle du massif d'ignames
\sn 3
\is tressage
\ps n.loc
\ge extrémité de la natte
\dn là où les fibres dépassent
\dt 03/Feb/2025

\lx havade
\dialx GOs
\is bananier
\ps n
\ge feuille sèche de bananier
\xv havade chaamwa
\xn feuille sèche de bananier
\dt 20/Aug/2021

\lx hava-hi
\dialx GOs
\ph 'haβa'hi
\va hava-hii-n
\dialx BO PA
\is corps_animaux
\ps n
\ge aile
\xv hava-hii-je
\xn son aile
\xv pu hava i nu
\dialx GOs
\xn j'ai des ailes (conte)
\et *kapa(k)
\el POc
\dt 02/Jan/2022

\lx havan
\dialx BO
\is plantes_processus
\ps v ; n
\ge flétri ; fané ; mort ; feuille fanée
\cf mènõ
\dialx GOs
\ce flétri ; fané
\dt 10/Jan/2022

\lx haveveno
\dialx PA
\va haveno
\dialx BO
\is insecte
\ps n
\ge grillon ; criquet
\dt 29/Jan/2019

\lx havwo
\ph 'haβo
\dialx GOs
\is arbre
\ps n
\ge bois de rose
\dn il pousse en bord de mer et a des fleurs jaunes semblables à celles du bourao
\sc Thespesia populnea
\scf Malvacées
\gb Umbrella tree
\et *qaqaparu
\el POc
\dt 27/Aug/2021

\lx havwò
\ph 'ha:βɔ
\dialx GOs
\va hawòl
\dialx BO
\va ha
\dialx GA
\is fonctions_naturelles_animaux
\ps v
\ge aboyer
\xv e ha-e da ? (ou) e havwò-e da ?
\xn pourquoi aboie-t-il?
\xv nhye kuau e huu, ti nhye e ha (ou) ti nhye e havwò ?
\xn ce chien qui a mordu, est-ce celui qui aboie?
\xv la ha i loto (ou) la havwò i loto
\xn ils aboient contre les voitures
\dt 22/Feb/2025

\lx hãwã
\dialx GOs
\va haawa
\dialx BO [Corne]
\is coutumes
\ps n
\ge effet (d'une parole, d'un médicament, etc.)
\ge conséquence ; marque
\dt 19/Aug/2021

\lx haxa
\dialx GOs
\va haa
\dialx GOs
\is grammaire_aspect
\ps ASP
\ge ne plus
\dn associé à une négation
\xv haxa kòi-je mwã na êńa
\xn il n'est plus là maintenant
\xv kavwö me haa khõbwe xa vhaa i me
\xn nous ne savons plus formuler nos paroles
\dt 08/Feb/2025

\lx hãxãî
\ph hɛ̃'ɣã.î
\va hangai, angai
\dialx PA BO
\va hã
\dialx PA
\dialx GOs PA BO
\is caractéristiques_objets
\ps v.stat
\ge grand ; gros ; volumineux
\dt 03/Feb/2025

\lx haxe
\dialx GOs PA BO
\va axe, hake
\dialx GO(s) PA BO
\va age
\dialx BO
\sn 1
\is grammaire_conjonction
\ps CNJ
\ge et ; mais ; mais (entre-temps ; pendant ce temps)
\xv la phweeku, hake gi ã po-ka aazo-ã
\dialx GOs
\xn ils bavardaient, pendant ce temps, le fils du chef pleurait
\xv nu mõgu hayu, haxe nu tròòli mã
\dialx GO
\xn je travaille quand même mais je suis malade / bien que je sois malade
\xv kain age
\dialx BO
\xn ensuite
\xv age novu
\dialx BO
\xn (ce)pendant
\sn 2
\is grammaire_conjonction
\ps CONCESSION
\ge bien que ; en dépit du fait que
\xv whili-je-da mwa xo ã-e, jo u a kai-je, haxe nhye e hããxe-je
\dialx GO
\xn elle le suit, en dépit du fait qu'elle a peur de lui
\sn 3
\is grammaire_conjonction
\ps CNJ
\ge d'une part … d'autre part
\dt 21/Feb/2025

\lx haxo
\dialx GOs
\is poisson
\ps n
\ge hareng
\dt 29/Jan/2019

\lx hayu
\dialx GOs BO
\sn 1
\is grammaire_modalité
\ps ADV ; MODIF
\ge indécis
\ge sans retour ; sans limites
\ge au hasard ; sans but ; de-ci de-là
\ge quelconque ; n'importe comment ; en vain
\xv kavwö lha pe-khila-vwo hayu
\dialx GOs
\xn ils ne sont pas allés en chercher n'importe où
\xv mhènõ-nyama xa bulu hayu
\dialx GOs
\xn les travaux qui sont innombrables (lit. sans fin)
\se nee hayu
\sge faire sans but
\se êgu hayu
\sge un homme quelconque
\se e pe-a hayu
\sge il va sans but
\sn 2
\is grammaire_modalité
\ps ADV ; MODIF
\ge quand même ; faire à contre-cœur |dialx{GOs}
\se pe-wa hayu
\sge chante quand même
\xv nee-hayu-ni !
\xn fais-le quand même !
\ng v.t. |lx{V-hayu-ni} dans une construction verbale complexe
\cf lòlòò
\dialx GOs
\ce sans but; au hasard
\dt 21/Feb/2025

\lx haza cee
\dialx GOs
\is maison
\ps n
\ge gaulettes qui retiennent la paille
\dn elles sont posées sur la paille et sont attachées avec des lianes
\dt 19/Dec/2021

\lx haza nu
\ph haða ɳu
\dialx GOs
\va ha(r)a-nu
\dialx PA
\is arbre_cocotier
\ps n
\ge fibre de feuille de cocotier
\ge partie inférieure de la palme de cocotier
\dn base arrondie de la palme là où elle s'attache au tronc
\ge gaine sèche de l'inflorescence du cocotier
\dn après la floraison
\dt 08/Feb/2025

\lx haze
\hm 1
\dialx GOs
\va hale
\dialx PA BO
\is caractéristiques_objets
\ps v
\ge différent ; à part ; à l'écart ; bizarre
\xv na-mi xa pwaixe ne haze
\dialx GO
\xn donne-moi autre chose
\xv ra hale tho-ny na ni cocova mèni
\dialx PA
\xn mon cri est différent de tous ceux des autres oiseaux
\xv a hale bwa-n xo a hale egu-n
\dialx PA
\xn sa tête part d’un côté, son corps de l’autre
\xv avwõno la hê-hale-ã
\dialx BO
\xn les clans ennemis
\xv ra pe-hale va i la
\dialx BO
\xn leurs paroles sont dispersées
\dt 18/Dec/2021

\lx haze
\hm 2
\dialx GOs
\va hale
\dialx BO
\is action
\ps v
\ge sécher (au soleil)
\ng v.t. |lx{hazee-ni} |dialx{GOs}, |lx{halee-ni} |dialx{PA, BO}
\gt sécher qqch. (au soleil)
\dt 21/Feb/2025

\lx haze na
\ph haðe ɳa
\dialx PA
\va hale na
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge si (contrefactuel)
\xv hale na yo tilòò, ye nu ru na hi-m va i la
\dialx PA
\xn si tu l'avais demandé, je te l'aurais donné
\dt 13/Aug/2021

\lx hazoo
\dialx GOs
\va halòòn
\dialx PA BO
\is parenté
\is société
\ps v
\ge marier (se) (pour une femme, prendre époux)
\xv la halòòn
\dialx PA
\xn ils se marient
\se phe alòò-n
\dialx PA
\sge se marier (prendre époux)
\cf phè thoomwã
\dialx GOs
\ce prendre une femme
\cf e-mõû
\ce époux
\dt 18/Apr/2024

\lx he
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge frotter
\ge procréer ; saillir (un animal)
\sn 2
\is action_corps
\ps n
\ge accouplement
\xv hêê-hee-çö
\dialx GOs
\xn le fruit de ton accouplement
\se pa-he-ni
\sge saillir (un animal)
\se ba-he
\sge bois avec lequel on allume le feu par friction
\se ce-he
\sge bois frotté pour allumer le feu
\se yaai-he, yaa-he
\dialx PA
\sge feu obtenu par friction de morceaux de bois
\dt 04/Nov/2021

\lx hè-
\dialx PA
\is grammaire_interpellation
\ps n.INTJ
\ge interpellation
\xv hè-m
\dialx PA
\xn hé! toi
\xv hè-yò
\dialx PA
\xn hé! vous deux
\xv hè-zò
\dialx PA
\xn hé! vous (plur.)
\dt 03/Feb/2025

\lx hê
\dialx GOs
\is préfixe_sémantique
\ps n
\ge prise (à la pêche, la chasse)
\xv çö êgi kamwêlè ? – Hê-pwe
\xn tu les as attrapés comment ? – À la pêche à la ligne
\xv hê-pwee-nu la lapya
\dialx GO
\xn j'ai pêché ces tilapias à la ligne
\se hê-do
\sge prise à la sagaie
\se hê-pwiò
\sge prise au filet
\dt 24/Feb/2025

\lx hê nu
\ph hẽ ɳu
\dialx GOs
\is arbre_cocotier
\ps n
\ge chair de coco
\dn (lit. contenu de la noix de coco)
\dt 09/Feb/2025

\lx hêbu
\dialx GOs
\va hêbun
\dialx PA BO
\wr A
\is grammaire_préposition_locative
\ps PREP
\ge devant ; avant
\xv hêbu nai nu
\dialx GO
\xn devant/avant moi
\xv hêbun i yu
\dialx PA
\xn devant toi
\xv hêbun ni nye mwa
\dialx PA
\xn devant cette maison
\wr B
\ps ADV
\sn 1
\is grammaire_adverbe
\ps ADV.LOC
\ge devant
\ge premier
\xv a hêbu !
\dialx GO
\xn va/pars devant/le premier !
\xv i ra u a hêbun wo/xo Teã-ma
\dialx PA
\xn Teã-ma marche devant
\sn 2
\is temps
\ps ADV.TEMP
\ge avant ; en avance ; d'abord
\xv e uça hêbu
\xn il est arrivé en avance
\an mura
\dialx GOs
\at après
\an mun
\dialx PA
\at après
\an mu
\at derrière, après
\wr C
\is temps
\ps MODIF
\ge d'avant ; d'autrefois
\xv ije ce pò tree mãla hêbu
\xn il est le fruit des jours passés/d'autrefois
\wr D
\is grammaire_conjonction
\ps CNJ
\ge avant
\se hêbu na
\sge avant de/que
\xv hêbu na ni hovho
\dialx PA
\xn avant de manger
\xv hêbu na nu hovwo
\dialx GOs
\xn avant que je ne mange
\dt 22/Feb/2025

\lx hê-cò
\dialx GO
\is corps
\ps n
\ge gland (du pénis)
\dt 21/Aug/2021

\lx hê-drewaa
\dialx GOs
\is pêche
\ps n
\ge prise à la nasse
\xv hê-drewaa-nu la kula
\dialx GO
\xn j'ai pêché ces crevettes à la nasse
\dt 24/Jan/2018

\lx hêêbwi
\dialx BO
\is action_corps
\ps v
\ge fermer la main
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx hêêdo
\dialx GOs WEM BO PA
\is oiseau
\ps n
\ge "grive" ; oiseau-moine
\sc Philemon diemenensis
\scf Méliphagidés
\dt 27/Aug/2021

\lx hêê-kiò
\is corps
\dialx GOs
\ps n
\ge viscères
\xv hêê-kiò-n
\dialx PA
\xn ses viscères
\dt 12/Jan/2022

\lx heela
\dialx GOs BO
\sn 1
\is mouvement
\ps v
\ge glisser
\sn 2
\is caractéristiques_objets
\ps v
\ge glissant
\ge lisse (cheveux)
\xv e heela
\dialx GOs
\xn la route est glissante
\dt 20/Aug/2021

\lx hêgi
\ph hẽŋgi
\dialx GOs PA BO
\is richesses
\ps n
\ge monnaie traditionnelle
\se hêgi pulo
\dialx PA
\sge monnaie blanche faite de coquillages blancs (selon Charles Pebu-Polae)
\se hegi mhõõng
\dialx PA
\sge monnaie offerte avec un rameau de n'importe quel arbre; cette monnaie est faite de coquillages enfilés sur une cordelette et séparés par des nœuds (selon Charles Pebu-Polae)
\cf mwani [empr. à money]
\ce argent
\dt 22/Feb/2025

\lx hêgo
\ph hẽŋgo
\dialx GOs PA BO
\is instrument
\ps n
\ge bâton ; canne
\xv e phe hêgoo-je
\dialx GO
\xn il a pris sa canne
\dn symbole d'ancienneté
\dt 17/Mar/2023

\lx hê-jige
\dialx GOs
\va hê-jigel
\dialx PA
\is armes
\ps n
\ge cartouche (de fusil)
\dt 08/Jan/2022

\lx hê-ka
\dialx GO
\is plantes
\ps n
\ge brède
\sc Solanum nigrum L.
\scf Solanacées
\dt 27/Aug/2021

\lx hê-kee
\dialx GOs
\is pêche
\is chasse
\ps n
\ge prise à la pêche ou la chasse
\dn ce terme réfère à toute prise en général sans toujours référer au contenu d'un panier
\xv ge le xa hê-kee-çö ? – Hû, ge le drube, ge le nõ
\dialx GOs
\xn as-tu pris quelque chose ? – Oui, il y a un cerf, il y a des poissons
\xv hê-kee-nu xa nu taaja xo do
\dialx GOs
\xn j'ai pris cela en piquant à la sagaie
\xv hê-kee-nu la lapya nu khai pwiò
\dialx GOs
\xn j'ai pris ces tilapias (lit. en tirant la senne)
\xv hê-kee-nu la nõ nu pawe pwiò
\dialx GOs
\xn j'ai pris ces poissons au filet épervier (lit. en lançant le filet épervier)
\xv hê-kee-çö nya lapya ?
\dialx GOs
\xn c'est toi qui as pris ces tilapias ?
\dt 24/Feb/2025

\lx hê-kênii
\dialx GOs
\is habillement
\ps n
\ge boucle d'oreille
\ge parure d'oreille
\dt 24/Aug/2021

\lx hê-kòlò
\dialx GOs PA BO
\va hê-xòlò
\dialx GO(s)
\sn 1
\is parenté
\ps n
\ge enfant de frères et de cousins (femme parlant)
\xv hê-kòlò-ny
\dialx PA
\xn mes neveux/nièces (enfants de frères, femme parlant)
\xv li a-pe-hê-kòlò-li
\dialx BO
\xn ils sont tante et neveux/nièces (enfants de frères)
\sn 2
\is parenté
\ps n
\ge famille ; allié
\xv avwònò la hê-kòlò-ã
\dialx BO
\xn les clans alliés
\xv pe-hê-xòlò-lò
\dialx GO
\xn ils (triel) sont de la même famille
\dt 08/Jan/2022

\lx hèlè
\ph hɛlɛ
\dialx GO PA BO
\sn 1
\is outils
\ps n
\ge couteau
\ge scie
\xv hèlè ã ãbaa-nu
\dialx GO
\xn le couteau de mon frère
\xv hèlè-nu
\dialx GO
\xn mon couteau
\xv hèlè-ny
\dialx BO
\xn mon couteau
\xv hèlè-ny hèlè-cee
\dialx PA
\xn ma tronçonneuse (couteau à bois)
\se hèlè pòńõ
\dialx GO
\sge petit couteau
\se hèlè pobe
\dialx BO
\sge petit couteau
\se hèlè hangai
\dialx PA
\sge sabre d'abatis
\sn 2
\is action
\ps v
\ge couper
\ge scie(r)
\ng v.t. |lx{hèlèèni}
\gt couper qqch.
\sn 3
\is jeu
\ps n
\ge figure du jeu de ficelle ("la scie")
\bw hele (POLYN) (POc *sele)
\dt 21/Feb/2025

\lx hèlè khawali
\dialx GOs
\va hèlè hã
\dialx WE
\is outils
\ps n
\ge sabre d'abattis
\dt 18/Mar/2023

\lx hê-me
\dialx GOs
\is corps
\ps n
\ge globe oculaire
\dt 24/Jan/2019

\lx hê-nawo
\is échanges
\dialx GOs PA BO
\ps n
\ge dons coutumiers
\dt 12/Jan/2022

\lx heni
\dialx GOs
\va henim
\dialx PA BO [BM]
\sn 1
\is action_corps
\ps v
\ge taper avec un bâton ; gauler
\dn pour frapper les roussettes ou gauler les grappes de noix de coco
\xv i henime nu
\xn il gaule la noix de coco avec un bâton (pour la faire tomber)
\ng v.t. |lx{henime}
\sn 2
\is armes
\ps n
\ge bâton pour lancer
\dt 20/Mar/2023

\lx hênu
\ph hẽɳu
\dialx GOs
\va hînu
\dialx PA
\va hênuul
\dialx BO
\sn 1
\is lumière
\ps n
\ge ombre (portée d'un animé)
\ge reflet
\xv hênuã cee
\dialx GO BO
\xn l'ombre d'un arbre
\sn 2
\is caractéristiques_personnes
\ps n
\ge image ; photo ; portrait (représentation)
\xv e nõle hênuã-nu na ni vea
\dialx GO
\xn il a vu mon reflet dans la vitre
\xv e alö-le hênuã-je na ni we
\dialx GO
\xn elle regarde son reflet dans l'eau
\xv hînua-n
\dialx PA
\xn son ombre ; son image ; sa photo
\xv pòi-ny nhye hînu
\dialx PA
\xn c'est une photo que j'ai prise
\xv hînua-ny
\dialx PA
\xn ma photo (me représentant)
\ng forme déterminée: |lx{hênuã}
\et *(q)anunu
\el POc
\dt 22/Feb/2025

\lx hênuã-me
\ph hẽɳuɛ̃me
\dialx GOs
\is habillement
\ps n
\ge visière
\dn faite d'une feuille, lit. ombre de l'œil
\dt 08/Feb/2025

\lx hèò
\dialx PA
\va -(è)ò
\dialx PA
\is grammaire_démonstratif
\ps DEM
\ge réfère au passé
\dn passé plus lointain dans le temps que |lx{-ò}
\xv ia je loto hèò ?
\dialx PA
\xn où est ta voiture d'avant ?
\xv ia loto i çö-ò ?
\dialx GO
\xn où est ta voiture d'avant ?
\xv liè emõû-n mali-èò
\dialx PA
\xn ces deux époux (référence à un passé plus lointain)
\xv liè emõû-n mãli-ò
\dialx PA
\xn ces deux époux (référence à un passé moins lointain)
\cf -la-èò
\ce pluriel
\dt 08/Jan/2022

\lx hê-pwe
\dialx GOs
\is pêche
\ps n
\ge prise à la ligne
\xv hê-pwee-nu la lapya
\dialx GO
\xn j'ai pêché ces tilapias à la ligne
\xv hê-pwee-ny (a) liã nõ
\dialx PA
\xn j'ai pêché ces poissons à la ligne
\dt 21/Feb/2025

\lx hê-pwiò
\dialx GOs
\is pêche
\ps n
\ge prise au filet
\xv hê-pwiò-nu la nõ
\dialx GO
\xn j'ai pêché ces poissons au filet
\xv çö êgi kamwene ? – Hê-pwe ; hê-pwiò ; hê-do
\xn tu les as attrapés comment ?  – À la ligne ; à la senne; à la sagaie
\dt 24/Feb/2025

\lx hê-phii-n
\dialx BO
\is corps
\ps n
\ge testicules
\dt 21/Aug/2021

\lx hê-thini
\dialx GOs BO
\is société_organisation
\ps n
\ge contenu de la barrière
\ge offrandes de nourriture dans les coutumes
\dt 18/Aug/2021

\lx hevwe
\ph heβe
\dialx GOs
\va yaave
\dialx PA BO
\is igname
\ps n
\ge igname longue
\nt selon Dubois et Charles Pebu-Polae
\dt 26/Mar/2022

\lx hê(ê)-
\dialx GOs
\va hêê-n
\ph hẽ:n
\dialx PA BO
\is préfixe_sémantique
\ps n
\ge contenu de
\xv hêê-he-çö
\dialx GO
\xn le fruit de ton accouplement
\xv a-niza hê-loto ?
\dialx GO
\xn combien de personnes contient cette voiture ?
\xv a-niza hê ?
\dialx GO
\xn combien de personnes y a-t-il ? (dans un contenant : maison, voiture)
\xv po-niza hê-loto ?
\dialx GO
\xn combien (de personnes) contient la voiture ?
\xv kixa hê-loto
\dialx GO
\xn il n'y a personne dans la voiture
\xv kixa hê
\dialx GO
\xn il n'y a rien dedans
\xv hêê-n
\dialx PA
\xn son contenu
\xv kixa/kiya hêê-n
\dialx PA
\xn il n'est pas chargé (réfère à un fusil)
\xv kixa/kiya hê-mwa
\dialx PA
\xn la maison est vide d'objet
\xv kixa hêê-n
\dialx BO
\xn c'est vide ; il n'y a rien dedans
\se hê-wõny
\dialx PA
\sge les marchandises ou les choses dans la cale du bateau
\se hê-mwa
\dialx BO
\sge l'intérieur/le contenu de la maison
\se hê-mwa-iyu
\sge marchandise
\se hê-êgu
\dialx BO
\sge la personnalité d'un homme
\dt 22/Feb/2025

\lx hi
\hm 1
\dialx PA
\is bois_travail
\ps v
\ge écorcer
\xv i hi zòòni
\dialx PA
\xn il écorce des niaoulis
\ng v.t. |lx{hili}
\gt écorcer
\dt 21/Feb/2025

\lx hi
\is habillement
\hm 2
\dialx GOs BO
\ps n
\ge morceau d'étoffe
\dn plus fine que |lx{mada}
\dt 08/Feb/2025

\lx hi-
\hm 3
\dialx PA
\is grammaire_préposition
\ps n.BENEF
\ge à ; pour
\xv nu tèè na hi-m nye wõjo-ny
\dialx PA
\xn je te prête mon bateau
\xv na hi-n
\dialx PA
\xn donne-le-lui
\xv la ra u na hi Kaawo
\dialx PA
\xn elles les donnent à Kaawo
\mr grammaticalisation de |lx{hii} main
\dt 21/Feb/2025

\lx hî
\dialx PA BO
\is grammaire_démonstratif
\ps DEIC.1
\ge ceci ; ce …-ci
\xv hî tèèn
\dialx BO
\xn ce jour-ci
\xv wãã-na da hî?
\dialx PA
\xn qu'est-ce que ceci ?
\dt 26/Mar/2022

\lx hia
\dialx PA BO
\is danse
\ps n
\ge crier en dansant
\nt selon Corne
\dt 26/Mar/2022

\lx hibil
\dialx PA BO
\is caractéristiques_personnes
\ps v.stat
\ge vif ; agile ; dynamique
\cf zibi
\dialx GOs
\dt 27/Oct/2023

\lx hing
\dialx PA BO
\sn 1
\is sentiments
\ps v
\ge haïr ; détester
\xv i hinge-nu
\dialx PA
\xn il me déteste
\ng v.t. |lx{hinge}
\sn 2
\is nourriture
\ps v
\ge dégoûté ; faire le difficile
\xv i a-hing
\dialx PA
\xn il fait le difficile
\cf kue
\dialx GOs
\ce détester ; refuser
\dt 22/Feb/2025

\lx hi-hõbwò
\dialx GOs
\is habillement
\ps n
\ge manche
\dn (lit. bras-vêtement)
\xv e zugi hi-hõbwò
\xn elle retrousse ses manches
\dt 09/Feb/2025

\lx hi-hoogo
\dialx GOs PA
\is topographie
\ps n
\ge ramification de montagne
\dt 16/Aug/2021

\lx hii
\dialx GOs PA
\va yi-n
\dialx BO
\sn 1
\is corps
\ps n
\ge main
\ge bras
\ge doigt
\xv hii-je
\dialx GOs
\xn sa main
\xv hii-n
\dialx PA
\xn main ; sa main
\se gu hii-n
\dialx PA
\sge main droite ; la droite
\se hii-n mãni kòò-n
\dialx BO
\sge les bras et les jambes; les membres
\se hii-n thoomwã
\dialx BO PA
\sge pouce
\se hii-n êmwèn
\dialx BO
\sge index
\sn 2
\is corps_animaux
\ps n
\ge aile
\ge tentacule
\sn 3
\is plantes
\ps n
\ge algues de rivière
\sn 4
\is plantes_partie
\ps n
\ge branche
\se hi-cee
\dialx BO
\sge branche d'arbre
\et *kuku
\el POc
\dt 22/Feb/2025

\lx hii-je bwa mhwã
\dialx GOs
\is corps
\ps n
\ge main droite (sa)
\dt 24/Jan/2019

\lx hii-je mhõ
\dialx GOs
\is corps
\ps n
\ge main gauche (sa)
\se mho-n
\dialx PA
\sge main gauche ; la gauche
\dt 15/Feb/2019

\lx hiili
\dialx PA BO
\va iili
\is action_corps
\ps v
\ge cacher (se)
\ge rétracter (se)
\dn dans une coquille: escargot, coquillage
\ge effacer
\cf hiliini
\ce effacer
\dt 08/Feb/2025

\lx hijini
\ph hiɲɟini
\dialx GOs BO
\va hiliini
\dialx BO
\is action_corps
\ps v
\ge effacer
\dt 26/Jan/2019

\lx hili
\hm 1
\dialx GOs BO
\sn 1
\is bois_travail
\ps v.t.
\ge écorcer (du niaouli)
\xv i hili ci yòòni
\xn il enlève l'écorce du niaouli
\sn 2
\is préparation_aliments
\ps v.t.
\ge écorcher (animal)
\ge dépecer
\dt 08/Feb/2025

\lx hili
\hm 2
\dialx GOs BO
\is action
\ps v
\ge bouger ; secouer (branches)
\dt 29/Jan/2019

\lx hili
\hm 3
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge plaindre (se) ; gémir
\dt 25/Jan/2019

\lx hiliçôô
\ph hiliʒõ:
\dialx GOs
\va yaoli
\dialx WEM WE
\is action_corps
\is jeu
\ps n
\ge balançoire ; balancer (se)
\xv e hiliçôô
\xn il se balance
\xv e pe-hiliçôô
\xn il se balance
\dt 26/Jan/2019

\lx hilò
\dialx GO PA
\va hiloo
\is arbre
\ps n
\ge morindier ; fromager
\dn arbuste à propriétés curatives ; son écorce a aussi des propriétés tinctoriales, jaune
\sc Morinda citrifolia
\scf Rubiacées
\dt 21/Feb/2025

\lx hîmi
\hm 1
\dialx GOs
\sn 1
\is action
\ps v
\ge serré ; coincé
\xv e hîmi hii-nu
\xn ma main est coincée
\sn 2
\is action_corps_animaux
\ps v
\ge pincer (crabes, langoustes)
\dt 20/Aug/2021

\lx hîmi
\hm 2
\dialx GOs
\va hõmi
\dialx PA
\is action_corps
\ps v
\ge fermer
\xv hîmi phwa-çö !
\xn ferme ta bouche !
\cf kivwi
\ce fermer (la porte)
\dt 14/Jan/2022

\lx hine
\ph hĩɳe
\dialx GOs
\va hine
\dialx PA BO
\is fonctions_intellectuelles
\ps v.t.
\ge savoir ; connaître ; comprendre
\xv e hine
\dialx GOs
\xn il le sait
\xv e hine-kaamweni
\dialx GOs
\xn il le sait bien, il le comprend bien
\xv kavwö nu hine khõbwe e zoma a-mi haa kòi-je (ou: e zoma a-mi hai kòi-je)
\dialx GOs
\xn je ne sais pas s'il viendra ou pas
\xv kavwö nu hine traabwa bwa chovwa
\dialx GOs
\xn je ne suis jamais monté à cheval
\xv yo mara hine
\dialx PA
\xn (maintenant) tu le sauras (après une punition)
\se te-(h)ine
\sge sûr, certain de (être)
\et *kilala, *kila
\el POc
\dt 18/Jul/2024

\lx hine-kaamweni
\ph hiɳe-ka:mweɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge comprendre (bien) qqch. ; reconnaître ; savoir bien qqch.
\dt 20/Dec/2024

\lx hinevwo
\ph 'hiɳeβo
\dialx GOs PA
\is fonctions_intellectuelles
\ps v ; n
\ge instruit ; intelligent ; connaissance ; intelligence
\xv e hinevwo
\dialx GOs
\xn il est intelligent ; c'est quelqu'un d'intelligent
\xv hinevwo i la
\dialx GOs
\xn leur connaissance
\cf hine
\ce savoir; connaître
\dt 22/Feb/2025

\lx hîńõ
\dialx GOs PA
\va hinõn
\dialx BO
\sn 1
\is action_corps
\ps n
\ge signe
\xv i thu pa-hinõn ja inu
\dialx BO
\xn il me fait signe
\sn 2
\is action_corps
\ps v
\ge montrer
\sn 3
\is caractéristiques_personnes
\ps v
\ge nu (être)
\xv i u pa-hinõ
\dialx PA
\xn elle est complètement nue (elle montre)
\et *khina
\eg savoir
\el PSO
\ea Geraghty
\dt 09/Apr/2024

\lx hińõ-a
\ph 'hinɔ̃a
\dialx GOs
\va hinõ-al
\dialx PA BO
\sn 1
\is temps_découpage
\ps n
\ge heure
\dn (lit. signe du soleil)
\xv poniza hińõ-a ?
\dialx GO
\xn quelle heure est-il ?
\xv ponira hinõ-al ?
\dialx PA
\xn quelle heure est-il ?
\xv na whaya hinõ-al ?
\dialx BO
\xn à quelle heure ?
\sn 2
\is instrument
\ge montre ; pendule |dialx{PA}
\dt 08/Feb/2025

\lx hińõõ-tree
\hm 1
\dialx GOs
\va hinõ-tèèn
\dialx PA BO
\is temps_découpage
\ps n
\ge aurore ; aube
\xv bwa hińõõ-tree  (lit. signe du jour ; |lx{tree} jour)
\dialx GO
\xn à l'aurore
\dt 20/Mar/2023

\lx hińõõ-tree
\hm 2
\ph hinɔ̃ʈe:
\dialx GOs
\va hinõ-tèèn
\dialx PA
\is astre
\ps n
\ge étoile du matin
\dn (lit. signe du jour ; |lx{tèèn} jour)
\dt 08/Feb/2025

\lx hi-pwaji
\ph 'hi'pwaɲɟi
\dialx GOs
\is crustacés
\ps n
\ge pinces du crabe
\dt 10/Jan/2022

\lx hituvaè
\dialx BO
\va hiluvai
\dialx GO
\is crustacés
\ps n
\ge langouste
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx hivwa
\dialx GOs
\is mollusque
\ps n
\ge moule ; coque
\dn sert de grattoir à banane
\cf hizu
\ce (plus petite)
\dt 08/Feb/2025

\lx hivwaje
\ph hiβaɲɟe
\dialx GOs
\is fonctions_naturelles
\ps v ; n
\ge sourire
\dt 25/Jan/2019

\lx hivwi
\hm 1
\ph hiβi
\dialx GOs
\is arbre
\ps n
\ge palétuvier gris
\sc Avicennia marina
\scf Verbénacées
\gb mangrove tree
\dt 08/Feb/2025

\lx hivwi
\hm 2
\ph hiβi
\dialx GOs PA
\va hivi
\dialx BO
\va hipi
\dialx BO arch.
\is action_corps
\ps v
\ge ramasser (nourriture, vêtement)
\ge entasser
\dt 24/Dec/2021

\lx hivwine
\ph hiβiɳe
\dialx GOs
\va hipine
\dialx GO(s)
\va hivine
\dialx BO
\is fonctions_intellectuelles
\ps v.t.
\ge ignorer ; ne pas savoir ; ignorer (s')
\xv e hivwine khõbwe da la çö caabi
\dialx GOs
\xn il ne sait pas quels objets tu as frappés
\xv li pe-hivwine-li ã-mi na ni nye ba-êgu
\dialx GOs
\xn ils s'ignorent à cause (lit. cela vient) de cette femme
\ng v.t. |lx{hivwine}
\gt ne pas savoir qqch.
\an hine
\at savoir
\dt 21/Feb/2025

\lx hivwinevwo
\ph hiβiɳeβo
\dialx GOs
\is fonctions_intellectuelles
\ps v.i
\ge ignorant (être un)
\xv e hivwinevwo
\dialx GOs
\xn il est (un) ignorant ; c'est un ignorant
\dt 22/Feb/2025

\lx hivwivwu
\ph hiβiβu
\dialx GO
\is oiseau
\ps n
\ge râle
\ge oiseau de terre
\dn petit oiseau de couleur brune, il court et mange les graines semées dans les champs
\dt 27/Aug/2021

\lx Hixe
\dialx PA
\is société_organisation
\ps n
\ge fille cadette de chef
\dt 27/Jan/2019

\lx hi-xè
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge une branche
\dn emploi rare
\xv hi-ru hi-cee
\xn deux branches d'arbre
\dt 08/Feb/2025

\lx hizu
\dialx GOs
\sn 1
\is mollusque
\ps n
\ge moule
\ge coquille de moule
\sn 2
\is ustensile
\ps n
\ge coquille de moule
\dn sert de grattoir à banane
\cf hivwa
\ce moule (plus grosse)
\dt 16/Feb/2025

\lx ho
\dialx PA
\is interaction
\ps v
\ge protéger
\se a-ho
\sge protecteur
\se hova mwa
\sge les protecteurs de la chefferie
\ng forme déterminée |lx{hova}
\dt 08/Jan/2022

\lx ho
\dialx GOs BO
\va hò
\dialx PA
\is grammaire_aspect
\ps ASP
\ge encore ; à nouveau |dialx{BO}
\xv e ra hò nûû
\dialx PA
\xn elle continue d'éclairer (à la torche)
\xv ho kõbwe õ-xe
\dialx BO
\xn répète encore une fois
\xv ho kole òn
\dialx BO
\xn mets encore du sel
\xv la ho kõbwe "u nòòl !"
\dialx BO
\xn il redit "réveillez-vous !"
\xv i ho na le nûû po-xe
\dialx BO
\xn il met une autre torche
\dt 22/Feb/2025

\lx ho-
\dialx GOs BO
\va hò
\dialx PA
\va hu-
\dialx BO
\is classificateur_possessif_nourriture
\ps CLF.POSS ; n
\ge part de nourriture carnée
\dn part de poisson et viande
\ge ration ; part de sucreries ; médicaments |dialx{PA}
\xv mi za u mhaza ganye mwã hoo-î
\dialx GO
\xn nous allons bientôt gagner notre nourriture
\xv mã novwö bî baani vwö hoo-bî
\dialx GO
\xn et alors nous le tuerons pour le manger (pour qu'il soit notre nourriture)
\xv hoo-ã
\xn notre nourriture carnée
\xv jaxa hò-m ênè
\dialx PA
\xn tu as assez de nourriture (lit. suffisant ta nourriture)
\xv bo hò-îî da ?
\dialx PA
\xn quelle est cette odeur de viande pour nous deux ?
\xv hò-n na nõ
\dialx PA
\xn sa ration de poisson
\xv hò-m na cèvèro
\dialx PA
\xn ta ration de viande
\dt 22/Feb/2025

\lx hò
\hm 1
\dialx GOs PA BO
\is insecte
\ps n
\ge cigale (grosse et verte)
\dt 29/Jan/2019

\lx hò
\hm 2
\dialx GOs
\is action_corps
\ps v
\ge sortir (de son habitacle) ; exorbité
\xv e hò pi-mee
\dialx GOs
\xn il a les yeux exorbités/écarquillés
\xv e hò kumee-mee
\dialx GOs
\xn il a la langue pendante
\dt 22/Feb/2025

\lx hõ
\hm 1
\dialx GOs
\va hom
\dialx PA BO
\is caractéristiques_personnes
\ps v.stat
\ge muet
\cf hû
\ce silencieux
\et *kumu
\eg muet
\el POc
\et *xxumu
\el PSO
\ea Geraghty
\dt 03/Feb/2025

\lx hõ
\hm 2
\dialx GOs
\va hô
\dialx PA
\wr A
\is caractéristiques_objets
\ps v.stat
\ge nouveau ; neuf
\ge récent
\se hõ loto
\dialx GO
\sge nouvelle voiture
\se hõ mwa
\dialx GO
\sge nouvelle maison
\wr B
\is grammaire_aspect
\ps ASP
\ge venir de
\xv e hõ a nye !
\dialx GO
\xn celui-là vient juste de partir !
\xv e hõ pwe
\dialx GO
\xn il vient de naître
\xv la hõ nee
\dialx GO
\xn ils l'ont fait récemment
\xv nu ra gaa hô hovwo
\dialx PA
\xn je viens de manger ; j'ai déjà mangé
\xv i gaa hô uvhi loto
\dialx PA
\xn il vient d'acheter une voiture
\cf hõ-xe
\dt 22/Feb/2025

\lx hô
\dialx PA
\is grammaire_quantificateur_mesure
\ps n
\ge morceau long et plat
\dn réfère à une surface allongée
\se hôô-cee
\sge planche
\se hôô-tòl
\sge plaque de tôle
\dt 16/Feb/2025

\lx hò pii-me
\dialx GOs
\va wò pii-me
\dialx GO(s)
\is action_tête
\ps v
\ge écarquiller les yeux
\xv i hò pii-mèè-n
\dialx PA GO
\xn elle écarquille les yeux
\xv e hò/wò pii-me
\dialx GO
\xn elle écarquille les yeux
\dt 05/Nov/2021

\lx hõ-ã
\dialx BO
\is grammaire_démonstratif
\ps PRON.DEIC
\ge celui-ci ; de ce côté-ci
\xv hõ-ã je, i pe-kîga waa-na
\xn quant à lui, il se rit ainsi
\nt selon Corne, BM
\cf hõõ-li
\ce de ce côté-là ; de l'autre côté
\dt 22/Feb/2025

\lx hôbòl
\dialx PA
\is interaction
\ps v
\ge jurer (que c'est vrai)
\dt 27/Jan/2019

\lx hõbwò
\ph hɔ̃bwɔ
\dialx GOs
\va hõbò
\dialx GO(s)
\va hãbwòn
\dialx PA BO
\va hõbwòn
\ph hɔ̃bwɔ̃n
\dialx BO
\is habillement
\ps n
\ge vêtements
\ge linge ; tissu
\se hõbwòni ba-mõgu
\dialx GO
\sge des vêtements pour le travail
\se hõbwò bwabu
\dialx GO
\sge sous-vêtements
\xv hõbwò ba-thu-mwêê
\dialx GOs
\xn de beaux vêtements
\xv hõbwòra ba-êgu; hõbwò-w-a ba-êgu
\dialx GOs
\xn des vêtements de femme
\xv hõbwò za ? – Hõbwòra pwal-i-nu
\dialx GOs
\xn quel type de vêtement ? – Des vêtements pour la pluie
\xv hõbwòra chomu
\dialx GOs
\xn des vêtements pour l'école
\xv hãbwòra khabu
\dialx PA
\xn vêtements pour le froid
\xv hõbwò xa whaya ? – Hõbwò xa khawali
\dialx GOs
\xn des vêtements comment ? – Des vêtements longs
\xv hõb(w)òli-nu
\dialx GOs
\xn mes vêtements
\xv hãbwòli-n (ou)  hõbòni-n
\dialx PA
\xn ses vêtements
\xv kixa hõbwòni-n
\dialx PA
\xn (il/elle) n'a pas de vêtements
\xv i udale hõbwòni-n u Kaavo
\dialx BO PA
\xn Kaavo met ses vêtements
\xv kixa hõbwòli-la
\dialx WEM
\xn ils n'ont pas de vêtements
\dt 24/Feb/2025

\lx hôbwo
\ph hõbwo
\dialx GOs
\va hôbo
\dialx GO(s) PA
\is interaction
\ps v
\ge garder ; surveiller
\ge attendre
\se a-hôbwo
\sge gardien ; surveillant
\dt 22/Feb/2025

\lx hõbwò-ko
\dialx GOs
\is habillement
\ps n
\ge robe popinée
\dt 28/Aug/2021

\lx hõbwò-tralago
\dialx GOs
\is habillement
\ps n
\ge robe
\dt 28/Aug/2021

\lx hôdò
\ph hõdɔ
\dialx GOs PA
\va hôdèn
\dialx BO
\is santé
\ps v
\ge jeûner ; jeûne
\xv we-kò tree xa nu hôdò
\dialx GO
\xn cela fait trois jours que je jeûne
\xv we-kòn na tèèn ka nu hôdò
\dialx PA
\xn cela fait trois jours que je jeûne
\se bo hôdèn
\dialx BO
\sge vendredi
\dt 28/Jan/2022

\lx hõge
\ph hɔ̃ŋge
\dialx GOs BO PA
\va hõxe
\dialx GO(s)
\is armes
\ps n
\ge doigtier de la sagaie (propulseur) ; lance sagaie
\dt 06/Feb/2019

\lx hõgo
\ph hɔ̃ŋgo
\dialx GOs
\is plantes
\ps n
\ge roseau
\dt 29/Jan/2019

\lx hõgõõne mhwããnu
\dialx PA BO
\va hõgõõn
\dialx PA BO
\is astre
\ps n
\ge deuxième quartier de lune
\dt 16/Aug/2021

\lx holö
\dialx GOs
\is caractéristiques_objets
\ps v
\ge lâche (être) ; grand
\dt 20/Mar/2023

\lx homi
\dialx PA BO
\is action_corps
\ps v.t.
\ge fermer ; pincer
\xv homi phwaa-m !
\xn tais-toi (ferme la bouche)
\cf kivwi phwaa-m !
\ce tais-toi (momentanément)
\dt 13/Oct/2021

\lx homwi
\dialx BO
\is feu
\ps n
\ge pincettes
\nt selon Corne
\et *(ŋ)kompi
\el POc
\eg serrer, pincer
\dt 26/Mar/2022

\lx hõn
\ph hɔ̃n
\dialx PA
\is eau
\ps n
\ge source
\dt 28/Jan/2019

\lx hòne
\dialx BO
\is action
\ps v
\ge réparer
\cf hò
\ce nouveau ; encore
\dt 22/Feb/2025

\lx hô-niza ?
\dialx GOs
\is grammaire_interrogatif
\ps INT
\ge combien de morceaux (pastèque, igname)
\dt 29/Jan/2019

\lx hônô
\dialx GOs PA
\is santé
\ps v.stat
\ge malade (gravement)
\ge alité
\xv e gaa hônô
\dialx PA
\xn il est très gravement malade/alité
\dt 22/Feb/2025

\lx hoo
\hm 1
\dialx PA
\is discours_interjection
\ps v
\ge signaler sa présence par un appel
\xv i hoo
\xn il signale sa présence par un appel
\dt 28/Jan/2019

\lx hoo
\hm 2
\dialx GOs PA
\is outils
\ps n
\ge coin (pour caler)
\dn (lit. nourriture)
\xv e thu hoo kòò-piòò
\dialx GO
\xn il cale le manche de la pioche
\xv ne-vwo hoo-n !
\dialx PA
\xn cale-le ! (lit. fais sa nourriture)
\dt 09/Feb/2025

\lx hoo
\hm 3
\dialx GOs
\va tau
\dialx BO PA
\is pêche
\ps v
\ge pêcher à marée basse ou à marée montante
\dt 26/Jan/2019

\lx hòò
\hm 1
\dialx GOs
\va hòòl
\dialx PA
\is discours_tradition_orale
\ps v ; n
\ge haranguer
\dn lors des grandes cérémonies
\ge discours sur le bois
\dn discours rythmé en tapant sur le bambou
\dt 08/Feb/2025

\lx hòò
\hm 2
\dialx GOs
\va hòòl
\dialx PA BO
\sn 1
\is grammaire_locatif
\ps ADV ; LOC
\ge loin ; éloigné ; lointain
\xv jige bu-hòò/buxò
\dialx GO
\xn un fusil à longue portée
\xv ge hòòl
\dialx PA
\xn il est loin
\sn 2
\is temps
\ps ADV
\ge longtemps (d'il y a)
\xv gee hòò
\dialx GO
\xn grand-mère d'il y a longtemps (= ancêtre)
\sn 3
\is grammaire_aspect
\ps v
\ge presque
\se kavwö hòò
\sge s'en falloir de peu que
\xv kavwö hòò vwo nu baani !
\xn il s'en est fallu de peu que je ne le frappe !
\cf a-ò
\ce éloigner (s')
\et *saud, *sauq
\ea Grace
\el POc
\dt 16/Jan/2025

\lx hòòa
\dialx GOs
\is temps_découpage
\ps n
\ge matin
\dt 28/Jan/2019

\lx hõõng
\ph hɔ̃:ŋ
\dialx PA
\sn 1
\is action_tête
\ps v
\ge sortir ; tirer (langue) ; écarquiller (yeux)
\xv i hõõnge kumèè-n
\dialx PA
\xn il tire la langue
\xv i hõõnge mèè-n
\dialx PA
\xn il écarquille les yeux
\cf phããde kumèè-n
\dialx PA
\ce tirer la langue
\sn 2
\is action_corps_animaux
\ps v
\ge sortir d'un trou (animal, reptile)
\dt 28/Jan/2022

\lx hoogo
\ph ho:ŋgo
\dialx GOs WEM PA BO
\va hogo
\dialx GOs PA
\is topographie
\ps n
\ge montagne
\dt 16/Aug/2021

\lx hooli
\dialx PA BO
\sn 1
\is action_corps
\ps v
\ge mélanger ; tourner (dans la marmite)
\sn 2
\is action
\ps v
\ge éparpiller ; semer la pagaille
\dt 26/Jan/2019

\lx hõõ-li
\ph hɔ̃:li
\dialx BO
\is grammaire_locatif
\ps LOC
\ge de l'autre côté
\nt selon Corne, BM
\an hõõ-ã
\at de ce côté-ci
\dt 27/Mar/2022

\lx hoońõ
\ph ho:nɔ̃
\dialx GOs PA BO
\is corps
\ps n
\ge intestins ; boyaux ; entrailles
\xv hoońõ ko
\dialx GOs
\xn les intestins du poulet
\xv hoońõ-nu
\dialx GOs
\xn mes intestins
\xv hoonõ-m
\dialx PA
\xn tes intestins
\dt 17/Jan/2022

\lx hoova
\dialx GOs
\is action_tête
\ps v
\ge souffler
\dt 26/Jan/2019

\lx hôô-wony
\ph hõ:wɔ̃ɲ
\dialx PA
\is grammaire_quantificateur_mesure
\ps n
\ge planche de bateau
\dt 16/Feb/2025

\lx horayee
\dialx PA
\va orayee
\dialx PA BO
\is maison
\ps n
\ge gaulettes circulaires du toit
\dn elles sont perpendiculaires à la pente du toit et retiennent les écorces de niaouli ou la paille ; elles sont plus petites que |lx{ce-mwa}
\ge baguette
\se orayee pwa
\sge gaulettes extérieures
\se orayee mwa
\sge gaulettes intérieures
\cf ce-mwa
\ce solives
\dt 05/Jan/2022

\lx hore
\dialx PA WEM BO
\is déplacement
\ps v
\ge suivre ; longer
\xv i hore bwèèxòò-n
\xn elle suit les traces de pas
\xv i pîîna hore jaaòl
\dialx BO
\xn elle se promène le long du fleuve
\xv i a-hore wèdò
\dialx PA
\xn il suit les coutumes
\dt 22/Mar/2023

\lx hovalek
\dialx PA
\sn 1
\is instrument_ponts
\ps n
\ge passerelle ; pont
\dn fait d'un tronc d'arbre ou d'une planche pour traverser une rivière
\sn 2
\is société_organisation
\ps n
\ge personne servant de lien entre deux clans
\dt 21/Feb/2025

\lx hova-mwa
\dialx GOs PA WEM
\is société_organisation
\ps n
\ge clans (tous les) qui composent la chefferie
\dn et qui la protègent en temps de guerre
\cf ho
\ce protéger
\dt 16/Feb/2025

\lx hovwa
\ph hoβa
\dialx GOs WEM
\va hova
\dialx PA BO WEM
\va hava
\dialx PA BO
\sn 1
\is déplacement
\ps v
\ge arriver ; arrivé
\se hovwa-da
\dialx GO
\sge arrivé en haut
\sn 2
\is grammaire_préposition_locative
\ps PREP
\ge jusqu'à
\xv e a na Gome hovwa Kumwa
\dialx GO
\xn il est allé de Gomen jusqu'à Koumac
\sn 3
\is grammaire_conjonction
\ps CNJ
\ge jusqu'à ce que
\xv ne nye-na hovwa-da na çö õgi
\dialx GO
\xn fais-le jusqu'à ce que tu aies fini
\an lhi (h)ava-du
\dialx PA
\at ils arrivent en bas
\dt 26/Aug/2023

\lx hovwa-da
\ph hoβanda
\dialx GOs
\va havha-da
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge jusqu'à (locatif)
\xv havha-da ni wôding
\dialx PA
\xn jusqu'au col
\dt 02/Jan/2022

\lx hovwa-da xa
\ph hoβanda ɣa
\dialx GOs
\va havha-da
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge jusqu'à ce que
\xv e yuu kòlò kêê-je ma õã-je xa ẽnõ gò hovwa-da xa whamã mwã
\dialx GO
\xn il vit chez son père et sa mère depuis qu'il est enfant jusqu'à ce qu'il soit devenu grand
\xv e yuu vwo hovwa-da xa poxi Hiixe
\dialx GO
\xn il vivait là jusqu'à ce que Hiixe soit enceinte
\xv havha-da xa u whamã mwã
\dialx PA
\xn jusqu'à ce qu'il soit devenu grand
\dt 02/Jan/2022

\lx hòvwi
\ph hɔβi
\dialx GOs
\va hòvi
\dialx BO PA
\sn 1
\is action_corps
\ps v
\ge soulever (pour chercher qqch.)
\xv e hòvwi paa
\xn elle soulève des pierres (pour chercher qqch.)
\sn 2
\is fonctions_naturelles
\ps v
\ge réveiller
\dn en secouant, en retournant qqn.
\dt 17/Feb/2025

\lx hovwo
\ph hoβo
\dialx GOs PA
\va hovho
\dialx PA BO
\va hopo
\dialx GO arch.
\sn 1
\is nourriture
\ps v ; n
\ge manger (générique) ; nourriture (générique)
\xv e pa-hovwo-ni la ẽnõ
\dialx GOs
\xn elle fait manger les enfants
\xv hovwo za ? – Hovwo ponga hauva
\dialx GOs
\xn quel type de nourriture ? – De la nourriture pour la levée de deuil
\xv i a-hovho
\dialx BO
\xn il est gourmand
\xv dòò-cee hovwo
\dialx PA
\xn feuilles comestibles
\cf biije
\ce mâcher des écorces ou du magnania
\cf bwaçu
\dialx GOs
\ce manger (respectueux, en parlant d'un chef)
\cf cèni
\dialx GOs
\ce manger (féculents)
\cf huu, huli
\ce manger (nourriture carnée, du coco)
\cf kûûńi
\dialx GOs
\ce manger (fruits, feuilles)
\cf whizi ê
\dialx GOs
\ce manger (canne à sucre)
\ng causatif: |lx{pa-hovwo-ni}
\gt faire manger
\sn 2
\is nourriture_tabac
\ps v
\ge chiquer (tabac) |dialx{BO}
\ge fumer (tabac) |dialx{BO}
\dt 24/Feb/2025

\lx hovwo thrõbò
\ph hoβo
\dialx GOs
\is nourriture
\ps n
\ge dîner ; souper
\dt 22/Feb/2025

\lx hòwala
\ph 'hɔwala
\dialx GOs
\va hòpwala
\dialx GO(s)
\va oala
\dialx WE
\is fonctions_naturelles
\ps v
\ge bâiller ; éructer
\dt 25/Mar/2022

\lx hõxa
\ph hɔ̃ɣa
\dialx GOs
\va hoxa
\dialx PA
\is grammaire_quantificateur_mesure
\ps n
\ge morceau ; part ; fragment
\se hoxa doo
\dialx PA
\sge un fragment de poterie
\dt 16/Feb/2025

\lx hoxaba
\dialx PA BO
\va hoyaba
\dialx BO [Corne]
\is plantes_partie
\ps n
\ge fibres pour panier
\sc Epipremnum pinnatum
\scf Aracées
\dt 27/Aug/2021

\lx hõxa-cee
\dialx GOs
\va hõõ-cee
\dialx PA
\is bois
\ps n
\ge planche
\dt 08/Jan/2022

\lx hoxaxe
\dialx PA
\is ustensile
\ps n
\ge grattoir métallique
\dt 26/Jan/2019

\lx hoxe
\dialx GOs PA
\is grammaire_aspect
\ps ITER
\ge encore ; à nouveau
\xv i hoxe a-da
\dialx PA
\xn il remonte encore
\xv i cabòl ta/ra i hoxe mããni
\dialx PA
\xn il se réveille puis se rendort encore
\ng abrégé en |lx{ho}
\dt 05/Jan/2022

\lx hô-xè
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge un morceau (de bois, fruit, igname, etc.)
\xv hô-xè, hô-tru, hô-kò, hô-pa, hô-ni, etc.
\dialx GOs
\xn un, deux, trois, quatre, cinq morceaux
\xv nu pii vwö hô-tru
\dialx GOs
\xn j'ai coupé en deux morceaux
\xv mwêênò hô-xè cee
\dialx GOs
\xn il reste un bout de bois
\xv hô-ru hõxa-cee
\dialx PA
\xn deux planches
\dt 30/Dec/2021

\lx hoxèè
\ph hoɣɛ:
\dialx GOs BO
\is grammaire_quantificateur_mesure
\ps v.QNT
\ge peu ; quelques
\xv hoxèè chaamwa i nu
\dialx GO
\xn j'ai peu de bananes
\xv hoxèè la mãla phwe-meevwu Maluma xa la uça
\dialx GO
\xn peu de gens du clan Maluma sont venus
\xv la pò hoxèè nai la
\dialx GO
\xn ils sont un peu moins nombreux qu'eux
\xv e cèni hoxèè kui nai nu
\dialx GO
\xn il mange moins d'igname que moi
\xv i cani hoxèè kwi nai nu
\dialx BO
\xn il mange moins d'igname que moi
\se pò-hoxèè
\sge peu
\an haivwö
\at beaucoup
\dt 16/Feb/2025

\lx höze
\dialx GOs
\va hure
\dialx WEM WE PA
\va hore
\dialx BO
\sn 1
\is déplacement
\ps v
\ge suivre (berge, rivière, etc.)
\xv e a höze kòli we-za
\dialx GO
\xn il suit le bord de la mer
\xv e a höze dè
\dialx GO
\xn il suit la route
\xv e a höze kòli we
\dialx GO
\xn il suit la berge de la rivière
\xv a-hore
\dialx BO
\xn aller en suivant
\sn 2
\is discours
\ps v
\ge raconter
\dn en suivant bien le fil de l'histoire
\ge dire (prière) ; réciter
\xv höze-zoo-ni !
\dialx GO
\xn dis-le bien !
\dt 08/Feb/2025

\lx hòzi
\dialx GOs
\is action_corps
\ps v
\ge mélanger ; tourner
\ge ratisser
\dt 20/Aug/2021

\lx hu
\hm 1
\dialx GOs BO
\is igname
\ps n
\ge igname sp. (la plus dure)
\sc Dioscorea alata L.
\scf Dioscoréacées
\dt 27/Aug/2021

\lx hu
\hm 2
\dialx PA BO (BM)
\va u, ho
\dialx BO
\is grammaire_agent
\ps AGT ; sujet
\ge agent (marque) ; sujet
\xv i pa-tòni gò hu ri?
\xn qui fait sonner la musique?
\xv i yòli-nu ho minòn
\xn le chat m’a griffé
\dt 25/Aug/2023

\lx hu
\hm 3
\dialx BO
\is grammaire_conjonction
\ps CNJ
\ge jusqu'à
\dn vient probablement de |lx{huli} suivre
\xv hu ra u kuuni
\xn jusqu'à ce qu'il ait fini
\dt 21/Feb/2025

\lx hû
\hm 1
\dialx GOs
\va hûn
\dialx PA BO
\is discours
\ps v
\ge taire (se) ; faire le silence ; rester silencieux
\se êgu hû
\dialx GOs
\sge une personne renfermée/timide
\xv i ku-hûn
\dialx PA
\xn il reste silencieux
\cf hô
\ce muet
\dt 22/Feb/2025

\lx hû
\hm 2
\dialx GOs
\is son
\ps v
\ge gronder
\xv hû niô
\xn le tonnerre gronde
\dt 26/Jan/2019

\lx hua
\dialx GOs
\va ua
\dialx GO(s)
\va uany
\dialx BO [BM]
\va whany
\dialx PA
\is interaction
\ps n
\ge malédiction
\dt 27/Jan/2019

\lx hûbale
\dialx PA BO
\is nourriture
\ps v
\ge manger sans dents
\nt selon Corne
\dt 26/Mar/2022

\lx hûbu
\dialx GOs
\va hûbun, hûbi
\dialx PA
\is religion
\ps n
\ge puissance ; charisme
\ge mana
\xv hûbu-n
\dialx PA
\xn sa puissance
\se hûbi-zo
\dialx GOs PA
\sge puissance bénéfique
\se hûbi-thraa
\dialx GOs
\va hûbi-raa
\dialx PA
\sge puissance maléfique
\dt 20/Mar/2023

\lx hûda
\dialx GOs PA BO
\is plantes
\ps n
\ge roseau
\dt 29/Jan/2019

\lx hu-êgu
\dialx GOs
\is santé
\ps n
\ge mycose
\dn (lit. qui mange les gens)
\dt 08/Feb/2025

\lx hûgu
\dialx GOs
\is caractéristiques_objets
\ps v.stat
\ge épais
\dt 03/Feb/2025

\lx huli
\is déplacement
\dialx PA BO
\ps v
\ge guider
\xv huli-je-mwa-mi kòlò-ny
\xn guide-la alors ici chez moi
\dt 08/Jan/2022

\lx hulò
\dialx GOs BO PA
\sn 1
\is configuration
\ps n
\ge bout (d'une chose longue) ; extrémité ; fin ; terme
\se hulò-de
\dialx GOs
\sge le bout de la route
\se hulò-ta
\dialx GOs
\sge le bout de la table
\se hulo cee
\dialx PA
\sge le faîte/la cime d'un arbre
\sn 2
\is interaction
\ps n
\ge résultat (bon ou mauvais) ; conséquence
\xv hulò vhaa i je
\dialx GO
\xn la conséquence de ses paroles
\xv hulò-n
\dialx BO
\xn son résultat
\dt 24/Jan/2022

\lx hulò-mhèńõ
\ph 'hulɔ'mhɛnɔ̃
\dialx GOs
\va hulò-mhee-n
\dialx PA BO
\is coutumes
\ps n
\ge don pour saluer les hôtes
\dn quand on arrive quelque part
\xv hulò-mhee-ny
\dialx BO PA
\xn ma coutume d'arrivée
\xv la na la hulò-mhee-la
\dialx BO PA
\xn ils donnent leur coutume d'arrivée
\dt 08/Feb/2025

\lx hu-mudree
\ph 'hũ'muɖe:
\dialx GOs
\is action_tête
\ps v
\ge couper avec les dents ; déchirer avec les dents
\dt 09/May/2024

\lx hûn
\dialx PA BO
\sn 1
\is temps_atmosphérique
\is son
\ps n ; v
\ge tonner ; gronder
\ge grondement (tonnerre)
\xv i hûn (n)e nhyô
\dialx PA
\xn le tonnerre gronde
\sn 2
\is eau
\is son
\ps n
\ge bruit de ruissellement ou de cascade d'eau
\xv hûn-a we
\dialx PA
\xn bruit de ruissellement de l'eau
\ng forme déterminée: |lx{hûn-a}
\gt grondement de qqch.
\dt 21/Feb/2025

\lx huraò
\ph huɽaɔ
\dialx GOs
\is sentiments
\ps v
\ge rester bouche bée
\xv e huraò
\xn il reste bouche bée
\dt 21/Feb/2025

\lx hurè
\dialx GOs
\va hurèn
\dialx PA
\is caractéristiques_objets
\ps v.stat
\ge inhabité ; vide ; abandonné
\se mwa hurè
\dialx GOs
\va mwa hurèn
\dialx PA
\sge maison vide, abandonnée
\dt 03/Feb/2025

\lx huu
\dialx GOs PA BO
\va whuu
\dialx GO(s) PA
\sn 1
\is nourriture
\ps v ; n
\ge manger (de la viande, du coco) ; nourriture carnée
\xv mi za huu mwa mònõ nhye êgu-ã
\dialx GO
\xn nous mangerons cet homme demain
\se huu pilon
\dialx PA
\sge manger de la viande
\sn 2
\is action_tête
\ps v
\ge mordre
\ge ronger
\xv i whuu-nu u kuau
\dialx PA
\xn le chien m'a mordu
\xv i huu pwe ?
\dialx BO
\xn ça mord ? (lit. il a mordu la ligne ?)
\xv i huu du u kuau
\dialx BO
\xn le chien ronge l'os
\xv i hu-nu kuau
\dialx BO
\xn le chien m'a mordu
\sn 3
\is action_corps
\ps v
\ge pincer
\ge piquer
\ge démanger
\xv i whuu-nu u neebu
\dialx PA
\xn le moustique m'a piqué
\xv i hu-nu u paaji
\dialx BO
\xn le crabe m'a pincé
\ng |lx{hu-po, hu-vo, hu-wo}
\gt manger qqch. (marque d'objet indéfini)
\cf hovwo
\dialx GOs
\ce manger (en général)
\dt 21/Feb/2025

\lx huvado
\dialx GOs PA BO
\va vado
\dialx GO(s) BO
\is corps
\ps v.stat
\ge cheveux blancs (avoir les)
\xv i huvado
\dialx BO
\xn il a les cheveux blancs
\dt 03/Feb/2025

\lx hu-vwo
\dialx PA
\va hu-po
\dialx PA
\sn 1
\is nourriture
\ps v
\ge manger (nourriture carnée)
\ge ronger
\cf hovwo
\ce manger (général)
\dt 07/Nov/2021

\lx huzooni
\dialx GO
\is plantes
\ps n
\ge plante
\sc Jussiaea suffruticosa L.
\scf Onagracées
\dt 27/Jan/2022

\lx i
\hm 1
\dialx GOs PA
\sn 1
\is grammaire_préposition
\ps POSS.INDIR
\ge à ; pour
\xv nu na loto i Abel
\dialx GOs
\xn je donne la voiture à/d'Abel
\xv zixòò-ny  i Thonòòl ma Thomaxim
\dialx PA
\xn mon conte de Thonòòl et Thomaxim
\sn 2
\is grammaire_préposition
\ps OBJ.INDIR
\ge marque d'objet indirect
\dt 30/Dec/2021

\lx i
\hm 2
\dialx PA BO
\is grammaire_pronom
\ps PRO 3° pers. SG (sujet)
\ge il, elle
\dt 24/Aug/2021

\lx -î
\dialx GOs PA
\is grammaire_pronom
\ps PRO 1° pers.duel.INCL (OBJ ou POSS)
\ge nous deux (incl.)
\ge nos (duel incl.)
\dt 24/Aug/2021

\lx i ?
\dialx GOs PA BO
\va ii ?
\dialx GOs PA BO
\va wi?
\dialx GO(s) BO
\is grammaire_interrogatif
\ps v.INT (humains)
\ge où ?
\xv i caaja ?
\dialx GOs
\xn où est Papa ?
\xv i thoomwã-ò ?
\dialx GOs
\xn où est cette femme ?
\xv e ka ?  ii je ?
\dialx GOs
\xn que se passe-t-il ? où est-elle ?
\xv çö a wi ?
\dialx GOs
\xn où vas-tu ?
\xv i Pol ?
\dialx BO
\xn où est Paul ?
\xv i je ?
\dialx GOs
\xn où est-il ?
\xv i cö ?
\dialx GOs
\xn où es-tu ?
\xv i la ?
\xn où sont-ils? 
\dt 14/May/2024

\lx ia?
\dialx GOs BO
\va hia
\dialx PA
\is grammaire_interrogatif
\ps v.INT (statique)
\ge où ? ; quel ?
\xv nu a kaze-du ; ko ia ? ; ko Waambi
\dialx GOs
\xn je vais à la pêche ; à quel endroit ? ; à Waambi
\xv ia mõ-çö ?
\dialx GOs
\xn où est ta maison ?
\xv ia hinõ ?
\dialx GOs
\xn quel/où est le signe ?
\xv hèlè ia ? ma kixa !
\dialx GOs
\xn mais quel couteau? il n'y en a pas !
\xv ia nye ãbaa-je thoomwã ?
\dialx GOs
\xn où/quelle est sa sœur ?
\xv ia la êgu ?
\dialx GOs
\xn où sont les gens ?
\xv ia êgu-ò ?
\dialx GOs
\xn où est cet homme ?
\xv ia kuau ?
\dialx GOs
\xn où est ce chien ?
\xv ia mõ-m ?
\dialx BO
\xn où est ta maison?
\cf ge ea (x) ?
\ce où se trouve (x) ?
\dt 20/Feb/2025

\lx iã
\ph iɛ̃
\dialx GOs PA BO
\is grammaire_pronom
\ps PRO.INDEP 1° pers.incl.PL
\ge nous (plur.incl.)
\xv iã=mãla ge=ã êna mwã hãgana
\dialx PA
\xn nous les gens qui sommes ici maintenant
\dt 28/Aug/2023

\lx ibî
\dialx GOs
\va ibîn
\dialx PA
\va iciibii
\dialx archaïque
\is grammaire_pronom
\ps PRO.INDEP 1° pers.duel excl.
\ge nous deux (excl.)
\dt 14/Aug/2021

\lx içò
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.DU 2° pers.
\ge vous deux
\dt 05/Jan/2023

\lx içö
\ph iʒo
\dialx GOs
\va iyo
\dialx PA BO
\va eyo
\dialx BO
\is grammaire_pronom
\ps PRO.INDEP 2° pers.SG
\ge toi
\xv novwö içö, çö yuu wãã-na
\dialx GO
\xn quant à toi, tu restes ainsi
\dt 08/Feb/2019

\lx içu
\ph iʒu
\dialx GOs
\va iyu
\dialx PA
\is échanges
\ps v
\ge vendre ; commercer |dialx{GOs}
\ge acheter |dialx{PA}
\xv e u içu-ni a-kò pwaji cai Kaawo xo õã-nu
\xn ma mère a vendu trois crabes à Kaawo
\xv e içu-ni cai Kaawo a-kò pwaji xo õã-nu
\xn ma mère a vendu à Kaawo trois crabes
\ng v.t. |lx{iyuni} |dialx{PA}; |lx{içuni} |dialx{GOs}
\gt acheter qqch.
\dt 21/Feb/2025

\lx ido
\dialx GO
\is grammaire_quantificateur_mesure
\ps QNT
\ge une demi-paire (de roussettes ou notous)
\dn dans les dons coutumiers
\xv wa-tru bwò ko ido
\xn deux lots de deux roussettes plus une demi-paire
\dt 21/Feb/2025

\lx îdò-
\dialx GOs PA BO
\sn 1
\is configuration
\ps n
\ge ligne ; alignement ; rangée (ignames, poteaux, etc.)
\xv îdò-kui, îdoo-kui
\dialx PA
\xn rangée d'ignames
\xv pe-îdò-bî
\dialx GO
\xn nous sommes de la même génération/lignée
\se îdò-êgu
\sge génération, lignée
\sn 2
\is classificateur_numérique
\ps CLF.NUM
\ge rangée (d'ignames, poteaux, etc.)
\xv îdò-xè îdò-kui
\dialx GOs
\xn une rangée d'ignames
\xv îdò-xè, îdo-tru, îdo-kò, îdo-pa îdò-kui, etc.
\dialx GOs
\xn 1, 2, 3, 4, 5 rangée(s) d'ignames
\dt 22/Feb/2025

\lx ii
\dialx GOs PA BO
\sn 1
\is préparation_aliments
\ps v
\ge retirer qqch. d'un contenant (marmite)
\ge vider (le four enterré) ; sortir du four
\ge servir (la nourriture)
\xv e ii dröö
\dialx GO
\xn elle sert (la nourriture de) la marmite (lit. elle sort de la marmite)
\xv e ii na ni kîbi
\dialx GO
\xn elle sort (la nourriture du four)
\xv e ii lai
\dialx GO
\xn elle sert le riz
\xv ii dröö mwã !
\dialx GO
\xn videz la marmite !
\xv ii mwã xo li doo
\dialx PA
\xn ils se servent dans la marmite !
\sn 2
\is action
\ps v
\ge sortir (de qqch.)
\dn par ex. d'une poche, d'un panier
\xv e ii hõbwò na ni bèsè
\dialx GO
\xn elle sert le linge de la cuvette
\xv la ii phò-kamyõ
\dialx GO
\xn ils déchargent le camion
\cf udi
\ce retirer, enlever
\cf yatre, yare
\ce sortir
\dt 17/Feb/2025

\lx îî
\dialx GOs PA
\is grammaire_pronom
\ps PRO.INDEP 1° pers.duel.INCL
\ge nous deux (incl.)
\xv xa îî, avwö mi baani
\dialx GO
\xn et quant à nous, nous avons voulu le tuer
\dt 27/Mar/2022

\lx iing
\dialx PA
\is nourriture
\ps v
\ge dégoûté ; faire le difficile
\dt 26/Jan/2019

\lx iili
\dialx PA BO [Corne]
\is mouvement
\va hiili
\ps v
\ge rétracter (se)
\dt 02/Feb/2019

\lx ije
\ph iɲɟe
\dialx GOs BO PA
\va iye
\dialx PA
\is grammaire_pronom
\ps PRO.INDEP.SG 3° pers.
\ge elle ; lui
\xv axe ije ça, e phe jitua i je
\dialx GOs
\xn mais quant à lui, il prend son arc
\xv ije nye
\dialx GOs
\xn c'est elle ; la voilà
\xv ije ce pò tree mãla hêbu
\dialx GOs
\xn lui, il est le fruit des jours passés
\xv u ije !
\dialx PA
\xn c'est lui !
\dt 22/Feb/2025

\lx ijèè !
\ph ijɛ:
\dialx GOs
\is grammaire_interpellation
\ps INTJ
\ge hé ! la personne-là !
\dt 21/Feb/2025

\lx ijè-òli
\ph iɲɟɛɔli
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.SG-DEIC (3° pers.)
\ge elle/lui (au loin)
\xv nu nyãnume ijè-òli
\xn je lui ai fait signe à elle/lui là-bas
\dt 02/Jan/2022

\lx ila
\dialx GOs
\va ilaa
\dialx PA
\is grammaire_pronom
\ps PRO.INDEP.PL
\ge eux ; elles
\dn quand ce pronom est employé pour référer à une seule personne, il a une valeur honorifique
\dt 22/Feb/2025

\lx ila lhaa-ba
\dialx GOs PA BO
\is grammaire_pronom
\ps PRO.INDEP.PL-DEIC.2
\ge voilà (les) là sur le côté
\dn en les désignant
\dt 08/Feb/2025

\lx ila lhaa-òli
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.PL-DEIC.3
\ge voilà (les) eux/elles là-bas
\dt 31/Oct/2021

\lx ila-è
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.PL-DEIC
\ge elles-là ; eux-là
\dt 31/Oct/2021

\lx ili
\hm 1
\dialx GOs PA
\is grammaire_pronom
\ps PRO.INDEP.DU 3° pers.
\ge eux deux
\dt 27/Oct/2021

\lx -ili
\hm 2
\dialx GOs PA
\va hili
\dialx PA
\is grammaire_démonstratif
\ps DEM
\ge cela (audible, mais invisible)
\xv la êgu mãla-ili
\dialx GOs
\xn ces gens (qu'on entend)
\xv la ba(a)-êgu mãla-ili
\xn ces femmes (qu'on entend)
\xv ila lha-ili
\xn c'est eux, ceux-là (qu'on entend)
\xv ili lhi-ili
\xn eux deux, ces deux-là (qu'on entend)
\xv nu tone je vhaa (h)ili
\dialx PA
\xn j'ai entendu ces propos-là
\xv tõne l(h)a-ili pwaixe !
\dialx PA
\xn écoute ces choses (bruits)
\xv da la-ò waal mãla-(h)ili
\dialx PA
\xn quelles sont ces chansons ?
\dt 22/Feb/2025

\lx ili-è
\dialx GOs
\va ili-e
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.DU-DEIC
\ge ces deux (proche)
\xv ili-è !
\xn hé ! vous deux (proche) !
\xv li za a mwã ili-è
\xn ces deux-ci partent alors
\xv novwö na lhi tho ili-è-bòli
\xn quand ils appellent les deux (parents) en bas
\dt 21/Feb/2025

\lx ilili
\dialx BO
\is guerre
\ps v
\ge pousser un cri de guerre
\nt selon BM
\dt 26/Mar/2022

\lx ilò
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP.TRIEL ou paucal
\ge eux trois ; eux (paucal)
\se ilò-è
\sge ceux-là (proche)
\se ilò-ba
\sge ceux-là (loin)
\dt 08/Feb/2025

\lx imã
\dialx GOs BO
\is fonctions_naturelles
\ps v ; n
\ge uriner ; urine
\xv we imòò-je 
\dialx GOs 
\xn son urine
\se we imã
\sge urine
\et *mimiR
\el POc
\ng forme déterminée: |lx{imòò-}
\dt 26/Aug/2024

\lx imaze
\ph i'maðe
\dialx GOs
\is échinoderme
\ps n
\ge holothurie ; "bêche de mer"
\dt 29/Jan/2019

\lx ime
\ph ime
\dialx GOs
\va icòme
\dialx arch.
\is grammaire_pronom
\ps PRO.INDEP 1° pers. triel.EXCL
\ge nous trois excl. ou paucal
\xv xa ime, me a yu kòlò-li
\xn et quant à nous trois, nous restions chez eux deux
\xv haxe ime mãlo-ã ce me pòi mani pèèbu
\xn mais nous, nous sommes leurs enfants et petits-enfants
\dt 26/Dec/2021

\lx ine
\dialx GOs BO
\is oiseau
\ps n
\ge pétrel noir ; pétrel de Tahiti
\sc Pseudobulweria rostrata ssp. trouessarti
\scf Procellariidés
\dt 27/Aug/2021

\lx inîjeò
\dialx PA
\is temps
\ps ADV
\ge auparavant ; la fois d'avant
\dt 28/Jan/2019

\lx inu
\ph iɳu
\dialx GOs PA
\is grammaire_pronom
\va inu
\dialx PA BO
\ps PRO.INDEP 1° pers.
\ge moi
\xv inu nye
\xn c'est moi ; me voilà ; je suis là
\dt 22/Feb/2025

\lx iò
\dialx GOs PA BO
\is temps_deixis
\ps ADV
\ge bientôt ; tout à l'heure (à) (futur ou passé)
\xv pe-ròòli iò !
\xn à tout à l'heure !
\xv jo, xa za wãme nye iò
\dialx GO
\xn puis, c'est comme tout à l'heure
\xv na iò mwã, ne kaça hovwo, ce mõ vara thiu mwã
\dialx GO
\xn tout à l'heure, après le déjeuner, nous nous disperserons
\xv iò waa !
\xn ce matin (rétrospectif)
\xv vhaa kòlò xa pwaixe na cò ezoma trõne iò
\dialx GO
\xn des paroles sur des choses que vous allez entendre tout à l'heure
\xv la kêê-la mãni kibu-la je nu kobwe iò
\dialx PA
\xn leurs pères et leurs grands-pères dont j'ai parlé tout à l'heure
\xv iò ni waang !
\dialx BO
\xn ce matin (rétrospectif)
\xv iò xa waang !
\dialx BO
\xn ce matin (prospectif)
\xv ge le xa ãbaa-we ne zoma a iò ne thrõbo
\dialx GO
\xn certains d'entre vous partiront ce soir (tout à l'heure le soir)
\xv ge le ãbaa wõ ma la kòi-ò
\dialx GO
\xn certains bateaux ont disparu
\dt 21/Feb/2025

\lx iõ
\ph iɔ̃
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP 1° pers. triel incl.
\ge nous trois incl. ; nous paucal
\dt 29/Jan/2019

\lx -iõ
\ph iɔ̃
\dialx GO
\va -õ
\dialx GO
\is grammaire_pronom
\ps PRO 1° pers.triel.incl. (OBJ ou POSS)
\ge nous trois
\xv mõ huu mwã xa hovwo, vwo du-õ mani chãnã-õ
\xn nous allons manger de la nourriture pour (restaurer) nos os et notre souffle
\xv mõ pe-nhuã-iõ
\xn nous prenons congé les uns des autres
\dt 19/Feb/2025

\lx iò-gò
\dialx GOs
\va iò-gòl
\dialx WEM WE PA BO
\is temps_deixis
\ps ADV
\ge tout à l'heure (passé)
\xv nu ògine iò-gò
\xn je l'ai déjà fait
\dt 21/Feb/2025

\lx ira
\dialx BO
\is cultures_champ
\ps n
\ge champ de cultures sur une pente
\dn les sillons suivent la direction de la pente ; des trous captent l'eau et permettent son écoulement, évitant l'érosion
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx irô
\dialx BO
\is interaction
\ps v
\ge déranger
\ng v.t. |lx{irôni}
\gt déranger qqn.
\dt 21/Feb/2025

\lx iva
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP 1° pers. excl. PL
\ge nous (plur. excl.)
\ng forme moderne de izava
\dt 08/Oct/2021

\lx ivwö
\ph iβo
\dialx GOs
\is parenté_alliance
\ps n
\ge belle-sœur
\cf bee
\ce beau-frère
\dt 27/Jan/2019

\lx iwa
\dialx GOs PA
\is grammaire_pronom
\ps PRO.INDEP 2° pers. PL
\ge vous (plur.)
\dt 27/Mar/2022

\lx iwe
\dialx GOs
\va icòòwe
\dialx archaïque
\is grammaire_pronom
\ps PRO.INDEP 2° pers. triel
\ge vous trois
\xv hãgana xa ge me kòlò-ã êna bwa kavwègu i we, ma nhye iwe nhyã we kaavwu mwa
\xn aujourd'hui nous trois nous tenons ici à côté de celui-là dans votre chefferie, car c'est vous qui êtes les gardiens de la maison
\dt 22/Feb/2025

\lx iyo
\dialx BO PA
\va eyo
\dialx BO
\is grammaire_pronom
\ps PRO.INDEP 2° pers. SG
\ge toi
\dt 29/Jan/2019

\lx iza
\dialx PA WE
\va iyãã
\dialx BO
\is grammaire_pronom
\ps PRO.INDEP 1° pers.PL.EXCL
\ge nous (plur. excl.)
\dt 26/Aug/2023

\lx izava
\dialx GOs
\ph iðava
\va zava
\ph ðava
\dialx GO(s)
\va za
\dialx PA
\is grammaire_pronom
\ps PRO.INDEP 1° pers.PL.EXCL
\ge nous (excl.)
\dn forme ancienne
\dt 08/Feb/2025

\lx izawa
\ph iðawa
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP 2° pers. PL
\ge vous
\dn forme ancienne
\dt 08/Feb/2025

\lx izòò
\dialx PA BO
\is grammaire_pronom
\ps PRO.INDEP 2° pers.PL
\ge vous (plur.)
\dt 24/Aug/2021

\lx ja
\hm 1
\ph ɲɟa
\dialx GOs BO
\is caractéristiques_objets
\ps n
\ge saletés ; ordures ; détritus ; déchets
\dt 26/Jan/2019

\lx ja
\hm 2
\dialx BO [Corne]
\va jan
\dialx BO [BM]
\sn 1
\is plantes_partie
\ps n
\ge paille ; brindille
\sn 2
\is maison
\ps n
\ge paille à toiture
\dt 15/Sep/2021

\lx ja
\hm 3
\dialx GOs
\va jak
\dialx BO PA
\is fonctions_intellectuelles
\ps v
\ge mesurer
\ge peser
\se baa-ja
\sge balance
\ng v.t. |lx{jange}
\gt mesurer qqch.
\dt 21/Feb/2025

\lx jaa
\hm 1
\dialx GOs PA BO
\va jaac
\dialx BO (Corne)
\is plantes
\ps n
\ge salsepareille
\sc Smilax sp.
\scf Smilacacées
\dn liane d'ornementation, sert à faire des guirlandes, à faire l'armature des berceaux des nourrissons et l'armature des épuisettes à crevette
\dt 27/Aug/2021

\lx jaa
\hm 2
\dialx GOs
\is discours
\ps n
\ge annonce
\xv la phããde jaa
\xn ils font une annonce
\ng forme courte de |lx{jaale}
\cf jaale
\ce annoncer
\dt 05/Jan/2022

\lx jaa-cee
\dialx GOs
\is bois
\ps n
\ge copeaux de bois
\dt 08/Nov/2021

\lx jaale
\dialx GOs BO
\is discours
\ps v
\ge annoncer ; prévenir
\xv la jaale xa vhaa
\xn ils ont annoncé une nouvelle (lit. parole)
\se baa-jaale
\sge geste destiné à annoncer
\cf tre-khõbwe
\ce prévenir
\cf phããde
\ce révéler ; annoncer
\dt 22/Feb/2025

\lx jaaò
\dialx GOs
\va jaaòl
\dialx PA
\va jaòòl
\dialx BO
\is eau
\ps n
\ge fleuve ; rivière ; Diahot (nom d'un fleuve)
\se kûdo phwè jaaò
\sge l'embouchure de la rivière
\se kòli jaaò
\sge rive de la rivière
\se ku jaòòl
\dialx BO
\sge source de la rivière
\se phwè jaòòl
\dialx BO
\sge embouchure de la rivière
\dt 29/Aug/2021

\lx jange
\dialx GOs
\va jaxe
\dialx BO PA
\is fonctions_intellectuelles
\ps v
\ge mesurer
\ge peser
\dt 23/Aug/2021

\lx jago
\dialx PA
\is nourriture
\ps v
\ge manger sans rien laisser aux autres
\dt 26/Jan/2019

\lx jak
\dialx BO
\is fonctions_intellectuelles
\ps v.i
\ge mesurer
\se ba-jak
\sge instrument de mesure
\ng v.t. |lx{jaxe}
\gt mesurer qqch.
\dt 21/Feb/2025

\lx jali
\dialx BO
\is taro
\ps n
\ge taro d'eau (clone)
\nt selon Dubois
\dt 27/Mar/2022

\lx jamali
\dialx GOs WEM BO PA
\wr A
\is bois
\ps n
\ge cœur de bois dur 
\dn par ex. de gaïac, bois de fer, bois pétrole
\wr B
\is caractéristiques_objets
\ps MODIF
\ge ferme ; dur ; solide
\dt 21/Feb/2025

\lx jamo-hi
\dialx PA
\is corps
\ps n
\ge poing
\xv jamo-hi-n
\xn son poing
\dt 24/Jan/2019

\lx jamwe
\dialx GOs WEM WE BO PA
\is soin
\ps v
\ge laver
\dn la vaisselle, les vêtements, les cheveux
\xv nu jamwe inu
\dialx GO
\xn je me lave
\xv e jamwe mee xo Kavwo
\dialx GO
\xn Kaawo se lave le visage
\xv e jamwe mee-je xo Kavwo
\dialx GO
\xn Kaawo lave son visage (celui de qqn. d'autre)
\xv e jamwe mee-je xo chaavwõ
\dialx GO
\xn elle se lave le visage avec du savon
\dt 21/Feb/2025

\lx jana
\dialx GOs BO
\is échanges
\ps n
\ge marché ; échange de marchandises
\dt 27/Jan/2019

\lx jara
\dialx GOs PA BO
\is grammaire_modalité
\ps MODAL
\ge désir ; nécessité
\xv kêbwa çö jara trõne ji=li
\dialx GO
\xn n'écoute surtout pas cela
\xv kavwö nu jara nee
\dialx GO
\xn je ne veux absolument pas le faire
\dt 06/Aug/2023

\lx jaxa
\dialx GOs BO
\wr A
\is grammaire_quantificateur_mesure
\ps MODAL.n
\ge mesure ; assez ; juste ; suffisant
\xv gu jaxa !
\dialx GO
\xn ça suffit !
\xv jaxa-n
\dialx BO
\xn ça suffit !
\xv pe-jaxa-li
\xn de même taille
\wr B
\ps MODAL.n
\sn 1
\is grammaire_modalité
\ps MODAL.n
\ge capable
\xv kavwö jaxa-je vwo e zòò
\dialx GO
\xn il est incapable de nager
\sn 2
\is grammaire_modalité
\ps MODAL.n
\ge faillir ; manquer de
\xv ja(xa) vwo kalu ẽnõ-ni !
\dialx GO
\xn cet enfant a failli tomber !
\xv za ja(xa) vwo la za mã !
\dialx GO
\xn ils ont failli mourir !
\sn 3
\is grammaire_modalité
\ps MODAL.n
\ge devoir (épistémique)
\xv jaxa 3 heures mwã
\dialx GO
\xn il doit être trois heures
\dt 16/Feb/2025

\lx jaxe
\dialx PA
\va jaaxe
\dialx BO
\is fonctions_intellectuelles
\ps v.t.
\ge mesurer
\ge peser
\xv i jaaxe we
\dialx BO
\xn elle mesure la hauteur de l'eau (avec son corps)
\ng v.i. |lx{jak}
\dt 20/Mar/2023

\lx je
\hm 1
\dialx GOs PA BO
\va ji
\dialx BO
\is chasse
\ps n
\ge piège ; lacet
\dn pour attraper des oiseaux, des rats
\xv wa je
\dialx BO
\xn corde pour piège
\dt 21/Feb/2025

\lx je
\hm 2
\dialx GO PA BO
\va çe
\dialx GOs
\is grammaire_démonstratif
\ps DEM.SG
\ge ce(tte)
\xv a-du je/çe thoomwã
\dialx GO
\xn cette femme descend
\xv ti je i tho ?
\dialx PA
\xn qui est-ce qui a appelé ?
\xv nu je nu nõõli
\dialx BO
\xn c'est moi qui l'ai vu
\xv ti je i a-daa-mi ?
\dialx PA
\xn qui est-ce qui est monté ici ?
\xv li ra u thaa mwã je dèè-we-li
\dialx PA
\xn elles creusent alors leur conduite d'eau
\dt 05/May/2024

\lx -je
\dialx GO
\is grammaire_pronom
\ps PRO 3° pers. SG (OBJ ou POSS)
\ge le ; la
\ge son ; sa ; ses
\dt 22/Feb/2025

\lx je-ba
\dialx GOs PA
\is grammaire_démonstratif
\ps PRO.DEIC.2
\ge ce ...-là (sur le côté, latéralement)
\xv nõõle je-ba thoomwã-ba
\dialx GOs
\xn regarde cette fille-là
\dt 06/Nov/2021

\lx jebo
\dialx PA WEM
\is maison
\ps n
\ge poutre qui soutient la toiture
\cf pwabwani ; pwabwaning
\ce poutre maîtresse
\dt 10/Jan/2022

\lx jee
\dialx PA
\is religion
\ps n
\ge lutins
\dn petits êtres aux cheveux longs, qui jouent des tours aux humains
\cf uramõ
\dialx GOs
\dt 10/Jan/2022

\lx jego
\dialx PA
\is maison
\ps v
\ge couvrir de paille racines vers l'extérieur
\dt 26/Jan/2019

\lx jèmaa
\dialx BO PA
\va dada
\dialx GO(s) PA
\sn 1
\is insecte
\ps n
\ge cigale
\dn petite, à tête verte, vit en forêt
\sn 2
\is caractéristiques_personnes
\ps n
\ge pleurnicheur
\dn emploi métaphorique
\dt 08/Feb/2025

\lx je-nã
\ph ɲɟeɳa
\dialx GOs
\va je-ne
\dialx PA BO
\is grammaire_démonstratif
\ps DEM.DEIC.2 médial; ANAPH
\ge ce ...-là ; voilà ; c'est cela !
\ge c'est cela ; exactement ; tout à fait
\xv mõû-çö mwã je-ne
\dialx GO
\xn celle-là est ton épouse
\xv je-nã mwã !
\xn c'était cela !
\xv je-nã thoomwã
\xn cette femme-là (dont on a parlé)
\xv çö trône je-nã gu dròrò ?
\dialx GO
\xn tu as entendu ce bruit hier ?
\xv mènòòn je-ne hinõ-al
\dialx PA
\xn demain à cette heure-ci
\dt 03/Feb/2025

\lx je-ò
\dialx GOs PA WEM
\is grammaire_démonstratif
\ps DEM.DEIC lointain ou ANAPH
\ge ce ...-là
\xv e phe-e xo je-ò thoomwã
\dialx WEM
\xn cette femme-là (en question) la prend
\xv ti mwã jè-ò
\dialx GOs
\xn qui est-ce celui-là ?
\xv i phe ra paò-da ni jè-ò mwa
\dialx PA
\xn il la prend et la jette à l’intérieur de cette maison-là
\dt 25/Dec/2021

\lx jeworo
\dialx BO
\is temps_découpage
\ps n
\ge chant du coq (à l'aube)
\dt 15/Aug/2021

\lx jeyu
\ph ɲɟeyu
\dialx GOs WE
\va jeü
\dialx PA BO [BM]
\is arbre
\ps n
\ge kaori
\dn son fruit représente les clans autour de la chefferie
\sc Agathis moorei
\scf Araucariacées
\dt 27/Aug/2021

\lx ji
\dialx GOs
\va jim
\dialx PA BO
\is crustacés
\ps n
\ge crabe de palétuvier
\dt 29/Jan/2019

\lx jia ?
\dialx GOs BO
\is grammaire_interrogatif
\ps PRON.INT
\ge le(s)quel(s) ?
\xv nu phe jia ?
\xn je prends le(s)quel(s) ?
\xv nu thooma-ni jia ?
\xn j'appelle le(s)quel(s) ?
\xv pòi-m (a) jia ?
\dialx BO
\xn lequel est ton enfant ?
\xv nu phe tiiwo jia ?
\dialx GOs
\xn je prends quel livre ?
\cf ia ?
\ce quel ; où ? (+ nom)
\dt 22/Feb/2025

\lx jibaale
\dialx GOs PA
\is insecte
\ps n
\ge araignée
\dn venimeuse, grosse et noire, vit dans la terre
\dt 08/Feb/2025

\lx jibwa
\dialx GOs BO
\is jeu
\ps v
\ge faire des galipettes ; faire des tonneaux
\ge retourner en bousculant
\dt 15/Aug/2021

\lx jingã
\dialx GOs
\va jing
\dialx PA
\va pajin
\dialx BO [Corne]
\is corps
\ps n
\ge gencives
\xv jingã-ny
\dialx PA
\xn mes gencives (une fois les dents tombées)
\dt 19/Dec/2021

\lx jige
\dialx GOs
\is armes
\va jigèl
\dialx PA
\va jigal
\dialx BO
\ps n
\ge fusil de chasse
\xv jige bu-ò
\xn un fusil qui frappe loin (à longue portée)
\se pao jige
\sge tirer (au fusil)
\xv jige i je
\dialx GO
\xn son fusil
\xv jigali-n
\dialx BO
\xn son fusil
\xv jigèle-n
\dialx PA
\xn son fusil
\dt 08/Oct/2021

\lx jigo
\dialx BO
\is arbre
\ps n
\ge palétuvier
\sc Rhizophora sp.
\scf Rhizophoracées
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx jiilè
\ph 'ji:lɛ
\dialx GOs
\is mouvement
\ps v
\ge tituber
\dt 25/Jan/2019

\lx jili
\dialx GOs PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal
\ge cela (éloigné, invisible mais audible)
\dn emploi anaphorique et cataphorique référant à qqch. dont je vais parler et que vous allez écouter
\xv çö trône jili gu ?
\dialx GO
\xn tu entends ce bruit ? (obligatoirement au présent)
\xv da jili gu ?
\dialx GO
\xn qu'est-ce que ce bruit ?
\xv ti jili ?
\dialx GO
\xn qui est-ce qu'on entend ?
\xv ti jili waa ?
\dialx GO
\xn qui est-ce qui chante ?
\xv ti jili kãã ?
\dialx GO
\xn qui est-ce qui crie ?
\xv kêbwa çö jara trõne jili
\dialx GO
\xn n'écoute surtout pas cela
\xv yu khò nõõli kõbwe i w(h)ãã-ili jili thoo-we
\dialx PA
\xn vois un peu comment est l'écoulement de l'eau (audible)
\xv jili mwarang ye nu vha na=le xo mwarang yaa ra me-nevwuu-n èna Phabua (Mwarang.003)
\dialx PA
\xn ce message coutumier dont je vais parler, c'est le message tel qu'il est fait à Paimboa
\mr forme contractée de |lx{je-ili}
\dt 22/Feb/2025

\lx jime
\dialx GOs
\is discours
\ps v
\ge assembler
\dn faire la synthèse des paroles avant de conclure
\dt 08/Feb/2025

\lx jimòng
\dialx PA
\is eau
\ps n
\ge mare qui sèche au soleil
\dt 08/Feb/2025

\lx ji-ni
\ph jiɳi
\dialx GOs
\va je-ni
\dialx GOs
\va je-nim, ji-nim
\dialx PA
\is grammaire_démonstratif
\ps DEM.DEICT (médial ou proche)
\ge cela
\ge cela (péjoratif, mis à distance) |dialx{PA}
\xv nõõle ji-ni thoomwã-ni
\xn regarde cette fille-là
\xv ji-ni çö kha-whili-je ça Thulirãã
\xn celle que tu emmènes c'est Thulirãã (un mauvais esprit)
\xv je-ni co phee-je ça Thulirãã
\xn celle que tu emmènes, c'est Thulirãã
\dt 27/Mar/2022

\lx jińõ ce-bo
\ph jinɔ̃ cɨ-mbo
\dialx GOs
\va jińõ ce-bon
\ph jinɔ̃ cɨ-mbɔn
\dialx PA BO
\is feu
\ps n
\ge tisons
\ge brandon de la bûche pour le feu de la nuit
\dt 08/Feb/2025

\lx jińõ yaai
\ph 'jinɔ̃ 'ya:i
\dialx GOs PA BO
\is feu
\ps n
\ge tisons
\ge brandon
\dn utilisé pour allumer le feu ou s'éclairer dans le noir
\dt 08/Feb/2025

\lx jinoji
\dialx BO
\is igname
\ps n
\ge igname (violette, tendre)
\nt selon Dubois
\dt 17/Feb/2025

\lx jińu
\hm 1
\ph 'jinu:
\dialx GOs PA BO
\is religion
\ps n
\ge puissance ; force spirituelle
\ge vertu (d'une plante, d'un sorcier)
\ge esprit
\dt 15/Aug/2021

\lx jińu
\hm 2
\ph 'jinu
\dialx GOs
\va jinuu-n, jinoo-n
\dialx BO
\is température
\ps n
\ge chaleur
\se jińu yaai
\dialx GO PA
\sge chaleur du feu
\se jińu a
\dialx GO
\va jinuu al
\dialx BO
\sge chaleur du soleil
\dt 12/Jan/2022

\lx jiò
\dialx GOs
\va jòò-n
\dialx BO [Corne]
\is corps
\ps n
\ge tempe
\dt 24/Jan/2019

\lx jitrua
\ph 'jiʈua, jiɽua
\dialx GOs
\is armes
\va jirua
\dialx GO(s)
\va jitua
\dialx PA BO
\ps n
\ge arc
\se me-jitrua
\sge flèche ; pointe de la flèche
\xv jitua i je
\dialx PA
\xn son arc
\dt 15/Feb/2019

\lx jiu
\dialx GOs
\va jiu-n
\dialx PA BO [BM]
\is grammaire_quantificateur_mesure
\ps n.QNT
\ge totalité ; tout
\se jiu-ã ; jiu-la
\dialx PA
\sge nous tous ; eux tous
\xv phe jiu-n
\dialx PA
\xn prends la totalité
\xv la huu jiu-n
\dialx PA
\xn ils mangent la totalité
\xv kavwö nu khõbwe jiu-n
\dialx PA
\xn tu n'as pas tout dit
\xv nu nèè jiu-n
\dialx BO
\xn je l'ai fait en totalité
\xv whaya jiu-la ?
\dialx BO
\xn combien sont-ils en tout ?
\dt 22/Feb/2025

\lx jivwa
\ph 'jiβa
\dialx GOs PA BO
\va jipwa
\dialx GO(s)
\is grammaire_quantificateur_mesure
\ps QNT
\ge nombre
\ge tous ; tout le monde ; totalité
\ge plusieurs
\xv li nhuã mwã i je jivwa la yada-li xo kêê-je mã õã-je
\dialx GO
\xn son père et sa mère lui lèguent tous leurs biens
\xv pe-jivwa-li/-lò/-la
\dialx GO
\xn ils (|lx{-li} duel)/ils (|lx{-lò} triel)/ils (|lx{-lò} pl.) sont en nombre égal
\xv a-mi jivwa
\dialx PA
\xn venez tous!
\dt 16/Feb/2025

\lx jivwa meewu
\dialx PA
\is grammaire_quantificateur_mesure
\ps n
\ge toutes sortes de
\dt 16/Feb/2025

\lx jiwaa
\ph 'jiwa:
\dialx GOs
\va jiia
\dialx WEM PA BO
\is oiseau
\ps n
\ge "siffleur" ; échenilleur calédonien
\sc Coracina caledonica caledonica
\scf Campéphagidés
\gb Melanesian Cuckoo-shrike
\dt 22/Feb/2025

\lx jo
\hm 1
\dialx GOs
\is grammaire_adverbe
\ps ADV
\ge soudain
\xv i jo gaajò
\dialx GO
\xn il est tout à coup surpris
\dt 25/Jan/2019

\lx jo
\hm 2
\dialx GOs BO
\va ço
\dialx GO(s)
\is grammaire_conjonction
\ps CNJ
\ge et après ; puis
\dt 29/Jan/2019

\lx jò
\hm 1
\dialx GOs
\va jòn
\dialx BO
\is action_corps
\ps v
\ge sauter ; sursauter
\xv nu jò
\xn je sursaute
\dt 29/Apr/2024

\lx jò
\hm 2
\dialx GOs
\va jòm
\dialx PA BO
\va jem
\dialx BO (Corne)
\is arbre
\ps n
\ge bancoulier
\dn on extrait une teinture noire de son fruit ; cette couleur est l'un des symboles des premiers occupants, des maîtres de la terre et des soutiens de la chefferie
\sc Aleurites moluccana L.
\scf Euphorbiacées
\se pò-jò
\dialx GO
\va pò-jòm
\dialx PA
\sge fruit de bancoulier
\dt 21/Feb/2025

\lx jòjò
\ph ɲɟɔɲɟɔ, ndjɔɲdjɔ
\dialx GOs
\va jòjòn
\ph ɲɟɔɲɟɔn
\dialx PA BO
\is fonctions_naturelles
\ps v
\ge trembler (de peur, de froid, de colère)
\ge vibrer ; réagir (à un bruit)
\xv nu jòjò
\dialx GO
\xn je tremble (de froid)
\xv i jòjòn na ênêdu ni puu-mwã
\dialx PA
\xn l grelotte dehors au mur de la maison
\xv e jò mwa
\xn la maison tremble (sous le vent)
\xv e jò dili
\xn la terre tremble
\xv i pa-jòni-nu xo kuau
\dialx BO
\xn le chien m'a fait sursauter
\ng causatif: |lx{pa-jòni}
\gt faire sursauter
\dt 14/Apr/2024

\lx jòme
\ph ɲɟɔme, djɔme
\dialx GOs
\is action_corps
\ps v
\ge secouer
\xv e jòme-nu xo loto
\xn la voiture m'a retourné/secoué
\xv nu pha-jòme-je
\xn je l'ai secoué
\ng causatif: |lx{pha-jòme}
\gt secouer qqn.
\dt 22/Feb/2025

\lx jomûgò
\ph ɲɟomûgɔ, ɲdjomûgɔ
\dialx GOs
\is oiseau
\ps n
\ge grive perlée (Méliphage barré)
\sc Glycifohia undulata
\scf Méliphagidés
\gb Barred Honeyeater
\dt 27/Aug/2021

\lx jòòwe
\ph ɲɟɔ:we, ndjɔ:we
\dialx GOs
\is caractéristiques_objets
\ps n
\ge objets ou débris flottés
\dt 03/Feb/2019

\lx jua
\dialx GOs BO
\is santé
\ps n
\ge verrue
\dt 25/Jan/2019

\lx jumeã
\ph ɲɟumeɛ̃, ɲdjumeɛ̃
\dialx GOs
\is poisson
\ps n
\ge mulet (le plus gros) ou maquereau
\sc Mugil cephalus
\scf Mugilidés
\cf whai
\ce mulet (de taille juvénile)
\cf naxo
\ce mulet noir de rivière
\cf mene
\ce mulet queue bleue
\dt 27/Aug/2021

\lx jumo
\dialx BO
\is poisson
\ps n
\ge rouget
\nt selon BM
\dt 27/Mar/2022

\lx jutri
\ph ɲɟuʈi, ɲdjuʈi
\dialx GOs
\is corps_animaux
\ps n
\ge nageoire
\dt 03/Feb/2019

\lx juyu
\dialx BO
\is plantes
\ps n
\ge cycas
\sc Cycas circinalis
\scf Cycadacées
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ka
\hm 1
\dialx GOs
\va ga, xa
\dialx GO(s) PA
\va ko
\dialx GO(s)
\sn 1
\is grammaire_conjonction
\ps COORD
\ge et alors ; et aussi ; et en même temps
\xv e trêê ka hopo
\dialx GO
\xn il court en mangeant (et aussi mange)
\xv e trêê ka wa
\dialx GO
\xn il court en chantant (et aussi chante)
\sn 2
\is grammaire_conjonction
\ps REL
\ge qui ; que
\xv kòlò je-nã wamã xa yazaa-je Mwe
\dialx GO
\xn chez ce vieux qui s'appelle Chouette
\xv kòlò je-nã wamã ka yala-n i Mwèn
\dialx PA
\xn chez ce vieux qui s'appelle Chouette (lit. dont le nom est Chouette)
\dt 22/Feb/2025

\lx ka
\hm 2
\dialx GOs PA BO
\va kò
\dialx GO(n)
\sn 1
\is temps_découpage
\ps n
\ge année
\xv ni ka xa po-xè
\xn la même année
\xv we-xe ka
\dialx BO
\xn une année
\xv nye ka hèbun
\dialx BO
\xn l'année d'avant
\xv nye ka hã
\dialx BO
\xn cette année
\se kaa dròòrò
\sge l'an dernier
\se kaa mõnõ
\dialx GO
\sge l'an prochain
\sn 2
\is cultures
\ps n
\ge récolte d'ignames
\ge plante annuelle
\et *taqu
\el POc
\dt 28/Jan/2022

\lx kã
\dialx GOs
\va kam
\dialx BO
\va kham
\dialx PA
\sn 1
\is action
\ps v
\ge érafler
\ge effleurer ; frôler
\ge glisser
\sn 2
\is déplacement
\ps v
\ge passer à côté en frôlant
\sn 3
\is interaction
\ps v
\ge éviter
\ge manquer
\dt 29/Aug/2021

\lx ka ?
\dialx GOs BO
\is grammaire_interrogatif
\ps v.INT
\ge que se passe-t-il ?
\ge pourquoi ? ; comment ?
\xv çö gi xa çö ka ?
\dialx GO
\xn pourquoi pleures-tu ?
\xv çö ka ?
\xn comment vas-tu ? ; qu'as-tu ? ; que t'arrive-t-il ? ; qu'y a-t-il ?
\xv e ka ?
\dialx GOs
\xn (alors) comment est-ce ? ; comment ça va ? ; qu'est-ce qui se passe ?
\xv e ka phagoo-m ? –  E zo
\dialx PA
\xn comment vas-tu ? – Ça va (se dit à quelqu'un de malade)
\xv i ka ?
\dialx BO
\xn (alors) comment est-ce ?
\xv i cabi inu ka ?
\dialx BO
\xn pourquoi m'a-t-il frappé ?
\dt 24/Feb/2025

\lx kaa
\hm 1
\dialx GOs
\is danse
\ps n
\ge battoir ; tambour
\dn en écorce pour rythmer la danse
\se ba-cabi kaa
\sge tambour ; battoir
\dt 22/Feb/2025

\lx kaa
\hm 2
\dialx GOs WE
\is eau_topographie
\ps n
\ge récif corallien
\dt 21/Feb/2025

\lx kãã
\ph kɛ̃:
\dialx GOs BO
\va khêê
\dialx PA
\is son
\ps v
\ge crier ; hurler ; pousser des cris ; crier de loin
\xv e kãã
\xn il pousse des cris
\dt 08/Oct/2021

\lx kaaça
\ph ka:ʒa
\dialx GOs
\is caractéristiques_objets
\ps v
\ge tendu (corde) ; raide
\dt 26/Jan/2019

\lx kããge
\dialx GOs
\va kããgèn
\dialx BO PA
\is fonctions_intellectuelles
\ps v
\ge croire ; espérer
\se me-kããgen
\dialx BO
\sge croyance; foi
\xv kavwa ai-nu vwö nu kããge khõbwe e mã
\dialx GOs
\xn je ne veux pas croire qu’il est mort
\ng v.t. |lx{kããgèni}
\gt croire en qqch.
\dt 21/Feb/2025

\lx kaageen
\dialx BO
\is interaction
\ps v
\ge obéir
\nt selon BM
\dt 26/Mar/2022

\lx kaaje
\dialx BO
\va kayè
\is plantes
\ps n
\ge jonc à corbeille
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx kaal
\dialx PA
\is médecine
\is action_plantes
\ps v
\ge cueillir des feuilles et herbes
\dn pour faire des médicaments
\dt 08/Feb/2025

\lx kaale
\dialx GOs PA BO
\is grammaire_modalité
\ps v
\ge laisser ; permettre
\xv kaale kee-çö ênè !
\dialx GOs
\xn laisse ton panier ici!
\xv kaale pu nu khõbwe-ayu-ni mwã ne ògine teen i ã
\dialx PA
\xn permettez-moi de dire comme cela que notre journée prend fin
\dt 05/Jan/2022

\lx kããle
\ph kɛ̃:le
\dialx GOs BO PA
\sn 1
\is médecine
\ps v
\ge soigner ; prendre soin de
\ge guérir
\sn 2
\is société
\ps v
\ge élever (enfants  animaux)
\ge s'occuper (de qqn.)
\sn 3
\is action
\ps v
\ge conserver
\cf yue
\ce adopter ; garder
\dt 22/Feb/2025

\lx kaamuda
\dialx PA
\is outils
\ps n
\ge hache (type de)
\dt 27/Jan/2019

\lx kaamweni
\ph ka:'mweɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge comprendre ; sage
\xv çö trõne-kaamweni ?
\dialx GO
\xn tu as bien compris ?
\xv çö hine-kaamweni ?
\dialx GO
\xn tu as bien compris ?
\xv çö kaamweni-zoo-ni ?
\dialx GO
\xn tu as bien compris ?
\xv kavwö nu trõne-kaamweni me-vhaa i la
\dialx GO
\xn je ne comprends pas leur façon de parler
\dt 14/Sep/2021

\lx kaano
\dialx GOs WEM
\is jeu
\ps n
\ge jeu de propulsion
\dn ce jeu se pratique avec deux bâtons: on tape sur une extrémité d'un bâton avec l'autre bâton et le gagnant est celui qui l'a propulsé le plus loin
\dt 22/Oct/2021

\lx ka-avè
\dialx BO
\is société
\ps n
\ge compagnon de naissance
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx Kaavwo
\ph 'ka:βo
\dialx PA
\is société_organisation
\ps n
\ge fille aînée de chef
\dt 27/Jan/2019

\lx kaavwu
\ph ka:βu
\dialx GOs
\va kapu
\dialx GO(s) arch.
\va kaawun
\dialx PA BO
\is société_organisation
\ps n
\ge gardien ; maître ; propriétaire
\xv kaawu-n
\dialx PA
\xn son maître
\xv kaawun i nu nye go
\dialx BO
\xn cette radio est à moi
\se kaavwu waaya
\dialx PA
\sge chef de guerre
\dt 24/Dec/2021

\lx kaavwu dili
\ph ka:βu dili
\dialx GOs PA WEM
\is société_organisation
\ps n
\ge maître du sol ; protecteur du sol (nom d'un clan)
\dt 08/Feb/2019

\lx kaaxo
\dialx GOs WE
\is parenté
\ps n
\ge cousin (terme respectueux)
\dn terme d'appellation ou de désignation envers des personnes plus âgées
\xv li pe-kaaxo
\xn ils sont cousins
\dt 21/Feb/2025

\lx kaayang
\dialx PA BO [Corne]
\is caractéristiques_objets
\ps v
\ge raide (être)
\ge tendu
\ge raide mort
\dt 20/Aug/2021

\lx kaba
\dialx GOs
\is action
\ps v
\ge protéger (se)
\xv nu kaba
\xn je me protège
\dt 02/Feb/2019

\lx kaba we
\dialx WEM WE BO
\is action_eau_liquide_fumée
\ps v
\ge puiser ; prendre de l'eau
\dn avec un petit récipient
\ge écoper
\ge retirer d'une marmite
\dn surtout du liquide
\xv i kaba we
\dialx BO
\xn elle puise de l'eau
\se ba-kam thõn
\dialx BO
\sge louche
\cf tröi
\ce puiser
\cf ii
\ce servir ; sortir de la marmite (riz)
\dt 22/Feb/2025

\lx kabu
\ph 'kabu
\dialx GOs
\va kabun
\dialx PA BO
\sn 1
\is religion
\ps v
\ge interdit ; sacré
\xv e kabu na mõ pweni nõ-ni
\dialx GO
\xn il nous est interdit de pêcher ce poisson-là
\xv kabu ne lha kòzole
\dialx GO
\xn c'est interdit de la gaspiller
\se mõ-xabu
\dialx GO
\sge église ; temple
\se mwa-kabun
\dialx PA
\sge église ; temple
\se pwe-kabun
\dialx PA
\sge sorcier (celui qui peut faire mourir)
\se camwa kabun
\dialx BO
\sge banane chef
\ng v.t. |lx{kaabuni} |ph{ka:'buni}
\sn 2
\is temps_découpage
\ps n
\ge semaine
\xv õ-tru xa e kaò ni kabu-è
\dialx GO
\xn cela fait deux fois que cela déborde (rivière) dans la semaine
\se ni kabu
\dialx GO
\sge dans la semaine
\se kabu dròòrò
\sge la semaine dernière
\et *tampu
\eg interdit
\el POc
\dt 22/Feb/2025

\lx kãbwa
\ph kɛ̃bwa
\dialx GOs BO
\va kabwa
\dialx PA
\is religion
\ps n
\ge génie forestier ; esprit ; ancêtre
\ge totem ; esprit bienfaisant ; dieu
\cf kãbwa-hili
\ce esprit (des forêts) (|lx{hili}: se retirer comme un serpent dans un trou)
\dt 07/Jan/2022

\lx kãbwaçò
\ph kɛ̃'bwaʒɔ, kɛ̃'bwadʒɔ
\dialx GOs
\va kabwayòl
\dialx PA BO
\va kabwoyòl, kaboyòl
\dialx BO [BM]
\is poisson
\ps n
\ge requin
\dt 21/Mar/2023

\lx kaça
\ph kaʒa
\dialx GOs
\va kaya
\dialx PA BO
\sn 1
\is configuration
\ps n.loc
\ge arrière (qqch., inanimé)
\se kaça wõ
\dialx GO
\sge la poupe du bateau
\se kaca kòò-n
\dialx WEM
\sge talon (arrière du pied)
\sn 2
\is grammaire_préposition_locative
\ge derrière
\ge après
\xv e phu kaça-je
\dialx GO
\xn il vole derrière lui
\dt 17/Feb/2025

\lx kaça hovwo
\dialx GO
\va kaya hovho
\dialx PA
\is temps_découpage
\ps n
\ge après-midi
\dn (lit. après le déjeuner)
\dt 08/Feb/2025

\lx kaça mõnõ
\dialx GO
\is temps_deixis
\ps n
\ge après-demain
\dt 29/Jan/2022

\lx kaça mwa
\dialx GOs
\va kaya-mwa
\dialx BO
\sn 1
\is maison
\ps n.loc
\ge arrière de la maison
\an me-mwa
\dialx GOs
\at le devant de la maison ; le sud
\sn 2
\is nom_locatif
\ps n.loc
\ge nord du pays
\dt 03/Feb/2025

\lx kaça-kabu
\dialx GOs
\va wa-kabun
\dialx PA BO
\is temps_jours
\ps n
\ge lundi (|lx{kaça-kabu}
\dn (lit. après le jour sacré)
\dt 08/Feb/2025

\lx kaça-kò
\ph ka'ʒa-kɔ
\dialx GOs
\is corps
\ps n
\ge talon
\dn (lit. arrière du pied)
\dt 09/Feb/2025

\lx kaci
\dialx PA BO
\is oiseau
\ps n
\ge émouchet bleu ; faucon ; buse blanche et noire
\sc Accipiter haplochrous
\scf Accipitridés
\gb White-bellied Goshawk
\dt 27/Aug/2021

\lx kacia
\dialx BO [BM]
\is plantes
\ps n
\ge lantana
\bw acacia (FR)
\dt 29/Jan/2019

\lx kâdi
\dialx GOs PA BO
\is interaction
\ps n
\ge récompense ; rétribution ; remerciement
\xv na kâdi
\xn donner une récompense
\xv kâdi-n
\dialx PA
\xn sa récompense
\dt 25/Aug/2021

\lx kâduk
\dialx BO
\va kâdu
\is plantes
\ps n
\ge liane de forêt
\dn utilisée pour la construction de cases
\nt selon Corne ; non vérifié
\dt 08/Feb/2025

\lx kadra
\dialx GOs
\va kadae
\dialx BO
\sn 1
\is action_corps
\ps v
\ge bien appuyer les pieds pour marcher
\sn 2
\is préfixe_sémantique_action
\ps v
\ge fouler au pied
\xv nu kadra bwa mu-cee
\dialx GO
\xn il foule au pied cette fleur
\xv i kadae nye tòimwa
\dialx BO
\xn il foule au pied cette vieille
\dt 21/Mar/2023

\lx kae
\dialx GOs PA WEM WE BO
\sn 1
\is interaction
\ps v.t.
\ge enlever ; ravir (femme)
\xv e kae-je
\dialx GO
\xn il l'a enlevée
\xv i kae-je ku Teã-ma
\dialx PA
\xn le chef l'a enlevée
\xv i kae na hii-n
\dialx PA
\xn il le lui a enlevé des mains
\sn 2
\is interaction
\ps v.t.
\ge garder pour soi ; mettre en sûreté
\xv ku piça vwö mõ kae mõlò
\dialx GO
\xn il est difficile de préserver la vie
\dt 06/Nov/2021

\lx kaè
\dialx GOs PA BO
\sn 1
\is cucurbitacée
\ps n
\ge citrouille
\sc Cucurbita pepo L.
\scf Cucurbitacées
\se kaè mii
\sge citrouille rouge
\se kaè phai
\sge citrouille
\sn 2
\is cucurbitacée
\ps n
\ge calebasse ; gourde
\sc Lagenaria siceraria
\scf Cucurbitacées
\se kaè pulo
\sge calebasse blanche
\se kaè pwali
\sge calebasse longue
\dt 30/Dec/2021

\lx kaè cèni
\ph 'ka.ɛ 'cɛni
\dialx GOs
\is cucurbitacée
\ps n
\ge citrouille 
\dn (lit. citrouille comestible)
\sc Cucurbita pepo L.
\scf Cucurbitacées
\dt 09/Feb/2025

\lx kae mõõxi
\dialx GOs PA
\is action
\ps v
\ge sauver qqn. ; sauver ; préserver (vie)
\xv e kae mõõxi-nu
\dialx GO
\xn il m'a sauvé la vie ; il m'a ravi à la mort
\xv i kae mõõxi-ny
\dialx PA
\xn il m'a sauvé
\dt 22/Feb/2025

\lx kaè thraxilò
\ph 'ka.ɛ 'ʈhaɣilɔ
\dialx GOs
\va kaè kuni
\dialx GO(s)
\is cucurbitacée
\ps n
\ge pastèque
\sc Citrullus vulgaris Schrad.
\scf Cucurbitacées
\dt 27/Aug/2021

\lx kae wòdo
\dialx GOs
\va kae wedo
\dialx PA
\is coutumes
\ps v
\ge suivre les usages
\xv mwa kae wedo
\dialx PA
\xn nous suivons les usages
\dt 28/Jan/2019

\lx kaen
\dialx PA
\is mollusque
\ps n
\ge gastéropode d'eau douce
\dt 29/Jan/2019

\lx kafaa
\dialx GOs
\is insecte
\ps n
\ge cafard
\bw cafard (FR)
\dt 29/Jan/2019

\lx kafe
\dialx GOs
\va kape
\dialx PA BO WEM WE
\is nourriture
\ps n
\ge café
\bw café (FR)
\xv li thu kape
\dialx WEM
\xn elles font du café
\dt 26/Jan/2019

\lx kâgao
\dialx GOs BO [Corne]
\is plantes
\ps n
\ge liane sauvage
\sc Ipomœa cairica (L.) Sweet
\scf Convolvulacées
\dt 27/Aug/2021

\lx kãgu
\ph kɛ̃ŋgu
\dialx GOs
\va kãgu-n
\dialx BO PA
\wr A
\is religion
\ps n
\ge esprit ; âme
\wr B
\is interaction
\ps v
\ge suivre qqn. comme son ombre
\dn se dit de qqn. à qui on est très attaché
\xv e kãgu-je
\xn il le suit partout (comme son ombre ; lit. c'est son ombre)
\et *tanu, *kanitu
\el POc
\dt 22/Feb/2025

\lx kãgu-hênû
\dialx GOs
\is caractéristiques_objets
\ps n
\ge négatif d'une photo
\cf hênû
\ce photo ; image
\dt 22/Feb/2025

\lx kãgu-mwã
\dialx GOs
\is religion
\ps n
\ge nain
\dn (lit. esprit de la maison)
\dt 08/Feb/2025

\lx kai
\hm 1
\dialx GOs PA
\is grammaire_préposition
\ps ASSOC
\ge avec
\dn (lit. derrière + animé)
\xv a kai-nu
\dialx GOs
\xn viens avec moi
\xv i a kai-n
\dialx PA
\xn il part avec lui
\xv ti nye u-da/ã-da kai-je ni mwã ?
\dialx GOs
\xn avec qui est-il entré dans la maison ?
\xv e a-xai kêê-je mã õã-je
\dialx GOs
\xn elle est partie avec son père et sa mère
\dt 08/Feb/2025

\lx kai
\hm 2
\dialx GOs BO PA
\sn 1
\is corps
\ps n
\ge dos
\xv kai-ny
\dialx PA BO
\xn mon dos
\sn 2
\is grammaire_préposition_locative
\ps n.loc
\ge après (qqn., animé)
\ge derrière 
\xv ge je kai-nu
\dialx GO
\xn il est derrière moi/dans mon dos
\xv ge je kai-ny
\dialx PA BO
\xn il est derrière moi
\xv a mwã kai-n (ə) Kaawo
\dialx PA
\xn Kaawo le suit
\et *taku
\el POc
\dt 22/Feb/2025

\lx kãjawa
\ph kɛ̃ɲɟawa, kɛ̃ɲdjawa
\dialx GOs
\va kajaa
\dialx BO
\is igname
\ps n
\ge igname (clone)
\dn selon Dubois
\dt 17/Feb/2025

\lx kaje
\dialx BO
\is taro
\ps n
\ge taro (clone) de terrain sec
\nt selon Dubois
\dt 27/Mar/2022

\lx kake
\dialx GO
\va xaxe
\dialx GO PA
\is grammaire_conjonction
\ps CNJ
\ge quoique
\dt 19/Dec/2021

\lx kakorola
\dialx PA BO [Corne]
\is insecte
\ps n
\ge cancrelat ; cafard
\bw cancrelat (FR)
\dt 29/Jan/2019

\lx kakulinãgu
\dialx GOs BO
\is oiseau
\ps n
\ge coucou à éventail ; gammier (noir, petit)
\sc Cacomantis pyrrhophanus pyrrhophanus
\scf Cuculidés
\gb Fan-tailed Cuckoo
\dt 22/Feb/2025

\lx kala
\is déplacement
\dialx GOs PA BO
\ps v
\ge partir ; quitter
\ge sauver (se)
\ge fuir du mauvais côté ; prendre la fuite
\xv va kala-du mwã
\xn nous repartons en bas maintenant
\xv e kalae-nu xo pwiri
\dialx GOs
\xn la perruche a fui en m'abandonnant
\xv e kala gèè i je
\dialx PA
\xn il laisse sa grand-mère
\dt 06/Nov/2021

\lx kalãnge
\ph ka'lãŋe
\dialx GOs
\va kala-kagee
\dialx PA
\is action
\ps v
\ge laisser ; quitter
\ge abandonner
\xv nu kalãnge-we
\xn je vous quitte
\dt 20/Aug/2021

\lx kalanden
\dialx PA BO
\is caractéristiques_personnes
\ps v
\ge négligent ; insouciant
\dt 26/Jan/2019

\lx kaleva
\dialx PA BO [BM]
\va kaleba
\dialx BO
\is caractéristiques_objets
\ps v
\ge plat
\ge aplatir ; aplati
\xv i kaleva nye paa
\dialx BO
\xn ce caillou est plat
\dt 08/Oct/2021

\lx kalò
\dialx GOs
\is jeu
\ps n
\ge grosse bille
\dt 28/Jan/2019

\lx kalòya
\dialx GOs
\is jeu
\ps v ; n
\ge corde à sauter ; sauter à la corde
\xv e cö kalòya
\xn elle saute à la corde
\xv e pao kalòya
\xn elle fait tourner la corde
\dt 19/Aug/2023

\lx kalu
\dialx GOs
\va kaalu
\dialx BO PA
\sn 1
\is mouvement
\ps v
\ge tomber (d'un animé)
\xv e kalu ã ẽnõ
\dialx GO
\xn l'enfant est tombé !
\xv phaa-kaalu-je
\dialx PA
\xn fais-le tomber !
\ng causatif: |lx{pha-kalu}
\gt faire tomber
\sn 2
\is action
\ps v ; n
\ge accident ; avoir un accident
\xv e kalu na bwa loto
\dialx GO
\xn il a eu un accident de voiture
\xv i kaalu na bwa cee
\dialx BO
\xn il est tombé de l'arbre
\cf thrõbo
\ce tomber (chose) ; naître
\dt 22/Feb/2025

\lx kaluni
\dialx GOs PA
\is coutumes
\ps v
\ge enlever les dons lors des cérémonies coutumières
\xv ba-kaluni êńa kui
\xn geste pour enlever ces ignames
\dt 21/Mar/2023

\lx kãmakã
\ph kɛ̃'makɛ̃
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge bizarre ; distrait
\se a-kamakã
\sge un distrait
\xv e pe-kamakã
\xn il est distrait
\dt 21/Aug/2021

\lx kamwe ?
\dialx GOs BO
\va kaamwe
\dialx GOs
\is grammaire_interrogatif
\ps v.INT
\ge que faire ?
\xv çò kamwe-je ?
\xn qu'est-ce que vous lui avez fait ?
\xv xa çö kaamwe-lha ?
\dialx GO
\xn et que leur as-tu fait ?
\dt 20/Mar/2023

\lx kamwene ?
\ph ka'mweɳe
\dialx GOs
\va kamwêlè ?
\dialx GO(s)
\va kamwêli
\dialx BO
\is grammaire_interrogatif
\ps v
\ge comment faire ? ; que faire ? (être comment?) ; faire ainsi
\xv çö kamwene ?
\dialx GOs
\xn comment (l')as-tu fait ?
\xv çö kamwêlè tiiwo i nu ?
\dialx GOs
\xn qu'est-ce que tu as fait avec mon livre ?
\xv cò kamwêlè me nee-xo ?
\dialx GOs
\xn comment avez-vous (duel) fait pour le faire ?
\xv jò kamwêli me nee-xo ?
\dialx BO
\xn comment avez-vous (duel) fait pour le faire ?
\cf ka?
\ce comment ?
\dt 24/Feb/2025

\lx kanang
\dialx PA
\is fonctions_naturelles_animaux
\ps v
\ge coïter (animaux)
\dt 25/Jan/2019

\lx kanii
\ph kaɳi:
\dialx GOs
\is oiseau
\ps n
\ge canard (importé)
\dt 29/Jan/2019

\lx kaò
\ph kaɔ
\dialx GOs PA BO
\is eau
\ps n
\ge inondation ; courant
\xv kò-mõnu kaò
\dialx PA
\xn la saison des pluies approche
\xv i paò khi ûûni=du bwa kaò
\dialx PA
\xn il le lance et le renverse dans le courant
\dt 27/Aug/2023

\lx karava
\dialx GO
\is navigation
\ps n
\ge pirogue
\nt non vérifié
\dt 25/Jan/2018

\lx kari
\dialx PA
\sn 1
\is action_corps
\ps v
\ge cacher (se)
\ge rester à l'écart
\se kari êgu
\sge personne craintive, qui reste à l'écart
\sn 2
\is caractéristiques_animaux
\ps v
\ge craintif
\dn surtout utilisé pour les animaux
\se kari mèni
\sge oiseau craintif
\dt 08/Feb/2025

\lx karolia
\dialx BO
\is jeu
\is mouvement
\ps v
\ge sauter à la corde
\nt selon Corne
\dt 27/Mar/2022

\lx karòò
\dialx GOs WEM
\va kharo
\dialx BO
\is eau_mer_plante
\ps n
\ge corail ; chaux
\dn selon [BM]
\cf vaxaròò
\ce patate de corail
\dt 09/Feb/2025

\lx katòli
\dialx GOs
\va katòlik
\dialx PA
\is religion
\ps n
\ge catholique
\bw catholique (FR)
\cf mõ-mãxi
\dialx GOs
\ce protestants ; prier
\dt 22/Feb/2025

\lx katre
\ph ka:ʈe, ka:ɽe
\dialx GOs
\va kaarèng
\dialx PA BO
\va katèng
\dialx BO
\va katèk
\dialx BO [Corne]
\is temps_atmosphérique
\ps n
\ge brouillard
\dt 06/Feb/2019

\lx katrepa
\ph kaʈepa
\dialx GOs
\is navigation
\ps n
\ge pirogue à balancier
\dt 06/Feb/2019

\lx katri
\ph kaʈi
\dialx GOs
\va kari
\dialx PA BO
\sn 1
\is plantes
\ps n
\ge gingembre (culinaire)
\sn 2
\is couleur
\ps n
\ge jaune ; orange ; curry
\dt 15/Sep/2021

\lx katria
\ph 'kaɽia
\dialx GOs
\va karia
\dialx GO(s)
\va kathia, karia
\dialx PA
\va katia
\dialx BO
\is santé
\ps v ; n
\ge lèpre ; lépreux ; avoir la lèpre
\bw katia (POLYN) (PPN *katia)
\dt 06/Feb/2019

\lx katrińi
\ph kaʈini
\dialx GOs
\va karini, karili
\dialx BO
\is insecte
\ps n
\ge mille-pattes
\dt 29/Jan/2019

\lx kau-
\dialx GOs BO
\sn 1
\is temps
\ps n
\ge année de
\sn 2
\is étapes_vie
\ps n
\ge âge
\xv kau-n |dialx{PA}, kau-je |dialx{GOs}
\xn son âge
\xv pòniza kau-çö ?
\dialx GO
\xn quel âge as-tu ?
\xv waya kau-m ?
\dialx BO
\xn quel âge as-tu ?
\se pe-kau-li
\sge ils ont le même âge
\cf ka
\ce année
\dt 24/Jan/2022

\lx kãûbi
\dialx GOs PA
\is action_tête
\ps v
\ge casser avec les dents ; écraser avec les dents (qqch. de dur)
\cf cubii
\ce déchirer avec les dents
\dt 17/Feb/2025

\lx kauda
\dialx BO
\is guerre
\ps v
\ge réfugier (se)
\nt selon Corne, Haudricourt ; non vérifié
\dt 26/Mar/2022

\lx kaureji
\ph kauredji
\dialx GOs
\va kaureim
\dialx PA
\is insecte
\ps n
\ge luciole
\dt 29/Jan/2019

\lx kausu
\dialx GOs
\is arbre
\ps n
\ge banian
\dn banian dont la sève sert à faire des balles de cricket
\bw caoutchouc (FR)
\dt 22/Feb/2019

\lx kava
\hm 1
\dialx GOs
\is poisson
\ps n
\ge dawa
\sc Naso unicornis
\scf Acanthuridés
\gb unicorn fish
\dt 27/Aug/2021

\lx kava
\hm 2
\dialx GO
\is action_plantes
\ps v
\ge cueillir (des fleurs, des herbes magiques)
\nt selon Haudricourt ; non vérifié
\dt 26/Mar/2022

\lx kavobe
\dialx GOs BO
\is igname
\ps n
\ge igname (petite)
\dt 29/Jan/2019

\lx kavuxavwu
\ph kavuɣaβu
\dialx GOs
\va kavu-avu
\dialx PA
\va kavu-kavu
\dialx BO (Corne)
\is plantes
\ps n
\ge fougère arborescente
\sc Cyathea sp.
\scf Cyathéacées
\dt 08/Jan/2022

\lx kavwa
\dialx GOs BO
\is grammaire_négation
\ps NEG
\ge ne … pas
\xv kavwa e bwòvwô ui la ẽnõ ni mõ-chomu ?
\dialx GOs
\xn n'est-il pas fatigué des enfants de l'école?
\xv Ôô ! e bwòvwô ui la !
\dialx GOs
\xn Oui ! il en est fatigué !
\xv êgõgò xa kavwa hine
\dialx GOs
\xn il ne le savait pas auparavant
\dt 04/Nov/2021

\lx kavwa hovwo
\ph kaβa hoβo
\dialx GOs
\is nourriture
\ps NEG
\ge non comestible
\dt 26/Jan/2019

\lx kavwègu
\ph kaβɛŋgu
\dialx GOs
\va kapègu
\dialx GO(s) arch.
\va kavègu
\dialx PA BO
\sn 1
\is société_organisation
\ps n
\ge chefferie
\ge cour de la chefferie
\xv kavwègu i aazo
\dialx GO
\xn la chefferie
\xv ni kavègu-la
\dialx PA
\xn dans leur chefferie
\xv kavègu i khiny
\dialx BO
\xn le territoire de l'hirondelle (conte)
\sn 2
\is habitat
\ps n
\ge village ; ensemble de maisons
\ge place du village
\dt 24/Dec/2021

\lx kavwö
\ph kaβω
\dialx GOs PA BO
\va kavwa
\dialx GOs BO
\va ka, kavwu
\dialx PA
\va kapoa
\dialx arch.
\is grammaire_négation
\ps NEG
\ge ne … pas
\xv kavwö nu trõne
\dialx GO
\xn je n'ai pas entendu
\xv kavwö pwa
\dialx GO
\xn il ne pleut pas
\xv kavwu pwal
\dialx PA
\xn il ne pleut pas
\xv kavwu nu hine
\dialx GO
\xn je ne sais pas
\xv kavwa ai-n
\dialx BO
\xn il n'a pas envie
\xv kavwa li tõne
\dialx BO
\xn ils n'entendent pas
\xv kavwö e
\dialx BO
\xn ce n'est pas ainsi
\se kavo … ro
\sge ne … plus
\cf kêbwa
\ce ne … pas (prohibitif)
\dt 21/Feb/2025

\lx kavwö hine
\ph kaβω hiɳe
\dialx GOs
\is grammaire_aspect
\ps v
\ge ne jamais faire qqch.
\dn (lit. ne pas savoir)
\xv e kavwö hine trûã
\dialx GOs
\xn il ne ment jamais
\dt 09/Feb/2025

\lx kavwö … gò
\dialx GOs
\is grammaire_aspect
\ps ASP
\ge pas encore
\xv kavwö e mããni gò
\dialx GO
\xn il ne dort pas encore
\xv kavwö e ne gò lã-nã
\dialx GO
\xn il n'a encore jamais fait ces choses-là
\xv kavwö nu ku nõõ-je gò
\dialx GO
\xn je ne l'avais encore jamais vu
\xv kavwö nu ku hovwo gò
\dialx GO
\xn je n'avais pas encore mangé
\cf penõõ
\dialx PA
\ce pas encore
\dt 20/Jan/2025

\lx kavwö … jara
\dialx GOs
\is grammaire_modalité
\ps MODAL.NEG
\ge ne pas vouloir
\xv kavwö nu jara phe
\xn je ne veux pas du tout le prendre
\dt 03/Feb/2025

\lx kavwö … mwã
\dialx GOs
\is grammaire_aspect
\ps ASP (révolu)
\ge ne … plus
\xv kavwö nu ẽnõ mwã
\xn je ne suis plus jeune
\xv kavwö tròòli mã mwã
\xn il n'est plus malade
\xv kavwö mããni mwã
\dialx GO
\xn il ne dort plus
\xv kavwö mi zo(ma) xa nõõli mwã pòi-î
\dialx GO
\xn nous ne reverrons plus notre enfant
\dt 04/Nov/2021

\lx kavwö … ne … gò
\ph kaβω ɳe
\dialx GOs
\is grammaire_aspect
\ps ASP
\ge encore jamais ; jamais
\xv kavwö nu ne ã-du gò Frans
\dialx GO
\xn je ne suis encore jamais allé en France
\xv kavwö nu ne nõõ-je
\xn je ne l'ai jamais vu(e)
\xv kavwö ne nee gò lã-nã
\xn il n'a encore jamais fait ces choses-là
\dt 21/Oct/2021

\lx kavwö … ne … taagin
\dialx PA BO [Corne]
\is grammaire_aspect
\ps ASP
\ge encore jamais ; jamais
\xv kavwö nu hovo taagin
\dialx BO
\xn je ne mange jamais
\dt 10/Mar/2023

\lx kavwö … penõõ
\dialx PA
\is grammaire_aspect
\ps ASP
\ge pas encore
\xv kavwö nu penõõ ku nõõ-je
\dialx PA
\xn je ne l'ai jamais vu
\xv kavwö i penõõ ku cabol
\dialx PA
\xn il ne dort pas encore
\xv kavwö nu penõõ ku hovwo
\dialx PA
\xn je n'ai pas encore mangé
\cf kavwö … ku … go
\dialx GOs
\ce ne pas encore
\dt 19/May/2024

\lx kavwoo
\dialx GOs
\ph 'kaβo:
\va kavwong
\dialx PA BO
\is fonctions_naturelles
\ps v
\ge roter
\dt 24/Feb/2025

\lx kawaang
\dialx PA BO [Corne]
\is temps_découpage
\ps ADV
\ge tôt le matin ; de bon matin
\dt 28/Jan/2019

\lx kawê
\dialx BO PA
\is insecte
\ps n
\ge sauterelle (de cocotier, grosse, verte)
\dt 29/Jan/2019

\lx kaweeng
\dialx PA BO [BM]
\is interaction
\ps v
\ge imiter ; singer
\xv i pe-kaweeng u me-va i nu
\dialx PA
\xn il imite ma façon de parler
\xv i kaweeng u me-va i nu
\dialx BO
\xn il imite ma façon de parler
\xv nu kaweeng u me-tèmèno i je
\dialx BO
\xn j'imite sa façon de marcher
\dt 27/Jan/2019

\lx kaxe
\dialx GO
\is grammaire_conjonction
\ps CNJ
\ge tandis que ; pendant
\se kage novwu
\dialx BO
\sge pendant que
\dt 13/Aug/2021

\lx kaya-kò
\dialx WEM
\is corps
\ps n
\ge arrière de la jambe
\an ala-me-kò
\at devant de la jambe
\dt 21/Aug/2021

\lx kayuna
\dialx GOs
\va kauna
\dialx PA BO
\is société
\ps n
\ge célibataire (homme ou femme)
\dt 27/Jan/2019

\lx kaza
\dialx GOs BO
\is pêche
\ps v
\ge pêcher (aller à la pêche à qqch.)
\xv çö kaze kaza ? – Nu kaze du cî
\dialx GO
\xn tu vas faire la pêche à quoi ? – Je vais à la pêche au crabe
\dt 22/Feb/2025

\lx kaze
\ph kaðe
\dialx GOs
\va kale
\dialx BO PA
\sn 1
\is eau_marée
\ps n; v
\ge marée montante (être) ; marée haute
\ge remplir (se) (mer) |dialx{BO}
\xv e wa-kaze
\dialx GO
\xn c'est la marée haute
\se me-kaze
\dialx GO
\va me-kale
\dialx PA
\sge marée montante (limite de l'eau montante)
\xv i kale
\dialx PA
\xn la marée monte
\xv i pwaa kale
\dialx PA
\xn la marée descend
\sn 2
\is pêche
\ps v
\ge pêcher (en bord de mer) ; chercher de la nourriture à la mer
\xv e a kaze
\dialx GO
\xn elle va à la pêche (à la mer à marée montante)
\xv e a kale
\dialx PA
\xn elle va à la pêche (à la mer à marée montante)
\xv i a-du kale
\dialx BO
\xn elle va à la pêche (à la mer à marée montante)
\et *ta(n)si(k)
\eg mer
\el POc
\dt 14/May/2024

\lx kaze kou
\dialx GO
\va kale kou
\dialx PA BO
\is eau_marée
\ps n
\ge marée d'équinoxe de printemps
\dt 16/Aug/2021

\lx kazi
\ph ka:ði
\dialx GOs
\va kali-
\dialx PA BO
\is parenté
\ps n
\ge cadet ; frères ou sœurs plus jeunes qu'ego
\xv kazi-nu
\dialx GO
\xn mon frère (plus jeune)
\xv kali-ny
\dialx PA
\xn mon cadet ; mes frères ou sœurs (plus jeunes)
\et *taji
\el POc
\dt 22/Feb/2025

\lx kazu kaza
\dialx GOs
\is jeu
\ps n
\ge balancelle
\dt 28/Jan/2019

\lx kazubi
\ph ka'ðumbi
\dialx GOs
\va karubi, katrubi
\ph ka'ɽubi
\dialx WEM WE
\va kharubin
\dialx PA BO
\wr A
\is grammaire_manière
\ps v
\ge faire vite
\xv kazubi-ni mõgu i çö !
\dialx GO
\xn accélère ton travail
\wr B
\is grammaire_adverbe
\ps ADV
\ge vite ; rapidement ; tout de suite
\xv ã-mi çö whili-da (è)nè kazubi
\dialx GO
\xn viens ici et amène-le vite ici
\an kha-maaçee, maaja, beloo
\at lentement, lent
\dt 31/Jan/2022

\lx ke
\dialx GOs
\va keel
\dialx PA BO
\is paniers
\ps n
\ge panier
\xv kee-nu
\dialx GO
\xn mon panier
\xv kee-ny
\dialx PA
\xn mon panier
\dt 26/Aug/2021

\lx ke-
\dialx GO PA
\va kee
\dialx PA
\is paniers
\is préfixe_sémantique
\ps CLF
\ge préfixe des paniers
\se ke-bukaka
\sge panier sacré
\se ke-haxaba
\sge panier de jonc
\se ke-hêgi
\sge panier à monnaie
\se ke-kabun
\dialx PA
\sge panier sacré
\se ke-simi
\sge poche de chemise
\dt 24/Jan/2022

\lx ke haba
\dialx BO
\is paniers
\ps n
\ge panier à tressage lâche
\dn utilisé pour filtrer le |lx{dimwa} [Corne]
\nt non vérifié
\dt 08/Jan/2022

\lx kea
\dialx GOs PA BO
\is position
\ps v
\ge incliné ; appuyé
\xv mhenõ-mwãã mãni mhenõ-kea
\xn nos soutiens et nos appuis
\dt 18/Sep/2021

\lx keala
\dialx GOs PA WEM
\va kevala
\dialx WEM WE
\is paniers
\ps n
\ge panier
\dn en palmes de cocotier, rectangulaire ou ovale, à fond plat : utilisé pour transporter de la nourriture des champs
\dt 08/Jan/2022

\lx kebwa
\dialx GOs
\is armes
\ps n
\ge sarbacane
\dt 02/Feb/2019

\lx kêbwa
\ph kẽbwa
\dialx GOs PA BO
\va kêbwa-n
\dialx BO
\is grammaire_modalité
\ps MODAL.n ; PROH
\ge interdiction ; ne pas falloir
\xv kêbwa ubò pwa !
\dialx GO
\xn ne sors pas !
\xv kêbwa hovwo !
\dialx BO
\xn ne mange pas !
\xv kêbwa-n na jo pò khõbwe mwã
\dialx BO
\xn il ne faut pas que tu dises quoi que ce soit
\cf kavwö
\ce ne … pas
\dt 03/Feb/2025

\lx ke-bwaxo
\dialx GOs
\va ke-baxo
\dialx GOs
\is paniers
\ps n
\ge panier tressé en pandanus
\dn doté d'une anse et utilisé comme sac à main
\dt 08/Jan/2022

\lx ke-cããdu
\ph ketjɛ̃:du
\dialx GOs
\is paniers
\ps n
\ge panier (rond)
\dn en palmes de cocotier
\dn plus grand que |lx{keruau}
\dt 08/Feb/2025

\lx ke-cak
\dialx BO [Corne]
\is paniers
\ps n
\ge sac
\bw sac (FR)
\ng composé: |lx{ke-} (préfixe des paniers) + sac
\dt 08/Jan/2022

\lx ke-cee
\dialx GOs PA
\va ke-çe
\dialx GO(s)
\is étapes_vie
\ps n
\ge cercueil
\dn (lit. panier en bois)
\dt 09/Feb/2025

\lx kê-chaamwa
\ph kẽcʰa:mwa
\dialx GOs
\is cultures_champ
\ps n
\ge bananeraie ; champ de bananiers
\dt 02/Jan/2022

\lx kêdi
\dialx GOs
\is action_corps
\ps v
\ge ouvrir une enveloppe
\dn par ex. de bounia
\dt 24/Feb/2025

\lx kee
\dialx GOs
\is grammaire_modalité
\ps v
\ge laisser ; permettre
\xv kee-je vwö yu ênè !
\dialx GO
\xn laisse-le rester ici !
\xv la kee-nu vwö nu pwaala
\xn ils m'ont laissé naviguer
\dt 17/Sep/2021

\lx kèè
\dialx GOs
\is arbre
\ps n
\ge palétuvier gris
\sc Avicennia marina
\scf Verbénacées
\dt 27/Aug/2021

\lx kêê
\hm 1
\dialx GOs BO PA
\is parenté
\ps n
\ge père (désignation)
\ge frère de père ; cousin du père
\ge époux de sœur de mère ; époux de cousine de mère
\xv kêê-n
\dialx PA
\xn son père
\xv kêê-n mãni kibu-n
\dialx PA
\xn les pères et grands-pères
\et *tama
\el POc
\dt 21/Feb/2025

\lx kêê
\hm 2
\dialx GOs PA
\va kê
\dialx GOs PA
\is cultures_champ
\ps n
\ge champ ; plantation
\ge emplacement
\se kê-pomõ-nu
\dialx GO
\sge mon champ
\xv kêê-n
\dialx PA
\xn son champ ; ses plantations
\dt 22/Feb/2025

\lx kêê-
\dialx GOs PA
\va kê
\dialx GO
\is préfixe_sémantique
\ps n
\ge champ ; emplacement
\se kê-chaamwa
\sge bananeraie
\se kê-kui êmwên
\sge côté du massif d'igname travaillé par les hommes
\se kê-kui thoomwã
\sge côté du massif d'igname travaillé par les femmes
\se kê-uva
\sge champ de taros
\dt 24/Feb/2025

\lx keeda
\dialx GOs BO PA
\va jeeda
\dialx BO
\is arbre
\ps n
\ge badamier
\sc Terminalia catappa L.
\scf Combrétacés
\dt 08/Oct/2021

\lx kee-hõbo
\ph 'ke:'hɔ̃mbo
\dialx GOs
\va kee-hãbwo
\is maison_objet
\ps n
\ge armoire
\dt 10/Jan/2022

\lx keejò
\dialx GOs
\is arbre
\ps n
\ge "bois de lait"
\dn les graines sont utilisées comme perles
\dt 08/Feb/2025

\lx kêê-kui
\dialx GOs PA
\is cultures_champ
\ps n
\ge champ d'ignames ; massif d'igname ; billon d'igname
\dt 08/Jan/2022

\lx kêê-khò
\ph kẽ:kʰɔ
\dialx GOs PA
\is cultures_champ
\ps n
\ge champ cultivé ; champ labouré
\dt 02/Jan/2022

\lx kêê-mu-cee
\dialx GOs
\va kê-muuc
\dialx PA
\is cultures_champ
\ps n
\ge jardin de fleurs
\dt 08/Nov/2021

\lx kêê-mwa
\dialx GOs
\is maison
\ps n
\ge terrassement de la maison ; emplacement de la maison
\dt 08/Mar/2019

\lx kêê-nu
\dialx GOs PA BO
\is cultures_champ
\ps n
\ge champ de cocotier
\dt 21/Mar/2023

\lx kee-pazalô
\dialx GOs
\is habillement
\ps n
\ge poche de pantalon
\dt 25/Jan/2018

\lx kêê-phwãã
\dialx GOs PA
\is cultures_champ
\ps n
\ge champ en jachère
\dn déjà récolté, dans lequel poussent des rejets
\dt 08/Feb/2025

\lx kêê-thre
\dialx GOs
\is cultures
\ps n
\ge coin débroussé pour cultures
\dt 21/Mar/2023

\lx kêê-yaai
\dialx PA BO
\is cultures_champ
\ps n
\ge champ après brûlis
\dt 26/Jan/2019

\lx kegoo
\dialx GOs
\is maison_objet
\ps n
\ge berceau
\dn sert aussi à bercer un bébé
\dt 08/Feb/2025

\lx kehin
\dialx BO
\is paniers
\ps n
\ge panier (à tressage serré)
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx kèlèrè
\dialx GOs
\is igname
\ps n
\ge igname (violette)
\dt 29/Jan/2019

\lx kêmi
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge enterrer qqch. ; mettre en terre
\xv lha kêmi jaa
\dialx GO
\xn ils ont enterré les ordures
\sn 2
\is préparation_aliments
\ps v
\ge mettre (à cuire) dans la cendre
\xv lha kêmi kîbi
\dialx GO
\xn ils ont enterré le four
\xv lha kêmi chaamwa na ni dra
\dialx GO
\xn ils ont mis les bananes à cuire sous la cendre
\et *tanum
\eg enterrer, mettre en terre
\el POc
\dt 21/Mar/2023

\lx ke-mõã
\dialx GOs WEM PA
\is paniers
\ps n
\ge panier de restes (de nourriture)
\xv ke-mõã-nu
\xn mon panier de restes (de nourriture)
\dt 04/Apr/2023

\lx kemõ-hi
\dialx GOs
\is corps
\ps n
\ge poing
\xv kemõ-hii-nu
\xn mon poing
\dt 19/Dec/2021

\lx kê-muuc
\dialx PA
\is cultures_champ
\ps n
\ge massif de fleurs
\dt 26/Jan/2019

\lx kêni
\ph kẽɳi, kîɳi
\dialx GOs WEM PA BO
\va kîni
\is corps
\ps n
\ge oreille
\xv kênii-je
\dialx GO
\xn son oreille
\xv kênii-n
\dialx PA
\xn (son) oreille
\se coo-kênii-n
\dialx PA
\sge le lobe de son oreille
\se dròò-kêni
\dialx GO
\sge lobe de l'oreille
\se dòò-kênii-n
\dialx BO
\sge pavillon de l'oreille
\se nhõ-kênii-n
\dialx PA
\sge cire de ses oreilles
\se phwe-kênii-n
\dialx PA
\sge pavillon de ses oreilles
\et *taliŋga
\el POc
\dt 09/Jul/2022

\lx kênii-doo
\dialx PA
\is ustensile
\ps n
\ge poignées de la marmite
\dt 26/Aug/2021

\lx kênim
\dialx PA
\is coutumes
\ps n
\ge prestation de deuil aux maternels
\xv na ni kênime-n
\xn lors de leur prestation de deuil
\dt 15/Feb/2019

\lx kênõ
\ph kẽɳɔ̃
\dialx GOs
\va kênõng
\ph kẽnɔ̃ŋ
\dialx BO PA
\sn 1
\is action
\ps v
\ge tourner ; retourner ; retourner (se) ; poser à l'envers
\xv e a-kênõ ni dau
\dialx GO
\xn il fait le tour de l'île
\xv e kênõge vea pwa-du bwabu
\dialx GO
\xn elle a mis le verre à l'envers
\xv i kênõge dau
\dialx PA
\xn il fait le tour de l'île
\xv e ku kênõ
\dialx GO
\xn il se retourne (debout)
\xv e tree kênõ
\dialx GO
\xn il se retourne (assis)
\xv la ku kênõ
\dialx GO
\xn ils sont debout en cercle
\xv la tree kênõ
\dialx GO
\xn ils sont assis en cercle
\xv i kênõng na kaya mwa
\dialx PA
\xn il tourne derrière la maison
\sn 2
\is mouvement
\ps v ; n
\ge faire des moulinets du bras ; moulinet du bras
\sn 3
\is caractéristiques_personnes
\ps v
\ge saoul (être)
\dn (lit. la tête tourne)
\sn 4
\is eau
\ps n
\ge tourbillon d'eau
\se we kênõ
\dialx GO
\sge tourbillon d'eau
\dt 24/Feb/2025

\lx ke-paa
\dialx GOs PA BO
\is paniers
\is armes
\ps n
\ge giberne de fronde
\dt 26/Aug/2021

\lx ke-palu
\dialx GOs
\is paniers
\ps n
\ge panier à richesses ; tirelire
\dt 24/Feb/2025

\lx keraa
\dialx GOs
\va keraò
\dialx PA
\sn 1
\is paniers
\ps n
\ge panier qui contient les ignames |lx{dimwa} grattées
\sn 2
\is pêche
\ps n
\ge épuisette à crevettes |dialx{GO}
\dt 24/Feb/2025

\lx kerao
\dialx GOs BO
\is instrument
\ps n
\ge instrument en paille pour gratter les bulbilles de |lx{dimwã} dans l'eau (Dubois)
\dt 08/Jan/2022

\lx kerewala
\dialx BO
\va kerewòla
\dialx BO
\is paniers
\ps n
\ge panier
\dn tressé en palmes de cocotier
\bw ketewola (POLYN) (PPN *ketepola)
\dt 08/Feb/2025

\lx keruau
\ph keɽuau
\dialx GOs
\va ke-truau
\ph keʈuau
\dialx GO(s)
\is paniers
\ps n
\ge panier
\dn tressé en palmes de cocotier
\dt 24/Feb/2025

\lx ke-tilo
\dialx BO
\is coutumes
\ps n
\ge ossature de la monnaie
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ke-thal
\dialx PA BO
\is paniers
\ps n
\ge panier en pandanus
\dt 27/Jan/2019

\lx ke-thi
\dialx GOs
\is paniers
\ps n
\ge porte-monnaie
\dn son nom vient du fait qu'il claque quand on le ferme
\dt 09/Feb/2025

\lx ke-tru
\dialx GOs
\va kèru
\dialx BO
\is paniers
\ps n
\ge panier à monnaie ; enveloppe de monnaie coutumière
\dt 27/Jan/2019

\lx ke-thrõõbo
\dialx GOs
\is paniers
\ps n
\ge panier de charges ; panier porté sur le dos
\dn panier porté avec une bandoulière ou comme un sac à dos
\cf thrõõbo
\ce porter sur le dos
\dt 24/Feb/2025

\lx kevaho
\dialx BO
\is paniers
\ps n
\ge panier à anses
\nt selon Corne ; non vérifié
\dt 24/Feb/2025

\lx kevalu
\dialx GOs BO
\sn 1
\is pêche
\ps n
\ge nasse (à anguilles ou poisson)
\dn filet (identique au |lx{deaang}, mais plus grand
\nt selon Dubois, Corne
\sn 2
\is maison
\ps n
\ge corbeille (sur le toit de la maison)
\dt 08/Feb/2025

\lx kevi
\dialx PA
\is préparation_aliments
\ps v
\ge évider avec une cuillère
\dn noix de coco, papaye, avocat
\dt 08/Feb/2025

\lx kevwa
\ph 'keβa
\dialx GOs
\va kepwa
\dialx arch.
\va kewang
\dialx BO PA
\sn 1
\is topographie
\ps n
\ge vallée ; creux (terrain)
\ge ravin ; talweg
\se ku-kevwa
\dialx GO
\sge le haut de la vallée
\sn 2
\is corps
\ps n
\ge espace entre les côtes |dialx{PA}
\dt 23/Jan/2022

\lx kêxê
\dialx GOs BO
\va kèèngè
\dialx BO [BM]
\is oiseau
\ps n
\ge perroquet ; loriquet calédonien
\sc Trichoglossus hæmatodus
\scf Psittacidés
\gb Rainbow Lorikeet
\dt 27/Aug/2021

\lx kèxi
\dialx BO
\is grammaire_conjonction
\ps CNJ
\ge et ensuite ; et alors
\nt selon Corne
\dt 26/Mar/2022

\lx ki
\hm 1
\dialx GOs WEM
\va kim
\dialx PA BO
\is plantes_processus
\ps v
\ge pousser ; grandir (plantes)
\ge croître ; germer
\xv e u-daa mwã ni nõ dradro, nhyã ki na êbòli kòli kaze
\dialx GOs
\xn il se baisse pour entrer dans un buisson de Vitex trifolia, celui qui pousse là-bas en bas au bord de mer
\xv la kim ada
\dialx BO
\xn ils poussent tout seuls
\dt 23/Apr/2024

\lx ki
\hm 2
\dialx GOs
\va kil
\dialx PA
\is santé
\ps v ; n
\ge douleur ; faire mal
\dt 19/Oct/2021

\lx kî
\hm 1
\dialx GOs PA BO
\is préparation_aliments
\ps v
\ge griller ; brûler
\xv e kî nõ
\xn il grille du poisson
\xv nu kîni
\xn je l'ai grillé
\ng v.t. |lx{kîni}
\dt 21/Mar/2023

\lx kî
\hm 2
\dialx GOs
\is mollusque
\ps n
\ge huître de palétuvier
\sc Saccostrea cucullata
\scf Ostréidés
\sc Atrina vexillum
\scf Ostréidés
\dt 24/Feb/2025

\lx kia
\dialx PA BO [BM]
\va khia
\dialx BO [BM]
\is médecine
\is religion
\ps v ; n
\ge appliquer (médicament) ; traiter
\ge médicament ; remède (coutumier)
\dt 15/Aug/2021

\lx kia po
\dialx BO
\is grammaire_négation_existentiel
\ps v
\ge il n'y a rien
\dn (lit. il n'y a pas chose)
\xv kia poo-le
\xn il n'y a rien là
\et *tika
\el POc
\dt 09/Feb/2025

\lx kia we
\dialx GOs PA BO
\is pêche
\ps v
\ge pêcher au poison
\dn (lit. empoisonner l'eau)
\et *tupa
\el POc
\dt 08/Feb/2025

\lx kiba
\dialx GOs PA
\is sentiments
\ps v
\ge refuser ; rejeter
\dt 21/Mar/2023

\lx kibao
\ph kimbao
\dialx GOs PA BO
\va khibao
\dialx GOs
\sn 1
\is action
\ps v
\ge toucher (une cible)
\dn avec un projectile et éventuellement tuer
\xv çö kibao ? – Hai, nu pa-tha !
\xn tu l'as touché ? – Non, je l'ai raté
\xv e kibao mèni
\xn il a touché l'oiseau
\cf cha
\ce toucher (cible)
\sn 2
\is coutumes
\ps v
\ge recevoir un geste (don) coutumier
\dn et remercier par un geste/don en retour
\dt 24/Feb/2025

\lx kîbi
\dialx GOs PA BO
\is préparation_aliments
\ps n
\ge four enterré
\xv i tu kîbi mhwããnu
\dialx BO
\xn la lune a un halo (lit. fait le four) (Dubois)
\se na-du nõ kîbi
\sge faire cuire au four enterré
\se cale kîbi
\dialx BO
\sge allumer le four
\se thu kîbi
\sge faire le four
\se paxa-kîbi
\sge pierres du four
\et *qumun
\el POc
\ea Blust
\dt 26/Aug/2021

\lx kibii
\dialx GOs PA BO
\va khibii
\dialx BO
\is action_corps
\ps v
\ge casser en morceaux ; piler (verre, assiette, miroir)
\xv bî ru pa-kibii bwa-m
\dialx PA
\xn nous allons te fracasser la tête
\se kibii mwani
\dialx PA
\sge faire de la monnaie (cf. "casser un billet")
\ng causatif: |lx{pa-kibii}
\dt 09/Mar/2023

\lx kîbö
\dialx GOs PA BO
\va keebo
\dialx BO
\is arbre
\ps n
\ge palétuvier rouge
\dn à racines aériennes
\sc Bruguiera gymnorhiza
\scf Rhizophoracées
\se kîbö mii
\sge palétuvier rouge
\dt 08/Feb/2025

\lx kîbö
\dialx GOs
\is action_corps
\ps v
\ge montrer ses fesses
\dt 21/Mar/2023

\lx kîbo, kîbwo
\dialx GOs PA BO
\va kîb(w)ò, kîbwòn
\dialx PA
\is plantes_partie
\ps n
\ge bourgeons ; rejet (de plante) ; germe
\ge jeunes feuilles (encore enroulées)
\dn qui sortent du cœur de la plante
\se kîb(w)oo cee
\dialx GO
\sge bourgeons d'arbre
\se kîb(w)oo nu
\dialx GO
\sge germe de coco
\se kîb(w)ò phuleng
\dialx PA
\sge rejet de Pipturus
\ng allongement pour la détermination de partie à tout
\dt 17/Feb/2025

\lx kibu
\dialx GOs PA BO
\is parenté
\ps n
\ge grand-père ; ancêtre
\xv kibu-ã
\dialx GO
\xn nos ancêtres
\et *tumpu
\el POc
\dt 02/Feb/2019

\lx kibwa
\dialx BO
\is action_corps
\ps v
\ge pousser ; précipiter qqch. dans
\ng v.t. |lx{kibwale}
\nt non vérifié
\dt 29/Mar/2022

\lx kîbwoo nu
\ph kîbwo: ɳu
\dialx GOs
\is arbre_cocotier
\ps n
\ge germe du coco
\dt 17/Aug/2021

\lx kica
\dialx BO
\is maison
\ps n
\ge paroi ; mur
\xv kica mwa
\xn les murs de la maison
\nt non vérifié
\dt 24/Feb/2025

\lx kîga
\dialx GOs WEM PA BO
\is fonctions_naturelles
\ps v ; n
\ge rire ; sourire
\xv yu kîgae da ?
\dialx BO
\xn de quoi ris-tu ?
\xv i pe-kîga hãda
\dialx BO
\xn il rit tout seul
\se pha-kîga
\sge faire rire
\dt 21/Mar/2023

\lx kii
\hm 1
\dialx GOs PA BO
\va kivi
\dialx BO
\is habillement
\ps n
\ge jupe ; jupon ; manou (femme)
\se kii thoomwã
\sge jupe
\xv kii-je
\dialx GO
\xn son manou
\xv kii-n
\dialx PA BO
\xn son manou ; sa jupe
\xv kia kii-n
\dialx BO
\xn il est nu
\xv kivi-n
\dialx BO
\xn sa jupe
\et *tipi
\eg loincloth
\el POc
\ea Blust
\dt 22/Feb/2025

\lx kii
\hm 2
\dialx GOs PA
\is action_plantes
\ps v
\ge tailler (un arbre)
\dn en cassant les branches à la main et vers le bas
\xv e kii mûû-cee
\xn il casse une tige de fleur
\dt 08/Feb/2025

\lx kîî
\dialx GOs
\is son
\ps v
\ge bruit aigu 
\dn qui vrille les oreilles
\xv e kîî kii-nu
\xn ça me vrille les oreilles
\dt 08/Feb/2025

\lx kiiça
\ph ki:dʒa, ki:ʒa
\dialx GOs
\va kiia
\dialx PA BO
\va khia
\dialx BO
\is sentiments
\ps v ; n
\ge jaloux (être) ; jalouser ; jalousie
\xv e kiiça i je
\dialx GO
\xn il est jaloux de lui
\xv e kiiça ui je
\dialx GO
\xn il est jaloux de/envers lui
\xv e kiiça ui ãbaa-je xo ã êgu-ã
\dialx GO
\xn cet homme est jaloux de son frère
\xv e kiiça ui Pwayili pexa dilii-je ?
\dialx GO
\xn est-il jaloux de Pwayili à cause de ses terres ?
\xv e kiiça pexa dilii-je ?
\dialx GO
\xn il est jaloux de ses terres ?
\xv ôô ! e kiiça !
\dialx GO
\xn oui, il en est jaloux !
\dt 20/Feb/2025

\lx kîîgaze
\dialx GOs
\is plantes
\ps n
\ge liane de bord de mer
\dn utilisée pour la pêche au poison
\sc Derris trifoliata
\scf Fabacées, Papilionacées
\dt 24/Feb/2025

\lx kiil
\dialx BO
\is paniers
\ps n
\ge panier
\se ki-wèdal
\sge panier à frondes
\se ki-rau
\sge panier en osier
\nt selon Corne ; non vérifié
\dt 24/Feb/2025

\lx kiixi
\ph ki:ɣi
\dialx GOs
\is son
\ps v
\ge crier
\dt 21/Mar/2023

\lx ki-kò
\dialx GOs
\va khi-kò
\is feu
\ps v
\ge allumer un feu de brousse ; brûler
\dn pour préparer un champ
\dt 08/Feb/2025

\lx ki-kui
\dialx GOs PA BO
\va khi-kui
\dialx GOs
\sn 1
\is préparation_aliments
\ps v
\ge griller/cuire au feu les prémices des ignames
\se ku kini
\sge igname grillée
\cf kini
\ce griller
\sn 2
\is coutumes
\ps n
\ge fête des prémices des ignames
\dt 22/Feb/2025

\lx ki-kha
\dialx GOs
\va khi-ga
\dialx GOs
\va ki khan
\dialx PA
\is cultures
\is feu
\ps v
\ge brûler (les champs) ; pratiquer le brûlis
\cf kini
\ce brûler ; griller
\dt 22/Feb/2025

\lx kilè
\dialx GOs
\is instrument
\ps n
\ge clé
\bw clé (FR)
\dt 27/Jan/2019

\lx kilee-ni
\ph kile:ɳi
\dialx GOs
\va kili
\dialx PA
\is outils
\ps v.t.
\ge fermer à clé
\xv kile-ni phwè-mwa !
\xn ferme la porte à clé !
\bw clé
\dt 08/Jan/2022

\lx kili
\dialx GOs BO PA
\is maison
\ps n
\ge alène
\dn alène de bois percée au bout et utilisée pour attacher la paille sur les toits avec des lianes
\dt 08/Oct/2021

\lx kilooc
\dialx PA
\is instrument
\ps n
\ge cloche
\bw cloche (FR)
\dt 27/Jan/2019

\lx kiluu
\dialx GOs BO
\va ciluu
\dialx PA
\sn 1
\is action_corps
\ps v
\ge courber (se)
\ge baisser la tête
\sn 2
\is religion
\ps v
\ge prosterner (se)
\se ku-kiluu
\sge se prosterner debout
\se tre-kiluu
\dialx GOs
\sge se prosterner assis
\cf bwagiloo
\ce se courber ; se prosterner
\dt 22/Feb/2025

\lx kimwado
\dialx GOs WEM WE BO PA
\is insecte
\ps n
\ge sauterelle (petite et verte)
\dt 29/Jan/2019

\lx kine
\hm 1
\ph kiɳe
\dialx GOs PA BO
\va kin
\dialx BO
\is action
\ps v
\ge ajouter ; allonger ; assembler
\se pe-kine-ni
\sge mettre bout à bout
\ng v.t. |lx{kine-ni}
\gt assembler qqch.
\dt 21/Feb/2025

\lx kine
\hm 2
\ph kiɳe
\dialx GOs PA
\va khine
\dialx GO
\is chasse
\ps v
\ge viser ; pointer
\xv e kine ã meni, jo e pa-tha
\dialx GO
\xn il a visé cet oiseau, et il l'a raté
\xv kine-zoo-ni drube !
\dialx GO
\xn vise bien le cerf !
\dt 21/Mar/2023

\lx kini
\ph kiɳi kîɳi
\dialx GOs
\va khînî
\dialx PA BO
\sn 1
\is feu
\ps v.t.
\ge brûler (brousse) ; incendier
\sn 2
\is préparation_aliments
\ps v.t.
\ge cuire/griller sur le feu
\dn sur les braises ou dans les cendres
\xv çö ki ko !
\xn fais griller le poulet !
\xv i khî kui
\dialx PA
\xn elle fait griller l'igname
\se ku kini
\sge igname grillée
\et *tunu
\el POc
\dt 08/Feb/2025

\lx kini
\dialx GOs PA BO
\is préparation_aliments
\ps v
\ge griller ; rôtir
\dt 21/Mar/2023

\lx kinõ
\dialx PA BO
\va khinõ
\dialx WE
\is fonctions_naturelles
\ps v ; n
\ge transpirer ; transpiration ; sueur
\xv hai kinõ
\dialx PA
\xn transpirer
\xv tho kinõ
\dialx PA
\xn transpirer (lit. la chaleur coule)
\ng forme déterminée: |lx{kinõ-n}
\gt sa sueur
\et *tunu
\eg chaud
\el POc
\dt 05/Jan/2022

\lx kînu
\dialx GOs BO
\is navigation
\ps n
\ge ancre
\xv lha pao kînu ênedu ni kòli kaze
\dialx GO
\xn ils jettent l'ancre en bas dans la mer
\dt 02/Nov/2021

\lx kiò
\dialx GOs PA BO
\is corps
\ps n
\ge ventre
\xv kiò-je
\dialx GO
\xn son ventre
\xv kiò-n
\dialx PA BO
\xn son ventre
\et *tia(n)
\el POc
\dt 30/Jan/2019

\lx kiri
\dialx PA BO
\is action_corps
\ps v
\ge ratisser
\ge gratter (la terre)
\dn comme les poules
\xv i kiri ja
\xn elle ratisse les saletés
\xv la kiri dili
\xn elles ratissent la terre
\dt 08/Feb/2025

\lx kiriket
\dialx GOs
\is jeu
\ps n
\ge cricket
\bw cricket (GB)
\dt 15/Aug/2021

\lx kitrabwi
\dialx GOs
\is interaction
\ps v
\ge accueillir ; recevoir
\dt 27/Jan/2019

\lx kivwa
\ph kiβa
\dialx GOs
\va kipa
\dialx GO
\va kivha
\dialx PA BO
\is instrument
\ps n
\ge fermeture ; couvercle
\se kivwa burei
\dialx GOs
\sge bouchon de bouteille
\se kivwa phwè-mwa
\dialx GOs
\sge porte de maison
\se kivha doo
\dialx PA
\sge couvercle de marmite
\xv kivha-n
\dialx BO
\xn son couvercle
\dt 06/Jan/2022

\lx kivwi
\ph kiβi
\dialx GOs
\va kivhi
\dialx PA BO
\sn 1
\is action
\ps v
\ge fermer ; boucher ; couvrir (boîte, marmite)
\xv khivwi pwaa-çö !
\dialx GO
\xn ferme ta bouche ! tais-toi !
\xv kivhi phwaa-m !
\dialx PA
\xn tais-toi ! (momentanément)
\xv kivhi doo !
\dialx PA
\xn couvre la marmite !
\cf thôni
\ce fermer à clé
\an thala
\at ouvrir
\sn 2
\is interaction
\ps v
\ge empêcher de (parler)
\xv e kivwi-nu vwo kêbwa ne nu vhaa !
\dialx GO
\xn il m'a empêché de parler !
\dt 08/Jan/2022

\lx kixa
\dialx GOs PA
\va kiga
\dialx GO(s)
\va kia
\dialx PA BO
\va kiaxa, cixa
\dialx PA
\is grammaire_négation_existentiel
\ps PRED.NEG
\ge il n'y a pas ; rien ; sans
\xv kixa na nu cèni
\dialx GO
\xn je n'ai rien à manger
\xv kixa xa la hõboli-je
\xn elle n'a pas de vêtements
\xv yö ru kixa mwa gèè i yu
\dialx PA
\xn tu n'auras plus de grand-mère
\xv cixa hovwo
\dialx PA
\xn rien à manger
\xv kia pu-n
\dialx BO
\xn sans raison (Dubois)
\xv kia puu-n
\dialx PA
\xn faire inutilement/sans raison
\xv kia poo le
\dialx BO
\xn il n'y a rien (BM)
\se kia gunan
\sge calme plat ; vide
\se kia kii-n
\dialx PA
\sge nudité sexuelle (lit. sans manou)
\se kia pòi-n
\dialx PA
\sge stérilité (femme) (lit. sans enfant)
\se kia yhala-n
\dialx PA
\sge sans nom
\dt 22/Feb/2025

\lx kixa ai
\dialx GOs
\va kiya ai
\dialx PA
\sn 1
\is caractéristiques_animaux
\ps v
\ge pas dressé
\dn (lit. qui n'a pas de cœur)
\xv kixa ai-je
\dialx GOs
\xn pas dressé
\xv kiya ai-n
\dialx PA
\xn il n'est pas dressé
\an thu ai-n
\at dressé
\sn 2
\is caractéristiques_personnes
\ps v
\ge qui n'a pas l'âge de raison
\xv kixa ai-je
\dialx GOs
\xn il n'a pas l'âge de raison (enfant) (lit. qui n'a pas de cœur)
\dt 09/Feb/2025

\lx kixa hê
\dialx GOs
\is caractéristiques_objets
\ps LOCUT
\ge vide
\dn (lit. pas de contenu)
\xv kixa hê kee-nu
\xn mon panier est vide
\xv kixa hê dröö
\xn la marmite est vide
\an pu hê
\at qui a un contenu
\dt 08/Feb/2025

\lx kixa khôme
\dialx GOs
\is grammaire_aspect
\is grammaire_modalité
\ps RESTR ; ASP
\ge ne faire que ; n'avoir de cesse que
\ge n'avoir jamais assez de
\xv kixa khôme nye hovwo jena !
\dialx GO
\xn il ne fait que manger ! il est toujours en train de manger celui-là (il n'est jamais rassasié)
\cf khôme
\ce rassasié ; comblé
\dt 22/Feb/2025

\lx kixa mwã
\dialx GOs
\is grammaire_négation_existentiel
\ps v
\ge il n'y a plus
\dt 10/Dec/2021

\lx kixa na
\ph kiɣa ɳa
\dialx GOs
\va kiaxa ne
\dialx PA
\is grammaire_négation_existentiel
\ps NEG
\ge personne (il n'y a) ; rien
\dn (lit. il n'y a pas de x que …)
\xv kixa na nu huu
\dialx GO
\xn je n'ai rien mangé
\xv kixa na nu noo-je
\dialx GO
\xn je n'ai vu personne
\xv kixa na la yuu
\dialx GO
\xn personne parmi eux ne reste
\xv kixa na iyô na tre-yuu
\dialx GO
\xn personne parmi nous ne reste
\xv kawa li tõne-xa ne e caxo
\dialx PA
\xn ils n'entendent personne qui répond
\xv kixa ne i a
\dialx PA
\xn personne ne part
\dt 09/Feb/2025

\lx kixa zòò
\dialx GOs
\is caractéristiques_objets
\ps v
\ge facile
\an pu zòò
\dialx GOs
\at difficile
\dt 10/Jan/2022

\lx ko
\dialx GOs BO PA
\hm 1
\sn 1
\is oiseau
\ps n
\ge poule ; coq ; volaille
\xv ko èmwèn
\dialx BO
\xn coq
\sn 2
\is oiseau
\ps n
\ge cagou
\dt 30/Dec/2021

\lx ko
\hm 2
\dialx GOs
\va xo, vwo
\dialx GO(s)
\va ka
\dialx GO(s)
\is grammaire_conjonction
\ps COORD
\ge et
\dt 03/Dec/2021

\lx ko
\hm 3
\dialx GO PA BO
\va (x)o, (vw)o, (k)u, (x)u
\dialx GO PA BO
\va (h)u
\dialx BO
\is grammaire_agent
\ps AGT ; sujet
\ge marque de sujet ou d'agent
\dt 22/Feb/2025

\lx ko
\hm 4
\dialx GOs
\va kavwö
\is grammaire_négation
\ps NEG
\ge ne … pas
\xv e wã mwã xo Kaawo : "ko (= kavwa, kavwö) çö nõõli pòi-nu ?"
\dialx GO
\xn Kaawo fait/dit : "tu n'as pas vu mon enfant ?"
\xv Hai ! ko  li zoma ubò mõnõ
\dialx GO
\xn Non ! ils ne vont pas sortir demain
\ng forme courte de la négation |lx{kavwö, kavwa}
\dt 05/Jan/2022

\lx kò
\hm 1
\ph kɔ
\dialx GOs BO
\is arbre
\ps n
\ge palétuvier
\dn à fruits comestibles
\sc Bruguiera sp.
\scf Rhizophoracées
\et *toŋoR
\el POc
\dt 08/Feb/2025

\lx kò
\hm 2
\ph kɔ
\dialx GOs PA BO
\is végétation
\ps n
\ge forêt ; brousse :
\ge cimetière (dans la forêt)
\xv nõ kò
\dialx GO
\xn brousse ; forêt
\et *quta(n)
\el POc
\dt 22/Feb/2025

\lx kò
\hm 3
\ph kɔ
\dialx GOs BO PA
\is corps
\ps n
\ge pied ; jambe
\xv kòò-nu
\dialx GO
\xn mon pied
\xv kòò-n
\dialx PA
\xn pied ; son pied
\xv kòò-m !
\dialx PA
\xn debout !
\se kò-uva
\sge extrémité inférieure du taro
\se kò-pazalô
\sge jambe du pantalon
\dt 22/Feb/2025

\lx -kò
\ph kɔ
\dialx GOs
\va -gò
\dialx GOs
\va -kòn
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge trois
\et *tolu
\el POc
\dt 17/Aug/2021

\lx kô-
\hm 1
\dialx GOs PA BO
\is position
\is préfixe_sémantique_position
\ps PREF (position couchée)
\ge couché
\se kô-xea
\dialx GOs PA
\sge s'allonger ; se reposer ; faire un petit somme
\xv e kô-zugi kòò-je
\dialx GO
\xn il est couché les jambes repliées
\xv nu kô-nõõli tèèn
\dialx PA
\xn je me suis réveillé de bonne heure
\et *qenop
\el POc
\dt 22/Feb/2025

\lx kò bwa me
\dialx GOs
\va a ni dõni êgu
\dialx PA
\is interaction
\ps v
\ge faire remarquer (se) ; se mettre en évidence
\xv e kò bwa me-ã
\dialx GO
\xn il se met en évidence (lit. il se tient debout devant nos yeux)
\xv i a-da ni dõni êgu
\dialx PA
\xn il se fait remarquer (lit. il passe au milieu des gens)
\dt 25/Aug/2021

\lx kôa
\dialx GOs PA BO
\va kôya, koeza
\dialx GO(s)
\is corps_doigt
\ps n
\ge majeur (doigt)
\dt 24/Jan/2019

\lx ko-a dròò-nu
\dialx GOs PA
\is arbre_cocotier
\ps n
\ge nervure centrale de la palme de cocotier
\dt 29/Jan/2019

\lx kô-alaxe
\ph kõ'alaɣe
\dialx GOs
\is position
\ps v
\ge dormir sur le côté ; couché sur le côté
\xv e kô-alaxe
\dialx GO
\xn il est couché sur le côté
\dt 23/Jan/2022

\lx kô-baaxò
\dialx GOs
\va kô-baaxòl
\dialx PA
\is position
\ps v
\ge couché en long
\dt 08/Jan/2022

\lx kô-bazòò
\dialx GOs BO
\is position
\ps v
\ge en travers ; couché en travers
\dn de l'entrée, d'un lit, etc.
\xv e kô-bazòò loto bwa dè
\xn la voiture est de travers sur la route
\xv ne pu kô-bazòò
\xn mets-le en travers
\dt 08/Feb/2025

\lx kòbwakò
\dialx GOs
\is parenté
\ps n
\ge cadet
\xv e kòbwakòò-je
\xn il est son cadet (lit. debout sur le pied)
\dt 26/Mar/2022

\lx kô-chòvwa
\dialx GOs
\is cordes
\ps n
\ge lanière du cheval
\dt 27/Jan/2019

\lx koe
\hm 1
\dialx GOs
\is plantes
\ps n
\ge pois d'angole ; ambrevade
\sc Cajanus indicus
\scf Fabacées, Légumineuses
\dt 22/Feb/2025

\lx koe
\hm 2
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge inventer (chant, histoire) ; créer
\xv wa xa e draa koe
\xn chanson qu'il a inventée lui-même
\dt 15/Feb/2019

\lx koè
\dialx BO
\is sentiments
\ps n ; v
\ge venger (se) ; vengeance
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx köe
\dialx GOs
\va khoe
\dialx BO [BM]
\sn 1
\is action_plantes
\ps v
\ge cercler (arbre) ; tailler (haie)
\sn 2
\is interaction_animaux
\ps v
\ge castrer (animal)
\dt 23/Jan/2022

\lx kôê
\dialx GOs BO PA
\is position
\ps v
\ge couché en tenant qqch. dans les bras
\xv e kôê ẽnõ
\dialx GO
\xn il est allongé avec l'enfant dans ses bras
\xv i kôê pòi-n
\dialx PA
\xn elle est allongée avec son enfant dans les bras
\dt 21/Mar/2023

\lx koèn
\dialx PA BO
\va khoeo
\dialx BO
\is action
\ps v
\ge disparaître ; perdre ; perdu ; absent
\xv koèn xa ãbaa wony
\dialx PA
\xn certains bateaux ont disparu
\xv koèn xa ãbaa êgu
\dialx PA
\xn certaines personnes sont absentes
\xv koèn ala-kòò-ny
\dialx PA
\xn mes souliers sont perdus
\xv i pha-koène ala-kòò-ny
\dialx PA
\xn il a perdu mes souliers
\xv i koèn a nòòla-ny
\dialx BO
\xn j'ai perdu mon argent
\xv u koèn
\dialx BO
\xn c'est perdu
\cf kòyò
\dialx GOs
\ce perdre
\cf kòi
\ce absent
\dt 10/Jan/2022

\lx kòe-piça-ni
\ph kɔe-pidʒa-ɳi
\dialx GOs
\is action_corps
\ps v
\ge tenir fermement qqch.
\dt 18/Feb/2023

\lx kôgò
\dialx GOs
\is mouvement
\ps v
\ge émerger (poisson)
\dt 25/Jan/2018

\lx kôgòò
\dialx GOs
\va kôgòò-n
\dialx BO PA
\va kugo
\dialx BO PA
\is grammaire_quantificateur_mesure
\ps n
\ge reste (le) ; restant ; surplus
\se kôgòò hovwo
\dialx GO
\sge restes de nourriture
\se kôgòò mwani
\dialx PA
\sge la monnaie (reste d'argent)
\se kôgòò-n
\dialx PA
\sge ce qui lui reste (lit. son reste)
\xv gaa mwêêno kôgòò-n (a) hovo
\dialx BO
\xn il reste encore de la nourriture à manger
\dt 16/Feb/2025

\lx kô-gòòn-a
\dialx GOs
\is fonctions_naturelles
\ps v
\ge sieste (faire la) ; faire la grasse matinée
\xv lha kô-gòòn-a
\dialx GO
\xn ils font la sieste (ou) la grasse matinée (lit. allongé-midi)
\dt 28/May/2024

\lx kòi
\hm 1
\dialx GOs BO
\va kòe, koèn koi
\dialx PA
\is grammaire_négation
\ps PRED.NEG (humain)
\ge absent ; ne pas/plus être là ; manquer ; sans
\xv bala kòi-la
\dialx GO
\xn ils ont disparu (à tout jamais)
\xv kòi-je gò !
\dialx GO
\xn il n'est toujours pas là!
\xv kòi-nu
\dialx GO
\xn je suis absent (mon absence)
\xv ge le ãbaa wõ ma la kòi-ò
\dialx GO
\xn certains bateaux ont disparu
\xv ge le la ãbaa ma la kòi-la
\dialx GO
\xn certains d'entre eux ont disparu
\xv gaa kòe ri ?
\dialx BO
\xn qui manque encore ?
\xv la gaa kòi-la ?
\dialx BO
\xn qui manque encore ?
\xv u kòi-yo kõnõbòn
\dialx BO
\xn tu avais disparu hier
\xv kòi-je
\dialx GO PA
\xn il est absent ; il est mort
\xv koi-n
\dialx PA
\xn il est absent
\xv u kwèi-je
\dialx BO
\xn il est absent
\cf kòyò
\ce disparaître; perdu (non humain)
\dt 22/Feb/2025

\lx kòi
\hm 2
\dialx GOs PA BO
\is corps
\ps n
\ge foie
\xv kòi dube
\dialx GO
\xn le foie du cerf
\xv kòi-nu
\dialx GO
\xn son foie
\xv kòi-n
\dialx PA BO
\xn son foie
\et *qate
\el POc
\dt 30/Jan/2019

\lx kô-i
\dialx GOs
\is fonctions_naturelles
\is mouvement
\ps v
\ge agiter (s') en dormant
\xv a-kô-i ẽnõ-ã
\xn cet enfant s'agite en dormant
\xv a-kô-i òri ẽnõ-ã
\xn cet enfant s'agite beaucoup en dormant
\dt 21/Mar/2023

\lx kô-kabòn
\dialx PA
\is corps
\ps n
\ge vulve
\dt 24/Jan/2019

\lx kò-kai
\dialx GOs
\va kò-xai
\dialx GOs
\va kòò-kain
\dialx PA
\is parenté
\ps n
\ge enfants suivant l'aîné
\dn (lit. debout derrière)
\xv ẽnõ kò-xai
\dialx GO
\xn le deuxième enfant (ou) les enfants suivant l'aîné
\xv ẽnõ kòò-kai-n
\dialx PA
\xn le ou les enfants suivant l'aîné
\cf kòòl kai-n
\dialx PA
\ce debout derrière lui
\dt 24/Feb/2025

\lx kô-kea
\dialx GOs
\va kô-kea, kô-xea
\dialx PA BO
\sn 1
\is position
\ps v
\ge incliné ; allonger un peu (s')
\sn 2
\is fonctions_naturelles
\ps v
\ge faire un petit somme ; faire la sieste
\cf kea
\ce incliné
\dt 22/Feb/2025

\lx kô-khiai
\dialx GOs
\va kô-xiai
\dialx GO(s)
\is feu
\ps v
\ge réchauffer (se) couché auprès du feu (la nuit)
\dt 23/Aug/2021

\lx kòlaao
\dialx GOs PA BO
\va kòlao; kòlaho
\dialx BO
\sn 1
\is mollusque
\ps n
\ge conque (gastéropode)
\sc Charonia tritonis
\scf Cymatidés
\sn 2
\is maison
\ps n
\ge conque (de la flèche faîtière)
\dt 15/Sep/2021

\lx kòladuu
\dialx GOs
\va kòladuu-n
\dialx BO
\is santé
\ps v.stat. ; n
\ge maigre ; maigrir
\ge dépérir
\xv e kòladuu phãgoo-je
\xn elle est maigre
\xv e khawali-je xo kòlaadu-je
\dialx GO
\xn il/elle est grand et maigre
\an waa
\at gros, corpulent
\dt 26/Mar/2023

\lx kole
\hm 1
\dialx GOs
\va kule
\dialx PA BO
\is mouvement
\ps v
\ge tomber
\xv kole pwa
\dialx GO
\xn il pleut
\xv i kule pwal
\dialx PA
\xn il pleut
\xv la daa pe-kule pò-mãã
\dialx PA
\xn les mangues tombent toutes d'elles-mêmes (toutes seules)
\cf thrõbo ; kaalu
\dt 20/Feb/2025

\lx kole
\hm 2
\dialx GOs BO
\va köle
\dialx GOs PA
\sn 1
\is pêche
\ps v
\ge jeter (filet) ; déployer ; étendre (filet)
\xv kole pwiò ; kule pwiò
\dialx GO
\xn jeter/étendre un filet dans l'eau
\sn 2
\is action_eau_liquide_fumée
\ps v
\ge vider ; renverser (liquide) ; répandre
\xv kole we
\xn arroser ; verser l'eau
\dt 22/Feb/2025

\lx kõle
\dialx BO
\is santé
\ps v
\ge avorter
\xv i kõle ẽnõ
\xn elle a avorté [BM]
\nt non vérifié
\dt 15/Sep/2021

\lx kole pwa
\dialx GOs
\va kule pwal
\dialx PA
\is temps_atmosphérique
\ps v
\ge pleuvoir
\dt 16/Aug/2021

\lx kole-ò
\dialx BO
\is nom_locatif
\ps n
\ge rive de l'autre côté
\nt non vérifié
\dt 23/Jan/2022

\lx kòli
\dialx GOs BO PA
\sn 1
\is configuration
\ps n.loc
\ge bord ; côté
\xv kòli jaaò
\dialx GO
\xn la rive de la rivière
\xv kòli jaòl
\dialx PA
\xn le bord de la rivière
\xv kòli we
\dialx GO
\xn la rive ; la berge
\sn 2
\is grammaire_préposition_locative
\ps PREP.LOC
\ge près de ; bord de (au) (très proche)
\xv e a höze kòli we-za
\dialx GO
\xn il suit le bord de la mer
\cf kòlo
\ce à côté (plus loin)
\dt 22/Feb/2025

\lx kòli kaze
\dialx GOs
\is topographie
\ps n ; LOC
\ge bord de mer
\xv nõõli kòli kaze
\xn regarde le bord de mer
\dt 30/Dec/2021

\lx kòli we-za
\dialx PA
\is topographie
\ps LOC
\ge bord de mer (au)
\xv nõõli kòli we-za
\xn regarde le bord de mer
\dt 08/Jan/2022

\lx kòlò
\dialx GOs
\va kòlò-n
\dialx PA BO
\va kòli
\dialx GO(s) PA
\sn 1
\is configuration
\ps n
\ge côté ; bord ; extrémité ; lisière
\ge flanc
\se kòlò bo
\sge le bord du ravin
\se kòlò dè
\dialx GO
\va kòlò den
\dialx PA
\sge bordure du chemin ; petit talus en bordure du chemin
\se bwa kòlò
\sge sur le côté
\se kòlò mhõ
\dialx PA
\sge à gauche
\se kòlò gu-i
\dialx PA
\sge à droite
\se kòlò bwa mhwã
\dialx GO
\sge du côté droit
\sn 2
\is nom_locatif
\ps n.loc
\ge près de ; à ; chez ; vers ; auprès de ; de l'autre côté de
\xv kòlò-nu
\dialx GO
\xn chez moi
\xv kòlò-ny
\dialx PA
\xn chez moi
\xv kòlò-çö
\dialx GO
\xn près de toi
\xv kòlò-m
\dialx PA
\xn près de toi
\xv e yu kòlò-lò
\dialx GO
\xn il vit chez eux
\xv kòlò phwa
\dialx GO
\xn au bord du trou
\xv kavwö mõ hine khõbwe ge ea mwã xa kòlò iò
\dialx GO
\xn on ne  sait pas où peut bien être la lisière
\xv ge je kòlò-n êba ni we
\dialx PA
\xn il est de l'autre côté de la rivière
\xv kòlò-òli
\xn de l'autre côté
\xv hãgana xa ge-me kòlò-ã êna bwa kavwègu i we
\xn aujourd'hui nous trois nous tenons ici dans votre chefferie
\se kòlò dèèn
\dialx PA BO
\sge exposé au vent
\se kòlò kò
\sge près de la forêt
\se kòlò pwèmwa
\dialx PA
\sge ouest (lit. côté de la porte)
\sn 3
\is grammaire_préposition
\ps PREP
\ge à ; pour ;  à propos de
\xv hulò kòlò Peiva
\dialx PA
\xn mon présent coutumier à Peiva
\xv e phe-du xo kêê-nu we-kò kui kòlò oã-nu
\dialx GOs
\xn mon père a apporté trois ignames à ma mère
\xv i kòòl kòlò-n
\dialx PA
\xn il prend sa défense
\xv hulò mèè-nu kòlò Peiva
\dialx GO
\xn mon présent coutumier pour/chez Peiva
\xv u kõbwe wo Thomaxim ya kòlò Thonòòl
\dialx PA
\xn Thomaxim dit à Thonòòl
\xv vhaa kòlò xa pwaixe na cò ezoma trõne iò
\dialx GO
\xn des paroles sur des choses que vous allez entendre tout à l'heure
\dt 24/Feb/2025

\lx kòlò ije
\dialx BO
\is parenté
\ps n
\ge fils du frère (sœur du père parlant)
\nt selon Corne
\dt 26/Mar/2022

\lx kòlò-khia
\dialx BO
\is cultures
\ps n
\ge pente du massif d'ignames
\nt selon Dubois
\cf khia
\ce massif d'ignames
\dt 26/Mar/2022

\lx kòmaze
\ph kɔmaðe
\dialx GOs
\is corps
\ps n
\ge poumons
\dt 06/Feb/2019

\lx kôme
\dialx GOs
\is action
\ps v
\ge étouffer
\xv e kôme-nu xo laai
\xn je me suis étranglée avec le riz (le riz m'a étouffée)
\dt 20/Aug/2021

\lx kò-mõnu
\dialx PA
\is déplacement
\ps v
\ge approcher
\xv kò-mõnu kaò
\dialx PA
\xn la saison des pluies approche
\dt 23/Aug/2021

\lx komwãcii
\dialx GOs
\is action_corps
\ps v
\ge chatouiller
\dt 25/Jan/2018

\lx kõmwõgi
\dialx GOs
\va kõmõgin
\dialx BO (Dubois)
\is grammaire_quantificateur_mesure
\ps QNT
\ge rond ; entier
\xv kavwö nu hine kõmwõgi-ni
\dialx GO
\xn je ne sais pas tout (Schooling)
\ng v.t. |lx{V-kõmwõgi-ni} dans une construction verbale complexe
\dt 16/Feb/2025

\lx kõnõbwòn
\ph kɔ̃nɔ̃mbwɔn
\dialx PA BO
\va kõnõ-bòn
\dialx PA BO
\is temps_deixis
\ps ADV
\ge hier
\xv je ka kõnõbwòn
\dialx BO
\xn l'an dernier
\cf dròòrò
\ce hier
\dt 30/Dec/2021

\lx kõnõbwòn èò
\dialx PA BO
\is temps_deixis
\ps ADV
\ge avant-hier
\se kõnõbòn aeò
\dialx PA
\sge avant-hier
\se kõnõbwòn èhò
\dialx PA
\sge avant-hier
\dt 29/Jan/2022

\lx kônõ-da
\ph kõɳɔ̃nda
\dialx GOs BO
\is position
\ps v
\ge couché sur le dos
\ge couché la tête vers l'intérieur de la maison
\xv e kônõ-da (ni dònò)
\dialx GO
\xn il est couché sur le dos (lit. couché vers le haut (vers le ciel))
\xv e kônõ-da bwaa-je ni nõ mwa
\dialx GO
\xn il est couché la tête vers l'intérieur de la maison
\xv i kônõ-da ni phwa
\dialx BO
\xn il est couché sur le dos [Corne]
\et *qenop
\eg couché
\el POc
\dt 17/Feb/2025

\lx kônõ-du
\ph kõɳɔ̃ndu
\dialx GOs BO PA
\is position
\ps v
\ge couché sur le ventre
\ge couché (la tête) vers la porte
\xv e kônõ-du
\xn il est couché sur le ventre (lit. couché vers le bas)
\xv e kônõ-du ni phwè-mwa
\dialx GO
\xn il est couché (la tête) vers la porte
\dt 17/Feb/2025

\lx kõńõõ
\ph kɔ̃nɔ̃:
\dialx GOs
\va kônôôl
\dialx BO PA
\sn 1
\is caractéristiques_personnes
\ps v
\ge paresseux (humain)
\xv êgu xa kõńõõ
\xn un paresseux
\sn 2
\is caractéristiques_animaux
\ps v
\ge doux ; apprivoisé (animal)
\ng causatif: |lx{pa-kõńõõ-ni}
\gt apprivoiser (animal)
\dt 08/Feb/2025

\lx kô-nõõ
\ph kõɳɔ:
\dialx GOs
\va kô-nòi
\dialx GO BO
\is fonctions_naturelles
\ps v
\ge rêver
\xv e kô-nõõ
\dialx GO
\xn il rêve
\xv nu kô-nõõ
\dialx GO
\xn j'ai fait un rêve
\xv kô-nõõ-nu i nyãnyã (ou) kô-nõi-nu nyãnyã
\dialx GO
\xn j'ai rêvé de maman
\xv nu kô-nòi-ny i nyãnyã
\dialx BO
\xn j'ai rêvé de maman
\dt 22/Mar/2023

\lx kônôô
\ph kõnõ:
\dialx WEM WE
\is caractéristiques_personnes
\ps v.stat
\ge maladroit ; gauche ; pas débrouillard
\dt 03/Feb/2025

\lx kô-nòòl
\dialx PA BO
\is fonctions_naturelles
\ps v
\ge rester éveillé (couché)
\dt 08/May/2024

\lx kô-nõõli tree
\ph kõɳɔ̃:li ʈe:
\dialx GOs
\va kô-nõõli tèèn
\dialx PA
\is fonctions_naturelles
\ps v
\ge réveiller (se) tôt
\xv bî kô-nõõli tree-mõnõ
\dialx GO
\xn nous nous levions tôt le lendemain matin (lit. voir le jour se lever)
\xv nu kô-nõõli tèèn
\dialx PA
\xn je me suis réveillé de bonne heure
\dt 02/Jan/2022

\lx kônya
\dialx GOs
\va kônyal
\dialx WEM WE PA
\is caractéristiques_personnes
\ps v
\ge drôle ; risible ; ridicule
\xv e a-thu kônya
\xn il est drôle ; il fait rire ; il fait le clown
\dt 22/Feb/2025

\lx kõ-nhyò
\dialx PA
\is mammifères
\ps n
\ge essaim de roussettes
\dt 08/Feb/2025

\lx kòò
\ph kɔ:
\dialx GOs
\va kòòl, kòl
\dialx PA BO WEM
\sn 1
\is position
\ps v
\ge debout ; debout (être) ; dresser (se) ; mettre debout (se) ; debout (être) immobile
\xv e kòò-da
\dialx GO
\xn elle se lève
\se pa-kòò-ni
\sge mettre debout ; dresser
\ng v.t. |lx{kòòli} |dialx{PA BO} ; |lx{kòò-ni} |dialx{GO}
\gt mettre debout ; dresser
\sn 2
\is action
\ps v
\ge arrêter de marcher ; s'arrêter
\xv çö kòò ! çö ga kòò gò !
\dialx GO
\xn attends! ; arrête-toi !
\xv kòòl !
\dialx PA
\xn arrête-toi !
\sn 3
\is action
\ps v
\ge attendre
\xv çö kòò vwö nu a threi cee
\dialx GO
\xn attends, je vais aller couper du bois
\et *tuqud
\el POc
\dt 22/Feb/2025

\lx kôô
\hm 2
\dialx GOs
\is insecte
\ps n
\ge mante religieuse
\dt 29/Jan/2019

\lx kôô
\hm 3
\dialx GOs
\va kôông
\dialx BO PA
\is oiseau
\ps n
\ge long-cou ; héron à face blanche ; héron gris des rivières
\sc Ardea novæhollandiæ nana; Ardea sacra albolineata
\scf Ardéidés
\dt 24/Feb/2025

\lx kôôbua
\dialx GOs
\va kôôbwa
\dialx GO
\va meebwa
\dialx PA WE WEM
\sn 1
\is caractéristiques_personnes
\ps v
\ge dynamique ; en forme |dialx{GOs}
\xv nu kôôbua
\xn je suis en forme
\sn 2
\is caractéristiques_personnes
\ps v
\ge obéir ; obéissant ; docile ; serviable ; prêt à aider ; bien disposé
\xv e a-kôôbwa
\xn il est serviable/prêt à aider
\se êgu a-kôôbua
\sge personne/quelqu'un toujours bien disposée
\an aa-kue
\at pas serviable (qui refuse toujours)
\dt 22/Feb/2025

\lx kòò-da
\dialx GOs PA BO
\is habillement
\ps v
\ge s'habiller élégamment |dialx{GOs}
\ge vêtir (se) ; habiller (s') |dialx{PA BO}
\dn se dit plutôt des vêtements du bas et des chaussures |dialx{PA BO}
\xv i kòò-da-le pazalõ
\dialx PA
\xn il met son pantalon
\xv kòò-da ni je hòbòni-m
\dialx PA
\xn habille-toi !
\xv a kòò-da-le hòbwòni-m
\dialx BO
\xn va t'habiller !
\xv a kòò-da-le taî-m
\dialx BO
\xn va t'habiller !
\dt 21/Mar/2023

\lx kòò-dö
\ph kɔ:dω
\dialx GOs
\is armes
\ps n
\ge manche de sagaie
\dt 02/Jan/2022

\lx kool
\dialx PA BO
\is eau_mer
\ps n
\ge vague
\et *qaRus
\eg couler
\el POc
\dt 10/Mar/2019

\lx kòòl
\dialx PA
\is richesses
\ps n
\ge monnaie kanak
\dn dont la longueur est calculée debout, de la hauteur de la tête jusqu'au sol (selon Charles Pebu-Polae)
\cf gò-hii, tabwa
\dt 22/Oct/2021

\lx kõõl
\dialx BO
\va kôôhòl
\dialx BO
\is son
\ps v
\ge gargouiller
\xv i kõõl (a) kiò-ny
\dialx BO
\xn mon ventre gargouille
\nt selon BM
\dt 26/Mar/2022

\lx kòòl kòlò
\dialx PA
\is interaction
\ps v
\ge prendre le parti de qqn. ; défendre
\xv i kòòl kòlò-n
\xn il prend sa défense
\dt 22/Feb/2025

\lx kòòli
\dialx GOs BO
\sn 1
\is santé
\ps v
\ge blesser (se)
\dn sur un objet piquant
\ge piquer (se)
\xv e kòòli kòò-je
\xn il s'est piqué le pied
\xv e thuvwu kòòli hii-je xo du-bwò
\xn il s'est piqué la main avec l'aiguille
\sn 2
\is action
\ps v
\ge gratter
\ge toucher avec une pointe
\se kòòli dili
\sge gratter la terre (comme les poules)
\dt 08/Feb/2025

\lx koone
\dialx BO
\is navigation
\ps v
\ge virer de bord vent debout ; louvoyer
\se pe-koone
\sge tirer des bordées vent debout; louvoyer
\cf pweweede nhe
\nt non vérifié
\dt 26/Jan/2019

\lx kööni
\ph kω:ɳi
\dialx GOs
\va kooni
\ph ko:ni
\dialx PA WEM
\is préparation_aliments
\ps v
\ge cuire à l'étouffée
\ge cuire au four enterré ; mettre au four enterré
\xv kööni pwò !
\xn mets la tortue au four !
\cf kîbi
\ce four enterré
\dt 26/Aug/2021

\lx kòò-pwe
\dialx GOs BO
\is pêche
\ps n
\ge canne à pêche
\dt 15/Sep/2021

\lx köö-vwölö
\ph kω:-'vwωlω
\dialx GOs
\va kuuwulo, kuuwolo
\dialx BO
\is corps
\ps v
\ge albinos
\dt 04/Feb/2019

\lx kòò-wamwa
\dialx GOs
\va kòò-wamon
\dialx BO
\is outils
\ps n
\ge manche de hache
\dt 27/Jan/2019

\lx kòòwe
\dialx BO
\is sentiments
\ps v
\ge tourmenté (être)
\dt 18/Mar/2021

\lx kôôxô
\dialx GOs
\va kôôhòl
\ph kõ:hɔl
\dialx PA BO
\va kôôl
\ph kõ:l
\dialx BO
\is fonctions_naturelles
\ps v
\ge gargouiller (ventre)
\xv e kôôxô kiò-nu
\dialx GO
\xn j'ai le ventre qui gargouille
\xv i kôôhòl kiò-ny
\dialx BO
\xn j'ai le ventre qui gargouille
\dt 10/Mar/2019

\lx kô-pa-ce-bò
\dialx GOs
\va kô-pha(a)-ce-bòn
\dialx WEM
\va kô-pha-ce-bòn
\ph kõpʰacɨbɔn
\dialx PA
\is position
\ps v
\ge couché près du feu ; dormir près du feu (la nuit)
\xv lha pwaa ce-bòn, wö lha kô-pha-ce-bòn
\dialx WEM
\xn ils cassent du bois pour la nuit, afin qu'ils dorment près du feu
\dt 22/Feb/2025

\lx ko-pe-bulu
\ph koβebulu
\dialx GOs
\is position
\ps v
\ge debout ensemble
\xv mõ uça vwö mwã ko-pe-bulu mwã hãgana
\dialx GO
\xn nous trois sommes venus pour que nous soyons (tous) réunis aujourd'hui
\xv mõ pe-ko pe-bulu êne
\xn nous sommes debout réunis ici
\ng |lx{ko-pe-bulu} s'utilise pour des groupes qui s'associent; tandis que |lx{ko-bulu} réfère à un seul groupe
\dt 24/Feb/2025

\lx kò-popwale
\dialx GOs
\is plantes_partie
\ps n
\ge tige de maïs
\ge maïs (pied de)
\se pò-popwale
\sge épi de maïs
\dt 21/Mar/2023

\lx kô-phaaxe
\dialx GOs
\va kô-phaaxen
\dialx PA BO
\is position
\ps v
\ge allongé en écoutant
\dt 25/Jan/2019

\lx kô-phi
\dialx GOs PA WE
\va pii
\dialx BO
\is corps
\ps n
\ge testicules
\xv kô-phii-n
\dialx PA WE
\xn ses testicules
\dt 21/Aug/2021

\lx kô-phöö
\dialx GOs
\is position
\ps v
\ge dormir sur le ventre
\dt 26/Aug/2021

\lx kô-raa
\ph kõɽa:
\dialx GOs PA BO
\is grammaire_modalité
\ps v.IMPERS
\ge impossible ; difficile
\dn (lit. couché mal)
\ge jamais
\xv kô-raa na nu mããni
\dialx GO
\xn je ne peux pas dormir
\xv kô-raa vwo thoi lã-nã
\dialx GO
\xn il ne pourra pas planter ces plants
\an kô-zo
\dialx GOs
\at possible, bon que
\an kôô-jo
\dialx BO
\at possible, bon que
\dt 09/Feb/2025

\lx kòròò
\dialx GOs
\va kònòò
\dialx GOs
\is fonctions_naturelles
\ps v
\ge étrangler (s') ; étouffer (s')
\xv e kòròò-nu xo hovwo
\dialx GO
\xn j'ai avalé de travers ; je me suis étouffé (lit. la nourriture m'a étouffé)
\dt 22/Feb/2025

\lx kotô
\dialx GOs
\is plantes
\ps n
\ge cotonnier
\bw coton (FR)
\dt 29/Jan/2019

\lx kô-töö
\dialx GOs BO
\is position
\ps v
\ge incliné (arbre)
\dn (lit. couché-ramper)
\cf kiluu
\ce se courber ; se prosterner
\dt 08/Feb/2025

\lx kotra
\ph koɽa
\dialx GOs
\va kora
\dialx BO
\is igname
\ps n
\ge igname (violette et grosse)
\nt selon Dubois
\dt 17/Feb/2025

\lx kòtrixãã
\ph kɔɽiɣɛ̃ kɔɽiɣẽ
\dialx GOs
\va kòtrikãã, kòtrikê
\ph kɔʈikɛ̃  kɔʈikẽ
\dialx arch.
\va kòriã, kòriê
\ph 'kɔɽiɛ̃  'kɔɽiẽ
\dialx GO(s)
\va kòòlixê
\dialx GO(s)
\is sentiments
\ps v ; n
\ge colère ; se mettre en colère
\cf pojai
\dialx GOs
\ce être en colère
\dt 21/Mar/2023

\lx kou
\hm 1
\dialx GOs PA
\is arbre
\ps n
\ge arbre de bord de mer
\dn sa sève est toxique et est utilisée pour soigner les piqûres de raie
\sc Excœcaria agallocha L.
\scf Euphorbiacées
\dt 27/Aug/2021

\lx kou
\hm 2
\dialx PA
\is temps_saison
\ps n
\ge saison de disette
\dn de décembre à avril
\dt 08/Feb/2025

\lx kovwanyi
\dialx GOs
\is interaction
\ps n
\ge compagnon ; ami
\bw compagnie (FR)
\xv li kovwanyi
\dialx GO
\xn ils sont compagnons
\xv kovwanyi-nu
\xn mes amis/compagnons
\dt 22/Feb/2025

\lx kò-waayu
\dialx PA
\is grammaire_aspect
\ps v
\ge persister à
\dn sens positif
\cf kò- < kòòl
\ce debout
\dt 08/Feb/2025

\lx kô-wãga
\dialx GOs
\is position
\ps v
\ge allongé les jambes écartées
\dt 08/Jan/2022

\lx kò-wiò
\dialx GOs
\va kòyò
\dialx GO(s)
\va koeo
\dialx GO(s)
\is parenté
\ps n
\ge frère cadet (tous les frères sauf l'aîné)
\ge cousin (parallèles: fille de frère de père ; fils/fille de sœur de mère)
\xv kò-wiò-nu
\xn mon frère cadet (plus jeune)
\cf ebiigi
\ce cousins croisés
\dt 26/Aug/2021

\lx kò-wony
\dialx BO
\is navigation
\ps n
\ge mât du bateau
\dt 26/Jan/2019

\lx köxa
\ph kωɣa
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge alors ; puisque
\xv köxa, na cö puxãnuu-je
\xn soit/alors puisque tu l'aimes
\xv köxa, co a-da whili-je-du
\xn et bien alors, monte et amène-la ici en bas
\dt 22/Feb/2025

\lx köxö
\ph kωɣω
\dialx GOs PA
\is discours
\ps v
\ge bégayer
\et *kakap
\eg bégayer
\el POc
\ea Blust
\dt 31/Jan/2019

\lx kòyò
\dialx GOs BO PA
\is action
\ps v (non humains)
\ge perdu ; disparaître
\xv e kòyò pòi-nu kuau
\xn mon chien est perdu
\xv e kòyò
\xn c'est perdu
\ng causatif: |lx{pa-kòyò-ni}
\gt supprimer ; enlever qqch.
\dt 22/Feb/2025

\lx kôzaxebi
\ph kõ'ðaɣembi
\dialx GOs BO
\va kôzakebi
\dialx GO
\is caractéristiques_personnes
\ps v
\ge avoir l'habitude ; sage
\xv nu kôzaxebi nye cabo ne whaa gò
\dialx GO
\xn j'ai l'habitude de me lever tôt le matin
\dt 21/Aug/2021

\lx kô-zo
\dialx GOs WEM
\va kô-yo
\dialx PA BO
\is grammaire_modalité
\ps v
\ge possible de ; permis de
\xv kô-zo na çö a-du traabwa ?
\dialx GO
\xn peux-tu venir t'asseoir ?
\xv kô-zo na nu whili-cö
\dialx GO
\xn je peux te conduire
\xv kô-zo na la pe-tòò-la monòn mãni bò-na
\dialx PA
\xn ils pourront se retrouver demain et après-demain
\an kô-raa na
\at impossible que
\cf e zo
\ce il faut que
\dt 13/Aug/2021

\lx ku
\hm 1
\dialx GO PA BO
\va ko, xo, vwo, o
\dialx GO PA BO
\va (h)u
\dialx BO
\is grammaire_agent
\ps AGT ; sujet
\ge sujet ; agent (marque)
\xv i kobwe ku Tèma
\dialx Haudricourt
\xn le chef dit
\xv i kubu ije ko kãbwa
\xn le dieu le frappe
\dt 26/Mar/2023

\lx ku
\hm 2
\dialx PA BO
\is plantes
\ps n
\ge liane
\dn variété de liane dont on mange les fruits légèrement grillés, et qui ont un goût de café
\dt 26/Aug/2021

\lx ku
\hm 3
\dialx GOs
\va kul, kuul
\dialx PA BO WE
\sn 1
\is mouvement
\ps v.i
\ge tomber (pour un inanimé) ; se répandre
\sn 2
\is plantes_processus
\ps v.i
\ge tomber (tout seul)
\dn comme les fruits et les feuilles
\xv la ku
\dialx GOs
\xn ils sont tombés (d'eux-mêmes)
\xv i kul
\dialx BO
\xn il est tombé
\xv la daa pe-kuul (e) pò-maak
\dialx PA
\xn les mangues tombent toutes seules/d'elles-mêmes
\xv la daa pe-kuul
\dialx PA
\xn elles tombent toutes seules/d'elles-mêmes
\xv i ra u kul mwã nye-du pò-jeü
\dialx PA
\xn le fruit de kaori est tombé
\cf thrõbo ; kaalu
\dt 24/Feb/2025

\lx ku
\hm 4
\dialx GOs PA
\va kun
\dialx PA
\is nom_locatif
\ps n
\ge endroit ; place
\xv kun-na
\dialx PA
\xn cet endroit-ci
\dt 23/Jan/2022

\lx ku
\hm 5
\dialx GOs BO PA
\va kuun
\dialx BO
\is topographie
\ps n
\ge fond de la vallée ; haut d'une vallée
\ge amont ou source de rivière ou creek
\se ku-nõgo
\dialx BO
\sge la source de la rivière
\et *qulu
\eg tête, sommet, upper end of valley
\el POc
\ea Geraghty
\dt 19/Oct/2021

\lx ku
\hm 6
\dialx GOs
\is grammaire_aspect
\ps ASP.HAB
\ge habitude
\xv e ku nee
\xn il le faisait souvent
\xv lò ku khûbu-bî c(h)ãnã
\xn ils nous tapaient souvent
\xv kavwö nu ku nõõ-je gò
\dialx GO
\xn je ne l'ai encore jamais vu
\xv kavwö e ku cabo gò
\dialx GO
\xn il ne s'était pas encore réveillé
\xv e ku phai ce-me mwã xo je
\dialx GO
\xn elle nous faisait tout le temps cuire nos féculents
\dt 20/Jan/2025

\lx ku-
\hm 1
\dialx BO
\is igname
\is préfixe_sémantique
\ps n
\ge préfixe des ignames
\se ku-kò
\sge igname poule
\se ku-mõlò
\sge igname blanche (la dernière à arriver à maturité)
\cf ku-bè ; ku-peenã ; ku-pe ; ku-jaa ; ku-cu ; ku-dimwã ; ku-bwaole ; ku-bweena
\ce noms de clones d'igname
\dt 24/Jan/2022

\lx ku-
\hm 2
\dialx GO PA BO
\is préfixe_sémantique_position
\ps PREF (position debout)
\ge debout
\xv i ku nõ-da
\xn il regarde en l'air debout
\xv ku nenèm !
\xn tiens-toi (debout) tranquille !
\et *tuqud
\el POc
\dt 03/Feb/2025

\lx ku-
\hm 3
\dialx GO PA BO
\is préfixe_sémantique
\ps PREF
\ge bout ; extrémité d'une surface ou d'une chose étendue
\dt 22/Feb/2025

\lx ku-ã
\dialx GOs
\va kunã
\dialx GO PA
\is nom_locatif
\ps n.loc
\ge de ce côté-là (pas loin)
\ge place
\xv kunãã-je
\dialx GO
\xn sa place
\cf phe kunã
\dialx GO PA
\ce prendre la place de qqn.
\dt 21/Feb/2025

\lx kuãgo
\dialx GO
\va kuãgòòn, kwãgòn
\dialx BO
\is mollusque
\ps n
\ge moule ; coquillage rond
\nt selon Corne
\sc Mytilus smaragdinus
\scf Mytilidés
\dt 21/Mar/2023

\lx kuani
\hm 1
\dialx PA
\is nourriture
\ps v
\ge laisser fondre dans la bouche
\dt 26/Jan/2019

\lx kuani
\is pêche
\hm 2
\dialx PA
\ps v
\ge mettre sur une filoche (poisson)
\xv pe-kuani li-ne nõ
\dialx PA
\xn mets ces deux poissons sur une filoche
\dt 24/Feb/2025

\lx kuau
\dialx GOs PA BO
\is mammifères
\ps n
\ge chien
\xv pòi-nu kuau
\dialx GOs
\xn mon chien
\xv e kòyò pòi-nu kuau
\dialx GOs
\xn mon chien est perdu
\bw kuau (POLYN) the young of an animal
\dt 10/Jan/2022

\lx ku-baazo
\dialx PA
\va ku-baayo
\dialx BO
\is position
\ps v
\ge travers (être de)
\ge incliné
\dn utilisé pour tout, y compris pour le soleil, lorsqu'il descend vers l'horizon
\dt 26/Mar/2022

\lx ku-baazo al
\dialx PA BO
\is temps_découpage
\ps n
\ge après-midi
\dn lorsque le soleil descend vers l'horizon
\dt 08/Feb/2025

\lx ku-be
\dialx GOs BO
\is igname
\ps n
\ge igname (variété)
\dn ignames à petites racines, plantées sur le bord du billon, elles poussent plus vite que celles à racines longues du centre du billon et donnent les premières récoltes (selon Charles Pebu-Polae)
\dt 22/Oct/2021

\lx kûbi
\dialx GOs PA BO
\sn 1
\is action
\ps v
\ge gratter
\sn 2
\is préparation_aliments
\ps v ; n
\ge écailler le poisson ; écaille (de poisson)
\xv nu kûbi nõ
\dialx GOs
\xn j'écaille un poisson
\xv kûbi-n
\dialx BO
\xn ses écailles
\sn 3
\is santé
\ps n
\ge croûtes sur la tête des bébés
\et *qunapi
\eg écailler
\el POc
\dt 21/Mar/2023

\lx kubo
\dialx GOs PA
\is action
\ps v
\ge attendre
\xv kubo-je !
\dialx GOs
\xn attends-le !
\xv çö tre-kubo ti ?
\dialx GOs
\xn qui attends-tu ?
\xv nu tre-kubo thoomwã-ã
\dialx GOs
\xn j'attends cette femme-ci
\ng forme courte de |lx{ku-hôbwo}
\dt 24/Feb/2025

\lx ku-bulu
\dialx PA
\is mouvement
\ps v
\ge rassembler debout (se)
\dt 26/Jan/2018

\lx ku-bwau
\dialx GOs BO PA
\is igname
\ps n
\ge igname ronde
\dn ignames à petites racines, on les plante sur le bord du billon, elles poussent plus vite que celles à racines longues du centre du billon et donnent les premières récoltes (selon Charles Pebu-Polae)
\cf ku-be, zaòl
\dt 22/Oct/2021

\lx ku-bweena
\dialx GOs BO
\is igname
\ps n
\ge igname
\dn sa tige est de la couleur d'un lézard
\dt 08/Feb/2025

\lx ku-bwii
\dialx GOs
\is igname
\ps n
\ge igname
\dn clone, à chair violette
\dt 17/Feb/2025

\lx ku-çaaxò
\ph ku'ʒa:ɣɔ
\dialx GOs
\va ku-caaxò
\ph ku-'ca:ɣɔ
\dialx PA BO
\sn 1
\is action_corps
\ps v.i
\ge cacher (se)
\sn 2
\is guerre
\ps v.i
\ge réfugier (se)
\cf caaxò
\ce se cacher
\cf v.t. thözoe
\ce cacher qqn.
\dt 21/Feb/2025

\lx ku-cabo
\ph kucabo
\dialx GOs
\is igname
\ps n
\ge igname
\dn qui sort de terre
\dt 08/Feb/2025

\lx ku-cimwi
\dialx GOs PA
\is position
\ps v
\ge debout en tenant qqch. serré dans la main
\dt 29/Mar/2022

\lx kûdi
\ph 'kûndi
\dialx GOs PA
\is configuration
\ps n
\ge coin ; angle
\dt 21/Mar/2023

\lx kûdi-mwa
\ph kû'ndimwã
\dialx GOs PA
\is maison
\ps n
\ge coin externe de la maison
\cf puni, puning
\ce fond de la maison ronde
\dt 21/Mar/2023

\lx kûdo
\hm 1
\dialx GOs
\is eau_topographie
\ps n
\ge baie
\se kûdo phwè jaaò
\sge l'embouchure de la rivière
\dt 23/Aug/2021

\lx kûdo
\hm 2
\dialx GOs
\is poisson
\ps n
\ge rémora
\sc Echeneis naucrates
\scf Echeneidés
\dt 15/Feb/2025

\lx kûdò
\ph kûndɔ
\dialx GOs PA
\va kido
\dialx PA BO WEM WE
\wr A
\ps n, v
\sn 1
\is nourriture
\ps n
\ge boisson
\xv kidoo-n
\dialx PA BO
\xn sa boisson
\sn 2
\is nourriture
\ps v
\ge boire
\xv nu kûdò
\xn je bois
\xv kûû kafe ?
\xn (tu) bois/veux du café ?
\xv la pe-kido
\dialx PA
\xn ils ont bu ensemble
\xv kido tòò
\dialx PA
\xn boire chaud
\xv kûdò tuuçò
\dialx GO
\xn boire de l'eau froide
\xv kido tuujong
\dialx PA
\xn boire froid
\ng causatif: |lx{pha-kido-ni} |dialx{PA}
\gt faire boire (qqn.)
\wr B
\is classificateur_possessif_nourriture
\ps CLF.POSS
\ge CLF des boissons
\xv kûdòò-nu we
\dialx GO
\xn mon eau (lit. ma boisson eau)
\xv kidoo-ny (a) we
\dialx PA
\xn mon eau (lit. ma boisson eau)
\et *qanjauq
\el PNC (Proto-Neo-Caledonian)
\ea Haudricourt
\dt 22/Feb/2025

\lx kue
\dialx GOs
\is position
\ps v
\ge tenir ; retenir 
\xv e kue ẽnõ
\xn il tient l'enfant (dans les bras)
\xv e kue-wãã-le
\xn il la tient ainsi 
\dt 06/May/2024

\lx kuè
\dialx GOs
\va kuel, kwèl
\dialx BO
\is interaction
\ps v
\ge refuser ; rejeter ; désobéir
\ge détester
\xv nu kuè na nu nhuã
\dialx GOs
\xn je refuse de la lâcher
\xv nu za mhãã kuè na mõ baani
\dialx GOs
\xn je refuse absolument que nous trois le tuions
\xv i kwèl
\dialx BO
\xn il ne veut pas
\se aa-kuè
\sge pas serviable (qui refuse toujours)
\ng v.t. |lx{kuèle ; kuène} |dialx{GO}
\gt refuser qqch.
\dt 24/Feb/2025

\lx kuee
\dialx GOs
\is action
\ps v
\ge résister (à une épreuve) ; tenir le coup
\dt 29/Jan/2019

\lx ku-êgu
\dialx GOs
\is igname
\ps n
\ge igname
\dn son nom vient du fait qu'elle a la forme d'une personne
\dt 09/Feb/2025

\lx kuel
\dialx PA BO
\va kwel
\dialx PA BO
\is interaction
\ps v.i
\ge rejeter ; refuser ; aimer (ne pas)
\xv i kuel
\dialx BO
\xn il ne veut pas
\xv co kwèl inu
\dialx BO
\xn tu ne veux pas de moi
\xv ã a-xe i kuèl na kõbwe i Tea-̃ma ã a-xe
\dialx PA
\xn l’un refuse que l’autre dise qu’il est le Grand Chef
\ng v.t. |lx{kuele, kuene}
\gt rejeter qqch./qqn.
\dt 21/Feb/2025

\lx kuèle
\dialx GOs PA BO
\va kwel
\dialx PA BO
\is interaction
\ps v.t.
\ge rejeter ; refuser ; détester ; aimer (ne pas)
\xv nu kuèle la nu nee
\dialx GOs
\xn je déteste ce que j'ai fait
\xv nu kuèli-çö, nu kwèli-çö
\dialx GOs
\xn je te déteste
\xv êgu xa nu kuèle
\dialx GOs
\xn une personne/quelqu'un que je déteste
\ng v.t. |lx{kuèli} (+ objet pronominal), |lx{kuèle, kuène} (+ objet nominal)
\ng v.i. |lx{kuè} |dialx{GOs}, |lx{kuel, kwèl} |dialx{BO PA}
\gt refuser
\dt 21/Mar/2023

\lx kugoo
\dialx GOs
\is caractéristiques_objets
\ps v
\ge mou ; flasque
\dt 26/Jan/2018

\lx kû-gòò
\dialx GOs
\va kô-go
\dialx BO
\wr A
\ps v.stat
\sn 1
\is caractéristiques_objets
\ge droit ; rectiligne
\sn 2
\is fonctions_intellectuelles
\ge vrai
\xv fhaa ka kû-gòò
\dialx GA
\xn parole vraie
\xv vhaa xa kû-gòò
\dialx GO
\xn parole vraie
\ng causatif |lx{pa-kû-gòò-ni} |dialx{GOs}
\gt approuver
\wr B
\is grammaire_modalité
\ps n
\ge droit ; autorisation
\xv pu kû-gòò-nu vwo nu vhaa cai çö
\dialx GO
\xn j'ai le droit de te parler
\dt 03/Feb/2025

\lx ku-gozi
\dialx GOs
\is igname
\ps n
\ge igname blanche
\dt 29/Jan/2019

\lx ku-hôboe
\dialx GOs
\va ku-hôbwo
\dialx GO(s) BO
\sn 1
\is action
\ps v
\ge surveiller ; garder ; faire le guet
\sn 2
\is guerre
\ps v
\ge embuscade (faire une) ; surveiller la route
\xv la ku-hôboe phwee-de-la
\xn ils les ont attendus sur le chemin
\cf hôbwo
\ce surveiller
\dt 20/Dec/2021

\lx kui
\dialx GOs BO PA
\is igname
\ps n
\ge igname
\sc Dioscorea alata
\scf Dioscoréacées
\xv kui-nu
\xn mon igname (tubercule)
\se ńõ-kui
\sge extrémité inférieure de l'igname
\se gu kui
\sge l'igname du chef (offerte pour les prémices)
\se cè-nu kui
\dialx GO
\sge mon igname (cuite) (lit. nourriture-ma igname)
\cf nom de différents clones : kui paao ; kui kamôve ; kui pòwa ; kui êpâdan/evadan ; yave ; kajaa ; kora ; kera ; kubwau ; djinodji ; bea ; mwacoa ; zara ; thiabwau ; ua ; papua ; hou ; dimwã
\et *qupi
\el POc
\dt 22/Feb/2025

\lx ku-ido-xe
\dialx GOs
\is action
\ps v
\ge aligner (des choses)
\xv çö ne wu ku-ido-xe ce-thîni
\xn aligne les poteaux de la barrière
\cf -xe
\ce un
\dt 29/Jan/2019

\lx kuii
\dialx GOs
\is corps
\ps n
\ge rein ; rognon
\dt 24/Jan/2019

\lx ku-jaa
\dialx GOs
\is igname
\ps n
\ge igname
\dn deux sortes : blanche ou jaunâtre
\dt 08/Feb/2025

\lx kû-jaa
\dialx GOs
\is discours
\ps v
\ge dire ; avertir ; prévenir
\xv lhi kû-jaa nye ẽnõ
\xn ils préviennent ce garçon
\xv lhò xa kû-jaa-li
\dialx GO
\xn ils leur annoncent (à eux deux)
\ng forme courte de |lx{khõbwe ça}
\gt dire à
\dt 21/Mar/2023

\lx ku-jaaò
\ph kuɲɟa:ɔ
\dialx GOs
\va ku-jaaòl
\dialx BO
\is topographie
\ps n
\ge amont du fleuve
\dt 24/Jan/2022

\lx ku-kevwa
\dialx GO
\va ku-kepwha
\dialx GO
\va ku-kewang
\dialx PA BO
\is topographie
\ps n
\ge haut de la vallée ; talweg
\cf ku-
\ce bout ; extrémité d'une surface étendue
\dt 22/Feb/2025

\lx ku-ko
\dialx GOs PA BO
\is igname
\ps n
\ge igname
\dn clone ; ressemble à une tête de poule
\dt 08/Feb/2025

\lx ku-kue
\dialx GOs
\is position
\ps v
\ge debout en portant (bébé)
\xv e ku-kue ẽnõ
\xn elle est debout portant le bébé
\dt 28/Jan/2018

\lx ku-khiai
\dialx GOs
\va ku-xiai
\dialx GO(s)
\is feu
\ps v
\ge réchauffer (se) debout près du feu
\dt 23/Aug/2021

\lx kul
\dialx BO
\va kuul
\dialx BO PA
\is action_eau_liquide_fumée
\ps v.i
\ge couler ; répandre (se) ; vider (se)
\xv i kul (a) we
\dialx PA
\xn l'eau a inondé
\dt 03/Feb/2025

\lx kula
\dialx GOs PA BO
\sn 1
\is crustacés
\ps n
\ge crevette
\se kula ni we-za
\dialx GO BO
\sge langouste
\se kula nõgo
\dialx GO
\sge crevette
\sn 2
\is jeu
\ps n
\ge figure du jeu de ficelle ("la crevette")
\et *quɖa(ŋ), *quraŋ
\el POc
\dt 15/Sep/2021

\lx kula we ni ki
\dialx GOs
\is santé
\ps v
\ge dysenterie (avoir la) ; diarrhée (avoir la)
\dn (lit. l'eau coule)
\dt 08/Feb/2025

\lx kula-be
\dialx GOs BO
\is crustacés
\ps n
\ge langouste
\dn de rivière, grosse et noire
\dt 08/Feb/2025

\lx kulaçe
\ph kuladʒe
\dialx GOs
\va kulaye
\dialx PA
\sn 1
\is santé
\ps v.stat
\ge raide (être) ; courbaturé
\xv kulaçe wa ne hii-nu
\dialx GO
\xn mes tendons de bras sont raides/endoloris
\sn 2
\is caractéristiques_objets
\ps v.stat
\ge dur (pain)
\dt 22/Feb/2025

\lx kula-kaze
\dialx GOs
\is crustacés
\ps n
\ge langouste
\dt 29/Jan/2019

\lx kule
\dialx GOs PA
\va kole, kula
\sn 1
\is action_eau_liquide_fumée
\ps v
\ge verser ; répandre ; vider
\xv i kule pwal
\dialx BO
\xn il pleut
\sn 2
\is pêche
\ps v
\ge déployer ; étendre (filet)
\dt 26/Aug/2021

\lx kulèng
\dialx BO WE
\is caractéristiques_personnes
\ps v
\ge fou ; imbécile
\ge saoul
\se a-kulèng
\dialx BO
\sge un fou
\dt 24/Feb/2025

\lx kuli
\dialx GOs
\va kule
\dialx PA BO
\is plantes_processus
\ps v.t.
\ge perdre ses feuilles
\xv la kuli dròò-cee
\dialx GO
\xn ils perdent leurs feuilles
\xv yeewa kuli dròò-la
\dialx GO
\xn c'est l'époque de la chute des feuilles
\ng v.i. |lx{ku} |dialx{GOs}, |lx{kul} |dialx{PA}
\gt tomber
\et *aqulu
\eg tombé, détaché
\el POc
\dt 06/Jan/2022

\lx kulò
\dialx GOs BO
\is préparation_aliments
\ps n
\ge couverture du four
\dn faite en écorce de niaouli ou de bananier
\dt 08/Feb/2025

\lx kumala
\dialx GOs BO PA
\va kumwala
\dialx BO PA
\is plantes
\ps n
\ge patate douce
\sc Ipomœa batatas
\scf Convolvulacées
\se kumala kari
\sge patate douce jaune
\se kumala èrona
\sge clone de patate douce
\dt 26/Jan/2022

\lx ku-manyô
\dialx GOs
\is igname
\ps n
\ge igname
\dn se ramifie comme le manioc
\dt 08/Feb/2025

\lx kumè
\dialx GOs BO PA
\sn 1
\is corps
\ps n
\ge langue
\xv kumèè-je
\dialx GO
\xn sa langue
\xv kûmè-n, kûmèè-n
\dialx PA
\xn sa langue
\ng allongement optionnel quand le nom est déterminé
\sn 2
\is plantes_partie
\ps n
\ge bourgeon
\xv kumèè-je
\dialx GO
\xn son bourgeon
\xv kumèè-n
\dialx PA
\xn son bourgeon
\se kumè ê
\dialx GO
\sge bourgeon de canne à sucre
\dt 13/Jan/2022

\lx kumee
\dialx GOs BO
\is arbre
\is plantes_partie
\ps n
\ge cime (arbre)
\se kumee-cee
\sge la cime de l'arbre
\dt 08/Nov/2021

\lx kumèè
\dialx GOs
\is plantes_partie
\ps n
\ge bourgeon ; pousses (toutes plantes)
\ge jeunes feuilles
\dn qui commencent à se déployer
\dt 17/Feb/2025

\lx kumèè chaamwa
\dialx GOs PA
\is bananier
\ps n
\ge cœur du bananier
\dt 13/Jan/2022

\lx kumèè nu
\dialx GO PA BO
\is arbre_cocotier
\ps n
\ge cœur de cocotier
\dn comestible
\dt 08/Feb/2025

\lx kumèè thoomwã
\dialx BO
\is plantes
\ps n
\ge "langue de femme"
\dn petit buisson aux feuilles épaisses et succulentes [Corne]
\sc Kalanchoëe pinnata
\scf Crassulacées
\dt 30/Aug/2021

\lx kumõõ
\dialx GOs
\is poisson
\ps n
\ge rouget
\sc Parupeneus spilurus
\scf Mullidés
\dt 27/Aug/2021

\lx kun
\dialx PA BO
\va kunõng
\dialx BO
\va ku
\dialx GO(s)
\sn 1
\is nom_locatif
\ps n
\ge endroit
\xv na ni kun-òli
\dialx PA GO
\xn dans cet endroit-là
\xv na ni kuni-m, na ni kuna-n
\dialx PA
\xn à ta place
\xv bo ca e ra Teã-ma na ni la kuna-n
\dialx PA
\xn la roussette est le Grand Chef dans ses endroits
\xv na ni kunõ-ny
\dialx BO
\xn dans mon endroit
\se phe kunã
\sge prendre la place de qqn.
\sn 2
\is société_organisation
\ge ensemble des clans formant la tribu |dialx{PA}
\dn |lx{kun} contient le |lx{phwe-meewu}
\dt 21/Feb/2025

\lx kun-êba
\dialx GOs
\is grammaire_direction
\ps LOC.DIR
\ge là sur le côté ; à cet endroit latéralement
\dt 16/Aug/2021

\lx kun-êda
\dialx GOs
\is grammaire_direction
\ps LOC.DIR
\ge là en haut ; à cet endroit en haut
\dt 16/Aug/2021

\lx kun-êdu
\dialx GOs
\is grammaire_direction
\ps LOC.DIR
\ge là en bas ; à cet endroit en bas
\dt 16/Aug/2021

\lx kuni-m
\dialx BO
\is nom_locatif
\ps n.loc
\ge là de ton côté
\nt selon BM
\dt 03/Feb/2025

\lx kuńô
\ph kunõ
\dialx GOs
\is insecte
\ps n
\ge asticot
\et *qulos
\el POc
\dt 29/Jan/2019

\lx ku-nõgo
\ph kuɳɔ̃ŋgo
\dialx GOs
\va ku-nõgò
\dialx BO PA
\is topographie
\ps n
\ge amont de la rivière ; source de la rivière
\cf phwè-nõgò
\ce source
\dt 21/Mar/2023

\lx kun-òli
\ph kuɳɔli
\dialx GOs
\va kun-òli
\dialx PA
\is nom_locatif
\ps n.loc
\ge de l'autre côté là-bas
\dn de la montagne, de la rivière
\xv nõõli je mwa xa ge êba kun-òli nani nõgò
\dialx GO PA
\xn regarde la maison qui est là-bas de l'autre côté de la rivière
\dt 08/Feb/2025

\lx ku-pe-bala
\ph kuβebala
\dialx GOs
\va ku-vwe-bala
\dialx GO(s)
\is position
\ps v
\ge aligné ; côte à côte
\se ku-vwe-bala
\sge debout en ligne ou côte à côte
\se tre-vwe-bala
\sge assis en ligne
\cf pe-bala
\ce équipe
\dt 22/Feb/2025

\lx ku-peenã
\dialx GOs PA BO
\is igname
\ps n
\ge igname
\dn pousse comme une anguille
\dt 08/Feb/2025

\lx ku-poxè
\dialx PA
\is mouvement
\ps v
\ge rassembler (se)
\dn (lit. debout-un)
\cf tree-poxè
\ce assis ensemble
\dt 08/Feb/2025

\lx ku-phõ
\dialx GOs
\va ku-phõng
\dialx BO WEM WE
\is position
\ps v
\ge debout tordu ; de travers
\xv e ku-phõ
\xn c'est tordu (d'un mur)
\xv ku-phõ duviju
\xn le clou est tordu
\dt 21/Mar/2023

\lx ku-phwa
\dialx GOs
\is topographie
\ps n
\ge haut de la vallée
\dt 28/Jan/2019

\lx kurô
\dialx BO
\is corps
\ps n
\ge clitoris
\nt selon Corne
\dt 26/Mar/2022

\lx ku-thiibu
\dialx GOs
\is position
\is action_corps
\ps v
\ge accouder (s')
\dt 20/Aug/2021

\lx ku-thua
\dialx GOs
\va kutuwa
\dialx GO(s)
\is igname
\ps n
\ge igname sauvage
\sc Dioscorea alata sauvage
\scf Dioscoréacées
\dt 27/Aug/2021

\lx kutra
\ph kuɽa, kuʈa
\dialx GOs
\va kura
\dialx BO PA
\sn 1
\is corps
\ps n
\ge sang
\xv kutraa-je ; kutraa-me
\dialx GO
\xn son sang ; notre sang
\xv kuraa-n
\dialx PA
\xn son sang
\sn 2
\is fonctions_naturelles
\ps v
\ge saigner
\xv e mõlò kuraa-je ; e mõlò kuraa-nu
\dialx GO
\xn il est nerveux (lit. son sang est vivant) ; je suis nerveux
\et *ɖaRaq
\el POc
\dt 22/Feb/2025

\lx kutru
\ph kuʈu, kuɽu
\dialx GOs
\va kuru
\ph kuɽu
\dialx GO(s)
\va kuru
\dialx PA BO
\is plantes_partie
\ps n
\ge tubercule du taro d'eau |lx{uva} ; taro d'eau
\dn terme employé dans le contexte coutumier
\xv kuru mãni kui
\xn taros et ignames
\cf uva
\ce pied de taro d'eau
\dt 27/Mar/2022

\lx ku-thralò
\dialx GOs
\is interaction
\ps v
\ge provoquer (se)
\dt 27/Jan/2019

\lx kûû
\hm 1
\dialx PA BO
\va kû
\dialx PA BO
\is nourriture
\ps v
\ge croquer (fruits, légumes verts)
\ge manger (des végétaux, fruits)
\xv nu kû orâ
\dialx PA
\xn je mange des oranges
\xv kû-ny orâ
\dialx BO
\xn ma part d'oranges
\ng v.t. |lx{kûûńi}
\dt 05/Jan/2022

\lx kûû
\hm 2
\dialx GOs
\sn 1
\is grammaire_aspect
\ps v
\ge terminer
\sn 2
\is action
\ps v
\ge (r)emplir
\ge envahir (de peur)
\xv e kûû-je xo hããxa
\xn il est envahi par la peur
\xv e kûû-je xo nhyaru
\xn il est envahi par la gale/les plaies
\ng v.t. |lx{kûûni}
\gt remplir; envahir
\dt 22/Feb/2025

\lx kûû-
\dialx GOs PA BO
\is classificateur_possessif_nourriture
\ps CLF.POSS
\ge part de fruits ou de feuilles
\xv kûû-nu pò-mã
\dialx GO
\xn ma mangue
\xv kûû-n
\dialx PA
\xn sa part
\xv kûû-m (e) cin |ph{kû:məcin}
\dialx PA
\xn ta part de papaye
\cf caa- ; cè- ; ho-
\dt 10/Jan/2022

\lx kuue
\dialx GOs
\is action_corps
\ps v
\ge tenir ; retenir
\dn par ex. tenir un animal par une bride ou une corde
\dt 22/Feb/2025

\lx kuuni
\hm 2
\ph ku:ɳi
\dialx GOs BO PA
\is grammaire_aspect
\ps v.t.
\ge achever; aboutir ; accomplir
\xv nu kuuni nyâ thòm
\dialx BO
\xn j'ai achevé cette natte
\xv nu kuuni hõbwoli-nu
\dialx GO
\xn j'ai achevé ma robe
\xv ce mõgu i yõ ce mõ u pe-nee-kuuni
\dialx GO
\xn notre tâche, nous l'avons menée à terme ensemble
\se ba-kuuni-xo
\sge la fin
\dt 05/May/2024

\lx kûûńi
\hm 1
\ph kû:ni  
\dialx GOs PA
\va kôôni
\dialx BO
\is nourriture
\ps v
\ge manger (des fruits, feuilles)
\cf hovwo
\ce manger (général)
\dt 17/Feb/2025

\lx kuvêê
\dialx PA
\va kuveen
\dialx BO
\is plantes_partie
\ps n
\ge bourgeon ; pousses (toutes plantes)
\ge jeunes feuilles
\dn qui commencent à se déployer
\dt 17/Feb/2025

\lx kuvêê nu
\dialx PA
\is arbre_cocotier
\ps n
\ge coco germé
\dt 29/Jan/2019

\lx kuvêê-uvwia
\dialx PA
\va kuvêê-uvhia
\dialx PA
\is taro
\ps n
\ge jeunes feuilles de taro de montagne
\dt 21/Mar/2023

\lx ku-wãga
\dialx GOs
\is position
\ps v
\ge debout jambes écartées
\dt 26/Jan/2018

\lx kuwe
\dialx GOs BO
\is igname
\ps n
\ge igname (violette)
\dt 17/Feb/2025

\lx ku-weê
\dialx GOs
\is igname
\ps n
\ge igname
\dn au goût sucré
\dt 08/Feb/2025

\lx ku-xea
\dialx GOs
\va ku-xia
\dialx GO(s)
\is position
\ps v
\ge adosser (s') ; adossé
\xv nu ku-xea ni gòò-mwa
\xn je suis adossé au mur
\dt 23/Jan/2022

\lx kûxû
\hm 1
\dialx GOs
\va kûû
\dialx PA BO
\va kûkû
\dialx BO
\is fonctions_naturelles
\ps v
\ge téter ; sucer
\xv e pa-kûxû-ni pòi-je
\dialx GO
\xn elle allaite son enfant
\xv e pa-kûûe pòi-n
\dialx PA
\xn elle allaite son enfant
\se pa-kûxû-ni
\sge allaiter (v.t.)
\dt 24/Feb/2025

\lx kûxû
\hm 2
\dialx GOs
\va kûxûl
\dialx BO PA
\is son
\ps v
\ge grogner ; gronder ; grommeler ; maugréer ; bougonner
\dt 26/Jan/2019

\lx kûxû
\hm 3
\dialx GOs
\is poisson
\ps n
\ge carangue (juvénile)
\dn selon les locuteurs, elle est appelée ainsi parce qu'elle murmure (|lx{kûxû}) quand on la sort de l'eau
\cf dròò-xibö
\ce carangue (la même, de taille moyenne)
\dt 10/Jan/2022

\lx ku-yabo
\dialx GOs
\va ku-yòbo
\dialx BO PA
\is action
\ps v
\ge attendre
\xv çö ku-yabo-nu ?
\dialx GOs
\xn tu m'attends ?
\xv ku-yòbo-nu !
\dialx PA
\xn attends-moi !
\xv i yòboe êgu
\dialx BO
\xn il attend quelqu'un
\xv yu ku-yòboe u i thi
\dialx BO
\xn tu attends que cela pousse
\dt 18/Dec/2021

\lx kuzaò
\dialx GOs
\va kuraò
\dialx WEM WEH
\is grammaire_quantificateur_mesure
\is grammaire_quantificateur_degré
\ps QNT
\ge trop (en) ; en surplus
\xv e tree-kuzaò po-kò
\xn il en reste trois en plus
\xv e tree-kuzaò haa-tru mãdra
\xn il reste deux pièces de tissu en plus
\dt 16/Feb/2025

\lx k(h)a-poxe
\dialx GOs
\is grammaire_distributif
\ps DISTR
\ge un par un
\xv hivwi k(h)a-poxe!
\xn ramasse-les un par un !
\dt 14/Aug/2021

\lx kha
\hm 1
\dialx GOs PA BO
\va kha(a)
\dialx PA
\is préfixe_sémantique_déplacement
\ps PREF
\ge faire qqch. en se déplaçant ou en étant en mouvement
\ge déplacer (se)
\xv nu phe ci-xãbwa po nu kha-hãbira
\dialx GO
\xn je prends l'étoffe et je l'emporte sous le bras
\xv me kha-thu-hinõõ ca ni mwã la xa trabwa êne
\dialx GO
\xn nous ferons un petit signe (d'au revoir) à ceux qui sont assis là
\xv çö kha-kai-çö Treã ma Mozau
\dialx GO
\xn (tu te déplaces) tu tournes le dos à tes fils Teã et Mozau
\xv e a-du õã-li xa u kha-nõõli
\dialx GO
\xn leur mère descend et aperçoit en même temps
\xv bî nee kha-gi vwo me za a-è
\dialx GO
\xn nous, on pleurait souvent quand on partait (à l'école)
\xv kêbwa na çö nee vwo kha-chelee-ni gòò-nu
\dialx GO
\xn il ne faut pas que tu lui fasses toucher le tronc du cocotier (en même temps qu'il descend)
\xv mhaza e u kha-tòè hii-je, novwö mhaza kha-êgi ã=e, 
\dialx GO
\xn juste au moment où elle se penche et tend (lit. lance) son bras, juste au moment où elle va l'attraper (il s'échappe)
\xv bî za xa trêê, bî za xa kha-tròòli, nu kha-tòè hii-nu
\dialx GO
\xn nous avons couru, nous les avons rattrapés en courant (et) j'ai tendu le bras (pour l'attraper) 
\xv e kha-tho
\dialx GOs
\xn elle appelle (en se déplaçant)
\xv e u tre kha-nõõli na-bòli
\dialx GOs
\xn elle les a déjà aperçus loin là-bas (en se déplaçant)
\xv e ra u yaoli, novwö ne i alavwu novwö ne i alavwu ce kha-thi-du hii-n ni ke-mhõ, ka hovwo nee wã
\dialx WEM
\xn elle se balance, quand elle a faim, elle plonge la main dans le panier de restes et mange en faisant comme ça
\xv lhi kha-thaba jè-ò pòi-li
\dialx PA
\xn ils portent en marchant leur enfant
\xv i kha kule kile
\dialx PA
\xn il a laissé tomber (perdu) sa clé en marchant
\xv i kha wal
\dialx PA
\xn il chante en marchant
\xv i khaa gi
\dialx PA
\xn il pleure en marchant
\xv i kha cö-da
\dialx BO
\xn il continue à monter
\ng indique une action faite en se déplaçant; en association avec un verbe de perception, le préfixe peut indiquer 
une perception non voulue
\dt 24/Feb/2025

\lx kha
\hm 2
\dialx GOs PA
\va ka
\dialx GOs PA
\is grammaire_aspect
\ps PERM
\ge qui a une propension ; qui a une propriété permanente caractéristique
\xv e nee kha-maaçe-ni
\dialx GO
\xn il le fait toujours lentement
\xv e a-kha-gi
\xn c'est un pleurnicheur ; il pleure pour un rien ; il est sensible
\xv e a-kha-thô
\xn elle est susceptible ; elle se met en colère pour rien ; elle est coléreuse
\xv e a-kha-mããni
\xn il dort sans cesse/facilement
\xv e a-kha-phorõ
\dialx GO
\xn il perd la mémoire
\dt 24/Feb/2025

\lx kha
\hm 3
\dialx GOs
\va khan
\dialx PA BO
\is cultures_champ
\ps n
\ge champ ; culture en forêt défrichée
\dt 07/May/2024

\lx kha
\hm 4
\dialx PA BO
\is caractéristiques_objets
\ps n
\ge fente ; craquelure ; craquelé ; fissuré (terre)
\dt 07/May/2024

\lx kha-
\dialx GOs PA BO
\va ka-
\is grammaire_distributif
\ps DISTR
\ge chaque ; chacun (+ numéral)
\xv na vwo k(h)a-po-tru
\dialx GO
\xn dispose-les par deux
\xv na vwo k(h)a-po-tu
\dialx PA
\xn dispose-les par deux
\xv na pe-k(h)a-po-xè, pe-k(h)a-po-tru, etc.
\dialx GO
\xn mets-les par un, par deux
\xv na wo k(h)a-po-tru po-mã cai êgu k(h)a-a-xè
\dialx GO
\xn donne les mangues par deux à chacune des personnes
\xv na cani êgu k(h)a-a-xè k(h)a-po-tru po-mã
\dialx GO
\xn donne à chaque personne deux mangues chacun
\xv li vara k(h)a-we-xè loto
\dialx GO
\xn chacun des deux a une voiture
\xv mo pe-k(h)a-a-niza na ni ba ?
\dialx GO
\xn nous (paucal) sommes combien par/dans chaque équipe ?
\se ka-õxe
\dialx BO
\sge quelquefois (Dubois)
\se ka-põge
\dialx BO
\sge chaque ; chacun (inanimé) (Dubois)
\se ka-age
\dialx BO
\sge chacun (Dubois)
\se kau ka-wege
\dialx BO
\sge chaque année (Dubois)
\se õ-ge na ni kau ka-wege
\dialx BO
\sge une fois par an (Dubois)
\dt 24/Feb/2025

\lx kha pwiò
\dialx GOs
\is pêche
\ps v
\ge pêcher au filet à la traîne
\cf khai pwiò
\ce tirer le filet
\dt 04/Apr/2024

\lx khaa
\hm 1
\dialx GOs PA BO
\is eau
\ps n
\ge mare ; étang
\dt 29/Jan/2019

\lx khaa
\hm 2
\dialx GOs
\is interaction_animaux
\ps v
\ge dresser (cheval)
\dt 27/Jan/2019

\lx khaa
\hm 3
\dialx GOs PA BO
\wr A
\is action_corps
\ps v
\ge appuyer ; écraser
\ge tasser (avec les mains ou les pieds)
\ge masser ; presser
\xv e khaa-du hõbwò
\dialx GO
\xn elle fait tremper le linge (en appuyant)
\xv e khaa-du bwa-je mwã nani we
\dialx GO
\xn (elle) lui enfonce la tête sous l'eau
\se khaa !
\dialx PA
\sge accélère ! (lit. appuie)
\se ya-khaa
\dialx GO
\sge lampe électrique (lit. feu-appuyer)
\wr B
\is préfixe_sémantique_action
\ps PREF
\ge action faite en appuyant avec le pied ou la main
\xv la khaa pu-mwa
\dialx GO
\xn ils font le mur de la maison en torchis
\dt 08/Feb/2025

\lx khaa-bîni
\ph kʰa:bîɳi
\dialx GOs
\va khaa-bîni
\dialx PA
\is action_corps
\is préfixe_sémantique_action
\ps v
\ge écraser (avec le pied)
\ge aplatir
\dt 24/Jan/2022

\lx khaabole
\dialx GOs
\va khaabule
\dialx PA BO
\va kaabule
\dialx BO [BM]
\is action_eau_liquide_fumée
\ps v
\ge envahir d'eau ; inonder ; déborder ; passer par-dessus
\ge tremper ; mouiller
\dn par ex. le linge dans l'eau
\xv i khaabule wony
\dialx PA
\xn le bateau a coulé
\dt 08/Feb/2025

\lx khaabu
\dialx GOs BO
\is fonctions_naturelles
\ps v.stat
\ge froid (avoir)
\xv nu hai khaabu
\dialx BO PA
\xn j'ai très froid
\xv nu hai-xaabu
\dialx GO
\xn j'ai très froid
\se wara khaabu
\dialx BO
\sge hiver (lit. époque du froid)
\cf tuuçò, tuyong
\dt 03/Feb/2025

\lx khaa-êgo
\dialx GOs
\va khaa-pi
\dialx WE WEM
\va khaa-vwi
\dialx GO(s)
\is fonctions_naturelles_animaux
\ps v
\ge couver (des œufs)
\cf khaa-pi ; khaa-vwi
\ce pondre ; couver
\dt 22/Feb/2025

\lx khaagi
\dialx GOs
\is sentiments
\ps v
\ge retenir (se) de pleurer
\dt 27/Jan/2019

\lx khaai-mudre
\ph kʰa:i'muɖe
\dialx GOs
\va khai-mode
\dialx BO
\is grammaire_manière
\sn 1
\ps v
\ge déchirer en tirant
\xv nu khaai-mudre hõbwòli-je
\dialx GO
\xn j'ai déchiré sa robe en tirant dessus
\sn 2
\ps n ; v
\ge résolution ; résolu ; définitif (être) ; définitivement
\xv nu wedoni khai-mode
\dialx BO
\xn j'ai fermement/définitivement décidé
\dt 03/Feb/2025

\lx kha-alawe
\dialx GOs
\va kha-olae
\dialx PA
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge partir en disant au revoir
\xv i kha-olae-nu wo gèè
\dialx PA
\xn grand-mère est partie en me disant au revoir
\dt 19/Feb/2025

\lx khaa-pi
\dialx GOs
\va khaa-vwi
\ph kʰa:βi
\dialx GOs
\is fonctions_naturelles_animaux
\ps v
\ge pondre
\cf khaa-êgo ; thu êgo
\ce pondre ; couver
\dt 22/Feb/2025

\lx khaa-tia
\dialx GOs
\va khaa-zia
\dialx GOs
\is action_corps
\is préfixe_sémantique_action
\ps v
\ge pousser ; bousculer qqn.
\xv e khaa-tia nu
\xn il m'a poussé
\cf tia
\ce pousser
\dt 21/Mar/2023

\lx kha-axe
\dialx GOs
\is grammaire_distributif
\ps DISTR
\ge chacun(e)
\xv li za kha-a-xe chòvwa vwö li za u a
\xn ils ont chacun un cheval pour partir
\dt 20/Dec/2021

\lx kha-bazae
\ph kʰa'baðae
\dialx GOs
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge dépasser en se déplaçant
\xv e kha-bazae-çö
\xn il t'a dépassé
\dt 24/Jan/2022

\lx khabe
\ph kʰabe
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge dresser (poteau, etc.)
\ge taper pour enfoncer (poteau)
\ge enfoncer
\sn 2
\is maison
\ps v
\ge construire ; édifier (maison)
\xv la pe-zage u la khabe nye mwa
\dialx PA
\xn ils s'entraident pour construire cette maison
\sn 3
\is société_organisation
\ps v
\ge établir ; instituer
\dt 22/Feb/2025

\lx khabe nõbu
\ph kʰabe ɳɔ̃bu
\dialx GOs BO
\is religion
\ps v
\ge mettre un tabou
\xv e khabe nõbu
\xn il a planté une perche d'interdiction
\xv nõbu-ã
\xn nos lois
\cf phu nõbu
\ce enlever un interdit
\dt 15/Aug/2021

\lx kha-bilòò
\ph 'kʰabilɔ:
\dialx GOs
\va ka-bilòò
\is action_corps
\ps v
\ge tordre (se) (doigt, cheville)
\ge fouler (se) (pied, cheville)
\xv e k(h)a-bilòò kòò-je
\xn il s'est foulé le pied
\dt 20/Mar/2023

\lx khaboi
\dialx BO
\is action_corps
\ps v
\ge étendre la main horizontalement
\dn comme pour tapoter
\nt selon Corne
\dt 08/Feb/2025

\lx khabwa
\dialx GOs
\is eau_marée
\ps n
\ge marée descendante
\dt 29/Jan/2019

\lx kha-bwaroe
\dialx GO PA
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge déplacer (se) en portant dans les bras
\xv nye e za thrõbo kha-bwaroe na khazia gòò-nu
\dialx GO
\xn lui, il descend en le portant tout près du tronc du cocotier
\xv jo ra kha-bwaroe-du-mi tha-ò mwa ni puu-n
\dialx PA
\xn apporte-le en bas dans tes bras ici jusqu’au pied (de l’arbre)
\dt 24/Jan/2022

\lx kha-çaaxò
\ph kʰa'ʒa:ɣɔ, kʰa'dʒa:ɣɔ
\dialx GOs
\va kacaaò
\dialx BO
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge marcher sans bruit ; déplacer (se) doucement
\xv e kha-çaaxò-da
\dialx GO
\xn il monte sans bruit
\xv kha-çaaxò mã mããni
\dialx GOs BO
\xn doucement car (il) dort
\cf ku-çaaxò
\ce en se cachant
\dt 24/Jan/2022

\lx khaçani
\ph kʰa'ʒaɳi
\dialx GOs
\is oiseau
\ps n
\ge hirondelle des grottes ; martinet soyeux
\sc Collocalia esculenta uropygialis
\scf Apodidés
\gb Glossy Swiftlet
\dt 21/Mar/2023

\lx kha-cimwî
\dialx PA
\is action_corps
\is préfixe_sémantique_déplacement
\ps v
\ge saisir en se déplaçant
\dn en emportant ou amenant
\xv kha-cimwî je-nã poxa kui !
\xn venez prendre cette petite igname
\xv i kha-cimwî-mi je-nã kui !
\xn il arrive en apportant cette igname
\xv i kha-cimwî-ò je-nã kui !
\xn il part en emportant cette igname
\dt 08/Feb/2025

\lx khaçò
\ph kʰadʒɔ
\dialx GOs
\va kayòl
\dialx WEM WE PA BO
\is habitat
\ps n
\ge cimetière
\dt 21/Mar/2023

\lx kha-çöe
\ph kʰa'ʒωe
\dialx GOs
\va khaa-jöe
\dialx PA
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge traverser à pied (une route)
\dt 24/Jan/2022

\lx kha-da
\dialx GOs PA
\va ka-da
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge monter à pied
\ge grimper (en marchant)
\se ba-ka-da
\dialx PA
\sge échelle
\dt 21/Mar/2023

\lx khagebwa
\dialx GOs
\va khagebwan
\dialx PA
\is interaction
\ps v
\ge laisser
\ge abandonner
\ge rejeter
\xv nu u khagebwan kee-ny na Pum
\dialx PA
\xn j'ai laissé mon panier à Poum
\xv haxe li kala khagebwan mwã la kera
\dialx PA
\xn elles partent en laissant leurs paniers
\dt 25/Dec/2021

\lx khãgee
\dialx GOs
\va keege
\dialx BO [BM]
\is interaction
\ps v
\ge laisser
\ge quitter
\xv va khãgee-wa
\xn nous vous quittons
\dt 21/Mar/2023

\lx kha-hêgo
\dialx GOs PA
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge marcher avec une canne
\xv i kha-hêgo
\dialx PA
\xn il marche (appuie) avec une canne
\dt 08/Sep/2024

\lx kha-hoze
\ph kʰa'hoze
\dialx GOs
\va a-hoze
\dialx GO(s)
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge suivre ; longer à pied
\xv e kha-hoze kòli we-za
\xn il longe le bord de la mer
\dt 24/Jan/2022

\lx khai
\hm 1
\ph kʰai
\dialx GOs PA
\is action_corps
\is armes
\ps v
\ge tirer à l'arc
\xv la pe-khai
\dialx PA
\xn ils tirent chacun de leur côté (compétition)
\dt 04/Apr/2024

\lx khai
\hm 2
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge retirer qqch.
\xv la kha chaamwa
\dialx PA
\xn ils coupent un régime de bananes
\xv la khai-bulu-ni nhye wony
\dialx PA
\xn ils tirent tous ensemble ce bateau (sur la grève)
\se khai muge
\dialx BO
\sge casser en tirant
\se khai pwiò
\sge (re)tirer le filet
\an tia
\at pousser
\sn 2
\is temps
\ps v
\ge fixer (date)
\xv i khai tèèn
\dialx PA
\xn il fixe une date
\xv i khai kai-je tèèn
\dialx BO
\xn il fixe une date de retour
\et *thaki
\el PSO
\ea Geraghty
\dt 04/Apr/2024

\lx kha-ku
\dialx GOs
\va kha-kule
\dialx PA
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge tomber en se déplaçant
\xv e kha-ku kile
\dialx GO
\xn la clé est tombée (alors qu’il marchait)
\xv i khaa-kul (e) kile
\dialx PA
\xn la clé est tombée (alors qu’il marchait)
\dt 06/May/2024

\lx kha-khibwaa
\dialx GOs
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge prendre un raccourci
\xv nu kha-khibwaa dè
\xn j'ai pris un raccourci
\xv nu phe dè-khibwaa
\xn j'ai pris un raccourci
\dt 24/Jan/2022

\lx kha-khööńe
\ph kʰa'kʰω:ne
\dialx GOs
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge marcher avec une charge sur le dos
\dt 21/Mar/2023

\lx khalu
\dialx GOs BO
\is topographie
\ps n
\ge creux ; dépression sur un terrain
\xv çö thu-mhenõ, çö phwêne mã pò khalu
\xn quand tu te promènes, fais attention car il y a un petit creux
\dt 24/Feb/2025

\lx kham
\dialx PA BO
\va khã
\dialx GO(s)
\is mouvement
\ps v
\ge ricocher
\ge effleurer
\ge éviter
\ge rater ; manquer
\dt 25/Aug/2021

\lx kha-maaçee
\ph kʰama:dʒe:
\dialx GOs
\sn 1
\is caractéristiques_personnes
\ps v.stat
\ge lent
\xv êgu xa a-kha-maaçee
\dialx GO
\xn c'est quelqu'un de lent
\sn 2
\is grammaire_adverbe
\ps ADV
\ge lentement
\xv nee kha-maaçe-ni
\dialx GOs
\xn fais-le lentement
\xv cèni kha-maaçe-ni cè-çö lai
\dialx GOs
\xn mange ton riz lentement
\ng forme transitive: |lx{V-maaçe-ni} dans une construction à verbe complexe
\cf maaça
\dt 03/Feb/2025

\lx kha-mudree
\dialx GOs
\va kha-mude
\dialx PA
\is action_corps
\is préfixe_sémantique_déplacement
\ps v
\ge déchirer (en se déplaçant)
\xv e kha-mudree hõbwòli-je xo thîni
\dialx GO
\xn la barrière a déchiré sa robe
\xv i kha-mude hõbwòli-n na bwa thîni
\dialx PA
\xn elle a déchiré sa robe sur la barrière
\dt 26/Jan/2022

\lx kha-nhyale
\dialx GOs
\is action_corps
\is préfixe_sémantique_action
\ps v
\ge écraser avec le pied
\dt 26/Jan/2022

\lx kha-phe
\ph 'kʰapʰe
\dialx GOs PA BO
\va kha-vwe
\ph kʰaβe
\dialx GO(s)
\wr A
\is action_corps
\is préfixe_sémantique_déplacement
\ps v
\ge emporter
\ge prendre ; saisir (en partant)
\xv kha-phe kôgòò hovwo
\dialx GO
\xn emporte les restes de nourriture
\xv e zòò xa a kha-vwe cee xo kani
\dialx GO
\xn le canard nage et porte le (bout de) bois
\xv kha-phe-mi !
\dialx GO
\xn apporte-le !
\xv zò kha-phe doo mãni bulaivi
\dialx PA
\xn vous (duel) prendrez vos sagaies et casse-têtes
\se trêê kha-vwe-mi
\dialx GO
\sge apporter ici en courant (lit. courir-prendre-vers.ici)
\wr B
\is grammaire_préposition
\ps PREP
\ge avec ; ensemble
\dt 24/Feb/2025

\lx khara-a nu
\dialx PA BO
\is arbre_cocotier
\ps n
\ge fibre de palme de cocotier
\dn prélevée sur la nervure centrale de la palme, elle sert de lien
\dt 08/Feb/2025

\lx kharu-mhwêê
\dialx GO
\va kharu-mhween
\dialx PA BO
\va kharu-mhween
\is action_eau_liquide_fumée
\ps v
\ge flotter (emporté par l'eau) ; échouer (sur la grève)
\dt 27/Aug/2023

\lx kha-thi
\ph kʰatʰi
\dialx GOs
\is santé
\is préfixe_sémantique_action
\ps v
\ge boiter ; boiteux
\dt 25/Mar/2022

\lx kha-thixò
\ph kʰa'tʰiɣɔ
\dialx GOs
\va kha-thixò
\dialx PA
\sn 1
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge marcher sur la pointe des pieds |dialx{GOs}
\dn (lit. appuyer piquer pied)
\sn 2
\is santé
\ps v
\ge boiteux ; boiter |dialx{PA}
\xv i kha-thixò
\xn il marche en boitant
\dt 08/Feb/2025

\lx kha-tho
\ph kʰatʰo
\dialx GOs
\is interaction
\is préfixe_sémantique_déplacement
\ps v
\ge appeler en se déplaçant
\xv e kha-tho
\dialx GO
\xn elle appelle (en marchant)
\dt 07/May/2024

\lx kha-thu-heela
\dialx GOs PA BO
\va kha-ru-heela
\is mouvement
\ps v ; n
\ge déraper
\ge glisser sur une glissoire
\ge glissoire 
\dn aménagée par les enfants sur une pente mouillée
\cf heela
\ce glisser
\dt 08/Feb/2025

\lx kha-thu-hinõõ
\dialx GOs
\is mouvement
\ps v ; n
\ge faire un geste d'au revoir en partant
\xv me kha-thu-hinõõ cani mwã lha xa hla trabwa êne
\xn nous ferons un petit signe (d'au revoir) à certains qui sont assis là
\dt 19/Feb/2025

\lx kha-tree-çimwi
\ph kʰa:ʈe:ʒimwi
\dialx GOs
\is action_corps
\is préfixe_sémantique_déplacement
\ps v
\ge attraper (en déplacement)
\ge rejoindre ; rattraper qqn.
\xv la thrêê kha-tree-çimwi-la
\xn ils courent pour les attraper
\dt 21/Mar/2023

\lx kha-trilòò
\ph kʰa'ʈilɔ:
\dialx GOs
\is interaction
\ps v
\ge demander la permission
\xv çö kha-trilòò ?
\xn tu as bien/vraiment demandé la permission ?
\dt 12/Feb/2025

\lx kha-trivwi
\dialx GO
\is préfixe_sémantique_déplacement
\ps v
\ge tirer en se déplaçant
\xv bî threi xa cee vwö e kha-trivwi xo je
\dialx GO
\xn nous coupons un bout de bois pour qu'il (me) tire en se déplaçant
\dt 10/Mar/2023

\lx kha-tròòli
\ph kʰaʈɔ:li
\dialx GOs
\is préfixe_sémantique_déplacement
\ps v
\ge rencontrer par hasard ; rattraper
\xv bî za xa kha-tròòli-la
\dialx GO
\xn nous les avons rattrapés aussi en courant
\dt 21/Mar/2023

\lx kha-thrõbo
\ph kʰa'ʈʰɔ̃bo
\dialx GOs
\is déplacement
\is préfixe_sémantique_déplacement
\ps v
\ge descendre en marchant
\dt 24/Jan/2022

\lx khau
\dialx GOs PA BO
\sn 1
\is mouvement
\ps v
\ge passer par-dessus
\ge enjamber
\xv e khau bwa thîni
\dialx GOs
\xn elle a enjambé la barrière
\xv xa e pe-phu khau wãã
\dialx GOs
\xn et il volait au-dessus (de moi) ainsi
\sn 2
\is déplacement
\ps v
\ge franchir (une montagne, un col)
\sn 3
\is interaction
\ps v
\ge transgresser (interdit)
\dt 24/Feb/2025

\lx khaû
\dialx GO PA BO [BM]
\is portage
\ps v
\ge transporter
\xv i khaû paa
\xn il transporte des pierres
\ng v.t. |lx{khaûne}
\gt transporter qqch.
\dt 21/Feb/2025

\lx khau-da
\ph kʰaunda
\dialx GOs PA BO
\is déplacement
\ps v
\ge passer par-dessus (montagne) ; passer d'une vallée à l'autre
\ge enjamber
\dt 24/Feb/2025

\lx khau-ni
\ph kʰauɳi
\dialx GOs PA BO
\sn 1
\is déplacement
\ps v.t.
\ge passer par-dessus (montagne)
\sn 2
\is interaction
\ps v.t.
\ge transgresser (règle, interdit)
\xv la khau-ni nõbu
\xn ils ont transgressé l'interdit
\xv e khau-ni nya khõbwe xo tagaza
\dialx GOs
\xn il a transgressé ce qu'a dit le docteur
\dt 24/Feb/2025

\lx khawali
\dialx GOs PA
\is caractéristiques_objets
\ps v.stat
\ge long (verticalement) ; haut
\xv e khawali-je
\dialx GO
\xn il/elle est grand(e)
\xv e khawali na ni mwa
\dialx GO
\xn il est plus haut que la maison
\xv e khawali nai nu
\dialx GO
\xn il est plus grand que moi
\xv e pò khawali trûã nai çö
\dialx GO
\xn il est un peu plus grand que toi
\cf phwawali
\ce long (horizontalement)
\dt 17/Feb/2025

\lx kha-whili
\ph kʰa'wʰili
\dialx GOs BO
\is action
\is préfixe_sémantique_déplacement
\ps v
\ge traîner (un cheval) ; emmener (personne)
\xv jo kha whili-je-mi
\dialx BO
\xn conduis-la ici
\dt 26/Jan/2022

\lx khazia
\dialx GOs
\va karia
\dialx PA BO
\va khatia
\dialx BO
\is grammaire_préposition_locative
\ps PREP.LOC
\ge près (être) ; auprès ; à côté de
\xv ã-mi khazia nu
\dialx GO
\xn viens à côté de moi
\xv trabwa khazia nu !
\dialx GO
\xn assieds-toi à côté de moi
\xv ge je khazia nu
\dialx GO
\xn assieds-toi à côté de moi
\xv khazia-çö
\dialx GO
\xn auprès de toi
\xv nu kônõ khazia nye cee
\dialx GO
\xn je suis allongé près de cet arbre
\dt 08/Nov/2021

\lx khee
\hm 1
\dialx GOs
\sn 1
\is action_eau_liquide_fumée
\ps v
\ge écoper
\xv e khee we
\xn il écope l'eau
\sn 2
\is action_corps
\ps v
\ge pelleter
\dt 09/Mar/2023

\lx khee
\is pêche
\hm 2
\dialx BO
\ps v
\ge attraper (des crevettes)
\dn avec une épuisette
\xv i khee kula
\dialx BO
\xn il attrape des crevettes (avec une épuisette)
\nt selon BM
\dt 24/Feb/2025

\lx khee
\hm 3
\dialx BO
\is mouvement
\ps v
\ge chavirer sur le côté ; gîter ; penché ; couché
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx khee we
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge prendre de l'eau
\dn avec un petit récipient
\ge écoper
\se ba-khee we
\sge écope
\cf tröi
\ce puiser
\dt 08/Feb/2025

\lx khège
\is action_corps
\dialx GOs
\ps v
\ge bousculer ; basculer
\xv e pa-khège-nu ni we
\xn il m'a fait tomber dans l'eau
\se pa-khège
\sge faire tomber ; déséquilibrer
\dt 22/Feb/2025

\lx khègele
\ph kʰɛ'ŋgele
\dialx GOs PA
\va khègel
\ph kʰɛ'gɛl
\dialx PA
\is action_corps
\ps v
\ge agiter un objet contenant qqch. (qui produit un son)
\dt 24/Feb/2025

\lx khêmèni
\dialx GOs
\va khemèn
\dialx PA BO
\is action
\ps v
\ge choisir ; trier
\dt 29/Jan/2019

\lx khêni
\ph kʰẽɳi
\dialx GOs
\va kheni
\dialx PA BO
\is interaction
\ps v
\ge envoyer qqn. faire qqch. ; ordonner
\xv e khêni-ni lhã-ã yabwe i je vwö lha a vwö lha a threi cee (Treã Aholi ma Hiixe.008)
\dialx GO
\xn il envoie ses sujets pour aller couper du bois
\ng v.t. |lx{khêni-ni}
\dt 06/Aug/2023

\lx khi
\hm 1
\dialx GOs PA BO WEM
\is grammaire_aspect
\ps ASP.ponctuel
\ge un coup
\ge bref ; rapide
\xv e khi-phu
\dialx GO
\xn il vole un coup (brièvement)
\xv ra u cabòl-da-mi emãli-ò maama, ra ra u khi pha-jo-gaajòn-i Teã-ma
\dialx PA
\xn ces deux diables-là se dressent et d'un coup font sursauter Teã-mã
\xv wãã-na i ra khi mwãju Teã-mã ?
\dialx PA
\xn comment se fait-il que Teã-mã revienne si vite?
\dt 22/Feb/2025

\lx khi
\hm 2
\dialx GOs PA BO WEM
\is grammaire_modalité
\ps ATTEN (ordre poli)
\ge un peu |dialx{GOs}
\xv khi ã-mi nõõli !
\dialx GO
\xn viens voir un peu !
\xv ã-mi vwo çö khi yawe duu-nu !
\dialx GO
\xn viens un peu me gratter le dos !
\xv çö gaa khi hovwo !
\dialx GO
\xn mange un peu !
\xv khi na-mi hèlè !
\dialx PA
\xn passe-moi un peu le couteau !
\xv zò khi tree-çãnã
\dialx WEM
\xn vous trois reposez-vous un peu
\dt 24/Feb/2025

\lx khi
\hm 3
\dialx GOs BO
\va khibi
\dialx PA
\is action_corps
\ps v
\ge frapper 
\xv hla paò khi-u-je ya bwabu
\dialx PA
\xn ils lancent (la sagaie), frappent et le terrassent au sol
\xv i pa(ò) khi-tòè-nu-du bwa kaò
\dialx PA
\xn il m’a frappé et lancé dans le courant
\xv i paò khi-ûû-ni-du bwa kaò
\dialx PA
\xn elle le lance (bout de bois) et le fait tomber dans le courant
\se khi-u
\sge frapper-faire tomber
\se khi nu
\sge frapper/fendre un coco
\dt 13/Jun/2024

\lx khia
\hm 1
\dialx PA BO
\is habillement
\ps v
\ge mettre (chapeau)
\xv i khia hau-n
\dialx PA
\xn il met son chapeau
\xv i khia mwêêga-n
\dialx PA
\xn il met son chapeau
\dt 25/Jan/2019

\lx khia
\hm 2
\dialx GOs BO PA
\is cultures_champ
\ps n
\ge champ/massif d'igname du chef
\ge billon 
\dn avant le stade du billon cultivé |lx{kêê}
\se thu khia
\sge planter le billon du chef
\se kêê khia
\sge le champ du chef
\dt 08/Feb/2025

\lx khiai
\dialx GOs PA BO
\is feu
\ps v
\ge réchauffer (se) auprès du feu
\se i te-khiai
\dialx PA
\sge il est assis auprès du feu
\se ce-khiai
\sge bois pour se réchauffer auprès du feu
\dt 23/Aug/2021

\lx khibii
\dialx GOs
\is plantes_processus
\ps v
\ge éclore
\dt 29/Jan/2019

\lx khi-böö
\dialx GOs
\is feu
\ps v
\ge éteindre en frappant (le feu)
\xv khi-böö yaai
\xn éteindre le feu
\dt 29/Apr/2024

\lx khibu
\dialx GOs BO
\is santé
\ps v ; n
\ge gonfler ; enfler (membre)
\ge ganglion
\ge bosse
\xv khibu na ni piçanga-ã
\dialx GO
\xn ganglion de l'aine (lit. de notre aine)
\xv khibu bwèèdrò-nu
\dialx GO
\xn j'ai une bosse sur le front (lit. mon front est enflé)
\xv khibu na ni nõ-ã
\dialx GO
\xn ganglion du cou
\ng causatif: |lx{pha-khibu-ni}
\gt gonfler qqch.
\cf phû
\ce enfler
\et *tumpuq, *tutumpu(q)
\eg pousser
\el POc
\dt 21/Feb/2025

\lx khibu bwèèdrò
\dialx GOs
\is santé
\ps v
\ge enflé (front) ; avoir une bosse sur le front
\dt 25/Jan/2019

\lx khibu me
\dialx GOs
\is santé
\ps v
\ge gonflé (yeux)
\ge enflé
\xv e za khibu me ã-e
\xn il a les yeux vraiment gonflés
\dt 25/Jan/2019

\lx khibwaa
\dialx GOs PA BO
\sn 1
\is action
\ps v
\ge couper ; barrer
\xv e khibwaa dèè-nu
\dialx GOs
\xn il m'a barré la route/coupé le chemin
\xv e khibwaa dèn
\dialx PA
\xn il a barré la route/coupé le chemin
\xv e kô khibwaa dè xo ã cee
\dialx GOs
\xn cet arbre a barré la route
\se dè-khibwaa
\dialx GOs
\sge un raccourci (lit. chemin coupé)
\se khõbwe khibwaa
\sge abréger (dire couper)
\sn 2
\is déplacement
\ps v
\ge traverser ; passer à travers
\xv e khibwaa kò
\dialx GOs
\xn il a traversé la forêt
\dt 22/Feb/2025

\lx khi-drale
\dialx GOs
\va khi-dale
\dialx PA
\is action_corps
\ps v
\ge frapper-couper (en deux) ; fendre (coprah)
\xv e khi-drale nu
\xn il fend le coco
\dt 29/Apr/2024

\lx khila
\dialx GOs BO PA
\is action_corps
\ps v
\sn 1
\ge chercher ; fouiller
\xv lha a khila mõû-je
\dialx GOs
\xn ils lui cherchent une épouse
\sn 1
\ge tenter
\xv e khila vwo kibao mèni, axe e bala tha
\dialx GOs
\xn il cherchait à tuer l'oiseau, mais il l'a raté
\an tròò
\dialx GOs
\at trouver
\an tòò, tòòli
\dialx PA
\at trouver
\dt 06/Aug/2023

\lx khilaavwi
\ph kʰila:βi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge tromper (se) (en parlant)
\xv nu khilaavwi
\dialx GO
\xn je me suis trompé
\dt 21/Mar/2023

\lx khilapuu
\dialx GOs
\is interaction
\ps v
\ge provoquer (par la parole ou l'attitude)
\xv e khilapuu çai nu
\xn il m'a provoqué
\se a-khilapuu
\sge un provocateur
\dt 21/Mar/2023

\lx khilò
\dialx GOs BO
\is action_eau_liquide_fumée
\ps v
\ge couler goutte à goutte ; fuir
\dt 26/Jan/2019

\lx khimò
\dialx GOs
\ph kʰimɔ
\va khimòn
\ph kʰimɔn
\dialx BO PA
\is caractéristiques_personnes
\ps v.stat
\ge sourd
\xv e khimò
\xn il est sourd
\dt 03/Feb/2025

\lx khinu
\hm 1
\ph kʰiɳu
\dialx GOs PA BO
\sn 1
\is santé
\ps v ; n
\ge malade ; maladie ; souffrance
\ge faire mal ; être douloureux
\xv e khinu hii-nu
\dialx GO
\xn j'ai mal au bras
\xv e khinu kòò-nu
\dialx GO
\xn j'ai mal au pied
\xv i khinu kòò-ny
\dialx PA
\xn j'ai mal au pied
\xv i khinu kiò-ny
\dialx PA
\xn j'ai mal au ventre
\xv i khinu hii-ny
\dialx BO
\xn j'ai mal au bras
\xv khinu ai-ny
\dialx PA
\xn je suis malheureux
\xv nu tòòli khinu
\dialx PA
\xn je suis tombé malade
\sn 2
\is étapes_vie
\ps n
\ge mort
\dn terme d'évitement et de respect
\dt 08/Feb/2025

\lx khinu
\hm 2
\ph kʰiɳu
\dialx GOs PA
\is température
\ps v
\ge chaud (être) (atmosphère, dans la maison)
\se me-khinu-a nõ mwa
\sge la chaleur à l'intérieur de la maison
\cf tòò
\dialx GOs
\ce avoir chaud
\dt 10/Jan/2022

\lx khi-trilòò
\ph kʰi'ʈilɔ:
\dialx GOs
\va ki-tilò, khi-cilo
\dialx PA
\va khi-tilòò
\dialx BO
\is interaction
\ps v
\ge demander un peu qqch.
\ge emprunter |dialx{PA}
\dn (lit. demander momentanément)
\dt 08/Feb/2025

\lx khò
\hm 1
\ph kʰɔ
\dialx PA
\is cultures
\ps v
\ge labourer
\et *quma
\el POc
\dt 30/Jan/2019

\lx khò
\hm 2
\ph kʰɔ
\dialx GOs
\is anguille
\ps n
\ge anguille de mer (sorte d')
\dt 29/Jan/2019

\lx khò
\hm 3
\dialx GOs
\va k(h)ò
\dialx PA
\is grammaire_quantificateur_mesure
\ps QNT ; atténuatif
\ge un peu ; un instant
\xv na yu khò têê-du pwa
\dialx PA
\xn cours un peu dehors
\xv jo khò a-e
\dialx PA
\xn va un peu là-bas
\se khõ na-mi !
\dialx GOs PA
\sge donne un peu !
\se khò tia-mi !
\dialx PA
\sge pousse un peu vers moi !
\dt 16/Feb/2025

\lx khô
\hm 1
\dialx GOs PA BO
\is cordes
\ps n
\ge liane ; corde ; courroie ; longe
\se khôô-keala
\sge anse de panier; bretelles de portage
\se khôô-wõ
\dialx GO BO
\va khô-wony
\dialx PA
\sge l'amarre du bateau
\se khô-pwe
\dialx BO
\sge ligne de pêche
\xv khôô-n
\dialx BO
\xn son anse de panier; bretelles de portage
\cf wa-
\ce lien
\dt 04/Apr/2023

\lx khô
\hm 2
\dialx GOs
\is caractéristiques_objets
\ps v.stat
\ge calme ; paisible (temps, atmosphère, personne)
\xv e khô
\xn il est calme
\dt 03/Feb/2025

\lx khô-
\hm 2
\dialx GOs
\is préfixe_sémantique
\ps PREF.CLF
\ge préfixe des boutures de plante (lianes)
\se khô-kumwala
\sge bouture de patate douce
\et *qulu
\eg tête, sommet
\el POc
\dt 04/Apr/2023

\lx khõ phaxee
\ph kʰɔ̃ 'pʰaɣe:
\dialx GOs
\va k(h)ò-phaxeèn
\ph k(ʰ)ɔ 'pʰaɣeɛn
\dialx PA
\is grammaire_quantificateur_mesure
\ps QNT ; atténuatif
\ge écoute un peu
\xv yu ra khò-phaxeèn !
\dialx PA
\xn écoute un peu !
\dt 16/Feb/2025

\lx khõbo
\dialx PA BO
\is action_corps
\ps v
\ge taper ; frapper (à mort)
\xv nu khõbo-je xo cee
\dialx BO
\xn je l'ai frappé avec un bout de bois [BM]
\xv hla u khõbo-nu
\dialx PA
\xn ils m'ont frappé (à mort)
\dt 08/Nov/2021

\lx khõbwe
\ph kʰɔ̃bwe
\dialx GOs
\va kõbwe
\ph kɔ̃bwe
\dialx GOs PA BO
\wr A
\is discours
\ps v
\ge dire ; penser ; croire
\xv lha xa khõbwe na mhã Jae
\dialx GOs
\xn ils croyaient que Jae était mort
\wr B
\is grammaire_conjonction
\ps CNJ ; COMP
\ge que ; quotatif
\xv i k(h)õbwe cai nu k(h)õbwe a khila Kavo
\dialx PA
\xn il m'a dit d'aller chercher Kavo
\ng la forme quotative est souvent raccourcie en |lx{bwe}
\dt 26/Aug/2023

\lx khõbwe-raa
\dialx GOs PA
\is interaction
\ps v
\ge dire du mal de qqn.
\xv e khõbwe-raa-i-nu
\xn elle a dit du mal de moi
\xv e khõbwe-raa-ini êgu
\dialx GOs
\xn elle a dit du mal de quelqu'un
\xv i khõbwe-raa-ini
\dialx PA
\xn il l'a mal dit
\dt 29/Mar/2022

\lx khô-bwiri
\dialx GOs
\is cordes
\ps n
\ge rênes
\dt 27/Jan/2019

\lx khò-çãńã
\ph kʰɔdʒɛ̃nɛ̃
\dialx GOs
\va kò-cãnã
\ph kɔcɛ̃nɛ̃
\dialx PA
\is interaction
\ps v
\ge insister ; demander avec insistance ; persister à (sens négatif) ; s'entêter
\dt 21/Mar/2023

\lx khoe
\dialx PA
\va khoè-n
\dialx BO [Corne]
\is parenté
\ps n
\ge frère ou sœur aîné(e)
\ge aîné (frère, sœur) ; deuxième frère aîné
\xv khoe-n thoomwã
\dialx PA
\xn son deuxième frère aîné
\et *tuqa
\el POc
\dt 15/Sep/2021

\lx khô-jitrua
\ph kʰõjiɽua
\dialx GOs
\va jitua
\dialx BO PA
\is armes
\ps n
\ge corde de l'arc
\dt 02/Jan/2022

\lx khô-kaze
\dialx GOs
\is eau_marée
\ps n
\ge marée étale (calme)
\dt 29/Jan/2019

\lx khô-kumwala
\dialx GOs
\va khô-kumãla
\dialx GO(s)
\ph ,kʰõku'm(w)ãla
\is cultures
\ps n
\ge bouture de patate douce
\xv êê-nu khô-kumwala
\xn mes boutures de patate douce
\dt 04/Apr/2023

\lx khòle
\dialx GOs BO PA
\is mouvement
\is action_corps
\ps v
\ge toucher (avec la main)
\ge serrer
\xv mi pe-khòle hii-î (réalisé |ph{hîî.î})
\xn serrons-nous la main
\dt 21/Mar/2023

\lx khòlima
\dialx GOs
\is action_corps
\ps v
\ge agripper (s')
\ge cramponner
\ge retenir (se)
\xv e kha khòlima
\xn il s'agrippe en même temps
\xv khòlima !
\xn serrez fort ! ; accrochez-vous !
\dt 20/Feb/2025

\lx khô-manyô
\dialx GOs
\is cultures
\ps n
\ge bouture de manioc
\xv êê-nu khô-manyô
\xn bouture de manioc
\dt 21/Mar/2023

\lx khôme
\dialx GOs PA
\sn 1
\is nourriture
\ps v
\ge rassasié (être) ; rassasier
\xv e khôme-nu xo bunya
\xn le bounia m'a rassasié (rempli)
\sn 2
\is grammaire_aspect
\ps n.aspectuel
\ge sans cesse
\xv kixa khôme nye mããni chãnã !
\dialx GO
\xn il dort sans cesse celui-là ! (il n'a jamais assez de sommeil)
\se kixa khôme
\sge ne jamais avoir assez ; n'avoir de cesse de
\dt 24/Feb/2025

\lx khò-na-mi
\dialx PA
\is interaction
\ps v
\ge donne-moi un peu
\dt 10/Mar/2023

\lx khône
\dialx GOs
\is caractéristiques_objets
\ps v
\ge immobile
\xv khône hava-hii-je
\xn il a les ailes immobiles (il plane)
\dt 26/Jan/2019

\lx khõnò
\ph kʰɔ̃ɳɔ̃
\dialx GOs
\is insecte
\ps n
\ge cocon d'insecte
\dt 10/Mar/2019

\lx khòò
\ph kʰɔ:
\dialx GOs
\va khòòl
\dialx PA BO
\is reptile
\ps n
\ge caméléon
\dt 29/Jan/2019

\lx khôô keala
\ph 'kʰõ: 'keala
\dialx GOs
\va khôô-ke
\dialx GO
\is portage
\ps n
\ge bretelles de portage
\dt 02/Jan/2022

\lx khooje
\dialx GOs
\is oiseau
\ps n
\ge émouchet bleu
\dn à ventre blanc
\sc Accipiter haplochrous
\scf Accipitridés
\gb White-bellied Goshawk
\dt 08/Feb/2025

\lx khööjo
\ph kʰω:ɲɟo
\dialx GOs
\va koojòng, khoojòng
\dialx PA BO
\sn 1
\is arbre
\ps n
\ge arbre à latex, "bois de lait rouge"
\dn dont la sève est utilisée comme poison pour la pêche stupéfiante
\sc Cerbera odollam Gaertn.
\scf Apocynacées
\se khööjo mii
\dialx GOs
\sge arbre à latex rouge
\sn 2
\is arbre
\ps n
\ge faux manguier, "bois de lait blanc"
\dn son fruit contient un noyau très toxique utilisé comme poison pour la pêche
\sc Cerbera manghas L.
\scf Apocynacées
\se khööjo phozo
\dialx GOs
\sge arbre à latex blanc
\dt 22/Oct/2021

\lx khööńe
\ph kʰω:ne
\dialx GOs PA BO
\is portage
\ps v
\ge porter sur l'épaule ; chargé
\xv e kha-khööńe cee
\dialx GO
\xn il marche avec un fagot sur le dos
\cf khoon
\dialx BO
\ce porter sur l'épaule
\dt 21/Mar/2023

\lx khòòni
\ph kʰɔ:ɳi
\dialx GOs
\va kòòni
\dialx BO PA
\is cultures
\is action_outils
\ps v
\ge piocher ; bêcher (champ)
\xv whara ò kòòni yaa-wòlò
\dialx PA
\xn le temps de labourer le champ du chef
\dt 22/Aug/2021

\lx khooni-n
\dialx PA
\is portage
\ps n
\ge ton fardeau
\dt 20/Dec/2021

\lx khô-pwe
\ph kʰɔ̃pwe
\dialx GOs BO
\is pêche
\ps n
\ge ligne (à hameçon)
\dt 02/Jan/2022

\lx khò-phaxeen
\dialx PA
\is grammaire_quantificateur_mesure
\ps v
\ge écouter un instant/un peu
\dt 22/Feb/2025

\lx khòraa
\dialx BO
\is action
\ps v
\ge gaspiller
\xv i khòrale nõõla-n
\xn il gaspille ses richesses [BM]
\ng v.t. |lx{khòrale}
\gt gaspiller qqch.
\dt 21/Feb/2025

\lx khoriing
\dialx PA
\is plantes_partie
\ps n
\ge rejet d'arbuste ou d'arbre taillé
\cf kîbwò ; kîbwòn
\ce rejet (qui part du pied)
\dt 29/Jan/2019

\lx khô-wõ
\dialx GOs
\va kô-wony
\dialx BO PA
\is navigation
\ps n
\ge cordage de bateau
\dt 21/Mar/2023

\lx khòzole
\dialx GOs
\va khòzoole
\sn 1
\is action
\ps v
\ge gaspiller (argent, nourriture)
\xv e khòzole hovwo
\xn il gaspille de la nourriture
\xv e khòzole thoomwã xo êmwê êńa
\xn ce garçon ne mérite pas cette fille
\sn 2
\is interaction
\ps v
\ge ne pas mériter ; moquer (se)
\xv e khòzole ciia ma nye e kaka-du mwã bwa bwa-je
\xn il se moque du poulpe car il lui a fait caca sur la tête
\dt 20/Dec/2021

\lx khûbu
\dialx GOs PA BO
\va kûbu
\dialx BO
\is action_corps
\ps v
\ge frapper ; tuer
\xv hla kûbu-ye
\dialx BO
\xn ils l'ont tué
\dt 21/Oct/2021

\lx la
\dialx GO PA BO
\wr A
\is grammaire_article
\ps ART.PL
\ge les
\xv la mõgu i ã
\dialx GOs
\xn nos travaux
\xv i hivwine da la yala-ã
\dialx BO
\xn il ne connaît pas nos noms [Corne]
\wr B
\is grammaire_article
\ps PRON.PL
\ge les x que
\xv nu kuele la nu ne
\dialx GOs
\xn je déteste ce que j'ai fait (les choses que)
\xv i hivwine (kõbwe) da la yu cabi
\dialx BO
\xn il ne sait pas quelles choses tu as frappées [Corne]
\dt 26/Feb/2023

\lx -la
\dialx GOs PA
\va -laa
\dialx GOs PA
\is grammaire_pronom
\ps PRO 3° pers. PL (OBJ ou POSS)
\ge les ; leur(s)
\dt 24/Aug/2021

\lx lã-ã
\dialx GOs PA BO
\va lhã, lã
\is grammaire_démonstratif
\ps DEM.DEIC.PL.ou ANAPH
\ge ces …-ci
\xv la thoomwã lã-ã
\xn ces femmes-ci
\xv l(h)ã-ã ba-êgu
\xn ces femmes-ci
\xv ila l(h)ã-ã
\xn les voici ceux-ci
\dt 13/Aug/2023

\lx laa-ba
\dialx GOs
\is grammaire_démonstratif
\ps DEM.DEIC PL
\ge ceux-là là-bas
\dt 24/Oct/2021

\lx laa-du
\dialx GOs PA
\is grammaire_démonstratif
\ps PRO.DEIC.PL
\ge ceux-là en bas
\dt 29/Jan/2019

\lx -la-eo
\dialx BO
\is grammaire_démonstratif
\ps DEM PL
\ge les …-là (loin de l'interlocuteur)
\nt selon Dubois
\cf -eo
\ce démonstratif singulier
\dt 24/Feb/2025

\lx lai
\dialx GOs
\is nourriture
\ps n
\ge riz
\se pwò-lai
\sge grain de riz
\dt 25/Aug/2021

\lx la-ida
\dialx GOs PA
\is grammaire_démonstratif
\ps DEIC.PL
\ge ceux-là en haut
\dt 29/Jan/2019

\lx lalue
\dialx GOs PA
\is plantes
\ps n
\ge aloès
\sc Furcræa fœtida
\scf Agavacées
\dt 27/Aug/2021

\lx lã-na
\dialx PA BO
\is grammaire_démonstratif
\ps DEM.DEIC.2 duel et ANAPH
\ge ceux-là
\xv pwawa ne jö whili-mi lã-na pòi-m ?
\xn est-il possible que tu amènes ici tes petits enfants ?
\dt 08/Jan/2022

\lx la-nim
\dialx PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal PL
\ge ces …-là (péjoratif)
\dn s'emploie pour les humains mis à distance
\dt 24/Feb/2025

\lx lao
\dialx PA BO
\va lau
\dialx BO
\is action
\ps v
\ge rater (cible) ; louper ; manquer
\cf pa-tha
\dialx GOs
\ce rater
\dt 10/Jan/2022

\lx la-òli
\dialx GOs PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal ou ANAPH
\ge ceux-là là-bas (visible)
\dt 03/Feb/2025

\lx lapya
\dialx GOs
\is poisson
\ps n
\ge tilapia
\sc Oreochromis mossambica
\scf Cichlidés
\bw tilapia (FR)
\dt 27/Aug/2021

\lx layô
\dialx GOs
\va laviã
\dialx WE
\va laviãn
\dialx BO PA
\is nourriture
\ps n
\ge viande rouge
\bw la viande (FR)
\dt 24/Dec/2021

\lx lazana
\dialx GOs
\is plantes
\ps n
\ge lantana
\dt 27/Oct/2023

\lx -le
\dialx GO PA BO
\is grammaire_locatif
\ps LOC.ANAPH
\ge là
\dt 27/Oct/2021

\lx lè
\dialx GOs
\is nourriture
\ps n
\ge lait
\bw lait (FR)
\dt 26/Jan/2019

\lx li
\dialx GO PA
\is grammaire_article
\ps ART.DU
\ge les deux
\xv li êgu mãli-ã
\dialx PA
\xn ces deux personnes-ci
\dt 08/Jan/2022

\lx -li
\dialx GO PA
\is grammaire_pronom
\ps PRO 3° pers. duel (OBJ ou POSS)
\ge les ; leur
\xv lhi ã-da mã pòi-li Numia ? – Hai ! lhi za ã-da hãda Numia
\xn ils sont partis avec leur fils à Nouméa ? – Non ! ils sont seulement partis tous les deux à Nouméa
\dt 24/Feb/2025

\lx li-ã
\dialx GOs PA
\is grammaire_démonstratif
\ps DEIC.1 duel ou ANAPH
\ge ces deux …-ci
\xv li-ã ba-êgu
\xn ces deux femmes-ci
\xv li-ã kòò-ny
\dialx BO
\xn mes deux pieds
\se li-ã-du
\sge ces deux-ci en bas
\se li-ã-da
\sge ces deux-ci en haut
\dt 30/Dec/2021

\lx li-è
\dialx GOs
\va li-ã
\dialx BO
\is grammaire_démonstratif
\is grammaire_article
\ps DEM duel ; article
\ge ces deux-ci
\xv li-è kòò-nu
\dialx GO
\xn mes deux pieds
\xv e za yaawa ui li-è whamã ?
\dialx GO
\xn est-il nostalgique de ses parents ?
\dt 30/Dec/2021

\lx li-ne
\dialx GOs
\is grammaire_démonstratif
\is grammaire_article
\ps DEM duel ; article
\ge ces deux-là (médial)
\xv e thi-gua-ni li-ne nõ
\dialx GOs
\xn il a enfilé les deux poissons sur la filoche
\xv zixôô-ny li-ne thoomwã
\dialx GOs
\xn c'est mon conte à propos de ces deux femmes
\dt 24/Feb/2025

\lx li-nim
\dialx PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal duel
\ge ces deux …-là (souvent péjoratif)
\dn s'emploie pour les humains mis à distance
\dt 08/Feb/2025

\lx li-òli
\dialx GOs PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal ou ANAPH
\ge ces deux-là là-bas ; là-bas (visible)
\xv li-òli
\xn ces deux-là là-bas
\xv li-òli mwã
\xn ces deux-là loin là-bas
\dt 03/Feb/2025

\lx lò
\dialx GOs
\is grammaire_article
\ps ART.TRI
\ge les trois ; marque de paucal
\xv me nõõli druube ma lò kovanye-nu dròrò
\dialx GO
\xn mes amis et moi avons vu des cerfs hier
\xv inu ma lò kovanye-nu me nõõli druube dròrò
\dialx GO
\xn mes amis et moi avons vu des cerfs hier
\dt 16/Aug/2023

\lx -lò
\dialx GO
\va -lòò
\dialx GO
\is grammaire_pronom
\ps PRO 3° pers. triel (OBJ ou POSS)
\ge eux trois ; leurs (à eux trois)
\dt 24/Aug/2021

\lx lòlòò
\dialx GOs
\is grammaire_adverbe
\is grammaire_modalité
\ps ADV
\ge au hasard ; sans but
\xv e pe-thu-mhenõ lòlòò
\xn il marche sans but
\xv e traabwa lòlòò
\xn il est assis sans rien faire
\xv çö za hine lòlòò òri !
\xn tu dis des bêtises ! (lit. tu sais sans savoir)
\dt 21/Mar/2023

\lx lòn
\dialx BO
\is interaction
\ps v
\ge mentir ; mensonge
\xv a-lòn
\dialx BO
\xn menteur
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx loto
\dialx GOs
\va lòto
\dialx BO
\va wathuu
\dialx GO(s)
\is déplacement_moyen
\ps n
\ge voiture ; auto
\bw l'auto (FR)
\dt 25/Jan/2019

\lx lha
\dialx GOs PA
\va le
\dialx BO
\is grammaire_pronom
\ps PRO 3° pers.PL (sujet)
\ge ils
\xv lha za yaawa dròrò
\dialx GOs
\xn ils étaient tristes hier
\dt 24/Aug/2021

\lx lhi
\dialx GOs PA
\is grammaire_pronom
\ps PRO 3° pers. duel (sujet)
\ge ils
\xv lhi za ubò dròrò ? – Hai ! lhi za yu avwõnõ
\dialx GO
\xn ils sont sortis hier ? – Non ! ils sont restés à la maison
\xv li za a-da bulu Numia ?
\xn ils sont partis ensemble à Nouméa ?
\xv za ili nye lhi a-da Numia
\xn c'est seulement eux deux qui sont partis à Nouméa
\dt 22/Feb/2025

\lx lhò
\dialx GOs
\va zò
\dialx WE
\is grammaire_pronom
\ps PRO 3° pers. triel sujet
\ge eux trois ; eux (paucal)
\se lhò-na
\sge eux trois (dx3)
\se lhò-ba
\sge eux trois là sur le côté
\se lhò-è
\sge ces trois-ci
\dt 24/Feb/2025

\lx lhòlòe
\dialx GOs
\va lòloi
\dialx BO PA
\sn 1
\is préparation_aliments
\ps v
\ge couper en lamelles
\sn 2
\is préparation_aliments
\ps n
\ge préparation culinaire d'ignames
\dn à base d'ignames coupées en fines rondelles
\xv e thu lhòlòe
\dialx GO
\xn elle coupe l'igname en fines rondelles pour faire un bounia
\cf bunya
\ce bounia
\cf eloe
\ce couper en lamelles
\dt 08/Feb/2025

\lx -m
\dialx BO PA
\is grammaire_pronom
\ps SUFF.POSS 2° pers.
\ge ton ; ta ; tes
\dt 29/Jan/2019

\lx ma
\dialx BO
\is action_corps
\ps v
\ge embrasser
\ge prendre dans ses bras
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx -ma
\is grammaire_conjonction
\dialx GO PA BO
\ps SUFF.associatif
\ge associatif
\se Treã-ma
\sge le Grand Chef (et ses sujets)
\dt 21/Feb/2025

\lx mã
\hm 1
\ph ṃæ̃
\dialx GOs PA BO
\va mhã
\dialx PA BO
\sn 1
\is étapes_vie
\ps v.stat
\ge mourir ; mort (être)
\se me-mhã
\dialx PA
\sge la mort
\sn 2
\is santé
\ps v.stat
\ge paralysé ; engourdi
\xv e mã na le ne
\dialx GOs
\xn il est apathique (lit. les mouches meurent là)
\xv e mã hii-je
\dialx GOs
\xn il est handicapé du bras
\xv e mã kòlò-je
\dialx GOs
\xn il est hémiplégique (lit. son côté est paralysé)
\xv mã phãgoo-je
\dialx GOs
\xn il est paralysé/impotent/invalide
\xv mhã ai-ny
\dialx PA
\xn je suis malheureux
\sn 3
\is santé
\ps n
\ge maladie ; malade
\xv kavwö nu tròòli mã mwã, nu zo mwã
\dialx GOs
\xn je ne suis plus malade ; je suis guéri
\xv nu tròòli mã
\dialx GOs
\xn je suis malade
\xv nu tòòli mhã
\dialx PA BO
\xn je suis malade ; je suis tombé malade
\xv mã i je
\dialx GOs
\xn sa maladie
\xv mhãi-n
\dialx BO
\xn sa maladie
\se we mhã
\dialx PA
\sge médicament
\et *mate
\el POc
\dt 22/Feb/2025

\lx mã
\hm 2
\dialx GOs PA BO
\sn 1
\is grammaire_conjonction
\ps CNJ
\ge et ; aussi
\ge avec
\xv xa nowö kêê Jae, ça za lhi za gi chãnã mã õã-je
\dialx GO
\xn quant au père de Jae, lui et sa mère pleurent toujours là  (i.e. la mère de Jae)
\xv e yu kòlò kêê-je mã õã-je
\dialx GO
\xn il vit chez son père et sa mère
\xv ça novwö ibî mã Paola, ça bî uça na ni choomu
\dialx GO
\xn quant à Paola et moi, nous revenions de l'école
\sn 2
\is grammaire_conjonction
\ps CNJ
\ge car ; parce que
\xv nòòl mã bî a pwe
\dialx PA
\xn réveille-toi car nous allons à la pêche
\xv nòòl mã u tèèn
\dialx BO
\xn réveillez-vous car il fait jour
\sn 3
\is grammaire_conjonction
\ps CNJ
\ge pour que
\cf mãni
\ce et avec
\dt 22/Feb/2025

\lx mã-
\dialx GOs PA
\is grammaire_nombre
\ps nombre
\ge marque du non-singulier
\xv êgu mãli-ã ; mãli-èni ; mãli-òli
\xn (duel) ces deux personnes-là ; ces deux personnes-là (dx2) ; ces deux personnes là-bas
\xv êgu mãlò-ã ; mãlò-èni ; mãlò-òli
\xn (paucal) ces gens-là ; ces gens-là (dx2) ; ces gens là-bas
\xv êgu mãla-ã ; mãla-èni ; mãla-òli
\xn (pluriel) ces gens-là ; ces gens-là (dx2) ; ces gens là-bas!
\xv ẽnõ mãlò !
\xn vous les enfants ! (triel, paucal)
\se mãli-
\sge marque de duel des déterminants
\se mãlò-
\sge marque de triel des déterminants
\se mãla-
\sge marque de pluriel des déterminants
\dt 24/Feb/2025

\lx mã nye
\dialx GOs
\va me nye
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge parce que ; du fait que
\xv bî hoxe pe-ada nõgò mã nye va a thraabu
\xn nous deux allons remonter à la rivière parce que nous allons faire la pêche
\xv tho-raa mã nye pòńõ mwã nye
\dialx GO
\xn il répond mal car il est faible
\xv mã nye za kòi-nu na ênè avwõnõ, pu-nye da a khilaa-je
\dialx GO
\xn la raison pour laquelle je n'étais pas ici à la maison, c'était parce que je suis parti la chercher
\xv na axa mõû-çö, ma nhye zo(ma) na mi (x)a pe-woovwa
\xn si tu veux une épouse, alors nous allons nous battre
\xv nu gi me nye e kalae-nu xo pwiri
\xn je pleure parce que la perruche a fui en m'abandonnant
\dt 21/Mar/2023

\lx maa-
\dialx PA
\is classificateur_possessif_nourriture
\ps CLF.POSS
\ge part de nourriture mastiquée
\xv maa-ny (a) caali (|ph{ma:ɲəca:li})
\dialx PA
\xn ma part de magnania
\dt 31/Jan/2022

\lx mãã
\hm 1
\dialx GOs PA BO
\is nourriture
\ps v
\ge mâcher (de la nourriture pour un bébé)
\xv e mãã cè-ẽnõ
\dialx GOs
\xn elle mâche la nourriture pour l'enfant
\dt 11/Mar/2019

\lx mãã
\hm 2
\dialx GOs
\va maak
\dialx PA BO
\is arbre
\ps n
\ge manguier
\sc Mangifera indica L.
\scf Anacardiacées
\xv po-xè pò-mãã
\xn une mangue
\dt 27/Aug/2021

\lx mãã
\hm 3
\dialx GOs WEM PA BO
\sn 1
\is oiseau
\ps n
\ge oiseau "lunette"
\sc Zosterops sp.
\scf Zosteropidés
\gb Silver-eye, Green-backed White-eye
\sn 2
\is oiseau
\ps n
\ge fauvette à ventre jaune ; fauvette gobe-mouche
\sc Gerygone Flavolateralis Flavolateralis
\scf Acanthizidés
\gb Fan-tailed Gerygone
\dt 10/Jan/2022

\lx mãã
\hm 4
\dialx GOs BO PA
\is mollusque
\ps n
\ge troca (gastéropode)
\sc Tectus (Tectus) pyramis
\scf Turbinidés
\dt 27/Aug/2021

\lx maaça
\dialx GOs
\is grammaire_manière
\ps MODIF
\ge ralentir ; calmer (se)
\xv e kô-maaça
\xn cela se calme (cyclone, mer)
\xv a maaça !
\xn va plus lentement ! ; ralentis !
\cf khô
\ce calme (mer, vent)
\dt 24/Feb/2025

\lx mãã-ce-bwòn
\dialx BO
\is feu
\ps n
\ge foyer
\nt selon BM
\dt 26/Mar/2022

\lx maadra
\dialx GOs
\va maada
\dialx BO
\is feu
\ps n
\ge foyer ; endroit où l'on fait le feu
\dt 21/Mar/2023

\lx maagò
\hm 1
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge apathique (être)
\dt 26/Jan/2019

\lx maagò
\hm 2
\dialx WEM BO PA
\is maison
\ps n
\ge poutre maîtresse des maisons carrées
\ge poutre faîtière
\dt 25/Aug/2021

\lx mããle
\dialx GOs
\va maalèm
\dialx BO PA
\is plantes
\ps n
\ge mimosa de forêt
\dn fleur à pompon jaune
\sc Pithecellobium schlechteri Guillaum
\scf Fabacées
\dt 08/Feb/2025

\lx maalò
\dialx BO
\is anguille
\ps n
\ge anguille jaune
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx maalu
\dialx BO PA
\is fonctions_naturelles
\ps v
\ge soif (avoir)
\xv nu maalu
\dialx PA
\xn j'ai soif
\xv maalu nu
\dialx BO
\xn j'ai soif (Dubois)
\dt 11/Mar/2019

\lx maama
\dialx PA
\is religion
\ps n
\ge diable ; mauvais esprit
\xv thu lhã-nã maama (x)a la aa-yuu ênida bwe Kaòla
\xn il y avait des diables qui vivaient là-haut au sommet de Kaòla
\dt 05/Nov/2021

\lx mããni
\ph mɛ̃:ɳi
\dialx GOs WEM BO PA
\va mãni
\dialx PA
\sn 1
\is fonctions_naturelles
\ps v ; n
\ge dormir ; couché (être) ; allongé (être)
\ge sommeil
\xv e paa-mããni-bî
\dialx GO
\xn elle nous endormait
\xv i mããni gòòn-al
\dialx PA BO
\xn il a fait la sieste
\se mõ-mããni
\dialx BO
\sge chambre à coucher
\ng causatif: |lx{paa-mããni-ni}
\gt endormir qqn.
\sn 2
\is action_corps
\ps v
\ge traîner par terre ; éparpiller |dialx{PA}
\dt 23/Jan/2022

\lx mããni-mhã
\dialx PA
\is fonctions_naturelles
\ps v
\ge dormir trop (faire la grasse matinée)
\dt 25/Jan/2019

\lx maara
\dialx PA
\is eau
\ps n
\ge marécage
\dt 29/Jan/2019

\lx maari
\dialx GOs BO
\is sentiments
\ps v
\ge admirer
\dt 27/Jan/2019

\lx mãã-trele
\dialx GOs
\va mãã-rele
\dialx GO(s)
\is oiseau
\ps n
\ge gobe-mouche
\sc Myiagra caledonica
\scf Monarchidés
\gb New Caledonian Flycatcher
\dt 27/Aug/2021

\lx maaya
\dialx GOs
\va mãã
\dialx PA
\va mhaa
\dialx WEM
\va maca
\dialx BO (Corne)
\is arbre
\ps n
\ge faux gaïac
\dt 21/Mar/2023

\lx maayèè
\dialx BO
\is caractéristiques_personnes
\ps v
\ge paresseux
\xv i a-maayèè
\xn il est paresseux
\nt selon Corne
\dt 26/Mar/2022

\lx mabu
\dialx GOs
\is santé
\ps v
\ge fatigué (après une nuit courte)
\xv e mabu
\xn il est fatigué
\dt 23/Jan/2022

\lx mada
\dialx BO PA
\va mara
\dialx PA BO
\va m(h)aza
\dialx GO
\is grammaire_aspect
\ps INCH, en cours
\ge venir de ; commencer ; en cours
\xv nu mada hup
\dialx PA
\xn je suis en train de manger
\xv cu mada kol
\dialx PA
\xn lève-toi !
\xv eka i mara tavune mòlò
\dialx BO
\xn quand commence la vie (Dubois)
\dt 02/Nov/2021

\lx mãdra
\dialx GOs
\is habillement
\va mãda
\dialx PA BO
\ps n
\ge morceau d'étoffe
\dn fait à partir de la racine du banian
\ge jupe de femme
\dn petite, formée de |lx{wepooe} et de |lx{pobil} (Dubois ms)
\ge manou ; pagne (des hommes)
\dt 08/Feb/2025

\lx mãdra bwabu
\dialx GOs
\is habillement
\ps n
\ge jupon
\dt 26/Jan/2018

\lx mãe
\dialx GOs
\va mãe
\dialx BO PA
\va mãi
\dialx BO
\is plantes
\ps n
\ge herbe ; paille ; chaume
\ge vétiver |dialx{BO}
\dn cette herbacée est utilisée pour retenir le talus ; ses racines séchées peuvent être utilisées pour parfumer
\sc Imperata arundinacea ou Imperata cylindrica
\scf Graminées
\dt 27/Mar/2022

\lx mãè-
\dialx GOs
\va mãi-
\dialx PA BO
\is classificateur_numérique
\ps CLF.NUM
\ge lot de trois ignames
\ge lot de quatre taros ou de quatre noix de coco
\dn offerts lors de la fête de la nouvelle igname, contexte cérémoniel
\xv mãi-ru
\dialx PA
\xn deux lots de trois ignames
\xv mãè-xè mãè-kui ; mãè-tru mãè-kui ; mãè-ni ma-xè mãè-kui, etc.
\dialx GO
\xn un paquet de trois ignames ; deux paquets de trois ignames ; six paquets de trois ignames, etc.
\xv mãè-ni ma-xè mãè-uva
\dialx GO
\xn six paquets de quatre taros
\dt 22/Feb/2025

\lx mãebo
\dialx GOs PA BO
\is plantes
\ps n
\ge citronnelle
\sc Cymbopogon citratus (L.) Stapf.
\scf Poacées, Graminées
\se dròò-mãebo
\dialx GO
\sge feuilles de citronnelle
\dt 27/Aug/2021

\lx mãè-nira ?
\dialx PA BO
\is grammaire_interrogatif
\ps INT
\ge combien de paquets de trois ? (ignames, etc.)
\dt 22/Feb/2025

\lx mãè-xè
\dialx GOs BO
\is classificateur_numérique
\ps CLF.NUM
\ge un lot (de trois ignames)
\xv mãè-nira ?
\xn combien de lots de trois ignames?
\cf mãè-tru, mãè-kò, etc.
\ce deux lots (de trois ignames), trois lots, etc.
\dt 24/Feb/2025

\lx mangane
\dialx BO
\is taro
\ps n
\ge taro (clone) de terrain sec
\nt selon Dubois
\dt 27/Mar/2022

\lx mâge
\dialx BO
\va mhâge
\sn 1
\is nœud
\ps n ; v
\ge nouer ; nœud (du filet)
\sn 2
\is pêche
\ps n ; v
\ge maille ; faire un filet
\dt 26/Jan/2018

\lx mãgi
\dialx GOs
\va mwãgi
\dialx PA BO
\sn 1
\is coutumes
\ps n
\ge hache ostensoir
\sn 2
\is outils
\ps n
\ge hache à double tranchant
\dt 15/Sep/2021

\lx mãgiça
\ph mɛ̃giʒa
\dialx GOs
\is poisson
\ps n
\ge poisson sauteur de palétuviers
\sc Périophtalme sp.
\scf Gobiidés
\dt 19/Feb/2025

\lx magira
\ph magiɽa
\dialx GOs
\is préparation_aliments
\ps v
\ge cuit (à moitié) ; pas assez cuit
\xv magira dröö
\xn la nourriture de la marmite est à moitié cuite (et le feu est à moitié éteint)
\dt 21/Mar/2023

\lx magòòny
\dialx BO
\is maison
\ps n
\ge traverses (charpente)
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx magu
\dialx GOs
\va maguny
\dialx BO PA
\va maguc
\dialx BO
\is insecte
\ps n
\ge guêpe noire
\xv pi maguc
\dialx BO
\xn nid de guêpes
\dt 20/Feb/2025

\lx maic
\dialx PA
\is plantes
\ps n
\ge maïs
\bw maïs (FR)
\dt 29/Jan/2019

\lx mãiyã
\dialx GOs
\va mãiã
\dialx GO(s)
\va meã
\dialx BO [BM]
\va mee
\dialx PA
\is nourriture
\is préparation_aliments
\ps v.stat
\ge cuit (mal) ; pas assez cuit (riz, viande)
\xv mãiyã dröö
\dialx GOs
\xn la nourriture de la marmite n'est pas cuite
\xv mãiyã gò
\dialx GOs
\xn c'est encore cru
\xv mãiyã layô
\xn la viande n'est pas cuite
\xv mãiyã lai
\xn le riz n'est pas cuit
\cf thraxilo ; thaxilo
\an minõ ; minong
\at cuit
\dt 03/Feb/2025

\lx maja
\hm 1
\ph maɲɟa
\dialx GOs
\is nourriture
\ps v
\ge altéré ; pas frais (nourriture)
\xv e maja nõ
\dialx GO
\xn le poisson n'est pas frais
\dt 09/Feb/2019

\lx maja
\hm 2
\ph maɲɟa
\dialx GOs
\va maya
\dialx BO
\is nourriture
\ps n
\ge miettes ; résidus
\dt 25/Aug/2021

\lx maja-cee
\ph maɲɟa-cɨ
\dialx GOs
\va ja-cee
\dialx GO(s)
\va maya-cee, meya-cee
\dialx BO
\is bois
\ps n
\ge copeaux de bois
\ge sciure
\dt 08/Nov/2021

\lx maja-hovwo
\dialx GOs
\is nourriture
\ps n
\ge miettes de nourriture ; reliefs de nourriture
\dt 25/Aug/2021

\lx majiwe
\dialx BO
\is habillement
\ps n
\ge collier (de jade) ; pendentif
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx majö
\ph maɲɟω
\dialx GOs BO
\is reptile
\ps n
\ge gecko ; margouillat ; tarente
\dt 21/Mar/2023

\lx makoyoo
\dialx GOs
\va maxoyoo
\dialx GO(s)
\is nourriture
\ps v
\ge désirer ; avoir envie de manger qqch.
\xv nu maxoyoo nõ
\dialx GO
\xn j'ai envie de poisson
\dt 29/Mar/2022

\lx mala
\dialx GOs
\is lumière
\ps n
\ge lumière du jour ; lumière
\se mala-a
\dialx GO
\sge lumière du soleil
\se mala-yaai
\dialx GO PA
\sge lumière du feu ; lumière électrique
\se mala-mhwããnu
\sge lumière de la lune
\dt 22/Feb/2025

\lx mãla
\dialx GOs PA
\is grammaire_nombre
\ps DEM PL (post-nom)
\ge marque de pluriel (des déterminants nominaux)
\xv la thoomwã mãla-êba
\dialx GO
\xn ces femmes là-bas sur le côté en contrebas
\xv haivwö la mãla phwe-meevwu Maluma xa la uça
\dialx GO
\xn beaucoup de gens du clan Maluma sont venus
\xv ni tree mãla-ò
\dialx GO
\xn dans les temps anciens
\xv mõ pe-traabwa na ni pira cee mãla-ã
\dialx GO
\xn nous sommes assis ensemble à l'ombre de ces arbres
\xv li ra u hore mwã la dèèn mãla-ò i ra phe-da-mi
\dialx PA
\xn ils suivent les chemins qu'il avait déjà pris en montant ici
\se êgu mãla-ã
\sge ces gens-ci (pluriel)
\se êgu mãla-na
\sge ces gens-là (dx2)
\se êgu mãla-òli
\sge ces gens là-bas
\se êgu mãla-eba
\sge ceux-là sur le côté en contrebas
\se êgu mãla-ã-du
\sge ces gens-là en bas
\se êgu mãla-ã-da
\sge ces gens-là en haut
\se êgu mãla-ò
\sge ces gens-là (anaphorique)
\cf mãlò-
\dialx GO
\ce triel
\cf mãli-
\ce duel
\dt 24/Feb/2025

\lx malèmwi
\dialx BO
\is interaction
\ps v
\ge plaire ; séduire
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx malevwi
\ph 'maleβi
\dialx GOs
\va maalemi
\dialx BO [BM]
\is action_tête
\ps v
\ge lécher
\dt 26/Jan/2019

\lx mãli
\dialx GOs PA
\is grammaire_nombre
\ps DEM duel (post-nom)
\ge marque de duel (des déterminants nominaux)
\xv li êgu mãli-ã
\dialx PA
\xn ces deux personnes-ci
\xv çö hine thoomwã mãli-eba ?
\dialx GOs
\xn tu connais ces deux filles là-bas sur le côté ?
\se êgu mãli-ã
\sge ces deux personnes-ci
\se êgu mãli êni
\sge ces deux personnes ici
\se êgu mãli-na
\sge ces deux personnes-là (proche)
\se êgu mãli-eba
\sge ces deux personnes-là sur le côté en contrebas
\se êgu mãli-òli
\sge ces deux personnes là-bas
\se êgu mãli-ã-du
\sge ces deux personnes-là en bas
\se êgu mãla-ã-da
\sge ces deux personnes-là en haut
\se êgu mãli-ò
\sge ces deux personnes-là (anaphorique)
\cf mãlò-
\dialx GO
\ce marque de triel des déterminants
\cf mãla-
\ce marque de pluriel des déterminants
\dt 24/Feb/2025

\lx mãli-na
\dialx GOs PA
\va mãli-ne
\dialx PA
\is grammaire_nombre
\ps DEM.DEIC duel et ANAPH
\ge ces deux-là
\xv thoomwã mãli-na nye çö kôbwe ? – Ô ! li-na
\dialx GOs
\xn ce sont ces deux femmes-là dont tu parles ? – Oui, ce sont ces deux-là
\dt 24/Feb/2025

\lx mãlò
\dialx GOs
\is grammaire_nombre
\ps DEM triel (post-nom)
\ge marque de triel (des déterminants nominaux)
\xv we kòòla êni xo ime mãlò-è
\xn vous qui êtes debout là et nous qui sommes debout ici
\se êgu mãlo-ã
\sge ces trois personnes-là
\se êgu mãlo-na
\sge ces trois personnes-là (dx2)
\se êgu mãlo-òli
\sge ces trois personnes là-bas
\se êgu mãlo-ã-du
\sge ces trois personnes-là en bas
\se êgu mãlo-ã-da
\sge ces trois personnes-là en haut
\se êgu mãlo-ò
\sge ces trois personnes-là (anaphorique)
\dt 20/Feb/2025

\lx maloom
\dialx WEM WE BO
\va malum
\dialx PA
\is soin
\ps v
\ge propre ; neuf
\xv thu malum
\dialx PA
\xn nettoyer; rénover
\dt 25/Jan/2019

\lx mãni
\ph mɛ̃ni
\dialx GOs PA BO WEM
\va meni
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge et ; et aussi (simultanément) ; avec ; etc. (énumération)
\xv bî ã-du kaaze mãni/mã õã-nu (ou) nu ã-du kaaze mãni/mã õã-nu
\dialx GOs
\xn je suis allé pêcher avec ma mère (le pronom duel est considéré comme plus correct par les locuteurs âgés; le pronom singulier est utilisé par les jeunes locuteurs)
\xv mwõ-bî mãni ãbaa-nu
\dialx GOs
\xn c'est notre maison à moi et mon frère
\xv bî waaçu mãni chòòva
\dialx GOs
\xn le cheval et moi avons persévéré
\xv zaa mãni pòi-je ka li a-yuu Wêwêne
\dialx GOs
\xn une poule sultane et son enfant vivaient à Wêwêne
\xv pu mwani i lò, pu loto i lò mãni
\dialx GO
\xn ils ont de l'argent, ils ont une voiture, etc.
\xv ili mãni nya gèè i je
\dialx PA
\xn lui et sa grand-mère
\xv e ciia mãni wa
\dialx GOs
\xn il danse et chante (en même temps)
\xv i ciia mãni wal
\dialx PA
\xn il danse et chante (en même temps)
\xv ênõli duu-n mãni canaa-n
\dialx PA
\xn il y a là l'os et le souffle
\xv pu na gi le kêê-ê mãni kibu-ê
\dialx PA
\xn c'est pour cela que sont présents nos pères et grands-pères
\dt 24/Feb/2025

\lx manyô
\dialx GOs PA
\is plantes
\ps n
\ge manioc
\dt 29/Jan/2019

\lx mararâ
\dialx BO
\is oiseau
\ps n
\ge fauvette calédonienne
\sc Megalurulus mariei
\scf Locustellidés
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx mara-tèèn
\dialx PA
\is temps_découpage
\ps n
\ge aube
\cf mala
\ce éclat ; lueur
\dt 22/Feb/2025

\lx masi
\dialx GOs
\is instrument
\ps n
\ge machine
\bw machine (FR)
\dt 27/Jan/2019

\lx maû
\dialx GOs PA
\is habillement
\is coutumes_objet
\ps n
\ge rouleau d'étoffe
\xv ha-ru maû ci-xãbwa
\dialx GO PA
\xn deux rouleaux d'étoffe
\dt 19/Oct/2021

\lx maü
\dialx BO
\sn 1
\is santé
\ps v
\ge démanger
\nt selon Corne, BM
\sn 2
\is caractéristiques_objets
\ps v
\ge piquant (piment)
\dt 27/Mar/2022

\lx mãû
\hm 1
\ph mɛ̃û
\dialx GOs PA BO
\va mãûng
\dialx BO
\is caractéristiques_objets
\ps v.stat.
\ge sec (linge, feuille) ; asséché (rivière)
\xv e mãû nõõ-nu
\dialx GO
\xn j'ai la gorge sèche
\xv dròò-chaamwa mãû
\dialx GO
\xn feuilles sèches de bananier
\se nu mãû
\dialx GO
\sge coco sec
\se ce mãûng
\dialx BO
\sge bois sec
\dt 20/Oct/2021

\lx mãû
\hm 2
\dialx GOs PA
\is coutumes
\ps n
\ge coutume (cérémonie) ou don coutumier
\dn don coutumier qui accompagne les signes de déclin de la personne jusqu'à sa mort et qui est offert au clan maternel (à l'oncle maternel)
\se mãû kura
\dialx PA
\sge don pour le sang (versé quand on s'est blessé ou que l'on a subi une opération) (lit. don sang)
\se mãû pu-bwaa-n
\dialx PA
\sge don pour les cheveux qui blanchissent (lit. don cheveux)
\se mãû parõ
\dialx PA
\sge don pour la chute des dents (lit. don dents)
\se mãû mèè-n
\dialx PA
\sge don pour la baisse de la vue (lit. don yeux)
\dt 24/Feb/2025

\lx mãû bweera
\dialx GOs
\va mhãû
\dialx PA
\is coutumes
\ps n
\ge coutume de deuil pour une femme
\dn don du mari au clan paternel de l'épouse) [lit. sec foyer]
\cf hauva
\ce don du clan paternel au clan maternel de l'épouse ou de l'époux
\dt 08/Feb/2025

\lx mã-wãge
\dialx GOs
\is santé
\ps n
\ge tuberculose ; tuberculeux
\xv e tròòli mã-wãge
\xn il a attrapé la tuberculose
\xv e mã wãge
\xn il a la tuberculose
\dt 26/Aug/2021

\lx ma-we
\dialx GOs BO
\is eau
\ps n
\ge puits |dialx{GO}
\dn (lit. eau morte)
\ge trou d'eau |dialx{BO}
\ge source occasionnelle |dialx{BO}
\dn ne coule qu'après de grosses pluies
\dt 09/Feb/2025

\lx maxa
\dialx GOs
\va maxal
\dialx PA BO
\sn 1
\ps n
\is temps_atmosphérique
\ge rosée ; brouillard de rivière
\sn 2
\is temps_saison
\ps n
\ge saison froide
\ge époque de maturité de l'igname
\dn de mars à avril
\cf pwebae
\dt 08/Feb/2025

\lx maxewa
\dialx GOs
\is oiseau
\ps n
\ge canard
\dn à gorge blanche
\dt 08/Feb/2025

\lx mãxi
\dialx GOs
\va mãxim
\dialx WEM WE
\va mhãkim, mhãxim
\dialx BO
\va mããxim
\dialx PA
\sn 1
\is action_tête
\ps v
\ge fermer les yeux (pour prier)
\se ba-mãxim
\dialx PA WEM
\sge (coutume pour fermer les yeux ; coutumes de deuil)
\sn 2
\is religion
\ps v
\ge prier
\ge recueillir (se)
\sn 3
\is santé
\ps v
\ge évanoui |dialx{BO}
\ge aveugle |dialx{PA}
\xv a novwö mãxim ye je-nè i ra thu "komãd"
\dialx PA
\xn quant à l'aveugle, elle donne des ordres
\dt 22/Feb/2025

\lx maxuã
\dialx PA
\is cultures
\ps v
\ge glaner de la canne à sucre
\cf zagaò ; zagaòl
\ce glaner (ignames, taros, bananes)
\dt 24/Feb/2025

\lx maya
\dialx PA BO
\va meya
\dialx PA
\is grammaire_adverbe
\ps ADV
\ge lent ; lentement
\dt 21/Mar/2023

\lx mãyo
\dialx GOs
\va maoe
\dialx BO [BM]
\va maü
\dialx BO
\is santé
\ps v
\ge démanger ; gratter ; piquant (sur la peau)
\xv e mãyo bwa-nu
\dialx GOs
\xn ma tête me démange
\dt 09/Feb/2019

\lx mazao
\dialx GOs
\is coutumes
\ps n
\ge coutume (cérémonie) de deuil des femmes issues de la chefferie
\dn une femme de chacun des clans de la chefferie portait deux robes l'une sur l'autre, chacune enlevait une robe qu'elle posait sur la femme assise (la sœur de la défunte ou celle qui a pris le nom de la défunte)
\dt 22/Oct/2021

\lx mazido
\dialx GOs
\is caractéristiques_objets
\ps v
\ge brillant ; scintillant
\dt 25/Mar/2022

\lx mazii
\dialx GOs
\is plantes_partie
\is tressage
\ps n
\ge feuilles de pandanus de creek
\dn feuilles de pandanus |lx{phivwâi} ou de |lx{thra}: utilisées pour tresser des nattes
\cf thra
\dialx GOs
\ce pandanus
\cf thral
\dialx PA
\ce pandanus
\dt 10/Jan/2022

\lx mazilo
\dialx GOs
\is poisson
\ps n
\ge poisson "balabio"
\sc Gerres acinaces
\scf Gerridés
\dt 27/Aug/2021

\lx me
\hm 1
\dialx GOs BO
\va pii-me
\dialx GO(s) PA
\va pi-mèè-n ; mèè-n
\dialx BO
\is corps
\ps n
\ge œil
\xv mee-je
\dialx GO
\xn son œil
\xv mèè-n mali
\dialx PA
\xn ses deux yeux
\xv e kò bwa mee-nu
\dialx GO
\xn il me bloque la vue (il est debout devant mes yeux)
\xv e kò bwa mee-ã
\dialx GO
\xn il se fait remarquer (il est debout devant nos yeux)
\se pu-mèè-n
\dialx PA
\sge sourcils (ses)
\se hê-me
\sge globe oculaire (lit. contenu de l'œil)
\se thala me
\sge ouvrir les yeux
\et *mata
\el POc
\dt 23/Jan/2022

\lx me
\hm 2
\dialx GOs PA
\va mèè-n
\dialx BO
\is corps
\ps n
\ge figure ; visage
\ge apparence
\xv nu ko bwa mee-we
\dialx GO
\xn je suis debout devant vous
\dt 08/Jan/2022

\lx me
\hm 3
\dialx GO
\is grammaire_pronom
\ps PRO 1° pers. triel.EXCL. (sujet, OBJ ou POSS)
\ge nous trois ; à nous trois
\dt 13/Sep/2021

\lx me
\hm 4
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge si ; quand (hypothétique)
\xv yhaamwa !, kavwö nu hine me e trõne
\dialx GOs
\xn je n'en sais rien !, je ne sais pas s'il a entendu
\xv yhaamwa me da la lhò trõne
\dialx GOs
\xn on ne sait pas ce qu'ils ont entendu
\xv yhaamwa me ezoma lhò uça
\dialx GOs
\xn on ne sait pas quand ils arriveront
\dt 24/Feb/2025

\lx me-
\hm 1
\dialx GOs PA BO
\va mèè-n
\dialx BO
\is caractéristiques_objets
\ps n
\ge pointe ; avant
\xv bwa me-õn
\dialx PA
\xn sur la pointe de sable
\se me-do
\dialx GO
\sge la pointe de la sagaie
\se me-loto
\dialx BO
\sge l'avant de la voiture
\et *mata
\el POc
\dt 12/Jan/2022

\lx me-
\hm 2
\dialx GOs BO PA
\sn 1
\is grammaire_dérivation
\ps PREF.NMZ
\ge action de ; façon de ; fait de
\xv nõõli me-phû-wa dònò !
\dialx GO
\xn regarde le bleu du ciel !
\xv nõõli me-mii-wa hõbwòli-je !
\dialx GOs
\xn regarde le rouge de sa robe !
\xv … pune me-piça xo/wo/o bwaa-je
\dialx GOs
\xn … à cause de son entêtement (de la dureté de sa tête)
\xv … pune nye piça bwaa-je
\dialx GOs
\xn … à cause du fait qu'il a la tête dure
\xv kêbwa ai-nu nye me-vhaa i je
\dialx GOs
\xn je n'aime pas sa façon de parler
\xv kêbwa ai-nu nye me-nee-vwo i je
\dialx GOs
\xn je n'aime pas ses manières/ses façons de faire
\xv kavwö nu trõne kaamweni me-vhaa i la
\dialx GOs
\xn je ne comprends pas leur façon de parler
\xv kavwö nu trõne kaamweni nye me-pe-thô i li
\dialx GOs
\xn je ne comprends pas comment ils se sont fâchés
\xv kavwö nu trõne kaamweni nye pe-thô i li
\dialx GOs
\xn je ne comprends pas leur dispute
\xv nu ru phweewe me-nee xa mwarang
\dialx PA
\xn je vais raconter la façon de faire un message coutumier
\xv i pe-kaweeng u me-va i nu
\dialx PA
\xn il imite ma façon de parler
\xv waya me-temwi pwaji ?
\dialx BO
\xn comment attrape-t-on les crabes ? [BM]
\xv yo nõõli nye me-yu i yã
\dialx BO
\xn vous voyez notre façon de vivre [BM]
\se me-bubu i je
\sge sa verdure
\se me-khinu-a nõ mwa
\sge la chaleur à l'intérieur de la maison
\se me-khinu-je
\sge sa souffrance (malade)
\se me-mããni
\sge sa façon de dormir
\se me-yuu i je
\sge sa façon de vivre
\sn 2
\is grammaire_dérivation
\ps DERIV des exclamatifs
\ge exclamatif
\xv me-kõńõõ ! ; me-xõńõõ !
\dialx GOs
\xn paresseux ! (lit. la paresse !)
\dt 24/Feb/2025

\lx mebo
\dialx GOs
\is insecte
\ps n
\ge guêpe ; abeille
\se mebo sapone
\dialx GO
\sge guêpe jaune
\dt 16/Aug/2021

\lx mebo hu-ã
\dialx GOs
\is insecte
\ps n
\ge abeille
\dn (lit. qui donne de la nourriture |lx{hu})
\dt 24/Feb/2025

\lx me-cöni
\dialx BO
\is grammaire_manière
\ps n
\ge peine ; difficulté
\xv i a-da mãni me-cöni
\dialx BO
\xn il monte avec peine
\nt selon Corne
\dt 26/Mar/2022

\lx medatri
\ph me'daɽi
\dialx GOs
\is arbre
\ps n
\ge mandarinier
\dt 29/Jan/2019

\lx me-de
\dialx GO BO
\va mide
\dialx BO
\is maison
\ps n
\ge gaulettes verticales (pointe des)
\dn les pointes des gaulettes forment la corbeille du poteau central de la case (selon Dubois)
\cf moko
\ce gaulettes horizontales enroulées autour des |lx{mede} (Dubois)
\dt 05/Jan/2022

\lx mee
\dialx GOs
\va mèèn
\dialx PA
\is nourriture_goût
\ps v
\ge salé (cuisine)
\xv u mee
\dialx GO
\xn c'est assez salé (quand on goûte dans la marmite)
\xv e mèèn
\dialx PA
\xn c'est salé (quand on goûte en faisant la cuisine)
\cf za !
\dialx GOs PA
\ce c'est trop salé au goût
\cf zanyi
\dialx GOs
\ce sel
\cf òn
\dialx PA BO
\ce sel
\cf õne
\dialx BO
\ce saler
\dt 21/Mar/2023

\lx mèè
\dialx BO
\is grammaire_aspect
\ps v
\ge commencer
\xv nu mèè (na) hovo
\dialx BO
\xn je commence à manger
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx mèèdi
\dialx BO PA
\va meedi
\dialx BO
\is sentiments
\ps v
\ge compatir ; pitié (avoir) ; prendre en pitié
\xv nu mèèdi-je
\dialx BO
\xn j'ai pitié de lui
\dt 27/Jan/2019

\lx meedrèè
\dialx GOs
\va meedèèn
\dialx PA BO
\is temps_atmosphérique
\ps n
\ge temps atmosphérique
\xv cabo meedrèè
\dialx GOs
\xn il fait lourd
\dt 21/Mar/2023

\lx meeji-thre
\ph me:ɲɟi ʈʰe
\dialx GOs
\is poisson
\ps n
\ge "bossu d'herbe"
\sc Lethrinus harak, Lethrinus lentjan
\scf Lethrinidés
\dt 02/Jan/2022

\lx meeli
\dialx GOs
\va melin
\dialx PA
\va mèèlin
\dialx BO
\is insecte
\ps n
\ge chenille (nom générique)
\dt 25/Aug/2021

\lx mèèni
\dialx GOs
\is santé
\ps n
\ge muguet (buccal du bébé)
\dt 09/Feb/2019

\lx mee-phwa
\dialx GOs
\va mee-wha
\dialx GOs
\va dò-phwa-n
\dialx PA
\is corps_animaux
\ps n
\ge bec (plat de canard)
\dt 30/Dec/2021

\lx mee-phwa mèni
\dialx GOs
\va mee-wha mèni
\dialx GO(s)
\is oiseau
\ps n
\ge bec d'oiseau
\dt 30/Dec/2021

\lx mèèvwu
\dialx GO PA BO
\va mèèpu
\dialx arch.
\is parenté
\ps n
\ge frères/sœurs ; phratrie ; sororité
\dn terme général, désignant la relation entre frère et sœur
\dt 08/Feb/2025

\lx mèng
\dialx BO
\is nourriture
\ps v
\ge difficile (qui fait le)
\nt selon Corne
\dt 26/Mar/2022

\lx me-hèlè
\dialx GOs
\is outils
\ps n
\ge lame du couteau ; pointe du couteau
\dt 12/Jan/2022

\lx me-hèlè ba-pe-thra
\dialx GOs
\is outils
\ps n
\ge lame de rasoir
\dt 14/Aug/2021

\lx me-jitrua
\ph 'me'jiɽua
\dialx GOs BO PA
\va me-jitua, me-jirua
\dialx PA
\is armes
\ps n
\ge flèche ; pointe de la flèche
\dt 10/Jan/2022

\lx me-kaze
\dialx GOs
\va me-kale
\dialx BO
\is eau_marée
\ps n
\ge marée montante ; marée haute
\dt 06/Nov/2021

\lx me-kõńõõ
\ph mekɔ̃nɔ̃:
\dialx GOs
\va me-xõńõõ
\sn 1
\is caractéristiques_personnes
\ps v.nmz
\ge paresse ; paresseux ; nonchalant ; mou
\xv me-xõńõõ !
\xn paresseux !
\sn 2
\is caractéristiques_animaux
\ps v.nmz
\ge doux ; apprivoisé (animal)
\dt 08/Feb/2025

\lx meli
\dialx GOs
\is grammaire_nombre
\ps DET duel
\ge marque de duel
\dn des déterminants
\xv êmwê meli-ã
\xn ces deux hommes-ci
\xv êmwê meli-ôli
\xn ces deux hommes-là
\dt 17/Feb/2025

\lx mèloo
\dialx GOs
\va mèloom
\dialx PA BO WEM WE
\va malòm
\dialx BO PA
\is caractéristiques_objets
\ps v.stat.
\ge transparent ; limpide (eau) ; clair
\xv nu kûdo we xa e mèloo
\dialx GOs
\xn je bois de l'eau limpide
\dt 20/Mar/2023

\lx memee
\hm 1
\dialx GOs
\va mêmê
\dialx BO
\is eau_topographie
\ps n
\ge cap
\dt 29/Jan/2019

\lx memee
\hm 2
\dialx PA
\va wamee ne
\dialx GO
\is grammaire_modalité
\ps MODAL
\ge ça doit être ; ce serait bien
\xv memee zo na mi tee-a
\dialx PA
\xn ce serait bien qu'on prenne la route, qu'on parte avant (les autres)
\xv i memee na le xo wha
\dialx PA
\xn c'est peut-être grand-père qui l'a mis là
\xv memee je
\dialx PA
\xn ce doit être elle
\xv ye memee mèni na êneda-ò bwe Kaòla
\dialx PA
\xn ce sont sans doute des oiseaux en haut là-bas sur Kaòla
\xv ra memee thûã wo Teã-mã
\dialx PA
\xn Teã-mã est sans doute en train de mentir
\dt 19/Apr/2024

\lx memexãi
\dialx GOs
\is oiseau
\ps n
\ge rossignol à ventre jaune
\sc Eopsaltria flaviventris
\scf Eopsaltriidés
\gb Yellow-bellied Robin
\dt 27/Aug/2021

\lx me-mwa
\dialx GOs
\va mee-mwa
\dialx PA BO
\sn 1
\is maison
\ps n.loc
\ge devant (le) de la maison
\sn 2
\is nom_locatif
\ps n.loc
\ge sud du pays
\an kaça mwa
\dialx GO
\at arrière de la maison
\an kaya mwa
\dialx PA BO
\at nord du pays
\dt 03/Feb/2025

\lx meń-a-me
\dialx GOs
\is grammaire_comparaison
\ps ADV
\ge tel quel
\dn (lit. c'est la même apparence)
\xv e za meń-a-me nhye pwaamwa-ã
\xn notre champ est resté tel quel
\cf mee
\ce yeux ; apparence
\dt 22/Feb/2025

\lx mene
\dialx GOs
\is poisson
\ps n
\ge mulet queue bleue
\sc Crenimugil crenilabris
\scf Mugilidés
\cf naxo
\ce mulet noir
\cf whai
\ce mulet (de taille juvénile)
\cf jumeã
\ce mulet (taille adulte)
\dt 27/Aug/2021

\lx me-nee-vwo
\ph meɳe:βo
\dialx GOs WEM
\va mèneevwo, mèneevwu-n, mèneexu-n
\dialx BO PA
\is interaction
\ps n
\ge attitude ; comportement ; manière de faire
\xv whaya me-nee-vwo bami ?
\dialx GO
\xn comment fait-on le bami (plat) ?
\xv nu kuele la me-nee-vwo i je
\dialx GO
\xn je n'aime pas ses manières
\xv kavwö nu trõne kaamweni me-nee-vwo i çö
\dialx GO
\xn je ne comprends pas ta façon de procéder
\dt 02/Jan/2022

\lx mèni
\dialx GOs PA BO
\va mèèni
\dialx BO
\is oiseau
\ps n
\ge oiseau
\et *manuk
\el POc
\dt 29/Jan/2019

\lx mèni bwa bolomakau
\dialx GOs
\va mèni bwa bòlòxau
\dialx GO(s)
\is oiseau
\ps n
\ge merle des Moluques
\sc Acridotheres tristis
\scf Sturnidés
\gb Common Myna
\dt 27/Aug/2021

\lx meńixe
\ph meniɣe
\dialx GOs 
\va menixe
\dialx PA BO
\is grammaire_comparaison
\ps v.COMPAR
\ge pareil (être) ; semblable
\xv menixe vhaa kòlò-n
\dialx PA
\xn la parole de son côté est la même
\dt 21/Apr/2024

\lx mènõ
\dialx GOs
\va mènõng
\dialx PA BO
\is plantes_processus
\ps v
\ge fané
\ge séché ; desséché (plantes)
\xv e mènõ khô-kui
\dialx GO
\xn la bouture de l'igname est fanée
\xv i mènõng a muu-n
\dialx BO
\xn la fleur est fanée
\dt 21/Mar/2023

\lx me-phwa
\ph mepʰwa
\dialx GOs
\va me-wha
\dialx GOs
\is corps
\ps n
\ge bouche
\xv me-phwa-n
\dialx PA
\xn sa bouche
\dt 02/Jan/2022

\lx me-phwamwa
\dialx GOs
\is nom_locatif
\ps n.loc
\ge sud
\dn (lit. visage du pays)
\dt 08/Feb/2025

\lx mèra-
\dialx GOs BO
\is insecte
\is préfixe_sémantique
\ps PREF
\ge préfixe des noms de chenilles
\se mèra-chaamwa
\sge chenille du bananier
\se mèra-mõõmõ
\sge chenille de peuplier
\se mèra-uva
\sge chenille verte du taro d'eau
\cf meeli
\ce chenille
\dt 11/Feb/2023

\lx mero
\dialx BO
\is armes
\ps n
\ge casse-tête à bout phallique
\dn est aussi une espèce d'arbre
\nt non vérifié
\dt 08/Feb/2025

\lx merodö
\ph me'rondɷ
\dialx GOs
\is caractéristiques_objets
\ps v
\ge pointu
\xv merodö bwe-mwa
\xn le toit de la maison est pointu
\dt 10/Oct/2021

\lx mèròò
\dialx PA BO WEM WE
\va mããro
\dialx BO [BM]
\is plantes
\ps n
\ge herbe ; pelouse
\xv bwa mèròò
\dialx PA
\xn sur l'herbe
\dt 29/Jan/2019

\lx me-thi
\dialx GOs BO PA
\is corps
\ps n
\ge téton (du sein)
\xv me-thi-n
\dialx BO
\xn son téton
\dt 24/Feb/2025

\lx me-trabwa
\dialx GOs
\is caractéristiques_objets
\ps NMZ
\ge forme
\xv haze me-trabwa mõ-xabu (mõ-kabu), tretrabwau, khawali mee !
\xn la forme du temple est bizarre, elle est ronde et la flèche est très haute !
\dt 03/Feb/2025

\lx mètrô
\dialx GO
\is instrument
\ps n
\ge battoir pour écorce
\nt non vérifié
\dt 27/Jan/2019

\lx mevwuu
\dialx GOs
\is oiseau
\ps n
\ge bengali à bec rouge
\sc Estrilda astrild
\scf Estrildidés
\dn ainsi nommé car est toujours en groupe
\gb Common Waxbill
\dt 27/Aug/2021

\lx me-wõ
\dialx GOs
\va mee-wony
\dialx PA BO
\is navigation
\ps n
\ge proue
\cf gò-wõ ; gò-wony
\dialx PA
\ce milieu du bateau
\an pòbwinõ-wõ ; mura-wõ
\at poupe du bateau
\dt 28/May/2024

\lx mèxèè
\dialx GO BO
\is préparation_aliments
\ps v
\ge cailler
\dt 21/Mar/2023

\lx mexò
\dialx GOs
\is santé
\ps n
\ge varicelle
\ge rougeole
\dt 26/Aug/2021

\lx meyaam
\dialx PA
\is cultures_champ
\ps n ; v
\ge tarodière sèche
\ge planter un champ de taros
\dn aux abords d'une source, sans conduite d'eau
\dt 24/Feb/2025

\lx -mi
\dialx GOs PA BO
\is grammaire_suff_directionnel
\ps DIR (centripète)
\ge vers ici ; vers ego
\xv ã-mi !
\xn viens ici !
\cf du-mi
\ce vers ici en bas
\cf da-mi
\ce vers ici en haut
\dt 11/Oct/2021

\lx mî
\dialx GOs PA BO
\va mhî
\dialx PA BO
\is grammaire_pronom
\ps PRO 1° pers. duel.INCL (sujet)
\ge nous deux
\dt 07/Aug/2023

\lx mibwa
\dialx GOs
\va miibwan
\dialx BO
\is religion
\ps n
\ge être aquatique (nom d'un)
\dn cet être est craint par les femmes enceintes, car il est responsable des déformations physiques, des albinos, etc.
\dt 26/Aug/2021

\lx mii
\dialx GOs PA BO
\sn 1
\is couleur
\ps v.stat.
\ge rouge ; violet
\se dili mii
\sge terre rouge
\se we mii
\sge vin (lit. eau rouge)
\sn 2
\is plantes_processus
\ps v.stat.
\ge mûr
\se chaamwa mii
\sge banane mûre
\et *maiRa, *meRaq
\eg rouge
\el POc
\dt 29/Jan/2019

\lx mîjo
\dialx BO JAWE
\is arbre
\ps n
\ge gaïac
\dt 29/Jan/2019

\lx mimaalu
\dialx BO
\is caractéristiques_objets
\ps v
\ge invisible
\se a-mimaalu
\sge un être invisible
\nt selon Corne
\dt 26/Mar/2022

\lx mimi
\dialx GOs
\va minòn
\dialx PA BO
\is mammifères
\ps n
\ge chat
\dt 29/Jan/2019

\lx mini-
\dialx GOs
\is nourriture
\ps n
\ge débris ; restes
\se mini-mã-bò
\sge les restes de nourriture mâchée par les roussettes
\se mini-cee
\sge sciure
\cf jaa-cee
\ce copeaux
\dt 23/Jan/2022

\lx mini nu
\dialx GOs PA BO
\is arbre_cocotier
\ps n
\ge résidu de coco
\dt 17/Aug/2021

\lx mińõ
\dialx GOs
\va minòng
\dialx PA BO
\is préparation_aliments
\ps v
\ge cuit
\xv u mińõ dröö
\dialx GOs
\xn les marmites sont prêtes (lit. sont cuites)
\xv u minòng doo
\dialx PA
\xn la marmite est prête
\xv ra u minòng mwã la hovwo
\dialx PA
\xn la nourriture est cuite
\cf phuu
\ce cuit
\an mãiyã, meã
\at mal cuit
\dt 21/Mar/2023

\lx minyõ
\dialx GOs
\is bananier
\ps n
\ge banane
\dn espèce de petite taille qui se mange bien mûre
\dt 08/Feb/2025

\lx mò
\dialx PA, BO
\is cultures
\ps v.i
\ge arracher (la paille)
\xv i mò mhae
\xn il arrache de la paille
\ng v.t. |lx{mòne, m(w)òni}
\dt 03/Feb/2025

\lx mõ
\hm 1
\dialx GOs
\va mòl
\dialx PA BO
\sn 1
\is caractéristiques_objets
\ps v.stat.
\ge asséché ; sec (rivière, etc.) ; peu profond
\xv u mõ
\xn c'est sec
\xv e mõ-du we
\xn l'eau a baissé/s'est retirée
\sn 2
\is topographie
\ps LOC
\ge à terre
\se bwa mõ
\dialx GO
\va bwa mòl
\dialx PA
\sge à terre (par rapport à la mer)
\ng causatif: |lx{paa-mòle, paa-moze}
\gt vider qqch.
\et *ma-masa
\eg sec, marée basse
\el POc
\dt 22/Feb/2025

\lx mõ
\hm 2
\ph mɔ̃
\dialx GOs WEM
\is grammaire_pronom
\ps PRO 1° pers. triel.INCL. (sujet)
\ge nous trois ; on
\xv mõgu i õ, ce mõ u pe-nee-kuuni
\xn notre tâche, nous l'avons menée à terme ensemble
\dt 19/Aug/2023

\lx mõ-
\ph mɔ̃
\dialx GOs PA BO
\va mwõ-
\dialx GO PA
\sn 1
\is maison
\ps PREF (de contenant)
\ge maison ; maison (grande chefferie)
\xv mõ-çö
\dialx GOs
\xn ta maison
\xv mwõ-õ
\dialx GOs
\xn c'est votre maison
\xv gu mwa i ã na ni mõ Treã Gome
\dialx GOs
\xn notre grande maison dans la chefferie de Treã Gomen
\xv mwa-dili mõ-nu
\dialx GOs
\xn ma maison en terre
\xv mõ-nu ca mwa-dili
\dialx GOs
\xn ma maison est en terre
\xv whaa mõ-ny (a) mwa-mãe
\dialx PA
\xn ma grande maison en paille
\xv ia mõ-m ?
\dialx BO
\xn où est ta maison ?
\xv mwa mõ-hovo
\dialx BO
\xn garde-manger (maison pour nourriture)
\xv mõ-da ? – Mõ-pe-rooli, mõ-cia
\xn une maison pour quoi?/qui sert à quoi ? – Une maison de réunion, une maison de danse
\xv mõ-ti ? –  Mõ-ãbaa-nu  (*mõ-ri : incorrect)
\dialx GOs
\xn la maison de qui ?  –  La maison de mon frère
\ng forme déterminée de |lx{mwa}
\sn 2
\is préfixe_sémantique
\ge contenant de qqch.
\se mõ-wetin
\dialx PA BO
\sge encrier
\sn 3
\is préfixe_sémantique
\ge manche (vêtement, couteau)
\cf hê-
\ce préfixe désignant un contenu
\dt 24/Feb/2025

\lx mõã
\dialx GOs WEM
\va mhõ
\dialx PA WEM
\va mõ
\dialx BO
\is nourriture
\ps n
\ge restes de nourriture
\ge provisions de route 
\xv mõã-nu
\dialx GOs
\xn mes restes de nourriture
\xv çö kaale xa mhõ-ny
\dialx WEM
\xn laisse-moi de la nourriture
\xv mhõ-la
\dialx PA BO
\xn leurs vivres
\xv la thu m(h)õ
\dialx PA BO
\xn ils préparent des vivres
\se ke-mõã
\dialx GOs
\sge panier à restes
\ng forme déterminée: |lx{m(h)õã}
\dt 08/Feb/2025

\lx mõ-butro
\ph mɔ̃'buɽo
\dialx GOs
\is maison
\ps n
\ge douche (lieu)
\dt 02/Jan/2022

\lx mõ-caaxo
\dialx GO
\va mõ-caao
\dialx GO
\is maison
\ps n
\ge maison où se retirent les femmes (pendant les menstruations)
\dn (lit. où l'on se cache)
\dt 08/Feb/2025

\lx mõ-cãna
\dialx GOs BO
\is corps
\ps n
\ge poumon
\dn (lit. contenant-respiration)
\xv mõ-cana-n
\dialx BO
\xn ses poumons
\dt 09/Feb/2025

\lx mõ-do
\dialx BO
\is armes
\ps n
\ge manche de la sagaie
\dt 26/Jan/2018

\lx mõ-ẽnõ
\ph mɔ̃ ɛ̃ɳɔ̃
\dialx GOs BO
\is corps
\ps n
\ge utérus
\ge placenta
\dt 28/May/2024

\lx mõgavwo
\ph mɔ̃'gaβo
\dialx GOs
\va mûgavo
\dialx PA
\is portage
\ps n
\ge corde de portage (des fagots)
\dt 25/Jan/2019

\lx môgo
\dialx GOs
\va mogòn
\dialx BO PA
\va mûgòn
\dialx BO
\is santé
\ps v
\ge migraine (avoir la) ; mal de tête (avoir un)
\xv nu môgo
\dialx GO
\xn j'ai mal à la tête
\xv nu mogòn
\dialx PA
\xn j'ai mal à la tête
\dt 26/Aug/2021

\lx mõgu
\dialx GOs BO
\va mwõgu
\dialx GO(s)
\sn 1
\is action
\ps v ; n
\ge travailler ; travail
\xv mõgu i õ
\xn notre tâche
\xv e õgine mõgu ẽnõ ã
\dialx GO
\xn il a fini le travail de l'enfant
\se ba-mõgu
\sge outils
\se mhènõ-mõgu
\sge lieu de travail
\se mhènõ ba-mõgu
\sge appentis à outils
\se mõgu-raa
\sge travailler mal ; faire de travers
\sn 2
\is fonctions_naturelles
\ps n
\ge sueur |dialx{BO}
\dt 22/Feb/2025

\lx mõ-hèlè
\dialx GOs BO PA
\is instrument
\ps n
\ge étui ; fourreau de couteau ; manche
\dt 27/Jan/2019

\lx mõ-hovwo
\dialx GOs
\va mõ-(h)ovwo
\dialx GO
\va mõ-hopo
\dialx GO arch.
\is corps
\ps n
\ge estomac
\dt 27/May/2024

\lx mõ-ima
\dialx GOs WEM BO
\is corps
\ps n
\ge vessie
\xv mõ-ima-n
\dialx BO
\xn sa vessie
\dt 24/Jan/2019

\lx mõ-iyu
\dialx BO
\is maison
\ps n
\ge magasin ; boutique
\dt 26/Jan/2019

\lx mõ-ja
\dialx GOs
\is maison_objet
\ps n
\ge poubelle
\dt 26/Jan/2019

\lx mõ-kabun
\dialx PA BO
\is religion
\ps n
\ge église ; temple
\dt 30/Dec/2021

\lx moko
\dialx BO
\is maison
\ps n
\ge gaulettes horizontales
\dn enroulées autour des |lx{me-de} pour former la corbeille du poteau central de la case (selon Dubois)
\nt non vérifié
\dt 05/Jan/2022

\lx mõlò
\ph mɔ̃lɔ
\dialx GOs
\va mòlò
\dialx PA BO
\va mòòlè
\dialx BO
\sn 1
\is étapes_vie
\ps v ; n
\ge vivre ; vivant ; vie ; existence
\xv na ni mõlò i ã
\dialx GOs
\xn dans nos vies
\xv mòlò-n
\dialx PA
\xn sa vie ; son existence
\xv li pe-mòlò bulu
\dialx PA
\xn ils vivent ensemble
\xv i mòlò kuraa-li
\dialx PA
\xn ils sont nerveux/agités (lit. leur sang vit)
\xv i mòlò phãgoo-n
\dialx BO
\xn il est vif/courageux
\se me-mòlò
\dialx PA BO
\sge vie
\sn 2
\is coutumes
\ps n
\ge coutumes
\xv mõlò-w-a kêê-ã mãni kibu-ã
\dialx GOs
\xn les coutumes de nos pères et grands-pères
\sn 3
\is fonctions_naturelles
\ps v
\ge rassasié
\xv nu mõlò
\dialx GO
\xn je suis rassasié ; j'ai assez mangé
\et *maqurip
\eg vie, vivre
\el POc
\dt 22/Feb/2025

\lx mõ-mãxi
\dialx GOs
\is religion
\ps n
\ge protestants (les)
\dt 21/Feb/2025

\lx mòne
\dialx PA
\va m(w)òni
\dialx BO [BM]
\is cultures
\ps v.t.
\ge arracher (la paille)
\ng v.i. |lx{mò}
\dt 06/Jan/2022

\lx mõnõ
\hm 1
\ph mɔ̃ɳɔ̃
\dialx GOs
\va mènòòn, mõnòn
\dialx PA WEM WE BO
\ph mɛnɔ:n, mɔ̃ɳɔ̃n
\is temps_deixis
\ps ADV
\ge demain ; lendemain (le) ; prochain
\xv mènòòn mãni bwòna
\dialx BO
\xn demain et après-demain
\xv yu ruma a mènòòn
\dialx PA
\xn tu partiras demain
\se bwò-na, kaça mõnõ
\dialx GO
\sge après-demain
\se mõnõ ne/na waa
\dialx GO
\sge demain matin
\se mõnõ ne thrõbwò
\dialx GO
\sge demain soir
\se kaa mõnõ
\dialx GO
\sge l'an prochain
\dt 02/Jan/2022

\lx mõnõ
\hm 2
\ph mɔ̃ɳɔ̃
\dialx GOs
\va mõnô, mwõnô
\dialx BO
\is nourriture
\ps n
\ge graisse (de tortue uniquement)
\ge huileux ; graisseux
\se mõnõ pwò
\sge graisse de tortue
\xv i mõnô cii-ny
\dialx BO
\xn ma peau est grasse
\et *moɲak
\eg graisse
\el POc
\dt 25/Aug/2021

\lx mõnu
\ph mɔ̃ɳu
\dialx GOs PA BO
\va mwonu
\dialx BO
\wr A
\is grammaire_préposition_locative
\ps v.loc (spatio-temporel)
\ge proche ; près
\xv tho ko ne mõnu trèè
\dialx GO
\xn le coq chante quand le jour est proche
\xv e za u hine-je mwã na mõnu
\dialx GO
\xn il le reconnaît alors quand il est proche
\xv kò-mõnu kaò
\dialx PA
\xn la saison des pluies est proche
\xv mõnu tree
\dialx GO
\xn il va bientôt faire jour
\xv u mõnu u tèèn
\dialx BO
\xn il va bientôt faire jour
\wr B
\is grammaire_adverbe
\ps ADV.LOC
\ge proche ; près
\xv ge mõnu
\xn il est proche
\xv ge-li mõnu
\xn ils sont proches
\wr C
\is grammaire_aspect
\ps v.ASP (imminent)
\ge sur le point de ; bientôt
\ge presque
\xv mõnu vwo lha a
\dialx GO
\xn ils sont sur le point de partir
\an hòò
\at loin
\dt 16/Feb/2025

\lx mõõ-
\dialx GOs PA
\va mwòòn
\dialx BO
\is parenté_alliance
\ps n
\ge beau-père ; belle-mère 
\dn (père/mère d'épouse ; père/mère du mari)
\ge gendre (mari de fille) ; belle-fille (épouse de fils)
\ge beau-père ; beau-fils
\dn le terme duel tombe en désuétude
\se mõõ-n
\dialx PA
\sge beau-fils, son beau-fils ; beau-père, son beau-père
\se mõõ-n thoomwã, mõõ-n doomwã
\dialx GO PA
\sge belle-fille, sa belle-fille ; belle-mère, sa belle-mère
\dt 22/Feb/2025

\lx mõõdi
\dialx GOs
\va mõõdim
\dialx WEM WE PA BO
\sn 1
\is sentiments
\ps v
\ge honte (avoir) (contexte de deuil)
\dn de ne pas avoir su conserver la vie, terme lié au deuil
\xv e mõõdi pexa la khõbwe ?
\dialx GOs
\xn a-t-il honte de ce qu'il a dit ?
\xv ôô ! e mõõdi !
\dialx GOs
\xn Oui, il en a honte !
\xv nu mõõdim na nu va
\dialx BO
\xn j'ai peur de parler
\sn 2
\is coutumes
\is interaction
\ps n
\ge deuil ; coutume de deuil
\dn cérémonie de deuil destinée au clan maternel
\cf cöńi, giul, thiin
\ce pleurer un mort ; être en deuil |dialx{BO, PA}
\dt 24/Feb/2025

\lx mõõmõ
\ph mɔ̃:mɔ̃
\nph nasalité légère
\dialx GOs PA BO
\sn 1
\is arbre
\ps n
\ge peuplier kanak
\dn représente la terre et la femme
\sc Erythrina indica Lam.
\scf Fabacées
\sn 2
\is arbre
\ps n
\ge érythrine à épines
\sc Erythrina fusca Lour
\scf Fabacées
\dt 27/Oct/2021

\lx mõõxi
\dialx GOs PA
\va mòòlè
\dialx BO
\sn 1
\is étapes_vie
\ps n
\ge vie (principe de vie)
\ge salut (le) (religion)
\xv e kae mõõxi-nu
\dialx GO
\xn il m'a sauvé la vie
\xv e kae mõõxi-ny
\dialx PA
\xn il m'a sauvé la vie
\sn 2
\is parenté
\ps n
\ge descendance
\xv kixa mõõxi
\dialx GO
\xn il est sans descendance (hommes) ; elle est morte (plante)
\sn 3
\is plantes
\ps n
\ge bouture
\xv nu thu mõõxi
\dialx GO
\xn je fais des boutures (de plantes ; le bouturage est lié à la notion de vie)
\et *maqudip
\el POc
\dt 24/Feb/2025

\lx mõõxõ
\dialx GOs
\va mòòxòm
\dialx BO
\is action
\ps v
\ge noyer (se)
\xv çö zoma mõõxõ na ni we
\xn tu pourras te noyer dans la mer
\dt 21/Mar/2023

\lx mõ-puçò
\dialx GO
\va mõ-puyòl
\dialx PA BO
\va mõ-wuyòl
\dialx BO
\is maison
\ps n
\ge cuisine
\dt 21/Mar/2023

\lx mõ-pwa
\dialx GO
\va mõ-pwal
\dialx BO
\sn 1
\is instrument
\ps n
\ge parapluie
\sn 2
\is plantes
\ps n
\ge champignon
\dn nom métaphorique
\dt 27/May/2024

\lx mõ-phaa-ce-bo
\dialx GOs
\va mõ-phaa-ce-bòn
\dialx WEM PA
\is feu
\ps n
\ge foyer ; maison où l'on fait le feu pour dormir
\dt 08/Oct/2021

\lx mõ-pha-yai
\dialx GOs WEM
\is feu
\ps n
\ge foyer
\dt 23/Aug/2021

\lx mõ-phòò
\dialx GOs
\is maison
\ps n
\ge toilettes ; cabinet
\dt 26/Jan/2019

\lx mõ-phwayuu
\dialx GOs
\va mwa-phwayu
\dialx GOs PA
\va mwa-pwaeu
\dialx BO
\is maison
\ps n
\ge maison où se retirent les femmes (pendant les menstruations)
\dt 25/Aug/2021

\lx mora
\dialx BO PA
\is santé
\ps v
\ge épuisé ; éreinté
\dt 25/Jan/2019

\lx môre
\ph mõɽe
\dialx GOs
\is discours
\ps v
\ge parler du nez
\dt 28/Jan/2019

\lx môtra ẽnõ
\ph mõʈa ẽɳɔ̃
\dialx GOs
\va möra ẽnõ
\dialx PA
\is parenté
\ps n
\ge benjamin
\dt 18/Aug/2021

\lx mõ-tri
\ph mõʈi
\dialx GOs
\is ustensile
\ps n
\ge bouilloire
\dn (lit. contenant-thé)
\dt 08/Feb/2025

\lx mõ-trò
\dialx GOs
\is eau_marée
\ps n
\ge marée basse du soir
\dt 21/Mar/2023

\lx mõû-
\dialx GOs
\va mõû-, maû
\dialx PA BO
\is parenté
\ps n
\ge épouse ; sœur de l'épouse ; épouse du frère
\xv la mõû-je
\dialx GO
\xn elles sont ses épouses
\xv mõû-nu
\dialx GO
\xn mon épouse; ma belle-sœur (épouse du frère)
\xv mõû-ny
\dialx PA BO
\xn mon épouse
\cf phalawu
\dialx BO
\ce belle-sœur
\dt 18/Mar/2021

\lx mõ-vhaa
\dialx GOs
\is maison
\ps n
\ge lieu de discussion
\dt 24/Jan/2022

\lx mõ-wae
\dialx WEM BO
\is maison
\ps n
\ge maison au toit à deux pentes
\dt 25/Aug/2021

\lx mõ-we
\dialx GOs
\sn 1
\is instrument
\ps n
\ge contenant à liquide ; calebasse
\sn 2
\is plantes
\ps n
\ge lys d'eau |dialx{BO}
\dt 08/Jan/2022

\lx mõ-yai
\dialx GOs WEH WEM PA
\is feu
\ps n
\ge allumettes ; boîte d'allumettes
\dn (lit. boîte à feu)
\dt 09/Feb/2025

\lx mòza
\dialx GOs
\va mora
\dialx BO PA
\is caractéristiques_personnes
\ps v
\ge las ; fatigué ; en avoir assez
\dt 26/Jan/2019

\lx mozi
\dialx GOs
\va mhõril
\dialx PA
\is fonctions_naturelles.
\ps v
\ge pleurnicher (enfant) ; sangloter
\dt 29/Aug/2021

\lx mu
\dialx GOs
\va mun
\dialx PA BO
\sn 1
\is grammaire_préposition_locative
\ps PREP
\ge derrière (être)
\xv mu nai çö
\dialx GO
\xn derrière toi
\xv mun i yu
\dialx PA GO
\xn derrière toi
\cf kai-nu
\ce juste dans mon dos
\sn 2
\is grammaire_locatif
\ps ADV
\ge après ; ensuite
\xv ge mu nye loto
\dialx GO
\xn la voiture est derrière
\xv ge mun nye loto
\dialx PA
\xn la voiture est derrière
\se yuu mu
\dialx GO
\sge rester derrière/en arrière
\dt 22/Feb/2025

\lx mû
\hm 1
\dialx GO
\va muu
\dialx PA
\va muuc
\dialx BO
\is plantes_processus
\ps v ; n
\ge fleur ; fleurir
\xv e mû mû-cee
\dialx GOs
\xn la fleur fleurit
\dt 08/Jan/2022

\lx mû
\hm 2
\dialx GOs BO
\va mûû
\dialx BO
\sn 1
\is fonctions_naturelles
\ps v
\ge ronfler |dialx{BO}
\sn 2
\is son
\ps v
\ge bourdonner ; faire un bruit de bourdon
\dt 23/Jan/2022

\lx mû-ce-dròò
\dialx GOs
\is plantes
\ps n
\ge nom des plantes à feuilles multicolores
\dn (lit. fleur-arbre-feuille)
\ge croton 
\dt 08/Feb/2025

\lx mû-cee
\dialx GOs
\is plantes_partie
\ps n
\ge fleur
\dt 08/Nov/2021

\lx mû-chaamwa
\dialx GOs
\is bananier
\ps n
\ge inflorescence de bananier
\dt 26/Aug/2021

\lx mudim
\dialx BO
\is mammifères_marins
\ps n
\ge dugong
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx mudra
\dialx GOs WEM
\va muda, môda
\dialx BO [BM]
\is action
\ps v.stat.
\ge déchiré (être) ; cassé
\xv i muda
\dialx BO
\xn c'est déchiré
\ng v.t. |lx{mudree} |dialx{GO}; |lx{mudee} |dialx{WEM, BO}
\gt casser qqch.
\dt 21/Feb/2025

\lx mudree
\dialx GOs
\va modree
\dialx GOs
\va mudee, môdee
\dialx BO [BM]
\is action_corps
\ps v.t.
\ge casser ; rompre (corde) ; déchirer
\se hu-mudree
\sge déchirer avec les dents
\dt 22/Mar/2023

\lx mudro
\dialx GOs
\va mudrã
\dialx GO(s)
\va mudo
\dialx PA
\va muda, mudo
\dialx BO
\is caractéristiques_objets
\ps v.stat. ; nom.adj.
\ge vieux ; usé (linge)
\ge haillons ; loques
\ge délabré
\xv mudro mwa
\dialx GO
\xn maison délabrée
\xv mudo hõbwòn
\dialx PA
\xn vêtements usés
\xv mudo-n
\dialx BO
\xn c'est vieux/usé
\dt 22/Feb/2025

\lx muga
\dialx GOs BO PA
\is fonctions_naturelles
\va muuga
\dialx BO
\ps v ; n
\ge vomir ; vomissure
\et *muta(q)
\eg vomir
\el POc
\dt 30/Jan/2019

\lx mugo
\dialx GOs BO
\va muge
\dialx PA WEM BO
\is bananier
\ps n
\ge banane ; bananier de la chefferie
\dn on ne peut que la bouillir, il est interdit de la griller
\se mugo mii
\sge banane mûre
\dt 15/Sep/2021

\lx muna-le
\dialx GOs PA
\is grammaire_adverbe
\ps ADV
\ge ensuite ; après
\dt 12/Sep/2021

\lx munõ
\ph muɳɔ̃
\dialx GOs
\va muunõ
\dialx BO
\is action_eau_liquide_fumée
\ps v
\ge arroser (fleurs)
\ge éteindre (feu)
\se munõ yai
\sge éteindre le feu
\dt 20/Aug/2021

\lx mura
\dialx PA BO
\va murò
\dialx BO
\va mun
\dialx PA
\sn 1
\is grammaire_locatif
\ps PREP
\ge après ; derrière
\xv murò-n
\dialx BO
\xn après lui
\sn 2
\is grammaire_conjonction
\ps CNJ
\ge après que
\xv mura na nu hovwo
\dialx PA
\xn après que j'aurai mangé
\an hêbu
\dialx GOs
\at avant, devant
\an hêbun
\dialx PA
\at avant, devant
\et *muri-
\eg rear, stern
\el POc
\ea Blust
\dt 10/Jan/2022

\lx murae
\dialx GOs BO
\is parenté
\ps n
\ge benjamin
\dt 27/Jan/2019

\lx mura-hovwo
\dialx PA
\va murò hovo
\dialx BO
\is temps_découpage
\ps n
\ge après-midi
\dn (lit. après le déjeuner)
\dt 08/Feb/2025

\lx mura-wõ
\dialx GOs
\is navigation
\ps n
\ge poupe
\an mè-wõ
\at proue du bateau
\et *muri-
\eg rear, stern
\el POc
\ea Blust
\dt 25/Aug/2021

\lx murò
\dialx GOs
\is habillement
\ps n
\ge couverture (pour dormir)
\xv muròò-nu
\xn ma couverture
\dt 26/Jan/2018

\lx muswa
\dialx GOs
\is habillement
\ps n
\ge mouchoir
\bw mouchoir (FR)
\dt 20/Feb/2018

\lx muzi
\dialx GOs
\va mulin
\dialx PA
\is coutumes_objet
\ps n
\ge plumet de monnaie
\dt 27/Jan/2019

\lx mwa
\dialx GOs
\is maison
\ps n
\ge maison
\se mwa pho
\dialx GO
\sge la maisonnée (lit. maison tressage) ; désigne les femmes et enfants
\xv mwa za ? – Mwa dili
\dialx GO
\xn quelle sorte de maison ? – Une maison en terre
\xv mwa xa whaiya ? – Mwa xa tretrabwau
\dialx GO
\xn quelle sorte de maison ? – Une maison ronde
\xv mõõ-nu
\dialx GOs
\xn ma maison
\se mwa-dinyo
\dialx BO
\sge case des plantations (Corne)
\se bu mwa
\sge emplacement de maison
\se bwaxeni mwa
\sge tertre
\se ce mwa
\sge solives
\se mwa huren
\dialx PA
\sge maison vide/abandonnée
\ng voir la forme déterminée: |lx{mõ(õ)}
\et *Rumaq
\el POc
\dt 24/Feb/2025

\lx mwã
\hm 1
\dialx GOs
\sn 1
\is grammaire_aspect
\ps ADV.SEQ (continuatif)
\ge alors ; continuer à
\xv mwã içö !
\xn à ton tour !
\cf draa içö !
\ce à ton tour !
\sn 2
\is grammaire_aspect
\ps ASP changement d'état (u … mwã)
\ge enfin
\xv i u mã mwã
\dialx PA
\xn il est mort
\xv i mwã hangai
\dialx BO
\xn il grossit
\dt 20/Oct/2021

\lx mwã
\hm 2
\dialx GO PA
\sn 1
\is déplacement
\ps v.réversif
\ge retour (faire en)
\xv lhi a-mwã(ã)-mi
\dialx GOs
\xn ils sont revenus
\xv axe novwö kaze, mwã yua a-da, yua a-da.
\xn mais, quant à la marée, elle ne cesse de remonter, ne cesse de monter
\sn 2
\is grammaire_aspect
\ps ASP réversif-itératif
\ge re- ; à nouveau
\xv a mwã(ã)-mi çö!, zòò mwã ã
\xn reviens ici toi !, il revient en nageant
\se a-mwã-e
\sge repartir
\se a-daa mwã
\sge remonter
\se na mwã
\sge rendre
\dt 24/Feb/2025

\lx mwã
\hm 3
\dialx GOs
\va mhwã
\dialx PA
\is grammaire_pronom
\ps PRO 1° pers.PL.INCL (sujet)
\ge nous (incl.)
\xv ègòl, thu je-ne waalei mhwã pwayi
\dialx PA
\xn autrefois, il y avait ces ignames que nous épluchions
\dt 06/Nov/2021

\lx mwãã
\dialx GOs
\is interaction
\ps v
\ge soutenir ; appuyer
\xv mhenõ-mwãã mãni mhenõ-kea
\xn nos soutiens et nos appuis
\dt 18/Sep/2021

\lx mwa-aravwa
\dialx GOs WEM
\va mwa-alaba, mwa-halapa
\dialx BO
\is maison
\ps n
\ge maison à toit plat ; maison au toit à deux pentes
\dt 21/Mar/2023

\lx mwããxe
\hm 1
\dialx GOs PA
\is action
\ps v
\ge redresser (fer)
\cf var. mhwãnge
\dt 08/Jan/2022

\lx mwããxe
\hm 2
\dialx BO
\is action
\ps v
\ge tordre (du fer)
\nt selon BM
\dt 27/Mar/2022

\lx mwa-çii
\dialx GO
\is préparation_aliments
\ps MODIF
\ge peau (cuire avec la)
\xv e phai walei mwa-çii
\xn elle cuit l'igname (sucrée) avec la peau
\mr |lx{mwa-cii} ou |lx{mwa-çii} (lit. enveloppe-peau)
\dt 08/Jan/2022

\lx mwacoa
\dialx PA
\is igname
\ps n
\ge igname ronde
\nt selon Dubois et Charles Pebu-Polae
\dt 08/Feb/2025

\lx mwa-draeca
\dialx GOs
\is navigation
\ps n
\ge pirogue double
\dt 26/Jan/2019

\lx mwang
\dialx PA BO
\is caractéristiques_personnes
\ps v.stat.
\ge mauvais ; mal
\xv i wal mwang
\dialx BO
\xn il chante mal
\dt 26/Jan/2019

\lx mwagi
\dialx GOs
\va mwagin
\dialx PA
\is oiseau
\ps n
\ge cagou (sorte de)
\sc Rhynochetos jubatus
\scf Rhynochetidés
\dt 27/Aug/2021

\lx mwãgi
\dialx GOs
\is plantes
\ps n
\ge cactus ; épineux
\dt 22/Feb/2025

\lx mwa-gol
\dialx BO
\va mwa-ol
\dialx BO
\is maison
\ps n
\ge abri de fortune
\nt selon Dubois, BM
\dt 27/Mar/2022

\lx mwa-hovwo
\dialx GOs
\is maison
\ps n
\ge garde-manger ; endroit où l'on garde la nourriture
\ge cantine
\dt 27/May/2024

\lx mwaitri
\ph mwaiʈi
\dialx GO
\is arbre
\ps n
\ge mûrier
\dt 29/Jan/2019

\lx mwaje
\dialx GOs PA
\is grammaire_manière
\ps n
\ge manière de faire ; façon de faire ; procédure
\xv mõ nee mwã xa mwaje kêê-ã mãni kibu-ã
\dialx GO
\xn nous voulons procéder à la façon de
nos pères et grands-pères
\dt 21/Feb/2025

\lx mwaji-n
\dialx PA
\is temps
\ps n
\ge temps ; durée
\xv pwali mwaji-n ?
\dialx PA
\xn combien de temps ?
\xv au mwaji-n
\dialx PA
\xn il est en retard
\xv au mwaji-m
\dialx PA
\xn ton retard
\dt 17/Oct/2021

\lx mwãju
\ph mwɛ̃ɲju
\dialx GOs PA BO
\va mwèju
\dialx GOs
\va mwaji
\dialx BO
\sn 1
\is déplacement
\ps v
\ge retourner (s'en)
\ge demi-tour (faire)
\ge revenir sur ses pas
\xv i ra u mwãju mwã a-ò
\dialx GO
\xn l'homme s'en retourne
\xv têên mwãju xo je
\dialx PA
\xn il revient en courant
\xv mwaji-a
\dialx BO
\xn votre retour (BM)
\cf pwaa
\dialx PA
\ce retourner (s'en) ; faire demi-tour ; revenir sur ses pas
\sn 2
\is coutumes
\ps v ; n
\ge contre-don (dans les dons coutumiers)
\ge rendre (la monnaie)
\xv mwãju kaja êgu
\dialx PA
\xn contre-don (lit. don après la personne)
\se mwãju kai
\dialx GO PA
\sge contre-don
\dt 10/Jan/2022

\lx mwa-kabu
\dialx GO
\is religion
\ps n
\ge église ; temple
\dt 28/Jan/2019

\lx mwani
\ph mwaɳi
\dialx GOs PA BO
\is richesses
\ps n
\ge argent
\bw money (GB)
\dt 27/Jan/2019

\lx mwâô
\dialx BO
\is mouvement
\ps v
\ge glisser
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx mwa-paa
\dialx PA
\is habitat
\va mwa-vaa
\dialx PA
\ps n
\ge abri dans/sous un rocher
\dt 26/Jan/2018

\lx mwa-pe-cinô
\dialx BO
\is maison
\ps n
\ge maison carrée
\dt 26/Jan/2019

\lx mwa-puçò
\dialx GOs
\va mwa pujò, mwa-wujò
\dialx GOs PA
\va mõ-puyòl
\dialx BO
\is maison
\ps n
\ge cuisine (lieu)
\dt 25/Aug/2021

\lx mwa-pho
\dialx GOs WEM
\va mwa-wo
\dialx GO(s) WEM
\is maison
\ps n
\ge maison où dorment femmes et enfants
\dn (lit. maison du tressage des feuilles de pandanus)
\cf pho
\ce tressage
\dt 08/Feb/2025

\lx mwa-phwamwêêgu
\dialx GO BO PA
\va mwa-phwamwãgu
\dialx PA
\is maison
\ps n
\ge maison des hommes
\dn grande maison servant de lieu de réunion
\cf gu-mwa
\dt 25/Aug/2021

\lx mwathra
\dialx GOs
\va mwara
\dialx GO(s)
\va mwarang
\dialx BO
\is guerre
\ps n
\ge nœud de guerre
\dn annonce coutumière
\ge message de guerre
\dn message transmis de chef en chef pour chercher des alliés lors des guerres
\dt 08/Feb/2025

\lx mwa-vèle
\dialx GOs PA
\is maison
\ps n
\ge abri (de champs)
\dn abri comportant une plate-forme sur laquelle on entrepose les récoltes
\dt 25/Aug/2021

\lx mwê
\dialx GOs
\va mwèn
\dialx PA
\va mwãulò
\dialx WE
\va mwãulòn
\dialx BO
\is oiseau
\ps n
\ge chouette
\sc Tyto alba lifuensis
\scf Tytonidés
\gb Barn Owl
\dt 27/Aug/2021

\lx mweau
\dialx GOs WEM PA
\is société_organisation
\ps n
\ge fils cadet de chef
\dt 27/Jan/2019

\lx mweça
\ph mweʒa
\dialx GOs
\is caractéristiques_personnes
\ps v.stat.
\ge vigoureux ; costaud ; courageux (qualifie le corps)
\dt 16/Jan/2024

\lx mwêê
\ph mwẽ:
\dialx GOs
\va mwêêng
\dialx PA BO
\ph mwẽ:ŋ
\is habillement
\ps n
\ge coiffe ; chapeau
\xv mwêênga-nu, mwêêxa-nu
\dialx GO
\xn mon chapeau
\xv i khia mwêêga-n
\dialx PA
\xn il met sa coiffe
\xv mwêêga-n
\dialx BO
\xn son chapeau
\ng forme déterminée: |lx{mwêêga, mwêêxa}
\dt 21/Mar/2023

\lx mwêêjè
\ph mwẽ:ɲɟɛ
\dialx GOs PA
\is coutumes
\ps n
\ge us et coutumes ; comportement ; attitude ; manière
\xv mwêêjè-je
\dialx GO
\xn son comportement
\xv zo mwêêjè-n
\dialx PA
\xn il a un comportement agréable
\dt 05/Jan/2023

\lx mwèèn
\dialx BO [BM]
\is plantes
\ps n
\ge cycas
\dt 29/Jan/2019

\lx mwêêno
\ph mwẽ:ɳo
\dialx GOs BO
\sn 1
\is grammaire_quantificateur_mesure
\ps v.IMPERS
\ge manque (il) ; rester
\xv mwêêno po-kò tiivwo !
\dialx GO
\xn il manque trois livres !
\xv mwêêno gò po-tru phwe-meevwu !
\dialx GO
\xn il manque encore deux clans !
\xv mwêêno içö mwã !
\dialx GO
\xn il ne reste plus que toi (lit. il manque toi qui dois encore jouer)
\xv mwêêno mwã lhò !
\dialx GO
\xn il ne reste plus qu'eux (lit. il manque eux qui doivent encore jouer)
\an kuzaò, kuraò
\at être en surplus, de trop
\sn 2
\is grammaire_modalité
\ps v.IMPERS. AVERTIF
\ge faillir ; s'en falloir de peu que
\xv mwêêno pòńõ vwo la za mã !
\dialx GO
\xn il s'en est fallu de peu qu'ils ne meurent !
\se mwêêno pòńõ vwo …
\dialx GO
\sge s'en falloir de peu que …
\dt 16/Feb/2025

\lx mweling
\dialx PA BO
\is nourriture_goût
\ps v.stat.
\ge acide
\dt 27/Jan/2019

\lx mwetre
\ph mweɽe
\dialx GOs
\va mwata
\dialx BO PA
\is nourriture
\ps v
\ge préparation de manioc, de la banane rapée
\dn cette préparation est enveloppée et cuite dans des feuilles de canne à sucre ou de cordyline sauvage (|lx{di})
\se mwetre chaamwa
\dialx GO
\sge banane rapée
\se mwetre nu
\dialx GO
\sge coco rapé
\se mwata manyõ
\dialx BO
\sge manioc rapé
\dt 22/Feb/2025

\lx mwömö
\dialx GOs
\is eau
\ps n
\ge bulles d'air
\dn à la surface de l'eau
\dt 08/Feb/2025

\lx mwozi
\dialx GOs
\is fonctions_naturelles
\ps v ; n
\ge hoquet ; hoquet (avoir le) ; hoqueter (en pleurs)
\dt 25/Jan/2019

\lx mhã
\hm 1
\dialx GOs
\va mhãm
\dialx BO
\is caractéristiques_objets
\ps v
\ge humide
\xv mhã cee
\dialx GO
\xn le bois est humide
\xv i mhãm (a) pã
\dialx BO
\xn le pain est humide [BM]
\dt 08/Nov/2021

\lx mhã
\hm 2
\dialx GOs BO
\va mhãng
\dialx PA
\sn 1
\ps n
\ge ligature ; liane (pour toiture)
\ge attache de paille (toit)
\xv mhã-wa mwa
\xn ligature pour la maison
\sn 2
\is maison
\ps v
\ge attacher (la paille aux chevrons)
\dt 30/Dec/2021

\lx mhã
\hm 3
\ph ṃʰɛ̃
\dialx GOs PA BO
\sn 1
\is grammaire_quantificateur_mesure
\ps INTENS ; QNT
\ge beaucoup
\xv e mhã gi
\dialx GO
\xn il pleure fort
\cf e cii gi
\dialx GO
\ce il pleure vraiment
\sn 2
\is grammaire_degré
\ps QNT
\ge très ; trop
\xv la mhã haivwö
\dialx GO
\xn ils sont trop/très nombreux
\xv e mhã mii, pu, baa, kari
\dialx GO
\xn c'est très rouge, bleu, noir, jaune (objet)
\xv i mhã geen
\dialx PA
\xn c'est très sale
\xv i mhã kole pwal
\dialx BO
\xn il pleut très fort
\sn 3
\is grammaire_comparaison
\ps QNT
\ge plus
\xv i mhã hovo nai inu
\dialx BO
\xn il mange plus que moi
\cf pa, para
\ce utilisé pour les animés
\dt 16/Feb/2025

\lx mhã ẽnõ
\ph mʰɛ̃ ɛ̃ɳɔ̃
\dialx GOs
\is grammaire_comparaison
\ps COMPAR
\ge plus jeune
\xv aba-ny mha ẽnõ
\dialx PA
\xn petit frère ; petite sœur
\xv abaa-nu xa mha ẽnõ
\dialx GO
\xn mon plus jeune frère ; ma plus jeune sœur
\dt 24/Feb/2025

\lx mhã mińõ
\dialx GOs
\is préparation_aliments
\ps v.stat.
\ge trop cuit
\dt 21/Mar/2023

\lx mhã whama
\dialx GOs
\is grammaire_comparaison
\ps COMPAR
\ge plus âgé ; plus vieux
\xv e mhã whama
\xn il est plus grand ; c'est le plus grand
\dt 22/Feb/2025

\lx mhãã
\hm 1
\dialx PA WEM BO
\va maak, mheek
\dialx BO
\is arbre
\ps n
\ge gaïac
\sc Acacia spirorbis Labil.
\scf Mimosacées
\dt 27/Aug/2021

\lx mhãã
\hm 2
\ph ṃʰɛ̃:
\dialx GOs
\va mhããng
\dialx BO
\is fonctions_naturelles
\ps v
\ge somnambule ; agiter (s') en dormant
\ge faire un cauchemar |dialx{BO}
\xv e mhãã
\xn il s'agite en dormant
\dt 24/Feb/2025

\lx mhãã
\hm 3
\dialx GOs
\is caractéristiques_personnes
\ps v.stat.
\ge radin ; pingre
\xv e a-mhãã
\xn c'est un pingre
\dt 21/Mar/2023

\lx mhaalöö
\dialx GOs
\is eau
\ps n
\ge marais salant ; marais ; terrain marécageux
\xv bwa mhaalöö
\xn dans un marais
\dt 21/Mar/2023

\lx mhããm
\dialx BO
\is eau
\ps n
\ge source
\nt non vérifié
\dt 29/Jan/2019

\lx mhããni
\dialx PA BO
\is nourriture
\ps v
\ge mâcher ; mastiquer
\dn des fibres de magnania par ex.
\et *mama
\eg mâcher
\el POc
\dt 08/Feb/2025

\lx mhãi
\dialx GOs PA BO
\wr A
\is corps
\ps n
\ge chair
\xv mhãi-me mani kuraa-me na êna
\dialx GOs
\xn c'est notre chair et notre sang qui est là
\wr B
\is grammaire_quantificateur_mesure
\ps n.QNT
\ge morceau (de viande, igname coupée)
\ge part ; fraction
\xv mwêêno mhãi vwo nu kibao-je !
\dialx GOs
\xn il s'en est fallu de peu que je ne le touche (ou) j'ai failli le toucher
\xv ra mhãi-je, je-ne ra kinii-n
\dialx WEM
\xn c'est une partie d'elle, c'est son oreille
\xv mhãi-ny
\dialx PA
\xn mon morceau
\xv mhãi ti/ri ?  – Mhãi-n
\dialx PA
\xn c'est la part/le morceau de qui ? – C'est sa part/son morceau
\wr C
\is classificateur_numérique
\ps CLF.NUM
\ge morceau ; part
\xv mhãi-xè, mhãi-ru
\dialx PA
\xn un, deux morceaux
\xv mhãi-xè mhava kui
\dialx PA
\xn un morceau d'igname
\dt 24/Feb/2025

\lx mha-mhwããnu
\dialx GOs
\is fonctions_naturelles
\ps v
\ge menstruations (avoir ses)
\se tròòli mha-mhwããnu
\sge avoir ses menstruations
\dt 25/Jan/2019

\lx mharii
\dialx BO
\is pêche
\ps v
\ge chasser les poissons vers le filet
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx mhava mhwããnu
\dialx GOs BO
\is astre
\ps n
\ge premier quartier de lune
\dt 28/Jan/2019

\lx mhavwa
\ph mʰaβa
\dialx GOs
\va mhava
\dialx PA BO
\is grammaire_quantificateur_mesure
\ps n.QNT
\ge morceau ; extrémité ; bout de qqch. ; un peu
\xv e na-e pòi-je xa mhavwa lai
\dialx GO
\xn elle donne à ses enfants un peu de riz
\xv mhava kui
\dialx PA
\xn un morceau d'igname
\xv mhava-n
\dialx PA
\xn l'extrémité ; son extrémité
\se mhava gò
\dialx PA
\sge un bout de bambou
\se mhava kui
\dialx PA
\sge un morceau d'igname
\dt 16/Feb/2025

\lx mhayu
\dialx BO
\is fonctions_intellectuelles
\ps n
\ge souvenir ; héritage des vieux
\nt selon BM
\dt 27/Mar/2022

\lx mhaza
\dialx GOs
\va maza
\va mhara
\dialx PA WEM
\sn 1
\is grammaire_aspect
\ps INCH
\ge commencer à ; se mettre à ; être sur le point de
\xv nu mhaza hovwo
\dialx GOs
\xn je commence tout juste à manger
\xv mi za u mhaza a khiila hãgana vwö mi baani
\dialx GOs
\xn mettons-nous à leur recherche maintenant pour les tuer
\xv la mhara buròm ẽnõ
\dialx WEM
\xn les enfants se sont mis à se baigner ; les enfants viennent de se baigner
\xv i ra gaa mhara a
\dialx PA
\xn il vient juste de partir
\xv nu mhara õgin a hovo
\dialx BO
\xn je viens de finir de manger
\xv i mhara a-du we
\dialx BO
\xn la marée commence juste à descendre
\sn 2
\is grammaire_aspect
\ps INCH
\ge être la première fois que
\xv po nye za mhaza nõõli xo je nye êmwê
\dialx GOs
\xn parce que c'est la première fois qu'elle voit un homme
\xv nu mhaza nõõ-je
\dialx GOs
\xn je vais la voir pour la première fois
\sn 3
\is grammaire_aspect
\ps INCH
\ge venir tout juste de
\xv e mhaza a nye !
\dialx GO
\xn celui-là vient juste de partir !
\xv e mhaza uça !
\dialx GO
\xn il vient juste d'arriver !
\xv e mhaza pwe
\dialx GO
\xn il vient de naître; nouveau-né (il n'y a pas d'autre mot pour nouveau-né)
\xv i mhara a
\dialx PA
\xn il vient juste de partir
\xv mhara tèèn
\dialx PA
\xn le jour se lève
\xv i ra gaa mhara pwal
\dialx PA
\xn il vient juste de se mettre à pleuvoir
\xv nu mhara hine
\dialx PA
\xn je comprends/sais maintenant
\sn 4
\is grammaire_aspect
\ps CNJ
\ge juste au moment où
\xv mhaza e u kha-tòè hii-je
\dialx GOs
\xn tout en se penchant, elle tend (lit. lance) son bras
\dt 24/Feb/2025

\lx mhe
\dialx GOs
\is arbre
\va me
\dialx BO
\ps n
\ge tamanou (faux)
\sc Gessois sp.
\scf Cunionacées
\dt 27/Aug/2021

\lx mhe-
\dialx GOs
\is grammaire_locatif
\ps PREF.n.lieu
\ge préfixe de nom de lieu
\se mhe-nõvwo
\sge cicatrice
\dt 21/Mar/2023

\lx mhè
\dialx GOs PA BO
\is déplacement
\ps v ; n
\ge promener ; promenade
\xv nu ra mhè-a-daa-mi
\dialx PA
\xn je suis monté ici faire une visite/une promenade
\xv mhèè-n
\dialx BO
\xn sa promenade
\dt 22/Feb/2025

\lx mhedrame
\dialx GOs
\va mhedam
\dialx PA
\is vent
\ps n ; LOC
\ge vent du nord ; nord
\dt 28/Jan/2019

\lx mhenõ
\ph mʰeɳɔ̃
\dialx GOs
\is déplacement
\ps n
\ge voyage en groupe ; déplacement en groupe
\se koe xa mhenõ
\sge préparer un voyage
\se thu-mhenõ
\sge voyager
\se pa-thu-mhenõ
\sge promener qqn.
\dt 21/Feb/2025

\lx mhenõ-mhõ
\dialx GOs
\va mhenõ-mhõng
\dialx PA BO
\is nœud
\ps n
\ge nœud
\dn y compris celui des filets
\dt 08/Feb/2025

\lx mhenõõ
\hm 1
\dialx GOs BO PA
\va mènõ
\dialx BO
\sn 1
\is grammaire_dérivation
\ps PREF.NMZ (nom de lieu)
\ge endroit ; place ; passage
\dn col, gué, passe dans un récif
\se mhenõõ-traabwa
\sge lieu où l'on s'assoit ; chaise
\se mhenõõ-cö
\dialx GO
\sge gué
\se mhenõ-còòl
\dialx PA BO
\sge gué
\se mhenõõ-mwãã mãni mhenõõ-kea
\sge soutiens et appuis (lit. lieu de soutien et lieu d'appui)
\sn 2
\is grammaire_aspect
\ps ASP
\ge en train de
\xv novwö ne ge je ne mhenõõ mããni, çe/ça me zoma baa-je
\dialx GO
\xn quand elle sera en train de dormir, on la frappera
\dt 22/Feb/2025

\lx mhenõõ
\hm 2
\dialx GOs WEM PA
\sn 1
\is déplacement
\ps n
\ge trace de pas
\xv mhenõõ va mhenõõ ?
\xn où vont les traces de pas ?
\sn 2
\is santé
\ps n
\ge marque ; trace
\se mhenõõ-hèlè
\dialx GO
\sge trace du couteau
\se mhenõõ-parô kuau
\dialx GO
\sge trace de dent du chien
\dt 23/Jan/2022

\lx mhenõõ pe-ki
\dialx GOs
\is configuration
\ps n
\ge raccord ; soudure
\dn réfère aussi à la soudure des deux parties du crâne
\dt 08/Feb/2025

\lx mhenõõ pe-vhi dè
\dialx GOs
\va mhenõ piga-dèn
\dialx BO
\is topographie
\ps n
\ge carrefour convergent ; lieu de rencontre sur un chemin
\dt 24/Jan/2022

\lx mhenõõ-a-pe-aze dè
\dialx GOs
\va menõ-aveale-dèn
\dialx BO PA
\is topographie
\ps n
\ge carrefour divergent
\dt 16/Jun/2022

\lx mhenõõ-içu
\ph 'mʰeɳɔ̃: 'iʒu
\dialx GOs
\va mhenõ-iyu
\dialx WEM BO
\va mõ-iyu
\dialx BO
\is nom_locatif
\ps n
\ge lieu de la vente
\ge magasin ; boutique
\dt 16/Jun/2022

\lx mhenõõ-kole ja
\ph 'mʰeɳɔ̃: ko'le ja
\dialx GOs PA
\is nom_locatif
\ps n
\ge dépotoir
\dt 16/Jun/2022

\lx mhenõõ-na-pwaawe
\dialx GOs
\ph mʰe'ɳɔ̃: na 'pwa:we
\is maison
\ps n
\ge étagère ; claie pour fumer (la nourriture)
\dt 22/Mar/2023

\lx mhenõõ-paxe we
\dialx PA
\is cultures
\ps n
\ge vanne de canal
\cf paxe
\dialx PA
\ce dévier
\dt 16/Jun/2022

\lx mhenõõ-tivwo
\dialx GOs
\is maison_objet
\ps n
\ge tableau
\dt 16/Jun/2022

\lx mhenõõ-trabwa
\dialx GO
\va mhenõõ-taabwa
\dialx PA
\is maison_objet
\ps n
\ge chaise |dialx{PA} ; lieu où l'on s'assoit |dialx{GO}
\dt 21/Mar/2023

\lx mhenõõ-yuu
\ph mʰeɳɔ̃: yu:
\dialx GOs
\va menõ yu
\ph meɳɔ̃ yu
\dialx GOs
\is habitat
\ps n
\ge demeure
\ge séjour
\ge lieu de résidence
\xv mhenõõ-yu-w-a kãgu êgu mãla mã
\dialx GOs
\xn la demeure des esprits des morts
\dt 12/May/2024

\lx mhe-nõvwo
\ph mʰe'ɳɔ̃βo
\dialx GOs
\is santé
\ps n
\ge plaie ; cicatrice
\ge blessure
\xv u mã mhe-nõvwo
\xn c'est cicatrisé (lit. la blessure est morte)
\xv u pònu lhã mhe-nõvwo ni mee-jèè
\dialx GO
\xn elle a beaucoup de plaies sur le visage
\dt 22/Mar/2023

\lx mhõ
\hm 1
\ph mʰɔ̃
\dialx GOs PA BO
\is insecte
\ps n
\ge fourmi noire (petite)
\dt 29/Jan/2019

\lx mhõ
\hm 2
\dialx GOs WE BO
\va mõ
\dialx GO
\va bwa mhõ
\dialx PA
\sn 1
\is grammaire_locatif
\ps LOC
\ge gauche (côté)
\se kòlò-je m(h)õ
\dialx GOs
\sge son côté gauche
\xv yi-ny (a) mhõ
\dialx BO
\xn ma main gauche
\sn 2
\is caractéristiques_personnes
\ps v.stat.
\ge gaucher
\xv e mhõ
\dialx GOs
\xn il est gaucher
\an gu hii-n
\at droit (côté)
\et *mauRi
\eg gauche
\el POc
\dt 27/Jan/2022

\lx mhõ
\hm 3
\ph mʰɔ̃
\dialx GOs
\va mhwòl
\dialx BO
\is nœud
\ps v ; n
\ge pli ; plisser
\ge nœud
\xv thu mhõ
\xn faire des plis
\dt 25/Aug/2021

\lx mhõdòni
\dialx GOs
\is grammaire_quantificateur_mesure
\ps n.QNT
\ge ajout à un lot (de dons coutumiers)
\dn il ne peut constituer un tas complet à lui seul
\xv mãè-xe xo mhõdòni
\dialx GO
\xn un lot de trois ignames et quelques (i.e. un lot plus une ou deux ignames en plus)
\dt 24/Feb/2025

\lx mhodrö
\dialx GOs
\va mhòdo
\dialx BO
\is nourriture
\ps v
\ge avarié ; tourné ; sûr
\ge moisi (nourriture)
\xv e mhodrö hovwo
\xn la nourriture est avariée
\dt 21/Mar/2023

\lx mhõge
\ph mʰɔ̃ŋge
\dialx GO PA BO
\sn 1
\is nœud
\ps v ; n
\ge nouer ; attacher avec un nœud ; faire un nœud
\xv mhõge pwio
\dialx BO
\xn faire un filet
\sn 2
\is coutumes
\ps v
\ge unir (s') ; rassembler (se)
\dn contexte coutumier
\dt 22/Feb/2025

\lx mhômõwe
\ph mʰõmɔ̃we
\dialx GOs
\is terrain_terre
\ps n
\ge boue
\dt 28/Jan/2019

\lx mhôwee
\dialx GOs
\is action_corps
\ps v
\ge essorer ; presser (fruit)
\ge tordre
\se mhôwee nu
\sge presser pour extraire le lait de coco
\et *momos
\eg squeeze
\el POc
\ea Blust
\dt 21/Mar/2023

\lx mhõzi
\va mhõril
\dialx PA BO
\is fonctions_naturelles
\ps v
\ge pleurnicher ; sangloter ; hoqueter
\dt 21/Mar/2023

\lx mhõ-zixe
\dialx GOs
\is nœud
\ps n
\ge nœud coulant
\dt 27/Jan/2019

\lx mhûûzi
\dialx GOs
\is crustacés
\ps n
\ge crabe de palétuvier
\dn plus gros que |lx{ji}
\dt 08/Feb/2025

\lx mhwããnu
\ph mwʰɛ̃:nu, mʰwɛ̃ɳu
\dialx GOs PA BO
\sn 1
\is astre
\ps n
\ge lune
\se mhava mhwããnu
\sge premier quartier de lune
\se hõgõõne mhwããnu
\dialx PA
\sge deuxième quartier de lune ; demi-lune
\se cabòl mhwããnu
\dialx PA
\sge pleine lune
\se phaa-mè mhwããnu
\sge pleine lune
\se mhwããnu ni trabwa
\sge demi-lune
\se we ni mè mhwããnu
\sge pluie qui accompagne la pleine lune
\se u tabwa mhwããnu
\dialx PA
\sge nouvelle lune
\sn 2
\is temps
\ps n
\ge mois
\dt 22/Feb/2025

\lx mhwacidro
\ph mʰwaciɖo
\dialx GOs
\is insecte
\ps n
\ge fourmi noire
\dt 29/Jan/2019

\lx mhwãnge
\ph mwʰɛ̃ŋe
\dialx GOs
\is action
\ps v
\ge redresser (fer)
\dt 01/Feb/2019

\lx mhwêê
\ph mʰwẽ:
\dialx GOs
\va mhwèèn
\dialx PA BO
\ph mʰwɛ:n
\is navigation
\ps v
\ge flotter ; dériver
\et *ma-qanu
\el POc
\dt 22/Feb/2025

\lx mhwêêdi
\dialx GOs PA BO
\va mwêêdi
\dialx PA BO
\is corps
\ps n
\ge nez
\xv mhwêêdi-n
\dialx PA BO
\xn son nez
\se dròò-mhwêêdi
\dialx GOs
\sge ailes du nez
\dt 21/Aug/2021

\lx mhwi
\dialx BO
\is caractéristiques_personnes
\ps v.stat.
\ge timide ; doux
\dt 26/Jan/2019

\lx -n
\dialx BO PA
\is grammaire_pronom
\ps SUFF.POSS 3° pers. SG
\ge son ; sa ; ses
\dt 29/Jan/2019

\lx na
\hm 1
\ph ɳa
\dialx GOs
\sn 1
\is grammaire_préposition_locative
\ps PREP.LOC (spatio-temporel)
\ge sur ; à
\xv êgu na Kolikò
\dialx GOs
\xn les gens de Koligo
\xv na le
\xn à cet endroit-là
\xv na bwa
\xn au-dessus
\sn 2
\is grammaire_préposition_locative
\ps PREP.LOC
\ge de (ablatif)
\xv bî a-daa-mi na Kumak
\dialx GOs
\xn nous deux arrivons ici de Koumac
\dt 24/Feb/2025

\lx na
\hm 3
\ph ɳa, ɳe
\dialx GOs
\va ne
\dialx GO(s)
\va na, ne
\dialx BO PA
\sn 1
\is grammaire_conjonction
\ps CNJ
\ge si ; quand (prospectif, hypothétique)
\xv ne yu a-mi
\dialx BO
\xn si tu viens
\xv e phe-mõgu ne whaa-gò
\xn elle commence le travail (quand c'est) tôt le matin
\xv e zoma uça na we-tru tre
\dialx GOs
\xn elle arrivera dans deux jours
\se mõnõ na whaa
\dialx GO
\sge demain matin
\an dròrò xa whaa
\dialx GOs
\at hier matin
\cf novwö-na
\dialx GOs
\ce si
\sn 2
\is grammaire_conjonction
\ps CNJ ; COMP ; REL
\ge que (irrealis)
\xv nee nye-na hovwa-da na çö õgi
\dialx GOs
\xn fais-le jusqu'à ce que tu aies fini
\xv e kaabu na mõ pweeni nõ-ni
\dialx GOs
\xn il nous est interdit de pêcher ce poisson-là
\xv e zo na çö wa zo
\dialx GOs
\xn il faut que tu chantes bien
\xv la khõbwe na nu a-du-ò
\dialx PA
\xn ils m'ont demandé d'aller là-bas en bas
\xv la oole-nu na nu a-è
\dialx BO
\xn ils m’ont empêché de partir
\xv nu hããxa ne nu a-da-ò
\dialx GOs
\xn j'ai peur d'aller là-haut là-bas
\xv a khila xa hèlè na ca
\dialx GOs
\xn va chercher un couteau qui coupe
\xv êgu ne thu-mhenõ
\dialx BO
\xn l'homme qui marche
\xv thoomwã ne nu nõõli
\dialx BO
\xn la femme que j'ai vue
\xv còòxe xa pã na popobe
\dialx PA
\xn coupe un tout petit peu de pain (coupe du pain qui soit tout petit)
\dt 24/Feb/2025

\lx -na
\ph ɳa
\dialx GOs PA BO
\is grammaire_démonstratif
\ps DEM.DEIC.2 médial; ANAPH
\ge là (visible ou non) ; cela
\xv nye-na
\xn cela
\xv we a èńiza ? – Me a nye-na !
\dialx GOs
\xn quand partez-vous ? – Nous partons tout de suite !
\xv ge na
\dialx BO PA
\xn il est là
\se bon na
\dialx PA
\sge après-demain
\dt 22/Feb/2025

\lx na bòli
\ph ɳa mbɔli
\dialx GOs BO PA
\is grammaire_direction
\ps LOC.DIR
\ge là-bas
\xv da yala cee na bòli ?
\dialx BO
\xn comment s'appelle cet arbre là-bas ?
\dt 08/Nov/2021

\lx na ênõli
\ph ɳa ẽɳɔli
\dialx GOs
\is grammaire_locatif
\ps PREP.LOC
\ge de là-bas (ablatif)
\dt 31/Oct/2021

\lx na hû-mi
\ph ɳa
\dialx GOs
\is mouvement
\ps v
\ge poser qqch. près d'ego
\xv na hû-mi !
\xn pose-le près de moi !
\dt 29/Mar/2022

\lx na kòlò
\ph ɳa
\dialx GOs
\is grammaire_préposition
\ps PREP (ablatif)
\ge de ; à
\xv e uvwi õgine-ni axe êgu po-mã na kòlò Pwayili
\xn il a acheté vingt mangues à Pwayili
\dt 22/Dec/2021

\lx na khõbwe
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge si jamais : au cas où
\xv na khõbwe cö trõne khõbwe ge-le xa thoomwã xa Mwani-mii
\xn si jamais tu entends dire qu'il y a une femme du nom de Mwani-mi
\dt 20/Dec/2021

\lx na mwã
\dialx GOs PA
\is interaction
\ps v
\ge rendre (qqch.)
\an trèè-naa
\dialx GOs
\an tèè-na
\dialx PA
\at prêter
\dt 17/Feb/2025

\lx na ni
\hm 1
\ph ɳa ɳi
\dialx GOs PA BO
\is grammaire_préposition_locative
\ps PREP (ablatif)
\ge hors de; de
\xv e ubò na ni mwa
\dialx GOs
\xn il est sorti de la maison
\xv ça novwö ibî mã Paola, ça bî uça na ni chomu
\dialx GO
\xn quant à Paola et moi, nous revenions de l'école
\xv i yare lavian na ni doo
\dialx BO
\xn il sort la viande de la marmite
\xv nu thôni-çö na ni nye kûdo
\dialx GOs
\xn je t'interdis la boisson (je te barre l'accès à cette boisson)
\dt 03/Feb/2025

\lx na ni
\hm 2
\ph ɳa ɳi
\dialx GOs PA BO
\wr A
\is grammaire_préposition_locative
\ps PREP.LOC
\ge dans ; sur ; de ; pendant ; parmi
\xv e nõle hênuã-nu na ni vea
\dialx GO
\xn il a vu mon reflet dans la vitre
\xv e alö-le hênuã-je na ni we
\dialx GO
\xn elle regarde son reflet dans l'eau
\xv khaa bwa-je mwã na ni we
\dialx GO
\xn (elle) lui enfonce la tête sous l'eau
\xv la kêmi chaamwa na ni dra
\dialx GO
\xn ils ont mis les bananes à cuire sous la cendre
\xv e niivwa na ni kò
\dialx GOs
\xn il s'est perdu dans la forêt
\xv e tha na ni dè
\dialx GOs
\xn il s'est trompé de chemin ; il s'est égaré
\xv i phe aa-xe na ni dõni la-ã ko
\dialx BO
\xn il a pris l'un parmi ces poulets
\xv kixa mwã na i zò(ò)n na ni phagòò-ny
\dialx PA
\xn il n'y a rien de toxique dans mon corps
\xv bo ca e ra Teã-ma na ni la kuna-n
\dialx PA
\xn la roussette est le Grand Chef dans ses endroits
\xv i thali kò na ni we-cee
\dialx PA
\xn il s'est pris les pieds dans les racines de l'arbre
\xv da je-ne i pada na ni kèè-m ?
\dialx PA
\xn qu'est ce qui racasse dans ton sac ?
\xv cö thaaboe na ni kûdi mwa
\dialx GO
\xn mets-la à l'abri dans un coin de la maison
\xv mo pe-k(h)a-a-niza na ni ba ?
\dialx GO
\xn nous (paucal) sommes combien dans chaque équipe ?
\xv e je-nã ńya e cińevwö na ni mõlò i ã
\dialx GOs
\xn voilà ce qui est important dans nos vies
\xv e tho kutra-da na ni tree ni pe-waaça
\dialx GOs
\xn le sang a coulé dans le passé pendant les jours de lutte
\xv da nye phò-çö na ni je-nã ?
\dialx GOs
\xn de quoi tu te mêles dans cette affaire?
\xv na ni kênime-n
\dialx PA
\xn lors de leur prestation de deuil
\se na ni phwevwöu
\dialx BO
\sge dans l'intervalle
\wr B
\is grammaire_préposition
\ps PREP
\ge de ; que (complément du comparatif)
\xv e khawali na ni mwa
\dialx GO
\xn il est plus haut que la maison
\xv ge je bwa na ni mwa-nu
\dialx GO
\xn il est plus haut que ma maison
\dt 22/Feb/2025

\lx na va
\ph ɳa va
\dialx GOs BO PA
\is grammaire_locatif
\ps PREP
\ge d'où (provenance)
\xv tho na va?
\dialx PA BO
\xn d’où prend il sa source ?
\xv nu hivwine za a-mi na va
\dialx BO
\xn je ne sais pas d'où ils viennent
\xv yu tooli na va nhya mõû-m ?
\dialx PA
\xn où as-tu trouvé ton épouse ?
\dt 21/Mar/2023

\lx naa
\ph ɳa:
\dialx GOs
\va na, ne
\dialx PA BO
\sn 1
\is échanges
\ps v
\ge donner
\xv e naa lai çai la pòi-je xo õ-ẽnõ ã
\dialx GO
\xn la mère donne du riz à ses enfants
\xv e naa lai xo õ-ẽnõ ã çai la pòi-je
\dialx GO
\xn la mère donne du riz à ses enfants
\xv e na-e pòi-je xa mhavwa lai
\dialx GO
\xn elle donne à ses enfants un peu de riz
\xv e na-e pòi-je ce-la lai
\dialx GO
\xn elle donne à ses enfants leur part de riz
\xv a-da na-e pi-nõõ-je (na-e est réalisé /ne:/)
\dialx GO
\xn monte lui donner son collier
\xv e naa çai la ce-la lai
\dialx GO
\xn elle leur donne leur part de riz
\xv i na i êgu
\dialx PA
\xn il le donne à quelqu'un
\xv na-e hi-n !
\dialx PA
\xn donne-le-lui (lit. poser main-sa)
\xv nu na hi Kaawo
\dialx BO
\xn je l'ai donné à K
\xv i na hii-m
\dialx PA
\xn il te l'a donné
\xv i na yii-ny
\dialx BO
\xn il me l'a donné
\ng |lx{na-e} est souvent réalisé |ph{ne:}
\sn 2
\is action
\ps v
\ge mettre
\ge poser
\se naa-du
\sge poser par terre
\dt 21/Feb/2025

\lx naa yaai
\ph ɳa:
\dialx GOs
\is coutumes
\ps v
\ge faire une demande coutumière
\dn (lit. donner le feu)
\dt 22/Oct/2021

\lx nãã-n
\dialx PA BO
\is interaction
\ps n
\ge injure ; offense ; affront ; calomnie ; mauvais sort
\ge leçon donnée pour faire réfléchir qqn.
\xv i pe-thoele nãã-n
\dialx PA
\xn il lui a lancé des insultes
\xv i pe-thoele nãã-la
\dialx PA
\xn il leur a lancé des insultes
\xv li pe-thoeli nãã-n
\dialx PA
\xn ils se lancent des insultes
\xv nu pe-thoele nãã-n
\dialx BO
\xn je l'ai insulté
\xv la pe-khõbwe nãã-n
\dialx PA
\xn ils s'injurient
\xv i khõbwe nãã-ny
\dialx PA
\xn il m'a insulté
\xv nu khõbwe nãã-n
\dialx PA
\xn je l'ai insulté
\xv co khõbwe la-ili ma vwu nãã-ri ?
\dialx PA
\xn à qui as-tu adressé ces insultes ?
\xv yo thu nãã-ny
\dialx BO
\xn tu m'as insulté
\se thu nãã-n
\sge offenser qqn.
\cf paxa-nãã-n, pawa-nãã-n
\ce injure ; offense ; affront
\dt 22/Feb/2025

\lx na-bulu-ni
\ph ɳabu'luɳi
\dialx GOs
\is interaction
\ps v.t.
\ge assembler ; rassembler
\dt 02/Jan/2022

\lx nai
\ph ɳai
\dialx GOs
\va nai
\dialx PA BO
\sn 1
\is grammaire_préposition_locative
\ps PREP
\ge par rapport à ; envers ; à
\xv nu ole nai çö
\dialx GOs
\xn je te remercie
\xv i thòxe nai nu u ciia
\dialx BO
\xn le poulpe s’est collé à moi
\xv la paree nai je whamã
\dialx PA
\xn ils ont été négligents envers ce vieil homme
\xv ye kavwö pewoo nai Kaawo
\dialx PA
\xn il ne prête pas attention à Kaawo
\xv phwaal nai yo ?
\dialx PA
\xn est-ce clair pour toi ?
\xv i vhaa-raa nai yo
\dialx PA
\xn il médit/il dit du mal de lui
\sn 2
\is grammaire_préposition_locative
\ps PREP.LOC
\ge par rapport à (complément du comparatif)
\xv e pwawali nai je
\dialx GO
\xn il est plus grand qu'elle
\xv e povwonu nai pòi-nu
\dialx GO
\xn il est plus petit que mon enfant
\xv e ẽnõ nai nu
\dialx GO
\xn il est mon cadet / il est plus jeune que moi
\xv e cii êgu nai je
\dialx GO
\xn il est vraiment plus grand qu'elle
\xv la pò haivwö nai la
\dialx GO
\xn ils sont un peu plus nombreux qu'eux
\xv la pò hoxèè nai la
\dialx GO
\xn ils sont un peu moins nombreux qu'eux
\sn 3
\is grammaire_préposition_locative
\ps PREP.LOC
\ge parmi
\xv a-tru nai we nye li zuma a iò ne thrõbo
\dialx GOs
\xn deux d'entre vous partiront ce soir
\xv la pe-thi thô nai la
\dialx GOs
\xn ils se provoquent entre eux (lit. piquent la colère)
\sn 4
\is grammaire_préposition_locative
\ps PREP
\ge de ; à ; à cause de
\xv yhaamwa e trõne nai kani a yhaamwa e trõne nai ti
\dialx GOs
\xn on ne sait pas s'il a entendu (cela) du canard ou d'on ne sait qui
\xv mii dili nai mwani
\dialx GO
\xn la terre est rouge à cause de l'argent (conte)
\xv i vaa nai ti  ?
\dialx BO
\xn de qui parle-t-il ?
\xv i vaa nai da ?
\dialx BO
\xn de quoi parle-t-il ?
\sn 5
\is grammaire_préposition_locative
\ps PREP.LOC
\ge par rapport à une position
\xv hêbu nai nu
\dialx GO
\xn devant/avant moi
\xv mu nai çö
\dialx GO
\xn derrière toi
\dt 22/Feb/2025

\lx nani
\hm 1
\dialx PA BO
\is grammaire_préposition
\ps PREP + nom commun pluriel
\ge à ; de ; à propos de
\xv kawu i pewoo nani pòi-n
\dialx BO
\xn elle ne s'occupe pas de ses enfants
\xv li pe-hivwine-li ã-mi nani nye ba-êgu
\dialx GOs
\xn ils s'ignorent à cause (lit. cela vient) de ces femmes
\xv ra hale tho-ny nani cocovwa mèni
\dialx PA
\xn mon cri est différent de tous ceux des autres oiseaux
\xv bî pweexu lhã pwaixe nani vhaa zuanga
\dialx GOs
\xn nous deux discutons de choses à propos de la langue zuanga
\xv kavwu nu vha mwã nani xa kun mwã
\dialx PA
\xn je ne vais pas parler de n'importe quels clans
\dt 06/Jul/2024

\lx nani
\hm 2
\ph ɳaɳi
\dialx GOs
\va nani
\dialx PA BO
\is mammifères
\ps n
\ge chèvre
\dt 01/Mar/2023

\lx napoine
\dialx BO
\is coutumes
\ps n
\ge feuilles enfouies pour la fécondité des femmes
\nt selon Dubois ; non vérifié
\dt 26/Mar/2022

\lx na-vwo
\dialx GOs PA BO
\ph ɳaβo
\is coutumes
\ps n
\ge cérémonie coutumière
\ge dons coutumiers
\dt 02/Jan/2022

\lx nawêni
\ph ɳawẽɳi
\dialx GOs
\va nawãni, naõni
\ph ɳawɛ̃ɳi, ɳaɔ̃ɳi
\dialx GO(s)
\va naõnil
\dialx PA BO
\va naõnin
\dialx BO
\is plantes
\ps n
\ge chou kanak |dialx{GOs}
\ge "épinard" |dialx{PA}
\sc Hibiscus manihot L.
\scf Malvacées
\dt 27/Oct/2023

\lx naxo
\ph ɳa
\dialx GOs
\va naxo, nago
\dialx BO PA
\is poisson
\ps n
\ge mulet noir (de cascade)
\sc Cestræus plicatilis
\scf Mugilidés
\cf whai
\ce mulet (de taille juvénile)
\cf mene
\ce mulet queue bleue
\dt 27/Aug/2021

\lx ne
\hm 1
\dialx GOs PA
\is grammaire_adverbe
\ps ADV.LOC
\ge ici ; là
\xv ti nye uça ne ?
\dialx GO
\xn qui est arrivé ici ?
\xv ti gi ne ?
\dialx PA
\xn qui est là ?
\dt 26/Nov/2021

\lx ne
\hm 1
\ph ɳe
\dialx GOs
\is outils
\ps n
\ge bout de verre
\dn utilisé pour entailler, couper
\dt 08/Feb/2025

\lx ne
\hm 2
\ph ɳe
\dialx GOs PA
\is grammaire_aspect
\ps ASP.FREQ
\ge souvent
\xv na nu uça avwõnõ, e zo ma nu ne a nõ nyaanya, 
\dialx GO
\xn quand je rentrerai chez moi, il faut que j'aille souvent voir ma mère 
\xv e ne thruã-me
\dialx GO
\xn il nous ment souvent
\xv lò ne khûbu-bî xo ãbaa-bî êmwê-e
\dialx GO
\xn nos (trois) frères nous frappaient souvent
\se kavwö … ne …
\sge ne pas souvent ; jamais
\xv kavwö ne mõgu êgu-ã !
\dialx GO
\xn il ne travaille pas souvent celui-là !
\xv kavwö ne e hine khõbwe gele-xa êmwê
\dialx GO PA
\xn elle n'avait jamais su qu'il existait des hommes
\xv kavwö li ne vhaa yai yo
\dialx PA
\xn ils ne te parlent jamais
\dt 24/Feb/2025

\lx ne
\hm 2
\ph ɳe
\dialx GOs
\va nèn
\dialx PA
\is caractéristiques_objets
\ps v.stat.
\ge émoussé
\dt 21/Mar/2023

\lx ne
\hm 3
\ph ɳe
\dialx GOs
\va nèn
\dialx PA BO
\is insecte
\ps n
\ge mouche ; moucheron
\et *laŋo, *lalo
\eg mouche
\el POc
\dt 21/Mar/2023

\lx nee
\ph ɳe:
\dialx GOs
\va ne, nee
\dialx BO PA
\sn 1
\is action
\ps v
\ge faire ; effectuer
\xv mi nee a chomu
\dialx GOs
\xn nous allions à l'école
\xv nu ne hivwine nee ça-ni la-ã pòi-nu
\dialx GOs
\xn je n'ai jamais su le faire/raconter (lit. souvent ne pas savoir faire) à mes enfants
\xv nee hayu mã i mayo !
\dialx PA
\xn fais-le quand même parce que cela démange !
\xv e ra nee
\dialx PA
\xn il l'a fait
\xv nee-yo-ni dili !
\dialx BO
\xn mets bien la terre
\se nee nhye thraa
\dialx GO
\sge lui faire du mal ; lui nuire
\sn 2
\is grammaire_aspect
\ps v
\ge commencer ; se mettre à
\xv novwö na u nee kõbwe wo pwayi la waalei wo hovwo
\dialx PA
\xn quand elle se met à éplucher les ignames pour manger
\se nee … vwö
\sge se mettre à ; entreprendre de
\cf po
\dialx GOs
\ce faire
\dt 22/Feb/2025

\lx nee
\ph ɳe:
\dialx GOs
\va nèèng
\dialx PA BO
\is temps_atmosphérique
\ps n
\ge nuage
\dt 21/Mar/2023

\lx nee ra
\ph ɳe: ra
\dialx WEM
\is grammaire_aspect
\ps v
\ge faire en même temps
\xv e nee ra ka "caca"
\xn elle fait cela et en même temps elle fait caca
\xv e nee ra pao yaoli
\xn en même temps/ensuite, elle lance la balançoire
\dt 13/Jan/2022

\lx neebu
\ph ɳɛ̃:bu
\dialx GOs
\va neebu
\dialx PA BO
\is insecte
\ps n
\ge moustique
\et *ɲamu(k)
\eg moustique
\el POc
\dt 21/Mar/2023

\lx nee-vwo
\dialx GOs BO
\ph ɳe:βo
\is action
\ps n
\ge actions ; actes
\dt 15/Oct/2021

\lx nee-wo mãni phwewede-vwo
\dialx GOs BO
\is coutumes
\ps n
\ge us et coutumes
\dt 21/Mar/2023

\lx nee-zo
\ph ɳe:zo
\dialx GOs
\is action
\ps v
\ge ranger (faire bien)
\ng v.t. |lx{ne-zoo-ni}
\gt ranger qqch.
\dt 21/Feb/2025

\lx nèm
\dialx PA
\is nourriture_goût
\ps v.stat.
\ge doux ; sucré
\ge insipide |dialx{BO}
\se we nèm
\dialx PA
\va we ne
\dialx GOs
\sge eau douce
\xv i ne zo
\dialx BO
\xn il a bon goût
\an we za
\at eau salée
\dt 27/Mar/2022

\lx neme
\ph ɳeme
\dialx GOs
\va nemee-n
\dialx PA BO
\is nourriture_goût
\ps n
\ge goût ; saveur
\xv e whaya neme ?
\dialx GOs
\xn quel goût ça a ?
\xv kixa neme
\dialx GO
\xn c'est sans goût/fade
\xv ke neme
\dialx PA
\xn c'est sans goût/fade
\xv i zo nemee-n
\dialx BO
\xn ça a bon goût
\se ne-zo
\sge délicieux ; bon au goût
\se ne-raa
\sge mauvais (au goût)
\et *ɲami
\eg goûter
\el POc
\dt 22/Feb/2025

\lx ne-mu
\ph ɳemu
\dialx GOs
\is action
\ps v
\ge faire ensuite ; faire après
\dt 02/Jan/2022

\lx nenèm
\ph nenɛm
\dialx PA
\va nenèèm
\dialx BO
\is caractéristiques_personnes
\ps v.stat.
\ge tranquille ; sage ; immobile
\se tee-nenèm !
\sge reste assis tranquille !
\se ku-nenèm
\sge rester debout tranquille
\dt 05/May/2024

\lx ne-phû
\ph ɳepʰû
\dialx GOs
\va nèn phûny
\dialx PA
\is insecte
\ps n
\ge mouche bleue
\dt 02/Jan/2022

\lx ne-raa
\ph ɳeɽa:
\dialx GOs BO
\is nourriture_goût
\ps v.stat.
\ge mauvais au goût
\ge acide |dialx{BO}
\cf thraa
\dialx GOs
\ce mauvais
\cf neme
\ce goût
\dt 10/Jan/2022

\lx neule
\dialx GOs
\ph ɳeule
\va neule-gat
\dialx BO
\is eau
\ps n
\ge eau saumâtre
\dt 29/Jan/2019

\lx ne-wã le
\ph ɳewãle
\dialx GOs
\is action
\ps v
\ge faire comme ceci
\se ne-wã na le
\sge faire comme cela
\dt 02/Jan/2022

\lx nè-zo
\ph ɳɛðo
\dialx GOs
\is nourriture_goût
\ps v.stat.
\ge sucré ; bon au goût ; succulent
\an nè-raa
\at mauvais au goût
\dt 02/Jan/2022

\lx ne, na
\hm 1
\dialx GOs PA BO
\is grammaire_relateur_possessif
\ps REL.POSS
\ge de
\xv pò-mugo ne kòò-n
\dialx WEM BO
\xn son mollet
\xv hò-m na cèvèro
\dialx PA
\xn ta ration de viande
\dt 27/Oct/2021

\lx ne, na
\hm 2
\ph ɳe ; ɳa
\dialx GOs BO
\is grammaire_modalité
\ps OPT
\ge optatif ; hortatif
\xv Poinyena ne çö phaxee !
\dialx GO
\xn Poinyena, écoute !
\xv çö a threi bwee-pòò, xa na çö uzi-zoo-ni vwo pozo zo
\dialx GO
\xn va couper une branche de bourao, et fais en sorte de bien l'écorcer pour qu'elle soit bien propre.
\xv na jo !
\dialx BO
\xn attention à toi ! (Dubois)
\ng exprime un ordre, un conseil, un souhait
\dt 07/May/2024

\lx ni
\hm 1
\ph ɳi
\dialx GOs
\va nhim
\dialx PA WEM WE BO
\is caractéristiques_objets
\ps v ; MODIF
\ge profond (mer ou rivière)
\xv we ni
\dialx GO
\xn eau profonde
\dt 03/Feb/2019

\lx ni
\hm 2
\ph ɳi
\dialx GOs PA BO
\va ne
\dialx GOs
\is grammaire_préposition_locative
\ps PREP (spatio-temporelle)
\ge dans (contenant) ; à ; vers
\xv e ã-da ni kò
\dialx GO
\xn il monte vers la forêt
\xv e ã-da ni nõ-kò
\dialx GO
\xn il s'enfonce dans la forêt
\xv na-du ni doo
\dialx BO
\xn mets-le dans la marmite
\xv ni don
\dialx PA
\xn dans le ciel
\xv ni ka
\xn dans l'année
\xv ni yevwa tòò
\dialx GO
\xn à la saison chaude
\xv na iò mwã, ne kaça hovwo, ce mõ vara thriu mwã
\dialx GO
\xn tout à l'heure, après le déjeuner, nous nous disperserons
\dt 21/Mar/2023

\lx ni
\hm 3
\ph ɳi
\dialx GOs
\va ning
\dialx WEM PA BO
\is maison
\ps n
\ge poteaux (petits) de maison
\se ninga-mwa
\dialx PA
\sge poteau (de maison)
\ng forme déterminée: |lx{ninga}
\cf nixò, nixòòl
\ce poteau central
\dt 10/Jan/2022

\lx -ni
\hm 1
\ph ɳi
\dialx GOs
\va -nim
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge cinq
\et *lima
\eg cinq
\el POc
\dt 29/Jan/2019

\lx -ni
\hm 2
\ph ɳi
\dialx GOs
\va -nim
\dialx PA
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal ou ANAPH
\ge ce …-là (péjoratif)
\xv çö za mõû-çö je-ni
\dialx GO
\xn c'est ton épouse celle-là (la laide)
\xv lhò ne kûbu-bî xo lò-ni ãbaa-bî êmwê-e
\dialx GO
\xn nos (trois) frères nous frappaient souvent
\xv enim
\dialx PA
\xn là (à distance)
\xv ã-nim
\dialx PA
\xn celui-là (péjoratif)
\xv hla pe-khila mwã ho ã=mãli=nim
\dialx PA
\xn ils cherchent de la nourriture pour les deux (enfants)
\xv je-ne êgu-nim
\dialx PA
\xn cette personne-là (péjoratif)
\xv thoomwã-nim
\dialx PA
\xn cette femme-là (péjoratif)
\xv êgu-nim
\dialx PA
\xn cette personne-là (péjoratif)
\xv loto-nim
\dialx PA
\xn cette voiture-là
\dt 24/Feb/2025

\lx ni gòò
\ph ɳĩ ŋgɔ:
\dialx GOs
\is nom_locatif
\ps n.loc
\ge au milieu
\se chaamwa ni gòò
\dialx GOs
\sge bananier de taille moyenne
\dt 03/Feb/2025

\lx ni mhenõ
\dialx GOs
\va ne mhenõ(õ), na mhenõ(õ)
\dialx GOs
\is grammaire_aspect
\ps ASP
\ge sans cesse ; sans arrêt
\xv ge-li gò ni mhenõ gi
\xn ils sont toujours en train de pleurer
\xv ge-je ne mhenõõ mããni
\xn elle est en train de dormir
\xv e bala na mhenõõ mããni
\xn elle en train de dormir
\dt 04/Nov/2021

\lx ni nõ
\ph ɳĩ ɳɔ̃
\dialx GOs
\is grammaire_préposition_locative
\ps LOC
\ge dans ; dedans ; à l'intérieur de
\xv e ã-da ni nõ ko
\dialx GOs
\xn il s'enfonce dans la forêt
\xv e u-da ni nõ mwa
\dialx GOs
\xn il entre à l'intérieur de la maison
\xv e u-du ni nõ we
\dialx GOs
\xn il plonge sous l'eau
\dt 14/Oct/2021

\lx ni xa
\ph ɳiɣa
\dialx GOs
\is grammaire_locatif
\ps LOC.INDEF
\ge quelque part ; quelque (temps)
\xv ge ni xa
\xn il est quelque part
\xv pe-tròòli ni xa tree !
\xn à un de ces jours !
\xv ge je ni xa bwa drau
\xn elle se trouve quelque part sur une île
\xv novwö ni xa teen mwã na yu whili thoomwã, yu ra mwaju mwã ya avwõnõ
\dialx PA
\xn si un jour tu trouves une épouse, tu reviendras alors à la maison
\dt 12/Mar/2023

\lx nii
\ph ɳi:
\dialx GOs
\va nii
\dialx PA BO
\is oiseau
\ps n
\ge canard sauvage (autochtone) ; canard à sourcils
\sc Anas superciliosa pelewensis
\scf Anatidés
\dt 21/Mar/2023

\lx niila
\ph ɳi:la
\dialx GOs PA
\is parenté
\ps n
\ge arrière-petit-enfant
\xv niila-nu
\dialx GO
\xn mes arrière-petits-enfants
\xv niila-ny
\dialx PA
\xn mes arrière-petits-enfants
\dt 21/Mar/2023

\lx niilöö
\ph ɳi:lω:
\dialx GOs
\va nhiilö
\dialx PA BO
\is eau
\ps n
\ge remous ; reflux
\ge tourbillon (d'eau, grand et lent)
\cf pomõnim
\ce petit tourbillon rapide
\dt 23/Aug/2021

\lx niivwa
\ph ɳi:βa
\dialx GOs
\va nipa
\dialx GOs arch.
\va niivha
\dialx PA BO
\is fonctions_intellectuelles
\ps v ; n
\ge erreur ; tromper (se)
\ge perdre (se)
\xv e niivwa na ni kò
\dialx GOs
\xn il s'est perdu dans la forêt
\dt 09/Mar/2023

\lx -ni-ma-ba
\ph ɳimamba
\dialx GOs PA BO
\is grammaire_numéral
\ps NUM
\ge neuf (=5 et 4)
\dt 02/Jan/2022

\lx -ni-ma-dru
\ph ɳimanɖu
\dialx GOs
\va -nim (m)a-du
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge sept (=5 et 2)
\dt 02/Jan/2022

\lx -ni-ma-gò
\ph ɳimaŋgɔ
\dialx GOs
\va nim (m)a kòn
\ph nimakɔn
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge huit (=5 et 3)
\dt 26/Aug/2023

\lx ni-malu
\dialx GO
\is caractéristiques_objets
\ps v.stat.
\ge profond ; invisible
\nt non vérifié
\dt 26/Jan/2018

\lx -ni-ma-xè
\ph ɳimaɣɛ
\dialx GOs
\va nim (m)a-xe
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge six (=5 et 1)
\dt 02/Jan/2022

\lx ninigin
\dialx BO
\is armes
\ps n
\ge casse-tête à bout dentelé
\dn est aussi une espèce d'arbre dont on utilisait une partie du tronc et le début des racines coupées en pointe, selon Dubois
\nt non vérifié
\dt 08/Feb/2025

\lx niô
\ph ɳiõ
\dialx GOs BO
\va nhyô
\dialx PA
\is temps_atmosphérique
\ps n
\ge tonnerre
\xv i hûn (e) nhyô
\dialx PA
\xn le tonnerre gronde
\dt 05/Nov/2021

\lx niû
\dialx BO
\is navigation
\ps n
\ge ancre
\xv niû wony
\dialx BO
\xn l'ancre du bateau
\ng v.t. |lx{niûni}
\gt ancrer qqch.
\nt selon Corne ; BM
\dt 21/Feb/2025

\lx niûni
\dialx BO [BM]
\is navigation
\ps v
\ge ancrer
\xv i niûni wony
\dialx BO
\xn il a ancré le bateau
\cf niû
\ce ancre
\dt 25/Aug/2021

\lx nixò
\ph ɳiɣɔ
\dialx GOs
\va nixòòl
\dialx PA BO WEM
\va nigòòl
\dialx BO
\is maison
\ps n
\ge poteau central de la case
\ge mât (bateau)
\se nixò-mwa
\dialx GOs
\sge poteau central de la case
\cf ning kò(òl)
\ce poteau debout
\cf ńodo
\ce gaulettes
\dt 22/Feb/2025

\lx -niza ?
\ph ɳiða
\dialx GOs
\va -nira ?
\dialx PA BO
\is grammaire_interrogatif
\ps INT
\ge combien?
\xv a-niza ?
\dialx GO
\xn combien ? (d'êtres animés)
\xv pu-niza
\dialx GO
\xn combien (de pieds d'arbres)
\xv pwò-nira ?
\dialx PA BO
\xn combien ? (de choses rondes)
\xv wè-nira
\dialx PA
\xn combien (de choses longues)
\xv wa(n)-nira
\dialx PA
\xn combien (de lots de deux roussettes ou notous)
\xv mãè-nira, etc.
\dialx PA BO
\xn combien (de bottes de paille)
\et *pinsa, *pija
\eg combien?
\el POc
\dt 21/Feb/2025

\lx -ni, -ini
\ph ɳi
\dialx GOs
\va -(i)ni
\dialx PA BO
\is grammaire_suffixe_valence
\ps SUFF
\ge suffixe transitif, applicatif
\xv nu zixõ-ni pwiri ma zine
\dialx GO
\xn je raconte l'histoire de la perruche et du rat
\xv i khibwa-raa-ini
\dialx PA
\xn il l'a mal coupé
\dt 29/Jan/2025

\lx nõ
\hm 1
\ph ɳɔ̃
\dialx GOs
\va nõ
\ph nɔ̃
\dialx PA BO WEM WE
\is nom_locatif
\ps n.loc
\ge lieu ; endroit ; plantation
\xv u-da ni nõ-kui
\dialx PA
\xn monter au champ d'ignames
\se nõ-avwõnõ
\dialx GOs
\sge cour de la maison
\se nõ-chaamwa
\sge bananeraie
\se nõ-gò
\sge bambouseraie
\se nõ-lalue
\sge une touffe d'aloès
\se nõ-mu-cee
\sge un massif de fleurs
\se nõ-nu
\sge cocoteraie ; plantation de cocotier
\dt 22/Feb/2025


\lx nõ
\hm 2
\ph ɳɔ̃
\dialx GOs
\va nõ
\dialx PA BO
\is nom_locatif
\ps n.loc
\ge intérieur (à l') ; dans
\xv e u-du ni nõ we
\dialx GO
\xn il plonge sous l'eau
\xv ge je (ni) nõ mwa
\dialx GO
\xn il est dans la maison
\se nõ dili
\sge sous la terre
\se nõ weza
\sge sous la mer
\se nõ phwamwa
\sge dans tout le pays
\cf ni
\ce vers
\et *lalo
\eg intérieur
\el POc
\dt 03/Feb/2025

\lx nõ
\hm 3
\ph ɳɔ̃
\dialx GOs PA BO
\is poisson
\ps n
\ge poisson
\et *lau(k)
\eg fish
\el POc
\dt 07/Sep/2021

\lx nõ
\hm 4
\ph ɳɔ̃
\dialx GOs
\va nõõl
\dialx PA BO
\va nõõ
\dialx PA
\is fonctions_naturelles
\ps v
\ge voir ; regarder
\xv nu nõõ-çö
\dialx GO
\xn je t'ai vu
\xv e nõ ciia xo zine
\dialx GO
\xn le rat voit le poulpe
\xv e nõ-du ni phwa ni gò-mwa
\dialx GO
\xn elle regarde par la fenêtre de la maison
\xv e nõ-wã-du
\dialx GO
\xn elle regarde vers le bas
\xv e nõ-pèńô
\dialx GO
\xn il regarde furtivement
\xv nu nõõ-jo
\dialx BO
\xn je te regarde ; je te vois
\xv nu nõõ-du nõõ-da nai je
\dialx PA
\xn je l'ai regardé de haut en bas
\xv nõõl (a) mèè-n
\dialx PA
\xn elle est voyante (lit. ses yeux voient)
\ng v.t. |lx{nõõ} + objet pronominal ; |lx{nõõli} (+ objet nominal animé) ; |lx{nõ(õ)le} (+ objet nominal inanimé)
\dt 22/Feb/2025

\lx nõ-
\ph ɳɔ̃
\dialx GOs
\va nõ-
\ph nɔ̃
\dialx PA
\is classificateur_numérique
\ps CLF.NUM
\ge préfixe des champs (d'ignames, etc.)
\xv nõ-xè nõ-kui, nõ-tru nõ-kui, nõ-kò nõ-kui, etc.
\dialx GOs
\xn un, deux, trois billons d'ignames
\xv nõ-xè kêê kui, etc.
\dialx GOs
\xn un champ d'ignames
\dt 27/Jan/2022

\lx ńõ
\hm 2
\ph nɔ̃
\dialx GOs
\is grammaire_restrictif
\ps RESTR
\ge seul(ement)
\xv weniza wõ ?  – Ça we-xè ńõ wõ ;  ça we-tru
\dialx GOs
\xn combien de bateaux? – Un seul bateau ; seulement deux
\dt 24/Feb/2025

\lx nõ kaö
\ph ɳɔ̃
\dialx GOs
\is fonctions_naturelles
\ps v
\ge regarder par-dessus
\xv nu nõ kaö-je
\xn je regarde par-dessus elle
\dt 24/Feb/2025

\lx nõ pèńô
\ph ɳɔ̃ pɛɳô
\dialx GOs
\is fonctions_naturelles
\ps v
\ge regarder furtivement
\cf pèńô
\ce voler ; dérober
\dt 22/Feb/2025

\lx nõ thiraò
\ph ɳɔ̃
\dialx GOs
\is caractéristiques_objets
\ps v
\ge transparent (voir à travers)
\dt 06/Jan/2022

\lx nobe
\ph ɳombe
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge voyeur (être)
\xv e nobe
\xn il est voyeur
\dt 26/Jan/2019

\lx nõbo
\ph ɳɔ̃bo
\dialx GOs
\va nõbo, nõbwo
\dialx WEM WE PA BO
\is nom_locatif
\ps n
\ge emplacement ; trace ; marque
\xv e thu nõboo hèlè na ênêda
\dialx GOs
\xn elle fait une entaille avec un couteau (à partir) d'en haut
\se nõboo hèlè
\dialx GOs
\sge blessure du couteau ; traces de couteau
\se nõboo paro
\dialx GOs
\sge morsure ; trace de dent
\se nõbo paro-n
\dialx PA
\sge morsure ; trace de dent
\se nõbo yai
\dialx PA
\sge brûlure ; trace de feu
\se nõbo jigal
\dialx BO
\sge trou fait par une balle de fusil
\dt 22/Feb/2025

\lx nõbu
\ph ɳɔ̃bu
\dialx GOs
\va nõbu
\ph nɔ̃bu
\dialx BO PA
\sn 1
\is coutumes_objet
\ps n
\ge perche avec un paquet
\dn qui signale un interdit
\ge signe d'interdiction
\xv i khabe nõbu
\dialx BO
\xn il a planté une perche d'interdiction
\se ce-nõbu
\sge perche signalant un interdit
\sn 2
\is société_organisation
\ps n
\ge interdit
\ge règle ; loi
\ge protection
\xv e na nõbu nye cee
\dialx GOs
\xn il a mis un interdit sur un arbre
\xv nõbu-ã
\xn nos lois
\se nõbu thoomwã
\sge don coutumier pour les fiançailles d'une jeune-femme
\se na nõbu
\sge poser un interdit
\se phu nõbu
\sge enlever un interdit
\dt 09/Feb/2025

\lx nobwò
\ph ɳobwɔ
\dialx GOs
\va nòbu
\dialx BO PA
\sn 1
\is action
\ps n
\ge tâche ; actes ; actions ; occupations
\xv nobwò-çö
\dialx GO
\xn ta tâche
\xv tu nòbu
\dialx BO
\xn faire une tâche
\xv la nòbu-î mwang
\dialx BO
\xn nos mauvaises actions (Dubois)
\sn 2
\is grammaire_modalité
\ps MODAL.n
\ge devoir ; tâche
\xv nobwò-nu vwo nu na ce-je mõnõ
\dialx GO
\xn je dois (ma tâche) lui donner à manger demain
\dt 03/Feb/2025

\lx nõbwo wha
\ph nɔ̃bwo
\dialx BO
\is corps
\ps n
\ge fontanelle
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ńodo
\ph nondo
\dialx GO WEM BO PA
\is maison
\ps n
\ge gaulettes servant d'appui aux solives
\dn sorte de sablière tenant les gaulettes verticales |lx{me-de} et les chevrons |lx{ce-mwa} du sommet du poteau central |lx{nigol} de la maison ronde (selon Dubois) (étymologie populaire "cou de la marmite")
\dt 10/Jan/2022

\lx nòe, ne
\ph ɳɔe, ɳe
\dialx GOs
\va nòe
\dialx BO
\va ne
\dialx PA
\is action
\ps v
\ge faire ; agir
\se nòe hayu
\sge faire au hasard
\xv me-nòe-wo
\dialx BO
\xn action
\dt 03/Feb/2019

\lx noga
\ph ɳoŋga
\dialx GOs BO
\is religion
\ps n
\ge voyant ; devin
\dt 15/Aug/2021

\lx nõgò
\ph ɳɔ̃ŋgɔ
\dialx GOs BO PA
\sn 1
\is eau
\ps n
\ge rivière ; creek |dialx{PA BO} ; ruisseau
\se ku-nõgò
\dialx GO
\sge amont de la rivière
\se phwè-nõgò
\dialx GO PA
\sge embouchure de la rivière ; confluent d'un creek dans un fleuve
\sn 2
\is topographie
\ps n
\ge ravin ; vallée
\se pwò-nõgo
\dialx BO
\sge petite vallée
\xv ni nõgo jaaòl
\dialx BO
\xn dans la vallée du Diahot
\dt 22/Mar/2023

\lx nõ-kò
\ph ɳɔ̃kɔ
\dialx GOs
\va nõ-ko, nõ-xo
\dialx PA
\is végétation
\ps n
\ge forêt ; brousse ; maquis
\dt 02/Jan/2022

\lx ńõ-kui
\dialx GOs
\is igname
\ps n
\ge partie inférieure de l'igname
\dt 21/Mar/2023

\lx nõ-khia
\dialx BO
\is igname
\ps n
\ge côté mâle du massif d'ignames
\ge billon
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx nõ-maari
\ph ɳɔ̃ma:ri
\dialx GOs
\is fonctions_naturelles
\ps v
\ge regarder avec envie, avec admiration
\dt 06/Jan/2022

\lx no-me
\ph ɳɔme
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge si ; hypothétique
\xv no-me [=novwö me] çö bala a, çö thomã-nu
\dialx GOs
\xn si jamais tu t'en vas, tu m'appelles
\xv e zo ma e no-me zo tree mònõ
\dialx GOs
\xn ce serait bien s'il fait beau demain
\ng |lx{no-me} est la forme courte de |lx{novwö me}
\dt 08/Jan/2022

\lx ńòme
\ph ɳɔme
\dialx GOs PA BO
\is fonctions_naturelles
\ps v
\ge avaler
\et *konom
\el POc
\dt 22/Mar/2023

\lx ńòme hô
\dialx GOs PA
\va nòm-(h)ô
\dialx PA
\is fonctions_naturelles
\ps v
\ge avaler sans mâcher
\dt 22/Mar/2023

\lx no-na
\ph ɳɔ̃ɳa
\dialx GO PA
\va no-ne
\dialx GO PA
\is grammaire_conjonction
\ps CNJ
\ge quand ; si
\xv no-na [=novwö na] uça
\xn quand elle arrivera
\ng |lx{no-na} est la forme courte de |lx{novwö na}
\dt 05/Jan/2022

\lx nõnõ
\hm 1
\dialx GOs PA
\is parenté_adresse
\ps n
\ge tantine (tante maternelle)
\dn terme d'adresse
\dt 14/Feb/2025

\lx nõnõ
\hm 2
\ph ɳɔ̃ɳɔ̃
\dialx GOs
\va nõnõm
\dialx PA BO
\sn 1
\is fonctions_intellectuelles
\ps n
\ge pensée ; rêve
\xv kavwu jaxa nõnõm i la
\dialx PA
\xn ils n'ont pas assez réfléchi
\xv nõnõm
\dialx BO
\xn pensées
\sn 2
\is fonctions_intellectuelles
\ps v
\ge penser ; rappeler (se) ; rêver ; rêvasser
\xv e nõnõ oã-je
\dialx GO
\xn il pense à sa mère
\xv yo ra nõnõ-nu ?
\dialx BO
\xn tu te souviens de moi ?
\ng v.t. |lx{nõnõ} + objet pronominal ou nominal humain ; |lx{nõnõmi} (+ objet nominal inanimé)
\dt 14/Feb/2025

\lx nõnõmi
\ph ɳɔ̃ɳɔ̃mi
\dialx GOs
\va nõnõmi
\dialx PA BO
\is fonctions_intellectuelles
\ps v.t.
\ge penser ; souvenir de (se)
\xv e nõnõmi pomõ-je
\dialx GO
\xn il pense à son pays
\xv çö nõnõmi da ?
\dialx GO
\xn à quoi penses-tu ?
\xv yo ra nõnõmi ?
\dialx BO
\xn tu te souviens de cela ?
\se tre-nõnõmi
\dialx GOs
\sge réfléchir
\ng v.t. |lx{nõnõ} + objet pronominal ou nom humain ; |lx{nõnõmi} (+ nom inanimé)
\ng causatif: |lx{pha-nõnõmi}
\gt rappeler (à qqn.); faire se souvenir de qqch.
\dt 21/Feb/2025


\lx nõõ
\hm 2
\ph ɳɔ̃:
\dialx GOs
\va nòi-n
\dialx BO
\is fonctions_naturelles
\ps v ; n
\ge rêve ; rêver
\xv e kô-nõõ
\dialx GO
\xn il rêve
\xv da nõõ i çö dròrò ?
\dialx GO
\xn qu'as-tu rêvé hier ?
\xv nu kô-nõõ
\dialx GO
\xn j'ai fait un rêve
\xv kô-nõõ-nu nyãnyã
\dialx GO
\xn j'ai rêvé de maman (mon rêve de maman)
\xv nõõ-nu
\dialx GO
\xn mon rêve
\xv nòi-m
\dialx BO
\xn ton rêve
\xv nu kò-nòi-ny i nyãnyã
\dialx BO
\xn j'ai rêvé de maman
\xv ju kò-nòi-m i ri ?
\dialx BO
\xn de qui as-tu rêvé?
\dt 22/Mar/2023

\lx nõõ
\hm 3
\ph ɳɔ̃:
\dialx GOs
\va nõõ
\ph nɔ̃:
\dialx BO PA
\is corps
\ps n
\ge cou ; gorge
\xv nõõ-n
\dialx PA
\xn son cou
\xv pu-nõõ-n
\dialx BO
\xn sa crinière
\dt 22/Mar/2023

\lx ńõõ
\hm 1
\ph nɔ̃:
\dialx GOs
\va nòl
\dialx BO PA
\sn 1
\is fonctions_naturelles
\ps v
\ge éveiller (s') ; réveiller (se)
\xv ńõõ a
\dialx GO
\xn le soleil monte (lit. s'éveille)
\xv pa-nõõli-je
\dialx PA
\xn réveille-le
\sn 2
\is action_tête
\ps v
\ge ouvrir les yeux
\dt 22/Mar/2023

\lx nõõ-baa
\ph ɳɔ̃:ba:
\dialx GO
\is fonctions_naturelles
\ps v
\ge regarder dans le noir
\dt 22/May/2024

\lx nõõ-doo
\dialx BO
\is caractéristiques_objets
\ps n
\ge col de la marmite
\dt 20/Aug/2021

\lx nõõ-hi
\ph ɳɔ̃:hi
\dialx GOs BO PA
\is corps
\ps n
\ge poignet
\xv nõõ-hii-je
\xn son poignet
\xv nõõ-hii-n
\dialx PA
\xn son poignet
\dt 02/Jan/2022

\lx nõõ-kò
\ph ɳɔ̃:kɔ
\dialx GOs
\is corps
\ps n
\ge cheville
\xv nõõ-kòò-je
\xn sa cheville
\dt 08/Jan/2022

\lx nõõle
\ph ɳɔ̃:le
\dialx GOs BO PA
\va nõle
\dialx GOs
\is fonctions_naturelles
\ps v.t.
\ge voir ; regarder
\xv nõle wõ !
\dialx GOs
\xn regarde le bateau !
\dt 19/May/2024

\lx nõõli
\ph ɳɔ̃:li
\dialx GOs
\va nõõli
\ph nɔ̃:li
\dialx BO PA
\is fonctions_naturelles
\ps v.t.
\ge scruter ; regarder
\xv nõõli nye ẽnõ
\dialx BO
\xn regarde cet enfant
\xv e nõõli xa kòi-li
\dialx GO
\xn elle voit qu'ils ne sont plus là
\xv za mhaza nõõli xo je nye êmwê
\dialx GOs
\xn c'est la première fois qu'elle voit un homme (elle n'en a jamais vu avant)
\dt 20/Dec/2021

\lx nõõ-za
\ph ɳɔ̃:ða
\dialx GOs
\is poisson
\ps n
\ge hareng des marais salants
\dn (lit. poisson du sel)
\dt 08/Feb/2025

\lx nõ-paa
\ph ɳɔ̃pa:
\dialx GOs
\is poisson
\ps n
\ge poisson-pierre
\sc Synanceja verrucosa
\scf Scorpænidés
\et *ɲopuq
\el POc
\dt 02/Jan/2022

\lx nõ-thòn
\dialx PA BO
\sn 1
\is végétation
\ps n
\ge broussailles ; maquis ; brousse
\xv poxa nõ-thòn
\dialx BO
\xn enfant illégitime [BM]
\sn 2
\is cultures_champ
\ps n
\ge jachère
\xv i nõ-thòn na mhenõ thu-poã
\dialx BO
\xn le champ est laissé en jachère [BM]
\et *talun
\eg fallow land, land returning to secondary growth
\el POc
\ea Blust
\dt 25/Dec/2021

\lx novwö exa
\ph ɳoβω
\dialx GOs
\va novw-exa
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge quand ; lorsque (passé)
\xv novwö (e)xa tho-da ilie, axe novwö nye, nye Mwani-mi, a-du
\dialx GOs
\xn alors que ces deux-là (les parents) appellent, alors elle, Mwani-mi, sort de la maison
\xv xa novwö (e)xa whamã mwã ã ẽnõ-ò
\dialx GOs
\xn et lorsque ce garçon-là est devenu grand
\cf nou-na, novwö na
\ce quand ; lorsque (futur)
\dt 24/Feb/2025

\lx novwö me
\ph ɳoβω
\dialx GO
\va novwu
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge quand
\xv novwö me nu xa kha-tòè hii-nu, axe balaa-cee za ne mwã kò
\dialx GO
\xn alors même que je tendais aussi le bras, alors (elle a lancé) un bout de bois et il a surgi une forêt
\xv novwu je-nã tèèn na yu ruma a-da
\dialx PA
\xn le jour où tu viendras
\dt 22/Feb/2025

\lx novwö na khõbwe
\ph ɳoβω ɳa
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge si jamais
\xv novwö na khõbwe çö trõne khõbwe ge-le-xa thoomwãaxa Mwani-mii
\dialx GO
\xn si jamais tu entends dire qu'il y a une femme du nom de Mwani-mi
\dt 12/Mar/2023

\lx novwö na … ça
\ph ɳoβω ɳa
\va no-na
\dialx GO
\va novwu-na … ye
\dialx PA
\va nou-na … ye
\dialx PA
\va nopu
\dialx arch.
\is grammaire_conjonction
\ps CNJ
\ge quand ; lorsque (référence au futur)
\ge si (hypothétique)
\xv novwö na mwã huu koi gèè, ça e ru mã ?
\dialx GOs
\xn si nous mangeons le foie de grand-mère, est-ce qu'elle meurt ?
\xv novwö na ge je ni mhenõõ mããni, ça/çe me zoma baa-je
\dialx GOs
\xn quand elle sera en train de dormir, nous la frapperons
\xv no-na uça xo traabwa ênè
\dialx GOs
\xn si elle arrive et qu'elle s'installe ici
\xv yhaamwa novwö a kaze
\dialx GOs
\xn On ne sait pas s'il va à la pêche
\xv nou na nu u nõõli xa kaareng na bwa kò, ye nu khõbwe "Kaavwo mwã je-ne"
\dialx PA
\xn quand j'aurai vu du brouillard dans la forêt, je dirai "ça c'est Kaavwo"
\xv nou na nu ruma nõõli xa kaareng na bwa ko, ye nu ruma khõbwe "Kaavwo mwã je-ne"
\dialx PA
\xn si je vois du brouillard dans la forêt, je dirai "ça c'est Kaavwo"
\xv nowu na nu ruma hoxe havha, mi ruma phe bala-n
\dialx PA
\xn quand je reviendrai, nous reprendrons la suite (à la limite où nous nous sommes arrêtés)
\xv nowu na koi-nu, ye nu a-da Numia
\dialx PA
\xn si je suis absent, c'est que je suis parti à Nouméa
\cf exa
\dialx PA
\ce quand ; lorsque (passé)
\cf novw(u)-exa
\dialx PA
\ce quand ; lorsque (référence au passé)
\cf (k)age novu
\ce pendant (Dubois)
\cf novu êga
\ce pendant (Dubois)
\dt 22/Feb/2025

\lx novwö … ça
\ph ɳoβω
\dialx GOs
\va novwö … ye
\dialx BO PA
\va nopo, novwu
\dialx arch.
\is grammaire_IS
\ps THEM
\ge quant à … alors
\xv novwö lie mèèvwu, ça li pe-kweli-li
\dialx GOs
\xn quant aux deux frères, ils se détestent
\xv novwö nyanya, ye i a-da Numia
\dialx PA
\xn quant à maman, elle est partie à Nouméa
\dt 24/Dec/2021

\lx nõ-wame
\dialx BO
\is fonctions_naturelles
\ps v
\ge voir mal
\nt selon Corne
\dt 27/Mar/2022

\lx noyo
\dialx BO
\is corps
\ps n
\ge sperme
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx nu
\hm 1
\ph ɳu
\dialx GOs
\va nu
\dialx BO PA
\is arbre_cocotier
\ps n
\ge coco (noix de)
\ge cocotier
\sc Cocos nucifera L.
\scf Arécacées
\se dixa nu
\sge lait de coco
\et *niuR
\el POc
\dt 30/Aug/2021

\lx nu
\hm 2
\ph ɳu
\dialx GOs
\va nu
\dialx PA BO
\is grammaire_pronom
\ps PRO 1° pers. SG (sujet ou OBJ)
\ge je
\dt 11/Oct/2021

\lx -nu
\ph ɳu
\dialx GOs
\is grammaire_pronom
\ps PRO 1° pers. SG (OBJ ou POSS)
\ge me ; mon ; ma ; mes
\xv hii-nu
\xn mon bras
\dt 29/Jan/2019

\lx nu hêgi
\ph ɳu hẽŋgi
\dialx GOs PA
\is richesses
\ps n
\ge longueur de monnaie
\xv we-xe nu hêgi
\dialx PA
\xn une longueur de monnaie
\dt 26/Aug/2021

\lx nu ki
\ph ɳu ki
\dialx GOs
\va nu kim
\dialx PA
\is arbre_cocotier
\ps n
\ge coco germé
\dt 17/Aug/2021

\lx nu mãû
\ph ɳu mãû
\dialx GOs
\is arbre_cocotier
\ps n
\ge coco sec
\cf nu-wee
\ce coco vert
\dt 17/Aug/2021

\lx nuãda
\ph ɳuãnda
\dialx GOs PA
\is arbre
\ps n
\ge palmier calédonien
\dn dont la croissance est rapide
\dt 08/Feb/2025

\lx nube
\ph ɳumbe
\dialx GOs
\is mouvement
\ps v
\ge faufiler (se)
\ge glisser (se)
\xv e nube
\xn il s'éclipse sans se faire remarquer ; il prend la tangeante
\dt 22/Feb/2025

\lx nuu
\dialx BO
\is parenté
\ps n
\ge famille
\dn terme sans doute lié au cocotier
\nt Selon BM ; non vérifié
\dt 08/Feb/2025

\lx nuu
\ph ɳũ
\dialx GOs PA
\sn 1
\is tressage
\ps n
\ge fibres longues ; lanières
\ge tresses de fibres de pandanus
\xv e töö nuu-pho bee
\xn elle coupe des fibres fraîches de pandanus
\sn 2
\is corps
\ps n
\ge mèches de cheveux
\xv nuu pu-bwaa-je
\dialx GO
\xn sa mèche de cheveux
\xv nuu pu-bwaa-n
\dialx PA
\xn sa mèche de cheveux
\dt 22/Mar/2023

\lx nûû
\ph ɳû:
\dialx GOs BO PA
\sn 1
\is lumière
\ps v
\ge éclairer (à la lampe, à la torche)
\se nûû xo ya-kha
\dialx GOs
\sge éclairer avec une lampe électrique (|lx{ya-kha} lit. lumière-appuyer)
\xv nûûe mwa !
\dialx BO
\xn éclaire !
\cf nûûe
\ce éclairer qqch.
\sn 2
\is pêche
\ps v
\ge pêcher à la torche
\xv li thu nûû
\dialx BO
\xn ils pêchent à la torche
\xv li tu nûû
\dialx BO
\xn ils descendent pêcher à la torche
\sn 3
\is lumière
\ps n
\ge torche
\ng |lx{nûûa-n} |dialx{BO}
\gt sa torche
\et *(me-)ɲuluq
\el PWMP
\dt 21/Feb/2025

\lx nuu-cee
\ph ɳu:cɨ
\dialx GOs PA
\is bois
\ps n
\ge écharde
\dt 02/Jan/2022

\lx nuu-pho
\ph ɳu:pʰo
\dialx GOs
\is tressage
\ps n
\ge lanières de pandanus (pour le tressage)
\ge fibres
\dt 02/Jan/2022

\lx nu-wee
\ph ɳuwe:
\dialx GOs
\is arbre_cocotier
\ps n
\ge coco vert
\dn contient du liquide
\dt 08/Feb/2025

\lx -ny
\dialx BO PA
\is grammaire_pronom
\ps SUFF.POSS 1° pers.
\ge mon ; ma ; mes
\dt 29/Jan/2019

\lx nyãã
\dialx GOs BO
\is terrain_terre
\ps n
\ge terre d'alluvion
\dt 28/Jan/2019

\lx nyaanya
\dialx BO
\is mouvement
\ps v
\ge dandiner (se) ; se balancer (en marchant)
\nt selon Corne
\dt 26/Mar/2022

\lx nyamã
\dialx GOs PA BO
\sn 1
\is mouvement
\ps v
\ge bouger ; remuer
\xv kêbwa nyamã !
\dialx GO PA
\xn ne bouge pas !
\dn |lx{nyamã} en |dialx{GOs} n'a que le sens de "bouger"
\sn 2
\is action
\ps v ; n
\ge travailler ; travail |dialx{PA BO}
\xv la me-nyamã i nu
\dialx BO
\xn mes travaux/œuvres (Dubois)
\xv thu mhèno-nyamã i nu
\dialx BO
\xn j'ai du travail
\se a-nyamã
\sge travailleur
\dt 22/Feb/2025

\lx nyamãle
\dialx GOs
\is mouvement
\ps v.t.
\ge bouger (un objet)
\dt 16/Feb/2019

\lx nyãnume
\dialx GOs
\is action_corps
\ps v
\ge signe de la main (faire un)
\xv nu nyãnume i je vwö/pu a-mi
\xn je lui ai fait signe de s'approcher
\dt 14/Oct/2021

\lx nyãnyã
\dialx GOs PA BO
\is parenté_adresse
\ps n
\ge maman
\ge tante maternelle
\dn terme d'adresse ou de désignation
\xv nyãnyã i je
\xn sa maman
\xv nyãnyã whamã
\xn la tante maternelle la plus âgée
\dt 08/Feb/2025

\lx nyè
\ph ɲɛ
\dialx GOs
\va nyèn
\dialx PA BO
\ph ɲɛn
\va nhyèn
\dialx WEM
\is plantes
\ps n
\ge curcuma ; gingembre (comestible)
\et *yaŋo
\eg turmeric
\el POc
\ea Blust
\dt 26/Aug/2021

\lx nyejo!
\dialx PA
\is parenté_adresse
\is grammaire_vocatif
\ps vocatif
\ge maman!
\dt 18/Aug/2021

\lx nyèn êmwèn
\dialx PA
\is plantes
\ps n
\ge gingembre mâle (non comestible)
\dn cette plante a une sorte de bourgeon qui fleurit de couleur rouge
\dt 26/Aug/2021

\lx nyiwã
\dialx GOs
\is position
\ps v.stat.
\ge recroquevillé
\xv e kô-nyiwã
\xn il dort recroquevillé
\dt 26/Jan/2018

\lx nyò
\dialx GOs
\is grammaire_démonstratif
\ps DEM.ANAPH
\ge cela (en question)
\mr forme courte de |lx{nye-ò}
\dt 08/Jan/2022

\lx nyòlò
\dialx PA
\va nyolõng
\dialx PA
\is action_tête
\ps v
\ge grimacer ; faire des grimaces
\xv i nyolõng
\dialx PA
\xn il fait des grimaces
\dt 09/Feb/2019

\lx ńhã
\ph nʰɛ̃
\dialx GOs
\va ńã
\dialx GOs
\va nhã
\dialx WEM WE BO PA
\is fonctions_naturelles
\ps n
\ge crotte ; excréments
\xv ńhõ-chòvwa
\dialx GO
\xn crotin de cheval
\xv nhõ-êgu
\dialx PA
\xn excrément de personne
\ng forme déterminée ou en composition: |lx{nhõ-}
\dt 22/Mar/2023

\lx nhe
\hm 1
\ph nʰe
\dialx PA
\is feu
\ps n
\ge bûche
\dt 04/Feb/2019

\lx nhe
\hm 2
\ph ɳʰe
\dialx GOs
\va nhe
\dialx PA BO
\is navigation
\ps n
\ge voile (bateau) ; bâche
\se nhewa-wô
\dialx GO
\va nhe-a wony
\dialx BO
\sge la voile du bateau
\se wony nhe
\dialx BO
\sge bateau à voile
\ng forme déterminée: |lx{nhe(w)a-}
\et *layaR
\eg voile
\el POc
\dt 13/Jan/2022

\lx nhei
\ph ɳʰei
\dialx GOs GA
\va nhei
\dialx PA
\is fonctions_naturelles
\ps v
\ge crampe (avoir une)
\xv e nhei kòò-nu
\dialx GO
\xn j'ai une crampe à la jambe
\xv mã nhei
\xn crampe (lit. maladie crampe)
\dt 23/Aug/2021

\lx nhi
\hm 1
\dialx BO
\is caractéristiques_objets
\ps v
\ge crisser (sous la dent) ; abrasif
\xv i hni la hovho
\dialx BO
\xn la nourriture crisse sous la dent
\dt 20/Aug/2021

\lx nhi
\hm 2
\ph nʰi
\dialx BO [BM]
\is terrain_pierre
\ps n
\ge roc ; rocher calcaire
\se paa-hni
\sge rocher calcaire (Dubois)
\dt 16/Aug/2021

\lx nhi
\hm 3
\ph ɳʰi
\dialx GOs
\va nhil
\dialx PA BO
\sn 1
\is fonctions_naturelles
\ps v
\ge moucher (se)
\ge renifler |dialx{PA}
\xv nu nhi
\dialx GO
\xn je me mouche !
\xv nhile têi-çö
\dialx GO
\xn mouche-toi ! (lit. mouche ta morve)
\xv nu nhile têi-nu
\dialx GO
\xn je me mouche ! (lit. je mouche ma morve)
\ng v.t. |lx{nhile}
\gt moucher qqch.
\sn 2
\is fonctions_naturelles_animaux
\ge renâcler (cheval)
\dt 21/Feb/2025

\lx nhii
\ph ɳʰi:
\dialx GOs
\va nhii
\dialx PA BO
\is action_plantes
\ps v
\ge cueillir à la main (fruit, baies)
\xv mõ a nhii-vwo
\dialx GO
\xn nous allons faire la cueillette
\xv yo nhi xa pòò-n (n)a pò-xe
\dialx PA
\xn cueille un fruit
\dt 24/Dec/2021

\lx nhiida
\ph ɳʰi:nda
\dialx GOs
\va niida
\dialx BO
\is insecte
\ps n
\ge lentes
\et *li(n)sa
\eg lentes
\el POc
\dt 29/Jan/2019

\lx nhiiji
\dialx BO PA
\va nhiije
\dialx PA BO
\is chasse
\ps n
\ge piège à oiseau ; collet
\ge lacet (à oiseau)
\dn utilisé pour prendre les oiseaux dans les arbres
\dt 08/Feb/2025

\lx ńho
\ph nʰo
\dialx GOs
\is igname
\ps n
\ge partie supérieure du tubercule d'igname
\dn cette partie supérieure est replantée après le prélèvement du bas du tubercule
\ng v.t. |lx{ńhome}
\gt couper et prélever
\dt 05/Jan/2022

\lx ńhõ-
\ph nʰɔ̃
\dialx GOs
\va ńõ-
\dialx GOs
\va nhõ-
\dialx PA BO
\sn 1
\is fonctions_naturelles
\ps n
\ge crotte
\xv ńhõ-je
\dialx GO
\xn ses excréments
\xv nhõõ-m
\dialx PA BO
\xn ta crotte
\ng forme déterminée ou en composition: |lx{nhã-}
\sn 2
\is discours_interjection
\ps n
\ge bien fait !
\xv e nhõõ-m !
\dialx WEM WE
\xn bien fait pour toi ! (lit. c'est ta crotte)
\xv i nhõ-n !
\dialx PA BO
\xn bien fait pour lui !
\dt 22/Mar/2023

\lx ńhôã
\ph nʰôã
\dialx GOs
\is santé
\ps v
\ge constipé
\xv e ńhôã
\xn il est constipé
\dt 14/Mar/2019

\lx ńhõbe
\ph nʰɔ̃mbe
\dialx GOs
\is fonctions_naturelles
\ps v
\ge crampe (avoir une)
\dt 25/Jan/2019

\lx nhõginy
\ph nʰɔ̃ŋgiɲ
\dialx PA
\va nõginy
\dialx BO
\is insecte
\ps n
\ge araignée
\dn de terre, noire, grosse
\dt 08/Feb/2025

\lx nhõî
\ph ɳʰɔ̃î
\dialx GOs PA
\va nhõî, nhõõî
\dialx BO
\sn 1
\is action_corps
\ps v
\ge lier ; ligoter ; attacher
\xv e pe-nhõî-le wa
\dialx GO
\xn il a attaché les cordes l'une à l'autre
\sn 2
\is cultures
\ge attacher la tige d'igname
\dt 19/Aug/2021

\lx ńhòme kui
\ph nʰɔme
\dialx GOs
\is cultures
\ps v
\ge couper et prélever le bas du tubercule d'igname et replanter la partie supérieure avec ses lianes
\dn cette opération se fait quand l'igname est encore verte
\dt 22/Aug/2021

\lx nhõõl
\dialx BO PA
\va nõõl
\dialx BO PA
\is richesses
\ps v
\ge argent ; monnaie
\ge richesses ; biens précieux
\xv nhõõla-ny
\xn mes biens
\xv nhõõla-ri ?
\xn à qui sont ces biens ?
\se nhõõ mii
\sge argent européen (lit. rouge, en cuivre)
\se nhõ pujo/pulo
\dialx BO
\sge monnaie blanche (en argent)
\ng forme déterminée: |lx{n(h)õõla-}
\dt 05/Jan/2022

\lx nhõõxi
\ph ɳʰɔ̃:ɣi
\dialx GOs
\va nhõõxi
\ph nʰɔ̃:ɣi
\dialx PA BO
\is préparation_aliments
\ps v
\ge envelopper et attacher (nourriture)
\xv nhõõxi mwata
\dialx BO
\xn envelopper du |lx{mwata} (préparation culinaire)
\dt 11/Jan/2022

\lx nhu
\ph ɳʰu
\dialx GOs
\va nhu
\ph nʰu
\dialx BO
\is mouvement
\ps v
\ge écrouler (s')
\ge dégringoler
\ge glisser
\xv e nhu dili na êńa
\dialx GO
\xn le terrain a glissé à cet endroit-là
\xv i nhu dili na êna
\dialx BO
\xn le terrain a glissé à cet endroit-là
\dt 26/Aug/2021

\lx nhuã, nuã
\ph ɳʰuɛ̃ ɳuɛ̃
\dialx GOs
\va nhuã, nuã
\ph nʰuɛ̃ nuɛ̃
\dialx BO PA
\is action_corps
\ps v
\ge lâcher ; relâcher
\ge laisser tomber
\xv nhuã-nu !
\dialx GOs
\xn lâche-moi !
\xv e nhuã cova
\dialx GOs
\xn il a relâché le cheval
\xv nhuã khô-choova !
\dialx GOs
\xn lâche la longe du cheval !
\xv lha u nhuã i la je yaai
\dialx PA
\xn ils leur laissent ces allumettes
\se pa-n(h)uã
\dialx PA BO
\sge laisser partir ; relâcher
\dt 22/Feb/2025

\lx nhya
\ph ɲʰa
\dialx GOs
\va nhyal
\dialx PA
\is caractéristiques_objets
\ps v
\ge écrasé ; mou
\dn une fois écrasé
\ng v.t. |lx{nhyale}
\gt ramollir ; écraser qqch.
\dt 22/Feb/2025

\lx nhyã
\ph ɲʰã
\dialx GOs PA BO
\is pêche
\ps n
\ge appâts ; amorces (pêche)
\xv nhyã-m
\dialx BO
\xn ton amorce
\dt 06/Feb/2019

\lx nhyã
\dialx GOs
\va nhyang
\dialx PA BO
\is coutumes
\ps n ; v
\ge coutume (cérémonie coutumière) ; fête ; occupations
\ge occuper (s')
\xv nhyã ponga/poxa …
\dialx GO
\xn une fête pour/de qqch.
\xv pe-nhyang
\dialx PA
\xn s'occuper avec qqch.
\xv i nee nhyanga-n na hi-n
\dialx PA
\xn il lui donne le don coutumier (don à la mère qui a élevé un garçon ; ce don est fait quand l'enfant a 7-8 ans)
\se nhyaga
\dialx BO
\sge fête de
\dt 22/Feb/2025

\lx nhyang mòlò
\dialx PA
\is coutumes
\ps n
\ge coutumes accompagnant la vie
\dn ce mot réfère à l'ensemble de cérémonies ou de dons coutumiers qui accompagnent le développement de l'enfant, de la conception jusqu'au moment où il porte le manou et qui sont offerts au clan maternel (i.e. à l'oncle maternel) ; ces gestes coutumiers ne sont faits que pour le premier né
\dt 19/Aug/2021

\lx nhyang mhã
\ph ɲʰaŋ
\dialx PA
\is coutumes
\ps n
\ge coutumes accompagnant le déclin de la vie
\dn ce mot réfère à l'ensemble de cérémonies ou de dons coutumiers qui accompagnent les signes de déclin de la personne jusqu'à sa mort et qui sont offerts au clan maternel (à l'oncle maternel)
\cf mhãu
\dt 19/Aug/2021

\lx nhyanga-n
\ph ɲʰaŋgan
\dialx PA BO [BM]
\is coutumes
\ps n
\ge don à la mère pour la remercier d'avoir élevé un garçon
\dn ce don se fait vers l'âge de 7-8 ans
\dt 16/Feb/2019

\lx nhyal
\dialx PA BO
\sn 1
\is caractéristiques_objets
\ps v
\ge mou
\ge gâté (fruit)
\xv nhyal ma nhyal
\dialx PA
\xn totalement mou
\xv li pa-nhyale, ra whã da
\dialx PA
\xn elles l’écrasent comme de la cendre
\ng causatif: |lx{pa-nhyale}
\gt écraser ; ramollir
\ng v.t. |lx{nhyale}
\gt ramollir
\sn 2
\is santé
\ps v
\ge purulent (blessure)
\dt 22/Feb/2025

\lx nhyale
\dialx GOs BO PA
\sn 1
\is action_corps
\ps v
\ge écraser (dans la main) ; ramollir
\se nhyal
\sge mou
\sn 2
\is cultures
\ps v
\ge émotter
\se nhyale dili
\sge émotter
\dt 26/Jan/2019

\lx nhyatru
\ph ɲʰaʈu
\dialx GOs
\va nhyaru
\ph ɲʰaɽu
\dialx GO(s)
\sn 1
\is caractéristiques_objets
\ps v.stat.
\ge mou ; trop mûr
\se pwaji nhyatru, pwaji nhyaru
\dialx GOs
\sge crabe mou
\sn 2
\is santé
\ps v.stat.
\ge couvert d'ulcères ; couvert de gale
\dt 26/Aug/2021

\lx nhyatru dili
\dialx GOs
\is terrain_terre
\ps n
\ge bonne terre (molle)
\dt 28/Jan/2019

\lx nhye
\dialx GOs PA
\va n(h)ya, n(h)yã, n(h)ye-ã
\dialx GOs PA BO
\wr A
\is grammaire_démonstratif
\ps DET.DEM
\ge ce …-ci ; ce(tte) ; ces
\xv nõõli nhya nõ !
\dialx GO
\xn regarde ce poisson !
\xv nye kuau
\dialx GO
\xn ce chien
\xv da nye pwaixe ba ?
\dialx GO
\xn quelle est cette chose là-bas ?
\xv e cabo mwã nyã we
\dialx GO
\xn cette source a jailli
\ng |lx{n(h)yã} est la forme courte de |lx{n(h)ye-ã}
\wr B
\is grammaire_démonstratif
\ps PRON.DEM.
\xv ma nhye iwe nhyã we kaavwu mwa
\dialx GO
\xn car c'est vous qui êtes les gardiens de la maison
\xv nhye u a
\dialx GO
\xn celle qui est partie
\xv e za u mhaza hoo-î mwã nhye
\dialx GO
\xn celui-ci va enfin devenir notre nourriture
\xv ti nhye mããni ?
\dialx GO
\xn qui est celui qui dort ici ?
\xv ti nye a ? – Za inu nye a
\dialx GO
\xn qui est-ce qui part ? – C'est moi qui pars
\xv da nye ku ? (ou) da nyã ku ?
\dialx GO
\xn qu'est-ce qui est tombé?
\xv da nyã i kul ?
\dialx PA
\xn qu'est-ce qui est tombé?
\xv phe nyã
\dialx PA
\xn prends ceci ! (en le tendant vers l'interlocuteur)
\se n(h)ye-ni
\sge cette chose-là
\se n(h)ye-na
\sge cette chose-là (DX2)
\se n(h)ye-ba
\sge cette chose-là sur le côté (pas loin)
\se n(h)ye-òli
\sge cette chose-là-bas (DX3)
\se n(h)ye-bòli
\sge cette chose loin en bas
\se n(h)ye-du mu
\sge cette chose derrière
\se n(h)ye-du
\sge cette chose en bas
\se n(h)ye-da
\sge cette chose en haut
\dt 22/Feb/2025

\lx nhye-ba
\dialx GOs WEM WE
\is grammaire_démonstratif
\ps PRON-DEIC
\ge cela (distance moyenne, mais visible)
\xv ge nhye-ba thoomwã êba
\xn il y a cette femme-là là-bas
\dt 03/Feb/2025

\lx nhye-da
\ph ɲʰɛ̃nda
\dialx GOs PA
\is grammaire_démonstratif
\ps PRON.DEM-DIR
\ge cela en haut
\an nhye-du
\at cela en bas
\dt 03/Feb/2025

\lx nhye-du
\ph ɲʰɛ̃ndu
\dialx GOs PA
\sn 1
\is grammaire_démonstratif
\ps PRON.DEM-DIR
\ge cela en bas
\xv je bi na ênè i tho-du ni nye-du we
\dialx PA
\xn cette conduite d'eau là dans laquelle coule l'eau en bas
\sn 2
\is temps
\ps PRON.DEM-DIR
\ge réfère à un événement plus bas dans la chaîne
\xv ra novwo nye-du tèèn a po-xè, a lhi u pe-kõbwe na le kõbwe "gasi"?
\dialx PA
\xn et un beau jour, elles se disent "on y va ?"
\xv novwo nye-du thõbònin a po-xè, a lhi u tòne u hale tho je we
\dialx PA
\xn mais un soir, elles entendent que le bruit de l'eau est différent
\xv li yu a(xe) novwo nyèdu tèèn (a)-du pò-xè
\dialx PA
\xn ils vivaient (là) et un beau jour
\dt 03/Feb/2025

\lx nhye-na
\ph ɲʰeɳa
\dialx GOs
\sn 1
\is grammaire_démonstratif
\ps DEICT.2 ; ANAPH
\ge cela
\sn 2
\is temps_deixis
\ps DEICT.TEMP
\ge maintenant ; tout de suite
\xv we a èńiza ? – Me a nhye-na !
\xn quand partez-vous ? –  Nous partons là maintenant !
\xv we a èńiza ? – Me a nhye !
\dialx GO
\xn quand partez-vous ? – Nous partons tout de suite !
\dt 24/Feb/2025

\lx nhyò
\ph ɲʰɔ
\dialx GOs PA
\va nyò, nyõ
\dialx BO
\is mammifères
\ps n
\ge essaim de roussettes
\ge nid de roussettes
\dn (endroit où les roussettes se posent de jour)
\dt 20/Feb/2025

\lx nhyô
\dialx GOs
\ph ɲʰõ
\va nyong, nyô
\dialx PA
\is action_eau_liquide_fumée
\ps v ; n
\ge baisser (niveau d'eau) ; descendre (niveau d'eau)
\ge décrue
\xv e nhyô we
\dialx GO
\xn l'eau baisse
\xv i nyông (a) kale
\dialx PA
\xn la marée descend
\dt 20/Dec/2021

\lx nhyôgò we
\ph ɲʰõŋgɔ
\dialx GOs
\va nyôgo we
\dialx PA BO
\is caractéristiques_objets
\ps n
\ge niveau (d'eau) ; profondeur
\xv e whaya nhyôgò we ? – E nhyô
\dialx GO
\xn comment est le niveau de l'eau ? –  Il a baissé
\xv a-ho-du-ò vwö po nhyôgò-çö ênè kòlò wããge-çö
\dialx GO
\xn avance un peu pour que le niveau de l'eau t'arrive à la poitrine (lit. pour que ton niveau d'eau soit à la poitrine)
\xv e tha-nhyôgò we
\dialx GO
\xn elle sonde la profondeur de l'eau
\cf nhyô
\ce baisser (niveau de l'eau)
\dt 22/Feb/2025

\lx nhyõli
\dialx GOs WEM WE PA BO
\va nyõli n(h)ye-õli
\is grammaire_démonstratif
\ps DEM.DEIC.3 distal
\ge cela là-bas (loin des interlocuteurs)
\xv mõ-cö nhyõli ma mõ-nu nhye
\dialx GO
\xn voilà ta maison là et voici la mienne
\xv yo phe-da mwã ni nhyõli  mwa
\dialx PA
\xn apporte-le dans cette maison là-bas
\xv li ra u a, hava mwã-è ênõli ni nyõli we
\dialx PA
\xn elles partent, arrivent enfin là-bas à cette rivière
\xv ti nyõli êgu-õli !
\dialx BO
\xn qui est-ce là-bas ?
\mr |lx{n(h)yõli} est la forme courte de |lx{n(h)ye-õli}
\dt 03/Feb/2025

\lx nhyôni
\dialx GOs
\ph ɲʰõɳi
\va nyõnim
\dialx PA BO
\is temps_atmosphérique
\ps n
\ge éclair
\dt 28/Jan/2019

\lx ò
\hm 2
\dialx GOs PA
\va -ò, hò
\dialx PA
\is grammaire_suff_directionnel
\ps V.DIR (centrifuge)
\ge éloigner (s') du locuteur
\xv ka(vw)u nu ru ò
\dialx PA
\xn je n’irai pas là-bas
\dt 25/Aug/2023

\lx -ò
\hm 1
\dialx GOs
\va -wò
\dialx GO(s)
\va -ò, hò
\dialx PA
\is grammaire_suff_directionnel
\ps DIR (centrifuge)
\ge éloigner (s') du locuteur
\xv e trêê-ò
\dialx GO
\xn il partit en courant
\xv e tia-da xo zòò-da-ò je
\dialx GO
\xn il pousse vers l'avant et nage en avant
\xv tha hò mwa ni puu-n
\dialx PA
\xn va là-bas au pied (de l’arbre)
\se ã-ò !
\sge va-t-en !
\se ã-du-ò !
\sge descends (en s'éloignant) !
\se phe-da-ò
\sge emporte-le en haut
\se phe-du-ò
\sge emporte-le en bas
\se tia-ò !
\sge pousse!
\se tia-da-ò !
\sge pousse vers le haut !
\dt 25/Dec/2021

\lx -ò
\hm 3
\dialx GOs PA
\is grammaire_démonstratif
\ps ANAPH (discours)
\ge cela (anaphorique) ; celui ; celle
\xv thoomwã-ò
\xn cette femme-là
\xv ije-ò thoomwã-ò
\xn c'est elle la femme en question
\xv axe novwö nye õã-lò Poimenya-ò yue Paola, ça fami õã-lò
\dialx GOs
\xn et quant à leur mère à eux trois, cette Poymegna qui a adopté Paola, leur mère fait partie de la famille
\xv i puya je=ò paa
\dialx PA
\xn il soulève un caillou
\dt 22/Feb/2025

\lx -ò
\hm 4
\dialx GOs
\va -o
\dialx PA WEM WE BO
\is grammaire_vocatif
\ps vocatif
\ge vocatif
\se nyanya-ò !
\dialx GOs
\sge ô maman !
\se caay-ò !
\dialx GO PA
\sge papa !
\se caaya-o !
\dialx WEM
\sge papa !
\se caay-o !
\dialx BO
\sge papa !
\se nyaj-o ! nyej-o
\dialx PA
\sge maman !
\et *-aw
\el PAN vocatif
\dt 16/Feb/2025

\lx õ
\hm 1
\ph ɔ̃
\dialx GOs BO PA
\is parenté
\ps n
\ge mère ; sœur de mère
\ge cousines de mère
\ge épouse du frère de père ; épouse des cousins de père
\xv õã-nu
\dialx GOs
\xn ma mère ; ma tante maternelle
\xv õõ-n
\dialx PA
\xn sa mère
\xv õõ-ny
\dialx BO
\xn ma mère
\xv õ Pol
\dialx BO
\xn la mère de Paul
\ng forme déterminée: |lx{õã-} |ph{ɔ̃ɛ̃} ou |lx{õõ-}
\dt 22/Feb/2025

\lx õ
\hm 2
\dialx GOs
\ph ɔ̃
\va òn
\ph ɔn
\dialx BO PA
\sn 1
\is eau_mer
\ps n
\ge sable |dialx{GOs PA BO}
\sn 2
\is eau_mer
\ps n
\ge sel |dialx{PA BO}
\et *qone
\eg sable
\el POc
\dt 23/Jan/2022

\lx õ
\hm 3
\dialx GOs
\ph ɔ̃
\va ô
\dialx BO PA
\is grammaire_quantificateur_mesure
\ps QNT
\ge tout ; tous
\se õ tree
\dialx GOs
\sge tous les jours
\se õ mhwããnu
\dialx GOs
\sge tous les mois
\se ô tèèn
\dialx PA BO
\sge tous les jours
\se ô taagin
\dialx PA
\sge souvent
\dt 16/Feb/2025

\lx õ-
\dialx GOs PA BO
\va on
\dialx PA BO
\is grammaire_quantificateur_mesure
\is classificateur_numérique
\ps CLF.NUM (multiples)
\ge n-fois
\se õ-xè
\sge une fois
\se õ-tru
\dialx GOs
\va õ-ru
\dialx PA BO
\sge deux fois
\xv õ-tru xa e kaò ni kabu-è
\dialx GOs
\xn cela fait deux fois que cela déborde (rivière) dans la semaine
\dt 16/Feb/2025

\lx -õ
\dialx GOs
\va -(h)õ
\is grammaire_pronom
\ps PRO.1° pers. triel.INCL. objet ou POSS
\ge nous trois (incl.) ; nous (paucal)
\xv e thaivwi-bulu-õ mwã
\xn il nous (paucal) rassemble ici
\xv je-ne mwã pe-ole kòlò-hõ hãgana
\xn voici encore (un geste) de remerciement entre nous maintenant
\dt 08/Feb/2025

\lx -ò … -mi
\dialx GOs PA WEM BO
\is grammaire_suff_directionnel
\ps DIR
\ge de-ci de-là ; en allant et venant ; partout
\xv e thumenõ-ò thumenõ-mi
\dialx GOs
\xn il fait des allées et venues
\xv i töö-ò töö-mi
\dialx PA
\xn il rampe par-ci par-là
\xv i yöö-ò yöö-mi xo je
\dialx PA
\xn il rampe par-ci par-là
\xv la pa(o)-ò pao-mi boo ; la pa-ò vao-mi boo
\dialx GOs
\xn ils lancent le ballon d'un côté et de l'autre
\xv lha wòvwa-ò wovwa-mi na ni nõ mwa-è
\dialx GOs
\xn ils se bagarrent partout à l'intérieur de cette maison
\xv novwö (e)na mõ ku-ò ku-mi le
\dialx GOs
\xn quand nous allons de-ci de-là
\xv e kênõ-ò kênõ-mi
\dialx GOs
\xn il tourne de-ci de-là
\xv e palè-ò palè-mi wo khila
\dialx GOs
\xn elle tâtonne de-ci de-là pour la chercher
\xv lha a-ò mwã a-mi
\dialx GOs
\xn ils vont de-ci de-là
\xv i a phõng-ò phõng-mi
\dialx PA
\xn il va en zigzag
\xv e hivwi pò-caai, lã-ã lha thrõbo-ò, thrõbo-mi êne
\dialx GOs
\xn elle ramasse des pommes canaques, celles qui sont tombées de-ci de-là ici
\xv e ra (x)o yaoli wã-ò wã-mi
\dialx WEM
\xn elle continue à se balancer comme ceci et comme cela (d'un côté et de l'autre)
\dt 24/Dec/2021

\lx ôã-mû-cee
\dialx GOs
\va ô-muuc
\dialx PA
\is plantes_partie
\ps n
\ge pied principal d'une plante
\dn (lit. mère des fleurs)
\dt 08/Feb/2025

\lx ôdri
\dialx GOs
\is armes
\va ôdim
\dialx PA BO
\ps n
\ge pierre de fronde (générique)
\dt 08/Oct/2021

\lx õgi
\ph ɔ̃ŋgi
\dialx GOs
\va õgin
\dialx BO PA WEM
\ph ɔŋgin
\sn 1
\is grammaire_aspect
\ps v.TERM
\ge finir ; terminer ; être prêt
\se ba-õgine
\dialx BO
\sge fin
\xv e õgine mõgu xo ã ẽnõ
\dialx GO
\xn l'enfant a fini son travail
\xv çö õgine kuuni mwã lã=nã
\dialx GO
\xn tu as entièrement fini ces (plantations)
\xv u õgin
\dialx PA
\xn c'est fini
\xv nu pavage õgin
\dialx PA
\xn j'ai tout préparé
\xv õgine vha !
\dialx BO
\xn arrête de parler
\ng v.t. |lx{õgine} |dialx{GOs}
\cf kûûni
\dialx GOs
\ce terminer complètement
\sn 2
\is grammaire_aspect
\ps ASP.ACC
\ge déjà
\xv nu õgi thözoe
\dialx GO
\xn je l'ai déjà caché
\xv çö õgi ã-du Frans ?
\dialx GO
\xn es-tu déjà allé en France ?
\xv e uvwi-õgine-ni a-xe êgu xè pò-mã
\dialx GO
\xn il a acheté vingt mangues
\xv nu õgi hovwo
\dialx PA
\xn j'ai déjà mangé
\sn 3
\is grammaire_aspect
\ps SEQ
\ge puis ; et puis ; ensuite
\xv e pwe, õgi e thuvwu phai nõ
\dialx GO
\xn il a pêché, puis il a cuit le poisson pour lui-même
\xv e pweni nõ, õgi e thuvwu phai
\dialx GO
\xn il a pêché le poisson, puis il l'a cuit pour lui-même
\et *qoti
\eg finir
\el POc
\dt 24/Feb/2025

\lx ohaim
\dialx PA
\va ohahèm
\dialx BO [Corne]
\is fonctions_naturelles
\ps v
\ge bâiller
\dt 25/Jan/2019

\lx ohe
\dialx PA
\va whe
\dialx PA
\is grammaire_locatif
\ps ADV
\ge côté (sur le) ; côté (à)
\xv po tee-ohe
\dialx PA
\xn assieds-toi un peu plus loin sur le côté
\dt 28/Jan/2019

\lx ô-jitua
\dialx GOs BO [Corne]
\va ô-jirua
\dialx GOs
\is armes
\ps n
\ge arc
\dt 22/Mar/2023

\lx ole
\dialx GOs PA BO
\is interaction
\ps v ; INTJ
\ge remercier ; merci
\xv nu ole nai çö
\dialx GOs
\xn je te remercie
\xv ole nai çö
\dialx GOs
\xn merci à toi
\dt 13/Aug/2021

\lx -òli
\dialx GOs
\va hòli
\is grammaire_démonstratif
\ps DEIC.3 (visible)
\ge là ; là-bas
\xv êgu-òli
\xn cette personne-là
\xv êgu-mãli-òli
\xn ces deux personnes-là
\xv êgu-mãla-òli
\xn ces personnes-là
\xv kwau-òli !
\xn le chien là-bas
\xv êmwê-òli !
\xn l'homme là-bas
\xv e thaavwu na ni bò-òli
\xn cela (le terroir) commence sur la pente là-bas
\dt 26/Dec/2021

\lx olo
\dialx GOs BO
\is feu
\ps v
\ge flamber ; brûler
\xv i olo
\dialx BO
\xn ça flambe
\dt 26/Jan/2019

\lx oloomã
\dialx GOs BO
\is feu
\ps n
\ge flamme
\se oloomã yaai
\sge flamme du feu
\dt 23/Aug/2021

\lx õmwã
\dialx GOs
\is mollusque
\ps n
\ge bernard-l'ermite (gastéropode)
\sc Dardanus megistos
\scf Décapodes
\et *qumaŋ
\eg bernard-l'ermite
\el POc
\dt 27/Aug/2021

\lx -on
\dialx BO
\is grammaire_restrictif
\ps RESTR + NUM
\ge seul ; seulement
\xv pòi-n (ha)da a-xè-on
\xn son seul enfant (Dubois)
\dt 20/Feb/2023

\lx òn
\dialx PA BO
\ph ɔn
\sn 1
\is eau_mer
\ps n
\ge sable ; plage ; rivage
\xv bwa òn
\dialx PA BO
\xn sur la grève ; sur le rivage ; sur le sable
\sn 2
\is nourriture
\ps n
\ge sel
\cf õne
\dialx BO
\ce saler
\et *qone
\eg sable
\el POc
\dt 22/Feb/2025

\lx õn na
\dialx PA
\is grammaire_aspect
\ps n.CNJ
\ge chaque fois que
\xv õn na yu havha, ye kòi-yu
\dialx PA
\xn chaque fois que je viens, tu n'es pas là
\dt 29/Jan/2019

\lx õ-niza ?
\dialx GOs
\va õ-nira ?
\dialx BO
\is grammaire_interrogatif
\ps INT
\ge combien de fois?
\xv õ-niza çö a-da Numea ?
\dialx GOs
\xn combien de fois es-tu allé à Nouméa ?
\dt 03/Feb/2019

\lx ono-n
\dialx BO
\is corps
\ps n
\ge estomac
\nt selon Coyaud ; non vérifié
\dt 26/Mar/2022

\lx ôô
\hm 1
\dialx BO
\is fonctions_naturelles_animaux
\ps n
\ge fumier ; crotte (animal)
\se ôô vaci
\dialx BO
\sge bouse de vache
\se ôô choval
\dialx BO
\sge crottin de cheval
\nt selon Corne
\dt 26/Mar/2022

\lx ôô
\hm 2
\dialx GO
\is grammaire_assertif
\ps INTJ
\ge oui
\xv Ôô ! e mõõdi !
\dialx GOs
\xn Oui, il en a honte !
\dt 13/Oct/2021

\lx òòl
\dialx PA BO [Corne]
\is caractéristiques_objets
\ps v
\ge suinter ; ramollir ; étirer (s')
\dn comme de la gomme
\xv òòl (a) we-phwaa-n
\xn il bave (se dit d'un bébé)
\dt 08/Feb/2025

\lx oole
\dialx PA BO
\sn 1
\is pêche
\ps v
\ge barrage à la pêche (faire un)
\dn avec des cailloux, pierres, branches
\sn 2
\is action
\ge barrer ; empêcher
\ps v.t.
\xv i oole dèn u je cee
\dialx PA
\xn cet arbre a barré la route
\xv i oole-vwo
\dialx PA
\xn il a fait un barrage ; il a bloqué (l'eau)
\xv i oole dèn
\dialx PA
\xn il a bloqué la route
\xv la oole-nu na nu a-è
\dialx BO
\xn ils m'ont empêché de partir
\cf khibwaa dèn
\ce barrer ; couper la route
\dt 22/Feb/2025

\lx oole we
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge barrer l'eau
\dt 26/Jan/2018

\lx õ-pe-gan
\dialx PA WE BO
\va haivwö
\dialx GO(s)
\is grammaire_quantificateur_mesure
\ps QNT
\ge souvent ; plusieurs fois
\dn (lit. 3 fois)
\dt 16/Feb/2025

\lx ô-phwalawa
\dialx GOs
\va ô-palawa
\dialx BO
\is nourriture
\ps n
\ge levain
\dn (lit. mère du pain)
\dt 08/Feb/2025

\lx orã
\dialx GOs BO
\is arbre
\ps n
\ge orange ; oranger
\bw orange (FR)
\dt 22/Feb/2025

\lx orèyi
\dialx GO WEM
\sn 1
\is maison
\ps n
\ge gaulettes circulaires du toit
\dn ces gaulettes retiennent la couverture du toit, i.e. les écorces de niaouli et la paille
\sn 2
\is maison
\ps v
\ge couper le bois qui sert de gaulettes
\cf zabò
\dt 25/Aug/2021

\lx òri
\hm 1
\ph ɔri
\dialx GOs BO
\va òtri
\ph ɔʈi
\dialx GO(s) WE
\is caractéristiques_personnes
\ps v.stat.
\ge fou (être)
\ge saoul ; ivre
\xv e òri
\dialx GOs
\xn il est fou
\cf kulèng
\dt 24/Feb/2025

\lx òri
\hm 2
\dialx GOs
\is grammaire_adverbe
\ps ADV
\ge adversatif
\xv e za ããbe òri !
\xn il croit tout savoir !
\xv çö za hine lòlò òri !
\xn tu dis des bêtises ! (lit. tu sais sans savoir)
\xv a-kô-ii òri ẽnõ ã !
\xn cet enfant s'agite beaucoup en dormant
\xv e ala òri êgu ba !
\xn ce qu'il est maladroit cet homme !
\xv a-pejooli òri êgu ba !
\dialx PA
\xn qu'est-ce qu'il est râleur cet homme là-bas !
\dt 08/Feb/2025

\lx õ-taagi
\dialx GOs
\va õ-taagin
\dialx PA BO
\is grammaire_aspect
\ps ADV fréquentatif
\ge souvent ; toujours
\xv i khõbwe õ-taagin
\dialx PA
\xn il dit souvent
\dt 16/Feb/2025

\lx õ-thõm
\dialx BO
\is natte
\ps n
\ge natte-manteau à longues pailles
\nt selon Corne
\dt 26/Mar/2022

\lx otròtròya
\ph oɽɔ'ɽɔya
\dialx GOs WEM
\va oroyai
\dialx GO(s) PA BO
\va yüe
\dialx BO
\is interaction
\ps v ; n
\ge bercer (un enfant) ; berceuse (chant)
\dt 27/Jan/2019

\lx ô-uvwia
\dialx GOs
\is plantes_partie
\ps n
\ge tige de taro de montagne
\dn tige principale (lit. mère du taro)
\dt 26/Aug/2021

\lx ovwee
\ph oβe:
\dialx GOs
\va ovee
\dialx BO [BM]
\is action
\ps v
\ge ôter ; enlever
\xv ovwee hõbwõli-ço
\dialx GOs
\xn enlève ta chemise
\xv ove hõbwõni-m
\dialx BO
\xn enlève ta chemise
\dt 29/Jan/2019

\lx õ-wara
\dialx PA
\va õ-waran
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge chaque fois que ; souvent
\xv õ waran ne yu havha …
\dialx PA
\xn chaque fois que tu arrives
\xv õ waran ne pwal …
\dialx PA
\xn chaque fois qu'il pleut
\xv õ-waran na yö havha
\dialx PA
\xn chaque fois que tu arrives
\xv õ-waran na i pwal
\dialx PA
\xn chaque fois qu'il pleut
\xv nu ra khõbwe õ-waran
\dialx PA
\xn chaque fois qu'on dirait qu'il va pleuvoir
\dt 22/Mar/2023

\lx õ-xè
\ph õɣɛ
\dialx GOs BO PA
\va hokè, hogè
\dialx BO
\is grammaire_numéral
\ps n-fois
\ge une fois
\ge un autre ; un nouveau
\xv õ-tru, õ-kò
\dialx GOs
\xn deux autres, trois autres
\xv nu trõne õ-tru thixa jige
\dialx GOs
\xn j'ai entendu deux autres coups de fusil
\dt 22/Mar/2023

\lx õ-xè on
\dialx BO PA
\is grammaire_numéral
\ps n-fois
\ge une seule fois
\nt selon Dubois ; non vérifié
\dt 22/Mar/2023

\lx õxèè
\dialx GOs
\va oxa
\dialx BO
\is grammaire_aspect
\ps ITER ; REV
\ge encore ; de nouveau
\xv e õxèè mõlò mwa
\dialx GOs
\xn il est revenu à la vie
\xv nee õxèè
\dialx GOs
\xn refais le !
\xv oxa na
\dialx BO
\xn donne encore [BM]
\se õxè nòe
\dialx BO
\sge faire à nouveau
\dt 22/Mar/2023

\lx õ-xèè nõ
\ph ɔ̃ɣɛ: ɳɔ̃
\dialx GOs
\is grammaire_numéral
\ps n-fois
\ge une seule fois
\cf pòxè nõ
\ce un seul
\dt 22/Mar/2023

\lx oya
\dialx PA
\is plantes_partie
\ps n
\ge résine
\se oya jeü
\sge résine de kaori
\se oya waawè
\sge résine de pin colonnaire
\dt 26/Aug/2021

\lx pa
\dialx GOs PA
\is tressage
\ps v
\ge tresser
\xv i pa keel
\dialx PA
\xn elle tresse un panier
\xv e pa thrô
\dialx GO
\xn elle fait du tressage de natte
\xv içö nye çö pae thrô ã ?
\dialx GO
\xn est-ce toi qui a tressé cette natte ?
\ng v.t. + objet défini: |lx{pae} |dialx{GOs}, |lx{pai} |dialx{PA}
\gt tresser qqch.
\et *patu
\eg tresser
\el POc
\dt 21/Feb/2025

\lx pa-
\dialx GOs PA
\va para
\dialx GOs PA
\is grammaire_quantificateur_degré
\ps DEGRE
\ge totalement ; très
\xv e za u cii pa-baa
\dialx GO
\xn il est vraiment très noir de peau
\xv e pa-phozo mwã
\dialx GO
\xn elle est très blanche
\xv u pa-hinõ, kixa tãî-n
\dialx PA
\xn elle est totalement nue, elle n'a pas de manou
\xv i pa-tha
\dialx PA
\xn il est totalement chauve
\se pa-baan
\dialx PA
\sge très noir; noirâtre
\se pa-whaa
\dialx GO
\sge très gros
\ng forme courte de |lx{para}
\gt très
\cf mhã
\ce très
\dt 08/Feb/2025

\lx -pa
\dialx GOs BO PA
\va -ba
\dialx GOs BO PA
\is grammaire_numéral
\ps NUM
\ge quatre
\et *pat
\eg quatre
\el POc
\dt 17/Aug/2021

\lx pã
\dialx WE
\is nourriture
\ps n
\ge pain
\bw pain (FR)
\dt 26/Jan/2019

\lx paa
\hm 1
\dialx GOs
\va paa
\dialx PA BO
\is guerre
\ps n
\ge guerre
\xv li pe-thu-paa lie phwe-meevwu
\dialx GO
\xn les deux clans se font la guerre
\se kapu paa, kavwu paa
\dialx GO
\sge chef de guerre
\se thi-paa, to-paa
\dialx BO
\sge embûches ; embuscade
\se paa tu
\sge mettre en fuite
\dt 22/Feb/2025

\lx paa
\hm 2
\dialx GOs PA BO
\is terrain_pierre
\ps n
\ge pierre ; caillou
\se mwa-paa
\sge grotte
\se phwè-paa
\dialx GO
\sge grotte
\se po paa
\dialx BO
\sge petit caillou
\se paa ni gò
\dialx GO
\sge pile (caillou pour la radio/musique)
\se paa ni ya-khaa
\dialx GO
\sge pile pour la torche
\et *patu
\eg pierre
\el POc
\dt 26/Aug/2021

\lx paaba
\dialx GOs
\is navigation
\ps v
\ge ramer
\se ba-paaba
\sge rame
\cf haal
\ce ramer
\dt 20/Jan/2019

\lx paa-bule
\dialx PA
\is action_eau_liquide_fumée
\ps v
\ge tremper dans l'eau
\dt 26/Jan/2019

\lx paaçae
\ph pa:ʒae
\dialx GOs
\is eau_mer
\ps n
\ge vague (mer)
\dt 29/Jan/2019

\lx paa-çôôe
\dialx GOs WEM
\ph pa:ʒõe
\va pa-nyoî
\dialx BO
\is action_corps
\ps v
\ge accrocher ; suspendre qqch.
\xv nu paa-çôôe ke
\dialx GO
\xn j'accroche le panier
\cf côô
\ce suspendu ; accroché
\dt 22/Feb/2025

\lx paa-duu-n
\dialx PA
\is étapes_vie
\ps v
\ge adulte (être) ; mature
\dn (lit. pierre-os)
\xv i paa-duu-n
\xn il est adulte
\xv ra u paa-duu-n
\xn il est adulte (il a les os en pierre)
\dt 09/Feb/2025

\lx paang
\dialx PA BO
\is cultures
\ps v ; n
\ge désherber ; faucher ; désherbage
\ge arracher l'herbe (à la main)
\xv ni bala paang i bin
\xn dans la partie de notre désherbage
\se ba-paang
\sge faucille
\ng v.t. |lx{paage}
\cf phaawa
\dialx GOs
\ce désherber
\dt 10/Jan/2022

\lx paaje
\ph pa:ɲɟe
\dialx GOs PA
\va paaye
\dialx PA BO
\is topographie
\ps n
\ge chaîne centrale
\dt 28/Jan/2019

\lx paa-majing
\dialx PA BO
\is terrain_pierre
\ps n
\ge quartz
\dn pierre blanche et dure, utilisée pour le four enterré
\dt 26/Aug/2021

\lx paa-moze
\dialx GOs
\va pa-mole
\dialx PA BO
\is action_eau_liquide_fumée
\ps v
\ge vider ; avaler (un liquide)
\dt 22/Feb/2025

\lx paa-neng
\dialx BO
\is coutumes
\ps n
\ge pierre ; couteau de circoncision
\dn fait dans cette pierre
\nt selon Corne
\cf tragòò ; tagòò
\ce circoncire
\nt non vérifié
\dt 08/Feb/2025

\lx paa-nhi
\dialx GOs
\is terrain_pierre
\ps n
\ge rocher (gros)
\dt 16/Aug/2021

\lx paara
\dialx BO
\is fonctions_intellectuelles
\ps n
\ge information ; nouvelle
\nt selon Corne
\dt 22/Feb/2025

\lx paa-trèè-mii
\dialx GOs
\va pa-tèè-mii
\dialx BO
\is feu
\ps n
\ge schistes
\ge pierre rouge
\dn elle est frappée pour produire des étincelles et allumer le feu
\dt 23/Aug/2021

\lx paawa
\dialx GOs
\is igname
\ps n
\ge igname (clone)
\dt 29/Jan/2019

\lx paça
\ph paʒa
\dialx GOs
\va payang, paya, peyeng
\dialx WEM PA BO
\is déplacement_moyen
\ps n
\ge route ; chemin frayé à l'outil
\dt 28/Aug/2021

\lx paçagò
\ph paʒaŋgɔ
\dialx GOs
\va payagòl
\dialx PA
\va peyego
\dialx BO
\is santé
\ps n
\ge hernie
\dt 25/Jan/2019

\lx paçô
\ph paʒõ
\dialx GOs
\is interaction
\ps MODIF
\ge gâter (enfant) ; vanter (se)
\xv e vhaa paçô-je
\dialx GOs
\xn il se vante ; il est vaniteux
\dt 22/Feb/2025

\lx pada
\dialx GOs
\va phada
\dialx PA
\is son
\ps v
\ge faire un bruit de percussion 
\dn comme des maracas
\ge cliqueter ; faire un bruit de cliquetis
\xv da jene i pada na ni kèè-m ?
\dialx PA
\xn qu'est ce qui racasse dans ton sac ?
\dt 08/Feb/2025

\lx page
\dialx GOs BO
\va pwaxe
\dialx BO
\is coutumes
\ps n
\ge tas de vivres
\se pa-page, pa-vwage
\sge préparer, prendre soin
\dt 28/Jan/2019

\lx pa-hinõ
\dialx GOs PA
\is habillement
\ps v
\ge nu (être)
\dn (lit. montrer)
\xv e pa-hinõ
\dialx GOs
\xn il est nu (il montre)
\dt 08/Feb/2025

\lx pa-hovwo-ni
\ph pahoβoɳi
\dialx GOs
\is nourriture
\ps v
\ge nourrir ; faire manger
\xv e pa-hovwo-ni la ẽnõ
\xn elle fait manger les enfants
\dt 09/Apr/2024

\lx pai
\hm 1
\dialx GOs BO
\va pae
\dialx GO(s)
\va pele
\dialx PA
\sn 1
\is tressage
\ps v ; n
\ge tresser ; faire une tresse
\ge tresse
\ge natte (faire une)
\sn 2
\is cordes
\ps v ; n
\ge torsader
\ge corde à plusieurs brins (tresser une)
\se pai wa
\dialx GOs
\sge tresser ou torsader une corde
\et *patu
\el POc
\dt 09/Jan/2022

\lx pai
\hm 2
\dialx GOs
\va pain
\dialx BO PA
\sn 1
\is plantes_partie
\ps n
\ge tubercule comestible
\xv kia pain
\dialx PA
\xn il n'y a pas de tubercule
\xv pain manyok
\dialx BO
\xn tubercule de manioc
\sn 2
\is discours
\ps n
\ge parole (chanson)
\xv wa xa ge le pai ?
\dialx GOs
\xn cette chanson a-t-elle des paroles ?
\cf paxa-vhaa
\ce mots ; paroles de chanson (en composition)
\dt 22/Feb/2025

\lx pai
\hm 3
\dialx GOs
\va phai
\dialx PA BO
\is classificateur_possessif
\ps CLF.POSS (armes)
\ge arme (de jet ou assimilé)
\xv pai-nu jige
\dialx GO
\xn mon fusil
\xv phai-ny bulaivi
\dialx PA
\xn mon casse-tête
\xv phai-ny nye bulaivi
\dialx PA
\xn c'est à moi ce casse-tête
\xv phai-ny ôdim
\dialx PA
\xn ma pierre de fronde
\xv phai-ny wamòn
\dialx PA BO
\xn ma hache
\xv phai-ny jigèl
\dialx PA
\xn mon fusil
\cf pha
\ce tirer ; lancer
\dt 22/Feb/2025

\lx pa-kamaze
\dialx GOs
\is habillement
\ps v
\ge mettre à l'envers (vêtements)
\dt 26/Jan/2018

\lx pa-kêmi
\ph pakẽmi
\dialx GOs
\is étapes_vie
\ps v
\ge enterrer quelqu'un
\xv la pa-kêmi-du je
\xn ils l'ont enterré
\dt 09/Apr/2024

\lx pa-ku-gòò-ni
\ph pakuŋgɔ:ɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge approuver
\dn (lit. dire que c'est droit, correct)
\se ku-gòò
\sge vrai ; droit ; juste
\dt 22/Feb/2025

\lx pala
\dialx BO
\is discours
\ps n
\ge parole (ancien)
\xv pala-ny
\xn mes paroles
\xv nu kõbwe pala
\xn j'ai dit des paroles
\nt selon BM
\dt 26/Mar/2022

\lx palao
\dialx BO
\is paniers
\ps n
\ge panier de réserves
\dn autour du mur
\nt selon BM
\dt 08/Feb/2025

\lx palawu
\dialx BO
\is eau_topographie
\ps n
\ge récif de corail
\nt selon BM
\dt 26/Mar/2022

\lx pale
\dialx GOs BO PA
\va palee
\dialx BO
\sn 1
\is action_corps
\ps v
\ge tâtonner
\ge chercher à tâtons
\ge palper
\ge effleurer
\xv e pale khila hõbwòli-je
\dialx GO
\xn elle cherche sa robe à tâtons
\xv la u pale pale na mwã kai wèdò
\dialx PA
\xn ils ont bien tâtonné pour que nous suivions les usages
\sn 2
\is pêche
\ps v
\ge fouiller dans un trou (sans y voir) ; plonger le bras dans qqch.
\ge pêcher à la main (sans y voir)
\ng |lx{pale} vs. |lx{palee}
\gt les formes définie et indéfinie se distinguent par la longueur
\dt 09/Mar/2023

\lx palu
\dialx GOs BO
\is échanges
\ps v
\ge avare (être) ; refuser de donner
\xv i paluu-nu
\dialx GO
\xn il a refusé de me donner qqch.
\xv i palu
\dialx BO
\xn il est mesquin
\se a-palu
\sge un avare
\ng v.t. |lx{paluni}
\gt priver qqn. de qqch.
\dt 21/Feb/2025

\lx pa-mudree
\ph pamuɖe:
\dialx GOs
\is action_corps
\ps v
\ge déchirer ; trouer (linge)
\xv e pa-mudree hõbwòli-je
\dialx GOs
\xn cela a déchiré son vêtement
\dt 09/Apr/2024

\lx panooli
\dialx BO [Corne]
\is plantes
\ps n
\ge plante
\sc Plectranthus parviflorus
\scf Labiées
\dn cette plante pousse au ras du sol et fait des petites fleurs bleues ; la décoction des feuilles et tiges sert de purge pour les bébés ; elle est aussi utilisée pour annoncer la naissance d'un enfant à son oncle maternel : on offre une tige de cette plante liée à une sagaie (pour un garçon) ou à une écorce de niaouli (pour une fille) (Guide des plantes du chemin kanak 1988)
\nt non vérifié
\dt 22/Oct/2021

\lx pa-nuã
\ph paɳuɛ̃
\dialx GOs
\va pa-nhuã
\dialx PA BO
\sn 1
\is action_corps
\ps v
\ge lâcher ; laisser sortir (prisonnier)
\ge laisser tomber
\ge déposer
\sn 2
\is grammaire_modalité
\ps v
\ge autoriser |dialx{GO}
\xv la pa-nuã-nu dròòrò
\dialx GO
\xn ils m'ont laissé sortir hier
\dt 02/Jul/2024

\lx pa-nue
\ph paɳue
\dialx GOs
\is interaction
\ps v
\ge apaiser
\dt 02/Jan/2022

\lx pa-nûû
\dialx GOs
\is lumière
\ps n
\ge torche
\dt 29/Jan/2019

\lx pa-nûûe
\ph paɳû:e
\dialx GOs
\is lumière
\ps v
\ge éclairer
\xv nu pa-nûûe-je
\dialx GOs
\xn je l'ai éclairé
\dt 02/Jan/2022

\lx pao
\dialx GOs
\va paò
\dialx PA BO
\sn 1
\is action_corps
\ps v
\ge jeter en l'air ; lancer
\ge agiter
\se pao-ò pao-mi
\sge lancer/jeter en tout sens
\se pao pwiò
\sge lancer le filet
\se pao pwe
\sge lancer la ligne
\se pao-du piò
\sge frapper avec la pioche (lit. lancer la pioche en bas)
\se pao jige
\sge tirer au fusil
\se pao paa
\sge jeter des cailloux
\xv i paò dèèn
\dialx BO
\xn le vent souffle fort
\cf pawe
\ce lancer à côté
\sn 2
\is armes
\ps v
\ge tirer (au fusil)
\sn 3
\is déplacement
\ps v
\ge prendre la route ; mettre (se) en route
\dt 20/Feb/2025

\lx pao kinu
\dialx GOs
\is navigation
\ps v
\ge ancrer ; jeter l'ancre
\dt 26/Jan/2018

\lx pao nã
\ph pao ɳɛ̃
\dialx GOs
\va pao nãã-n
\dialx PA
\is interaction
\ps v
\ge injurier ; offenser
\dn (lit. jeter offense)
\xv e pao nãã-nu
\dialx GOs
\xn il m'a offensé
\cf tòè nãã-n
\dialx PA
\ce injurier
\dt 08/Feb/2025

\lx pao paxa-nãã-n
\dialx PA
\is interaction
\ps n
\ge offense ; injure
\xv i pao paxa-nãã-ny
\dialx PA
\xn il m'a offensé
\dt 20/Dec/2021

\lx pao-biini
\dialx PA
\is action
\ps v
\ge cabosser
\dt 29/Jan/2019

\lx pao-cabi
\dialx PA
\is action_corps
\ps v
\ge tambouriner
\dt 26/Jan/2019

\lx pao-drale
\dialx GOs
\va pao-dale
\dialx PA
\is action_corps
\ps v
\ge casser en jetant
\dt 22/Mar/2023

\lx pao-gaaò
\dialx PA
\is action_corps
\ps v
\ge casser (du verre en jetant)
\dt 26/Jan/2019

\lx pao-kibi
\dialx PA
\is action
\ps v
\ge faire éclater (en jetant)
\xv la pao-kibi bwaa-n
\dialx PA
\xn ils lui ont fracassé la tête (avec un casse-tête)
\dt 29/Jan/2019

\lx pao-khau
\dialx PA
\is action_corps
\ps v
\ge jeter par-dessus ; lancer par-dessus
\dt 24/Feb/2025

\lx pao-khi-dale
\dialx PA
\is action_corps
\ps v
\ge fracasser (en jetant)
\dt 26/Jan/2019

\lx paophi
\dialx GOs
\is pêche
\ps v
\ge faire du bruit dans l'eau pour effrayer les poissons
\dn se fait en frappant les mains dans l'eau geste analogue à celui fait pour le jeu |lx{thathibul}
\dt 08/Feb/2025

\lx pao-thali
\dialx PA
\is action_corps
\ps v
\ge frapper (enfant)
\dt 26/Jan/2019

\lx pa-pii-ni
\ph papi:ɳi
\dialx GOs
\is préparation_aliments
\ps v
\ge vider qqch. (marmite)
\dt 29/Mar/2022

\lx pa-pònume
\dialx PA BO
\is action
\ps v.t.
\ge raccourcir
\dt 29/Jan/2019

\lx papua
\dialx GOs BO
\is igname
\ps n
\ge igname (longue et dure)
\dt 17/Feb/2025

\lx pa-pho
\ph papʰo
\dialx GOs PA
\is tressage
\ps v ; n
\ge tressage ; vannerie
\ge tresser ; faire de la vannerie
\xv novwö pa-pho ca/ça mõgu i baa-êgu
\dialx GOs
\xn quant à la vannerie, c'est le travail des femmes
\cf pai-pho
\dt 02/Jan/2022

\lx pa-phuu-ni
\ph 'papʰu:ɳi
\nph accent sur la syllabe initiale
\dialx GOs
\is action
\ps v
\ge gonfler qqch.
\cf phuu
\ce enflé
\dt 29/Mar/2022

\lx pa-phwa-ni
\ph papʰwaɳi
\dialx GOs BO
\is action
\ps v
\ge trouer (ballon)
\ge déchirer (tissu)
\xv e pa(a)-phwa-ni hõbwòli-nu
\dialx GOs
\xn il a déchiré ma robe
\dt 02/Jan/2022

\lx para
\hm 1
\dialx BO [Corne]
\is plantes
\ps n
\ge herbe 
\dn poussant le long des rivières
\sc Brachiaria mutica
\scf Graminées
\dt 08/Feb/2025

\lx para
\hm 2
\dialx GOs
\is grammaire_quantificateur_degré
\ps DEGRE
\ge très
\xv e para pòńõ
\dialx GOs
\xn il est très petit
\ng |lx{para} ne s'utilise qu'en référence à des humains/animés
\dt 06/Mar/2023

\lx parang
\dialx BO
\is préparation_aliments
\ps v
\ge rôtir ; frire
\se ba-parang
\sge poêle à frire
\dt 26/Aug/2021

\lx paree
\dialx PA BO
\is interaction
\ps v
\ge négliger ; délaisser ; abandonner
\xv la paree nai je whamã
\xn ils ont négligé ce vieil homme (ils ne s'en sont pas occupés)
\cf thaxiba
\ce rejeter
\dt 22/Dec/2021

\lx parèma
\dialx GOs
\va parèman
\dialx PA
\is nom_locatif
\ps n
\ge côte
\ge rivage ; bord de mer
\se parèma pom(w)õ-li
\dialx GOs
\sge côte est (de l'autre côté)
\se parèma pomwã
\dialx GOs
\sge côte ouest
\dt 23/Jan/2022

\lx parixo
\dialx GOs
\is mouvement
\ps v
\ge faire des culbutes ; faire des tonneaux (voiture)
\dt 26/Jan/2018

\lx pa-tòò-e
\ph patɔ:ɛ
\dialx GOs
\is préparation_aliments
\ps v.t.
\ge réchauffer (nourriture)
\xv la pa-tòò-e dröö
\xn elles réchauffent les marmites
\dt 02/Jan/2022

\lx pa-trevwaò
\ph paʈeβaɔ
\dialx GOs
\va pa-tevwaò
\ph pateβaɔ
\dialx PA BO
\is action_corps
\ps v
\ge jeter
\xv e pa-trevwaò ja-je bwabu
\dialx GOs
\xn elle a jeté ses saletés par terre
\dt 02/Jan/2022

\lx patrô
\dialx GOs
\ph paʈõ
\va parô-, parôô-n
\dialx BO PA
\is corps
\ps n
\ge dent
\xv parôô-n
\dialx PA
\xn ses dents
\cf whau
\ce édenté
\dt 24/Jan/2019

\lx pa-thrô
\ph paʈʰõ
\dialx GOs
\is tressage
\ps v
\ge tresser une natte ; faire une natte
\dt 02/Jan/2022

\lx pavada
\dialx GOs
\va pavade
\dialx PA WE
\is son
\ps v
\ge bruit (faire du)
\xv pavade kivha doo
\dialx PA
\xn le couvercle de la marmite fait du bruit
\dt 26/Aug/2021

\lx pavwa
\ph paβa
\dialx GOs
\is action
\ps v.i
\ge préparatifs (faire les)
\xv tree pavwa mã iwa mwã a
\dialx GOs
\xn préparez-vous à partir
\xv lha pavwa ponga nhyãã-mõlò
\dialx GOs
\xn ils font les préparatifs pour les fêtes coutumières
\xv lha pavwa ponga pe-na-vwo
\dialx GOs
\xn ils font les préparatifs pour les fêtes coutumières
\dt 03/Feb/2025

\lx pavwã
\ph paβɛ̃
\dialx GOs
\is fonctions_intellectuelles
\ps v ; n
\ge confiance (avoir) ; espoir ; espérer
\dt 27/Jan/2019

\lx pavwange
\ph paβaŋe
\dialx GOs
\va pavang, pavange
\dialx BO
\is action
\ps v.t.
\ge préparer (en général) ; faire des parts (vivres)
\dt 20/Aug/2021

\lx pavwaze
\ph paβaze
\dialx GOs
\is discours_tradition_orale
\ps v
\ge réciter les généalogies
\xv êgu e aa-pavwaze
\xn celui qui fait le discours sur les généalogies
\dt 28/Jan/2019

\lx pawe
\dialx GOs BO
\va paawe
\dialx BO
\is action_corps
\ps v
\ge jeter ; lancer ; frapper (de haut en bas)
\xv hê-kee-nu la nõ nu pawe pwiò
\dialx GOs
\xn j'ai pris ces poissons au filet épervier (lit. en lançant le filet épervier)
\dt 20/Aug/2021

\lx pawe hii-je
\dialx GOs
\is interaction
\ps v
\ge faire un signe de la main
\xv e pawe hii-je
\xn il lui a fait un signe de la main
\dt 19/Aug/2021

\lx paxa
\dialx PA
\is plantes
\ps n
\ge herbes vertes
\dn poussant dans les rivières ou terrains marécageux
\dt 08/Feb/2025

\lx paxa-
\dialx GOs
\is préfixe_sémantique
\ps PREF
\ge contenant ; produit de
\se paxa-wa
\sge thème de chant
\cf hê
\dt 24/Jan/2022

\lx paxaa
\dialx GOs PA
\is santé
\ps n
\ge maladie contagieuse (grippe, etc.)
\xv nu tròòli paxaa
\dialx GOs
\xn j'ai attrapé la grippe
\dt 26/Aug/2021

\lx paxaawa
\dialx GOs PA
\is plantes
\ps n
\ge coléus (symbole de vie)
\sc Lamiaceæ Plectranthus scutellarioides (L.) R.Br. (Caballion); Solenostemon scutellarioides
\scf Labiacées
\dn un bouquet de coleus accompagne la monnaie traditionnelle donnée aux oncles maternels à la naissance de l'enfant
\dt 22/Mar/2023

\lx paxa-dili
\dialx PA BO [Corne]
\is cultures
\ps n
\ge motte de terre
\dt 26/Jan/2019

\lx paxadraa
\dialx GOs
\va paxadaa
\dialx WEM
\is préparation_aliments
\ps v
\ge frire
\se ba-paxadraa
\sge poêle à frire
\dt 03/Feb/2019

\lx paxa-ka
\dialx GOs
\is igname
\ps n
\ge prémices
\dn la première igname récoltée de l'année
\cf ka
\ce année ; an
\dt 22/Feb/2025

\lx paxa-kîbi
\dialx GOs BO PA
\va paxawa kîbi
\dialx GO(s)
\is préparation_aliments
\ps n
\ge pierres du four enterré
\cf paa
\ce pierre
\cf kîbi
\ce four
\dt 10/Feb/2019

\lx paxa-kui
\dialx GOs BO PA
\is plantes_partie
\ps n
\ge tubercule d'igname
\dt 29/Jan/2019

\lx paxa-kumwala
\dialx GOs PA
\is plantes_partie
\ps n
\ge tubercule de patate douce
\xv kia paxa-kumwala
\xn il n'y a pas de patate douce
\dt 29/Jan/2019

\lx paxa-nãã-n
\dialx PA BO
\is interaction
\ps n
\ge injure ; insulte ; offense
\ge mauvais sort
\cf nãã-n
\ce injure
\dt 19/Aug/2021

\lx paxa-nõ
\ph pa'ɣa-nɔ̃
\dialx GOs PA BO
\va paga-nõ
\dialx BO
\is corps
\ps n
\ge estomac ; gésier
\xv paxa-nõ-nu
\dialx GO
\xn mon estomac
\xv paxa-nõ-n
\dialx PA BO
\xn son estomac
\dt 06/Nov/2021

\lx paxa-nõ-dröö
\dialx GOs
\va paxa-nõ-doo
\dialx PA
\is caractéristiques_objets
\ps n
\ge fond de la marmite (face externe)
\dt 22/Mar/2023

\lx paxa-nõ-pu
\dialx BO
\is fonctions_naturelles
\ps n
\ge crachat (de grippe)
\nt selon Corne
\dt 26/Mar/2022

\lx paxa-pwe
\dialx GOs
\is pêche
\ps n
\ge plomb de la ligne
\dt 26/Jan/2018

\lx paxa-pwiò
\dialx GOs
\is pêche
\ps n
\ge plomb du filet
\dt 26/Jan/2018

\lx paxa-uva
\dialx GOs BO PA
\is plantes_partie
\ps n
\ge tubercule du taro d'eau
\dt 29/Jan/2019

\lx paxa-vhaa
\ph pa'ɣa-va:
\dialx GOs
\is discours
\ps n
\ge mot
\dn (lit. produit de la parole)
\dt 09/Feb/2025

\lx paxa-wa
\dialx GOs BO
\va paga-wal
\dialx PA
\is musique
\ps n
\ge paroles de la chanson ; thème d'un chant
\se la paxa-wa
\dialx GOs
\va la paga-wal
\dialx PA
\sge les paroles de la chanson
\dt 09/Jan/2022

\lx paxa-we
\dialx GOs
\is pêche
\is configuration
\ps n
\ge trou d'eau
\dn dans une rivière ou la mer
\dt 08/Feb/2025

\lx paxa-zee
\dialx GOs
\is fonctions_naturelles
\ps n
\ge expectorations
\dt 08/Mar/2019

\lx paxa-zume
\dialx GOs
\is fonctions_naturelles
\ps n
\ge crachat
\xv paxa-zume i je
\xn son crachat
\dt 30/Dec/2021

\lx paxe
\dialx PA BO
\is action_eau_liquide_fumée
\ps v
\ge dévier (eau)
\xv paxe we
\xn dévier l'eau
\xv nu paxe hale we
\xn j'ai dévié l'eau
\dt 26/Jan/2019

\lx paxeze
\dialx GOs
\is interaction
\ps v
\ge interdire qqch. (momentanément) ; empêcher (qqn. de faire qqch.)
\xv e paxeze-nu vwo nu a-mi kòlò-je
\xn il m'a empêché de venir chez lui
\dt 17/Feb/2025

\lx paxu
\dialx GOs PA
\sn 1
\is habillement
\ps v
\ge nu (être)
\xv i paxu
\dialx PA
\xn il est nu
\sn 2
\is grammaire_quantificateur_mesure
\ps v
\ge manquer
\xv paxu bwèdò hii-je
\dialx GOs
\xn il lui manque un doigt
\cf bwehulo, bwexulo
\ce manquer
\dt 16/Feb/2025

\lx payang
\dialx BO
\is déplacement
\ps n
\ge grande route
\dt 21/Jan/2018

\lx pazalõ
\is habillement
\dialx GOs PA
\va pajalõ
\dialx GO(s)
\ps n
\ge pantalon
\bw pantalon (FR)
\se pajalõ pagoo
\dialx GO
\sge short
\dt 20/Dec/2021

\lx pa-zo
\dialx PA
\va payo
\dialx BO
\is cultures
\ps v
\ge qui donne/produit bien (champ)
\dt 26/Jan/2019

\lx pazòò
\dialx GOs
\va paarò
\dialx PA
\is terrain_terre
\ps n
\ge terre damée ; terre dure
\dt 22/Feb/2025

\lx pe
\hm 1
\dialx GOs BO PA
\is poisson
\ps n
\ge raie
\et *paRi
\eg raie
\el POc
\dt 29/Jan/2019

\lx pe
\hm 2
\dialx PA
\is chasse
\ps n
\ge plateforme posée dans les arbres
\dn situés sur le passage des roussettes, les chasseurs s'y postaient et interceptaient les roussettes avec des branches pour les faire tomber au sol
\et *pataR
\eg platform of any kind
\el POc
\ea Blust
\dt 08/Feb/2025

\lx pe
\hm 3
\dialx PA BO
\is maison
\ps n
\ge gaulettes formant la corbeille
\dt 17/Feb/2019

\lx pe-
\hm 1
\dialx PA BO
\is grammaire_aspect
\ps ASP
\ge en train de
\ge sans but (activité)
\xv e za pe-trabwa êbòli bwa õ vwo za pe-tree-nõ-du
\xn il est resté assis là-bas en bas sur le sable et il reste à regarder vers la mer
\xv novo dòny i pe-na-hi-n bwa dèèn
\dialx PA
\xn la buse, quant à elle, plane avec le vent
\xv nu ga pe-kû pò-puleng
\dialx BO
\xn je suis en train de manger un fruit de Pipturus
\xv cabo mwã nhyã we na khazia mõ-je vwo pe-tho-du mwã
\dialx GO
\xn une source a jailli près de sa maison et elle coule vers le bas
\xv e pe-kênõ
\dialx GO
\xn il tourne et il tourne
\dt 22/Feb/2025

\lx pe-
\hm 2
\dialx GOs PA
\sn 1
\is grammaire_réciproque
\ps PREF.REC
\ge mutuellement
\xv la pe-khõbwe
\xn ils discutent
\xv pe-ãbaa-li
\xn ils sont frères
\xv pe-dilo-la
\dialx PA GO
\xn ils sont égaux (Dubois)
\sn 2
\is grammaire_réciproque_collectif
\ps COLL
\ge ensemble
\dn devant un nombre: marque un tout
\xv la pe-thaivwi
\xn ils se réunissent
\xv pe-po-niza?
\dialx GOs
\xn combien y en a-t-il en tout?
\xv pe-po-nira?
\dialx PA GO
\xn combien y en a-t-il en tout? (Dubois)
\xv pe-po-nimadu
\dialx PA BO
\xn il y en a sept en tout (Dubois)
\se pevwe
\dialx GOS
\sge tous ensemble
\se pepe
\sge tous ensemble (Dubois)
\dt 08/Feb/2025

\lx pè
\dialx GOs BO PA
\is corps
\ps n
\ge cuisse ; fesse
\xv pèè-nu
\dialx GOs
\xn ma cuisse ; mes fesses
\xv pèè-n
\dialx PA
\xn sa cuisse ; ses fesses
\se po-pee
\sge petites cuisses
\et *paqa(l)
\el POc
\dt 22/Feb/2025

\lx pe-a
\dialx GOs
\is déplacement
\ps v
\ge aller (sans but, atélique)
\xv e pe-a hayu
\xn il va sans but
\dt 30/Dec/2021

\lx pe-a-bala
\dialx GO PA
\is grammaire_réciproque_collectif
\ps v ; n
\ge même équipe (être dans); marcher ensemble ; marcher côte à côte
\xv pe-a-bala-bî
\dialx GO
\xn nous sommes tous deux dans la même équipe
\xv mõ pe-a-bala ?
\dialx GO
\xn sommes-nous tous trois dans la même équipe ?
\dt 24/Feb/2025

\lx pe-a-da
\dialx GO
\is grammaire_réciproque_collectif
\ps COLL
\ge monter ensemble
\xv bî pe-a-da nõgò
\xn nous deux allons monter à la rivière ensemble
\dt 12/Nov/2021

\lx pe-a-hoze
\dialx GOs
\is déplacement
\ps v
\ge longer qqch. (activité)
\xv e pe-a-hoze kòli we-za
\dialx GOs
\xn il longe le bord de la mer
\xv e pe-a hoze kòli we
\dialx GOs
\xn il suit la berge de la rivière
\dt 29/Mar/2022

\lx pe-a-kai-
\dialx GOs PA
\is déplacement
\ps v
\ge suivre (se) ; suivre ; marcher en file indienne
\xv la pe-a-kai-la xo la êgu
\dialx GOs
\xn les gens marchent en file
\xv la a-kai lha-êba êgu xo lha-êba êmwê
\dialx GOs
\xn ces hommes là-bas suivent ces gens là-bas
\dt 22/Aug/2021

\lx peale
\dialx PA BO
\sn 1
\is grammaire_dispersif
\ps MODIF
\ge à chacun ; à part
\sn 2
\is grammaire_dispersif
\ps n ; v
\ge différent ; distinct
\xv ge-ã ni peale
\xn nous sommes dans (des endroits) différents
\xv peale vhaa i la
\xn leur langage est distinct
\dt 12/Oct/2021

\lx pe-alo
\dialx GOs BO
\is grammaire_aspect
\ps ASP
\ge en train de regarder
\xv nu hâ pe-alo
\dialx BO
\xn j'admire la belle vue
\dt 22/Mar/2023

\lx pe-a-vwe
\dialx GOs
\is grammaire_réciproque_collectif
\ps v
\ge ensemble (être)
\xv lha pe-a-vwe bulu
\xn ils sont ensemble en groupe
\xv lha pe-a bulu
\xn ils y vont ensemble
\dt 30/Dec/2021

\lx pe-bala
\dialx GOs
\is société
\is grammaire_réciproque_collectif
\ps n
\ge même équipe (être dans la)
\xv pe-bala-la
\xn ils sont dans la même équipe
\dt 27/Jan/2019

\lx pe-balan
\dialx BO
\is grammaire_comparaison
\ps v
\ge ressembler (se) ; pareil (être)
\xv la pe-balan
\xn ils se ressemblent
\dt 29/Jan/2019

\lx pe-bu
\dialx GOs WEM
\sn 1
\is guerre
\is grammaire_réciproque_collectif
\ps v
\ge bagarrer (se) ; battre (se)
\sn 2
\is action
\is grammaire_réciproque_collectif
\ps v
\ge cogner (se) ; entrer en collision
\xv lhi pe-bu-i-li
\dialx GOs
\xn ils sont entrés en collision
\dt 25/Mar/2022

\lx pe-bulu
\dialx GOs BO
\is interaction
\is grammaire_réciproque_collectif
\ps COLL
\ge ensemble
\xv mi pe-nee-bulu-ni
\dialx GOs
\xn nous deux le faisons ensemble
\dt 22/Mar/2023

\lx pecabi
\dialx BO
\is santé
\ps v.stat.
\ge endolori
\nt selon Corne et Haudricourt
\dt 26/Mar/2022

\lx pe-çabi
\ph peʒambi
\dialx GOs
\va pe-cabi
\dialx BO [Corne]
\is grammaire_réciproque_collectif
\is guerre
\ps v
\ge battre (se)
\dt 02/Jan/2022

\lx peçi
\dialx GOs
\va peyi
\dialx PA
\is mollusque
\ps n
\ge "savonnette" (coquillage)
\sc Codakia tigerina
\scf Lucinidés
\dt 27/Aug/2021

\lx pe-cimwi hi
\dialx GOs PA
\is action_corps
\is grammaire_réciproque
\ps v
\ge serrer (se) la main
\xv li pe-cimwi hi, li pe-cimwi hii-li
\dialx PA
\xn ils se serrent la main
\dt 24/Aug/2021

\lx pe-cinõõ trò mãni tree
\dialx GOs
\is temps_découpage
\ps n
\ge minuit
\dn (lit. le juste milieu entre la nuit et le jour)
\dt 08/Feb/2025

\lx pe-cö-ò cö-mi
\ph peco.ɔ comi
\dialx GOs BO
\va pe-cool
\dialx PA
\is mouvement
\ps v
\ge sauter de-ci de-là
\dt 02/Jan/2022

\lx peçööli
\ph peʒω:li, pedʒω:li
\dialx GOs
\va pejooli
\ph peɲɟo:li
\dialx PA
\is interaction
\ps v
\ge engueuler ; tancer
\se a-peçööli
\sge râleur
\xv la pe-pejooli
\dialx PA
\xn ils se disputent
\dt 19/Aug/2021

\lx pe-chôã
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps v
\ge jouer (se) des tours ; taquiner (se)
\ge chercher querelle (se)
\dt 14/Aug/2021

\lx pe-dodobe
\dialx GOs BO
\is interaction
\ps v
\ge feindre ; faire semblant
\xv nu pe-dodobe thiidin
\dialx BO
\xn je feins d'être en colère
\nt selon Corne
\dt 22/Mar/2023

\lx pe-dõńi
\ph pedɔ̃ni
\dialx GOs BO
\is grammaire_préposition_locative
\ps PREP
\ge parmi ; entre
\xv pe-dõńi-la
\dialx GO
\xn entre eux
\xv ge je ni pe-dõńi X ma Y
\dialx GO
\xn il est entre X et Y
\xv i phe aa-xe na ni dõni la-ã ko
\dialx BO
\xn il a pris un des poulets
\dt 02/Jan/2022

\lx pedõńõ
\ph pedɔ̃nɔ̃
\dialx GOs
\va penõnõ, penõni
\ph penɔ̃nɔ̃, penɔ̃ni
\is grammaire_locatif
\ps LOC
\ge milieu (au) ; parmi
\se penõnõ mwa
\sge au milieu de la maison
\se penõni cee
\sge au milieu des arbres
\dt 16/Apr/2024

\lx pee-
\dialx BO
\is anguille
\is préfixe_sémantique
\ps n
\ge préfixe des anguilles
\xv pee-nõ
\dialx BO
\xn anguille (dont les yeux ressemblent à ceux d'un poisson)
\cf peeńã
\ce anguille
\dt 24/Jan/2022

\lx pèèbu
\dialx GOs WEM PA BO
\is parenté
\ps n
\ge petit-fils ; petite-fille ; descendant
\xv pèèbu-nu
\dialx GO
\xn mon petit-fils ; ma petite-fille
\xv pèèbuu-m
\dialx WEM
\xn ton petit-fils ; ta petite-fille
\xv peebu-n
\dialx PA
\xn son petit-fils ; sa petite-fille
\dt 20/Feb/2025

\lx peenã
\dialx GOs PA BO
\is anguille
\ps n
\ge anguille (de rivière)
\dt 21/Mar/2023

\lx peenã nõgò
\ph pe:ɳɛ̃ ɳɔ̃ŋgɔ
\dialx GOs
\is anguille
\ps n
\ge anguille de creek
\dt 21/Mar/2023

\lx peeni
\dialx GOs
\is action_corps
\is préparation_aliments
\ps v
\ge pétrir (pain)
\xv e peeni draa-phwalawa
\xn elle pétrit la farine
\dt 20/Aug/2021

\lx peenu
\dialx PA BO
\is cultures_champ
\ps n
\ge tarodière
\ge butte de terre au bord du canal de la tarodière
\dn à terre bien remuée (selon Charles Pebu-Polae)
\cf bwala ; aru
\dt 25/Mar/2022

\lx peera
\dialx BO
\is plantes_partie
\ps v
\ge bouton (en) ; non éclos
\nt selon Corne, non vérifié
\dt 25/Mar/2022

\lx pegaabe
\dialx GOs
\is interaction
\ps v.stat.
\ge humble (se faire) ; petit (se faire) ; abaisser (s')
\dn formule de respect dans les discours coutumiers
\xv nu pegaabe i nu bwa xai-wa (< kai-wa)
\xn je me fais petit derrière vous
\dt 20/Dec/2021

\lx pe-gaixe
\ph pe'ŋgaiɣe
\dialx GOs PA BO
\is grammaire_comparaison
\ps ADV
\ge parallèle ; dans le même alignement ; de même niveau ; ex æquo
\ge même alignement ; parallèle
\ge ajuster 
\dn pour mettre sur la même ligne
\xv lhi pe-gaixe
\xn ils sont sur la même ligne (au même niveau)
\xv lhi hovwa pe-gaixe
\dialx GOs
\xn ils sont arrivés ex-æquo
\xv cö ne vwo lhi pe-gaixe
\dialx GOs
\xn ajuste les pour qu'ils soient sur la même ligne
\xv lhi uça gaixe
\dialx GOs
\xn ils arrivent en même temps
\dt 20/Feb/2025

\lx pègalò
\dialx GOs
\is plantes
\ps n
\ge pomme cythère
\dt 29/Jan/2019

\lx pègòò
\dialx GOs WE
\va pègòm
\dialx BO
\is étapes_vie
\ps v.stat.
\ge dans la force de l'âge (être) ; bonne santé (être en)
\dt 27/Jan/2019

\lx pe-gu-xe
\dialx PA
\is déplacement
\is grammaire_réciproque_collectif
\ps v
\ge marcher en file indienne
\xv la pe-gu-xe
\dialx PA
\xn ils marchent en file indienne
\dt 30/Jan/2019

\lx pe-hatra
\dialx GOs
\va pe-hara
\dialx GO(s) PA WEM
\va haram
\dialx BO
\is action_corps
\ps v
\ge jongler
\dn par ex. avec des oranges sauvages, occupation pratiquée dans le pays des morts
\ng v.t. |lx{pe-harame}
\dt 05/Jan/2022

\lx pe-haze
\dialx GOs
\va pe-aze
\dialx GO(s)
\va pe-hale, pe-ale
\dialx BO [BM]
\va ve-ale
\dialx PA BO
\is grammaire_dispersif
\ps dispersif ; DISTR
\ge séparément ; chacun de son côté
\ge différent l'un de l'autre
\xv lhi pe-haze
\xn ils sont différents l'un de l'autre
\xv lhi a pe-haze ãbaa-nu ma nata
\dialx GO
\xn mon frère et le pasteur sont partis chacun de leur côté
\xv lhi u a-da mwa a-ve-hale na li-nã phwamwa
\dialx PA
\xn ils s'en retournent dans leur terroir respectif
\xv thiu ve-ale
\dialx BO
\xn dispersez-vous !
\xv mènõ ve-ale
\dialx BO
\xn embranchement
\xv ra pe-hale va i la
\dialx BO
\xn leurs paroles sont dispersées
\xv a-ve-hale
\dialx BO
\xn se disperser
\ng v.t. |lx{pe-haze-ni}
\dt 05/Jan/2023

\lx pe-hê-xòlò
\dialx GOs PA
\is parenté
\ps n
\ge même famille (de la)
\xv pe-hê-xòlò-lò
\dialx GO
\xn ils sont (tous trois) de la même famille
\cf hê-kòlò, hê xòlò
\ce de la même famille (du même côté)
\dt 18/Aug/2021

\lx pe-hiliçôô
\ph pehiliʒô:  pehilidʒô:
\dialx GOs
\is mouvement
\ps v
\ge balancer (se)
\xv è pe-hiliçôô
\xn il se balance
\dt 02/Jan/2022

\lx pe-hine
\dialx GOs
\is fonctions_intellectuelles
\is grammaire_réciproque
\ps v.REC
\ge connaître (se)
\xv pe-hine-ã
\dialx BO
\xn nous nous connaissons mutuellement
\dt 27/Jan/2019

\lx pe-hò
\dialx GOs PA WEM
\is jeu
\ps v
\ge jouer à chat perché ; jouer au loup
\dt 22/Mar/2023

\lx pe-hoze
\dialx GOs
\va pe-(h)òre
\dialx BO
\is déplacement
\ps v
\ge suivre ; suivre (se)
\xv nu pe-(h)òre nyè jaòl
\dialx BO
\xn je suis ce fleuve Diahot
\dt 22/Mar/2023

\lx pe-îdò
\dialx GOs
\is grammaire_comparaison
\ps n
\ge même lignée
\xv pe-îdò-bî
\dialx GOs
\xn nous deux sommes de la même lignée
\dt 12/Nov/2021

\lx pe-iô
\dialx GOs
\is grammaire_pronom
\ps COLL.PRO
\ge c'est à nous trois ensemble
\dt 10/Feb/2019

\lx pe-jaxa
\dialx GOs
\is grammaire_comparaison
\ps n
\ge même taille ; même mesure
\xv pe-jaxa-li
\dialx GOs
\xn ils sont de même taille
\dt 29/Jan/2019

\lx pe-jivwa
\ph peɲɟiwa
\dialx GOs
\is grammaire_comparaison
\ps NUM.COMPAR
\ge nombre égal (être en)
\xv pe-jivwaa-li/-lò/-la
\dialx GO
\xn ils (duel, triel, pluriel) sont en nombre égal
\dt 24/Feb/2025

\lx pe-jölö
\dialx GOs
\is fonctions_naturelles
\ps v
\ge accoupler (s') ; faire l'amour
\dt 25/Jan/2019

\lx pe-ka-
\dialx GOs
\is grammaire_distributif
\ps QNT.DISTR
\ge plusieurs (à)
\xv la pe-ka-a-tru ni nõ loto
\dialx GO
\xn ils étaient à deux dans chaque voiture
\xv la pe-a-ni ni nõ loto
\dialx GO
\xn ils étaient à cinq dans la (même) voiture
\dt 24/Feb/2025

\lx pe-ka-a-niza ?
\dialx GOs
\va pe-aniza ?
\dialx GO(s)
\is grammaire_interrogatif
\is grammaire_distributif
\ps DISTR
\ge combien (d'êtres humains) dans chaque x ?
\xv pe-ka-a-niza na ni ba ?
\dialx GOs
\xn combien de personnes y a-t-il dans chaque équipe ?
\xv mõ pe-ka-a-niza na ni ba ?
\dialx GOs
\xn nous sommes combien de personnes dans chaque équipe ?
\xv pe-ka-a-niza iwe ni nõ loto ?
\dialx GOs
\xn à combien (étiez)-vous dans chaque voiture ?
\xv pe-a-niza iwe ni nõ loto ?
\dialx GOs
\xn à combien (étiez)-vous dans la (même) voiture ?
\xv mõ chue pe-ka-a-niza ?
\dialx GOs
\xn on joue à combien ?
\dt 24/Feb/2025

\lx pe-kae
\dialx GOs
\va pe-kaeny
\dialx PA
\is interaction
\is grammaire_réciproque
\ps v
\ge disputer (se) (jeu, compétition) ; garder pour soi
\xv li pe-kae nhye loto
\dialx GO
\xn ils se disputent cette voiture
\xv li pe-kae je thoomwã
\dialx GO
\xn ils se disputent cette femme
\xv i pe-kae balô
\dialx PA
\xn il garde le ballon pour lui (et ne le passe pas)
\xv li pe-kaeny u je loto
\dialx PA
\xn ils se disputent pour/à cause de cette voiture
\xv kêbwa pe-kaeny !
\dialx PA
\xn il ne faut pas être égoïste (garder pour soi)
\dt 22/Feb/2023

\lx pe-ka-po-niza ?
\dialx GOs
\is grammaire_interrogatif
\is grammaire_distributif
\ps DISTR
\ge à combien (d'objets) dans chaque x. ? (distributif)
\xv mo pe-ka po-niza ?
\xn combien sommes-nous chacun ? (chaque groupe ou équipe dans un jeu)
\dt 14/Aug/2021

\lx pe-ka-po-xè
\dialx GOs
\is grammaire_distributif
\ps DISTR
\ge un par un (mettre)
\se pe-ka-po-tru, pe-ka-po-ko
\sge deux par deux, trois par trois
\xv mo pe-ka-po-xè ba-tiiwo
\xn nous avons reçu chacun un stylo
\xv lhi thu-mhenõ pe-ka-po-tru
\xn ils marchent deux par deux
\xv mo thu-mhenõ pe-ka-po-kò
\xn nous marchent trois par trois
\dt 22/Feb/2023

\lx pe-ki
\dialx GOs
\is action
\is grammaire_réciproque
\ps v
\ge raccorder
\ng v.t. |lx{pe-kine}
\dt 05/Jan/2022

\lx pe-kîga
\ph pekiŋga
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge rire (se)
\xv çö pe-kîga hãda
\dialx GOs
\xn tu ris tout seul
\xv hoã je, i pe-kîga wãã-na
\dialx BO
\xn quant à lui, il se rit ainsi
\dt 21/Mar/2023

\lx pe-kine
\ph pekine
\dialx GOs
\is action
\is grammaire_réciproque_collectif
\ps v.t.
\ge mettre bout à bout (et allonger)
\xv e pe-kine hôxa-cee
\dialx GOs
\xn il a mis les planches bout à bout
\dt 02/Jan/2022

\lx pe-kô-alö
\dialx GOs
\is position
\is grammaire_réciproque_collectif
\ps v
\ge couché (être) face à face
\xv l(h)ò pe-kô-alö-i-lò
\xn ils (triel) sont couchés face à face
\dt 24/Feb/2025

\lx pe-koone
\dialx BO
\is navigation
\ps v
\ge louvoyer ; tirer des bords vent debout
\nt non vérifié
\cf koone
\dt 26/Jan/2019

\lx pe-kû
\dialx BO
\is grammaire_aspect
\ps v
\ge en train de manger
\xv nu ga pe-kû pò-puleng
\xn je suis en train de manger ce fruit
\dt 29/Jan/2019

\lx pe-ku-alö
\dialx GOs
\is position
\is grammaire_réciproque
\ps v
\ge debout (être) face à face
\xv lò pe-ku-alö-i-lò
\dialx GOs
\xn ils (triel) sont debout face à face
\xv lò pe-ku-alö
\dialx GOs
\xn ils (triel) sont debout face à qqch. d'autre
\dt 24/Feb/2025

\lx pe-kûbu
\ph pekûbu
\dialx GOs PA
\is guerre
\is grammaire_réciproque
\ps v
\ge battre (se) (avec armes)
\xv lhi pe-kûbu-li
\dialx GO
\xn ils se battent
\dt 19/Aug/2023

\lx pe-ku-çaaxo
\ph pekuʒa:ɣo
\dialx GOs
\is jeu
\ps v
\ge jouer à cache-cache
\dt 02/Jan/2022

\lx pe-kueli
\dialx GOs
\is sentiments
\is grammaire_réciproque
\ps v
\ge détester (se) ; rejeter (se)
\xv lhi pe-kueli-li ui nye thoomwã xo liè mèèvwu xa êmwê
\dialx GOs
\xn les deux frères se rejettent à cause de cette femme
\xv lha pe-kueli-la pexa dili/pune dili xo la êgu
\dialx GOs
\xn les gens se rejettent à cause de la terre
\xv li-ã mèèvwu ça lhi pe-kueli-li
\dialx GOs
\xn quant aux deux frères, ils se rejettent
\xv li-è mèèvwu xa êmwê, lhi pe-kueli-li pexa nye thoomwã
\dialx GOs
\xn et les deux frères, ils se rejettent à cause de cette femme
\xv lhi pe-kueli li-è mèèvwu xa êmwê ui nye thoomwã
\dialx GOs
\xn les deux frères se rejettent à cause de cette femme
\xv lhi pe-kueli-li li-è mèèvwu xa êmwê
\dialx GOs
\xn les deux frères se rejettent
\dt 04/Feb/2023

\lx pe-khabeçö
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge plonger (pour s'amuser, se dit d'enfants)
\dt 06/Oct/2018

\lx pe-khînu
\dialx PA
\is sentiments
\ps v
\ge triste ; malheureux
\xv pe-khînu ai-ny
\dialx PA
\xn je suis triste/malheureux
\dt 22/Feb/2025

\lx pe-khõbwe
\dialx GOs
\is discours
\is grammaire_réciproque_collectif
\ps v
\ge discuter ; mettre d'accord (se)
\xv bî pe-khõbwe po bî pe-tròò-bî mõnõ
\xn nous deux avons convenu de nous retrouver demain
\dt 24/Jun/2024

\lx pe-meńixè
\dialx GOs PA BO
\is grammaire_comparaison
\ps v.COMPAR
\ge pareil (être) ; semblable
\xv za xa khõbwe xo ã kuau pe-meńixè
\dialx GO
\xn et à nouveau ce chien dit la même chose
\xv mõ hu hovwo xa pe-meńixè
\dialx GO
\xn nous mangeons la même nourriture
\cf pe-poxè
\ce pareil
\dt 22/Mar/2023

\lx pe-mhe
\dialx GOs BO
\is déplacement
\is grammaire_réciproque_collectif
\ps v
\ge aller ensemble ; accompagner
\xv pe-mhe-ã
\dialx GO
\xn nous faisons route ensemble
\xv pe-mhe-la
\dialx BO
\xn ils font route ensemble
\xv ai-nu vwö pe-mhe-î
\dialx GO
\xn je veux t'accompagner (ou) je veux que nous deux y allions ensemble
\xv ai-nu vwö pe-mhe-õ
\dialx GO
\xn je veux vous accompagner vous deux (ou) je veux que nous (triel) y allions ensemble
\xv ai-ny (w)u ra pe-mhe-ã
\dialx BO
\xn je veux vraiment vous accompagner (BM)
\xv ciibwin je ai-n (p)u pe-mhe-la
\dialx BO
\xn le rat veut les accompagner (BM)
\xv a-mi ho pe-mhe-î
\dialx BO
\xn viens avec moi (BM)
\xv nu a kaè-n ho pe-mhe-yò
\dialx BO
\xn va avec lui (BM)
\se pe-mhe a
\dialx BO
\sge accompagner
\cf mhe, mhenõ
\ce déplacement
\ng |lx{mhe} est la forme courte de |lx{mhenõ}
\dt 24/Feb/2025

\lx pe-na
\dialx GOs
\is grammaire_réciproque_collectif
\ps v
\ge envoyer (s') mutuellement
\xv li vara pe-na a-pe-vhaa i li
\xn ils s'envoient mutuellement un messager
\dt 14/Aug/2021

\lx pe-na bwa
\dialx GOs
\is action_corps
\is grammaire_réciproque_collectif
\ps v
\ge empiler
\dt 26/Jan/2019

\lx pe-na-bulu-ni
\ph peɳabu'luɳi
\dialx GOs
\is mouvement
\is grammaire_réciproque_collectif
\ps v.t.
\ge rassembler ; assembler
\dt 02/Jan/2022

\lx pe-na-bwa na kui
\dialx GOs
\is coutumes
\ps n
\ge empilement d'ignames
\dt 28/Jan/2019

\lx pe-na-hi-n
\dialx PA
\is caractéristiques_personnes
\ps v
\ge planer
\dn dans son emploi métaphorique, il réfère à quelqu'un de paresseux
\xv novwu ã i baaro, e pe-na-hi-n bwa dèèn
\xn quant à celui qui est paresseux, il plane dans le vent
\dt 22/Oct/2021

\lx pe-na-vwo
\dialx GOs PA
\is coutumes
\is grammaire_réciproque_collectif
\ps v
\ge faire une cérémonie coutumière ; faire les échanges coutumiers
\dt 10/Feb/2019

\lx pe-nee-zo-ni
\ph peɳe:ðoɳi
\dialx GOs
\is interaction
\is grammaire_réciproque_collectif
\ps v
\ge pardonner (se) ; se réconcilier
\dn (lit. faire bien)
\xv mi pe-nee-zo-ni
\xn nous deux nous sommes pardonnés
\xv lha pe-nee-zo-ni vha-raa xo la êgu
\xn les gens se sont pardonnés les insultes
\xv mõ pe-nee-zoo-ni
\xn nous nous pardonnons entre nous
\cf pe-tha thria
\dialx GOs
\ce faire une coutume de pardon
\dt 08/Feb/2025

\lx pèńô
\ph pɛnõ
\dialx GOs
\va pènô
\dialx PA BO
\is échanges
\ps v
\ge voler ; dérober
\xv e nõ-pèńô
\dialx GOs
\xn il regarde furtivement
\xv e pènôe liè-ò trip i li
\xn il/elle vole leur estomac à toutes les deux
\xv a-pènô
\dialx BO
\xn voleur
\ng v.t. |lx{pèńôe, pènôe}
\et *penako, *panako
\el POc
\ea Blust
\dt 22/Mar/2023

\lx pe-no-du
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge regarder en bas (sans but)
\xv e kavwö pe-no-du mwã bwabu
\dialx GOs
\xn elle ne regarde pas par terre
\dt 30/Dec/2021

\lx penuu
\dialx GOs
\is société_organisation
\ps n
\ge liens de famille
\xv mõ penuu
\dialx GOs
\xn on a des liens de famille
\dt 06/Feb/2019

\lx pe-nhõî thria
\dialx GOs
\is grammaire_réciproque_collectif
\is interaction
\ps v
\ge pardonner (se)
\dn (lit. attacher le pardon)
\xv mõ pe-nhõî thria
\xn nous trois avons fait la coutume de pardon
\dt 08/Feb/2025

\lx pe-paree
\dialx PA BO [BM]
\is fonctions_intellectuelles
\ps v.i
\ge distrait (être) ; ne pas faire attention ; être négligent
\dt 18/Aug/2021

\lx pe-pîîna
\dialx GOs BO
\is grammaire_aspect
\ps ASP
\ge en train de se promener
\xv nu pe-pîîna, pe-hòre nyè jaaòl
\xn je me promène, je suis le fleuve
\dt 22/Mar/2023

\lx pe-po
\dialx BO
\is grammaire_quantificateur_mesure
\ps QNT
\ge toutes les choses
\xv la pepe-po
\xn toutes les choses (Dubois ms)
\nt non vérifié
\dt 16/Feb/2025

\lx pe-pò-niza ?
\dialx GOs
\is grammaire_interrogatif
\ps INT
\ge combien en tout (avons-nous en commun)?
\se pe-pò-ni-ma-dru
\dialx GOs
\sge sept en tout
\se pe-pò-ru
\dialx PA
\sge deux en tout
\se pe-pò-kon
\dialx PA
\sge trois en tout
\dt 09/Jan/2022

\lx pe-po-xè
\dialx GOs
\is grammaire_comparaison
\ps v
\ge semblable ; pareil ; autant
\xv pe-po-xee-li
\xn ils (duel) sont semblables
\xv pe-po-xee-la
\xn ils sont semblables
\xv lha pe-po-xee
\xn ils sont semblables ; ils se ressemblent
\xv pe-po-xee pwaxi-li
\xn leurs enfants sont de même taille (hauteur)
\xv pe-po-xee chinõ-li
\xn ils sont de même taille (largeur)
\xv pe-po-xee ala-mee-li
\xn leurs visages se ressemblent
\cf pe-po-xee-lò
\ce ils (triel) sont semblables
\cf pe-menixe
\ce pareil
\dt 24/Feb/2025

\lx pe-pwaixe
\dialx GOs
\is grammaire_quantificateur_mesure
\is grammaire_réciproque_collectif
\ps QNT
\ge toutes les choses possédées ensemble
\xv pe-pwaixe-î
\xn toutes les choses que nous deux possédons ensemble/qui nous sont communes
\dt 22/Feb/2025

\lx pe-pwali
\dialx GOs
\va pe-pwawali
\dialx PA
\is grammaire_comparaison
\ps COMPAR
\ge même hauteur
\xv pe-pwali-li
\dialx GOs
\xn ils ont la même taille (hauteur)
\cf pwali, pwawali
\ce long ; haut
\dt 22/Feb/2025

\lx pe-pwawè
\dialx GOs
\va pe-pwawèn
\dialx PA
\is interaction
\ps v
\ge imiter
\xv e pe-pwawè kêê-je
\xn il imite son père
\dt 27/Jan/2019

\lx pe-pwò-tru mèèvwu
\dialx GOs
\is grammaire_numéral
\ps NUM
\ge il y a deux variétés
\dt 18/Aug/2021

\lx pe-phããde kui
\dialx GOs
\is coutumes
\ps n
\ge cérémonie de la nouvelle igname
\dn (lit. se présenter les ignames)
\dt 22/Oct/2021

\lx pe-pha-nõnõmi
\ph pepʰaɳɔ̃ɳɔ̃mi
\dialx GOs
\is grammaire_réciproque
\is interaction
\ps v
\ge rappeller (se) mutuellement
\xv lhi pe-pha-nõnõmi xo Kaavwo mã Hiixe la mwêêje ẽgõgò
\dialx GOs
\xn Kaavwo et Hiixe se rappellent mutuellement les coutumes d'antan
\xv lhi pe-pha-nõnõmi cai li la mwêêje ẽgõgò
\dialx GOs
\xn elles se rappellent mutuellement les coutumes d'antan
\xv lhi pha-nõnõmi la mwêêje ẽgõgò xo Kaavwo mã Hiixe
\dialx GOs
\xn Kaavwo et Hiixe rappellent (à d'autres) les coutumes d'antan
\dt 26/Jan/2022

\lx pe-phao
\dialx PA
\is déplacement
\ps v
\ge prendre la route
\xv pe-phao-bin ni dèn
\dialx PA
\xn nous prenons la route
\dt 14/Mar/2019

\lx pe-phaza-hããxa
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps v
\ge faire (se) peur mutuellement
\xv hã pe-phaza-hããxa
\dialx GOs
\xn nous jouons à nous faire peur mutuellement
\dt 26/Jan/2022

\lx pe-phöö
\dialx GOs
\is position
\ps v
\ge couché sur le ventre
\dn étape de l'évolution d'un bébé
\dt 16/Feb/2025

\lx pe-phumõ
\ph pe'pʰumɔ̃
\dialx GOs WEM BO PA
\is discours
\ps v ; n
\ge discourir ; prêcher ; enseigner ; discours
\dt 02/Jan/2022

\lx pe-phweexu
\dialx GOs PA BO
\is discours
\is grammaire_réciproque_collectif
\ps v
\ge discuter
\cf phweexu
\ce discuter
\dt 28/Jan/2019

\lx pe-ravwi
\dialx GOs
\is mouvement
\ps v
\ge faire la course (de vitesse) ; poursuivre (se)
\cf thravwi
\ce poursuivre (à la chasse); chasser; écarter (chiens, volailles)
\dt 25/Jan/2019

\lx pe-ravhi
\dialx BO PA
\is soin
\ps v
\ge raser (barbe)
\xv i pe-ravhi
\xn il se rase
\cf pe-thra
\dialx GOs
\ce raser (se)
\dt 22/Mar/2023

\lx pe-rulai
\dialx PA BO [Corne]
\is interaction
\ps v
\ge moquer (se)
\se a-pe-rulai
\sge un moqueur
\cf uza
\dialx GOs
\ce moquer (se)
\cf ula
\dialx BO
\ce moquer (se)
\dt 10/Jan/2022

\lx pe-tizi mabu
\dialx GOs
\is jeu
\ps LOCUT
\ge jeu consistant à faire rire l'autre
\dn (le premier qui rit a perdu)
\dt 09/Feb/2025

\lx pe-tò do
\dialx PA BO
\is interaction
\is grammaire_réciproque
\ps v
\ge pardonner (se) mutuellement
\cf tòè do
\ce lancer la sagaie
\dt 26/Jan/2022

\lx pe-tòe
\dialx BO
\is grammaire_aspect
\ps ASP.INACC
\ge en train de planter
\xv nu ga pe-tòe êê-ny (ã) èm
\dialx BO
\xn je suis en train de planter mes plants de canne à sucre
\dt 05/Jan/2022

\lx pe-tou
\dialx GOs BO
\is coutumes
\ps v
\ge partager (partage coutumier des ignames)
\dt 28/Jan/2019

\lx pe-tha
\dialx GOs WEM
\is jeu
\ps n
\ge jeu de lancer de sagaie (lancer-ricochet)
\dt 28/Jan/2019

\lx pe-tha thria
\dialx GOs
\va pe-tha thia
\dialx PA
\is coutumes
\is interaction
\is grammaire_réciproque_collectif
\ps v
\ge pardonner (se) (pardon coutumier)
\xv lhi pe-tha-thria li-e ãbaa-nu êmwê
\dialx GO
\xn mes deux frères se sont pardonnés
\cf pe-ne-zo-ni
\ce pardonner
\dt 30/Dec/2021

\lx pe-thahî
\dialx GOs
\va pe-tha-hînõ
\dialx GO(s)
\va pe-thahîn
\dialx PA BO
\is jeu
\ps v ; n
\ge jouer aux devinettes ; deviner ; concours de devinettes
\xv la pe-thahîn
\dialx PA
\xn ils jouent aux devinettes ; ils se posent des devinettes
\cf thahîn
\dialx PA BO
\ce devinettes
\dt 22/Feb/2025

\lx pe-thaivwi
\ph petʰaiβi
\dialx GOs PA
\is interaction
\is grammaire_réciproque_collectif
\ps v
\ge réunir (se)
\dt 02/Jan/2022

\lx pe-thauvweni
\ph petʰauβeni
\dialx GOs
\is fonctions_intellectuelles
\is interaction
\ps v
\ge tromper (se) en prenant qqch.
\ge prendre qqch. par erreur
\dt 29/Mar/2022

\lx pe-thi
\dialx GOs
\is feu
\ps v
\ge incendier ; mettre le feu
\xv e pe-thi
\xn il a mis le feu
\se êgu xa a-pe-thi
\sge un pyromane
\dt 23/Aug/2021

\lx pe-thi thô
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps v
\ge provoquer (se)
\xv lha pe-thi thô nai la
\dialx GOs
\xn ils se provoquent (piquent la colère)
\dt 26/Jan/2022

\lx pe-thilò
\dialx GOs
\va pe-dilo
\dialx BO
\sn 1
\is grammaire_réciproque
\ps v
\ge paire ; faire équipe ; être en binôme avec
\xv pe-thilò-bî mã Mario
\dialx GO
\xn je fais équipe avec M
\sn 2
\is interaction
\ps v
\ge pareil (être) ; égal
\se pe-dilo-li
\dialx BO PA
\sge ils sont en binôme
\se pe-dilo-la
\dialx BO PA
\sge ils sont pareils
\dt 26/Mar/2022

\lx pe-thirãgo
\dialx GOs
\is position
\is grammaire_réciproque_collectif
\ps v
\ge superposer (se)
\xv nee vwö lha pe-thirãgo
\xn fais en sorte qu'ils se superposent
\dt 30/Dec/2021

\lx pe-thu-ba
\dialx GOs
\is société
\is grammaire_réciproque_collectif
\ps v
\ge faire équipe
\xv mõ pe-thu-ba
\xn nous trois faisons équipe
\cf bala-nu
\ce mon équipier
\dt 26/Aug/2021

\lx pe-thu-menõ
\dialx GOs
\va pe-tumenõ
\dialx GO(s)
\is déplacement
\ps v
\ge déplacer (se) ; promener (se)
\xv e pe-thu-menõ
\xn il se promène
\xv nu pe-thu-menõ-du kolo-je
\xn je vais vers chez lui (sans but)
\xv bî pe-thu-menõ mãni ãbaa-nu xa thõõmwa
\dialx GOs
\xn j'ai fait le chemin avec ma sœur
\xv lhi pe-thu-menõ (bulu) xo Kaavo mã Hiixe
\dialx GOs
\xn Kaavo et Hiixe ont fait le chemin ensemble
\cf e pe-piina
\ce il se promène
\dt 30/Dec/2021

\lx pe-thu-paa
\dialx GOs
\is guerre
\is grammaire_réciproque
\ps v
\ge faire la guerre (se)
\xv li pe-thu-paa (xo) lie phwe-meevwu
\xn les deux clans se font la guerre
\dt 26/Jan/2022

\lx pe-tre-alö
\dialx GOs
\is position
\is grammaire_réciproque
\ps v
\ge assis (être) face à face
\xv hî pe-tre-alö-i-î
\xn nous deux sommes assis face à face
\xv me pe-tre-alö-i-me
\xn nous trois sommes assis face à face
\dt 26/Jan/2022

\lx pe-trò
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps v
\ge rencontrer (se)
\xv bî pe-trò-bî mã Jan dròrò
\dialx GOs
\xn Jean et moi nous sommes rencontrés hier
\xv kô-zo na lha pe-tòò-la mõnòn mãni bòna
\dialx PA
\xn ils pourront se retrouver demain et après-demain
\dt 26/Jan/2022

\lx pe-tròòli
\dialx GOs PA
\va pe-ròòli
\dialx GO(s)
\is grammaire_réciproque
\ps v ; n
\ge rencontrer (se)
\ge réunion ; réunir (se)
\xv li pe-ròòli bwa dè
\dialx GOs
\xn ils se sont rencontrés sur la route
\dt 26/Jan/2022

\lx pe-tròòli èò
\dialx GOs
\va pe-ròòli iò
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps LOCUT
\ge à tout à l'heure ; à tout de suite ; à bientôt
\dn (lit. on se rencontre tout à l'heure)
\se pe-ròòli !
\sge à bientôt
\dt 26/Jan/2022

\lx pe-tròòli mõnõ
\dialx GOs
\va pe-ròòli mõnõ
\dialx GO(s)
\va pe-tòòli menon
\dialx PA
\sn 1
\is grammaire_réciproque
\ps v
\ge dire à demain ; au revoir (se)
\sn 2
\is interaction
\ps LOCUT
\ge à demain !
\dt 22/Feb/2025

\lx pe-tròòli ni tree
\dialx GOs
\va pe-ròòli ni tree
\dialx GOs
\is interaction
\ps LOCUT
\ge à un de ces jours !
\xv pe-tròòli ni xa tree !
\xn à un de ces jours !
\dt 25/Aug/2021

\lx pe-thra
\ph peʈʰa
\dialx GOs
\va pe-thaa
\dialx BO PA
\is soin
\ps v
\ge raser (se)
\xv e pe-thra
\dialx GOs
\xn il se rase (en train de, activité)
\xv ge je pe-thra
\dialx GOs
\xn il est en train de se raser
\xv i pe-tha Pwayili
\dialx PA
\xn P. se rase (activité)
\xv i còòxe pu-n wu ra u thaa
\dialx PA
\xn il se coupe les poils à ras
\se ba-pe-thra
\sge rasoir
\dt 02/Jan/2022

\lx peu
\hm 1
\ph pe.u
\dialx GOs
\va peul
\dialx PA BO
\is grammaire_modalité
\ps MODAL
\ge en vain ; sans résultat ; vide ; tant pis ! ; ce n'est pas grave
\xv nu mõgu ni peu
\dialx GO
\xn je travaille pour rien/sans résultat
\xv mo vhaa ni peu
\xn nous parlons pour rien/dans le vide
\xv peu, kêbwa çö jara trõne ji=li
\xn inutile, n'écoute surtout pas cela
\dt 22/Feb/2025

\lx peu
\hm 2
\dialx BO
\is action_tête
\ps v
\ge claquer des lèvres en signe de mépris
\nt selon Corne
\dt 26/Mar/2022

\lx peve-n
\dialx PA
\is instrument
\ps n ; v
\ge usage ; emploi ; servir à
\xv peve-n da jene ?
\xn ça sert à quoi ?
\xv kixa pune peve-n
\xn ça ne sert à rien
\xv la phe duu-ny vwo peve hêgi
\xn ils prennent mes os pour servir de monnaie
\dt 26/Aug/2023

\lx pevera
\dialx WEM WE
\is caractéristiques_personnes
\ps v
\ge débrouillard
\dt 26/Jan/2019

\lx pe-vii
\dialx GOs
\is interaction
\is grammaire_réciproque
\ps v
\ge éviter (s')
\xv li pe-vii
\dialx GOs
\xn ils s'évitent
\cf pii
\ce éviter ; esquiver
\dt 22/Feb/2025

\lx pevwe
\ph peβe
\dialx GOs
\va pevwe
\dialx PA BO
\va pepe
\dialx arch.
\is grammaire_quantificateur_mesure
\ps QNT
\ge tous ; chacun
\xv la pevwe a bulu
\dialx GO
\xn ils partent tous (en même temps)
\xv lò pevwe pe-be-yaza
\dialx GO
\xn ils(triel) ont tous le même nom
\xv pevwe ge la êńa
\dialx GO
\xn ils sont tous ici
\xv hã ra pevwe êgu
\dialx BO
\xn nous sommes tous des hommes
\xv la pepe-po
\dialx BO
\xn toutes les choses (Dubois)
\cf cavwe
\dialx GOs
\ce tous ensemble
\dt 16/Feb/2025

\lx pevwe jiu-n
\dialx PA BO
\is grammaire_quantificateur_mesure
\ps QNT
\ge tout ; la totalité
\dt 16/Feb/2025

\lx pe-vwii
\ph peβi:
\dialx GOs
\va pe-viing
\dialx PA WEM
\va phiing
\dialx PA BO
\is déplacement
\is grammaire_réciproque_collectif
\ps v
\ge rejoindre (se)
\ge joindre (se)
\xv la pe-vwii
\dialx GOs
\xn ils se rejoignent
\se pe-phiing, pe-viing
\dialx BO
\sge se rejoindre
\dt 02/Jan/2022

\lx pevwö
\ph peβω
\dialx GOs
\sn 1
\is fonctions_intellectuelles
\ps v
\ge attention (faire) à
\xv kavwö çö pevwö nai je
\dialx GOs
\xn ne fais pas attention à lui
\sn 2
\is soin
\ps v
\ge s'occuper de (enfant, qqn.)
\dt 17/Feb/2025

\lx pevheano
\ph peβeaɳo
\dialx GOs PA
\is insecte
\ps n
\ge grillon
\dt 22/Mar/2023

\lx pe-wãme
\dialx GOs WEM
\is grammaire_comparaison
\ps v
\ge ressembler (se) ; semblable (être)
\xv pe-w(h)ãme-li
\xn elles se ressemblent
\dt 28/Feb/2023

\lx pe-wèle
\dialx BO PA
\is interaction
\is grammaire_réciproque
\ps v
\ge disputer (se) ; battre (se)
\dn avec ou sans armes
\dt 08/Feb/2025

\lx pewi
\dialx GOs PA
\is action_corps_animaux
\ps v
\ge mordre (en parlant d'animaux)
\xv kuau a-pewi
\xn un chien qui mord
\cf huu
\dialx PA
\ce manger (protéines, sucre) ; piquer ; mordre
\dt 22/Feb/2025

\lx pewoo
\dialx PA BO
\is interaction
\ps v
\ge faire attention
\xv ye kavwö pewoo nai Kaawo
\dialx PA
\xn il ne prête pas attention à Kaawo
\xv kawu i pewoo nani pòi-n
\dialx BO
\xn elle ne s'occupe pas de ses enfants [BM]
\dt 10/Mar/2023

\lx pe-wovwa
\dialx GOs PA
\va pe-woza
\dialx WEM
\is guerre
\is grammaire_réciproque
\ps v
\ge battre (se)
\dn avec ou sans armes
\cf pe-wèle
\ce battre (se) ; disputer (se)
\dt 22/Feb/2025

\lx pe-whaguzai
\dialx GOs
\is discours
\is grammaire_réciproque_collectif
\ps v ; n
\ge débattre ; discuter ; débat ; discussion
\dt 30/Jan/2019

\lx pe-whili
\dialx GOs
\is déplacement
\is grammaire_réciproque_collectif
\ps v
\ge suivre (se) ; marcher l'un derrière l'autre
\xv li pe-whili
\xn ils se suivent
\dt 25/Jan/2019

\lx pexa
\dialx GOs PA
\is grammaire_préposition
\ps PREP
\ge au sujet de ; à propos de (sens maléfactif)
\xv lie mèèvwu, li pe-kweli-li pexa nye thoomwã
\dialx GOs
\xn quant aux deux frères, ils se détestent à cause de cette femme
\xv e mõõdi pexa la khõbwe ?
\dialx GOs
\xn a-t-il honte de ce qu'il a dit ?
\xv la pe-vhaa pexa nye-nã
\dialx GOs
\xn ils ont discuté de cela
\xv la pe-vhaa pexa nu
\dialx GOs
\xn ils ont discuté de moi
\xv e za yaawa pexa li
\xn oui ! il a la nostalgie d'eux
\xv e tròròwuu ãbaa-nu pexa pòi-je
\dialx GOs
\xn mon frère est content de son enfant
\cf pune
\ce à cause de
\dt 16/Feb/2023

\lx pexu
\dialx GOs PA BO
\is interaction
\ps v ; n
\ge médire ; médisance
\xv li pexu i nu
\xn ils disent du mal de moi
\xv e a-pe-pexu
\dialx GOs
\xn il est médisant
\dt 14/Mar/2019

\lx pe-yu
\dialx GOs
\is société
\ps v
\ge célibataire (être)
\xv e pe-yu
\xn elle est célibataire
\dt 26/Mar/2022

\lx pezii
\dialx GOs
\is jeu
\ps v
\ge jouer
\nt noté |lx{peti, pezi} par Grace
\dt 09/Jan/2022

\lx pezoli
\dialx GOs
\va peroli
\dialx PA
\is caractéristiques_objets
\ps v.stat.
\ge rugueux
\dt 26/Jan/2019

\lx pe-zööni
\dialx GOs
\is interaction
\ps v
\ge maudire (se) mutuellement
\cf thööni
\ce maudire
\dt 25/Aug/2021

\lx pi
\hm 1
\dialx GOs PA BO WE WEM GA
\sn 1
\is oiseau
\ps n
\ge œuf
\se pi-ko
\dialx GOs
\sge œuf de poule
\sn 2
\is poisson
\ps n
\ge frai ; œufs (crustacés) ; laitance (poisson)
\se pi-a nõ
\dialx GOs
\sge œufs de poisson
\se pi-pwòn
\dialx PA
\sge œuf de tortue
\cf êgo
\dialx GOs
\ce œuf
\sn 3
\is fonctions_naturelles_animaux
\ps v
\ge œuf (avoir des)
\xv e pi
\dialx GOs
\xn il a des œufs
\et *mpiRa
\eg œuf
\el POc
\dt 22/Mar/2023

\lx pi
\hm 2
\dialx GOs
\is préparation_aliments
\ps v
\ge cuit (mal) ; pas cuit
\dt 22/Mar/2023

\lx pia
\dialx GOs PA
\is taro
\ps n
\ge taro géant (sauvage, à larges feuilles)
\sc Alocasia sp.
\scf Aracées
\et *(m)piraq
\el POc
\dt 27/Aug/2021

\lx pi-ai
\dialx GOs BO
\is corps
\ps n
\ge omoplate
\dn (lit. carapace-cœur)
\xv pi-ai-nu
\dialx GOs
\xn mon omoplate
\dt 09/Feb/2025

\lx pi-ãmu
\dialx BO PA
\is nourriture
\ps n
\ge miel en rayon
\dt 26/Jan/2019

\lx pi-bwa
\dialx GOs PA
\va du-bwaa-n
\dialx BO PA
\is corps
\ps n
\ge crâne
\xv pi-bwaa-nu
\dialx GOs
\xn mon crâne
\dt 24/Jan/2019

\lx pi-bwèdò
\dialx GOs BO
\is corps
\ps n
\ge ongle
\xv pi-bwèdò hii-n
\dialx PA
\xn ses ongles de doigt de main
\xv pi-bwèdò kòò-n
\dialx PA
\xn ses ongles de doigt de pied
\xv pi-kòò-je
\dialx GO
\xn ses ongles de pied
\cf bwèdò
\dialx GOs PA
\ce doigt
\dt 10/Jan/2022

\lx pibwena
\dialx PA
\is taro
\ps n
\ge taro (clone)
\dt 26/Jan/2018

\lx piça
\ph piʒa, pidʒa
\dialx GOs
\va piya
\dialx BO
\is caractéristiques_objets
\ps v
\ge dur
\ge résistant
\ge fort
\ge solide
\xv e piça phwe-mwa
\dialx GO
\xn la porte est dure (à ouvrir)
\xv e piça bwaa-je
\dialx GO
\xn il est têtu
\xv cimwi piyaa-ni
\dialx BO
\xn serre-le fort
\cf nhyatru, nhyaru
\ce mou (substance, terre)
\dt 10/Jan/2022

\lx piçanga
\ph pidʒaŋa
\dialx GOs
\va pijanga
\dialx BO
\is corps
\ps n
\ge aine
\xv pijanga-n
\dialx BO
\xn son aine
\dt 24/Jan/2019

\lx piçilè
\ph pidʒilɛ
\dialx GOs
\va pivileng
\dialx PA BO
\is insecte
\ps n
\ge guêpe maçonne
\xv pi-pivileng
\dialx BO
\xn nid de la guêpe maçonne
\dt 16/Aug/2021

\lx pidi
\dialx GO
\is corps
\ps n
\ge clitoris
\dt 24/Jan/2019

\lx pi-dili
\dialx GOs PA
\va pigo dili
\dialx PA
\is cultures
\ps n
\ge motte de terre
\dt 26/Jan/2019

\lx pidru
\dialx GOs
\va pidu
\dialx PA BO
\is parenté
\ps n
\ge jumeaux
\dt 27/Jan/2019

\lx piga
\dialx GOs PA
\is action
\ps v
\ge craquer
\dt 22/Mar/2023

\lx pigi yaai
\dialx GOs
\is feu
\ps v
\ge pousser le feu
\dn sous la marmite, ou dans la maison ; moins fort que |lx{carûni}
\cf tha-carûni
\ce pousser le feu
\dt 09/Jan/2022

\lx pi-hi
\dialx GOs PA BO
\va pi-yi
\dialx BO
\is corps
\ps n
\ge ongle
\xv pi-hii-je
\dialx GOs
\xn ses ongles de main
\xv pi-hii-n
\dialx PA BO
\xn ses ongles des doigts
\xv pi-yii-n
\dialx PA
\xn ses ongles des doigts
\dt 14/Mar/2019

\lx pii
\hm 1
\dialx GOs BO PA
\is action_corps
\ps v
\ge couper en deux ; partager
\ge rompre
\dn un bout de pain, un bout d'igname cuite
\xv e pii phwalawa
\xn il a coupé le pain
\dt 08/Feb/2025

\lx pii
\hm 2
\dialx GOs PA BO
\sn 1
\is interaction
\ps v
\ge éviter (qqn. ou qqch.) ; esquiver
\xv e pii do
\dialx GOs
\xn il évite la sagaie
\xv nu pii paa
\dialx GOs
\xn j'ai évité la pierre
\se pe-vii, pe-pii
\dialx GOs
\sge s'éviter
\sn 2
\is guerre
\ps v
\ge parer (un coup)
\dt 17/Feb/2025

\lx pii
\hm 3
\dialx GOs PA BO
\va pii-n
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v.stat. ; nom.adj.
\ge vide
\xv e pii kamyõ
\dialx GOs
\xn le camion est vide
\xv e pii dröö
\dialx GOs
\xn la marmite est vide
\xv kixa hêê-n, pii-n (a) mwa
\dialx PA
\xn il n'y a rien dedans, la maison est vide
\xv pii-n (a) mwa
\dialx PA
\xn une maison vide (d'objets)
\xv pii bwat
\dialx PA
\xn une boîte vide
\xv pii-n
\dialx BO
\xn c'est vide (lit. son vide)
\ng causatif: |lx{pa-pii-ni}
\gt vider qqch.
\sn 2
\is crustacés
\ps n
\ge carapace vide
\ge écaille de tortue
\xv pii-n
\xn sa carapace (vide)
\se pi-pwaji
\dialx BO
\sge carapace vide de crabe
\sn 3
\is mollusque
\ps n
\ge coquille vide (de coquillage)
\xv pii-n
\dialx PA
\xn sa coquille (vide)
\se pi-tagiliã
\dialx BO
\sge coquille vide de bénitier
\dt 22/Feb/2025

\lx piia
\dialx GOs BO PA
\is interaction
\ps v
\ge disputer (se) (verbalement) ; chamailler (se)
\xv pe-piia, pe-vhiia
\dialx GOs
\xn se combattre ; se faire la guerre
\xv la piia pexa dili
\dialx PA
\xn ils se disputent à propos des terres
\xv li pe-pii-li
\dialx PA
\xn ils se disputent
\dt 22/Feb/2025

\lx pii-gu
\dialx GOs PA BO
\sn 1
\is mollusque
\ps n
\ge valve de coquillage
\sn 2
\is instrument
\ps n
\ge râpe
\dn faite d'une valve de coquillage, utilisée pour gratter le coco, la banane, etc.
\dt 08/Feb/2025

\lx pii-me
\is corps_artefact
\dialx GOs PA
\ps n
\ge lunettes
\se mwõ-pii-me
\sge étui à lunettes (lit. maison, contenant de l'œil)
\dt 23/Jan/2022

\lx pîînã
\dialx GOs BO PA
\is déplacement
\ps v
\ge voyager ; promener (se)
\ge rendre visite
\xv èńiza nye çö thaavwu pîînã-du êbòli bwabu ?
\dialx GOs
\xn quand es-tu allée en France pour la première fois ?
\se pe-pîînã
\sge se promener
\dt 31/Oct/2021

\lx pii-nu
\ph pi:ɳu
\dialx GOs
\va pi-nu
\dialx GO(s)
\is arbre_cocotier
\ps n
\ge coquille vide de noix de coco
\dt 02/Jan/2022

\lx pii-peçi
\ph pi:peʒi
\dialx GOs
\va pi-peyi, pi-peji
\dialx PA
\is corps
\ps n
\ge rotule
\dn (lit. carapace du coquillage nommé "savonnette")
\ge malléole
\xv pi peya kò-ny
\dialx BO
\xn ma rotule
\se we-peji
\sge synovie
\dt 09/Feb/2025

\lx pii-pwaji
\dialx GOs PA
\is crustacés
\ps n
\ge carapace de crabe
\dt 29/Jan/2019

\lx pii-pwò
\ph pi:pwɔ
\dialx GOs
\va pii-pwòn
\dialx PA BO
\is reptile_marin
\ps n
\ge carapace de tortue
\dt 22/Mar/2023

\lx pii-ragooni
\dialx GOs
\sn 1
\is mollusque
\ps n
\ge valves de coquille saint-jacques
\sn 2
\is instrument
\ps n
\ge râpe ; grattoir
\dn faite d'une valve de coquille saint-jacques, utilisée pour gratter le coco, la banane, la papaye, etc.
\dt 08/Feb/2025

\lx piixã
\dialx GOs
\is poisson
\ps n
\ge picot noir ; picot (générique)
\dt 08/Feb/2025

\lx piixã ni paa
\dialx GOs
\is poisson
\ps n
\ge picot rayé
\dn il se cache entre les pierres
\sc Siganus lineatus
\scf Siganidés
\dt 27/Aug/2021

\lx pijeva
\dialx GO
\va pijopa
\dialx GO
\is danse
\ps n
\ge danse (type de)
\nt non vérifié
\dt 28/Jan/2019

\lx pijoo
\dialx BO
\is action
\ps v
\ge briser ; casser à moitié
\nt selon BM
\dt 27/Mar/2022

\lx pijopa
\dialx GO
\is religion
\ps n
\ge dieu des enfers
\nt non vérifié
\dt 28/Jan/2019

\lx pi-kò
\dialx GOs PA
\is corps
\ps n
\ge griffe ; ongle de pied
\se pi-kò kuau
\sge griffe de chien
\xv pi-kòò-n
\dialx PA
\xn son ongle de pied
\dt 14/Mar/2019

\lx pi-me
\dialx GOs BO PA
\is corps
\ps n
\ge œil ; globe oculaire
\xv pi-mèè-m
\dialx PA
\xn ton œil
\dt 16/Sep/2021

\lx pi-mèni
\dialx WE WEM GA
\is oiseau
\ps n
\ge œuf d'oiseau
\dt 29/Jan/2019

\lx pi-nõ
\dialx GOs PA BO
\ph pinɔ̃
\is habillement
\ps n
\ge collier
\ge pendentif
\xv pi-nõõ-je
\dialx GO
\xn son collier
\xv pi-nõõ-n
\dialx PA BO
\xn son collier
\dt 02/Jan/2022

\lx piò
\dialx GOs PA BO
\sn 1
\is astre
\ps n
\ge étoile
\sn 2
\is échinoderme
\ps n
\ge étoile de mer |dialx{GOs}
\et *pituqun
\el POc
\dt 22/Mar/2023

\lx pio yaai
\dialx BO
\is astre
\ps n
\ge Mars
\dn (lit. étoile feu)
\dt 08/Feb/2025

\lx piòò
\dialx GOs
\va piyòc
\dialx BO
\is outils
\ps n
\ge pioche
\bw pioche (FR)
\dt 27/Jan/2019

\lx pira
\dialx GOs PA
\is nom_locatif
\ps n.loc
\ge dessous (le) ; sous (une surface, un point)
\xv a-du pira ta
\xn va sous la table
\xv a-e traabwa ni pira cee
\dialx GOs
\xn va t'asseoir sous l'arbre
\xv lha hovwa-da ni nhye pira cee
\dialx GOs
\xn ils arrivent sous le couvert de l'arbre
\xv e thii-da ni pira khii-n
\dialx PA
\xn elle le met sous sa jupe
\dt 03/Feb/2025

\lx pitre
\ph piʈe
\dialx GOs
\va pire
\dialx BO
\is tressage
\ps v ; n
\ge tresser (corde) ; corde tressée
\dt 27/Jan/2019

\lx pitrê
\dialx GOs
\is crustacés
\ps n
\ge crabe plein
\dn aussi appelé "double peau" ; ce mot réfère au moment où la carapace dure se détache et la nouvelle carapace molle se forme
\dt 08/Feb/2025

\lx pivida
\dialx PA
\is insecte
\ps n
\ge sauterelle
\dn à pattes rouges et au corps jaune, fait un bruit de crécelle
\dt 25/Aug/2021

\lx piviige
\dialx BO
\is action_corps
\ps v
\ge saisir avec le bras ; embrasser
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx pivivu
\dialx BO
\is mammifères
\ps n
\ge chauve-souris
\nt selon Corne
\dt 27/Mar/2022

\lx pivwia
\ph piβia
\dialx GOs
\va pivia
\dialx PA
\va pevia
\dialx BO
\is interaction
\ps v
\ge quereller (se) ; disputer (se) ; gronder
\xv li u pivwia
\xn ils se sont disputés
\dt 24/Dec/2021

\lx pivwilo
\ph piβilo
\dialx GOs
\va pwivwilö
\dialx GO(s)
\va pivhilö
\dialx PA
\va pevelo
\dialx BO
\sn 1
\is oiseau
\ps n
\ge hirondelle des grottes ; martinet soyeux
\sc Collocalia esculenta uropygialis
\scf Apodidés
\gb Glossy Swiftlet
\sn 2
\is oiseau
\ps n
\ge hirondelle du Pacifique
\sc Hirundo tahitica subfusca
\scf Hirundinidés
\dt 08/Oct/2021

\lx pivwizai
\dialx GOs
\is caractéristiques_objets
\ps v
\ge étroit (passage en mer, sur terre)
\xv nu thu-menõ bwa dè ka/xa e pivwizai
\dialx GOs
\xn je me promène sur un chemin étroit
\an wala
\at large
\dt 26/Jan/2018

\lx pi-wãge
\dialx GOs
\is corps
\ps n
\ge clavicule
\xv pi-wãge-nu
\xn ma clavicule
\dt 24/Jan/2019

\lx pixãge
\dialx GOs
\is religion
\ps n
\ge croix
\dt 28/Jan/2019

\lx pixu
\dialx GOs
\is son
\ps v
\ge grincer
\xv e pixu phwè-mwa
\dialx GOs
\xn la porte grince
\xv e pixu vèle
\dialx GOs
\xn le lit grince
\xv lha pixu cee
\dialx GOs
\xn les arbres ploient en faisant du bruit (sous l'effet du vent)
\dt 30/Dec/2021

\lx piyu
\dialx PA BO
\is armes
\ps n
\ge pierre de fronde
\dn petite, noire, dure
\xv paa-piyu
\xn serpentine (avec laquelle on faisait les pierres de fronde)
\dt 08/Feb/2025

\lx piyuli
\dialx PA BO [BM]
\is interaction
\ps v
\ge disputer (se) ; chercher querelle ; reprocher
\se a-piyuli
\sge qui cherche toujours des querelles ; qqn. qui fait toujours des reproches
\se pe-piyuli
\sge se disputer, se chamailler
\dt 22/Feb/2025

\lx pizò
\dialx GOs
\ph piðɔ
\va pilo-n, pilò(ò)
\dialx PA BO
\va pila
\dialx BO
\sn 1
\is nourriture
\ps n
\ge chair (en général) ; viande
\ge chair (d'igname, taro)
\se pizò kui
\dialx GOs
\sge chair de l'igname
\se pila kuru
\dialx BO
\sge chair du taro (Dubois)
\cf layô
\ce viande
\sn 2
\is corps
\ps n
\ge muscle
\dt 21/Mar/2023

\lx po
\hm 1
\dialx GOs BO
\va poo
\dialx BO
\sn 1
\ps n
\ge chose
\sn 2
\is grammaire_pronom
\ps PRON indéfini
\ge quelque chose
\xv i khila xa po
\dialx BO
\xn il cherche qqch.
\xv kawu nu nõõli xa po
\dialx BO
\xn je ne vois rien
\dt 21/Feb/2025

\lx po
\hm 2
\dialx GOs BO
\va pwò
\dialx BO
\va thu
\dialx PA
\sn 1
\is action
\ps v
\ge faire
\xv e po za ?
\dialx GOs
\xn qu'a-t-elle fait ? ; comment a-t-elle fait ?
\xv i pwò ra ?
\dialx BO
\xn que fait-elle ? ; comment a-t-elle fait ?
\xv i po na
\dialx PA
\xn elle a fait cela
\xv e zo xo nu ! axe içö, çö zoma po za mwa ?
\dialx GOs
\xn ç'est bien pour (lit. avec) moi, mais toi, comment feras-tu donc ?
\se po za ? ; po ra ?
\sge faire comment ?
\et *pua(t)
\el POc
\sn 2
\is grammaire_existentiel
\ge il y a
\dt 22/Feb/2025

\lx po-
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM (générique et des objets ronds)
\ge CLF des objets ronds (fruits, heure, etc.)
\se po-xè (1); po-tru (2); po-kò (3); po-pa (4), etc.
\dialx GOs
\sge un, deux, trois, quatre objets ronds
\xv po-xè pò-mãã
\xn une mangue
\ng |lx{a(a)-}(animés), |lx{go-, we-, pepo-}
\dt 22/Feb/2025

\lx pò
\hm 1
\dialx GOs
\va pò-n
\dialx PA BO
\va pwò
\dialx GOs BO
\is plantes_partie
\ps n
\ge fruit ; graine
\xv pòò-n ; pò-n
\dialx PA
\xn son fruit
\xv pwò-n
\dialx BO
\xn le fruit (de l'arbre)
\xv po-xè pò-mãã
\xn une mangue
\xv p(w)o-kò pò-mãã
\dialx GO
\xn trois mangues
\se pò-cee
\sge fruit
\se pò-caai
\sge fruit du jamelonier
\se p(w)ò-mãã
\dialx GO
\sge mangue
\et *pua(q)
\el POc
\dt 18/Mar/2023

\lx pò
\hm 2
\dialx GOs
\va pòl
\dialx BO
\va pul
\dialx PA
\is plantes
\ps n
\ge fougère (petite)
\sc Pteridium aquilinum
\scf Dennstaedtiacées
\dt 26/Jan/2022

\lx pò
\hm 3
\dialx GOs PA BO
\va pwò
\dialx BO
\is grammaire_quantificateur_mesure
\ps QNT ; atténuatif
\ge peu (un)
\xv pò na-mi
\dialx GOs
\xn donne un peu
\xv e pò ẽnõ nai nu
\dialx GOs
\xn il est un peu plus jeune que moi
\xv nu pò trûã-çö
\dialx GOs
\xn je t'ai un peu menti
\xv nu pò thûã-yu
\dialx PA
\xn je t'ai un peu menti
\dt 16/Feb/2025

\lx pò
\hm 4
\dialx GOs
\is poisson
\ps n
\ge poisson "ruban" ; poisson-sabre
\sc Trichiurus lepturus
\scf Trichiuridés
\dt 27/Aug/2021

\lx pô
\ph põ
\dialx GOs
\va pôm
\dialx PA BO
\is insecte
\ps n
\ge papillon de nuit
\dn papillon marron et duveteux qui se nourrit de fruit
\et *mpompoŋ
\el POc
\dt 22/Oct/2021

\lx pò a-hu-ò
\dialx GOs
\va pwo a-wò
\dialx GOs
\sn 1
\is déplacement
\ps v
\ge avancer un peu
\sn 2
\is mouvement
\ps v
\ge pousser (se) un peu ; faire un peu de place
\ng |lx{-(w)ò} est le directionnel centripète
\dt 05/Jan/2022

\lx po za ?
\dialx GOs
\va po ra ?
\dialx PA
\is grammaire_interrogatif
\ps v.INT
\ge que faire ? ; faire comment ?
\xv çö po za ?
\xn que fais-tu ?
\dt 14/Aug/2021

\lx pò-baa
\dialx BO
\is richesses
\ps n
\ge perles de verre
\nt selon BM ; non vérifié
\dt 26/Mar/2022

\lx pobe
\dialx PA BO
\va pwobe
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v.stat.
\ge petit
\sn 2
\is grammaire_quantificateur_mesure
\ps v.stat.
\ge un peu
\cf popobe
\ce un petit peu
\dt 16/Feb/2025

\lx pobil
\dialx PA BO
\is richesses
\ps n
\ge ceinture de femme finement tressée
\dn elle est faite avec les racines de bourao, sert de monnaie et a plus de valeur que |lx{wepoo} ; elle faisait plusieurs fois le tour du bassin et se portait au-dessus de |lx{wepoo} (selon Dubois ms. et Charles Pebu-Polae)
\cf thabil
\dialx PA
\ce ceinture de femme (monnaie)
\dt 30/Mar/2022

\lx pobo
\dialx GOs PA BO
\is déplacement
\ps n
\ge trace
\dn laissée par un animal ou une personne qui a dormi à un endroit
\xv pobo-n
\dialx PA
\xn ses traces
\dt 08/Feb/2025

\lx pobo-poin
\dialx GO WE
\is cultures_champ
\ps n
\ge champ ; trace de champ abandonné
\cf poin
\dialx BO
\ce champ
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx pòbwinõ
\dialx PA BO
\sn 1
\is corps
\ps n
\ge fesses ; derrière ; postérieur
\xv pòbwinõ-n
\xn ses fesses
\sn 2
\is nom_locatif
\ps n
\ge nord
\xv ni pòbwinõ mwã
\xn au nord du pays
\dt 27/Jan/2022

\lx pòbwinõ-wõ
\dialx GOs
\va pòbwinõ-wony
\dialx PA BO
\is navigation
\ps n
\ge poupe
\cf mura-wõ
\ce poupe
\dt 25/Aug/2021

\lx pò-bwiri
\dialx GOs
\is instrument
\ps n
\ge mors
\dt 27/Jan/2019

\lx pôbwi-we
\ph põbwi we
\dialx GOs
\is eau
\ps n
\ge lac
\dt 03/Feb/2019

\lx pò-caai
\ph pɔca:i
\dialx GOs PA
\sn 1
\is plantes_fruits
\ps n
\ge pomme canaque
\sc Syzygium malaccense; Eugenia malaccensis
\scf Myrtacées
\sn 2
\is plantes_fruits
\ps n
\ge fruit de "pomme-rose"
\sc Syzygium jambos
\scf Myrtacées
\dt 02/Jan/2022

\lx pò-ci
\ph pɔcɨ
\dialx GOs
\va po-cin
\ph pɔcin
\dialx PA WE
\is plantes_fruits
\ps n
\ge papaye
\dt 02/Jan/2022

\lx po-da ?
\dialx GO
\is grammaire_interrogatif
\ps INT
\ge pour quoi ?
\ng forme courte de |lx{ponga da ?}
\gt pour quoi ?
\dt 07/Jan/2022

\lx podi
\dialx GOs PA BO
\va pwodi
\dialx BO
\is bananier
\ps n
\ge bananier ("banane chef")
\dn il est interdit de la cuire sur la braise, elle ne peut être que bouillie
\sc Musa sp.
\scf Musacées
\et *pundi
\el POc
\dt 10/Jan/2022

\lx pòdòu
\dialx GOs
\is caractéristiques_objets
\ps n
\ge objet emballé
\dt 21/Jan/2018

\lx pò-draado
\dialx GOs
\is corps
\ps n
\ge iris (œil)
\dn (lit. fruit de Vitex trifoliata)
\dt 09/Feb/2025

\lx põng
\ph pɔ̃ŋ
\dialx BO
\is arbre_cocotier
\ps n
\ge gaine de l'inflorescence du cocotier
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ponga
\ph poŋa
\dialx GOs
\is grammaire_préposition
\ps PREP.BENEF
\ge pour
\xv ponga çö
\xn pour toi
\xv e zaa uvi tiiwo ponga je
\dialx GOs
\xn il s'est acheté un livre pour lui-même
\xv hovwo ponga hauva
\xn de la nourriture pour la levée de deuil
\xv lha pavwa ponga nhyãã-mõlò
\dialx GOs
\xn ils font les préparatifs pour les fêtes coutumières
\dt 30/Dec/2021

\lx ponga da ?
\ph poŋa da
\dialx GOs BO
\va puxã da ?
\dialx GO(s) PA
\is grammaire_interrogatif
\ps INT
\ge pourquoi faire ? ; à quoi sert ?
\dt 30/Dec/2021

\lx pogabe
\dialx GOs BO
\is caractéristiques_objets
\ps v.stat.
\ge mince ; étroit
\ge imperceptible
\ge très petit
\dt 20/Aug/2021

\lx poge kui
\dialx GOs
\is coutumes
\ps n
\ge tas d'ignames
\dt 28/Jan/2019

\lx pò-hoxèè
\dialx GOs
\is grammaire_quantificateur_mesure
\is grammaire_comparaison
\ps QNT
\ge moins
\dt 16/Feb/2025

\lx poi
\dialx GO
\is grammaire_modalité
\ps HORT
\ge que !
\dt 30/Jan/2019

\lx pòi
\dialx GOs PA WEM BO
\va pwe
\dialx BO
\sn 1
\is parenté
\ps n
\ge enfant (fille/fils) ; enfant de frère et de cousins (homme parlant)
\ge enfant de fils de frère ou sœur du père (= petits cousins, homme parlant)
\ge enfant de fils de frère ou de sœur de mère (homme parlant)
\ge enfant de sœur et de cousines (femme parlant)
\se pòi-nu whamã
\dialx GOs
\sge mon aîné
\se pòi-nu ẽnõ
\dialx GOs
\sge mon dernier enfant
\xv pòi-je
\dialx GOs
\xn son enfant
\xv pòi-m
\dialx WEM
\xn ton enfant
\xv gele xa pwe-m/pòi-m ?
\dialx BO
\xn as-tu des enfants ?
\sn 2
\is classificateur_possessif
\ps CLF.POSS
\ge classificateur possessif (animaux et objets créés)
\xv la pòi-je ko mãni ilã-ã
\dialx GOs
\xn ses poules et les autres (animaux)
\xv pòi-ã vaaci
\dialx PA
\xn notre bétail
\cf pööni
\ce enfant de sœur (homme parlant)
\cf hê-kòlò
\ce enfant de frère et de cousin (femme parlant)
\dt 31/Jan/2022

\lx pò-kênii
\is habillement
\ph pɔkẽɳi:
\dialx GOs
\ps n
\ge boucle d'oreilles
\dn (lit. fruit des oreilles)
\dt 09/Feb/2025

\lx poki
\dialx GOs PA BO
\va poxi
\dialx GOs
\is fonctions_naturelles
\ps v
\ge enceinte (être)
\xv e poxi Hiixe
\xn Hiixe est enceinte
\dt 04/Nov/2021

\lx pò-kîga
\ph pɔkîŋga
\dialx GOs
\is fonctions_naturelles
\ps v
\ge rire un peu
\dt 21/Mar/2023

\lx po-kiò
\dialx GOs PA BO
\wr A
\is corps
\ps n
\ge ventre
\xv e naa-da mwã ni pò-kiò-n
\dialx PA
\xn elle le met dans son ventre
\wr B
\is caractéristiques_personnes
\ps v.stat.
\ge ventru ; corpulent
\dt 21/Dec/2021

\lx pò-kô-hu-ò
\dialx GOs
\va pwo kô-wò
\dialx GOs
\is action_corps
\ps v
\ge pousser (se) un peu ; faire un peu de place (position couchée)
\dt 08/Feb/2025

\lx pò-ko-kabu
\dialx GOs
\va pò-ko-xabu
\dialx GO(s)
\va po-ko-kabun
\dialx PA BO
\is temps_jours
\ps n
\ge jeudi
\dn (lit. 3 jours avant le jour sacré/dimanche)
\dt 20/Feb/2025

\lx pò-ku-hu-ò
\dialx GOs
\va pwo ku-wò
\dialx GOs
\is action_corps
\ps v
\ge pousser (se) un peu (debout) ; faire un peu de place (quand on est debout)
\dt 26/Jan/2019

\lx pola
\dialx PA BO
\va pwola
\dialx BO
\va thrale
\dialx GO(s)
\is natte
\ps n
\ge natte
\dn utilisée pour couvrir le toit, elles sont faites de deux demi palmes de cocotier
\et *mpola
\el POc
\dt 08/Feb/2025

\lx pò-mãã
\is plantes_fruits
\dialx GO PA
\ps n
\ge mangue
\dt 26/Aug/2021

\lx pò-maxa
\dialx GOs
\is temps_atmosphérique
\ps n
\ge buée
\dn (lit. fruit de saison froide)
\dt 08/Feb/2025

\lx pòmi-nõ
\dialx GOs
\va pòbwi-nòò-n
\dialx BO
\is corps
\ps n
\ge derrière ; postérieur
\ge fesses
\dt 27/Jan/2022

\lx pòminõ phwamwa
\dialx GOs
\is nom_locatif
\ps n.loc
\ge nord du pays
\dt 03/Feb/2025

\lx pomitee
\dialx GOs
\va 'pomtee
\dialx GO(s)
\is nourriture
\ps n
\ge pomme de terre
\bw pomme de terre (FR)
\dt 26/Jan/2019

\lx pomõ
\dialx GOs
\va pòmõ-n
\dialx PA
\va pwòmõ, pwamõ
\dialx WE PA
\va pòmwõ
\dialx BO
\sn 1
\is cultures_champ
\ps n.loc (forme POSS de pwamwa)
\ge champ
\xv kê-pomõ-nu
\dialx GOs
\xn mon champ
\sn 2
\is habitat
\ps n.loc
\ge pays
\xv pomõ-nu
\dialx GOs
\xn mon pays ; chez moi
\sn 3
\is nom_locatif
\ps n.loc
\ge chez
\xv nu a-da pòmwõ-ny
\dialx BO
\xn je monte chez moi
\xv pòmõ-n
\dialx PA
\xn chez lui (lit. demeure-sa)
\dt 22/Feb/2025

\lx pomõ-da
\dialx GOs PA
\va pomwa-da, poma-da
\dialx BO
\is grammaire_direction
\ps n.DIR
\ge en amont ; en haut
\dt 05/Nov/2021

\lx pomõ-du
\dialx GOs PA
\va pomwõdu, pomõ-du
\dialx BO
\is grammaire_direction
\ps n.DIR
\ge en aval ; en bas
\dt 05/Nov/2021

\lx pomõ-mi
\dialx GOs
\is grammaire_direction
\ps n.loc
\ge par ici
\xv pomõ-mi ne
\xn approche-toi vers ici
\xv pomõ-mi xòlò nu
\xn approche-toi de moi
\dt 03/Feb/2025

\lx pomõnim
\dialx PA
\is eau
\ps n
\ge tourbillon d'eau
\dn petit et rapide
\cf niilöö
\ce grand tourbillon lent
\dt 08/Feb/2025

\lx pomõ-òli
\dialx GOs BO
\is grammaire_direction
\ps n.DIR
\ge là-bas au-delà (de l’endroit en question)
\xv a-e pomõ-òli !
\dialx GOs
\xn va de l'autre côté là-bas (du champ ou de l’endroit en question)
\xv a mwa-e pomõ-òli !
\dialx BO
\xn va de l'autre côté là-bas
\dt 17/Apr/2024

\lx pò-mugo ni hii
\dialx WEM
\va pò-mugo ne hii-n
\dialx BO
\is corps
\ps n
\ge biceps
\dt 19/Jun/2022

\lx pò-mugo ni kò
\dialx WEM
\va pò-mugo ne kòò-n
\dialx BO
\va pò-muge ni kòò-n
\dialx PA
\is corps
\ps n
\ge mollet
\xv pò-muge ni kòò-ny
\dialx PA
\xn mon mollet (lit. le fruit banane de ma jambe)
\xv pò-mugo ni/ne kòò-n
\dialx BO
\xn son mollet
\dt 19/Jun/2022

\lx po-ni
\ph poɳi, poni
\dialx GOs
\va po-nim
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge cinq
\dt 08/Oct/2021

\lx pò-ni-kabu
\dialx GOs
\va pò-ni-kabun
\dialx PA BO
\is temps_jours
\ps n
\ge mardi
\dn (lit. 5 jours avant le jour sacré/dimanche)
\dt 20/Feb/2025

\lx po-ni-ma-ba
\dialx GOs
\va po-nim (m)a-ba
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge neuf
\dt 08/Oct/2021

\lx po-ni-ma-dru
\dialx GOs
\va po-nim (m)a-ru
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge sept
\dt 08/Oct/2021

\lx po-ni-ma-gò
\dialx GOs
\va po-nim (m)a-kòn
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge huit
\dt 08/Oct/2021

\lx pòni-ma-wee
\dialx GOs
\va pòdi-ma-pwèèl
\dialx BO [Corne]
\is corps
\ps n
\ge cheville ; malléole de la cheville ?
\dn selon [Haudricourt]
\dt 09/Feb/2025

\lx po-ni-ma-xè
\dialx GOs
\va po-nim (m)a-xè
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge six
\dt 08/Oct/2021

\lx po-niza ?
\ph poɳiða
\dialx GOs
\va po-nita? po-nira?
\dialx PA BO
\va pwo-nira?
\dialx BO
\is grammaire_interrogatif
\ps INT
\ge combien ? (inanimés)
\xv po-niza hîńõ-a ?
\dialx GOs
\xn quelle heure est-il ?
\xv pwo-nira hinõ-al ?
\dialx BO
\xn quelle heure est-il ?
\xv po-niza kau-çö ?
\dialx GOs
\xn quel âge as-tu ?
\dt 11/Mar/2023

\lx pòńõ
\ph pɔnɔ̃
\dialx GOs
\is grammaire_quantificateur_mesure
\ps QNT
\ge peu ; un peu (quantité)
\xv ii cè-çö xo pòńõ
\dialx GO
\xn sers ta nourriture (féculents) en petite quantité (n'en prends pas trop)
\xv mwêêno pòńõ vwo la za mã !
\dialx GO
\xn il s'en est fallu de peu qu'il ne meurent !
\xv coxe phwalawa na pòńõ
\dialx GO
\xn coupe un peu de pain
\se mwêêno pòńõ vwo …
\sge s'en falloir de peu que
\cf pobe, popobe
\dialx PA
\ce un peu
\dt 16/Feb/2025

\lx po-nõgò
\ph poɳɔ̃ŋgɔ
\dialx GOs PA WEM
\va nogo
\dialx BO
\is eau
\ps n
\ge creek ; rivière ; ruisseau
\dt 02/Jan/2022

\lx pònu
\ph pɔɳu
\dialx GOs PA BO
\va pwònu
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v
\ge plein (être)
\xv e pònu xo we
\dialx GO
\xn c'est plein d'eau
\xv u pònu lhã mhenõvwo ni mee-jèè
\dialx GO
\xn elle a beaucoup de cicatrices/de plaies sur le visage
\sn 2
\is caractéristiques_animaux
\ps v
\ge plein (crabe)
\et *ponuq
\eg full
\el POc
\dt 25/Dec/2021

\lx pô-nu
\ph põɳu
\dialx GOs
\va pon
\dialx BO
\is arbre_cocotier
\ps n
\ge rachis de coco
\dt 02/Jan/2022

\lx pònum
\dialx PA BO
\va pònèn, pwanèn
\dialx BO
\is caractéristiques_objets
\ps v.stat.
\ge court ; petit
\xv i pònum xo hangai
\dialx PA
\xn il est petit et gros
\xv pònum (a) mada
\dialx BO
\xn le tissu est court
\se pa-pònume hôxa cee
\sge raccourcir un bout de bois
\ng causatif: |lx{pa-pònume}
\gt raccourcir
\dt 05/Jan/2022

\lx ponyãã
\dialx GOs
\va ponyam
\dialx PA BO
\is caractéristiques_personnes
\ps v.stat.
\ge aimable ; doux ; gentil
\xv e ponyãã
\dialx GOs
\xn il est doux
\xv i a-ponyam
\dialx PA
\xn il est gentil
\dt 26/Jan/2019

\lx pòò
\dialx GOs PA BO
\is arbre
\ps n
\ge bourao
\dn terme générique
\sc Hibiscus tiliaceus L.
\scf Malvacées
\et *paRu
\el POc
\dt 08/Feb/2025

\lx pòòdra
\dialx GOs
\va pòòda
\dialx BO
\va pòòdaang
\dialx BO
\is arbre
\ps n
\ge bourao (de bord de mer)
\dn son écorce comestible est une nourriture de disettes ; elle sert aussi à fabriquer les jupes de femme et des cordages
\sc Hibiscus tiliaceus
\scf Malvacées
\dt 27/Aug/2021

\lx pòò-hovwo
\dialx GOs
\is arbre
\ps n
\ge bourao
\dn espèce dont l'écorce est comestible
\dt 05/Mar/2019

\lx pööni
\dialx GOs
\va puuni
\dialx PA BO
\sn 1
\is parenté
\ps n
\ge neveu (enfant de sœur, homme parlant)
\ge enfant de sœur du père ou de fille de frère (= petits cousins)
\ge cousins de mère
\xv pööni-je
\dialx GO
\xn son neveu ; sa nièce (utérin)
\xv pööni-n
\dialx BO
\xn son neveu ; sa nièce (utérin)
\sn 2
\is parenté
\ps n
\ge oncle maternel (|dialx{PA BO} seulement)
\ge époux de sœur de père (|dialx{PA BO} seulement)
\se èvwööni
\dialx GO
\sge oncle maternel
\cf pòi
\ce enfant de frère et de cousins (homme parlant); enfant de sœur et de cousines (femme parlant)
\dt 22/Feb/2025

\lx poo-yaai
\dialx PA BO
\is feu
\ps n
\ge étincelle (du feu)
\dt 26/Jan/2019

\lx po-pa
\dialx GO
\is grammaire_numéral
\ps NUM
\ge quatre
\dt 17/Aug/2021

\lx pò-pa-kabu
\dialx GOs
\va pò-pa-xabu
\dialx GO(s)
\va pò-pa-kabun
\dialx PA BO
\is temps_jours
\ps n
\ge mercredi
\dn (lit. 4 jours avant le jour sacré/dimanche)
\dt 20/Feb/2025

\lx pò-pa-kudi
\dialx GOs
\is configuration
\ps v
\ge carré (lit. quatre coins)
\dt 21/Dec/2021

\lx pöpö
\is parenté_adresse
\dialx GOs PA
\ps n
\ge tonton (utérin)
\dn langage des enfants
\dt 08/Feb/2025

\lx popobe
\dialx PA BO [Dubois]
\va pobe
\dialx WE
\sn 1
\is caractéristiques_objets
\ps v.stat.
\ge petit ; court
\ge menu
\sn 2
\is grammaire_quantificateur_mesure
\ps v.stat.
\ge un tout petit peu
\xv cooxe xa pã na popobe
\dialx PA
\xn coupe un tout petit peu de pain
\dt 16/Feb/2025

\lx pò-pwa
\dialx GOs
\va pò(ò)-pwal
\dialx PA
\is temps_atmosphérique
\ps n
\ge goutte de pluie
\dt 16/Aug/2021

\lx pò-pwaale
\dialx GOs
\va pò-vwale
\dialx GO(s)
\is plantes
\ps n
\ge maïs (épi de)
\se kò-pò-pwaale
\sge tige de maïs ; maïs (plante)
\dt 22/Feb/2025

\lx pò-pwe
\dialx GOs PA BO
\va pwò-pwe
\dialx GO(s) BO
\is pêche
\ps n
\ge hameçon
\dt 26/Aug/2021

\lx po-ra ?
\dialx BO PA
\va po-za?
\dialx GO
\is grammaire_interrogatif
\ps v
\ge quoi faire ? 
\dn (lit. faire quoi ?)
\xv la po-ra ?
\xn qu'ont-ils fait ?
\dt 22/Feb/2025

\lx pò-sitrô
\dialx GOs
\is plantes_fruits
\ps n
\ge citron
\dn (lit. fruit citron)
\bw citron (FR)
\dt 08/Feb/2025

\lx pò-thala
\dialx GOs
\is caractéristiques_objets
\ps v
\ge entrouvert
\xv pò-thala phwèè-mwa
\xn la porte est entrouverte
\dt 20/Aug/2021

\lx pò-thi
\dialx GOs
\va pò-ti-n, pwò-thi-n
\dialx BO
\is corps
\ps n
\ge mamelon du sein
\xv pò-thi nu
\dialx GO
\xn mon sein
\dt 24/Jan/2019

\lx pò-tree-hu-ò
\dialx GOs
\va pwo tree-wò
\dialx GOs
\is mouvement
\is action_corps
\ps v
\ge pousser (se) un peu (assis)
\ge faire un peu de place (quand on est assis)
\dt 20/Aug/2021

\lx po-tru
\dialx GOs
\ph poʈu
\va po-ru
\dialx PA
\va po-du, po-ru
\dialx BO
\is grammaire_numéral
\ps NUM
\ge deux (générique)
\se ba-po-ru
\sge deuxième
\dt 02/Jan/2022

\lx pò-tru kabu
\dialx GOs
\ph pɔʈu kambu
\va po-ru-kabun
\dialx PA BO
\va bo-hode
\dialx PA
\is temps_jours
\ps n
\ge vendredi
\dn (lit. 2 jours avant le jour sacré/dimanche)
\dt 20/Feb/2025

\lx pòvwòńò
\dialx GOs
\is caractéristiques_objets
\ps v.stat.
\ge petit ; fin (en épaisseur)
\ng réduplication de |lx{pòńò}
\dt 22/Mar/2023

\lx pòvwonû
\dialx GOs
\is caractéristiques_objets
\va pòponû
\dialx GO(s)
\va ponû
\dialx BO PA WE
\va ponum
\dialx BO
\ps v.stat.
\ge court (taille, hauteur)
\xv e pòvwonû nai pòi-nu
\dialx GOs
\xn il est plus petit que mon enfant
\xv nu pòvwonû trûã nai çö
\dialx GOs
\xn je suis un tout petit peu plus petit que toi
\an waa
\dialx GOs
\at grand
\dt 10/Jan/2022

\lx pòvwòtru
\dialx GOs
\va pwòvwòtru
\dialx GO(s)
\va pòpòru
\dialx PA
\is fonctions_intellectuelles
\ps v
\ge douter ; hésiter
\xv la pòpòru
\dialx PA
\xn ils ont hésité
\ng réduplication de |lx{pò-tru} deux > |lx{pò-pò-tru} être entre deux avis
\dt 06/Jan/2022

\lx po-vhaa
\dialx GOs
\is discours
\ps v
\ge parler doucement
\dn (lit. un peu parler)
\dt 09/Feb/2025

\lx pôwe
\ph põwe
\dialx GOs
\is eau
\ps v
\ge mouillé (par la pluie, la rosée)
\dt 29/Jan/2019

\lx poweede
\dialx GOs PA BO
\va pweede
\dialx GO
\is mouvement
\ps v
\ge retourner (verre, seau, etc.)
\ge tourner (page)
\ge changer ; corriger |dialx{BO}
\dt 04/Feb/2025

\lx pò-wha
\dialx GOs
\is igname
\ps n
\ge igname
\dn ressemble au fruit du figuier
\dt 25/Aug/2021

\lx poxa
\hm 1
\dialx GOs PA GO
\va poga
\dialx PA
\sn 1
\is caractéristiques_personnes
\ps n
\ge jeune
\se poxa kò
\dialx GOs
\sge bâtard (lit. enfant de la forêt)
\se poxa no mãe
\dialx GOs
\sge bâtard (lit. enfant dans la paille)
\sn 2
\is caractéristiques_animaux
\ps n
\ge petit (le petit d'un animal)
\se poxa ko
\sge poussin
\se poxa poxa
\sge porcelet
\sn 3
\is caractéristiques_objets
\ps n
\ge petit
\se poxa mwa
\sge petite maison
\se poxa wõ
\sge petit bateau
\ng |lx{poxa} est souvent abrégé en |lx{po}
\dt 05/Jan/2022

\lx poxa
\hm 2
\dialx GOs
\va pwaxa
\dialx GA
\va poka
\dialx PA WE BO
\va pwaka, pwòka
\is mammifères
\ps n
\ge porc ; cochon
\xv poxa i nu
\xn mon cochon
\bw emprunt polynésien
\dt 16/Aug/2021

\lx poxa aazo
\dialx GOs
\va poga aao
\dialx BO
\is société_organisation
\ps n
\ge fils du chef
\dn (lit. le petit)
\dt 08/Feb/2025

\lx poxa Teã
\dialx PA
\va poxo Teã
\is société_organisation
\ps n
\ge petit chef
\dt 08/Oct/2021

\lx poxabu
\dialx BO
\is nourriture
\ps n
\ge gâteau de taro
\nt selon Corne ; non vérifié
\dt 24/Feb/2025

\lx poxabwa
\dialx GOs
\va paxâbwa
\dialx PA BO
\is igname
\ps n
\ge igname
\dn longue et à racine profonde plantée au centre du billon (selon Charles Pebu-Polae)
\dt 17/Feb/2025

\lx poxa-du hogo
\dialx GOs PA
\is topographie
\ps n
\ge ramification dans la montagne
\dt 28/Jan/2019

\lx poxa-he
\dialx GOs
\is feu
\ps n
\ge tisonnier
\dt 26/Jan/2018

\lx poxawèo
\dialx BO
\va pwawèo
\dialx BO
\va pokaweo
\dialx BO
\is interaction
\ps v ; n
\ge imiter ; imitation
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx poxa-zaaja
\ph poxaza:dja
\dialx GOs
\is poisson
\ps n
\ge loche (de taille adulte)
\dt 02/Jan/2022

\lx po-xè
\dialx GO PA
\is grammaire_numéral
\ps CLF.NUM (objets ronds)
\ge un (objet rond) ; un jour
\xv po-xè, po-tru
\xn un, deux, etc.
\xv ni ka xa po-xè
\dialx GOs
\xn la même année
\xv li ẽno ni ka xa po-xè
\dialx GOs
\xn ils sont nés la même année
\xv po-xè kênii-je
\dialx GOs
\xn il n'a qu'une oreille
\xv li u a po-xe
\dialx PA
\xn ils partent ensemble (comme un)
\xv ni tèn xa po-xe
\dialx BO
\xn un jour
\dt 17/Aug/2021

\lx poxe hõ
\dialx PA
\is grammaire_conjonction
\ps CNJ
\ge mais seulement
\xv ra êgu a-xe, poxe hõ, ma ka mhwã nõõ-je
\xn c'est une personne, mais seulement nous ne pouvons pas le voir
\dt 03/Feb/2025

\lx po-xe kabu
\dialx GOs
\va po-xe kabun
\dialx BO
\va cavato
\dialx PA BO
\is temps_jours
\ps n
\ge samedi
\dn (lit. 1 jour avant le jour sacré/dimanche)
\dt 20/Feb/2025

\lx po-xè nõ
\ph pɔɣɛ ɳɔ̃
\dialx GOs
\is grammaire_quantificateur_mesure
\ps RESTR
\ge un seul(ement)
\xv nu nõõle po-xè nõ wõ
\dialx GOs
\xn j'ai vu un seul bateau
\dt 16/Feb/2025

\lx poxee ma
\dialx GOs
\va (h)axe poxee ma
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge en revanche ; pourtant
\xv kavwö li yue-me, axe poxee ma li zalae-me
\xn ils ne nous ont pas adoptés, mais en revanche, ils nous ont élevés
\dt 21/Apr/2024

\lx poxèè na
\ph pɔɣɛ: ɳa
\dialx GOs PA
\va poxè
\dialx PA BO
\is grammaire_modalité
\ps MODAL
\ge peut-être que
\xv poxèè na ezoma uça mõnõ
\dialx GOs
\xn il viendra peut-être demain
\xv poxèè na e
\dialx GOs
\xn c'est peut-être ainsi/cela
\xv poxèè na kavwö trõne
\dialx GOs
\xn il n'a peut-être pas entendu
\xv poxè na u ruma havha menon
\dialx PA
\xn il viendra peut-être demain
\xv novwo na i havha na poxè kòi-nu, ye yu thala mwa
\dialx PA
\xn s'il vient demain quand je serai peut-être absent, alors ouvre la maison
\xv poxè na e kha-phe xo Kaavo
\dialx PA
\xn peut-être que Kaavo l'a pris en partant
\dt 26/Dec/2024

\lx poxi
\dialx GOs
\va poki
\dialx PA
\is fonctions_naturelles
\ps v
\ge enceinte (être)
\xv e poxi
\xn elle est enceinte
\cf pu
\ce enfler ; gonflé ; grossir
\dt 25/Jan/2019

\lx pò-zaalo
\dialx GOs
\is plantes_fruits
\ps n
\ge fruit du "gommier"
\dt 29/Jan/2019

\lx pozo
\dialx GOs
\is habillement
\ps n
\ge bague
\se pozo-hi
\sge bague; alliance
\dt 25/Jan/2019

\lx pu
\hm 1
\dialx GOs PA BO
\is corps
\ps n
\ge cheveux
\ge poil
\ge fourrure
\ge plume
\xv pu-n
\dialx PA
\xn ses cheveux/poils
\xv pu-bwaa-je
\dialx GOs
\xn ses cheveux (il faut spécifier la partie du corps)
\se pu-mhwêêdi-n
\dialx PA
\sge sa moustache
\et *pulu
\eg poil
\el POc
\dt 22/Feb/2025

\lx pu
\dialx GOs
\va pum
\dialx PA BO
\va bu, bo
\dialx BO
\hm 2
\sn 1
\is feu
\ps n
\ge fumée
\xv pubu yaai
\dialx BO
\xn la fumée du feu
\ng forme déterminée: |lx{pubu}
\gt fumée de qqch.
\sn 2
\is terrain_terre
\ps n
\ge poussière
\xv pubu dili
\dialx GOs PA BO
\xn poussière de la terre
\sn 3
\is santé
\ps v
\ge inconscient
\xv i pum (a) mèè-n
\dialx BO
\xn il est inconscient (lit. ses yeux sont dans la fumée)
\dt 21/Feb/2025

\lx pu
\hm 3
\dialx GO PA BO
\va vwö, wu
\is grammaire_conjonction
\ps CNJ
\ge pour ; afin de
\xv nu nyãnume i je vwö/pu a-mi
\dialx GO
\xn je lui ai fait signe de s'approcher
\xv nu ru phweewu-yu pu yu tõne
\dialx BO
\xn je vais te le raconter pour que tu entendes
\dt 20/Feb/2025

\lx pu
\hm 4
\dialx GOs PA
\is grammaire_existentiel
\ps v
\ge il y a
\xv pu mõû pòi-je
\dialx GO
\xn leur fils a une épouse
\xv pu mwani i lò, pu loto i lò
\dialx GO
\xn ils ont de l'argent, ils ont une voiture
\xv mô vara pu phò-ã
\dialx GOs
\xn nous avons chacun notre charge/mission
\xv pu ẽnõ i nu
\dialx PA
\xn j'ai des enfants
\dt 04/Nov/2021

\lx pu-
\dialx GOs PA
\sn 1
\is préfixe_sémantique
\ps PREF.CLF
\ge pied de qqch.
\dn d'arbre ou de toute plante ayant une souche et des racines
\ge tubercules
\ge bâtiment (qqch. qui a des fondations)
\xv nu coxada mwã ni pu-caai i yo
\dialx PA
\xn j’ai échoué au pied de ton pommier canaque
\sn 2
\is classificateur_numérique
\ps CLF.NUM
\ge n-pied de (plantes)
\xv pu-xè, pu-tru, pu-kò chaamwa, etc.
\dialx GOs
\xn un, deux, trois pieds de bananier
\dt 17/Feb/2025

\lx pû
\dialx GOs PA BO
\is plantes
\ps n
\ge haricot
\sc Phasoleus sp.
\scf Fabacées
\se pû-bilò
\sge haricot sauvage (à gousse biscornue)
\se pû-mii
\sge haricot violet
\se pû-zol
\sge haricot vert et violet sauvage (non comestible)
\se pò-pû
\sge gousse de haricot
\dt 17/Jan/2022

\lx pu gènè
\dialx GOs
\is caractéristiques_objets
\ps v
\ge avoir des couleurs
\ge coloré
\xv pu gènè hõbwòli-çö !
\xn ta robe a beaucoup de couleurs !
\dt 14/Sep/2021

\lx pu hêê
\dialx GOs
\is caractéristiques_objets
\ps v
\ge avoir un contenu
\xv pu hêê-dröö ? – Elo, pu hêê-dröö !
\xn il y a quelque chose dans la marmite ? – Oui, il y a quelque chose dans la marmite
\an kixa hêê-dröö
\at la marmite est vide
\dt 22/Feb/2025

\lx pu nee
\dialx GO PA BO
\is grammaire_manière
\ps v
\ge faire exprès
\xv kavwö nu pu nee
\dialx PA
\xn je n'ai pas fait exprès
\dt 26/Feb/2023

\lx pu nye
\dialx GOs
\va po nye
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge du fait que ; parce que
\xv pu nye za maza nõõli xo je nye êmwê
\xn parce que c'est la première fois qu'elle voit un homme
\xv ma nhye za kòi-nu na ênè avwõnõ, po nhye nu za a khila-je
\xn la raison pour laquelle je n'étais pas ici à la maison, c'était parce que je suis parti la chercher
\dt 04/Nov/2021

\lx pu pai
\dialx GOs
\is caractéristiques_objets
\ps v
\ge avoir des tubercules
\dt 04/Jun/2018

\lx pu puńõ
\dialx GOs
\va pu paxa-nõ
\dialx GOs
\is caractéristiques_objets
\ps v.stat.
\ge avoir un fond
\dt 22/Mar/2023

\lx pu pwò
\dialx GOs
\is caractéristiques_objets
\ps v
\ge avoir des fruits
\xv e za pu pwò, mãã ni ? – Ô, e za pu pwò
\xn y a-t-il des fruits (dans) ce manguier ? – Oui, il y a des fruits
\dt 22/Feb/2025

\lx pu zòò
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge difficile
\dn (lit. il y a des difficultés)
\an kixa zòò
\at facile
\dt 09/Feb/2025

\lx pua
\dialx PA
\is plantes_processus
\ps v
\ge produire ; donner (des fruits, tubercules)
\xv la u pua mãã
\dialx PA
\xn les manguiers sont chargés de fruits/donnent en abondance
\dt 22/Feb/2025

\lx puãgo
\dialx GOs
\va pwãgo
\dialx GO(s)
\is mollusque
\ps n
\ge coquillage
\dt 29/Jan/2019

\lx pubu dili
\dialx GOs PA BO
\is terrain_terre
\ps n
\ge poussière de la terre
\dt 16/Aug/2021

\lx pu-bwakira-me
\dialx GOs
\va pu-bwakitra-me
\is corps
\ps n
\ge sourcils
\dt 24/Jan/2019

\lx pu-bwa-n
\dialx BO PA
\is corps
\ps n
\ge barbe
\ge moustache
\ge cheveux
\xv kul-du mwã pu-bwa-n kai-n
\dialx PA
\xn ses cheveux tombent sur son dos
\dt 13/Jan/2022

\lx pu-bwò
\dialx GOs
\va pu-bò
\dialx PA BO
\is corps_animaux
\ps n
\ge poils de roussette
\dt 29/Mar/2022

\lx puçee
\dialx GOs
\va puuye, puuce
\dialx BO
\is danse
\ps n
\ge pilou (danse)
\dn accompagné de percussion et cri |lx{ae, ae!}
\ge lieu de danse |dialx{BO}
\dt 08/Feb/2025

\lx puçiu
\ph pudʒiu
\dialx GOs
\is coutumes_objet
\is habillement
\ps n
\ge affaires (de qqn.)
\ge biens personnels d'un défunt
\dn remis à sa parenté maternelle
\dt 17/Feb/2025

\lx puçò
\ph puʒɔ
\dialx GOs
\va puyòl, puçòl
\ph pujɔl, puʒɔl
\dialx WE PA BO
\is préparation_aliments
\ps v
\ge cuisine (faire la) ; cuisiner
\xv kavwö e puço gèè
\dialx GOs
\xn la grand-mère n'a pas fait la cuisine
\se mwa-puyòl
\dialx PA
\sge cuisine
\se aa-puyòl
\dialx PA
\sge cuisinier
\dt 03/Feb/2019

\lx pudi-gò
\dialx GOs
\va puding-gò
\dialx PA BO
\is plantes_partie
\ps n
\ge nœud de bambou
\dt 26/Aug/2021

\lx pu-döölia
\dialx GOs
\is plantes
\ps n
\ge épineux
\xv e pu-döölia cee
\xn c'est un arbre épineux
\dt 08/Nov/2021

\lx pu-drõgò
\dialx GOs
\va pu-dõgò
\dialx PA
\is maison
\ps n
\ge masque ; chambranles sculptés
\xv pu-drõgò ni phwèè-mwa Treã-ma
\dialx GOs
\xn les gardiens de la porte du Grand Chef
\dt 24/Feb/2025

\lx pue
\dialx GOs
\is interaction
\ps v
\ge respecter ; honorer
\se thu puu
\sge montrer du respect
\xv e vhaa pue-je
\dialx GOs
\xn il se vante ; il parle respectueusement de lui-même en s'accordant de l'importance
\xv e zo na çö pue èvwööni-çö
\dialx GOs
\xn il faut respecter ton oncle maternel
\ng v.t. |lx{pueli, puuli}
\dt 22/Feb/2025

\lx pui
\ph pu.i
\dialx GOs PA
\va phui-n
\dialx BO
\sn 1
\is topographie
\ps n
\ge hauteur
\xv mwa na bwa pui
\dialx GOs
\xn une maison sur une hauteur
\xv ge ni pui
\dialx PA
\xn elle est sur une hauteur (maison)
\sn 2
\is corps
\ps n
\ge bosse (sur la tête)
\dt 09/Jan/2022

\lx pû-kai-ã
\dialx GOs
\is corps
\ps n
\ge rein
\xv pû-kai-nu
\dialx GOs
\xn mon rein (lit. haricot-dos)
\dt 21/Dec/2021

\lx pu-ko
\dialx GOs
\is oiseau
\ps n
\ge plume de poule
\dt 29/Jan/2019

\lx pu-kòò
\dialx GOs
\va pu-kòò-n
\dialx BO
\is corps
\ps n
\ge hanche
\dn (lit. la base des jambes)
\nt selon Corne
\dt 09/Feb/2025

\lx pulòn
\dialx PA
\is insecte
\ps n
\ge punaise (de lit, bois)
\dt 29/Jan/2019

\lx pum-a mèè
\dialx GOs WEM WE BO PA
\is santé
\ps n
\ge évanouissement ; évanoui
\xv pum (a) mèè-n
\dialx PA
\xn il est inconscient (lit. il a de la fumée dans les yeux)
\cf burò la mee-je
\dialx GOs
\ce il est évanoui (lit. ses yeux sont dans l'obscurité)
\dt 10/Jan/2022

\lx pu-mee
\dialx GOs
\is corps
\ps n
\ge cils ; sourcils
\xv pu-mee-ã
\xn nos cils (lit. poil-œil-nos)
\xv pu-mee-n
\dialx PA
\xn cils ; sourcils (lit. poils-yeux)
\dt 22/Feb/2025

\lx pu-mèni
\dialx GOs PA
\is oiseau
\ps n
\ge plume d'oiseau
\dt 25/Aug/2021

\lx pu-mwa
\is maison
\dialx GOs
\ps n
\ge arrière de la maison
\cf kaça mwa
\ce arrière de la maison
\dt 05/Nov/2021

\lx pumhããmã
\dialx GOs
\is caractéristiques_objets
\ps v
\ge léger
\dt 22/Mar/2023

\lx pu-n ma
\dialx BO
\is grammaire_préposition
\ps PREP
\ge à cause de ; parce que
\xv pu-n ma kia po ênêda po hu-da
\xn parce qu'il n'y a rien un peu plus haut
\dt 19/Jan/2025

\lx pune
\dialx GOs
\va puni
\dialx PA
\is grammaire_conjonction
\ps CNJ ; PREP
\ge parce que ; à cause de ; car
\xv kô-raa mo a pune dree
\dialx GO
\xn on ne peut pas partir à cause du vent
\xv mii mwã dili, puni nye e mwani mii mwã je-nã
\dialx GO
\xn la terre est devenue rouge, du fait que c'est de l'argent rouge cela
\xv e thô pune nu
\dialx GOs
\xn il est en colère (lit. fermé) à cause de moi
\xv nu Teã-ma pu na novwö pu-ny, ca la nee vwo pu-dõgò
\dialx PA
\xn je suis le Grand Chef parce que mes plumes, ils en font des plumes pour (orner) les masques
\xv haxe nu mòlò mwã xa puni ne ɉe-nã êgu
\dialx PA
\xn mais j'ai eu la vie sauve grâce à cette personne
\se pune lò
\dialx GOs
\sge à cause d'eux
\se puni yo
\dialx PA
\sge à cause de toi
\ng |lx{puni/pune} + pronom
\cf ui
\ce envers ; à cause de ; à propos de
\dt 24/Feb/2025

\lx pune da ?
\dialx GOs PA
\va puna da?
\dialx BO PA
\is grammaire_interrogatif
\ps INT
\ge pourquoi ? ;  pour quelle raison ?
\xv yo khõbwe cu Teã-ma puna da?
\dialx PA
\xn pourquoi dis-tu que tu es le Grand Chef ?
\xv pune da u yu gi ?
\dialx BO
\xn pourquoi pleures-tu ?
\dt 21/Feb/2025

\lx pu-ni
\ph puɳi
\dialx GOs
\va pu-ning
\dialx PA
\is maison
\ps n
\ge fond de la maison ronde
\ge base du poteau
\dn là où se trouvent les poteaux |lx{ning}
\xv i tabwa ni pu-ning
\dialx PA
\xn elle est assise au fond de la maison
\dt 05/Jan/2022

\lx puńõ
\ph punɔ̃
\dialx GOs BO PA
\is configuration
\ps n
\ge fond
\xv puńõõ-kee
\dialx GO
\xn le fond du panier
\xv punõõ-keel
\dialx PA
\xn le fond du panier
\dt 22/Mar/2023

\lx pu-nõõ
\ph puɳɔ̃:
\dialx GOs
\va pu-nõõ-n
\dialx PA
\is corps
\ps n
\ge nuque
\xv puu-nõõ-je
\dialx GOs
\xn la base de son cou
\xv puu-nõõ-n
\dialx PA
\xn la base de son cou
\dt 12/Jan/2022

\lx puńõõ-dröö
\ph punɔ̃:ɖω:
\dialx GOs
\va punõõ-doo
\dialx PA
\is configuration
\ps n
\ge fond de la marmite
\dt 22/Mar/2023

\lx puńõõ-we
\ph punɔ̃:we
\dialx GOs BO PA
\is configuration
\ps n
\ge fond de l'eau
\ge lit de la rivière |dialx{BO}
\dt 22/Mar/2023

\lx pu-nye da
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge parce que ; à cause de
\xv ma nye za kòi-nu na ênè avwõnõ, pu-nye da a khilaa-je
\dialx GO
\xn la raison pour laquelle je n'étais pas là à la maison, c'est parce que je suis parti la chercher
\dt 22/Feb/2025

\lx pu-pee-ã
\dialx GOs
\va pu-vee
\dialx GO(s)
\is corps
\ps n
\ge hanche
\xv pu-pee-nu
\dialx GOs
\xn ma hanche (lit. la base des cuisses)
\xv pu-pee-n
\dialx BO
\xn sa hanche (lit. la base des cuisses)
\dt 24/Jan/2019

\lx pu-pii
\dialx GOs
\is crustacés
\ps n
\ge crabe 'double peau'
\dn cette carapace est décollée mais n'est pas encore tombée
\cf a-pii
\ce crabe mou (dont la carapace est tombée)
\dt 22/Aug/2021

\lx pu-phwa
\dialx GOs
\va pu-phwa
\dialx BO PA
\is corps
\ps n
\ge barbe ; moustache
\xv pu-phwa-n
\dialx PA
\xn sa barbe/moustache
\xv pu-phwa-ny
\dialx BO
\xn ma barbe
\dt 03/Feb/2019

\lx puradimwã
\ph puɽa'dimwã
\dialx GOs PA
\sn 1
\is oiseau
\ps n
\ge tourterelle verte
\sc Chalcophaps indica
\scf Columbidés
\gb Emerald Dove
\sn 2
\is société_organisation
\ps n
\ge messager de la chefferie
\dt 15/Sep/2021

\lx pure
\dialx GOs BO
\va pyèro
\dialx PA BO
\is oiseau
\ps n
\ge "merle noir" ; stourne calédonien
\sc Aplonis striatus striatus
\scf Sturnidés
\gb New Caledonian starling
\dt 22/Feb/2025

\lx puri
\dialx BO
\is reptile_marin
\ps n
\ge serpent de mer
\dt 29/Jan/2019

\lx putrakou
\ph pu'ɽakou
\dialx GOs
\va purakou
\dialx GO(s)
\is poisson
\ps n
\ge carangue (taille adulte)
\dt 26/Aug/2021

\lx putrumi
\ph pu'ɽumi
\dialx GOs
\va purumi
\ph pu'rumi
\dialx PA BO
\is insecte
\ps n
\ge fourmi (petite et rouge)
\bw fourmi (FR)
\dt 08/Oct/2021

\lx putruna
\ph puɽuɳa
\dialx GOs
\is habillement
\ps n
\ge manou (hommes)
\dt 25/Jan/2019

\lx puu
\hm 1
\dialx GOs PA BO
\va pu
\dialx GO(s)
\va puu-n
\dialx BO PA
\va puxu-n
\dialx BO
\sn 1
\is plantes_partie
\ps n
\ge pied ; tronc
\ge base
\xv a mwã xò je ra hòva-du mwã ni puu-n
\dialx PA
\xn il s’approche et arrive en bas au pied (de l’arbre)
\sn 2
\is société_organisation
\ps n
\ge ancêtres ; origine
\se puu-ã
\sge notre origine/Dieu
\sn 3
\is fonctions_intellectuelles
\ps n
\ge source ; cause
\xv puu xo la wòvwa
\dialx GOs
\xn la raison de leur dispute
\se pu-neda ?
\sge pourquoi ?
\se kixa puu
\dialx GOs
\sge il n'y a pas de raison ; sans cause
\se kia puu-n
\dialx PA
\sge sans raison ; sans cause
\se puu fa
\dialx GA
\sge l'origine d'une affaire
\cf pune
\ce parce que ; car
\et *puqu(n)
\el POc
\dt 22/Feb/2025

\lx puu
\hm 2
\dialx GOs
\va pulu
\dialx BO
\is navigation
\ps n
\ge perche pour pousser la pirogue
\ge gaffe ; barre
\dt 25/Aug/2021

\lx puu-cee
\dialx GOs PA BO
\is arbre
\ps n
\ge collet de l'arbre
\ge pied ; tronc ; base
\dt 08/Feb/2025

\lx puu-mwa
\dialx GOs PA BO
\is maison
\ps n
\ge mur de la maison (tous les murs)
\dt 05/Nov/2021

\lx puuńô
\is discours
\ph p'u:nõ
\dialx GOs
\va puunòl
\dialx PA BO WEM
\ps v
\ge discourir ; faire le discours de coutume ; haranguer
\ge parler pour se réconcilier ; paix (faire la) ; faire un discours coutumier
\dt 15/Aug/2021

\lx puuvwoo
\dialx GOs
\is poisson
\ps n
\ge tarpon à filament
\sc Megalops cyprinoides
\scf Megalopidés
\dt 27/Aug/2021

\lx puvwu
\dialx GOs
\va pupu-n, puvwu-n
\dialx BO
\is corps
\ps n
\ge nuque
\xv puvwu-nu
\dialx GOs
\xn ma nuque
\xv puvwu-ny
\dialx BO PA
\xn ma nuque
\dt 03/Feb/2019

\lx puxãnu
\ph puɣɛ̃ɳu
\dialx GOs
\va poxãnu, pwããnu
\dialx GO(s)
\va poxònu, poonu
\dialx BO
\is sentiments
\is interaction
\ps v ; n
\ge amour ; compatir ; avoir pitié de
\ge aimer ; affectionner
\xv mõ puxãnu-ni mwã êne-ò
\dialx GO
\xn nous vénérons ce passé
\xv nu ma poxònu-m
\dialx BO
\xn j'ai très pitié de toi
\se puxãnu-ayu
\dialx GOs
\sge la grâce
\ng v.t. |lx{puxãnu-ni}
\dt 05/Jan/2022

\lx puxa-treã
\dialx GOs
\is société_organisation
\ps n
\ge clan cadet ; cadet de la chefferie
\dt 19/Aug/2021

\lx puxu-n
\dialx PA
\va puvwu-n
\dialx PA
\is maison
\ps n
\ge fond (de la maison)
\xv jo na mwa puxu-n
\xn pose-le au fond (de la maison)
\dt 25/Dec/2021

\lx puya
\dialx BO
\is préparation_aliments
\ps v
\ge vider le four enterré
\nt selon Corne
\dt 27/Mar/2022

\lx puyai
\dialx PA
\is bananier
\ps n
\ge bananier ("banane chef")
\dt 25/Mar/2022

\lx pwa
\hm 1
\dialx GOs
\is poisson
\ps n
\ge barracuda
\sc Sphyræna barracuda
\scf Sphyrénidés
\dt 27/Aug/2021

\lx pwa
\hm 2
\dialx GOs
\va pwal
\dialx PA
\is temps_atmosphérique
\ps n
\ge pluie
\se we pwal
\dialx PA
\sge eau de pluie
\dt 16/Aug/2021

\lx pwa
\hm 3
\dialx GOs PA BO
\va pwau
\dialx BO
\is grammaire_locatif
\ps ADV.LOC
\ge dehors ; extérieur
\xv a pwa !
\xn sors !
\xv na pwa
\xn au dehors
\xv e a-du pwa
\xn il est sorti dehors (de la maison)
\dt 22/Mar/2023

\lx pwaa
\hm 1
\dialx GOs PA BO
\sn 1
\is action_plantes
\ps v
\ge casser (bois en pliant)
\ge cueillir (en cassant pour les fleurs)
\ge replier la tige de l'igname
\dn sur elle-même quand elle dépasse la hauteur du tuteur
\xv e pwaa kui
\dialx GOs
\xn elle replie (la tige de) l'igname
\se pwaa mu-cee
\sge cueillir des fleurs
\se pwaa ce-bòn
\dialx WEM
\sge casser du bois pour la nuit
\se pwaa aamu
\dialx PA
\sge recueillir le miel (en cassant la branche de l'arbre sur laquelle se trouve la ruche)
\sn 2
\is action_corps
\ps v
\ge presser dans la main
\ng v.t. |lx{pwaale}
\et *posi
\el POc
\dt 22/Feb/2025

\lx pwaa
\hm 2
\dialx PA
\va phwaal
\dialx BO
\is déplacement
\ps v
\ge faire demi-tour ; revenir de ; rentrer de
\ge tourner
\xv bî pwaa-bin Numia
\dialx PA
\xn nous rentrons/sommes rentrés de Nouméa
\xv phwaal-ò phwaal-mi
\dialx BO
\xn il tourne de-ci de-là
\dt 12/Nov/2021

\lx pwaa
\hm 3
\dialx GOs
\is mollusque
\ps n
\ge grisette
\dt 29/Jan/2019

\lx pwaaci
\ph pwa:cɨ
\dialx GOs
\va pwayi
\dialx PA BO
\is préparation_aliments
\ps v
\ge éplucher (manioc) ; peler (avec les doigts)
\dn des bananes, tubercules cuits
\xv i pwayi la waalei wo hovwo
\dialx PA
\xn elle épluche les ignames pour manger
\cf thebe
\ce éplucher ; peler (avec un couteau, igname ou taro cru)
\dt 22/Feb/2025

\lx pwaade
\dialx BO
\is poisson
\ps n
\ge hareng
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx pwaadi
\dialx GOs
\is action
\ps v
\ge arrêter ; interrompre
\xv e u pwaadi phwa-je
\xn elle l'a sevré
\xv ba-pwaadi phwa-je
\xn plante qui permet de sevrer un nourrisson (arrête l'allaitement)
\dt 29/Jan/2019

\lx pwaang
\dialx PA
\is corps
\ps n
\ge joue
\dt 24/Jan/2019

\lx pwaala
\dialx GOs PA BO
\is navigation
\ps v
\ge naviguer (à la voile) ; voguer
\se wõ pwaala
\sge bateau à voile
\et *parau, *palauR
\eg flotte, voyager, bateau
\el POc
\ea Blust
\dt 08/Feb/2025

\lx pwaale
\dialx GOs PA BO
\is action_plantes
\ps v.t
\ge casser (bois en pliant) ; couper (en cassant)
\ge cueillir (en cassant la tige des fleurs)
\xv u ru pwaale cee
\dialx BO
\xn il va casser le bois
\dt 26/Mar/2022

\lx pwaalu
\dialx GOs PA BO
\is caractéristiques_objets
\ps v.stat.
\ge lourd ; grave
\ge cher |dialx{PA, BO}
\se ole pwaalu
\dialx PA
\sge merci beaucoup
\se thu pwaalu
\dialx GO PA
\sge respecter; honorer
\xv lha nõ pwaalu-ni
\dialx PA
\xn considérer qqch. avec respect
\ng v.t. |lx{V-pwaalu-ni} dans une construction verbale complexe
\an (h)aom
\at léger
\dt 21/Feb/2025

\lx pwããnu
\ph pwɛ̃:ɳu
\dialx GOs
\va poxããnu, puxãnu
\dialx GO(s)
\is sentiments
\ps v ; n
\ge aimer ; amour
\xv nu mã pwããnuu-je
\dialx GOs
\xn je l'aime beaucoup
\xv e pwãnuu ẽnõ
\dialx GOs
\xn elle aime cet enfant
\dt 18/Aug/2021

\lx pwaawe
\dialx GOs PA WEM BO
\is préparation_aliments
\ps v
\ge fumer (poisson)
\xv lhi thu pwaawe nõ
\xn ils fument (font le fumage) du poisson
\se ba-pwaawe
\sge fumoir
\dt 22/Mar/2023

\lx pwabwaluni
\ph pwabwaluɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge tromper (se) ; faire une erreur (dans ses gestes)
\xv nu pwabwaluni
\dialx GOs
\xn je me suis trompé
\dt 22/Mar/2023

\lx pwabwani
\ph pwabwaɳi
\dialx GOs WEM
\va p(w)abwaning
\dialx PA BO
\is maison
\ps n
\ge poutre maîtresse
\ge panne sablière
\dn elle supporte la toiture des maisons carrées ou rondes
\cf jebo
\dt 12/Jan/2022

\lx pwang
\hm 1
\dialx BO
\is cultures
\ps n
\ge fossé d'écoulement entre les massifs de culture
\dn se trouve au bord du massif d'igname (selon Corne)
\dt 22/Oct/2021

\lx pwang
\hm 2
\dialx BO
\is igname
\ps n
\ge igname du chef (violette)
\cf gu-kui
\ce igname du chef
\cf pwalamu
\ce igname du chef
\dn variété blanche, selon Dubois)
\dt 17/Feb/2025

\lx pwai
\dialx GOs
\is nourriture_tabac
\ps n
\ge cigarette
\dt 27/Jan/2019

\lx pwai-ce
\ph pwaicɨ
\dialx GOs
\is nourriture_tabac
\ps n
\ge pipe
\dt 02/Jan/2022

\lx pwaiòng
\dialx BO PA
\va pwayòng
\dialx BO PA
\is igname
\ps n
\ge sillon du massif d'ignames
\cf nõ-khia
\ce billon
\cf kê
\ce billon ; champ
\dt 22/Feb/2025

\lx pwaixe
\dialx GOs PA
\va pwaike
\dialx GO
\is caractéristiques_objets
\ps n
\ge chose
\xv po-tru pwaixe
\dialx GOs
\xn deux choses
\xv bî pweexu pwaixe na ni vhaa zuanga
\dialx GOs
\xn nous deux discutons de choses sur la langue zuanga
\xv kixa pwaixe le
\dialx GOs
\xn il n'y rien (à faire)
\se pwaixe haze
\sge autre chose
\dt 15/Mar/2023

\lx pwajã
\dialx PA BO [Corne]
\is arbre
\ps n
\ge arbre
\dn ses graines sont utilisées comme teinture rouge pour les poils des masques
\sc Macaranga
\scf Euphorbiacées
\dt 08/Feb/2025

\lx pwaji
\ph pwaɲɟɨ
\dialx GOs PA BO
\va pwaaje
\dialx PA BO
\is crustacés
\ps n
\ge crabe de vase de palétuvier
\se pwaji nhyatru
\dialx GO
\va pwaji nhyaru
\dialx PA
\sge crabe mou
\cf cî, ji(m)
\dt 10/Jan/2022

\lx pwajiò
\dialx PA BO [Corne]
\is oiseau
\ps n
\ge rossignol à ventre jaune
\sc Eopsaltria flaviventris
\scf Eopsaltriidés
\dt 27/Aug/2021

\lx pwajò
\dialx GOs
\va pwajòl
\dialx BO
\is religion
\ps n
\ge lieu sacré
\ge présent déposé devant l'autel
\dn selon [Corne]
\ge autel près de la case
\dn selon [Corne, Dubois]
\xv pwajò-ã
\dialx GOs
\xn notre lieu sacré
\dt 09/Feb/2025

\lx pwal
\dialx PA BO
\va kole pwa
\dialx GO
\is temps_atmosphérique
\ps v ; n
\ge pleuvoir ; pluie
\xv i õgin a pwal
\dialx BO
\xn il a cessé de pleuvoir
\se we-pwal
\sge eau de pluie
\se pwal ni kòu
\sge grandes pluies
\dt 13/Aug/2021

\lx pwalamu
\dialx BO
\is igname
\ps n
\ge igname du chef
\cf gu-kui
\ce igname du chef
\cf pwang
\ce igname du chef
\dn variété violette, selon Dubois
\nt non vérifié
\dt 17/Feb/2025

\lx pwala-mwaji
\ph ,pwala'mwãɲɟi
\dialx GOs
\va pwali-mwaji-n
\dialx PA
\sn 1
\is temps
\ps n
\ge temps long
\sn 2
\is caractéristiques_personnes
\ps LOCUT. nominale
\ge temps long (mettre un) à faire qqch.
\ge lenteur
\xv pwala-mwaji-nu vwö nu mõgu
\dialx GOs
\xn j'ai mis longtemps à travailler (lit. mon temps long à)
\xv pwala-mwaji-je vwö e ã-mi
\dialx GOs
\xn il a mis du temps pour venir
\xv e za mõgu (vwö) pwala-mwaji-je êńa
\dialx GOs
\xn il a longtemps travaillé là
\xv e mããni xo pwala-mwaji-je ẽnõ ã
\dialx GOs
\xn cet enfant dort et cela fait longtemps
\dt 21/Feb/2023

\lx pwalee
\dialx GOs
\is action_corps
\ps v
\ge étaler (natte)
\ge étendre (natte)
\ge déployer (membre)
\ge ouvrir (livre)
\xv e pwalee thrõ
\xn elle étale la natte
\xv e kô-pwalee kòò-je
\xn elle est allongée les jambes étendues
\xv e tree-pwalee kòò-je
\xn elle est assise les jambes étendues
\cf zhugi, zugi
\ce replier
\dt 05/Jan/2022

\lx pwali
\dialx BO PA
\sn 1
\is caractéristiques_objets
\ps v.stat.; n
\ge long(ueur) ; haut(eur)
\ge grand(eur) ; taille
\xv ra u pwali yala-m
\dialx BO
\xn ton nom est long
\xv pe-pwali-li
\dialx PA
\xn ils ont la même taille
\sn 2
\is temps
\ps v.stat. ; ADV
\ge longtemps
\xv kavwö pwali
\dialx PA
\xn pas longtemps après
\dt 03/Feb/2025

\lx pwali mwaji-n
\dialx PA
\is grammaire_temps
\ps LOCUT. nominale
\ge il y a longtemps
\ge longtemps (mettre) à faire quelque chose; faire lentement
\xv pwali mwaji-n ?
\dialx PA
\xn combien de temps ?
\dt 30/Mar/2022

\lx pwamwãgu
\dialx PA BO
\va pwamwègu
\dialx BO
\sn 1
\is société_organisation
\ps n
\ge place de réunion dans le village ; emplacement des hommes (dans une réunion)
\ge assemblée
\cf pwamwã-ròòmwa
\ce emplacement des femmes
\sn 2
\is discours
\ps v
\ge discuter
\xv la pwamwãgu ni da ?
\dialx PA
\xn de quoi parlent-ils ?
\dt 15/Sep/2021

\lx pwãmwãnu
\dialx PA
\va pobwanu
\dialx BO
\is richesses
\ps n
\ge monnaie
\dn de valeur moindre que |lx{yòò} et |lx{weem}, mais de valeur équivalente à |lx{yhalo} (selon Charles Pebu-Polae) ; Dubois : 1 pobwanu de 5 m vaut 10 fr) ; hiérarchie des valeurs : yòò > weem > yhalo
\cf dopweza ; weem; yòò ; yhalo
\dt 05/Jan/2022

\lx pwamwã-toomwã
\ph pwamwɛ̃ro:mwɛ̃
\dialx PA BO
\va pavo-romwa
\dialx BO (Dubois)
\is société_organisation
\ps n
\ge emplacement des femmes ; maison des femmes
\cf pwamwãgu
\ce emplacement des hommes (réunion)
\dt 31/Dec/2021

\lx pwãû
\dialx GOs BO
\va gò-pwãû
\dialx PA
\is musique
\ps n
\ge bambou
\se gò-pwãû
\sge sorte de bambou qui sert de percussion
\dt 08/Feb/2025

\lx pwawa
\dialx WEM WE BO PA
\is grammaire_modalité
\ps MODAL
\ge difficile ; impossible
\xv e pwawa xa me tooli kun je-nã
\dialx PA
\xn il nous est impossible de trouver cette place (façon de faire)
\xv pwawa na i ru pe-me-ã
\dialx BO
\xn il est impossible qu'il nous suive
\xv pwawa ne jö whili-mi lã-nã pòi-m ?
\dialx WEM
\xn ne peux-tu amener tes enfants ?
\dt 18/Jan/2025

\lx pwawaa
\dialx GOs
\va pwawa
\dialx PA BO
\va pwaaò-n
\dialx BO
\is corps
\ps n
\ge joue
\xv pwawa-n
\dialx PA
\xn sa joue
\xv pwaaò-n
\dialx BO
\xn sa joue
\se cabi pwawa-n
\sge gifler
\dt 19/Feb/2025

\lx pwawaale
\dialx GOs
\va pwapale
\dialx GO(s)
\is cultures
\ps n
\ge maïs
\dt 26/Jan/2019

\lx pwawalèng
\dialx BO
\is arbre
\ps n
\ge arbre
\sc Ricinus communis
\scf Euphorbiacées
\nt selon Corne
\dt 29/Mar/2022

\lx pwawe
\dialx BO [BM]
\is échanges
\ps v
\ge récompenser ; payer ; rétribuer
\xv yu ne wu pwawe i nu ?
\dialx BO
\xn tu l'as fait pour me récompenser ?
\dt 23/Aug/2021

\lx pwaxa tree
\dialx GOs
\is temps_découpage
\ps LOCUT
\ge toute la journée
\dt 28/Jan/2019

\lx pwaxilo
\dialx PA BO
\va pwagilo
\dialx PA BO
\va pwagilo
\dialx arch.
\is maison
\ps n
\ge porte (de maison)
\cf phwe-mwa
\dialx GOs
\cf phwee-mwa
\dialx PA
\dt 10/Jan/2022

\lx pwazi
\dialx GOs
\is préparation_aliments
\ps v
\ge écorcher (s')
\dt 27/Jan/2018

\lx pwe
\hm 1
\dialx GOs BO
\is pêche
\ps v ; n
\ge pêcher à la ligne ; ligne (de pêche)
\se pao pwe
\dialx BO
\sge lancer la ligne
\se khô-pwe
\dialx BO
\sge ligne
\se pwò-pwe
\dialx BO
\sge hameçon
\ng v.t. |lx{pweni}
\dt 05/Jan/2022

\lx pwè
\hm 2
\dialx GOs
\sn 1
\is fonctions_naturelles
\ps v
\ge accoucher ; enfanter
\sn 2
\is étapes_vie
\ps v
\ge naître
\xv e mhaza pwè
\xn il vient de naître; nouveau-né (seul mot pour nouveau-né)
\et *potu
\eg apparaître
\el POc
\dt 22/Mar/2023

\lx pwê
\dialx PA
\is insecte
\ps n
\ge libellule
\dt 29/Jan/2019

\lx pwebae
\dialx BO
\is temps_saison
\ps n
\ge époque où les ignames commencent à mûrir
\dn de février à mars
\nt selon Dubois ; non vérifié
\cf wogama, maxal
\dt 08/Feb/2025

\lx pwebwe
\dialx GOs BO
\is mouvement
\ps v
\ge tourner autour
\xv za pwebwe (xo) ne
\dialx GO
\xn les mouches tournent autour
\xv la pwebwe xo aamu
\dialx BO
\xn les mouches tournent autour [BM]
\dt 09/Aug/2023

\lx pwedaou
\dialx BO
\is bananier
\ps n
\ge bananier (clone)
\nt selon Dubois, non vérifié
\dt 25/Mar/2022

\lx pwede
\dialx GOs
\is guerre
\ps v
\ge cerner ; encercler
\xv lha pwede-je
\xn ils l'ont cerné
\dt 21/Dec/2021

\lx pwedee
\dialx GOs
\is déplacement
\ps n
\ge traces de bois trainés
\dt 27/Jan/2018

\lx pwe-ẽnõ
\dialx BO
\is étapes_vie
\ps n
\ge nourrisson
\nt selon Corne
\dt 26/Mar/2022

\lx pwêng
\dialx PA BO
\is plantes
\ps n
\ge herbe
\dn herbe dont la décoction des feuilles sert à la première purge des bébés juste après la naissance et avant l'allaitement
\dt 18/Feb/2019

\lx pwe-kewang
\dialx BO
\is topographie
\ps n
\ge ravin ; précipice
\dt 28/Jan/2019

\lx pwen
\dialx BO
\is corps
\ps n
\ge abdomen
\dt 24/Jan/2019

\lx pwepweede nhe
\dialx GOs
\is navigation
\ps v
\ge virer de bord (par vent arrière)
\cf pweede
\ce tourner
\dt 25/Aug/2021

\lx pweralo
\dialx BO
\is temps_saison
\ps n
\ge mois où l'on débrousse les plantations
\nt selon Dubois ; non vérifié
\dt 26/Mar/2022

\lx pweuna
\dialx PA BO
\is caractéristiques_personnes
\ps v
\ge gourmand ; glouton
\dt 26/Jan/2019

\lx pwevwenu
\dialx GOs
\va pwemwêning, pwepwêni (Dubois)
\dialx BO
\is eau
\ps n
\ge tourbillon (sur un cours d'eau)
\dt 29/Jan/2019

\lx pwewee
\dialx GOs
\va pweween
\dialx PA
\va phweween
\dialx BO
\sn 1
\is déplacement
\ps v
\ge revenir (en sens inverse) ; se retourner
\ge contourner
\se a-pwewee
\sge revenir ; retourner en sens inverse
\xv i phweween xo Kaavo
\dialx BO
\xn Kaavo se retourne
\sn 2
\is action_corps
\ps v
\ge fouler (se) ; tordre |dialx{BO}
\dt 22/Feb/2025

\lx pweweede
\dialx GOs BO PA
\is mouvement
\va pepweede; pwepwe
\dialx GO
\ps v
\ge tourner ; retourner ; poser à l'envers ; retourner un objet (sur le même plan)
\ge changer ; traduire
\ng réduplication de |lx{pweede}
\gt tourner
\dt 06/Jan/2022

\lx pwe-ya
\dialx GOs
\va pwe-yal
\dialx BO
\is maison
\ps n
\ge rangée de paille sur un toit
\dt 22/Mar/2023

\lx pweza
\dialx GO PA
\is bananier
\ps n
\ge bananier
\ge "banane de la chefferie"
\dn selon [Charles Pebu-Polae]
\se dròò-pweza
\sge feuille de bananier
\se nõ-pweza
\sge champ de bananier
\cf cabeng, pòdi
\dialx PA
\ce clone de bananiers-chefs
\dt 09/Feb/2025

\lx pwii-khai
\dialx GOs BO
\is pêche
\ps n
\ge senne ; filet pour cerner
\dn filet que l'on déploie en le tirant
\cf khai
\ce tirer
\dt 09/Feb/2025

\lx pwiò
\dialx GOs PA
\va puio, puiyo, pwiyo
\dialx BO
\is pêche
\ps n
\ge filet ; senne
\se kha pwiò
\sge pêcher au filet (en se déplaçant)
\et *pukot
\eg filet
\el POc
\dt 30/Dec/2021

\lx pwi-pawe
\dialx GOs PA
\va pwi-pao
\dialx GO(s)
\is pêche
\ps n
\ge filet épervier
\dt 26/Aug/2021

\lx pwiri
\ph pwiɽi
\dialx GOs PA
\va pwitri
\dialx GOs
\va pwiiri
\dialx BO
\sn 1
\is oiseau
\ps n
\ge perruche ; Nymphique cornue
\sc Eunymphicus cornutus
\scf Psittacidés
\sn 2
\is oiseau
\ps n
\ge Loriquet à diadème
\sc Charmosyna diadema
\scf Psittacidés
\dt 30/Dec/2021

\lx pwiwii
\dialx GOs WEM PA BO
\is oiseau
\ps n
\ge notou
\sc Ducula goliath
\scf Columbidés
\dt 27/Aug/2021

\lx pwiya
\dialx BO
\is préparation_aliments
\ps v
\ge déterrer (le four)
\dt 26/Jan/2019

\lx pwò
\dialx GOs
\va pò
\dialx GO(s)
\va pwòn
\dialx PA BO
\va pòn, pô
\dialx BO PA
\is reptile_marin
\ps n
\ge tortue de mer
\et *poɲu
\eg tortue
\el POc
\dt 29/Jan/2019

\lx pwòk
\dialx BO
\is navigation
\ps n
\ge voile (petite) de pirogue
\nt selon Corne ; non vérifié
\cf nhe
\ce voile
\dt 27/Mar/2022

\lx pwò-mhãã
\dialx GOs
\is reptile_marin
\ps n
\ge tortue verte
\dt 29/Jan/2019

\lx pwone
\dialx WEM
\va ponèn
\dialx PA
\va pwònèn
\dialx BO
\is grammaire_quantificateur_mesure
\ps v.stat. ; QNT
\ge petit ; mince ; un peu
\xv taabwa mwã nhye pwone
\dialx WEM
\xn il en reste un peu
\xv i hovo pwònèn nai inu
\dialx BO
\xn il mange moins que moi (Corne)
\dt 16/Feb/2025

\lx pwono
\dialx GOs
\is pêche
\ps n
\ge barrage (sur la rivière pour la pêche)
\se thu pwono
\sge faire un barrage
\dt 20/Jan/2019

\lx pha
\dialx PA BO
\is vent
\ps v
\ge souffler (vent)
\dt 09/Mar/2023

\lx pha-
\dialx GO BO PA
\va phaa-, pa-
\is grammaire_causatif
\ps PREF.CAUS
\ge faire (faire)
\xv e pha-pheze-je kii-je
\dialx GO
\xn elle lui fait ceindre sa jupe/son manou
\dt 31/Dec/2021

\lx phaa
\dialx GOs BO PA
\sn 1
\is corps
\ps n
\ge poumon
\xv phaa-n
\dialx PA
\xn ses poumons
\sn 2
\is navigation
\ps n
\ge radeau ; flotteur
\xv phaa nee xo gò
\dialx GO
\xn un radeau fait en bambou
\xv phaa-m
\dialx PA
\xn ton radeau
\et *paRaq
\eg poumon
\el POc
\dt 21/Dec/2021

\lx phãã
\dialx GOs
\is poisson
\ps n
\ge "aiguillette"
\sc Tylosorus crocodilus crocodilus
\scf Belonidés
\dt 27/Aug/2021

\lx phãã
\dialx GOs
\va phê
\dialx GOs
\va phêng
\dialx PA
\va pang, phang
\dialx BO [BM]
\is plantes
\ps n
\ge brède ; laiteron
\dn sorte de pissenlit
\sc Sonchus oleraceus L.
\scf Astéracées
\se dròò-phãã, dròò-phê
\dialx GOs
\sge pissenlit
\dt 08/Feb/2025

\lx phaa-bi-ni
\ph pʰa:biɳi
\dialx GOs
\is action
\ps v
\ge dégonfler
\dt 02/Jan/2022

\lx phaa-butrõ
\ph ,pʰa:'buɽõ
\dialx GOs
\va pa-burõ
\dialx GO(s)
\is soin
\ps v
\ge baigner (enfant)
\xv e phaa-butrõ-ni ẽnõ
\dialx GOs
\xn il fait baigner l'enfant ; il donne un bain à l'enfant
\dt 22/Feb/2025

\lx phaa-ce-bwo
\ph pʰa:cɨbwo
\dialx GOs
\is feu
\ps v
\ge faire du feu (pour la nuit)
\se phaai ce-bwo
\sge allumer le feu pour la nuit
\se kô-pha-ce-bòn
\dialx WEM
\sge dormir auprès du feu (la nuit)
\dt 31/Dec/2021

\lx phaa-cii
\dialx GOs
\va phaa-çi
\dialx GO(s)
\is action_corps
\ps v
\ge épouiller ; chercher les poux
\xv e phaa-cii-ni bwaa-nu
\dialx GOs
\xn elle épouille ma tête
\xv çö a-da-mi phaa-cii-nu
\dialx GOs
\xn viens m'épouiller
\dt 13/Aug/2021

\lx phããde
\ph pʰɛ̃:de
\dialx GOs BO PA
\va phãde
\is action_corps
\ps v
\ge montrer
\ge présenter
\ge révéler
\xv la phããde vhaa
\dialx GO(s)
\xn ils annoncent une nouvelle
\xv la phããde fim
\dialx PA
\xn ils montrent un film
\xv la phããde jaa
\xn ils annoncent les nouvelles
\dt 11/Jan/2022

\lx phããde cii-phagòò
\dialx PA
\is interaction
\ps v
\ge faire acte de présence
\xv la phããde cii-phagòò
\dialx PA
\xn ils ont fait acte de présence
\dt 25/Aug/2021

\lx phããde kui
\dialx GOs
\is coutumes
\ps n
\ge fête des prémices des ignames
\dn (lit. montrer l'igname)
\dt 22/Oct/2021

\lx phaa-gò
\ph pʰa:ŋgɔ
\dialx GOs PA BO
\va phaa
\dialx GO(s)
\is navigation
\ps n
\ge radeau (en bambou)
\dt 02/Jan/2022

\lx phaamee
\dialx GOs PA
\is parenté
\ps n
\ge aîné (des enfants)
\xv phaamee pòi-nu
\dialx GOs
\xn l'aîné de mes enfants
\xv phaamee ẽnõ
\dialx PA
\xn l'aîné des enfants
\dt 27/Jan/2019

\lx phaa-puio
\dialx BO [BM]
\is pêche
\ps n
\ge flotteur du filet
\dt 26/Jan/2019

\lx phaa-tuuçò
\dialx PA BO
\va pha-ruyòng, pha-thuyòng
\dialx PA
\va phaxayuk
\dialx BO
\sn 1
\is plantes
\ps n
\ge plante ; graminée
\sc Polygonum subsessile
\scf Polygonacées
\sn 2
\is médecine
\ps n
\ge nom de la purge à base de plante "Polygonum subsessile" 
\dn selon [Corne]
\dt 16/Feb/2025

\lx phaa-udale
\dialx GOs
\is habillement
\ps v
\ge vêtir ; habiller qqn.
\xv e phaa-udale hõbwò i Kaavwo
\dialx GOs
\xn elle a habillé Kaavwo
\dt 29/Mar/2022

\lx phaavã
\dialx PA BO
\is feu
\ps n
\ge suie ; noir de fumée
\dt 26/Jan/2019

\lx phaavwi
\ph pʰa:βi
\dialx GOs
\is cultures
\ps v.t.
\ge désherber ; couper l'herbe ; débrousser
\xv nu phaavwi pomõ-je
\xn je débrousse son champ
\dt 26/Jan/2019

\lx phaawa
\dialx GOs
\is cultures
\ps v
\ge désherber ; couper l'herbe
\ge faucher
\xv e phaawange haavwu-je
\xn il désherbe son jardin
\ng v.t. |lx{phaawange, phaawi}
\cf paang
\dialx PA
\dt 10/Jan/2022

\lx phaaxe
\ph pʰa:ɣe
\dialx GOs
\va phaxeèn, phakeen
\dialx BO PA
\is fonctions_naturelles
\ps v
\ge écouter ; prêter l'oreille
\ge scruter
\xv lha u phaxeèn
\dialx PA
\xn ils avaient entendu
\xv lha u phaxeèn khõbwe i ru a-e wo Teã-mã wo a mwã è ni Kumwaang
\dialx PA
\xn ils avaient entendu que Teã-mã allait partir pour se rendre à Koumak
\dt 22/Mar/2023

\lx phabuno
\dialx GOs
\is insecte
\ps n
\ge ver de bancoul
\dn au stade juvénile
\dt 08/Feb/2025

\lx pha-ca
\is mouvement
\ph pʰaca
\dialx GOs
\ps v
\ge toucher (une cible)
\xv e aa-pha-ca
\xn il est adroit (il a touché la cible)
\cf pha
\ce lancer
\dt 17/Feb/2025

\lx phacaani
\ph pʰaca:ɳi pʰacʰa:ɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge approuver
\dt 22/Mar/2023

\lx pha-cabi
\ph pʰacambi
\dialx GOs
\is action_corps
\ps v
\ge taper pour enfoncer
\dt 09/Apr/2024

\lx phaçegai
\ph pʰaʒeŋgai
\dialx GOs
\va pha-caxai
\dialx PA
\is interaction
\ps v
\ge taquiner qqn. ; embêter
\xv e a-pe-phaçegai
\dialx GOs
\xn il est taquin
\dt 29/Mar/2022

\lx pha-chomu-ni
\ph pʰa'cʰomu-ɳi, pʰa'cʰo:mu-ɳi
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge enseigner
\dt 02/Jan/2022

\lx phago
\ph pʰaŋgo
\dialx GOs PA BO
\is corps
\ps n
\ge corps ; enveloppe corporelle
\xv phagoo-n
\dialx PA
\xn son corps
\dt 22/Mar/2023

\lx phãgo
\ph pʰɛ̃ŋgo
\dialx GOs
\is caractéristiques_objets
\ps n
\ge forme
\xv e whaya phãgo ?
\dialx GOs
\xn quelle est sa forme ?
\xv e waya phãgoo loto?
\xn quelle est la forme de la voiture ?
\xv phãgoo-mwa
\dialx PA
\xn forme du toit (pente vue de l'intérieur)
\cf bwa-mwa
\ce toit
\dt 22/Mar/2023

\lx pha-gumãgu-ni
\dialx GOs
\is fonctions_intellectuelles
\ps v.t.
\ge approuver qqn. ; acquiescer
\dt 29/Mar/2022

\lx phai
\dialx GOs
\va phaai
\dialx BO PA
\is préparation_aliments
\ps v
\ge bouillir ; faire cuire
\xv kavwö e phai lai xo gèè
\dialx GOs
\xn la grand-mère n'a pas fait cuire le riz
\xv e phai kui
\dialx GOs
\xn elle fait cuire l'igname
\xv çö phai da ?
\xn qu'as-tu fait cuire ?
\xv i phai mwa kidoo-n
\dialx PA
\xn il fait bouillir de l’eau pour leur boisson
\dt 24/Dec/2021

\lx phai cii
\dialx GOs
\is préparation_aliments
\ps v
\ge bouillir/ cuire (les ignames) avec la peau
\xv e phai cii kui
\dialx GOs
\xn elle a cuit les ignames avec la peau
\dt 22/Feb/2025

\lx phai mwa-çii
\dialx GOs
\is préparation_aliments
\ps v
\ge cuire avec la peau (bananes)
\xv nu phai mwa-çii chaamwa
\dialx GOs
\xn j'ai fait cuire les bananes avec leur peau
\dt 26/Jan/2019

\lx phai-yaai
\dialx GOs BO
\va pha-yaai
\dialx PA WEM
\is feu
\ps v
\ge allumer un feu
\cf thi yaai
\dt 29/Jan/2022

\lx phaja
\dialx WEM WE PA BO
\is interaction
\ps v
\ge demander à qqn. ; interroger qqn.
\ge demander (se)
\xv nu phaja i je
\dialx BO
\xn je lui ai demandé
\cf zala, zhala
\dialx GOs
\ce demander à qqn.
\dt 21/Feb/2025

\lx phajoo
\ph pʰaɲɟo:
\dialx GOs PA BO
\sn 1
\is plantes_partie
\ps n
\ge entre-nœuds (de bambou, canne à sucre)
\se phajoo-ê
\dialx GOs
\sge entre-nœuds de canne à sucre
\xv phajoo-n
\dialx BO
\xn son entre-nœud (d'une plante)
\sn 2
\is corps
\ps n
\ge phalange (doigt)
\dt 22/Feb/2025

\lx phajoo-hii-n
\dialx BO
\is corps
\ps n
\ge avant-bras
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx phajoo-kòò-n
\dialx BO
\is corps
\ps n
\ge bas de la jambe
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx pha-kôńôô-ni
\ph ,pʰakõ'nõ:ɳi
\dialx GOs
\is interaction_animaux
\ps v
\ge domestiquer (un animal) ; apprivoiser
\dt 22/Mar/2023

\lx phalawe
\hm 1
\dialx GOs
\va phalaxe
\dialx GOs
\is corps
\ps n
\ge thorax ; cage thoracique ; côtes
\xv e bii phalawe-je
\xn il a les côtes fêlées
\cf zabò
\ce côte
\dt 22/Mar/2023

\lx phalawe
\hm 2
\dialx PA
\is maison
\ps n
\ge étagère ; claie
\dn sur laquelle on suspendait les paniers dans la maison, ou sur laquelle on fumait la nourriture
\dt 22/Feb/2025

\lx phalawu
\dialx BO [BM]
\va phalau
\dialx BO
\is parenté_alliance
\ps n
\ge belle-sœur (sœur d'épouse ; épouse du frère)
\dn terme tombé en désuétude, maintenant on utilise |lx{bee-}
\xv phalawu-ny
\dialx BO
\xn ma belle-sœur
\dt 05/Jan/2022

\lx phãna
\dialx GOs BO
\is interaction
\ps v
\ge gronder ; disputer
\xv e phãna-nu
\xn il m'a grondé
\dt 22/Mar/2023

\lx pha-nõnõ
\ph pʰaɳɔ̃ɳɔ̃
\dialx GOs
\is interaction
\ps v
\ge faire se souvenir ; rappeler qqch. à qqn.
\xv pha-nõnõ çai je xo a-du-mi !
\xn rappelle-lui de descendre !
\xv pha-nõnõmi-je xo a-du-mi !
\xn rappelle-lui de descendre !
\ng causatif: |lx{pha-nõnõmi}
\gt rappeler à qqn.
\dt 21/Feb/2025

\lx phaò
\dialx GOs PA
\is action_corps
\ps v
\ge entasser
\xv lhi phaò paa
\dialx GOs
\xn ils entassent les pierres ; ils font un tas de pierres
\xv lha ra u phaò dou-la
\dialx PA
\xn elles préparent leurs tas de vivres
\dt 22/Feb/2025

\lx phaöö
\dialx GOs
\va phauvwo
\dialx BO
\is vent
\ps n
\ge tourbillon de vent ; trombe de vent
\nt selon Corne
\dt 27/Mar/2022

\lx pharu
\dialx BO
\is nourriture
\ps v
\ge boire chaud
\nt selon Corne
\dt 25/Mar/2022

\lx pha-ta
\dialx GOs
\va pha-tha
\dialx PA
\is action
\ps v
\ge rater ; manquer
\xv e pha-ta xo Pwayili na ni mèni
\dialx GO
\xn Pwayili a raté l'oiseau
\xv nu pha-ta na ni kar
\dialx GO
\xn j'ai raté le car
\xv nu pha-ta
\dialx GO
\xn j'ai raté (la cible)
\cf pao-ta
\ce lancer-rater
\dt 22/Mar/2023

\lx pha-tuya
\dialx BO
\is coutumes
\ps v
\ge enlever un tabou ; rendre profane
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx pha-tre-çaxoo-ni
\ph ,pʰaʈe'ʒaɣo:ɳi
\dialx GOs
\is interaction
\ps v
\ge apaiser ; calmer (qqn.)
\dt 17/Feb/2025

\lx phau
\dialx GO
\va paup
\dialx PA BO
\is habillement
\ps n
\ge coiffure en sparterie ; turban (des anciens)
\nt selon Dubois ms. ; non vérifié
\dt 26/Mar/2022

\lx phavwu
\ph pʰaβu
\dialx GOs
\va phavwuun
\dialx PA
\is grammaire_quantificateur_mesure
\ps QNT
\ge groupe de personnes (nombreuses) ; foule
\ge beaucoup
\xv phavwu i la
\dialx GO
\xn ils sont très nombreux
\se phavwu êgu
\dialx GO
\sge une foule de gens
\dt 16/Feb/2025

\lx phavwuun
\dialx PA BO
\is grammaire_quantificateur_mesure
\ps QNT
\ge beaucoup
\xv nu nooli êgu axe phavwuun
\dialx PA
\xn j'ai vu beaucoup de gens (lit. j'ai vu beaucoup de gens et ils sont nombreux)
\xv phawuun nyama
\xn c'est beaucoup de travail
\xv phawuun mwa
\xn il y a beaucoup de maisons
\xv phavwuu êgu
\dialx PA
\xn il y a beaucoup de gens
\dt 16/Feb/2025

\lx phawe
\dialx GOs BO
\is coutumes
\ps v ; n
\ge tas (d'ignames, de pierres)
\ge aligner les ignames ; faire des tas d'ignames
\dn pour les cérémonies
\se phawe kui
\sge tas d'ignames
\se phawe paa
\sge tas de pierres
\dt 08/Feb/2025

\lx phaza-
\dialx GOs PA
\va para
\dialx BO
\is grammaire_causatif
\ps PREF.CAUS
\ge faire (causatif)
\dt 29/Jan/2019

\lx phaza-hããxe
\dialx GOs PA
\va para-hããxe
\dialx PA
\is interaction
\ps v
\ge effrayer ; faire peur à qqn.
\xv e phaza-hããxe-nu
\dialx GO
\xn il m'a fait très peur
\xv e phaza-hããxe-ni ẽnõ
\dialx GO
\xn il a fait très peur à l'enfant
\xv la phaza-hããxee ẽnõ
\dialx PA
\xn ils font très peur aux enfants
\dt 10/Apr/2024

\lx pha-…-ni
\ph pʰa-…-ɳi
\dialx GOs
\is grammaire_causatif
\ps PREF.CAUS
\ge faire faire qqch. ; laisser
\xv e pha-thrõbo-ni
\xn il l'a fait tomber
\dt 29/Mar/2022

\lx phe
\hm 1
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge prendre ; enlever
\ge emporter
\xv phe-vwo !
\dialx PA
\xn sers-toi ! (emporte)
\se phe-da-ò
\dialx GO
\sge emporte-le en haut
\se phe-du-ò
\dialx GO
\sge emporte-le en bas
\se phe-da-e
\dialx GO
\sge emporte-le (transverse)
\se phe-ve
\dialx PA
\sge emporter (transverse)
\sn 2
\is portage
\ps v
\ge porter
\ge apporter
\xv e phe-du xo kêê-nu we-kò kui kòlò oã-nu
\dialx GOs
\xn mon père a apporté trois ignames à ma mère
\xv e phe-du kòlò oã-nu we-kò kui xo kêê-nu
\dialx GOs
\xn mon père a apporté trois ignames à ma mère
\xv e phe-da xo kêê-nu kòlò èvwööni la ci-kãbwa ponga na-wo
\dialx GOs
\xn mon père a apporté des tissus à mon oncle pour la fête
\xv e phe-da ci-kãbwa xo kêê-nu kòlò èvwööni ponga na-wo
\dialx GOs
\xn mon père a apporté des tissus à mon oncle pour la fête
\sn 3
\is action
\ps v
\ge recevoir
\ge obtenir
\sn 4
\is interaction
\ps v
\ge accepter
\dt 22/Mar/2023

\lx phe
\hm 2
\dialx BO
\is instrument
\ps n
\ge lime ; pierre à aiguiser
\nt selon BM
\dt 22/Mar/2023

\lx phe bala
\dialx GOs
\is action
\ps v
\ge prendre la suite
\dt 22/Mar/2023

\lx phe hòò
\dialx GOs
\is action_corps
\ps v
\ge emporter loin là-bas
\dt 22/Mar/2023

\lx phe-caaxo
\dialx GOs
\is action_corps
\ps v
\ge prendre en cachette
\xv e phe-caaxo-ni hèlè
\xn il a pris le couteau en cachette
\ng v.t. |lx{phè-caaxo-ni}
\dt 22/Mar/2023

\lx pheege
\dialx BO
\is portage
\ps v
\ge tenir dans ses bras
\xv i pheege hii-n
\xn il le tient dans ses bras
\nt selon BM ; non vérifié
\dt 27/Mar/2022

\lx phele
\dialx PA BO
\va pheze
\dialx GO
\is action_corps
\ps v
\ge attacher ; ceindre (manou)
\ge rouler (des torons)
\xv i phele mãda i ye
\dialx PA
\xn il met son manou
\xv nu phele kii-ny
\dialx BO
\xn je mets mon manou
\dt 02/Nov/2021

\lx phe-mi
\dialx GOs
\is action_corps
\ps v
\ge apporter
\dt 22/Mar/2023

\lx phènòng
\dialx BO
\is religion
\ps v ; n
\ge ensorceler
\xv i phènòng i nu
\xn il m'a jeté un sort
\se a-phènòng
\sge sorcier
\ng causatif: |lx{pha-phènòng-e}
\gt ensorceler qqn.
\nt selon Corne, BM
\dt 21/Feb/2025

\lx phe-vwo
\dialx GOs PA
\is action
\ps v
\ge emporte-le ! ; sers-toi !
\xv phe-vwo !
\dialx PA
\xn sers-toi !
\xv chooval phe-vwo
\dialx PA
\xn cheval de somme
\dt 31/Dec/2021

\lx pheze
\dialx GO
\va phele
\dialx PA BO
\is action_corps
\ps v
\ge attacher ; ceindre (manou)
\ge rouler (des torons)
\xv la phe pu-ny ma vwo la phele
\dialx PA
\xn ils prennent mes poils pour les rouler (en torons)
\ng causatif: |lx{pha-pheze}
\gt faire ceindre
\dt 05/Jan/2022

\lx phi
\hm 1
\dialx GOs
\va phii-n
\dialx PA BO
\is corps
\ps n
\ge sexe (de l'homme) ; pénis
\xv hê-phii-n
\dialx BO
\xn testicules
\cf cò
\ce sexe (de l'homme)
\dt 21/Aug/2021

\lx phidru
\dialx GOs
\va phiju
\dialx BO
\is maison
\ps n
\ge paille du faîtage
\dn formant un bourrelet
\dt 08/Feb/2025

\lx phiing
\dialx PA
\is interaction
\ps v ; n
\ge assemblée ; rassembler
\dt 27/Jan/2019

\lx phiige
\dialx GOs
\va peenge
\dialx BO [BM]
\sn 1
\is coutumes
\ps v.t.
\ge rassembler de la nourriture
\dn pour une cérémonie coutumière
\sn 2
\is feu
\ps v.t.
\ge rassembler/entasser les braises
\ng v.i. |lx{phiing} |dialx{PA}, |lx{phii} |dialx{GOs}
\dt 08/Feb/2025

\lx phinãã
\dialx GOs PA BO
\is fonctions_intellectuelles
\ps v ; n
\ge lire ; (ra)conter
\ge compter ; compter sur qqn. ; nombre
\xv i phinãã bwa xai pòi-li
\dialx GOs
\xn ils comptent sur leur enfant
\dt 29/Mar/2022

\lx phiri kûû
\ph pʰiri kũ:
\dialx GOs
\va phili kûû
\ph pʰili kũ:
\dialx GO(s)
\is son
\ps v ; n
\ge son d'une vibration ; plaque vibrante (musique)
\dn vibrer comme un boomerang
\ge vibrer ; faire du bruit
\xv e phiri kûû ã loto
\xn cette voiture vibre
\dt 08/Feb/2025

\lx phiu
\dialx GOs
\is arbre
\ps n
\ge tamanou
\sc Calophyllum inophyllum L.
\scf Guttifères
\PEO PEO bakuRa (Gerahgty)
\dt 27/Aug/2021

\lx phivwâi
\dialx GOs
\is plantes
\ps n
\ge pandanus sauvage
\dn de bord de creek; les feuilles de ce pandanus servent au tressage ; les roussettes mangent ses fruits
\dt 08/Feb/2025

\lx pho
\dialx GOs BO
\is tressage
\ps n
\ge feuille de pandanus sèche pour le tressage
\dn débarrassées de leurs piquants, elles sont prêtes pour le tressage
\xv la thu pho
\xn elles font de la vannerie
\dt 26/Aug/2021

\lx phò
\hm 1
\dialx GOs BO
\va pho-
\dialx PA
\sn 1
\is action_corps
\ps n
\ge charge ; fardeau
\ge mêler de (se)
\dn emploi métaphorique
\xv phò-cee
\dialx GOs BO
\xn fagot/tas de bois (à porter)
\xv pho-m
\dialx PA
\xn ta charge ; ton fardeau
\xv mõ vara pu phò-ã
\dialx GOs
\xn nous avons chacun notre charge/mission
\xv da nye phò-nu ?
\dialx GOs
\xn que dois-je apporter? (lit. quelle est ma charge ?)
\xv phò-nu lai ?
\dialx GOs
\xn dois-je apporter du riz ?
\xv phò kamyô
\dialx GOs
\xn la charge du camion
\xv da nye phò-çö na ni je-nã ?
\dialx GOs
\xn de quoi tu te mêles dans cette affaire?
\xv kixa phò-çö na le !
\dialx GOs
\xn tu n'as rien à voir avec cela !
\sn 2
\is classificateur_possessif
\ps n ; CLF.POSS
\ge part (apportée lors d'une coutume)
\xv phò-nu phò-cee
\dialx GOs
\xn ma charge de fagot de bois
\xv phò-nu lai
\dialx GOs
\xn ma part de riz (qu'on doit apporter pour une cérémonie coutumière)
\xv phò-la nõ
\dialx BO
\xn leur charge de poisson
\sn 3
\is grammaire_modalité
\ps n
\ge charge ; mission
\xv novwö phò-je na xòlò Treã-ma ça/ca a-phe-fhaa
\dialx GOs
\xn quant à sa mission auprès du chef, c'est d'être le porte-parole
\xv pho-m (a) nõ
\dialx PA
\xn tu es chargé d'apporter du poisson
\xv pho-m (a) tha kui
\dialx PA
\xn tu es chargé de déterrer le ignames
\xv pho-m (a) puyòl
\dialx PA
\xn c'est à toi de faire la cuisine
\et *papa
\eg porter sur le dos
\el POc
\dt 22/Feb/2025

\lx phò
\hm 2
\dialx GOs
\is grammaire_quantificateur_mesure
\ps QNT ; DISTR
\ge tous
\ge chacun
\xv cocovwa la-ã nu phò zala-la
\xn ils sont nombreux ceux à qui j'ai demandé à chacun
\xv nu phò zala-la kuuni
\xn je leur ai demandé à tous
\xv nu phò-phe kuuni
\xn j'ai tout pris
\dt 16/Feb/2025

\lx phò-
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge charge ; fardeau
\dn compter ce qu'on apporte (dans une cérémonie coutumière)
\xv phò-xè, phò-tru, etc.
\dialx GOs
\xn un tas, deux tas, etc.
\xv phò-xè phò-cee
\dialx PA
\xn un fagot de bois à porter
\dt 08/Nov/2021

\lx phõ
\dialx GOs
\va phõng
\dialx PA BO WE WEM
\is caractéristiques_objets
\ps v.i
\ge tordu ; tors
\xv i baxòòl ha i phõng ?
\dialx BO
\xn il est droit ou tordu ? (BM)
\cf phõge
\ce tordre qqch.
\dt 21/Feb/2025

\lx phoã
\dialx GOs PA
\va phoê
\dialx GOs PA
\is plantes
\ps n
\ge plante (générique)
\ge cultures
\xv e wara thu phoê
\dialx PA
\xn c'est l'époque de préparer les champs
\dt 22/Mar/2023

\lx phò-cee
\dialx GOs PA
\is feu
\is bois
\is configuration
\ps n
\ge fagot de bois
\xv phò-nu phò-cee
\dialx GOs
\xn mon fagot de bois
\dt 08/Nov/2021

\lx phò-ê
\dialx GOs
\is configuration
\ps n
\ge fagot de canne à sucre
\dn apporté lors des cérémonies
\dt 18/Feb/2019

\lx phöge
\dialx GOs
\is action_corps
\ps v
\ge entasser ; faire des tas
\xv phöge hovwo
\xn faire des tas de nourriture
\dt 22/Mar/2023

\lx phõge
\dialx GOs
\va phõnge
\dialx GOs PA
\va phõng
\dialx BO
\sn 1
\is action_corps
\ps v
\ge tordre (en revenant vers l'arrière)
\sn 2
\is caractéristiques_objets
\ps v
\ge tordu
\cf phõ
\ce tordu
\dt 22/Mar/2023

\lx phò-kui
\dialx GOs
\is coutumes
\ps n
\ge tas d'igname
\dn apporté lors des cérémonies
\dt 14/Feb/2023

\lx phole
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge tremper dans l'eau
\cf pole
\dialx nêlêmwa
\et *puraq
\eg immerse
\el POc
\dt 10/Jan/2022

\lx phòlò ja
\dialx GOs
\va phòlò jang
\dialx PA
\is caractéristiques_objets
\ps n
\ge objet ou bois flotté
\ge débris laissés par l'inondation
\dn laissés par la mer ou la rivière
\dt 20/Oct/2021

\lx phölöö
\dialx GOs PA
\va phuloo
\dialx BO
\va phuluu, wuluu
\dialx WEM
\is eau
\ps v
\ge trouble (eau) ; sale (eau) ; limoneux
\se thu phulu
\dialx WEM
\sge troubler (l'eau)
\xv la ra u mara thu-wuluu (thu phuluu)
\dialx WEM
\xn ils ont encore plus troublé l'eau
\dt 22/Mar/2023

\lx phölöö we
\dialx GOs
\is eau
\ps n
\ge eau trouble
\dt 22/Mar/2023

\lx phònõ
\ph pʰɔɳɔ̃
\dialx GOs
\va phònõng
\dialx PA
\is religion
\ps v
\ge ensorceler ; jeter des sorts
\xv e a-phònõng
\dialx PA
\xn c'est un ensorceleur
\xv e phònõnge-nu
\dialx GOs
\xn il m'a ensorcelé
\ng v.t. |lx{phònõnge}
\dt 06/Jan/2022

\lx phoo
\dialx GOs PA BO
\va phwoo
\dialx GO(s)
\is action
\ps v
\ge remplir ; charger
\xv la phoo-da kamyõ
\dialx GOs
\xn ils chargent le camion
\xv e phoo-du nye keruau
\dialx GOs
\xn elle remplit ce panier
\xv haxe lha phoonu bwa vwo ê-mãlã êgu ê-mãlã
\dialx GOs
\xn ces gens-là remplissent (un bateau)
\xv phoo-du a we
\dialx BO
\xn remplis-la d'eau
\dt 17/Jul/2024

\lx phòò
\ph pʰɔ:
\dialx GOs
\va phòòl
\ph pʰɔ:l
\dialx BO PA
\va pòòl, pwòl
\dialx BO
\is fonctions_naturelles
\ps v
\ge déféquer
\dt 10/Feb/2019

\lx phöö
\dialx GOs
\is position
\ps v
\ge tourner sur le ventre (se)
\xv e phöö ẽnõ
\dialx GOs
\xn l'enfant s'est mis sur le ventre
\dt 28/Jan/2018

\lx phõ-o phõ-mi
\dialx GOs
\va phõng-ò phõng-mi
\dialx PA
\is déplacement
\ps v
\ge faire des zigzags
\xv e a phõ-ò phõ-mi
\dialx GOs
\xn il va en zigzag
\xv i a phõng-ò phõng-mi
\dialx PA
\xn il va en zigzag
\dt 22/Mar/2023

\lx phôôme
\dialx GOs
\va phõõm
\dialx BO
\is préparation_aliments
\ps v
\ge envelopper de la nourriture dans des feuilles
\dn pour les faire cuire
\dt 08/Feb/2025

\lx phòtrõ
\ph pʰɔʈɔ̃, pʰɔ'ɽɔ̃
\dialx GOs
\va phòrõ
\dialx GO(s)
\va pòròm
\dialx PA BO
\is fonctions_intellectuelles
\ps v ; n
\ge oublier ; pardonner ; pardon
\xv phòrõ-nu !
\dialx GO
\xn pardonne-moi !
\xv e phòrõ-nu
\dialx GO
\xn il m'a pardonné ; il m'a oublié
\xv i pòrò-nu
\dialx BO
\xn il m'a pardonné
\xv pòrò-nu ma kavwu nu pu nee
\dialx PA
\xn pardonne-moi car je ne l'ai pas fait exprès
\xv e a-phòrõ
\dialx GO
\xn il oublie souvent
\xv e a-kha-phòrõ
\dialx GO
\xn il perd la mémoire (maladie)
\xv nu tilò pòròm yai caaya-m
\dialx BO
\xn j'ai demandé pardon à ton père
\ng v.t. |lx{phòtrõme}
\gt oublier qqch.
\dt 22/Feb/2025

\lx phòtrõme
\ph pʰɔ'ʈɔ̃me, pʰɔ'ɽɔ̃me
\dialx GOs
\va phòrõme
\dialx PA
\va pòrõme
\dialx BO
\is fonctions_intellectuelles
\ps v.t.
\ge oublier ; pardonner
\xv e pe-phòrõme ne a-mi
\dialx GO
\xn il a oublié de venir
\ng v.t. avec un pronom objet: |lx{phòtrõ, pòrõ} ; |lx{phòtrõme, pòrõme} pour tout autre type d'objet
\dt 10/Jan/2022

\lx phowôgo
\dialx GOs
\is eau
\ps n
\ge écume
\dt 29/Jan/2019

\lx phozo
\ph pʰoðo
\dialx GOs
\va polo, pulo
\dialx PA BO
\sn 1
\is couleur
\ps v.stat.
\ge blanc
\se êgu polo
\dialx BO PA
\sge personne albinos
\sn 2
\is caractéristiques_objets
\ps v.stat.
\ge propre ; neuf
\xv e phozo zo
\dialx GOs
\xn il est bien propre (personne)
\xv e phozo
\dialx GOs
\xn il est propre/ neuf (objet)
\dt 22/Feb/2025

\lx phòzo
\ph pʰɔðo
\dialx GOs
\va phòlo
\dialx PA BO
\is nourriture
\ps v
\ge boire chaud (soupe)
\ge manger du non solide
\dn bouillie, purée, soupe
\dt 08/Feb/2025

\lx phòzò
\ph pʰɔðɔ
\dialx GOs
\va phòlò
\dialx PA
\is action
\ps v
\ge casser (se)
\dt 29/Jan/2019

\lx phu
\hm 1
\dialx GOs
\va phul
\dialx PA BO
\is préparation_aliments
\ps v
\ge bouillir (eau)
\ge cuit (être)
\ge prêt (être)
\xv la phu dröö !
\dialx GOs
\xn les marmites sont prêtes ! (cuites et retirées du feu)
\xv e phu (we) !
\dialx GOs
\xn ça bout !
\xv i phul (a) we !
\dialx BO
\xn l'eau bout !
\et *puro
\el POc
\dt 22/Mar/2023

\lx phu
\hm 2
\dialx GOs PA BO
\sn 1
\is plantes
\ps n
\ge herbe à paille
\dn à tige longue poussant en touffe et utilisée pour le torchis
\sc Themeda triandra
\scf Graminées
\sn 2
\is maison
\ps n
\ge premier rang de paille au bord du toit ; rebord du toit de chaume
\xv e thu phu
\dialx GOs
\xn il met la première rangée de paille
\dt 08/Feb/2025

\lx phu
\hm 3
\dialx GOs
\va phuxa
\dialx PA
\sn 1
\is habillement
\ps v
\ge enlever (chapeau)
\xv e phu mwêêxa-je
\dialx GOs
\xn il enlève son chapeau
\xv i phu hau-n
\dialx PA
\xn il enlève son chapeau
\sn 2
\is préparation_aliments
\ps v
\ge retirer (marmite du feu)
\xv phuu drööa-wa !
\dialx GOs
\xn retirez votre marmite (du feu) !
\cf phuxa
\dialx PA
\ce enlever
\dt 28/Mar/2024

\lx phu
\hm 4
\dialx GOs PA
\va phwu
\dialx BO
\is cultures
\ps v
\ge arracher (paille, taro d'eau)
\xv i phu kuru
\xn elle arrache le taro d'eau
\ng v.t. |lx{phugi}
\dt 05/Jan/2022

\lx phu
\hm 5
\dialx GOs BO PA
\is action_corps_animaux
\ps v
\ge voler (oiseau)
\et *Ropok
\el POc
\dt 30/Jan/2019

\lx phu
\hm 6
\dialx BO
\is santé
\ps v ; n
\ge tousser ; grippe
\xv i tooli phu
\dialx BO
\xn il a attrapé la grippe
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx phû
\dialx GOs
\va pû
\dialx GOs
\va phûny
\dialx WEM BO PA
\is couleur
\ps v.stat.
\ge vert ; bleu
\xv e p(h)û phãgoo mèni
\dialx GOs
\xn cet oiseau est vert (lit. le corps de l'oiseau est vert)
\xv nooli me-p(h)û-w-a dònò !
\dialx GOs
\xn regarde le bleu du ciel !
\xv nèn phûny
\dialx PA
\xn mouche verte
\dt 22/Feb/2025

\lx phu kuru
\dialx PA
\is cultures
\ps v
\ge arracher les taros d'eau
\ng v.t. |lx{phugi}
\dt 05/Jan/2022

\lx phu nõbu
\ph pʰu ɳɔ̃bu
\dialx GOs
\is coutumes
\ps v
\ge lever un interdit
\cf khabe nõbu
\ce mettre un interdit
\dt 18/Feb/2019

\lx phubwe
\dialx GOs BO
\va phöbwe
\dialx GO(s)
\va phobwe
\dialx BO
\is préparation_aliments
\ps v
\ge passer sur la flamme
\dn pour assouplir les feuilles, ou pour enlever la couche gluante des anguilles
\xv la phubwe dòò-chaamwa
\dialx BO
\xn ils passer les feuilles de bananier sur la flamme
\cf ûne
\ce passer sur la flamme
\dt 08/Feb/2025

\lx phue
\dialx GOs
\is instrument
\ps n
\ge fouet
\bw fouet (FR)
\xv thila phue
\xn le bout/mèche du fouet
\dt 25/Aug/2021

\lx phugi
\dialx GOs PA BO
\sn 1
\is action_plantes
\ps v.t.
\ge arracher (à la main)
\dn des herbes ou des plantes à racine |lx{kumala, kuru}, etc.
\ge décoller ; enlever |dialx{BO}
\xv phugi drawalu !
\dialx GOs
\xn arrache l'herbe !
\xv i phugi mãe !
\dialx PA
\xn elle arrache la paille
\sn 2
\is médecine
\ps v.t.
\ge cueillir des herbes magiques |dialx{BO}
\ng v.i. |lx{phu}
\dt 08/Feb/2025

\lx phulè
\dialx GOs
\va phulèng
\dialx PA BO
\va phulek
\dialx BO
\is plantes
\ps n
\ge Pipturus
\dn ses fibres servent à faire des nasses, des cordes de fronde
\sc Pipturus argenteus
\scf Urticacées
\se pò-phulèng
\sge fruit de Pipturus
\cf alamwi
\dt 09/Jan/2022

\lx phumõ
\dialx GOs
\va pumõ
\dialx PA
\sn 1
\is discours
\ps v
\ge discourir ; faire un discours (coutumier)
\ge prêcher ; sermonner
\xv e pe-phumõ
\dialx GOs
\xn il fait son prêche
\xv e a-phumõ
\dialx GOs
\xn c'est un sermonneur
\sn 2
\is discours
\ps v
\ge conseiller
\xv e phumõ-nu
\dialx GOs
\xn il m'a conseillé
\xv e pumõ-nu
\dialx PA
\xn il m'a conseillé
\xv e phumõ-ni pòi-nu
\dialx GOs
\xn il a conseillé mon enfant
\xv li pe-pumõ
\dialx PA
\xn ils se sont consultés, ont pris conseil l'un auprès de l'autre
\ng v.t. |lx{phumõ} (+ objet pronominal), |lx{phumõ-ni} (+ objet nominal, nom commun)
\dt 24/Jan/2022

\lx phumwêê
\dialx GOs
\is navigation
\ps v
\ge flotter
\dt 26/Jan/2018

\lx phunò
\ph pʰuɳɔ
\dialx GOs
\is coutumes
\ps n
\ge coutume de deuil
\ge ornement de deuil
\dt 22/Mar/2023

\lx phunò mã
\ph pʰuɳɔ
\dialx GOs
\is coutumes
\ps n
\ge coutume de deuil
\dt 19/Aug/2021

\lx phu-nõgo
\ph pʰu'ɳɔ̃ŋgo
\dialx GOs
\is santé
\va punõgo
\dialx PA BO
\ps v
\ge tousser ; avoir la grippe
\dt 26/Aug/2021

\lx phuri
\dialx BO
\is santé
\ps v
\ge grossir
\nt selon BM
\dt 26/Mar/2022

\lx phuu
\hm 1
\dialx GOs PA BO
\va phuuxu
\dialx GO
\sn 1
\is mouvement
\ps v.i
\ge monter (eau) ; déborder
\xv e phuu we
\dialx GOs
\xn l'eau monte (rivière)
\xv e phuuxu we
\dialx GOs
\xn l'eau déborde (rivière)
\xv i phuu we
\dialx PA
\xn l'eau monte (rivière)
\sn 2
\is santé
\ps v.i
\ge gonfler ; enfler (membre, partie du corps)
\xv phuu mee-je
\dialx GOs
\xn ses yeux enflent
\ng causatif: |lx{pha-phuu-ni}
\gt faire gonfler qqch.
\cf khibu
\ce enfler
\dt 21/Feb/2025

\lx phuu kò
\dialx GOs
\is santé
\ps n
\ge elephantiasis
\dn (lit. jambe qui enfle)
\dt 08/Feb/2025

\lx phuvwu
\ph pʰuβu
\dialx GOs
\va phuul
\dialx BO PA
\is fonctions_naturelles
\ps v
\ge ronfler
\dt 25/Jan/2019

\lx phuxa
\dialx GO PA
\is action
\ps v
\ge enlever
\xv i phuxa hau-n
\xn il enlève son chapeau
\cf phuu 2
\dt 07/Nov/2021

\lx phuzi
\ph pʰuði
\dialx GOs
\is caractéristiques_personnes
\ps v.stat.
\ge gras
\dt 26/Jan/2019

\lx phwa
\hm 1
\dialx GOs PA BO
\is corps
\ps n
\ge bouche
\se ci-phwa
\sge lèvre
\xv phwa-n
\dialx PA
\xn sa bouche
\dt 28/Aug/2023

\lx phwa
\hm 2
\dialx PA
\wr A
\is configuration
\ps n
\ge trou
\ge ouverture
\ge trou
\ge orifice
\ge creux
\se phwe-paa
\sge entrée de la grotte
\ng forme déterminée: |lx{phwe-}
\wr B
\is configuration
\ps v.stat.
\ge percé
\ge troué
\xv i phwa
\dialx PA BO
\xn c'est troué
\ng causatif: |lx{pa-phwa-ni}
\gt trouer
\wr C
\is classificateur_numérique
\ps CLF.NUM
\ge trous (pour les plantations) ; 
\ge tas de prestations cérémonielles
\et *papa
\eg trou
\el POc
\dt 08/Feb/2025

\lx phwa
\hm 3
\dialx PA
\va phwal
\dialx BO
\is astre
\ps n
\ge ciel
\xv i pao ni phwa
\dialx PA
\xn il le jette en l'air
\cf dòòn
\dialx PA
\ce cieux
\dt 10/Jan/2022

\lx phwa-
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge coup ; détonation
\se phwa-xè, phwa-tru
\dialx GOs
\sge uen, deux détonation(s)
\xv i thi je phwa-xe
\dialx PA
\xn il y a eu une détonation
\dt 22/Feb/2025

\lx phwa draalae
\dialx GOs
\is discours
\ps v
\ge parler français
\se phwa dalaan
\sge langue des étrangers
\dt 21/Aug/2021

\lx phwa ni gòò-mwa
\dialx GOs WEM WEH
\va phwe-kuracee
\dialx PA
\is maison
\ps n
\ge fenêtre
\dt 26/Jan/2019

\lx phwaa
\dialx GOs
\va phwaal
\dialx PA BO
\sn 1
\is lumière
\ps v
\ge clair (être) ; faire jour ; dégagé (ciel) ; faire beau
\xv e thaavwu phwaa
\dialx GOs
\xn il commence à faire jour
\xv e phwaa
\dialx GOs
\xn il fait jour
\xv phwaal (e) tèèn
\dialx PA
\xn un jour clair
\xv i phwaal (a) dòn
\dialx BO
\xn le ciel est dégagé
\sn 2
\is interaction
\ps v.stat.
\ge clairement ; public
\xv e kô-phwaa vhaa-je
\dialx GOs
\xn sa parole est claire
\xv phwaal nai yo ?
\dialx PA
\xn est-ce clair pour toi ?
\se khõbwe-phwaaze
\dialx GOs
\sge parler clairement
\se vaa phwal
\dialx BO
\sge parler clairement/à voix haute
\sn 3
\is cultures
\ps v
\ge défriché ; dégagé
\xv phwamwa kavwö phwaa
\dialx GOs
\xn un champ non débroussé/défriché
\ng v.t. |lx{phwaale} |dialx{BO, PA}, |lx{phwaaze} |dialx{GOs}
\dt 22/Feb/2025

\lx phwaa khoojòng
\dialx PA
\is interaction
\ps v
\ge faire des plaisanteries grivoises
\cf khoojòng
\ce faux manguier
\dt 27/Jan/2019

\lx phwããgo
\dialx GOs BO
\va pwããgo
\dialx PA BO
\is bananier
\ps n
\ge bananier (clone de)
\sc Musa sp.
\scf Musacées
\dt 27/Aug/2021

\lx phwaal
\dialx PA BO
\is mouvement
\ps v
\ge rouler (se)
\ge tourner ; faire demi-tour
\se phwaal-ò phwaal-mi
\dialx BO
\sge il tourne de-ci de-là
\dt 25/Aug/2021

\lx phwaala tèèn
\dialx BO
\is lumière
\ps n
\ge lumière du jour ; lumière
\dt 29/Jan/2019

\lx phwaa-me mhwããnu
\dialx GOs
\is astre
\ps n
\ge pleine lune
\dt 28/Jan/2019

\lx phwaaza-tree
\dialx GOs
\va phwaala-tèèn, phwara-tèèn
\dialx PA BO
\is temps_découpage
\ps n
\ge aurore ; premières lueurs du jour
\xv phwaaza tree
\dialx GOs
\xn c'est l'aurore ; le jour se lève
\dt 22/Feb/2025

\lx phwalawa
\dialx GOs
\va pwalaa, palawa
\dialx PA
\is nourriture
\ps n
\ge pain
\bw flour (GB)
\dt 26/Jan/2019

\lx phwamwa
\dialx GOs PA BO
\va pwamwò-n
\dialx PA
\sn 1
\is habitat
\ps n
\ge pays ; tribu ; contrée
\xv pwamò-ny
\dialx PA
\xn mon pays ; chez moi
\se me-phwamwa
\dialx GOs
\sge le sud du pays
\se pòminõ phwamwa
\dialx GOs
\sge le nord du pays
\ng forme déterminée: |lx{pwamwò} |dialx{PA}, |lx{pomõ} |dialx{GOs}
\sn 2
\is cultures_champ
\ps n
\ge champ ; plantation
\xv a bwa-(ph)wamwa !
\dialx GOs
\xn va au champ !
\et *panua
\el POc
\dt 22/Feb/2025

\lx phwau-n
\dialx PA BO [BM]
\is coutumes
\ps n
\ge don à la mère pour la naissance d'un enfant
\xv i thu phwau-n
\xn il faire un don pour une naissance
\dt 18/Dec/2021

\lx phwawali
\dialx GOs BO PA
\va pwapwali
\dialx BO PA
\va pwali
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v.stat.
\ge long ; haut
\ge grand
\ge longtemps
\xv e phwawali
\dialx GO
\xn il est grand
\xv e phwawali nai je
\dialx GO
\xn il est plus grand qu'elle
\sn 2
\is temps
\ps v.stat.
\ge long ; lent
\xv phwawali-je vwo burô
\dialx GO
\xn il met du temps à se laver
\xv e burô phwawali
\dialx GO
\xn il se lave longtemps
\xv phwawali mwã tree
\dialx GO
\xn les jours passent
\cf khawali
\ce long; haut
\dt 22/Mar/2023

\lx phwaxa
\dialx GOs PA
\is caractéristiques_objets
\ps n
\ge longueur
\ge durée
\ge hauteur
\xv phwaxa wõ
\dialx GO
\xn la longueur du bateau
\xv ni phwaxa tree
\dialx GO
\xn la durée de la journée ; toute la journée
\xv phwaxa tèèn
\dialx PA
\xn la durée de la journée ; toute la journée
\xv phwaxa dèn
\dialx PA
\xn la longueur du chemin
\dt 22/Feb/2025

\lx phwaxaa
\dialx GOs
\is crustacés
\ps n
\ge crabe de sable
\dn sert d'amorce
\dt 08/Feb/2025

\lx phwaxi
\dialx GOs PA BO
\is caractéristiques_personnes
\ps n
\ge hauteur ; taille (en hauteur)
\xv pe-phwaxi-li
\dialx GOs PA
\xn ils sont de même taille
\xv waya phwaxi-m ?
\dialx PA
\xn tu mesures combien ? (lit. quelle est ta hauteur ?)
\xv phwaxi-n
\dialx BO
\xn sa taille
\dt 21/Aug/2021

\lx phwaxoi-n
\dialx BO
\is corps
\ps n
\ge diaphragme
\nt selon BM
\dt 26/Mar/2022

\lx phwa-xûdo
\dialx GOs
\is eau_topographie
\ps n
\ge chenal
\cf kûdo
\ce baie
\dt 23/Aug/2021

\lx phwayuu
\dialx GOs WEM WE BO
\va whayuu
\dialx GO(s)
\is fonctions_naturelles
\ps n
\ge sang menstruel
\xv e tòòli phwayuu
\dialx GO
\xn elle a ses règles
\se mwa phwayuu ; mõ-pwhayuu
\dialx GO
\sge maison où se retirent les femmes
\dt 28/Jan/2022

\lx phwa-zua zuanga
\dialx GOs
\is discours
\va phwa-zua yuanga
\dialx PA BO
\ps n
\ge langue zuanga/yuanga
\dt 26/Mar/2022

\lx phwè-
\dialx GOs
\sn 1
\is configuration
\ps n
\ge trou de ; orifice
\se phwè-pwaji
\sge trou de crabe
\se phwè-bii
\dialx PA
\sge orifice de la conduite d'eau
\se phwè-doo
\dialx PA
\sge ouverture de la marmite
\ng forme dépendante en composition de |lx{phwa}
\gt trou
\sn 2
\is topographie
\ps n
\ge tranchée
\se phwè-wano
\sge tranchée sur crête forestière
\dt 17/Mar/2023

\lx phwe-ai
\dialx GOs PA BO
\sn 1
\is corps
\ps n
\ge cœur
\xv phwe-ai je
\dialx GOs
\xn son cœur
\xv phwe-ai-n
\dialx PA BO
\xn son cœur
\sn 2
\is sentiments
\ps n ; v
\ge amour ; sentiment ; affection ; désir ; volonté
\ge désirer
\dt 15/Sep/2021

\lx phwè-bozo
\dialx GOs
\va pwe-bulo-n
\dialx PA
\va phwè-bujo-n
\dialx BO
\is corps
\ps n
\ge nombril
\xv phwè-bozoo-je
\dialx GO
\xn son nombril
\et *mpusos
\eg nombril
\el POc
\dt 09/Jan/2022

\lx phwè-chãnã
\ph pʰwɛcʰɛ̃ɳɛ̃
\dialx GOs
\is corps
\ps n
\ge diaphragme ; sternum
\dn (lit. orifice du souffle)
\dt 09/Feb/2025

\lx phwè-dè
\dialx GOs
\va phwè-dèn
\dialx PA BO WEM
\sn 1
\is déplacement_moyen
\ps n
\ge chemin
\sn 2
\is société_organisation
\ps n
\ge chemin coutumier
\xv i a va nye phwè-dèn ?
\dialx BO
\xn où va ce chemin ?
\dt 05/Nov/2021

\lx phwee
\dialx GOs
\is santé
\is fonctions_naturelles
\ps v
\ge expectorer
\ge expulser par la bouche
\ge cracher
\dt 23/Aug/2021

\lx phweegi
\dialx PA
\is bananier
\ps n
\ge banane chef
\dn il est interdit de la cuire sur la braise, elle ne peut être que bouillie
\dt 19/Feb/2025

\lx phwè-êgu
\dialx GOs
\is corps
\ps n
\ge vagin
\dt 24/Jan/2019

\lx phwee-mee pwiò
\dialx GOs PA BO
\is pêche
\ps n
\ge maille de filet
\dt 26/Jan/2019

\lx phwèè-mwa
\dialx GOs PA BO
\va phwè-mwa
\sn 1
\is maison
\ps n
\ge porte (de maison)
\sn 2
\is société_organisation
\ps n
\ge guide
\dn personne qui sert de "chemin" pour entrer dans la chefferie |dialx{PA}
\cf pwaxilo
\dialx PA BO
\ce porte
\dt 08/Feb/2025

\lx phwee-mwãgi
\dialx GOs
\is grammaire_manière
\ps ADV
\ge inutile ; vainement ; en vain
\xv phwè-dè xa phwee-mwãgi
\xn un chemin coutumier qui est mal fait
\xv mõgu xo phwee-mwãgi
\dialx GOs
\xn un travail qui ne sert à rien
\dt 20/Oct/2021

\lx phweewe
\dialx BO PA
\is discours
\ps v
\ge raconter
\xv nu ru phweewe je-nã vajama
\dialx PA
\xn je vais raconter cette histoire
\dt 21/Dec/2021

\lx phwèè-we
\dialx GOs PA BO
\va phwè-we
\dialx GO PA
\sn 1
\is eau_topographie
\ps n
\ge passe (dans la mer)
\sn 2
\is eau
\ps n
\ge trou d'eau ; mare ; embouchure
\dt 26/Aug/2023

\lx phweexoe
\dialx GOs
\is discours
\ps v
\ge annoncer (avec un geste coutumier)
\xv la a phweexoe mã
\dialx GOs
\xn ils sont allés annoncer un deuil
\dt 23/Aug/2021

\lx phweexu
\dialx GOs PA BO
\va phweeku
\dialx GOs
\va phweewu
\dialx BO
\sn 1
\is discours
\ps v ; n
\ge annoncer ; raconter
\ge annonce ; information ; nouvelle (souvent mauvaise)
\xv ge le phweexu
\dialx GO
\xn il y a une nouvelle
\xv nu ru phweewu-yu pu yu tõne
\dialx BO
\xn je vais te le raconter pour que tu entendes
\xv tõne je phweewu
\dialx BO PA
\xn écoute cette nouvelle
\xv phweexu-a-ny
\dialx PA
\xn mon histoire (que je raconte)
\xv phweexu na inu
\dialx PA
\xn mon histoire (à propos de moi)
\sn 2
\is interaction
\ps v
\ge bavarder ; converser ; discuter
\xv la pe-phweexu
\dialx GO PA
\xn ils bavardent ; ils se racontent des histoires
\dt 22/Feb/2025

\lx phwè-gòògoni
\ph pʰwɛ-
\dialx PA
\is corps
\ps n
\ge sternum
\dt 19/Mar/2019

\lx phwè-kêni
\dialx GOs
\is corps
\ps n
\ge trou de l'oreille
\xv phwè-kênii-je
\xn le trou de son oreille
\dt 21/Aug/2021

\lx phwè-kevwa
\dialx GO
\is topographie
\ps n
\ge ouverture de la vallée
\dt 16/Aug/2021

\lx phwè-kui
\dialx GOs WEM PA
\is cultures
\ps n
\ge trou (pour planter l'igname)
\cf zaa phwa
\ce faire le trou pour planter l'igname
\dt 08/Feb/2025

\lx phwè-kurace
\dialx PA BO
\is maison
\ps n
\ge fenêtre
\dt 19/Mar/2019

\lx phwè-meevwu
\dialx GOs PA BO
\is société_organisation
\ps n
\ge clan ; famille
\dt 22/Mar/2023

\lx phwè-mhwêêdi
\dialx GOs PA BO
\is corps
\ps n
\ge narine
\xv phwè-mhwêêdi-n
\dialx BO
\xn son nez
\dt 21/Aug/2021

\lx phwêne
\dialx GOs
\va phwêneng
\dialx PA
\is interaction
\ps INTJ ; v.admonitif
\ge attention! ; faire attention
\xv çö phwêne ma pò khalu
\dialx GO
\xn fais attention, il y a un petit creux
\xv ra u mha phwêneng !
\dialx PA
\xn faites bien attention!
\dt 18/Jan/2025

\lx phwè-nõgò
\ph pʰwɛɳɔ̃ŋgɔ
\dialx GOs BO PA
\is eau_topographie
\ps n
\ge embouchure de la rivière
\ge confluent d'un creek dans un fleuve
\dt 02/Jan/2022

\lx phwè-ńhã
\dialx GOs
\va phwè-ńã
\dialx GOs
\va phwè-nhõ
\dialx PA BO
\va phwè-bwinõ-n
\dialx PA BO
\is corps
\ps n
\ge anus (grossier) 
\dn selon [Dubois]
\xv phwè-nhõ-n
\dialx PA BO
\xn son anus
\dt 09/Feb/2025

\lx phweõ
\dialx GOs
\va phwee-hòn, phweòn
\dialx BO [Corne]
\is eau
\ps n
\ge source
\dn endroit où l'eau sourd
\dt 08/Feb/2025

\lx phwè-paa
\dialx GOs
\is topographie
\ps n
\ge grotte
\dt 28/Jan/2019

\lx phwè-peenã
\dialx GOs
\is anguille
\ps n
\ge trou d'anguille
\dn anfractuosité dans la roche où se cachent les anguilles
\dt 22/Mar/2023

\lx phwè-po-xaò
\dialx GOs
\va phwè-vwo-xaò
\dialx GO(s)
\is cultures
\ps n
\ge caniveau
\cf kaò
\ce inonder
\dt 22/Aug/2021

\lx phwevwöu
\dialx GOs PA BO
\sn 1
\is configuration
\ps n.loc
\ge intervalle entre deux rangées d'un champ d'ignames
\se phwevwöu mwa
\dialx PA
\sge intervalle entre les maisons
\sn 2
\is nom_locatif
\ps n.loc
\ge entre ; dans l'intervalle
\xv i a-da phwevwöu êgu
\dialx PA
\xn il marche entre les gens
\xv nee-je ni phwevwöu daalaèn
\dialx BO
\xn mets-le entre les deux européens
\se na ni phwevwöu
\dialx BO
\sge dans l'intervalle
\dt 24/Feb/2025

\lx phwè-waa
\dialx GOs
\va phwè-whaa
\dialx GOs
\va phwè-whara
\dialx PA
\is corps
\ps n
\ge fontanelle
\xv phwè-waa i je
\xn sa fontanelle
\cf wha
\ce grandir
\dt 22/Mar/2023

\lx phwè-wado
\dialx BO
\sn 1
\is santé
\ps n
\ge carie dentaire
\sn 2
\is corps
\ps n
\ge interstice entre les dents
\ge trou dans la dentition
\dt 15/Sep/2021

\lx phwè-woo
\dialx GOs
\is cordes
\ps n
\ge lasso (pour attraper un cheval)
\ge nœud coulant
\dt 21/Aug/2021

\lx phwe-zatri
\ph pʰweðaɽi
\dialx GOs
\va pwe-zari
\dialx PA
\va pwe-yari
\dialx BO
\is médecine
\ps n
\ge guérisseur 
\dn (lit. origine du médicament)
\dt 08/Feb/2025

\lx phwè-zini
\dialx GOs
\va phwe-thîni
\dialx PA
\sn 1
\is maison
\ps n
\ge porte de clôture
\ge portail
\sn 2
\is société_organisation
\ps n
\ge entrée de la chefferie
\dt 15/Sep/2021

\lx phwia, phwiça
\dialx GOs PA WEM WE BO
\va phuça
\ph pʰudʒa
\dialx GO(s)
\va phuya
\dialx BO
\sn 1
\is fonctions_naturelles
\ps v
\ge réveiller qqn.
\dn en secouant, en retournant
\xv e phwiça-me vwo me a mwã
\dialx GO
\xn elle nous réveillait pour que nous partions
\xv phwia-je !
\dialx GO
\xn réveille-le !
\xv phuya mèni wu la u nòòl !
\dialx PA
\xn secoue les oiseaux pour qu'ils se réveillent !
\sn 2
\is mouvement
\ps v
\ge retourner ; soulever une pierre |dialx{PA}
\dn pour voir s'il y a qqch. en dessous
\dt 22/Feb/2025

\lx phwi-n
\dialx BO
\va phui-n
\dialx BO
\is santé
\ps n
\ge bosse
\xv phwi-n
\xn sa bosse
\nt [Corne] non vérifié
\dt 31/Dec/2021

\lx phwioo
\dialx GOs
\va phoyo
\dialx GO(s)
\is parenté
\ps n
\ge aîné (fils) 
\dn de la famille et de la phratrie
\xv phwioo-nu
\dialx GO
\xn mon aîné
\xv phwioo-ã
\xn notre fils aîné
\xv ẽnõ-ã ça e thrõbo phwioo-iço
\xn cet enfant-là est ton fils aîné (lit. est né ton aîné)
\dt 08/Feb/2025

\lx phwô
\dialx GOs
\is action_corps
\ps v
\ge courber ; tordre
\xv e phwô-ò phwô-mi
\dialx GOs
\xn il zigzague
\dt 26/Jan/2019

\lx phwòòn
\dialx BO
\va fwòòn
\dialx BO
\is poisson
\ps n
\ge loche (petite) ; lochon
\dt 29/Jan/2019

\lx phwòzi
\dialx GOs
\va phwòli
\dialx BO
\sn 1
\is action_corps
\ps v
\ge presser ; écraser (la pulpe de coco)
\sn 2
\is soin
\ps v
\ge masser
\et *posi
\el POc
\dt 22/Mar/2023

\lx ra
\dialx PA
\va za
\dialx GOs
\is grammaire_assertif
\ps PTCL (assertive)
\ge assertif
\xv e ra yuu phwamwa
\xn il reste sur ses terres
\dt 05/Jan/2023

\lx ra ?
\dialx PA WE
\va za ?
\dialx GO(s)
\is grammaire_interrogatif
\ps INT (post-verbal)
\ge quoi?
\xv çö pwò-ra ? (ou) çö pò-ra ?
\dialx WE
\xn que fais-tu?
\xv çö pò-za ?
\dialx GOs
\xn que fais-tu?
\dt 05/Nov/2021

\lx ra u
\dialx PA BO
\va za u
\dialx GO(s)
\is grammaire_assertif
\ps MODAL
\ge vraiment ; tout à fait
\xv i ra u thoo/thuu pao-i-je jigel
\dialx BO
\xn il s'est tiré un coup de fusil à lui-même
\xv i ra u thuu pao-i-je jigel
\dialx PA
\xn il s'est tiré un coup de fusil à lui-même
\xv kabun ra u è
\dialx BO
\xn c'est tout à fait défendu
\dt 25/Feb/2025

\lx ri ?
\dialx PA WE
\is grammaire_interrogatif
\ps INT.POSS
\ge qui (de/à) ?
\xv hèlè ri nye ?
\xn à qui est ce couteau ? (lit. couteau de qui ?)
\cf ti ?
\ce qui ?
\ng la forme |lx{ri} n'existe pas en |dialx{GO(s)}
\dt 05/Jan/2022

\lx ru
\dialx PA BO
\va to, ro
\dialx BO WEM
\is grammaire_temps
\ps FUT
\ge futur ; prospectif
\xv i ru pwal
\dialx PA
\xn il va pleuvoir
\xv nu ru a
\xn je pars ; je vais partir
\xv no ro khõbwe yaala-li
\dialx WEM
\xn je vais dire leur nom
\cf u ru
\ce généralement postposée au pronom sujet
\dt 22/Feb/2025

\lx ruma
\dialx PA BO
\va toma
\dialx BO
\is grammaire_temps
\ps FUT
\ge futur
\xv nu ruma khõbwe
\dialx PA
\xn je le dirai
\xv mwa ruma õ-xe pe-ròòli
\dialx BO
\xn on se reverra une fois
\dt 14/Aug/2021

\lx salaa
\dialx GOs
\is nourriture
\ps n
\ge salade
\bw salade (FR)
\dt 26/Jan/2019

\lx sapone
\dialx GOs
\is société
\ps n
\ge japonais
\bw japonais (FR)
\dt 27/Jan/2019

\lx simi
\dialx GOs
\va cimic
\dialx PA
\is habillement
\ps n
\ge chemise
\bw chemise (FR)
\xv e udale simi
\dialx GOs
\xn il met sa chemise
\xv i udale cimic
\dialx PA
\xn il met sa chemise
\dt 09/Jul/2022

\lx siro
\dialx GOs
\is nourriture
\ps n
\ge sirop
\bw sirop (FR)
\dt 26/Jan/2019

\lx ta
\hm 1
\dialx GO PA
\is grammaire_conjonction
\ps CNJ
\ge alors ; puis
\dt 03/Feb/2025

\lx ta
\hm 2
\dialx PA
\va ra
\dialx PA
\is grammaire_assertif
\ps PTCL (assertive)
\ge vraiment
\dt 26/Jan/2022

\lx ta
\hm 3
\dialx GOs
\va taam
\dialx WE
\va tav
\dialx PA
\is maison_objet
\ps n
\ge table
\bw table (FR)
\dt 26/Jan/2022

\lx ta
\hm 4
\dialx GOs
\is action
\ps v
\ge rater ; manquer
\xv e ta na ni dè
\xn il s'est trompé de chemin ; il s'est égaré
\dt 22/Feb/2025

\lx taa
\dialx GOs PA BO
\sn 1
\is action_corps
\ps v
\ge creuser
\ge plonger la main pour creuser
\sn 2
\is cultures
\ps v
\ge déterrer les tubercules 
\dn ignames, taro de montagne
\se taa kumwala
\sge déterrer des patates douces
\se taa kui
\sge déterrer les ignames
\se taa uvhia
\sge déterrer les taros de montagne
\sn 3
\is action_outils
\ps v
\ge piocher
\dt 08/Feb/2025

\lx tãã
\dialx GOs
\sn 1
\is mouvement
\ps v
\ge surgir
\sn 2
\is action_eau_liquide_fumée
\ps v
\ge gicler
\ng causatif: |lx{pa-tãã}
\gt faire gicler
\dt 05/Jan/2022

\lx taa kui
\dialx GOs
\va thaa
\dialx PA
\is cultures
\ps v
\ge creuser (pour récolter des ignames) ; récolter les ignames
\se warau taa kui
\dialx GOs
\sge époque de la récolte des ignames
\se wara thaa kui
\dialx PA
\sge époque de la récolte des ignames
\dt 22/Mar/2023

\lx taa phwa
\ph ta: pʰwa
\dialx GOs
\va taa-vwa
\ph ta:βa
\is action_corps
\ps v
\ge creuser un trou ; creuser une tombe
\xv e taa phwè-kui
\xn il creuse les trous pour l'igname (lit. les trous des ignames)
\xv mi a taa-vwa-ni pomõ-ã
\xn allons préparer (les trous pour les plants dans) notre champ
\ng v.t. |lx{taa-vwa-ni}
\dt 13/Jan/2022

\lx taa-bi
\dialx GOs
\is action_corps
\ps v
\ge piler ; écraser
\se ba-taa-bi
\sge pilon
\dt 22/Mar/2023

\lx taabö
\dialx GOs
\is configuration
\ps v ; n
\ge séparer ; séparation ; frontière
\se taabö-wo mwa
\sge cloison
\se mhenõ taabö-wo
\sge frontière (endroit de la séparation)
\dt 18/Feb/2019

\lx taabunõõ
\ph ta:buɳɔ̃:
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge ajouter du liquide
\dt 26/Jan/2019

\lx taabwa pi
\dialx PA
\is fonctions_naturelles_animaux
\ps v
\ge couver des œufs
\dt 25/Jan/2019

\lx taagi kui
\dialx BO
\is cultures
\ps v
\ge replier la tigne d'igname
\dn sur elle-même quand elle dépasse la hauteur du tuteur (selon Corne)
\nt non vérifié
\dt 08/Feb/2025

\lx taagine
\ph ta:ŋgiɳe
\dialx GOs
\va taagin
\dialx PA BO
\va tagin
\dialx BO
\va taagi
\dialx WEM
\sn 1
\is grammaire_aspect
\ps v
\ge continuer ; perpétuer
\xv kêbwa co taagin !
\dialx PA
\xn ne recommence pas ! (à un enfant)
\ng v.t. |lx{taagine}
\sn 2
\is grammaire_aspect
\ps v ; ASP
\ge toujours ; tout le temps
\xv i ra u nee-taagine
\dialx PA
\xn il le fait continuellement
\xv i pajaa-je taagine
\dialx PA
\xn il lui demande constamment
\xv i pajaa taagine yai je
\dialx PA
\xn il lui demande constamment
\xv i ra ku thiloo-je taagin u mwani
\dialx PA
\xn il lui demande souvent de l'argent
\xv i pwal taagin
\dialx BO
\xn il pleut sans cesse
\se ô-taagin
\dialx PA
\sge constamment
\dt 05/Jan/2022

\lx taaja
\dialx GOs BO
\is chasse
\is pêche
\ps v
\ge piquer (à la sagaie)
\dt 26/Jan/2019

\lx taare
\dialx BO
\is feu
\ps v
\ge séparer ; dégager
\ge étaler (le feu du four)
\dn i.e. enlever le bois du feu et éparpiller les braises, selon [BM, Corne]
\nt non vérifié
\dt 09/Feb/2025

\lx tabaò
\dialx GO PA
\is mouvement
\va taabawe
\dialx BO
\ps v
\ge toucher avec la sagaie ; piquer
\dt 22/Mar/2023

\lx tabila
\dialx BO PA WE
\is action_outils
\is outils
\ps v
\ge clouer
\dt 25/Jan/2019

\lx tabwa
\dialx PA
\is richesses
\ps n
\ge monnaie kanak
\dn monnaie dont la longueur est calculée en position assise, de la hauteur de la tête jusqu'au sol (selon Charles Pebu-Polae)
\cf kòòl, gò-hii
\ce monnaies
\dt 22/Oct/2021

\lx taçinô
\ph taʒiɳõ
\dialx GOs
\is interaction
\ps v
\ge embêter ; empêcher ; déranger
\xv taçinô-je !
\xn embête-le !
\xv e pe-taçinô
\xn il est casse-pieds/embêtant
\dt 22/Feb/2025

\lx taçuuni
\ph taʒu:ɳi
\dialx GOs
\is interaction
\ps v ; n
\ge refuser la demande de pardon ; refuser un geste coutumier
\an uvwa
\at accepter la demande de pardon
\dt 27/Oct/2023

\lx tae
\dialx GOs PA BO [BM]
\is action_corps
\ps v
\ge allonger (le pas ou le bras)
\dn pour saisir qqch
\ge étendre (bras, jambe)
\xv i tae kòò-n
\dialx BO PA
\xn elle étend ses jambes
\dt 08/Feb/2025

\lx ta-ecâna
\dialx GO
\is grammaire_temps
\ps ADV
\ge toujours
\nt selon Haudricourt ; non vérifié
\dt 27/Mar/2022

\lx tagi
\dialx PA BO
\sn 1
\is cordes
\ps n
\ge toron de corde
\sn 2
\is action_corps
\ps v
\ge enrouler (s')
\dn se dit d'une liane autour d'un arbre, de la tige d'igname autour du tuteur
\ge emmêler (s') dans une corde
\ge prendre (se) dans un filet (en s'enroulant)
\dt 08/Feb/2025

\lx tago
\dialx GOs BO
\is action
\ps v
\ge arrêter
\xv nu cabòl yai jo wu nu tago-je
\dialx PA
\xn j'apparais devant toi pour t'empêcher (de faire qqch.)
\dt 21/Feb/2025

\lx tagooni
\dialx GOs
\is mollusque
\ps n
\ge coquille saint-jacques
\dt 29/Jan/2019

\lx tãî
\dialx PA BO
\sn 1
\is habillement
\ps n
\ge vêtements
\xv i thu tãî Kaavo
\dialx PA
\xn Kaavo s'habille
\xv kixa tãî-n
\dialx PA
\xn (il/elle) est nu(e)
\xv tãî-m
\dialx BO
\xn tes vêtements [BM]
\sn 2
\is étapes_vie
\ps n
\ge ornement de deuil
\dt 05/Nov/2021

\lx taluang
\dialx PA
\is cultures
\ps v
\ge ravager les champs (pour des animaux)
\ge ne pas respecter les règles des cultures ou de chasse
\dn ou de la nature en général
\xv la taluang êê-ny xu choval
\dialx PA
\xn les chevaux ont ravagé mes plants
\dt 08/Feb/2025

\lx ta-m
\dialx BO
\is grammaire_interpellation
\ps n.INTJ
\ge hé !
\xv ta-m !
\xn hé toi ! (BM)
\dt 21/Feb/2025

\lx tãnim
\dialx BO
\is armes
\ps n
\ge pierre noire de fronde
\nt selon Corne ; non vérifié
\dt 08/Feb/2025

\lx taru
\dialx GOs
\is igname
\ps n
\ge igname (violette)
\dt 29/Jan/2019

\lx taue
\is caractéristiques_personnes
\dialx BO
\ps v
\ge juste ; droit
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx ta-uuni
\ph ta-u:ɳi
\dialx GOs
\is action_corps
\ps v
\ge renverser ; faire tomber
\xv lha ta-uuni la-ã ba-raabwa
\dialx GOs
\xn ils ont renversé ces chaises
\dt 21/Dec/2021

\lx taxaza
\dialx GOs
\is médecine
\ps n
\ge docteur
\dt 27/Jan/2018

\lx taye
\dialx BO
\is corps
\ps n
\ge tibia
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx ta-zo
\dialx PA BO
\is caractéristiques_personnes
\ps v
\ge agile ; prompt ; vif
\xv mha ta-zo kòò-n
\dialx BO
\xn il a les jambes très agiles
\dt 26/Jan/2019

\lx te
\dialx PA
\is plantes_processus
\ps v
\ge commencer à mûrir (tubercules, fruits)
\dt 29/Jan/2019

\lx tee
\dialx PA
\is grammaire_restrictif
\ps RESTR
\ge seulement
\xv tee a-xè pòi-n
\dialx PA
\xn il a un seul enfant
\xv tee a-ru pòi-n
\dialx PA
\xn il a seulement deux enfants
\xv ra tee a-kòn (a) pòi-n
\dialx PA
\xn il n'a que trois enfants
\xv ra tee we-kòn (a) wony a nu nooli
\dialx PA
\xn je n'ai vu que trois bateaux
\dt 24/Aug/2021

\lx tee-a-xe
\dialx PA
\is grammaire_quantificateur_mesure
\ps QNT
\ge unique ; seul (+ animé)
\xv tee-a-xè pòi-n
\dialx PA
\xn son unique enfant
\cf (h)ãda
\ce seul
\dt 16/Feb/2025

\lx teele
\dialx PA
\is santé
\ps v
\ge fatigué
\dt 23/Jan/2022

\lx tèèn hãgana
\dialx PA
\is temps_deixis
\ps LOCUT
\ge aujourd'hui
\dt 28/Jan/2019

\lx tèèn ne hêbun
\dialx PA
\is temps_deixis
\ps ADV
\ge avant-hier ; il y a trois jours
\dt 31/Dec/2021

\lx tèè-na
\dialx PA BO
\is échanges
\is interaction
\ps v
\ge prêter
\dt 19/Aug/2021

\lx teevwuun
\dialx PA
\va tee-puu-n
\is action
\ps v
\ge faire avant ; commencer avant
\xv co teevwuun !
\xn commence avant !
\dt 08/Oct/2021

\lx tèng
\dialx BO
\is configuration
\ps v ; n
\ge fourche
\ge brancher
\nt selon BM
\et *saŋa
\el POc
\ea Blust
\dt 26/Mar/2022

\lx têi
\dialx GOs BO PA
\va thêi
\dialx GOs
\is corps
\ps n
\ge morve
\xv e tho têi-çö
\dialx GOs
\xn tu as le nez qui coule (lit. ta morve coule)
\xv nhile têi-çö
\dialx GOs
\xn mouche ta morve
\xv têi-m
\dialx PA
\xn ta morve
\xv e tûûne têi-n
\xn il se mouche (lit. essuie sa morve)
\dt 22/Mar/2023

\lx tèn
\dialx BO
\is navigation
\ps n
\ge pirogue double
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx tene
\dialx BO
\is religion
\ps v ; n
\ge blasphémer ; blasphème
\nt selon Dubois, non vérifié
\dt 25/Mar/2022

\lx teol
\dialx BO
\va teo
\dialx BO
\is action_corps
\ps v
\ge déchirer (tissu)
\xv i teo kii-n
\dialx BO
\xn il a déchiré son manou [BM]
\dt 26/Mar/2022

\lx terè
\dialx PA BO
\is caractéristiques_objets
\ps v.stat.
\ge mince ; fin
\dt 26/Jan/2019

\lx tewai
\dialx BO PA
\is armes
\ps n
\ge casse-tête à bout rond
\dt 27/Jan/2018

\lx texee
\dialx BO
\is action
\ps v
\ge serrer (nœud)
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx ti ?
\dialx GO PA BO
\sn 1
\is grammaire_interrogatif
\ps INT
\ge qui ?
\xv ti nye uça ne ?
\dialx GO
\xn qui est arrivé ici ?
\xv ti thoomwã ne çö kha-whili-je na
\dialx GO
\xn qui est la femme que tu as amené là ?
\xv ti je i a ?
\dialx PA
\xn qui est parti ?
\xv ti nye ?
\dialx GO
\xn qui est-ce ?
\xv ti çö ?
\dialx GO
\xn qui es-tu ?
\xv ti gi ne ?
\dialx PA
\xn qui est là ?
\xv ti nyõli ?
\dialx PA
\xn qui est là ?
\sn 2
\va ri ?
\dialx WE PA
\is grammaire_interrogatif
\ps INT
\ge de qui ? ; à qui ?
\xv hèlèè ri ?
\dialx WE PA
\xn à qui est le couteau ? c'est le couteau de qui ?
\xv hèlèè ti nye ? (*ri)
\dialx GOs
\xn à qui est le couteau ? c'est le couteau de qui ?
\xv õa ti nye ?
\dialx GOs
\xn c'est la mère de qui celle-là ?
\xv kêê ti/ri ?
\dialx PA
\xn le père de qui ?
\ng |lx{ri ?} est la forme déterminée de |lx{ti ?}, uniquement en |dialx{PA BO} (cette forme n'existe pas en GO)
\gt à qui ?
\cf da ?
\ce quoi ?
\et *(n)sai
\el POc
\dt 10/Jan/2022

\lx ti xa ?
\dialx PA
\is grammaire_interrogatif
\ps INT (indéfini)
\ge qui donc ?
\xv ti xa êgu na i cabi mwa ?
\dialx PA
\xn qui a bien pu frapper à la maison ?
\xv ti-xa na i a ?
\dialx PA
\xn qui est-ce qui est parti ?
\dt 16/Oct/2021

\lx tia
\dialx GOs BO
\va tiia
\dialx GO(s)
\is action_corps
\ps v
\ge pousser horizontalement ; pousser qqn. à faire qqch.
\xv tia loto
\xn pousser une voiture
\dt 29/Mar/2022

\lx tibi
\dialx GOs
\is couture
\ps v
\ge coudre
\dt 22/Mar/2023

\lx tibö
\dialx PA
\va tibu
\dialx PA
\is classificateur_numérique
\ps CLF.NUM
\ge grappe
\dn de tomates ou de fruits en grappe
\xv tibö-xe tibö tomwa
\xn une grappe de tomates (lit. une grappe grappe de tomates)
\ng |lx{tibö} ne s'emploie plus au-delà de "un"
\dt 08/Feb/2025

\lx tigi
\hm 1
\dialx GOs PA BO
\va tigin
\dialx WE
\sn 1
\is caractéristiques_objets
\ps v
\ge emmêlé ; inextricable ; embrouillé
\ge coincé ; bloqué ; enchevêtré
\ge pris (dans un filet)
\xv e tigi pu-bwaa-çö
\dialx GOs
\xn tes cheveux sont emmêlés
\sn 2
\is action
\ps v
\ge mettre des obstacles ; entraver
\xv la pe-tigi
\dialx GOs
\xn ils sont en conflit (bloqués par des problèmes)
\xv li pe-tigin
\dialx WE
\xn ils sont en conflit
\se pa-tigi-ni
\sge mettre des obstacles
\sn 3
\is coutumes
\ps n
\ge difficultés
\dn liées aux cérémonie funéraires, réfère aux perturbations de l'ordre social, selon D. Bretteville
\dt 08/Feb/2025

\lx tigi
\hm 2
\dialx GOs PA BO
\sn 1
\is caractéristiques_objets
\ps v
\ge englué ; collé
\sn 2
\is instrument
\ps n
\ge bâton à glu
\dn la glu provient du fruit du gommier, ce bâton est utilisé pour attraper les cigales en leur collant les ailes
\dt 25/Aug/2021

\lx tigi
\hm 3
\dialx GOs BO PA
\sn 1
\is végétation
\ps n
\ge forêt impraticable ; fourré
\sn 2
\is caractéristiques_objets
\ps v.stat.
\ge dense ; épais
\xv kò tigi
\dialx BO
\xn forêt dense
\dt 26/Mar/2023

\lx tii
\dialx GOs
\va tii-n
\dialx PA BO
\is fonctions_intellectuelles
\ps v ; n
\ge écrire ; écriture
\ge marquer ; graver
\xv tii-n
\dialx BO
\xn son écriture
\xv i tii yaal na bwa cee
\dialx BO
\xn il écrit/grave son nom sur l'arbre
\se we-tii
\dialx GOs
\va we-tiin
\dialx PA BO
\sge encre
\dt 12/Jan/2022

\lx tii-hênu
\ph ti:hêɳu
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge dessiner
\dt 02/Jan/2022

\lx tiiu
\dialx BO
\is santé
\ps v
\ge piquer ; démanger
\xv i tiiu
\xn ça pique; ça démange (comme une plaie sous l'effet de l'alcool [BM])
\dt 09/Jan/2022

\lx tiivwo
\dialx GOs PA
\is fonctions_intellectuelles
\ps n
\ge lettre ; livre ; écrits
\dt 18/Mar/2021

\lx tiivwo kabu
\dialx GOs
\is religion
\ps n
\ge Bible
\dn (lit. livre sacré)
\dt 08/Feb/2025

\lx tikudi
\dialx GOs
\is eau_topographie
\ps n
\ge havre
\dt 29/Jan/2019

\lx tili
\dialx GOs
\is plantes_processus
\ps v
\ge multiplier (se) ; faire des feuilles (arbres)
\xv e tili cee
\xn l'arbre se couvre de feuilles
\xv e tili kui
\xn l'igname fait des feuilles
\dt 08/Nov/2021

\lx tio
\dialx PA
\sn 1
\is action_corps
\ps v
\ge déchirer ; faire un accroc
\xv i tio patalõ
\dialx PA
\xn il a fait un accroc à son pantalon
\xv i tio kii-n
\dialx PA
\xn il a déchiré son manou
\sn 2
\is action_corps
\ps v
\ge érafler ; écorcher (s')
\xv tio kòò-n
\dialx PA
\xn éraflure ; écorchure sur sa jambe
\xv i kha-tio kòò-n
\dialx PA
\xn il s'est écorché la jambe (en marchant)
\dt 22/Feb/2025

\lx tithaa
\dialx PA
\is action
\ps v
\ge effleurer ; ricocher
\dt 29/Jan/2019

\lx tiwã
\dialx GOs
\is déplacement
\ps v
\ge passer sous une barrière
\dt 22/Mar/2023

\lx tixãã
\dialx WE
\va tilixãi
\dialx BO
\is sentiments
\ps v
\ge colère (être très en colère)
\dt 18/Mar/2021

\lx ti-yaai
\dialx GOs
\is feu
\ps n
\ge suie (du feu)
\dn suie accumulée sur le plafond ou les marmites
\xv bö ti-yaai
\xn l'odeur du feu
\dt 23/Mar/2019

\lx tiyôô
\dialx PA
\is déplacement
\ps v
\ge passer sous une barrière
\dt 27/Jan/2018

\lx tò
\dialx GO
\is caractéristiques_objets
\ps v.stat.
\ge mou ; lisse (cheveux)
\dt 27/Jan/2018

\lx tò do
\dialx GOs PA BO
\is guerre
\ps v
\ge paix (faire la)
\dn (lit. lancer la sagaie)
\cf tòè
\ce lancer
\dt 09/Feb/2025

\lx tòè
\dialx GOs PA BO
\sn 1
\is armes
\ps v
\ge lancer (sagaie, etc.)
\xv nu tò do
\dialx GOs
\xn je lance la sagaie/fais la paix
\xv mõ a tò do
\dialx GOs
\xn on va faire du lancer de sagaie/faire la paix
\xv nu tòè
\dialx PA
\xn je l'ai lancée
\sn 2
\is action_corps
\ps v
\ge piquer avec la sagaie
\ge donner un coup
\xv i tò je doo-n, tò-kòòl ni wãge-n
\dialx BO
\xn il lance sa sagaie, qui se fiche dans sa poitrine [Coyaud]
\se tò-puu
\sge pousser avec la perche (bateau)
\se tò-vao (< tòè-pao)
\sge piquer avec la sagaie
\sn 3
\is déplacement
\ps v
\ge filer comme une flèche ; courir à toute allure
\xv e za tòè ã ẽnõ-ba
\dialx GOs
\xn l'enfant là-bas court à toute vitesse
\sn 4
\is jeu
\ps n
\ge figure du jeu de ficelle "la sagaie"
\dt 21/Dec/2021

\lx tòè kîga
\dialx GOs
\is interaction
\ps v
\ge éclater de rire
\xv tòè-da kîga
\xn (il) éclate de rire
\dt 21/Mar/2023

\lx tòè-nããn
\dialx PA BO
\is interaction
\ps v
\ge lancer des injures, des offenses (piquer)
\ge injurier ; offenser
\xv li pe-tòè-li nããn
\dialx PA
\xn ils se lancent des insultes mutuellement
\cf pao paxa-nããn
\dialx PA
\ce lancer des injures/des offenses
\dt 22/Feb/2025

\lx tòò
\hm 1
\dialx GOs PA BO WEM
\sn 1
\is fonctions_naturelles
\ps v.stat.
\ge chaud (avoir)
\xv nu tòò
\dialx GO
\xn j'ai chaud
\xv tòò a
\dialx GO
\xn le soleil chauffe
\xv mhã tòò-raa !
\xn il fait trop chaud !
\se mhã tòò
\sge très chaud
\se kavwö mhã tòò
\sge pas trop chaud
\se we tòò/we-ròò
\sge eau chaude/brûlante
\ng causatif: |lx{pa-tòò-e dröö}
\gt réchauffer la marmite
\sn 2
\is feu
\ps v
\ge rougi ; enflammé |dialx{BO}
\ge brûler ; brûlant
\xv i tòò hii-m ?
\dialx BO
\xn tu t'es brûlé la main ?
\xv tòò nye kinii-n
\dialx WEM
\xn son oreille est brûlée
\dt 22/Feb/2025

\lx tòò
\hm 2
\dialx PA BO WEM
\is interaction
\ps v
\ge rencontrer ; trouver qqn.
\xv yò thaa tòò-nu mwã pe-pwexu
\dialx BO
\xn vous deux êtes venus me trouver pour que nous (pl.inclusif) discutions
\ng v.t. |lx{tòòli}
\gt trouver qqch./qqn.
\dt 22/Feb/2025

\lx töö
\hm 1
\dialx GOs BO
\sn 1
\is tressage
\ps v
\ge couper en lanières
\dn des fibres de pandanus pour le tressage
\xv e töö nu-pho
\xn elle coupe en lanières des fibres à tresser
\sn 2
\is action_plantes
\ps v
\ge écorcer (enlever les épines)
\dt 08/Feb/2025

\lx töö
\hm 2
\dialx GOs PA WEM
\va too
\dialx PA BO
\is déplacement
\ps v
\ge ramper (enfant)
\ge marcher à quatre pattes
\ge hisser (se)
\ge déplacer (se) sans bruit
\ge aller de nuit taper à la fenêtre d'une fille
\xv e thumenõ kha-töö
\dialx GO
\xn il marche courbé (vieillard)
\se kô-töö
\sge incliné
\cf beela
\dialx GOs
\ce ramper (reptile)
\dt 10/Jan/2022

\lx töö-pho
\dialx GOs
\is action_corps
\ps n
\ge faire des lamelles de feuilles de pandanus
\dt 27/Jan/2018

\lx tòòri
\dialx GOs
\va toorim
\dialx PA
\is préparation_aliments
\ps v
\ge brûlé ; carbonisé (au fond de la marmite)
\xv e tòòri lai
\dialx GOs
\xn le riz est brûlé
\xv tõne bö toorim
\dialx PA
\xn ça sent le (l'odeur de) brûlé
\dt 22/Feb/2025

\lx tòòwu
\dialx BO [BM]
\va thòòwu
\is grammaire_modalité
\ps MODAL.n
\ge vouloir
\xv i tòòwu-ny na nu phe
\dialx BO
\xn je veux le prendre
\nt non vérifié
\dt 03/Feb/2025

\lx toro
\dialx BO
\is reptile
\ps n
\ge gecko ; margouillat
\cf majo
\dt 29/Jan/2019

\lx tou
\dialx GOs PA BO
\va toe
\dialx BO
\wr A
\is échanges
\ps v ; n
\ge partager ; distribuer
\ge partage (dans les fêtes coutumières)
\se pe-tou
\dialx PA
\sge se partager
\se phwe-tou
\dialx PA
\sge recevoir en partage
\xv mi pe-toe la ãbaan
\dialx BO
\xn nous nous sommes partagés les parts
\wr B
\is classificateur_numérique
\ps CLF.NUM
\ge CLF des aliments enveloppés dans des feuilles
\ge tas
\dn distribué dans des cérémonies coutumières
\xv tou-xè, tou-tru, etc.
\dialx GOs
\xn un, deux tas, etc.
\xv içö tou-xè
\dialx GOs
\xn un tas pour toi
\dt 22/Feb/2025

\lx tò-vao
\dialx GOs
\is pêche
\ps v
\ge piquer à la sagaie
\mr |lx{tòè-pao} (lit. piquer-jeter)
\dt 08/Jan/2022

\lx tua
\dialx GOs BO PA
\is action_corps
\ps v
\ge dénouer ; défaire (nœud) ; détacher
\xv e pe-tua hãda
\dialx GOs PA
\xn il s'est défait tout seul
\cf thaò
\ce dénouer
\dt 20/Aug/2021

\lx tua pwaala
\dialx BO
\is navigation
\ps v
\ge tirer des bords
\xv i tua pwaala wony
\xn le bateau tire des bords
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx tubun
\dialx PA BO [Corne]
\is discours
\ps v
\ge grommeler ; grogner
\dt 28/Jan/2019

\lx tuu
\dialx GOs
\is habitat
\ps v
\ge déménager
\ge partir avec toutes ses affaires
\dt 24/Aug/2021

\lx tûû
\dialx GOs
\is action_corps
\ps v
\ge érafler (s') ; égratigner (s')
\dt 27/Jan/2018

\lx tuuçò
\dialx GOs
\ph tu:ʒɔ
\va tuujong tuuyòng
\dialx PA BO [BM]
\sn 1
\is santé
\ps n
\ge froid ; fièvre
\xv mã tuuçò
\dialx GOs
\xn fièvre (lit. maladie froid)
\xv e phe-je xo tuuçò
\dialx GOs
\xn il a de la fièvre (lit. le froid l'a pris)
\xv e tròòli tuuçò
\dialx GO
\xn il a attrapé froid
\xv nu tòòli tuujong
\dialx BO
\xn j'ai attrapé froid
\xv nu mã o/u tuuyòng
\dialx PA
\xn je meurs de froid
\sn 2
\is température
\ps v
\ge faire froid
\ge refroidir ; refroidi
\xv i tuujong
\dialx BO
\xn il fait froid
\xv i tuujong aari
\dialx BO
\xn le riz est froid
\ng causatif: |lx{pha-tuuço-ni}
\gt refroidir
\dt 22/Mar/2023

\lx tûûne
\dialx GOs PA BO
\is action_corps
\ps v
\ge essuyer
\ge nettoyer
\ge ratisser
\xv e tûûne-je
\dialx GOs PA
\xn il s'essuie
\xv nu tûûne-nu
\dialx GOs
\xn je m'essuie
\dt 05/Jan/2023

\lx tuuwa
\dialx BO
\is mouvement
\ps v
\ge glisser (se) ; se faufiler
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx tha
\hm 1
\dialx GOs
\is action
\ps v
\ge dévier ; déraper
\xv e tha na i je
\xn il l'a dévié de lui
\dt 22/Mar/2023

\lx tha
\hm 2
\dialx GOs BO PA
\is action_plantes
\is préparation_aliments
\ps v
\ge écorcer (du coco sur un épieu) ; éplucher (coco)
\se tha nu
\sge éplucher du coco
\dt 24/Aug/2023

\lx tha
\hm 3
\dialx GOs
\is poisson
\ps n
\ge mulet "queue bleue" (de taille juvénile)
\sc Crenimugil crenilabris
\scf Mugilidés
\dt 27/Aug/2021

\lx tha
\hm 4
\dialx GOs
\is navigation
\ps n
\ge pagaie ; perche
\dt 10/Feb/2019

\lx tha ê
\dialx GOs
\va tho êm
\dialx PA
\is action_plantes
\is cultures
\ps v
\ge cueillir la canne à sucre
\dt 26/Jan/2019

\lx tha nu
\ph tʰa ɳu
\dialx GOs PA BO
\is action_plantes
\is préparation_aliments
\ps v
\ge écorcer le coco (sur un épieu)
\ng v.t. |lx{thaxe}
\dt 24/Aug/2023

\lx tha nhyôgò we
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge sonder la profondeur de l'eau
\dt 22/Mar/2023

\lx thaa
\hm 1
\dialx GOs
\sn 1
\is nourriture
\ps v
\ge piquer (nourriture) ; piquant
\ge pimenté
\sn 2
\is nourriture
\ps n
\ge piment rouge
\dt 30/Dec/2021

\lx thaa
\hm 2
\dialx PA BO
\is déplacement
\ps v
\ge arriver ; passer par ; aller
\xv i thaa-du bwa pô
\dialx PA
\xn il arrive sur le pont
\xv i thaa-da-mi
\xn il monte vers ici
\se i thaa-ò tha-mi
\dialx PA
\sge il va de ci de là ; il va et vient ; il fait les cent pas
\se thaa-da/thaa-du
\dialx PA BO
\sge jusqu'à vers le haut/jusqu'à vers le bas
\se thaa-e
\dialx PA
\sge arriver d'une direction transverse
\dt 22/Feb/2025

\lx thaa
\hm 3
\dialx GOs BO PA
\is cultures
\ps n
\ge tuteur d'ignames (petit)
\cf du
\ce tuteur d'ignames (grand)
\dt 26/Jan/2019

\lx thãã
\dialx PA BO
\is oiseau
\ps n
\ge héron des rivières
\sc Ardea novæhollandiæ nana; Ardea sacra albolineata
\scf Ardéidés
\dt 24/Feb/2025

\lx thaaboe
\dialx GOs BO WEM
\va thaaboi, thabui, thabwi, thaabwe
\dialx GO(s) WEM BO
\sn 1
\is maison
\ps v
\ge couvrir (un objet)
\xv li u thaabwe mwa
\dialx BO
\xn ils couvrent la maison
\sn 2
\is action_corps
\ps v
\ge mettre à l'abri ; couvrir
\xv cö thaaboe na ni kûdi mwa
\dialx GO
\xn mets-la à l'abri dans un coin de la maison
\xv i thaaboe-la
\dialx WEM
\xn elle les couvre (d'une couverture)
\dt 21/Mar/2023

\lx thaabwi
\dialx GOs PA BO
\is soin
\ps v
\ge nettoyer
\ge soigner
\ge réparer
\xv haxe na ênè çö khõbwe vwo nu thaabwe na le nhye pòò-nu
\dialx GOs
\xn mais c'est à cet endroit que tu m'as dit que je prenne soin de cette noix de coco
\ng v.t. |lx{thaabwi} (+ objet pronominal), |lx{thaabwe} (+ objet nominal)
\dt 02/Mar/2023

\lx thaam !
\dialx PA
\va thãã
\dialx GO
\is discours_interjection
\ps INTJ ; appel respectueux à une pers.
\ge hé dites-moi ! ; écoutez !
\dt 08/Feb/2025

\lx thaao
\dialx BO
\is caractéristiques_objets
\ps v.stat.
\ge rugueux
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx thaa-tia
\dialx GOs BO
\va thaa-zia
\dialx GOs
\is action_corps
\ps v
\ge pousser (qqn.)
\cf tia
\ce pousser (voiture)
\dt 17/Feb/2025

\lx thaavwan
\dialx PA BO
\va thaavan
\is eau_marée
\ps n
\ge marée basse ; le rivage à marée basse
\xv thaavan gòòn-al
\dialx BO
\xn marée basse de l'après-midi
\xv thaavan thõbwõn
\dialx BO
\xn marée basse du soir
\dt 23/Aug/2021

\lx thaavwo
\dialx PA
\is action
\ps v
\ge créer ; faire qqch. ; façonner
\ng v.t. |lx{thaavwoni}
\dt 29/Mar/2022

\lx thaavwu
\dialx GOs
\va thaapu
\dialx GO(s)
\va thaavwun, thaapun
\dialx PA
\va taavwun, taapun
\dialx BO
\va teewun
\dialx WEM
\is grammaire_aspect
\ps v
\ge commencer ; mettre à (se)
\xv e thaavwu pujo
\dialx GO
\xn elle se met à faire la cuisine
\xv èńiza nye çö thaavwu pîînã-du êbòli bwabu ?
\dialx GO
\xn quand es-tu allée en France pour la première fois ?
\xv li thaapu u wa
\dialx GO
\xn ils commencent à chanter
\xv nu taavwu pwexu
\dialx BO
\xn je commence à parler
\ng v.t. |lx{thaavwuni, tavwune}
\dt 05/Jan/2022

\lx thaawe
\dialx PA
\is action
\ps v
\ge garder précieusement ; conserver
\dt 29/Jan/2019

\lx thaaxô
\dialx GOs
\va thaxõõ
\dialx PA BO
\is action_corps
\ps v
\ge empêcher (bagarre) ; arrêter (qqn.)
\ge bloquer ; barrer (route) ; empêcher (de se déplacer)
\xv e thaaxô-nu
\dialx GOs
\xn il m'a arrêté
\xv i thaxõõ-nu
\dialx PA BO
\xn il m'a arrêté
\xv lha thaaxô-la vwö/pu kêbwa ne lha a
\dialx GOs
\xn ils les ont empêché de partir
\xv e thaaxô-ni loto
\dialx GOs
\xn il a arrêté la voiture
\ng v.t. |lx{thaaxô} (+ objet pronominal) ; |lx{thaaxô-ni} (+ nom commun)
\dt 17/Feb/2025

\lx thaaxôni
\ph tʰa:'ɣõɳi
\dialx GOs
\va thaxõni
\dialx BO
\va tagòni
\dialx BO arch.
\is action
\ps v.t.
\ge arrêter ; garder ; retenir
\xv nu thaaxô-ni mwã na êne
\dialx GOs
\xn je vais arrêter (l'histoire) ici
\xv lha thaaxô-ni dè
\dialx GOs
\xn ils ont bloqué la route
\dt 06/Jan/2022

\lx thabil
\dialx PA
\is richesses
\ps n
\ge ceinture de femme tressée (monnaie)
\dn ceinture faite à partir des racines de bourao tapées, lavées, séchées (selon Charles Pebu-Polae)
\cf pobil, wepòò
\dialx PA
\ce ceinture de femme (monnaie)
\dt 10/Jan/2022

\lx thabila
\dialx GOs PA
\is action
\ps v
\ge protéger ; préserver ; garder
\xv thabila-zoo-ni
\dialx GOs
\xn protège-le bien !
\dt 19/Feb/2019

\lx thabòe
\dialx GOs PA
\is action_plantes
\ps v
\ge effeuiller
\dn en pinçant et cassant la tige des feuilles avec le pouce et l'index
\xv i thabòe dòò naõnil
\dialx PA
\xn elle effeuille le chou kanak
\dt 09/Jan/2022

\lx tha-bulu-ni
\ph tʰabuluɳi
\dialx GOs
\is action_corps
\ps v
\ge entasser ; rassembler
\xv tha-bulu-ni ja !
\xn rassemble les saletés !
\cf na-bulu-ni
\ce rassembler
\dt 02/Jan/2022

\lx tha-carûni
\dialx PA
\va tha-yarûni
\dialx PA
\is feu
\ps v
\ge pousser le feu
\xv tha-yarûni yaai !
\xn pousse le feu !
\cf carû ; carûn
\ce pousser le feu
\cf pigi yaai
\ce pousser le feu
\dt 26/Jan/2019

\lx thaçe
\ph tʰadʒe
\dialx GOs
\va thaçi
\dialx GOs
\is action_corps
\ps v
\ge cogner ; frapper fort
\ge lancer fort
\dt 22/Mar/2023

\lx thadi
\dialx GOs BO
\is action
\ps v
\ge démolir (toiture de la maison, etc.) ; enlever (la paille du toit)
\ge saccager
\xv nõli je mwa, mã je i thadi u dèèn
\dialx BO
\xn regarde cette maison, car c'est celle que le vent a abîmée
\dt 01/Jan/2022

\lx thae
\ph tʰae
\dialx GOs PA BO
\is action_corps
\ps v
\ge attacher (qqch. avec une corde) ; lier
\xv i thae còval
\dialx PA
\xn il attache le cheval
\xv i thae pu-bwaa-n
\dialx PA
\xn elle s'attache les cheveux
\xv i thae-ni mwã je mwa
\xn il fait alors la ligature des gaulettes (de l'armature) de la maison
\ng v.t. |lx{thae-ni}
\dt 17/Feb/2025

\lx thaeza
\dialx GOs
\va thaila
\dialx GO
\va taela
\dialx WE
\is outils
\ps v
\ge clouer ; fixer (avec un clou)
\ge taper pour enfoncer (clou)
\dt 08/Oct/2021

\lx thagi
\dialx GOs WEM WE PA BO
\va t(h)agi
\dialx BO
\is action_corps
\ps v
\ge plumer (volaille)
\ge arracher les poils
\ge arracher (feuilles, lianes)
\xv i thagi ko
\dialx BO
\xn elle plume la poule
\dt 20/Aug/2021

\lx thai
\dialx GOs
\is habillement
\ps v
\ge enfiler (vêtement)
\xv e thai kii-je
\xn il a mis son manou
\dt 27/Jan/2018

\lx thaî
\dialx GO(s)
\va thahîn
\dialx PA BO
\is discours_tradition_orale
\ps n ; v
\ge devinette ; poser une devinette
\xv i thahîn yaa inu
\dialx BO
\xn il me pose une devinette [BM]
\dt 01/Sep/2021

\lx thãi
\dialx GOs PA BO
\is poisson
\ps n
\ge carpe
\sc Kuhlia sp.
\scf Kuhliidés
\cf zòxu
\dialx GOs
\ce carpe
\dt 10/Jan/2022

\lx thai hõbò
\dialx GOs
\is habillement
\ps v
\ge mettre ; enfiler (vêtement)
\cf udale hõbò
\ce mettre; enfiler (vêtement)
\dt 22/Jan/2018

\lx thaila
\dialx GOs
\va thaeza
\dialx GO
\va taela
\dialx WE
\va taabila, tabila
\dialx PA
\is action_outils
\ps v
\ge clouer
\dt 25/Jan/2019

\lx thaivwi
\ph tʰaiβi
\dialx GOs PA
\va thaiving
\dialx PA
\va thaaviing
\dialx BO [BM]
\sn 1
\is action_corps
\ps v.i
\ge entasser
\xv thaivwi ja !
\xn ramasse/entasse les détritus ! (fais le ramassage des détritus)
\sn 2
\is interaction
\ps v.i
\ge réunir/rassembler (se)
\se pe-thaivwi
\sge se réunir
\ng v.t. |lx{thaivwinge}
\gt réunir ; rassembler des gens
\dt 22/Feb/2025

\lx tha-khège
\is action_corps
\dialx GOs
\ps v
\ge pousser-bousculer
\xv tha-khège ã-e
\xn celui-là est bousculé
\dt 22/Mar/2023

\lx thala
\ph tʰala
\dialx GOs BO PA
\is action_corps
\ps v
\ge ouvrir
\dn une maison, boîte, marmite, etc.
\ge déboucher (bouteille)
\ge enlever (couvercle)
\xv e thala mee-je
\dialx GO
\xn il ouvre les yeux
\xv thala phwa !
\dialx GO
\xn ouvre la bouche !
\xv thala go !
\dialx GO
\xn mets la musique ! (lit. ouvre)
\xv thala we !
\dialx GO
\xn ouvre l'eau !
\xv thala phwèè-mwa !
\dialx GO
\xn ouvre la porte !
\xv e thala-da phwèè-mwa
\dialx PA
\xn elle ouvre la porte (pour entrer)
\xv e thala-du phwèè-mwa
\dialx PA
\xn elle ouvre la porte (pour sortir)
\xv thala doo !
\dialx PA
\xn ouvre la marmite !
\xv thala bwat !
\dialx PA
\xn ouvre la boîte
\dt 08/Feb/2025

\lx thalei
\dialx WEM PA BO
\is maison
\ps n
\ge chambranles sculptées de porte
\cf drõgò
\ce masque
\cf throo-mwa ; thoo-mwa
\ce flèche faitière
\dt 10/Jan/2022

\lx thali
\dialx PA BO
\sn 1
\is mouvement
\ps v
\ge buter (sur qqch.) ; trébucher
\xv i thali kò na ni we-cee
\dialx PA
\xn il s'est pris les pieds dans les racines de l'arbre
\xv i thali kò bwa paa
\dialx PA
\xn il a trébuché sur une pierre
\sn 2
\is action_plantes
\ps v
\ge gauler (fruit)
\sn 3
\is action_corps
\ps v
\ge taper
\dt 17/Feb/2025

\lx thane
\dialx BO
\is préparation_aliments
\ps v
\ge chauffer
\et *raraŋ
\eg heat sthg or o.s by fire
\el POc
\ea Blust
\nt selon BM ; non verifié
\dt 27/Mar/2022

\lx thãne
\dialx PA
\is armes
\ps n
\ge pierre de fronde (grosse)
\dt 27/Jan/2018

\lx thani
\ph tʰaɳi
\dialx GOs WEM WE
\is caractéristiques_personnes
\ps v
\ge vif ; dynamique ; en forme
\dt 26/Jan/2019

\lx thano
\dialx BO
\is plantes
\ps n
\ge fougère lianescente
\scf Schizéacée
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx tha-nhyale
\dialx GOs
\is action_corps
\ps v
\ge fendre et couper en morceaux (citrouille) ; écraser
\cf tha-
\ce taper ou piquer avec qqch.
\dt 22/Mar/2023

\lx thaò
\dialx GOs BO
\va thawa
\dialx GO(s)
\is action_corps
\ps v
\ge dénouer (corde) ; défaire (nœud) ; relâcher un peu
\cf tua
\ce nouer
\dt 26/Jan/2019

\lx tha-pwe
\dialx GOs PA BO
\is pêche
\ps v
\ge pêcher à la ligne
\dt 26/Jan/2019

\lx thathibul
\dialx PA
\is jeu
\ps v
\ge jeu
\dn on provoque une explosion en emprisonnant de l'air entre les mains et en les croisant l'une sous l'autre sous l'eau
\dt 22/Oct/2021

\lx thatra-hi
\ph tʰaʈahi
\dialx GOs
\sn 1
\is configuration
\ps n
\ge bouquet de paille à la main
\sn 2
\is action_corps
\ps v
\ge brandir dans la main
\dt 02/Jan/2022

\lx tha-truãrôô
\ph 'tʰa'ʈuɛ̃ɽõ: ; tʰaʈʰuɛ̃ɽõ:
\dialx GOs PA
\va thruatrôô
\dialx arch. (Haudricourt)
\va ta-tuarô
\dialx BO [BM]
\is insecte
\ps n ; v
\ge toile d'araignée (faire une)
\dt 08/Feb/2025

\lx tha-thrûã
\dialx GOs BO
\va tha-thûã, ta-thûã
\dialx BO
\is jeu
\ps v ; n
\ge jeu de ficelle ; jouer au jeu de ficelle
\dt 15/Aug/2021

\lx thau
\dialx GOs
\va thaul
\dialx PA BO
\is armes
\ps n
\ge pierre de fronde
\dn allongée et polie aux deux bouts
\xv paa-thaul
\dialx BO PA
\xn pierre de fronde
\dt 08/Feb/2025

\lx tha-uji
\dialx BO
\va taauji
\dialx BO
\is action_corps
\ps v
\ge pousser et faire tomber à la renverse
\nt selon BM
\dt 26/Mar/2022

\lx thaxe
\dialx GOs BO
\sn 1
\is feu
\ps n
\ge cadre du foyer
\xv thaxe-ã
\xn notre foyer
\sn 2
\is maison
\ps n
\ge pierre de seuil |dialx{BO}
\dt 23/Jan/2022

\lx thaxebi
\ph 'tʰaɣebi
\dialx GOs BO
\is interaction
\ps v
\ge accuser ; calomnier ; diffamer
\xv e thaxebi-ni êmwê
\dialx GOs
\xn il a accusé l'homme
\dt 23/Dec/2021

\lx thaxee-phwè-mwa
\dialx GOs BO
\va taage, thaaxe
\dialx BO
\sn 1
\is maison
\ps n
\ge seuil de la porte
\ge pierre de seuil
\sn 2
\is feu
\ps n
\ge pierre autour du foyer |dialx{BO}
\nt selon BM
\dt 26/Mar/2022

\lx thaxiba
\dialx GOs PA
\va thakiba
\dialx GO(s)
\va taxiba
\dialx BO PA
\sn 1
\is interaction
\ps v
\ge refuser ; rejeter ; chasser |dialx{PA}
\ge abandonner ; délaisser
\sn 2
\is interaction
\ps v
\ge moquer (se) ; mépriser
\sn 3
\is coutumes
\ps v
\ge répudier (femme) ; congédier
\cf paree
\ce délaisser
\dt 09/Jan/2022

\lx thaxim
\dialx PA BO
\is mouvement
\ps v
\ge rouler (se) par terre
\ge frotter (se) par terre
\ge vautrer (se)
\dt 13/Jan/2022

\lx thazabi
\dialx GOs
\va tharabil
\dialx PA
\sn 1
\is action_corps_animaux
\ps v
\ge frétiller (poisson)
\ge battre des ailes
\sn 2
\is mouvement
\ps v
\ge rouler (se) par terre (animal, enfant)
\dt 20/Jan/2019

\lx thê
\dialx GOs
\va thê, thã
\dialx BO
\is oiseau
\ps n
\ge héron de nuit
\sc Nycticorax caledonicus caledonicus
\scf Ardéidés
\dt 22/Mar/2023

\lx thebe
\dialx GOs
\va tebe
\dialx BO [Corne]
\is action_plantes
\ps v
\ge tailler (arbre, plante)
\ge appointer
\ge éplucher
\dn en taillant avec un geste orienté vers l'extérieur
\dt 20/Feb/2025

\lx thee
\dialx GOs
\va tioo
\dialx PA
\is action_corps
\ps v
\ge déchirer en long
\dt 22/Mar/2023

\lx theepwa
\dialx GOs
\is déplacement
\ps v
\ge galoper
\dt 27/Jan/2018

\lx thèl
\dialx PA BO
\is cultures
\ps v.i
\ge débroussailler
\dn des champs ou des chemins au sabre d'abattis
\ge défricher
\se whara-a thèl ; whara-ò thèl
\sge époque du débroussaillage (mai)
\ng v.t. |lx{thèli}
\dt 08/Feb/2025

\lx thele
\dialx GOs
\is chasse
\is pêche
\is guerre
\ps v
\ge planter (sagaie) ; frapper
\dt 26/Jan/2019

\lx thele paa
\dialx GOs
\is mouvement
\ps v
\ge ricocher (faire) un caillou
\dt 27/Jan/2018

\lx thèmi
\dialx BO
\is action_tête
\ps v
\ge lécher
\nt selon BM
\cf maalemi
\dialx BO
\ce lécher
\cf thami
\dialx langue caac
\ce lécher
\et *samu(k)
\el POc
\dt 26/Mar/2022

\lx the-paò
\dialx PA
\va teevaò
\dialx BO
\is action_eau_liquide_fumée
\ps v
\ge jeter (parfois dans l'eau)
\dt 26/Aug/2023

\lx theul
\dialx PA BO
\is poisson
\ps n
\ge mulet de rivière
\dn mulet juvénile, il remonte le cours des rivières, puis devient |lx{naxo} à l'âge adulte
\sc Cestræus plicatilis
\scf Mugilidés
\et *(s,d)aud
\el POc
\cf teut
\dialx langue caac
\dt 10/Jan/2022

\lx thi
\hm 1
\ph tʰi
\dialx GOs BO PA
\is corps
\ps n
\ge sein
\ge mamelle
\se we-thi
\sge lait maternel
\se me-thi
\sge mamelon
\xv thi-n
\dialx PA
\xn son sein
\xv pwò-thi-n
\dialx BO
\xn son mamelon
\et *susu
\eg sein
\el POc
\dt 21/Aug/2021

\lx thi
\hm 2
\dialx GOs
\is santé
\ps n
\ge bouton ; acné
\dt 25/Jan/2019

\lx thi
\hm 3
\dialx GOs PA BO
\sn 1
\is action
\ps v
\ge exploser ; éclater
\se thi-pholo
\sge taper dans l'eau
\xv e thi niô
\dialx GOs
\xn le tonnerre tonne
\cf hû niô
\dialx GOs
\ce le tonnerre gronde
\cf i hûn (e) nhyô
\dialx PA
\ce le tonnerre gronde
\sn 2
\is son
\ps n
\ge détonation ; coup (de fusil)
\se thixa jige
\dialx GOs
\sge un coup de fusil
\xv nu trõne thixa õ-xè
\dialx GOs
\xn j'ai entendu une fois un coup de fusil
\xv nu trõne thixa ne phwa-xè, phwa-tru
\dialx GOs
\xn j'ai entendu un coup de fusil, deux coups (de fusil)
\xv nu trõne thixa xa phwa-tru
\dialx GOs
\xn j'ai entendu deux coups de fusil
\xv nu trõne thixa jige xa phwa-kò
\dialx GOs
\xn j'ai entendu trois coups de fusil
\xv i thi je phwa-xe
\dialx PA
\xn il y a eu une détonation
\xv i thi jigèl
\dialx BO
\xn il y a eu un coup de fusil
\ng v.t. |lx{thile} |dialx{PA}
\gt toucher avec une balle de fusil
\dt 04/Apr/2023

\lx thi parô
\dialx GOs
\is soin
\ps v
\ge curer (se) les dents
\dt 06/May/2024

\lx thi yaai
\dialx GOs PA WEM BO
\is feu
\ps v
\ge allumer un feu de brousse ; mettre le feu
\dn (lit. piquer le feu),  allumer un feu avec des palmes de coco secs ou des brindilles enflammées
\ge attiser le feu en remuant les braises avec un bâton [PA]
\xv e pe-thi
\dialx GOs
\xn il a mis le feu
\xv êgu xa a-pe-thi
\dialx GOs
\xn un pyromane
\se a-pe-thi
\sge pyromane
\se nõbo-yaai
\sge brûlure; traces de feu
\dt 09/Feb/2025

\lx thibe
\ph tʰibe
\dialx GOs PA BO
\va thebe
\dialx GO(s) BO
\is préparation_aliments
\ps v
\ge éplucher ; peler
\ge gratter avec un couteau
\dn des légumes, ignames ou taros crus, des fruits
\xv nu thebe kumwala o hèlè-m
\dialx BO
\xn j'ai pelé la patate douce avec ton couteau
\cf pwaji, pwayi
\ce éplucher ; peler (avec les doigts, banane, manioc, tubercule cuit)
\dt 22/Feb/2025

\lx thi-bö
\dialx GOs
\va bwo
\dialx BO
\is feu
\ps v
\ge éteindre (petit feu, lumière)
\ng v.t. |lx{thi-böö-ni}
\cf khi-böö
\dialx GOs WEM WE
\ce éteindre le feu
\dt 22/Feb/2025

\lx thi-bööni
\ph tʰi'bɷ:ɳi
\dialx GOs
\va bwo, bo
\dialx BO
\is feu
\ps v
\ge éteindre (un feu)
\dn en fouillant et dispersant les braises
\se thi-bööni yaai
\dialx GO
\sge éteindre le feu en fouillant
\dt 08/Feb/2025

\lx thibuyul
\dialx PA
\is mouvement
\ps v
\ge rebondir et revenir en sens inverse
\dt 27/Jan/2018

\lx thi-bwagil
\dialx PA BO
\is position
\is mouvement
\ps v
\ge agenouiller (s')
\ge genoux (être à)
\dt 25/Aug/2021

\lx thiçe
\ph tʰiʒe
\dialx GOs
\sn 1
\is feu
\ps v
\ge remuer les braises
\sn 2
\is préparation_aliments
\ps v
\ge cuire sous la cendre ; mettre à cuire sous les braises
\xv e thiçe pò-vwale
\xn il grille un épi de maïs sous la cendre
\dt 23/Aug/2021

\lx thiçoo
\is mouvement
\ph tʰiʒo:
\dialx GOs
\va ticul
\dialx PA
\ps v
\ge rebondir
\dt 25/Aug/2021

\lx thidin
\dialx PA BO WEM
\is sentiments
\ps v.stat.
\ge coléreux ; en colère ; irrité
\xv i thidin i nu
\dialx PA
\xn il est en colère contre moi
\xv i thidin puni nu
\dialx PA
\xn il est en colère contre à cause de moi
\dt 27/Jan/2019

\lx thige
\dialx PA WE
\va thege
\dialx BO
\is couture
\ps v
\ge coudre ; piquer
\se ba-thige
\dialx PA
\va ba-thege
\dialx BO
\sge fil à coudre
\et *saqit
\eg sew
\ea Blust
\el POc
\dt 12/Jan/2022

\lx thi-gu
\dialx GOs BO
\va thi-gu(a)
\dialx PA
\is pêche
\ps v
\ge enfiler (sur une filoche)
\xv e thi-gua nõ
\dialx GOs
\xn il met des poissons sur la filoche
\xv i thi-gua nõ
\dialx BO
\xn il enfile des poissons sur la filoche
\xv e thi-gua-ni li-ne nõ
\dialx GOs
\xn il a enfilé ces deux poissons sur la filoche
\se gua nõ
\sge filoche de poissons
\ng forme déterminée de |lx{gu > gua}
\ng v.t. |lx{thi-gua-ni}
\dt 06/Jan/2022

\lx thii
\hm 1
\dialx GOs PA BO WEM
\sn 1
\is action_corps
\ps v
\ge piquer ; faire une piqûre
\ge fouiller (dans un trou avec un bâton)
\ge extraire (épine)
\se thi kili
\dialx PA
\sge piquer la navette
\se thii phwè-pwaaji
\dialx GOs PA
\sge piquer/fouiller dans un trou (à la recherche de crabe)
\xv i ra u phe deang, thii ne phwe-keel ra a
\dialx PA
\xn elle prend l'épuisette, la met dans l'ouverture du panier et part
\sn 2
\is action_corps
\ps v
\ge provoquer
\xv la pe-thii thô nai la
\dialx GOs
\xn ils se provoquent (piquent la colère)
\cf thaa, thòzò
\ce piquer
\et *susuRi
\eg piquer, coudre
\el POc
\ea Blust
\dt 04/Apr/2023

\lx thii
\hm 2
\dialx GOs BO
\is soin
\ps v
\ge peigner ; peigner (se)
\xv nu thii-vwo
\dialx GOs
\xn je me peigne
\xv nu thii pu-bwaa-nu
\dialx GOs
\xn je peigne mes cheveux
\xv e thii-vwo ẽnõ-ã
\dialx GOs
\xn l'enfant se peigne
\xv e thuvwu thii pu-bwaa-je
\dialx GOs
\xn il se peigne lui-même
\xv e thuvwu thii pu-bwaa ẽnõ-ã
\dialx GOs
\xn l'enfant se peigne
\xv e thii pu-bwaa-je xo ẽnõ-ã
\dialx GOs
\xn l'enfant le peigne
\xv e thii pu-bwaa ẽnõ-ã
\dialx GOs
\xn il peigne les cheveux de l'enfant
\et *saRu
\eg comb
\el POc
\ea Blust
\dt 21/Dec/2021

\lx thii
\hm 3
\dialx GO BO [BM]
\va thiin
\dialx BO
\is feu
\ps v
\ge incendier ; mettre le feu
\xv i thii nõ-tòn
\dialx BO
\xn il a mis le feu aux broussailles
\dt 22/Mar/2023

\lx thii
\hm 4
\dialx GO PA WEM
\is coutumes_objet
\ps n
\ge touffe de poils de roussette et fil de coton
\dn selon [Dubois ms.], utilisée pour faire la monnaie (avec des coquillage, etc.)
\dt 09/Feb/2025

\lx thii kura
\dialx GOs PA
\is interaction
\ps v
\ge énerver ; agacer
\dn (lit. piquer le sang)
\dt 08/Feb/2025

\lx thiibu
\dialx BO
\is action_corps
\ps v
\ge appuyer (s')
\nt selon Corne
\dt 27/Mar/2022

\lx thii-du
\dialx GOs BO
\is action_corps
\ps v
\ge plonger le bras (dans une cavité, dans l'obscurité)
\dt 22/Mar/2023

\lx thii-no
\dialx BO
\is corps
\ps n
\ge fesses
\nt selon Dubois ; non vérifié
\dt 26/Mar/2022

\lx thiinyûû
\dialx GOs
\va thiyu
\dialx BO [Corne]
\is mammifères_marins
\ps n
\ge marsouin
\dt 25/Aug/2021

\lx thii-puu
\dialx GOs PA
\is cultures
\ps v
\ge planter des palmes de cocotier
\dn ou des branches d'autres arbres dans le sol, ces branchages sont plantés dans le sol pour faire des barrières, un abri de champ ou un abri temporaire
\xv i thii-puu mwa
\dialx PA
\xn il pique des palmes ou branches comme protection
\dt 08/Feb/2025

\lx thiipuun
\dialx PA
\is interaction
\ps v
\ge piquer ; provoquer
\dt 27/Jan/2019

\lx thiipuun ka thae pwaxilo, kuu mwa xa doon ku ico
\dialx PA
\va thiiphuu thahulò
\dialx GO BO
\is discours_tradition_orale
\ps LOCUT
\ge formule de fin de conte
\se thiiphuu thahulò
\dialx GO
\sge à ton tour d'en ajouter un autre (formule utilisée pour les contes)
\dt 22/Mar/2023

\lx thii-vwo
\dialx GOs
\is soin
\ps v
\ge coiffer (se)
\ge peigner (se)
\ng agramm. *nu pe-thi-vwo
\dt 05/Jan/2022

\lx thîî-yaai
\dialx WEM
\is feu
\ps v ; n
\ge étincelle du feu ; crépiter (feu)
\xv ra u a-mi thîî nye yaai
\xn une étincelle de ce feu est projetée sur elle
\dt 06/Nov/2021

\lx thi-kham
\dialx PA
\is mouvement
\ps v
\ge ricocher
\dt 27/Jan/2018

\lx thila
\dialx GOs PA BO
\va thira
\dialx BO
\sn 1
\is armes
\ps n
\ge plumet de fronde
\ge mèche (du fouet)
\se thila wèda
\dialx PA
\va thira wèdal
\dialx BO
\sge plumet de fronde
\se thila phue
\sge le bout/mèche du fouet
\cf cetil
\ce plumet de fronde
\sn 2
\is danse
\ps n
\ge bouquet de paille pour la danse
\dt 09/Jan/2022

\lx thile
\dialx PA
\is chasse
\ps v.t
\ge toucher (avec une balle de fusil)
\dt 17/Feb/2023

\lx thilò
\dialx GOs
\is grammaire_quantificateur_mesure
\ps n
\ge paire ; l'autre d'une paire
\se thilò-nu
\dialx GOs
\sge mon binôme ; la personne qui fait équipe avec moi
\xv pe-thilò-bî mã Mario
\dialx GOs
\xn nous faisons équipe Mario et moi
\xv pe-thilò-li
\dialx GOs
\xn ils font équipe ensemble
\xv pe-thilò ala-xò
\dialx GOs
\xn la paire de chaussures ; les deux chaussures
\xv a khila thilò ala-kòò-çö !
\dialx GOs
\xn va chercher ton autre chaussure !
\xv ia thilò ala-xòò-çö ?
\dialx GOs
\xn où est ton autre chaussure ?
\cf thixèè ala-xò
\dialx GOs
\ce une seule chaussure
\dt 22/Feb/2025

\lx thi-ma-thi
\dialx GOs
\is son
\ps v
\ge pétarader
\xv e thi-ma-thi loto
\xn la voiture pétarade
\dt 27/Jan/2018

\lx thîni
\ph tʰiɳi
\dialx GOs PA BO
\is habitat
\ps n
\ge barrière ; clôture
\ge enclos
\xv thîni(v)a-nu, thînia-nu
\dialx GOs
\xn ma barrière
\se thu thîni
\dialx GOs
\sge faire une barrière
\se thîni hauva
\dialx GOs
\sge enclos
\se phwe-thîni
\dialx GO PA BO
\va phwe-zîni
\dialx GOs
\sge porte de clôture ; barrière
\se ce-thîni
\sge poteaux de barrière
\se thîni-cee
\dialx GOs
\sge une barrière en bois
\se thîni-pòl
\dialx BO
\sge une barrière en fougère
\ng forme déterminée: |lx{thini(v)a}
\dt 22/Feb/2025

\lx thîni hauva
\ph tʰiɳi hauva
\dialx GOs
\is société_organisation
\ps n
\ge enclos pour les dons coutumiers
\dn enclos ceint par une barrière où l'on met les dons coutumiers destinés au clan maternel ; les oncles maternels démolissent l'enclos et emportent alors les dons ; ceci ne se pratique que pour les personnes de rang important
\dt 22/Oct/2021

\lx thîni-a kavwègu
\ph tʰîɳi
\dialx GOs
\is habitat
\is société_organisation
\ps n
\ge palissade de la chefferie
\dt 23/Mar/2019

\lx thiò dimwa
\dialx GOs PA BO
\va thixò, thiò
\dialx PA BO
\is préparation_aliments
\ps v
\ge gratter l'igname sauvage |lx{dimwa} ; râper
\xv la thi(x)ò la ca-la dimwa
\dialx BO
\xn elles rapent leur igname sauvage à manger
\dt 30/Mar/2022

\lx thiò nu
\ph tʰiɔ ɳu
\dialx GOs PA
\va thixò
\dialx GO(s) PA BO
\is arbre_cocotier
\ps v
\ge décortiquer le coprah
\dn au couteau
\dt 08/Feb/2025

\lx thi-paa
\dialx BO
\va tho-paa
\is guerre
\ps n
\ge embuscade (guerre) ; embûches
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx thi-pöloo
\ph 'tʰiβωlo:
\dialx GOs
\va thu-vwuloo
\dialx WEM WEH
\is pêche
\ps v
\ge troubler (l'eau)
\dn par exemple pour pêcher dans la rivière
\cf p(h)öloo
\ce trouble
\dt 08/Feb/2025

\lx thi-pu
\dialx PA
\is soin
\ps v
\ge peigner (se)
\xv i thi pu-bwaa-n
\dialx PA
\xn il se peigne
\xv i thi pu Kaavo
\dialx PA
\xn Kaavo se peigne
\xv i thi puu-n
\dialx PA
\xn il se peigne
\dt 03/Feb/2019

\lx thi-puu
\dialx BO PA
\va tho-puu
\dialx GO(s) BO
\is navigation
\ps v
\ge pousser (bateau) avec la perche
\dn (lit. piquer)
\xv i tho-pue phaa-gò
\dialx BO
\xn il pousse le radeau avec une perche
\se ba-thi-puu
\dialx PA BO
\sge perche
\cf thoe, thi
\dialx GO
\ce piquer
\dt 08/Feb/2025

\lx thi-pholo
\dialx GOs BO
\is pêche
\is action_corps
\ps v
\ge taper dans l'eau pour faire du bruit
\dn et effrayer le poisson afin de le pousser dans le filet
\dt 05/Mar/2019

\lx thiram
\dialx PA
\va thiam
\dialx BO
\is danse
\ps n
\ge danse d'accueil des hommes
\dt 28/Jan/2019

\lx thiraò
\dialx GOs PA BO
\is déplacement
\va thirawa
\ps v
\ge traverser ; passer à travers ; transpercer
\xv mõ nõ thiraò hõbwòli-çö
\dialx GOs
\xn on voit à travers ta robe
\dt 22/Mar/2023

\lx thirawa
\dialx GOs
\is déplacement
\ps v
\ge traverser ; passer à travers
\xv e thirawa do
\xn la sagaie est passée à travers
\xv nu no thirawa phwè-mwa
\xn je vois à travers la porte
\xv nu no thirawa ni hõbwòli-je
\xn je vois à travers ses vêtements
\dt 23/Mar/2019

\lx thi-tou
\dialx GOs PA
\is échanges
\ps v
\ge distribuer en partage
\dt 23/Aug/2021

\lx thi-ula
\dialx PA
\is action_corps
\ps v
\ge décrocher en piquant avec un bâton (qqch. qui se trouve en hauteur)
\dt 17/Feb/2025

\lx thivwaa
\dialx GOs BO
\ph tʰiβa:
\va thipwa-n
\dialx PA
\sn 1
\is habillement
\ps n
\ge bagayou ; étui pénien
\xv thipwa-n
\dialx PA
\xn son étui pénien
\sn 2
\is pêche
\ps n
\ge flotteur de filet |dialx{BO}
\xv thivwaa pwiò
\dialx BO
\xn flotteur de filet
\dt 23/Jan/2022

\lx thivwi
\dialx GOs BO
\ph tʰiβi
\va thipi
\ph tʰipi
\dialx GO(s)
\is fonctions_naturelles
\ps v
\ge aspirer (liquide avec une paille)
\ge sucer (bonbon)
\dt 23/Aug/2021

\lx thivwölöö
\ph 'tʰiβωlω:
\dialx GOs BO
\va tivwoloo
\dialx BO
\is santé
\ps v ; n
\ge chancre
\ge pustule
\ge boutons (sur le corps)
\ge pus
\dt 22/Mar/2023

\lx thixa jige
\dialx GOs
\is armes
\va thixa jigel
\dialx PA
\ps n
\ge coup de fusil
\dt 27/Jan/2018

\lx thixèè
\dialx GOs
\va thaxee
\dialx PA
\is grammaire_quantificateur_mesure
\ps QNT
\ge un d'une paire ; un seul (d'une paire)
\xv thixèè mee-je
\dialx GOs
\xn il est borgne (il a un seul œil)
\xv thixèè ala-xòò-je
\dialx GOs
\xn il n'a qu'une seule chaussure
\xv e mhõge ala-xòò-je thixèè
\dialx GOs
\xn il n'a attaché qu'une chaussure
\xv nu tròòli thixèè ala-xòò-nu
\dialx GOs
\xn je n'ai trouvé qu'une chaussure
\xv nu tròòli-(h)ãdaa-ni thixèè ala-xòò-nu
\dialx GOs
\xn je n'ai trouvé qu'une seule chaussure
\xv ala-kò thaxee
\dialx PA
\xn une seule chaussure
\cf wè-xè hii-je
\dialx GOs
\ce il n'a qu'un seul bras
\cf thilò
\dialx GOs
\ce l'autre d'une paire
\dt 16/Feb/2025

\lx thixèè mee-je
\dialx GOs
\is santé
\ps v
\ge borgne
\xv e thixèè mee-nu
\xn je suis borgne (lit. il est unique mon œil)
\dt 25/Mar/2022

\lx thixò
\is position
\dialx GOs
\ps v
\ge mettre (se) sur la pointe des pieds
\cf ala-kò-thixò, ala-xò-thixò
\ce chaussure à talon
\mr vient de |lx{thi kò}
\dt 19/Feb/2025

\lx thixudi
\dialx PA BO
\va thivwudi
\dialx PA BO
\is configuration
\ps n
\ge coin ; angle
\xv thivwudi thîni
\xn coin de l'enclos
\cf kudi
\ce coin
\dt 26/Jan/2019

\lx thiza
\dialx GOs
\va chira
\dialx PA
\is armes
\ps n
\ge barbeau de la sagaie
\dt 08/Oct/2021

\lx thizi
\dialx GOs
\is action_corps
\ps v
\ge essuyer (s') les fesses
\xv e thizi buxè
\xn elle s'essuie (l'anus)
\dt 10/Feb/2019

\lx thizii
\dialx GOs
\va thiri
\dialx PA
\va tiri
\dialx BO WEM
\is corps_doigt
\ps n
\ge auriculaire
\se thiri-a kòò-n
\dialx PA
\sge petit orteil
\dt 22/Aug/2021

\lx tho
\hm 1
\ph tʰo
\dialx GOs PA BO
\sn 1
\is son
\ps v ; n
\ge cri ; appel;  son ; appeler
\xv ra hale tho-ny na ni cocovwa mèni
\dialx PA
\xn mon cri est différent de tous ceux des autres oiseaux
\xv tho-mi xo Honorine
\dialx WEM
\xn Honorine appelle (vers elle)
\se tho-mi
\sge appeler vers soi
\sn 2
\is musique
\ps v ; n
\ge chanter (oiseau) ; chant
\ge musique
\se tho-ko
\sge le chant du coq
\se tho-mèni
\dialx GOs
\sge le chant de l'oiseau
\xv e tho mèni
\dialx GOs
\xn l'oiseau chante
\xv tho gò
\dialx BO
\xn il y a de la musique
\xv i pa-thoni gò hu ri ?
\dialx BO
\xn qui fait jouer de la musique ?
\ng v.t. |lx{thoni}
\et *soRov(i)
\el POc
\ea *cho
\el PSO (proto-south Oceanic)
\ea Geraghty
\dt 06/Jan/2022

\lx tho
\hm 2
\ph tʰo
\dialx GOs PA BO
\is action_eau_liquide_fumée
\ps v
\ge couler (eau, sang) ; écouler (s')
\xv e tho-du na ea ?
\dialx GO
\xn où prend-il sa source ? (fleuve)
\xv tho na va ?
\dialx PA
\xn où prend-il sa source ? (fleuve)
\xv thoo-we
\dialx BO
\xn courant d'eau ; fuite d'eau ; cascade
\dt 22/Feb/2025

\lx thò
\dialx GOs
\is arbre
\ps n
\ge banian ; caoutchouc
\dt 29/Jan/2019

\lx thò-
\dialx GOs PA BO
\is classificateur_numérique
\ps CLF.NUM
\ge régime de bananes
\xv thò-xè, thò-tru, thò-kò, thò-pa, thò-ni thò-chaamwa, etc.
\dialx GOs
\xn un, deux, trois, quatre, cinq régimes de bananes, etc.
\se thò-xè thò-chaamwa
\dialx GO
\sge un régime de bananes
\dt 22/Feb/2025

\lx thô
\hm 1
\ph tʰõ
\dialx GOs
\va thòn
\dialx PA BO
\sn 1
\is action
\ps v
\ge fermer
\xv e tee thô
\dialx GOs
\xn c'est fermé
\xv e thô kinîî-nu
\dialx GOs
\xn j'ai les oreilles bouchées
\xv thòn mwã ã paa
\dialx PA
\xn la pierre ferme (l'entrée)
\ng v.t. |lx{thôni}
\sn 2
\is caractéristiques_objets
\ps v
\ge étanche
\se ba-kevwö thô
\dialx GO
\se ba-kam thô
\dialx PA
\va ba-kam thòn
\dialx BO
\sge louche
\dt 22/Mar/2023

\lx thô
\hm 2
\dialx GOs
\is interaction
\ps n ; v
\ge colère contre qqn. (être en) ; dispute
\xv e thôe-nu
\dialx GOs
\xn elle est en colère contre moi
\xv e a-ka-thô
\dialx GOs
\xn elle est susceptible; elle se met en colère pour rien
\xv li pe-thô
\dialx GOs
\xn ils sont en conflit
\xv kavwö nu trõne kaamweni nye pe-thô i li
\dialx GOs
\xn je ne comprends pas leur dispute
\dt 22/Feb/2025

\lx tho ńõ
\ph tʰo nɔ̃
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge péter ; avoir des vents
\gb pass wind
\xv i tho nõ-n
\dialx BO
\xn il fait un pet
\cf vhii
\dialx GOs
\ce péter
\dt 22/Mar/2023

\lx thò-chaamwa
\ph ,tʰɔ'tʃʰa:mwa, tʰɔ'cʰa:mwa
\dialx GOs PA BO
\is bananier
\ps n
\ge régime de bananes
\dt 02/Jan/2022

\lx thoda
\dialx PA
\is interaction
\ps n
\ge malédiction ; mauvais sort
\xv we whany mãni thoda
\xn potion/antidote contre les malédictions et mauvais sorts
\dt 09/Jan/2022

\lx thodia
\ph 'tʰodia
\dialx GOs
\va thidya, tidya
\dialx BO
\is caractéristiques_objets
\ps v.stat.
\ge rouillé
\dt 26/Jan/2019

\lx thòè
\dialx GOs
\va thòi
\dialx GOs
\va thoe, toe
\dialx BO PA
\is cultures
\ps v
\ge planter ; mettre en terre (ignames, taros)
\xv kô-raa vwö thòè lhã-nã
\dialx GO
\xn il ne pourra pas planter ces plants
\dt 24/Feb/2025

\lx thonga
\bw tonga (POLYN) (PPN *tonga)
\ph tʰoŋa
\dialx GOs
\is santé
\ps n
\ge plaie sur les pieds
\dt 25/Jan/2019

\lx thogaavwi
\ph tʰo'ga:βi
\dialx GOs PA BO
\va tho'gapi
\dialx GO(s) arch.
\va tho'gavhi
\dialx PA BO
\is temps_atmosphérique
\ps n
\ge écho
\xv nu trõne thogaavwi
\dialx GOs
\xn j'ai entendu l'écho
\se pha-thogavwi
\dialx PA
\sge faire écho
\dt 24/Dec/2021

\lx thôge
\dialx GOs
\is santé
\ps v
\ge stérile (femme)
\xv e thôge
\xn elle est stérile
\dt 25/Jan/2019

\lx thòi haa
\dialx GOs
\is cultures
\ps v
\ge planter des taros au bord de l'eau
\dn sans système d'irrigation
\xv a thòi haa !
\dialx GOs
\xn va planter les taros au bord de l'eau !
\dt 08/Feb/2025

\lx thoimwã
\dialx GOs
\va toimwa
\dialx PA BO
\is étapes_vie
\ps n
\ge vieille femme
\cf whamã
\dialx GOs
\ce vieil homme
\dt 10/Jan/2022

\lx thô-kui
\dialx GOs PA BO
\va pwe-nô kui
\dialx WEM
\is igname
\ps n
\ge extrêmité inférieure de l'igname
\cf bwe-kui
\dialx GOs PA
\ce tête de l'igname
\dt 20/Mar/2023

\lx thô-kuru
\dialx GOs PA BO
\is taro
\ps n
\ge extrêmité inférieure du taro
\dt 22/Mar/2023

\lx tho-khõbwe
\dialx GOs PA
\is interaction
\ps v
\ge annoncer publiquement ; faire une annonce
\dt 27/Jan/2019

\lx thòlòe
\dialx BO
\va tholee tolee
\dialx BO
\is action_eau_liquide_fumée
\ps v
\ge répandre (se) (eau, fumée)
\nt selon Corne
\dt 27/Mar/2022

\lx thomã
\dialx GOs WEM PA BO
\is interaction
\ps v.t.
\ge appeler ; interpeller
\xv thomã-je !
\dialx GOs
\xn appelle-le !
\xv e thomã-çö !
\dialx GOs
\xn il/elle t'appelle !
\xv thomã-je xo õ-Milen
\dialx WEM
\xn la mère de Milène l'appelle
\xv i thomã Kaawo
\dialx BO
\xn il appelle Kaavo
\xv i thomã-nu
\dialx BO
\xn il m'appelle
\ng |lx{thomã} a un objet pronominal; |lx{thomãni} a un objet nominal
\dt 06/Jan/2022

\lx thomãni
\ph tʰomɛ̃ɳi
\dialx GOs PA
\is interaction
\ps v.t.
\ge appeler qqn.
\xv e thomãni êgu na bòli
\xn il appelle les gens d'en bas
\ng |lx{thomãni} a un objet nominal
\dt 29/Mar/2022

\lx tho-me
\dialx PA
\is caractéristiques_personnes
\ps v
\ge crâner ; faire le malin
\xv i pe-tho-me
\xn il fait le malin
\dt 26/Jan/2019

\lx thòn
\is végétation
\ps n
\ge brousse
\xv li ra xa a mwã ni thòn
\dialx PA
\xn elles repartent alors dans la brousse
\dt 25/Dec/2021

\lx thôni
\ph tʰõɳi
\dialx GOs WEM WE PA BO
\sn 1
\is action
\ps v.t.
\ge fermer qqch.
\dn avec un objet, un couvercle
\xv i ra tee thôni mwã je phwè-bii
\dialx PA
\xn il (fruit) bloquait déjà l’orifice de la canalisation
\xv thôni phwè-mwa !
\dialx BO
\xn ferme la porte !
\se thôni we
\sge fermer le robinet d'eau/la vanne
\sn 2
\is interaction
\ps v.t.
\ge interdire
\ge empêcher (obstacle)
\xv la thôni dè ko/xo la êgu
\dialx GOs
\xn les hommes ont barré/bloqué la route
\xv nu thôni-çö na ni nye kûdo
\dialx GOs
\xn je t'interdis la boisson (je te barre l'accès à cette boisson)
\ng v.i. |lx{thô}
\dt 22/Feb/2025

\lx thoo
\hm 1
\dialx GOs PA BO
\is cultures
\ps v
\ge arracher la canne à sucre ; récolter
\xv e thoo ê
\dialx GOs
\xn il arrache/récolte la canne à sucre
\xv i thoo èm
\dialx PA
\xn il arrache/récolte la canne à sucre
\xv thoo xa whala-m èm !
\dialx PA
\xn va récolter de la canne à sucre pour toi !
\dt 22/Feb/2025

\lx thoo
\hm 2
\dialx PA BO
\va throo
\dialx GO(s)
\is habillement
\ps n
\ge aigrette (coiffure)
\ge plume ou fleur plantée sur le sommet de la tête
\xv thoo-ny
\dialx PA
\xn mon plumet
\dt 24/Aug/2021

\lx thòò
\dialx GOs BO
\is fonctions_naturelles
\ps v
\ge accoupler (s') ; avoir des relations sexuelles
\ge violer
\xv i thòò-e
\dialx BO
\xn il l'a violée
\dt 25/Jan/2019

\lx thöö
\hm 1
\ph tʰω:
\dialx GOs PA
\va thoo
\dialx BO
\is interaction
\ps v
\ge maudire
\xv i thoo-je
\dialx BO
\xn il l'a maudite
\dt 27/Jan/2019

\lx thöö
\hm 2
\dialx PA
\va tuu
\dialx BO
\is grammaire_aspect
\ps ASP
\ge en même temps
\dt 29/Jan/2019

\lx thõõbo
\dialx GOs
\va thõõbon
\dialx BO
\va thõba, thõõbwa
\dialx PA WEM
\is portage
\ps v
\ge porter sur le dos ou les épaules
\dn avec une corde de portage ; ce sont les femmes qui portent ainsi les paniers
\xv e thõõbone cee
\dialx GOs
\xn elle porte le bois sur le dos
\xv i thõõbo-ni xo/xu õõ-li
\dialx GOs
\xn leur mère les porte sur le dos
\se ke-thõõbo
\sge panier porté sur le dos
\ng v.t. |lx{thõõbone, thõõboni, thõõboli} |dialx{GOs PA BO}
\gt porter qqch.
\dt 21/Feb/2025

\lx thòòli
\dialx GOs PA
\is mollusque
\ps n
\ge "coquilon" (à coquille longue)
\ge bernard-l'ermite
\dn qui se met dans la coquille du |lx{thooli}
\sc Cœnobita olivieri
\scf Cœnobitidés
\dt 08/Feb/2025

\lx thoomwã
\ph tʰo:mwɛ̃
\dialx GOs PA
\is société
\ps n
\ge femme ; féminin
\se phe thoomwã
\sge se marier (prendre une épouse)
\se ẽnõ thoomwã
\sge fille
\cf thoimwã
\ce vieille-femme
\dt 18/Apr/2024

\lx thoomwã kòlò
\dialx PA BO
\is parenté
\ps n
\ge nièce
\dn (fille de frère et cousins)
\xv thoomwã kòlò-ny
\dialx PA
\xn fille de frère ; nièce
\dt 22/Feb/2025

\lx thooni
\ph tʰo:ɳi
\dialx GOs PA
\sn 1
\is interaction
\ps v
\ge annoncer des informations ; présenter
\sn 2
\is coutumes
\ps v
\ge désigner un tas pour un clan (coutume)
\dt 22/Feb/2018

\lx thööni
\ph tʰω:ɳi
\dialx GOs PA
\is interaction
\ps v
\ge maudire
\se pe-zööni
\sge se maudire mutuellement
\dt 13/Oct/2021

\lx thòòzò
\dialx GOs
\va thòròe
\dialx PA
\is pêche
\ps v
\ge piquer (dans un trou)
\dn avec une sagaie pour chercher des anguilles, poissons
\xv e thòòzò peenã
\dialx GOs
\xn il pique pour trouver une anguille
\xv e thòròe para
\dialx PA
\xn il pique les herbes (pour trouver des anguilles)
\xv e thòròe paa
\dialx PA
\xn il pique (sous) les pierres (pour trouver des anguilles, poissons)
\cf thaa, thi
\ce piquer
\dt 08/Feb/2025

\lx tho-puu
\dialx GOs BO
\va thi-puu
\dialx PA BO
\is navigation
\ps v
\ge pousser (bateau) avec la perche
\dn (lit. piquer)
\xv i tho-pue phaa-gò
\dialx BO
\xn il pousse le radeau avec une perche
\se ba-thi-puu
\dialx PA BO
\sge perche
\cf thoe
\dialx GOs
\ce piquer
\dt 08/Feb/2025

\lx thou
\dialx GOs
\va theun
\dialx BO [Corne]
\is fonctions_naturelles_animaux
\ps v
\ge muer (lézard, serpent)
\xv e thou pwaji
\xn le crabe mue
\dt 25/Jan/2019

\lx thovwa
\dialx GOs
\ph tʰoβa
\va thòva
\dialx BO [BM]
\is caractéristiques_objets
\ps v
\ge plat ; aplati
\xv ne vwö thovwa !
\xn aplatis-le (lit. fais pour que ce soit plat)
\dt 20/Aug/2021

\lx thô-vwaa-ni
\ph tʰõβaɳi
\dialx GOs
\is action_corps
\ps v.t.
\ge rapiécer
\dn (lit. fermer le trou)
\se thô-vwaa
\sge pièce (pour rapiécer)
\cf phwa
\ce trou
\dt 08/Feb/2025

\lx thòxe
\dialx PA
\va thòxe, thòòge
\dialx BO [BM]
\is caractéristiques_objets
\ps v
\ge collé (par ex. au fond de la marmite)
\xv i thòxe na inu u ciia
\dialx BO
\xn le poulpe s'est collé à moi
\xv i thòòge ari
\dialx BO
\xn le riz est collé
\dt 26/Jan/2019

\lx thozoe
\dialx GOs
\va toroe
\dialx PA BO
\sn 1
\is action_corps
\ps v
\ge cacher ; dissimuler qqch.
\xv na-mi mwani pu nu tre thozoe
\dialx GOs
\xn donne-moi (lit. ici) l'argent pour que je le cache
\se kô-thozoe
\sge caché
\cf ku-çaaxò
\ce se cacher
\sn 2
\is interaction
\ps v
\ge garder secret
\sn 3
\is étapes_vie
\ps v
\ge enterrer (qqn.)
\cf khêmi
\dialx GOs
\ce enterrer qqch.
\dt 17/Feb/2025

\lx thu
\dialx GOs WEM BO PA
\va tho
\dialx BO
\sn 1
\is action
\ps v
\ge faire
\se thu-paa
\dialx GO
\sge faire la guerre
\se thu-pwalu
\sge respecter
\se thu kibi
\sge faire le four
\se ba thu da ?
\sge ça sert à quoi ?
\sn 2
\is grammaire_existentiel
\ps v
\ge il y a
\xv u thu ai-n
\dialx PA
\xn il a de la maturité
\xv u thu pòi-n mwã
\dialx PA
\xn il a des enfants maintenant
\xv thu je-nè bò thi-cee
\dialx PA
\xn il y avait là une date de fête fixée
\xv i thu angai kale
\dialx BO
\xn c'est la haute marée d'équinoxe (Dubois)
\xv i thu paga tavane
\dialx BO
\xn c'est la basse marée d'équinoxe (Dubois)
\xv thu hava-hi-la
\dialx BO
\xn ils ont des ailes (BM)
\xv thu xa radio i yo ?
\dialx BO
\xn as-tu une radio ? (BM)
\xv õ, thu radio i nu
\dialx BO
\xn oui, j'ai une radio (BM)
\dt 12/Jan/2022

\lx thu ãbaa
\dialx GOs
\va tho ãbaa-n
\dialx BO
\is action
\ps v
\ge ajouter ; mettre plus ; compléter
\xv thu ãbaa mwani
\dialx GOs
\xn ajoute de l'argent
\xv na ãbaa mwani
\dialx GOs
\xn donne plus d'argent
\dt 29/Jan/2019

\lx thu ai
\dialx GOs
\va thu ai-n
\dialx PA
\sn 1
\is caractéristiques_animaux
\ps v
\ge dressé (cheval, animal)
\xv thu ai-n
\dialx PA
\xn il est dressé
\sn 2
\is caractéristiques_personnes
\ps v
\ge raisonnable ; mature (personne)
\xv u thu ai-n
\dialx PA
\xn il a une conscience ; il est mûr (lit. il y a son cœur)
\cf kiya ai-n
\ce pas dressé
\dt 22/Feb/2025

\lx thu bwa
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge entêté
\xv e a-thu-bwa
\xn il est entêté
\dt 27/Jan/2018

\lx thu bwahî
\dialx GOs
\is interaction
\ps v
\ge prêter serment
\xv nu thu bwahî na bwa ala-mè pu-ã
\xn je prête serment devant Dieu
\dt 27/Jan/2019

\lx thu êgo
\dialx GOs
\is fonctions_naturelles_animaux
\ps v
\ge pondre
\cf khaa-pi, khaa-vwi
\ce pondre
\dt 25/Jan/2019

\lx thu haal
\dialx PA
\is action
\ps v
\ge mettre de côté ; réserver
\dt 29/Jan/2019

\lx thu hoo
\dialx GOs PA
\va nee-vwo hoo-n
\dialx PA
\is outils
\ps v
\ge caler le manche
\xv e thu hoo kòò-piòò
\dialx GOs
\xn il cale le manche de la pioche
\xv nee-vwo-n
\dialx PA
\xn cale-le !
\dt 27/Jan/2019

\lx thu hubu
\dialx GOs
\va thu hubun
\dialx PA
\is caractéristiques_personnes
\ps v
\ge orgueilleux ; faire le fier ; manquer d'humilité
\xv e thu hubu
\dialx GOs
\xn il est orgueilleux/méchant
\xv i thu hubun
\dialx PA
\xn il est orgueilleux
\dt 22/Feb/2025

\lx thu mõõxi
\dialx GO
\is action_plantes
\ps v
\ge faire des boutures
\dn (lié à la notion de vie)
\xv nu thu mõõxi
\dialx GO
\xn je fais des boutures
\dt 09/Feb/2025

\lx thu mwêê
\dialx GOs BO
\is habillement
\ps v
\ge apprêter (s') ; parer (se) ; vêtir (se)
\xv e thu mwêê Kaavwo
\dialx GOs
\xn Kaavwo se pare
\xv hõbwò ba-thu-mwêê
\dialx GOs
\xn de beaux vêtements
\cf mwêê
\ce chapeau
\cf udale
\dialx GOs
\ce s'habiller
\dt 10/Jan/2022

\lx thu mwêêxa
\dialx GOs
\is habillement
\ps v
\ge apprêter (s') ; préparer (se) (habits et maquillage)
\ge parer (se) ; se vêtir
\xv la thu mwêêxa
\dialx GOs
\xn ils s'apprêtent/se parent
\cf thu mwêê
\dt 22/Feb/2025

\lx thu paa
\dialx GOs
\is guerre
\ps v
\ge faire la guerre
\xv li pe-thu-paa lie phwe-meevwu
\dialx GOs
\xn les deux clans se font la guerre
\dt 24/Aug/2021

\lx thu pi
\dialx PA BO
\is fonctions_naturelles_animaux
\ps v
\ge pondre
\dn (lit. faire la coquille)
\nt selon Corne
\dt 09/Feb/2025

\lx thu pwaalu
\dialx GOs BO
\is interaction
\ps v ; n
\ge respect ; respecter
\dt 09/Jan/2022

\lx thu pwalu
\dialx PA BO
\is interaction
\ps v
\ge faire une fête
\dt 09/Jan/2022

\lx thu phu
\dialx GOs
\is maison
\ps n
\ge mettre la première rangée de paille au bord du toit
\xv e thu phu
\xn il met la première rangée de paille
\dt 26/Jan/2019

\lx thu phwa
\dialx GOs PA BO
\is cultures
\ps v
\ge faire un trou
\dn pour les cultures, etc.
\dt 08/Feb/2025

\lx thua
\ph tʰwa, tʰua
\dialx GOs PA BO
\is caractéristiques_animaux
\ps v.stat.
\ge sauvage ; non domestiqué
\xv kuau thua
\dialx GOs
\xn chien sauvage
\xv poka thua
\dialx PA
\xn cochon sauvage
\xv di thua
\xn cordyline sauvage
\dt 22/Mar/2023

\lx thuada
\dialx GOs WEM BO
\is armes
\va yada-pia ?
\dialx PA
\ps n
\ge armes de guerre (lance, sagaie)
\ge sagaie (grande) de guerre |dialx{BO}
\dt 09/Jan/2022

\lx thu-ãda
\dialx GOs PA
\is caractéristiques_personnes
\ps v ; n
\ge orgueil ; vouloir surpasser
\ge entêter (s') ; entêté |dialx{PA, GOs}
\xv li pe-thu-ãda
\xn ils sont en compétition ; ils font de la surenchère
\dt 22/Feb/2025

\lx Thulixâân
\dialx PA
\is religion
\ps n
\ge divinité qui favorise la pluie
\dt 28/Jan/2019

\lx thu-me
\ph tʰuṃe
\dialx GOs PA
\is caractéristiques_personnes
\ps v
\ge fier ; se faire remarquer ; faire le malin
\ge faire du charme ; draguer
\ng causatif: |lx{pa-thu-me-ni}
\gt arborer qqch.; faire remarquer qqch.
\dt 22/Feb/2025

\lx thu-mhenõ
\ph tʰumʰeɳɔ̃
\dialx GOs PA
\va tomèno
\dialx BO
\is déplacement
\ps v
\ge marcher (faire des traces)
\ge aller
\ge promener (se)
\ge fonctionner (machine)
\xv bî pe-thu-mhenõ mãni ãbaa-nu xa thoomwã
\dialx GOs
\xn j'ai fait le chemin avec ma sœur
\se thu-mhenõ-hayu
\dialx GOs
\sge aller sans savoir où/sans direction ; aller à contrecœur, en se forçant
\se thu-mhenõ lòlò
\dialx GOs
\sge marcher au hasard/sans but
\se thu-mhenõ ka udu
\dialx GOs
\sge marcher en se baissant
\se thu-mhenõ-du mu
\dialx GOs
\sge marcher à reculons (lit. en bas derrière)
\dt 22/Feb/2025

\lx thu-phoã
\dialx GOs PA BO
\is cultures
\ps v
\ge cultiver ; faire un champ
\se mhenõ thu-phoã
\dialx GOs
\sge un champ cultivé
\se mhenõ thu-phoã
\dialx BO
\sge un champ cultivé
\dt 22/Mar/2023

\lx thu-tãî
\dialx PA
\is habillement
\ps v
\ge habiller (s') ; vêtir (se)
\xv i thu-tãî Kaavo
\xn Kaavo s'habille
\dt 20/Feb/2018

\lx thuu
\hm 1
\dialx GOs BO
\sn 1
\is santé
\ps v
\ge gale (avoir la) ; boutons sur la figure (avoir des)
\xv e thuu bwaa-je
\xn il a des plaques sur la tête
\sn 2
\is santé
\ps v
\ge maladie des féculents (avoir la)
\xv e thuu kui
\xn l'igname a une maladie
\dt 17/Feb/2025

\lx thuu
\hm 2
\dialx PA
\va thuvwu
\dialx GO(s)
\is grammaire_réfléchi
\ps RFLX
\ge réfléchi (du sujet, agent)
\xv i rau thuu theei-je (x)o hèlè
\dialx PA
\xn il s'est frappé avec un couteau
\xv i theei-je xo hèlè
\dialx PA
\xn il l'a frappé avec un couteau ; il lui a donné un coup de couteau
\xv nu thuu hããxa
\dialx PA
\xn je me fais peur
\xv i ra-u/ra-o thuu pao-i-je jigel
\dialx PA
\xn il s'est tiré un coup de fusil à lui-même
\dt 22/Feb/2025

\lx thuvwu
\dialx GOs
\va thuu
\dialx PA
\va tuu
\dialx BO
\is grammaire_réfléchi
\ps RFLX
\ge réfléchi (du sujet, agent)
\xv e thuvwu thrae pu-bwaa-je
\dialx GOs
\xn il s'est rasé les cheveux
\xv e thuvwu kòòli hii-je xo dubwo
\dialx GOs
\xn il s'est piqué la main avec l'aiguille
\xv e thuvwu zòi hii-je
\dialx GOs
\xn il s'est coupé le bras ((in)volontairement ; voir |lx{draa pune} "se couper volontairement")
\xv e thuvwu phai nõ
\dialx GOs
\xn il a cuit le poisson pour lui-même
\xv e thuvwu vhaa ui-je cai Kaavo
\dialx GOs
\xn elle parle d'elle-même à Kaavo
\xv Hiixe ça e thuvwu vhaa ui-je cai Kaavo
\dialx GOs
\xn quant à Hiixe, elle parle d'elle-même à Kaavo
\xv e thuvwu tròròwuu ui je (ou) e thuvwu tròròwuu
\dialx GOs
\xn il est content de lui-même [faux *e thuvwu tròròwuu-je ui je]
\xv e thuvwu tròròwuu-je ui nye ẽnõ-ã
\dialx GOs
\xn il se sent content de cet enfant
\xv i ra u/ra o thuu pao i je jigel
\dialx PA
\xn il s'est tiré un coup de fusil contre lui-même
\dt 22/Feb/2025

\lx thuvwu zòi
\ph 'tʰuvwu 'ðɔi
\dialx GOs
\is grammaire_réfléchi
\ps v.RFLX
\ge couper (se)
\xv nu thuvwu zòi-nu
\xn je me suis coupé
\dt 22/Mar/2023

\lx trabwa
\ph ʈambwa
\dialx GOs
\va tabwa
\dialx BO PA WEM
\sn 1
\is position
\ps v
\ge asseoir (s') ; assis
\ge percher (se) ; perché
\ge rester
\xv i ra u hai (= haivwo) nha, buròm-raa, tabwa mwa nye nya pwo-ne
\dialx WEM
\xn elle est couverte de crotte, elle se lave mal, il en reste un peu là
\se mhènõõ-trabwa
\dialx GOs
\va mhènõõ-tabwa
\dialx BO
\sge chaise ; siège
\se we-tabwa
\dialx BO
\sge eau stagnante
\sn 2
\is mouvement
\ps v
\ge poser (se)
\ge atterrir ; toucher terre
\sn 3
\is fonctions_naturelles_animaux
\ps v
\ge couver (œufs)
\ng |lx{tre-}
\gt en composition
\dt 22/Feb/2025

\lx trabwa mhwããnu
\dialx GOs
\va tabwa mhwããnu
\dialx BO
\is temps_découpage
\ps n
\ge nouvelle lune
\dt 28/Jan/2019

\lx trabwi
\dialx GOs
\is caractéristiques_personnes
\ps v.stat.
\ge hyperactif
\xv ẽnõ xa e a-trabwi
\xn un enfant qui est hyperactif
\dt 27/Oct/2023

\lx tragiliã
\dialx GOs
\va trãgilijã
\dialx GO(s)
\va tãgiliã
\dialx PA BO
\is mollusque
\ps n
\ge bénitier (mollusque)
\sc Tridacna maxima
\scf Tridacnidés
\dt 22/Mar/2023

\lx tragòò
\dialx GOs
\va taagò
\dialx BO
\is corps
\is étapes_vie
\ps n
\ge circoncision ; subincision
\dn circonstance lors de laquelle l'on donnait le "bagayou" (étui pénien) au jeune garçon
\cf gò
\ce couteau à subincision
\dt 10/Jan/2022

\lx trale
\ph 'ʈale
\dialx GOs
\va tali
\dialx PA
\is action_plantes
\ps v
\ge cueillir un fruit qui n'est pas mûr
\ge gauler ; taper pour faire tomber (fruit)
\dt 20/Aug/2021

\lx trali
\dialx GOs
\va tali
\dialx PA BO
\is cultures
\ps v
\ge arracher
\dn paille, herbes, lianes
\xv i tali mèròò
\dialx PA
\xn il arrache l'herbe
\xv i tali wal
\dialx PA
\xn il arrache des lianes
\xv i tali mãe
\dialx PA
\xn il arrache des herbes (à paille)
\dt 08/Feb/2025

\lx travwa
\ph ʈaβa
\dialx GOs
\va trapa
\dialx GO(s)
\va tavang
\dialx PA
\is nourriture_tabac
\ps n
\ge tabac ; cigarette
\bw tabac (FR)
\dt 19/Feb/2019

\lx tre
\dialx GOs
\va tree
\dialx GOs
\va tee, te
\dialx PA BO
\is grammaire_aspect
\ps PRE.V-état
\ge déjà ; état antérieur
\xv li za u tre kha-nõõli
\dialx GOs
\xn ils l'ont déjà vue en se déplaçant
\xv la tre threi mãe hêbu vwo la yaaze mwã
\dialx GOs
\xn ils ont coupé la paille avant de couvrir la maison
\xv novwö xa lhi tree trõne je-nã
\dialx GOs
\xn comme ils ont déjà entendu cela
\xv lha u tee a-duu-mi
\dialx PA
\xn ils étaient descendus auparavant
\xv co tee a
\dialx PA
\xn pars avant
\xv co tee-vwun
\dialx PA
\xn commence avant
\xv tee hovwo
\dialx PA
\xn mange avant
\dt 17/Apr/2024

\lx Treã
\dialx GOs
\va Teã
\dialx BO PA
\is société_organisation
\ps n
\ge fils aîné de chef
\dt 21/Dec/2021

\lx Treã-ma
\dialx GOs
\va Têã-ma
\dialx PA BO
\is société_organisation
\ps n
\ge Grand Chef (et les clans alliés)
\dt 21/Feb/2025

\lx tre-bwaalu
\dialx GOs PA
\is position
\ps v
\ge assis en tailleur
\dt 27/Jan/2018

\lx trebwalu
\dialx GOs
\va tebwalu
\dialx BO PA
\is topographie
\ps n
\ge petite élévation ; colline
\dt 16/Aug/2021

\lx treçaaxo
\ph ʈe'ʒa:ɣo
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge calme ; paisible
\dt 12/Feb/2019

\lx tre-çôô
\ph ʈeʒõ:
\dialx GOs
\is position
\ps v
\ge pendu ; accroché
\ng |lx{tre}: marque d'état, fait déjà accompli
\cf chôô
\ce suspendre ; accrocher
\dt 22/Feb/2025

\lx tre-chaaçee
\ph ʈecʰa:ʒe:
\dialx GOs
\is position
\ps ADV
\ge travers (de) ; bancal ; penché sur un côté ; pas à niveau
\xv e tre-chaaçee mwa
\xn la maison penche d'un côté
\dt 02/Jan/2022

\lx tre-chamadi
\ph ʈecʰamandi
\dialx GOs
\is discours
\ps v
\ge prédire (l'avenir)
\dt 02/Jan/2022

\lx tree
\hm 1
\ph ʈe:
\dialx GOs
\va tèèn, tèn
\ph tɛ:n, tɛn
\dialx PA BO
\is temps_découpage
\ps n
\ge jour ; journée
\xv e zo tree
\dialx GOs
\xn il fait beau
\xv e thraa tre
\dialx GOs
\xn il fait mauvais temps
\xv tree monõ
\dialx GOs
\xn le jour du lendemain
\xv õ tree
\dialx GOs
\xn tous les jours
\xv õ tèèn
\dialx PA
\xn tous les jours
\xv we-ru tèèn
\dialx BO
\xn deux jours
\xv u tèèn
\dialx BO
\xn il fait jour
\xv we-nira tèèn
\dialx PA
\xn combien de jours ?
\et *ɖan(i), *daqani
\el POc
\dt 28/May/2024

\lx tree
\hm 2
\dialx GOs
\va tee, te
\dialx PA BO
\sn 1
\is grammaire_contraste
\ps CONTR
\ge contraste
\dn entre deux agents, deux moments, deux actions dont l'une est antérieure
\xv nu tree nee
\dialx GOs
\xn je vais le faire (pendant que tu fais autre chose)
\xv çö tree yu (ê)nè, ma nu a pwe
\dialx GOs
\xn reste ici car je vais à la pêche
\xv ezoma nu tree puçò, na çö a pwe
\dialx GOs
\xn je vais (rester) faire la cuisine, quand tu iras pêcher
\sn 2
\is grammaire_aspect
\ps ASP
\ge rémansif
\xv traabwa ênõli vwo tre alö-i-õ
\dialx GOs
\xn il était assis là et restait à nous regarder
\xv mõ tree kò êna
\dialx GOs
\xn nous restons debout là
\xv e za pe-trabwa êbòli bwa õ vwo za pe-tree-nõ-du
\dialx GOs
\xn il est resté assis là-bas en bas sur le sable et il reste à regarder vers la mer
\xv lhi tee-phaaxeen wo je tho we
\dialx PA
\xn elles restent à écouter le bruit de l'eau
\xv i ra tee-thôni mwã je phwee-bii
\dialx PA
\xn il (fruit) bloque l'orifice de la conduite d'eau
\xv tee-taabwa ênè me nu a pwaa ce-bòn,
\dialx WEM
\xn reste assise ici,  je vais casser du bois pour la nuit
\xv na yu, te yu èna ma ya a khila xa ho-ã
\dialx BO
\xn et toi, tu vas rester là et nous allons chercher notre nourriture [BM]
\dt 08/Feb/2025

\lx tree
\hm 3
\dialx GOs
\va tee
\dialx PA
\is préfixe_sémantique_position
\is position
\ps PREF (position assise)
\ge assis (faire)
\xv mi tree-bulu
\dialx GOs
\xn nous sommes assis ensemble
\xv e tree-raa mwa !
\dialx GOs
\xn la maison est mal située
\xv co tee-nenèm !
\dialx PA
\xn reste assis tranquille (à un enfant)
\dt 24/Jan/2022

\lx trèè
\hm 2
\dialx GOs
\va tee
\dialx PA
\is déplacement
\ps v
\ge conduire ; diriger (voiture, bateau, etc.)
\xv i tee loto
\dialx PA
\xn il conduit une voiture
\dt 22/Mar/2023

\lx trêê
\ph ʈẽ:
\dialx GOs
\va têên
\ph tɛ̃ɛ̃n
\dialx PA BO WEM
\sn 1
\is déplacement
\ps v
\ge courir
\xv e trêê-ò
\dialx GO
\xn il s'éloigne en courant
\xv e trêê u-pwa
\dialx GO
\xn il sort en courant
\xv na yu khò têê-du pwa
\dialx PA
\xn cours un peu dehors
\sn 2
\is grammaire_manière
\ps v
\ge faire vite ; faire sans obstacle
\xv za trêê-ulu-ò ã-e hèlè-ã
\dialx GO
\xn le couteau s'enfonce sans obstacle
\xv i ra u têê-uda mwã ni je mwã
\dialx PA
\xn elle entre vite dans la maison
\dt 22/Mar/2023

\lx tree hêbu
\dialx GOs
\is temps_deixis
\ps ADV
\ge avant-hier (lit. le jour d'avant) ; il y a trois jours
\dt 21/Dec/2021

\lx trêê kha-ve
\dialx GOs
\va thêên
\dialx PA
\is déplacement
\ps v
\ge courir en emportant qqch.
\cf kha-phe, kha-ve
\ce apporter
\dt 22/Mar/2023

\lx tree-çãnã
\ph ʈe:'ʒɛ̃ɳɛ̃
\dialx GOs
\va teecãnã
\dialx WEM
\is fonctions_naturelles
\ps v
\ge reprendre son souffle ; reprendre haleine
\ge reposer (se)
\ge souffler
\cf chãnã
\ce se reposer ; respirer
\dt 22/Feb/2025

\lx tree-çimwî
\ph ʈe:'ʒimwi
\dialx GOs
\va tee-cimwî, tee-jimwî, tee-yimwî
\dialx PA BO
\is action_corps
\ps v
\ge attraper ; saisir
\ge retenir ; serrer
\ge mémoriser ; retenir
\xv e tree-cimwî ã-e kînu
\dialx GOs
\xn il reste à retenir cette ancre
\xv whaya me tee-yimwî pwaji ?
\dialx BO
\xn comment attrape-t-on les crabes ?
\cf cimwî
\ce tenir ; saisir
\dt 22/Feb/2025

\lx tree-e
\ph ʈe.e
\dialx GOs
\is position
\ps v
\ge assis en tenant qqch. dans les bras
\xv e tree-e ẽnõ
\xn il est assise avec l'enfant dans ses bras
\cf trabwa
\dialx GOs
\cf tabwa
\dialx PA
\dt 22/Mar/2023

\lx tree-hôboe
\dialx GOs
\is position
\ps v
\ge rester à attendre
\xv e tree-hôboe-je
\xn elle reste à l'attendre
\dt 04/Nov/2021

\lx trêê-kai
\dialx GOs
\is chasse
\ps v
\ge poursuivre ; courir derrière
\cf trêê
\ce courir
\cf kai
\ce dos
\dt 22/Mar/2023

\lx tree-kiyai
\dialx GOs
\va tre-xiyai
\dialx GOs
\va tee-kiyai
\dialx PA
\is position
\ps n
\ge asseoir (s') auprès du feu
\dn pour se réchauffer
\xv a tree-kiyai !
\dialx GOs
\xn va t'asseoir près du feu (pour te réchauffer)
\ng forme courte de |lx{tre-khinu-yaai}
\gt (lit. assis-chauffer-feu)
\dt 08/Feb/2025

\lx tree-kòò
\is position
\dialx GOs
\ps v
\ge rester debout ; s'arrêter (de bouger)
\xv nu tree-kòò-nu
\xn je me suis arrêté (debout)
\dt 22/Mar/2023

\lx tree-ku
\dialx GOs
\is préfixe_sémantique_position
\ps v
\ge rester
\xv nu khabe cee vwö kòò, vwö nu khòlimã le; na çö tree-ku hõbwò-nu
\dialx GOs
\xn je planterai ce bout de bois pour qu'il soit droit, afin que je m'y agrippe, et toi tu resteras là à m'attendre
\dt 22/Feb/2025

\lx trèèlè
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge paresseux ; avoir la flemme
\dt 16/Jan/2024

\lx tree-poxe
\dialx GO
\is position
\ps v
\ge assis ensemble
\dt 12/Jun/2024

\lx tree-trabwa
\dialx GOs
\is position
\ps v
\ge rester assis
\xv e tree-trabwa, tree-hôboe-je
\xn elle reste assise, reste à attendre
\dt 04/Nov/2021

\lx tree-yuu
\dialx GOs
\va te-yu
\dialx PA BO
\is position
\ps v
\ge rester ; demeurer
\xv te-yu bulu
\dialx BO
\xn rester ensemble
\dt 22/Mar/2023

\lx trèèzu
\dialx GOs
\va tèèzo
\dialx PA
\is topographie
\ps v.stat. ; n
\ge aplani ; plat ; plaine
\xv ni tèèzo
\dialx PA BO
\xn dans la plaine
\dt 03/Feb/2025

\lx tre-go
\dialx GOs
\is position
\ps v
\ge assis en rêvant
\xv e tre-go
\xn il est assis en étant dans les nuages
\dt 27/Jan/2018

\lx tre-hû
\dialx GOs
\is position
\ps v
\ge assis sans parler
\dn (lit. assis-muet)
\dt 08/Feb/2025

\lx trehuni
\ph ʈehuɳi
\dialx GOs
\is religion
\ps n
\ge foi
\dt 27/Jan/2018

\lx tre-kea
\dialx GOs
\va tre-xea
\dialx GO(s)
\is position
\ps v
\ge assis adossé à qqch.
\dt 29/Mar/2022

\lx tre-kuçaaxo
\dialx GOs
\va tee-kujaaxo
\dialx PA
\is position
\ps v
\ge assis en cachette à l'affût
\dt 22/Feb/2025

\lx tre-kue
\dialx GOs
\is sentiments
\ps v
\ge envier ; jaloux (être) ; jalouser
\xv e tre-kue-le loto i nu
\xn il est jaloux de ma voiture
\se a-tre-kue
\sge un jaloux
\dt 12/Oct/2021

\lx tre-khõbwe
\dialx GOs
\va te-kôbwe
\dialx BO
\is interaction
\is discours
\ps v
\ge annoncer ; prévenir ; promettre ; déclarer ; aviser
\cf tre-chamadi
\ce prédire
\dt 27/Jan/2019

\lx tre-nene
\ph ʈeɳeɳe
\dialx GOs
\va tee-nenem
\dialx PA BO
\is grammaire_manière
\ps v ; n
\ge paix ; tranquille ; paisible
\ge rester tranquille ; reposer en paix
\xv mo za trenene na koi ẽnõ mãlò
\dialx GOs
\xn on est tranquille quand les enfants ne sont pas là
\dt 22/Mar/2023

\lx tre-nõnõmi
\ph ʈeɳɔ̃ɳɔ̃mi
\dialx GOs
\va tee-nõnõmi
\dialx PA BO
\is fonctions_intellectuelles
\ps v
\ge prévoir
\ge penser à ; réfléchir
\xv co tee-nõnõmi da ?
\dialx PA
\xn à quoi penses-tu ? (sans rien dire)
\dt 02/Jan/2022

\lx tre-pavwã
\ph ʈepaβa
\dialx GOs
\va tre-pavhã
\dialx GO(s) BO PA
\is fonctions_intellectuelles
\ps v ; n
\ge espoir ; confiance (avoir) ; espérer
\cf pavwã
\ce confiance
\ng v.t. |lx{tre-pavwãni}
\dt 06/Jan/2022

\lx tre-paxo
\dialx GOs
\va tee-vhaxol, tee-waxol
\dialx PA BO
\is position
\ps v
\ge accroupi ; s'accroupir
\dt 25/Jan/2019

\lx tre-pwalee kò-ã
\dialx GOs
\is position
\ps v
\ge assis les jambes allongées
\xv mwã tre-pwalee kò-ã
\xn nous sommes assis les jambes allongées
\mr (lit. assis-étaler jambes-nos)
\dt 08/Jan/2022

\lx tre-raa
\dialx GOs
\is caractéristiques_objets
\ps v
\ge cahoteux (route, chemin)
\ge mauvais ; dangereux
\cf thraa
\ce mal ; mauvais
\dt 22/Feb/2025

\lx tre-thibu
\ph ʈetʰibu
\dialx GOs
\va tre-zibu
\dialx GO(s)
\is mouvement
\is position
\ps v
\ge agenouiller (s') ; agenouillé
\dt 02/Jan/2022

\lx tretrabwau
\ph ʈeɽa'bwa.u
\dialx GOs
\va terebwau
\dialx PA BO
\is caractéristiques_objets
\ps v.stat.
\ge rond
\dt 26/Jan/2019

\lx tre-xinãã
\ph ʈe'ɣiɳɛ̃:
\dialx GOs
\va tre-khinu-a
\dialx GO(s)
\va tee-khinu-al
\dialx PA
\is soin
\ps v
\ge chauffer assis au soleil (se)
\ge sécher au soleil (se)
\dt 02/Jan/2022

\lx tre-xiyai
\ph ʈe'ɣijaj
\dialx GOs PA
\va tre-khi(y)ai, te-xi(y)ai
\dialx GO(s)
\va tee-khi(y)ai
\dialx PA
\is feu
\ps v
\ge chauffer (se) assis près du feu
\dt 25/Aug/2023

\lx trezô
\dialx GOs
\is caractéristiques_objets
\ps v
\ge fermé
\dn se dit des portes, fenêtres, etc. qui se ferment toutes seules 
\cf thôni
\ce fermer
\dt 08/Feb/2025

\lx trèzoo
\dialx GOs
\is mouvement
\va too
\dialx BO [BM]
\ps v
\ge échouer (s')
\xv e trèzoo wõ na bwa yaò, na bwa õ
\dialx GOs
\xn le bateau s'est échoué sur le corail, sur le sable
\xv i too wony na bwa kharo
\dialx BO
\xn le bateau s'est échoué sur le corail
\dt 25/Aug/2021

\lx tri
\ph ʈi
\dialx GOs
\is nourriture
\ps n
\ge thé
\bw tea (GB)
\dt 26/Jan/2019

\lx trile
\dialx GOs WEM
\va tilèèng
\dialx PA BO
\is oiseau
\ps n
\ge "suceur" oiseau ; Méliphage à oreillon gris
\sc Lichmera incana
\scf Méliphagidés
\gb Dark brown Honeyeater
\dt 27/Aug/2021

\lx trilòò
\dialx GOs
\va tilòò, tilò
\dialx PA BO
\is interaction
\ps v ; n
\ge demander qqch. ; demande ; requête
\xv e trilòò kêê-je xa dili
\dialx GOs
\xn il demande à son père de la terre
\xv e trilòò dili xo Pwayili ui kêê-je
\dialx GOs
\xn Pwayili demande de la terre à son père [moins courant]
\xv e trilòò cai çò
\dialx GOs
\xn il vous demande
\xv e trilòò-nu
\dialx GOs
\xn il m'a demandé
\xv i tilòò kõbwe nu ru a
\dialx BO
\xn il a demandé si je partirai
\xv tilòò-m, tilòò-ny
\dialx PA
\xn ta demande, ma demande
\se trilòò porô
\dialx GOs
\va tilòò poròm
\dialx BO
\sge demander pardon
\se ki-tilò
\dialx PA
\sge emprunter (lit. demander un peu, pour un moment)
\dt 29/Mar/2022

\lx trirê
\ph ʈiɽẽ, ʈiʈẽ
\dialx GOs
\is fonctions_naturelles
\ps v ; n
\ge sueur ; transpiration ; transpirer
\xv nu a-trirê
\xn je suis en sueur
\se hai trirê
\sge transpirer beaucoup ; avoir trop chaud
\dt 22/Feb/2025

\lx trivwi
\dialx GOs
\ph ʈivwi
\va tripwi
\ph ʈipwi
\va tiwi
\dialx BO [BM]
\va tipui
\dialx BO (Corne)
\is action_corps
\ps v
\ge traîner par terre ; tirer
\dt 26/Jan/2019

\lx trò
\hm 1
\dialx GOs
\ph ʈɔ
\va tòn, thòn
\dialx WEM PA BO
\is lumière
\is temps_découpage
\ps v ; n
\ge nuit ; obscurité
\ge nuit (faire)
\xv u ru tòn
\dialx BO
\xn il va faire nuit
\xv u tòn
\dialx BO
\xn il fait nuit
\dt 15/Aug/2021

\lx trò
\hm 2
\ph ʈɔ
\dialx GOs
\va tò
\dialx BO
\is fonctions_naturelles
\ps v.i
\ge entendre
\ge sentir
\et *ɖoŋoR
\el POc
\dt 03/Feb/2025

\lx trö
\dialx GOs
\is caractéristiques_objets
\ps v
\ge fissuré ; crevassé ; crevasser (se) ; fissurer (se)
\xv e trö dili
\xn la terre est crevassée/se crevasse
\dt 22/Feb/2025

\lx tröi
\dialx GOs
\va töi
\dialx GOs
\va tui
\dialx WEM WE PA BO
\is action_eau_liquide_fumée
\ps v
\ge puiser de l'eau
\xv e t(r)öi we
\dialx GOs
\xn il puise de l'eau
\xv lha a t(r)öi-ni mwã kûdo-la
\dialx GOs
\xn ils vont puiser leur eau à boire
\xv a tui-ni we
\dialx BO
\xn va chercher l'eau
\se ba-tröi
\dialx GOs
\sge écope
\se ba-tui; ba-rui
\dialx PA BO
\sge cuiller
\se mhenõ-tröi
\dialx GOs
\sge endroit où l'on puise de l'eau
\ng v.t. |lx{tröi-ni}
\cf khee we
\dialx GOs
\ce écoper
\dt 22/Mar/2023

\lx tromwa
\dialx GOs
\va toma
\dialx GO(s)
\is nourriture
\ps n
\ge tomate
\bw tomate (FR)
\dt 26/Jan/2019

\lx trõne
\ph ʈɔ̃ɳe
\dialx GOs
\va tõne
\dialx BO PA
\sn 1
\is fonctions_naturelles
\ps v.t.
\ge entendre ; sentir (ressentir)
\xv ka u nu tõne
\dialx BO
\xn je ne l'ai pas entendu
\sn 2
\is fonctions_naturelles
\ps v.t.
\ge sentir (odeur ou toucher) ; entendre
\xv e trõne boo-je
\dialx GO
\xn il sent son odeur
\xv tõne bo-n
\dialx PA
\xn sentir son odeur
\sn 3
\is fonctions_intellectuelles
\ps v.t.
\ge comprendre ; entendre dire
\xv e za trõne khõbwe za Thulirãã
\dialx GO
\xn il entend dire que c'était Thulirãã
\xv u trõne khõbwe e mõlò
\dialx GO
\xv il lui semble/elle sent qu'elle revient à la vie
\dt 06/Aug/2023

\lx trõne bo
\ph ʈɔ̃ɳe
\dialx GOs
\va tõne bo-n
\dialx PA BO
\is fonctions_naturelles
\ps v
\ge sentir (une odeur)
\xv i tõ bwon
\dialx PA
\xn il sent une odeur
\dt 16/Feb/2025

\lx trõne-kaamweni
\ph ʈɔ̃ɳe
\dialx GOs
\is fonctions_intellectuelles
\ps v
\ge comprendre
\xv kavwö nu trõne-kaamweni me-vhaa i la
\dialx GOs
\xn je ne comprends pas leur façon de parler
\xv çö trõne-kaamweni ?
\dialx GOs
\xn tu as compris ?
\dt 14/Sep/2021

\lx tròò
\ph ʈɔ:
\dialx GOs WEM
\va tòò
\dialx BO
\is action
\ps v.t.
\ge trouver ; trouver (se) dans un état ; rencontrer
\xv li tròò-la
\dialx GO
\xn ils les trouvent
\xv çò za a-mi tròò-nu
\dialx GO
\xn vous êtes venus me rendre visite
\ng |lx{tròò} a un objet pronominal ; v.t. |lx{tròòli} + objet nominal
\dt 22/Mar/2023

\lx tròòli
\ph ʈɔ:li
\dialx GOs
\va tòòli, tooli
\dialx PA
\is action
\ps v.t.
\ge trouver qqch.
\se kha-tròòli
\dialx GO
\sge rencontrer par hasard
\xv nu tròòli mã
\dialx GO
\xn je suis tombé malade
\xv nòme çö tròòli-xa, çö thomã-nu
\dialx GO
\xn si tu en trouves, tu m'appelles
\xv nòme çö tròòli-vwo, çö thomã-nu
\dialx GO
\xn si tu en trouves, tu m'appelles
\xv hã ra tòò-je
\dialx PA
\xn nous l'avons trouvé/rencontré
\dt 22/Mar/2023

\lx tròòli mhã-mhwããnu
\dialx GOs
\is fonctions_naturelles
\ps v
\ge menstruations (avoir ses)
\dt 23/Aug/2021

\lx tròòli phwayuu
\dialx GOs
\is fonctions_naturelles
\ps v
\ge menstruations (avoir ses)
\cf tròòli mhã-mhwããnu
\ce avoir ses menstruations
\dt 23/Aug/2021

\lx trörö
\ph ʈωʈω, ʈωɽω
\dialx GOs
\va toro
\dialx BO
\is reptile
\ps n
\ge lézard
\dn petit et marron, vit dans l'herbe
\dt 08/Feb/2025

\lx tròròvwuu
\ph ʈɔɽɔwu:
\dialx GOs
\va tròròwuu
\dialx GO(s)
\is sentiments
\ps v ; n
\ge joie ; joyeux ; réjouir (se) ; content (être)
\xv e kîga mã la tròròvwu-je
\xn il rit du fait de ses joies
\xv e tròròvwuu-je
\dialx GOs
\xn il est content
\xv e tròròvwuu ui nhye ẽnõ-ã
\dialx GOs
\xn il est content de cet enfant
\xv e tròròwuu ãbaa-nu pexa pòi-je
\dialx GO
\xn mon frère est content de son enfant
\xv e tròròwuu pexa nye ẽnõ-ã
\dialx GOs
\xn il est content de cet enfant
\xv e tròròwuu pexa wõjo-je
\dialx GOs
\xn il est content de son bateau
\xv e tròròwuu pexa mõõ-je
\dialx GOs
\xn il est content de sa maison
\xv e za tròròwuu
\dialx GOs
\xn il en est content (il est content de cela)
\dt 07/Aug/2024

\lx trourou
\dialx GOs WEM
\is oiseau
\ps n
\ge tourou ; grive perlée
\sc Phylidonyris undulata
\scf Méliphagidés
\gb Barred Honeyeater
\dt 27/Oct/2023

\lx tröxi
\dialx GOs WE
\va tööxi
\dialx PA Paita
\sn 1
\is outils
\is armes
\ps n
\ge hache
\dn petite, en fer
\ge tamioc
\se jige tröxi
\dialx GOs WE
\sge fusil
\sn 2
\is matière
\ge fer
\bw toki (POLYN) (PPN *toki)
\dt 08/Feb/2025

\lx -tru
\ph ʈu
\dialx GOs
\va -ru, -lu
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge deux
\et *ɖua
\el POc
\dt 08/Oct/2021

\lx trûã
\ph ʈũ.ɛ̃
\dialx GOs WEM
\va thûã
\dialx PA
\va tûãn
\dialx BO
\sn 1
\is interaction
\ps v ; n
\ge mentir ; mensonge
\ge jouer des tours
\xv nu trûã i je
\dialx GOs
\xn je lui ai joué un mauvais tour
\xv kavwö yo thûã i nu ?
\dialx PA
\xn n'es-tu pas en train de me mentir ?
\sn 2
\is grammaire_comparaison
\ps COMPAR
\ge comparatif
\xv nu povwonû trûã nai çö
\dialx GOs
\xn je suis un tout petit peu plus petit que toi
\xv nu pò pwawali trûã nai çö
\dialx GOs
\xn je suis un peu plus grand que toi
\xv nu pò whamã trûã nai çö
\dialx GOs
\xn je suis un peu plus vieux que toi
\dt 09/Nov/2021

\lx truãrôô
\ph ʈu'ɛ̃ɽõ:
\dialx GOs
\va truãrô
\dialx BO
\va tuarôn
\dialx PA
\is insecte
\ps n
\ge araignée
\se tha-truãrôô
\sge toile d'araignée
\dt 29/Jan/2019

\lx truçaabèlè
\ph ʈu'dʒa:bɛlɛ, ʈu'ʒa:bɛlɛ
\dialx GOs
\va thruucaabèlè
\dialx arch.
\va tuyabèlèp
\dialx PA
\va tuyabèlè
\dialx BO
\is temps_atmosphérique
\ps n
\ge arc-en-ciel
\dt 09/Jan/2022

\lx truu
\dialx GOs
\va thuu
\dialx PA
\va tuu
\dialx BO
\is pêche
\ps v
\ge plonger (pour pêcher) ; faire de la plongée
\dt 26/Jan/2019

\lx truuçi
\ph ʈu:dʒi, ʈu:ʒi
\dialx GOs
\va tuuyi
\ph tu:yi
\dialx PA
\va tuji
\dialx BO
\is grammaire_numéral
\ps NUM
\ge dix
\se truuçi bwa po-xè
\dialx GOs
\va tuuyi bwa po-xe
\dialx PA
\sge onze
\se truuçi bwa po-tru  (10 plus/sur 2)
\dialx GOs
\va tuuyi bwa po-ru
\dialx PA
\sge douze
\se truuçi bwa po-kò
\dialx GOs
\va tuuyi bwa po-kòn
\dialx PA
\sge treize
\se truuçi bwa po-pa
\dialx GOs
\va tuuyi bwa po-pa
\dialx PA
\sge quatorze
\se truuçi bwa po-ni
\dialx GOs
\va tuuyi bwa po-nim
\dialx PA
\sge quinze
\se truuçi bwa po-ni-ma-xe
\dialx GOs
\va tuuyi bwa ponimaxe (< po-nim-(m)a-xe)
\dialx PA
\sge seize
\se truuçi bwa po-ni-ma-dru
\dialx GOs
\va tuuyi bwa ponimadu (< po-nim-(m)a-du)
\dialx PA
\sge dix-sept
\se truuçi bwa po-ni-ma-gò
\dialx GOs
\va tuuyi bwa ponimagòn (< po-nim-(m)a-gòn)
\dialx PA
\sge dix-huit
\se truuçi bwa po-ni-ma-ba
\dialx GOs
\va tuuyi bwa ponimaba (< po-nim-(m)a-ba)
\dialx PA
\sge dix-neuf
\dt 12/Jan/2022

\lx thra
\hm 1
\ph ʈʰa
\dialx GOs
\va tha(a), pa-tha
\dialx PA BO
\sn 1
\is caractéristiques_corps
\ps v.i
\ge chauve
\xv tha bwaa-n
\dialx BO
\xn il est chauve
\xv i pa-tha
\dialx PA
\xn il est totalement chauve
\xv i tha
\dialx PA
\xn il est chauve
\sn 2
\is caractéristiques_objets
\ps v.i
\ge court ; ras
\xv i coxe pu-n wu ra u thaa
\dialx PA
\xn il se coupe les cheveux pour qu'ils soient courts
\sn 3
\is soin
\ps v.i
\ge raser (se)
\xv e pe-thra Pwayili
\dialx GOs
\xn Pwayili se rase
\xv e pe-thra
\dialx GOs
\xn il se rase
\xv i pe-tha
\dialx PA
\xn il se rase
\xv ge je pe-thra
\dialx GOs
\xn il est en train de se raser (la barbe)
\xv i pe-ravhi
\dialx PA BO
\xn il se rase
\ng v.t. |lx{thrae}
\dt 03/Feb/2025

\lx thra
\hm 2
\ph ʈʰa
\dialx GOs
\va thal
\ph tʰal
\dialx PA BO
\is plantes
\ps n
\ge pandanus
\sc Pandanus sp.
\scf Pandanacées
\dt 06/Jan/2022

\lx thra
\hm 3
\ph ʈʰa
\dialx GOs
\va tha(a)
\dialx BO PA
\sn 1
\is soin
\ps v.t.
\ge raser
\ge tondre (poils)
\xv nu thrae pu-phwa-nu
\dialx GOs
\xn je me suis rasé la barbe
\xv e thrae pu-phwa-je
\dialx GOs
\xn il s'est rasé la barbe (ou) il a rasé la barbe de qqn. d'autre
\xv e thrae pu-phwa (xo) Pwayili
\dialx GOs
\xn Pwayili s'est rasé la barbe
\xv nu thrae pu-phwa Pwayili
\dialx GOs
\xn j'ai rasé la barbe de Pwayili
\ng v.i. |lx{thra}, v.t. |lx{thrae}
\dt 21/Feb/2025

\lx thraa
\ph ʈʰa:
\dialx GOs
\va thaa
\dialx PA BO
\va mwang
\dialx BO
\is caractéristiques_objets
\ps v.stat. ; MODIF
\ge mauvais ; dangereux ; mal
\xv thaa mõû-n
\dialx BO
\xn sa femme est méchante
\se êgu-raa
\dialx PA
\sge une personne méchante
\se ne-raa
\sge mauvais au goût ; faire mal
\xv e khõbwe-raa-i-nu
\dialx GO
\xn elle a dit du mal de moi
\xv e khõbwe-raa-ini êgu
\dialx GO
\xn elle a dit du mal de quelqu’un
\xv i khõbwe-raa-ini
\dialx PA
\xn il l’a mal dit
\xv i ne-raa
\dialx PA
\xn cela a mauvais goût
\xv i bo-raa
\dialx BO
\xn il sent mauvais
\xv i vhaa-raa nai yo
\dialx PA
\xn il médit/dit du mal de lui
\xv la nee-raa-ini
\dialx PA
\xn ils l'ont mal fait
\xv i nee-raa-ini ye
\dialx PA
\xn il lui a fait du tort/du mal
\xv i khõbwe-raa-ini
\dialx PA
\xn il l'a mal dit
\xv nu kõbwe-raa-ini
\dialx BO
\xn je l'ai mal raconté
\xv e jamwe-raa-ini
\dialx WEM
\xn elle s'est mal lavée
\xv i khibwa-raa-ini
\dialx PA
\xn il l'a mal coupé
\ng en composition, |lx{thaa} se prononce |ph{ra:} ; forme transitive dans une construction à verbe complexe: |lx{-raa-ini}
\et *(n)saqa(t)
\el POc
\dt 22/Feb/2025

\lx thraalo
\dialx GO
\va thaalo
\dialx BO (Corne)
\is plantes
\ps n
\ge plante
\sc Cordia myxa
\scf Boraginacées
\dt 27/Aug/2021

\lx thraanõ
\ph ʈʰa:ɳɔ̃
\dialx GOs
\is poisson
\ps n
\ge loche (de rivière, de taille adulte)
\sc Eleotris melanosoma
\scf Éléotridés
\cf baatro ; baaro
\ce lochon (juvénile)
\dt 17/Feb/2025

\lx thraavwã
\ph ʈʰa:βɛ̃
\dialx GOs
\va thraapã
\ph ʈʰa:pɛ̃
\va thaavwan
\dialx PA BO
\va thaavan
\dialx BO PA
\is eau_marée
\ps n
\ge marée descendante ; marée basse
\dt 08/Oct/2021

\lx thraba
\dialx GOs
\va thaba
\dialx PA BO
\is portage
\is action_corps
\ps v
\ge porter qqch. de lourd dans les bras
\ge soulever
\xv lhi kha-thaba jè-ò pòi-li dudu
\dialx PA BO
\xn elles portent en marchant leur petit crâne (conte)
\cf köe
\ce porter dans les bras
\dt 22/Mar/2023

\lx thrãbo
\ph ʈʰɛ̃bo
\dialx GOs
\is caractéristiques_objets
\ps v.stat.
\ge épais
\dt 06/Feb/2019

\lx thrabu
\dialx GOs
\va thabu
\dialx PA BO
\is pêche
\ps v
\ge pêcher en rivière
\dn avec un filet ou à la main
\se thrabu lapia
\sge pêcher le tilapia
\dialx GOs
\se thrabu kula
\dialx GOs
\sge pêcher la crevette
\dt 08/Feb/2025

\lx thrae
\hm 1
\ph ʈʰae
\dialx GOs
\va thaèè
\dialx BO [BM, Corne]
\is matière
\ps n
\ge argile à pot ; glaise
\dt 06/Jan/2022

\lx thrale
\ph 'ʈʰale
\dialx GOs
\va thalei
\dialx PA
\is natte
\ps n
\ge natte
\dn faite de deux palmes de cocotier tressées
\cf benõõ
\dt 25/Aug/2021

\lx thraliwa
\ph 'ʈʰaliwa
\dialx GOs
\va thaliwa
\dialx WEM
\va thaliang
\dialx PA
\is fonctions_naturelles
\ps v
\ge tuer (se) ; suicider (se)
\dt 23/Aug/2021

\lx thralo
\dialx GOs
\va thalo
\dialx PA BO
\is arbre
\ps n
\ge "cerisier bleu" ; "bois bleu"
\sc Eleocarpus angustifolius
\scf Elæocarpacées
\dt 27/Aug/2021

\lx thramwenge
\ph ʈʰa'mweŋe
\dialx GOs
\is action
\ps v
\ge fabriquer ; créer
\dt 29/Jan/2019

\lx thrao
\dialx GOs
\va thao
\dialx PA BO
\is plantes
\ps n
\ge champignon (terme générique)
\dt 29/Jan/2019

\lx thrao-da
\dialx GOs WEM
\va thrawa-da
\dialx GO(s)
\va thaa-da
\dialx PA BO
\is déplacement
\ps v
\ge arriver en haut
\xv thaa-da
\dialx BO
\xn jusqu'à
\an thrao-du
\at arriver en bas
\dt 25/Jan/2019

\lx thrava
\ph ʈʰava
\dialx GOs
\is caractéristiques_personnes
\is grammaire_modalité
\ps v.stat. ; n
\ge mauvais ; le mal
\xv lã thrava i ã
\xn nos mauvaises actions
\dt 03/Feb/2025

\lx thravayu
\dialx GOs
\va thayu
\dialx PA
\is fonctions_naturelles
\ps n
\ge bave de mort
\xv thayu-n
\dialx PA
\xn sa bave (d'un mourant)
\dt 24/Mar/2019

\lx thravwi
\dialx GOs
\ph ʈʰawɨ
\va thawi, tawi
\dialx PA BO
\is chasse
\ps v
\ge poursuivre (à la chasse)
\ge chasser ; écarter (chiens, volailles)
\xv e thawi poxa mèni
\dialx PA
\xn il chasse les petits oiseaux
\dt 21/Aug/2021

\lx thrawa
\ph ʈʰawa
\dialx GOs
\va thrao
\dialx GO(s)
\va thawa, thaa
\dialx BO
\wr A
\is déplacement
\ps v
\ge arriver ; survenir
\xv e u thrawa
\xn il est arrivé
\wr B
\is grammaire_préposition_locative
\ps PREP
\ge jusqu'à (lieu ou temps)
\wr C
\is grammaire_conjonction
\ps CNJ
\ge jusqu'à ce que
\se thrawa-da
\dialx GO
\sge jusqu'à ce que
\se thawa-da, thaa-da
\dialx BO
\sge jusqu'à ce que
\xv nu waayu thaa-da i phããde je wèdali-n
\dialx BO
\xn j'ai insisté jusqu'à ce qu'il montre sa fronde
\dt 14/Oct/2021

\lx thraxe
\ph ʈʰaɣe
\dialx GOs
\is sentiments
\ps v
\ge gémir (de douleur) ; hurler
\dt 27/Jan/2019

\lx thraxilo
\dialx GOs
\va thaxilò
\dialx PA
\va taxilò
\dialx BO
\is nourriture
\ps v
\ge manger vert et cru (des fruits)
\cf mãiyã
\dialx GOs
\ce cru ; pas cuit
\cf meâ
\dialx BOs
\ce cru ; pas cuit
\dt 10/Jan/2022

\lx thre
\dialx GOs
\va the
\dialx BO
\is plantes
\ps n
\ge mousse verte de rivière
\dt 29/Jan/2019

\lx thre kha
\dialx GOs
\va thèl
\dialx BO PA
\is cultures
\ps v
\ge préparer les champs (ignames) ; débroussailler les champs (ignames)
\cf threi
\ce couper
\dt 10/Feb/2019

\lx three
\dialx GOs
\va thee
\dialx BO [Corne]
\is action_plantes
\ps v
\ge rapporter des boutures chez soi
\dt 28/Aug/2021

\lx threi
\ph ʈʰei
\dialx GOs WEM
\va thei, thèi
\dialx BO PA
\sn 1
\is action_corps
\ps v.t.
\ge couper d'un coup
\ge abattre (arbre)
\ge frapper (qqn. pour le tuer)
\xv i thei u/xo/vwo wamòn
\dialx PA
\xn il l'a abattu avec une hache
\sn 2
\is bois_travail
\ps v
\ge tailler du bois
\ge sculpter
\xv a-thei cee
\dialx PA
\xn un sculpteur
\se the cee
\dialx PA
\sge sculpter du bois
\ng v.i. + objet indéfini |dialx{GOs} |lx{thre}
\gt couper
\cf dei, thibe
\ce couper
\dt 17/Feb/2025

\lx thre-mii
\ph ʈʰemi:
\dialx GOs
\va the-mii
\dialx PA BO
\is plantes
\ps n
\ge érythrinier à piquants
\dn à fleurs rouges
\se mu thre-mii
\dialx GOs
\sge fleur d'érythrinier
\dt 08/Feb/2025

\lx thri
\ph ʈʰi
\dialx GOs WEM
\va thiing, thing
\dialx BO [BM]
\va thiin
\dialx BO [Corne]
\is société_organisation
\ps n
\ge deuil de chef (coutumes)
\ge levée de deuil d'un Grand Chef
\se ce-thri
\dialx GOs
\sge perche qui annonce le décès d'un chef
\cf hauva
\ce levée de deuil pour le commun des mortels
\cf mõõdim
\ce coutumes de deuil (générique)
\dt 21/Feb/2025

\lx thria
\dialx GOs
\va thia
\dialx PA
\is coutumes
\ps n
\ge pardon coutumier
\se tha thria
\sge demander pardon
\xv li pe-tha-thria li-è ãbaa-nu êmwê
\dialx GOs
\xn mes deux frères se sont pardonnés
\xv mõ pe-tha-thria
\dialx GOs
\xn nous avons fait un pardon coutumier
\xv mõ pe-nhõî-thria
\dialx GOs
\xn nous avons fait un pardon coutumier (lit. nous avons attaché le pardon coutumier)
\dt 09/Jan/2022

\lx thridoo
\dialx GOs
\is poisson
\ps n
\ge murène
\sc Gymnothorax sp.
\scf Murénidés
\dt 24/Feb/2025

\lx thringã
\ph ʈʰiŋɛ̃
\dialx GOs
\va thrixã
\dialx GO(s)
\va thingã
\dialx BO
\is corps_animaux
\ps n
\ge queue (oiseau, animal)
\xv thingã-n
\dialx BO
\xn sa queue
\dt 10/Feb/2019

\lx thrimavwo
\dialx GOs
\va thrimapwo
\dialx GO(s)
\is poisson
\ps n
\ge picot
\dn de palétuvier, remonte les rivières
\ge poisson-papillon
\dt 08/Feb/2025

\lx thrîmi
\ph ʈʰîmi
\dialx GOs
\va thimi
\dialx PA BO
\is soin
\is action_corps
\ps v
\ge peindre
\ge enduire
\ge teindre (cheveux)
\xv ju thimi mèè-n
\dialx BO
\xn étale-la (crème) sur ton œil
\xv nu ru thimi mõ-ny u mii
\dialx BO
\xn je vais peindre ma maison en rouge
\se ba-thimi
\sge pinceau
\dt 20/Aug/2021

\lx thrirûû
\dialx GOs
\is interaction
\ps v
\ge ennuyer (s')
\xv e thrirûû
\xn elle s'ennuie
\dt 22/Mar/2023

\lx thriu
\ph ʈʰiu
\dialx GOs
\va thiu
\dialx BO
\is déplacement
\is mouvement
\ps v
\ge partir
\ge disperser (se)
\xv mô thriu !
\dialx GOs
\xn partons !
\xv la u thriu pe-haze !
\dialx GOs
\xn ils partent chacun de leur côté !
\xv la u thiu veale !
\dialx BO
\xn ils se sont dispersés !
\dt 22/Aug/2021

\lx thrivwaja
\ph ʈʰivwaja
\dialx GOs
\va thripwaja
\dialx arch. (Haudricourt)
\is vent
\ps n
\ge vent d'ouest
\dt 24/Dec/2021

\lx thrô
\dialx GOs
\ph ʈʰõ
\va thôm
\dialx PA BO
\is natte
\ps n
\ge natte (de pandanus) ; nœud
\xv thrô za ?
\dialx GOs
\xn une natte pour quel usage ?
\xv thrôbwo-nu
\dialx GOs
\xn ma natte
\xv thôbò-n
\dialx BO
\xn sa natte
\se thrôbwo covwa
\dialx GOs
\sge selle de cheval (une natte)
\se thrôbwo loto
\dialx GOs
\sge housse de siège de voiture (une natte)
\se thrôbwo ta
\dialx GOs
\sge nappe
\se thrôbwo vèlè
\dialx GOs
\sge drap
\se pa thrô
\sge faire une natte ; faire un nœud
\dt 22/Mar/2023

\lx thrõbo
\dialx GOs WEM
\ph ʈʰɔ̃mbo
\va thõbo
\ph tʰɔ̃mbo
\dialx BO PA
\sn 1
\is mouvement
\ps v
\ge tomber ; descendre
\xv e thrõbo-du
\dialx GOs
\xn il descend
\xv e kha-thrõbo pu trabwa
\dialx GOs
\xn il descend pour se poser (oiseau)
\xv e thrõbo gò
\dialx GOs
\xn il ne fait pas encore nuit (lit. cela décline encore)
\xv thõbo-du-mi !
\dialx PA
\xn descends ici !
\xv thõbo-mi jè pò-caai, thõbo-du ni dòò-phwa-n
\dialx PA
\xn une pomme canaque tombe ici, elle tombe sur ses lèvres
\xv i thõbo al
\dialx BO
\xn le soleil décline
\se we-thõbo
\sge prise/ouverture d'eau
\ng causatif: |lx{pa-thrõbo-ni}
\gt faire tomber ; faire descendre
\cf cö-du
\ce descendre de (cheval)
\cf cö-da
\ce débarquer
\sn 2
\is étapes_vie
\ps v
\ge naître
\xv nu thrõbo ni ka 1992
\dialx GOs
\xn je suis né en l'année 1992
\cf pwe
\ce naître
\dt 22/Feb/2025

\lx thrõbo trò
\dialx GOs
\va thòbwò tòn
\dialx PA
\is temps_découpage
\ps n
\ge tombée de la nuit
\dt 22/Mar/2023

\lx thrõb(w)ò
\dialx GOs
\ph ʈʰɔ̃bwɔ
\va thõbwòn, thõbòn
\dialx PA BO
\is temps_découpage
\ps n
\ge soir ; nuit
\xv za gaa õ thrõbò
\dialx GO
\xn tous les soirs
\xv ra u thõbwòn
\dialx BO
\xn il fait nuit
\dt 04/Nov/2021

\lx thrõgo
\ph ʈʰɔ̃ŋgo
\dialx GOs
\va thõgo
\dialx BO
\is action_plantes
\ps v
\ge cueillir
\dn des feuilles, herbes, jeunes pousses
\xv thrõgo dròò-cee
\dialx GOs
\xn cueillir des feuilles d'arbre
\ng v.t. |dialx{GOs} |lx{thrõgo-ni}
\gt cueillir qqch.
\dt 08/Feb/2025

\lx throli
\ph ʈʰoli
\dialx GOs
\is arbre_cocotier
\ps n
\ge rachis de coco
\dt 29/Jan/2019

\lx thròlò
\dialx GOs
\va thòlò
\dialx PA
\is maison
\ps v ; n
\ge couvrir (une maison) ; couverture (en général)
\ge faitage sculpté
\nt selon Dubois
\dt 26/Mar/2022

\lx thròlòe
\dialx GOs
\va thòlòe
\dialx PA
\va tòloè
\dialx BO (Corne)
\va tholòè, tòlee
\dialx BO (BM)
\va tholei
\dialx WEM
\is action_corps
\is mouvement
\ps v
\ge déplier
\ge étendre (bras, etc.)
\ge étaler (natte, etc.)
\ge étaler (s') (pour des plantes)
\ge ramper (lianes)
\xv e thròlòè thrô
\dialx GOs
\xn elle étale la natte
\xv cee xa thròlòè
\dialx GOs
\xn un arbre qui déploie (ses branches)
\xv tòlee thôm bwabu
\dialx BO
\xn étale une natte au sol
\dt 08/Nov/2021

\lx throloo
\dialx WEM
\is maison
\ps n
\ge décorations sur la flèche faîtière |lx{throo-mwa} de la maison (comportant des conques, etc.)
\dt 24/Feb/2025

\lx throo
\ph ʈʰo:
\dialx GOs WEM
\va thoo
\dialx PA BO
\sn 1
\ps n
\is corps_animaux
\ge crête ; aigrette
\xv throo-je
\dialx GOs
\xn son aigrette
\sn 2
\is habillement
\ps n
\ge plumet
\dn dans les cheveux ou la coiffure
\dt 08/Feb/2025

\lx throobwa
\dialx GOs
\va thobwang
\dialx PA
\is plantes
\ps n
\ge liane des endroits humides
\dn liane à fleurs blanches et à grosses feuilles; elles parasitent les arbres
\sc Ipomœa macrantha
\scf Convolvulacées
\dt 22/Oct/2021

\lx throo-mwa
\dialx GOs WEM
\va thoo-mwa
\dialx PA BO
\is maison
\ps n
\ge flèche faîtière
\dt 26/Jan/2019

\lx throonye
\dialx GOs
\is poisson
\ps n
\ge loche géante ; carite
\sc Epinephelus lanceolatus
\scf Serranidés
\dt 22/Feb/2025

\lx throvwa
\dialx GOs
\va thropwa
\dialx arch.
\is anguille
\ps n
\ge anguille de creek
\dn noire et verte, à grosse tête
\dt 08/Feb/2025

\lx thrûã
\ph ʈʰũɛ̃
\dialx GO
\is jeu
\ps n
\ge jeu de ficelle (figure du)
\dt 28/Jan/2019

\lx thrugò
\dialx GOs
\ph ʈʰuŋgɔ
\va thugò
\dialx PA
\is outils
\ps n
\ge herminette
\dt 27/Jan/2019

\lx thruumã
\dialx GOs
\va thuumã
\dialx PA BO
\va tuumã
\dialx PA BO (Corne)
\is sentiments
\ps v.stat. ; n
\ge heureux ; joyeux ; content ; joie
\xv i thuumã i a buram
\dialx BO
\xn il est content d'aller se baigner
\se pa-thuumã
\dialx BO
\sge rendre heureux
\dt 03/Feb/2025

\lx u
\hm 1
\dialx GOs PA
\is grammaire_aspect
\ps ASP.ACC
\ge accompli ; changement d'état
\xv u uça
\dialx GOs
\xn il est déjà arrivé
\xv ma u uça mwã Jae
\dialx GOs
\xn Jae est enfin revenu
\xv u õgi
\dialx GOs
\xn c'est fini
\xv u õgin
\dialx PA
\xn c'est fini
\xv mi za u mhaza a khila hãgana vwo mi baani
\dialx GOs
\xn mettons-nous à leur recherche maintenant pour les tuer
\xv lhi u tòne u hale thoo je we
\dialx PA
\xn elles entendent que le bruit de l'eau est différent
\dt 21/Dec/2021

\lx u
\hm 2
\dialx PA BO [BM, Corne]
\va o
\dialx PA BO
\is grammaire_préposition
\ps PREP (instrumental)
\ge avec ; par ; grâce à ; du fait de ; à cause de
\xv teemwi u yi ã
\dialx BO
\xn on les attrape avec la main (crabes)
\xv i ra ku thiloo-je taagin u mwani
\dialx PA
\xn il lui demande souvent de l’argent
\xv nu mã o/u tuuyòng
\dialx PA
\xn je meurs de froid
\xv nu thebe kumwala o hèlè-m
\dialx BO
\xn pelé la patate douce avec ton couteau
\xv nu ru thimi mõ-ny u mii
\dialx BO
\xn je vais peindre ma maison en rouge
\dt 21/Dec/2021

\lx u
\hm 3
\dialx GOs BO
\is mouvement
\is grammaire_direction
\ps v
\ge pencher (se)
\ge baisser (se)
\ge courber (se)
\dn par ex. pour entrer dans une maison
\se u-du !
\sge baisse-toi !
\se u-da !
\sge entre ! (dans la maison)
\se u-e !
\sge entre ! (latéralement)
\xv e u kha-tòè hii-je
\dialx GOs
\xn tout en se penchant, elle tend (lit. lance) son bras
\xv i u-du mwã al
\dialx BO
\xn le soleil s'est couché
\dt 24/Feb/2025

\lx u
\hm 4
\dialx GO PA BO
\va ku, xu, xo, (vw)o
\dialx GO PA BO
\va (h)u
\dialx BO
\is grammaire_agent
\ps AGT ; sujet
\ge sujet ; agent (marque)
\xv i whili u ti/ri ?
\xn qui l'a mangé (la canne à sucre) ?
\xv i oole dèn u je cee
\dialx PA
\xn cet arbre a barré la route
\xv i pa-tòni gò u ti/ri ?
\dialx BO
\xn qui joue de la musique ?
\xv i thòxe na inu u ciia
\dialx BO
\xn le poulpe s'est collé à moi
\xv nõli je mwa, mã je i thadi u dèèn
\dialx BO
\xn regarde cette maison, c’est celle que le vent a abîmée
\dt 26/Mar/2023

\lx u
\is mouvement
\hm 5
\dialx GOs PA
\ps v
\ge tomber
\xv u je cee
\xn cet arbre est tombé
\xv e u cee xa we-xe
\dialx GOs
\xn un arbre est tombé
\xv lha u la-ã cee
\dialx GOs
\xn ces arbres sont tombés
\xv paò khi-u-je ya bwabu
\dialx PA
\xn (ils) lancent (la sagaie), frappent et le terrassent au sol
\ng v.t. |lx{ûûni}
\dt 09/Jul/2024

\lx û
\dialx GOs
\is arbre_cocotier
\ps n
\ge spathe de cocotier
\ge étoffe
\se û-vwa nu
\sge fibre du cocotier
\dt 17/Aug/2021

\lx u pwa, u-vwa
\dialx GO PA
\va u-va
\dialx WEM
\is déplacement
\ps DIR
\ge aller vers l'extérieur
\se u pwa !
\sge sors ! ; dehors !
\dt 31/Oct/2021

\lx u ru
\dialx WE PA
\is grammaire_temps
\ps FUT
\ge futur proche
\xv u ru tòn
\dialx PA
\xn il va faire nuit
\xv u ru pwal !
\dialx PA
\xn il va pleuvoir
\xv u ru pwal ha ka uru pwal?
\xn va-t-il pleuvoir ou non?
\xv u ru còge yo !
\dialx BO
\xn tu vas te couper ! [BM]
\cf ru
\dt 24/Mar/2019

\lx ua
\dialx BO
\is igname
\ps n
\ge igname
\dn violette, selon Dubois
\dt 17/Feb/2025

\lx uãme
\dialx GOs PA
\is fonctions_intellectuelles
\ps v
\ge ne pas voir l'évidence
\xv wa ra uãme-nu ?
\xn vous ne m'avez pas vu? (comment est-ce possible ?)
\dt 08/Feb/2025

\lx ubò
\ph ubɔ
\dialx GOs
\is déplacement
\ps v
\ge sortir (de la maison)
\xv e ubò na ni mwa
\dialx GOs
\xn il est sorti de la maison
\xv ubò-du pwa !
\dialx GOs
\xn va dehors !
\xv e ubò pwa
\dialx GOs
\xn il sort dehors
\xv kixa vhaa na ezoma ubò na ni phwa-õ
\dialx GOs
\xn aucun mot ne sortira de nos bouches
\dt 26/Dec/2021

\lx ûbo
\dialx GOs
\is action
\ps v
\ge défaire (se) tout seul
\xv e ûbo mhenõ-mhõ
\xn le nœud s'est défait
\dt 20/Mar/2023

\lx ubòl
\dialx PA BO [Corne]
\is action_corps
\ps n
\ge détacher
\ge lâcher
\ge laisser
\dt 20/Aug/2021

\lx uça
\ph uʒa
\dialx GOs
\is déplacement
\ps v
\ge arriver
\xv ti nye uça xòlò i çö ?
\xn qui t'a rendu visite hier ?
\xv e uça xòlò i çö xo ti ?
\xn qui t'a rendu visite hier ?
\xv nu uça-da na Frans
\xn j'arrive de France
\xv e uçaa mwa-mi êne
\xn il revient ici
\dt 21/Dec/2021

\lx uda
\dialx BO
\is maison
\ps n
\ge poutre sablière
\dn poutre circulaire supportant la charpente des cases rondes (selon Dubois)
\nt mot non vérifié
\dt 22/Oct/2021

\lx u-da
\dialx GOs
\is déplacement
\ps v
\ge entrer
\dn se baisser pour entrer dans une maison
\ge monter
\xv e u-da-mi
\xn il est entré ici
\xv e u-da ni mwa
\dialx GOs
\xn il est entré dans la maison
\an u-du
\at se baisser pour sortir
\dt 08/Feb/2025

\lx udang
\dialx BO
\is taro
\ps n
\ge taro (clone) de terrain sec
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx udale
\dialx GOs PA
\is habillement
\ps v
\ge mettre (un vêtement)
\dn (lit. monter dedans)
\xv e udale hõb(w)òli-je
\dialx GOs
\xn il met son vêtement
\xv e udale hõbwò xo Kaavwo
\dialx GOs
\xn Kaavwo met son vêtement
\xv e udale hõbwòli Kaavwo xo Hiixe
\dialx GOs
\xn Hiixe a enfilé les vêtements de Kaavo
\xv e udale simi
\dialx GOs
\xn il met sa chemise
\xv i udale cimic
\dialx PA
\xn il met sa chemise
\xv i udale orop u Kaavwo
\dialx PA
\xn Kaavo a mis/met sa robe
\cf thai hõbò
\ce enfiler ; mettre
\an udi
\at enlever
\dt 22/Feb/2025

\lx udi
\dialx GOs BO
\sn 1
\is habillement
\ps v
\ge enlever (en général, vêtement, etc.)
\ge ôter
\xv e udi simi i je
\dialx GOs
\xn il enlève sa chemise
\xv e udi mwêêga-je
\dialx GOs
\xn il enlève son chapeau
\an udale
\at mettre (vêtement)
\sn 2
\is action_corps
\ps v
\ge extraire (épine, etc.) ; sortir
\dt 20/Aug/2021

\lx ûdo
\dialx GOs
\is arbre
\ps n
\ge palétuvier
\dn dont les feuilles sont comestibles
\dt 08/Feb/2025

\lx u-du
\dialx GOs PA BO WEM
\sn 1
\is mouvement
\ps v
\ge baisser (se) ; pencher (se) |dialx{GOs}
\ge coucher (se) (soleil) |dialx{GOs}
\ge plonger |dialx{GOs, BO}
\ge enfoncer (s') (dans la boue, le sable) |dialx{GOs, BO}
\ge disparaître |dialx{GOs, BO}
\xv e u-du a
\dialx GOs
\xn le soleil se couche
\xv u-du al
\dialx BO
\xn le soleil se couche
\xv u-du ni dili
\dialx BO
\xn s'enfoncer dans la boue
\xv i u-du ni pira hoogo
\dialx BO
\xn il disparaît derrière la montagne
\sn 2
\is déplacement
\ps v
\ge rentrer (dans un trou, dans l'eau) |dialx{GOs, BO}
\ge sortir (d'une maison) |dialx{GOs, BO}
\dn se baisser pour sortir d'une maison
\xv têê udu xo Thonòòl pwa
\dialx PA
\xn Thonòòl sors de la maison en courant dehors
\se u-pwa, u-du-pwa
\dialx GOs
\sge se baisser pour sortir (|lx{pwa} dehors)
\dt 08/Feb/2025

\lx ui
\hm 1
\dialx GOs PA BO
\sn 1
\is feu
\ps v
\ge souffler (sur le feu)
\xv i ui yaai
\dialx BO PA
\xn elle ravive/attise le feu en soufflant
\se ba-u
\sge éventail pour attiser le feu
\sn 2
\is médecine
\ps v
\ge souffler des feuilles médicinales (pour guérir)
\se ba-u
\sge feuilles médicinales (qu'on souffle pour guérir)
\dt 25/Aug/2021

\lx ui
\hm 2
\dialx GOs
\va wi
\is grammaire_préposition
\ps PREP
\ge au sujet de ; envers ; à propos de (+ animés préférentiellement) ; à cause de
\xv e kiiça ui ãbaa-je xo êmwê-è
\xn cet homme est jaloux de son frère
\xv e pana-nu ui çö
\xn il m'a grondé à cause de toi
\xv e thuvwu vhaa ui je xo Hiixe çai Kaavo
\xn Hiixe parle d'elle-même à Kaavo
\xv e thô ui nu
\xn il en colère envers/après moi
\xv e za yaawa ui lie whamã ?
\xn est-il nostalgique de ses parents ?
\xv kavwa e bwòvwô ui la ẽnõ ni mõ-chomu ?
\xn n'est-il pas fatigué des enfants de l'école?
\xv e tròròvwuu ui nye ẽnõ-ã
\xn il est content de cet enfant
\xv axe mõ kò vwo mõ pe-mwãã ui ã
\xn mais nous sommes debout là pour nous soutenir mutuellement entre/envers nous
\xv ai-ã vwo mõ nee mwã xa mwaje kêê-ã mãni kibu-ã ui ewööni-ã kòi-je mwã
\xn nous voulons procéder à la façon de nos pères et grands-pères envers notre oncle maternel qui n'est plus là désormais
\ng |lx{pune meedree}
\gt à cause du temps (mais *ui meedree est agrammatical)
\cf pexa, pune
\ce à cause de
\dt 21/Feb/2025

\lx ui gò
\dialx GOs
\is musique
\ps v
\ge jouer de la flûte
\se gòò-ui
\sge flûte
\dt 28/Jan/2019

\lx ui-bööni
\dialx GOs PA BO
\is feu
\ps v
\ge éteindre en soufflant (bougie, allumette)
\xv i ui-bööni ya-pao
\dialx GO
\xn elle éteint l'allumette en soufflant
\dt 23/Aug/2021

\lx uilu
\dialx GOs WEM
\va uvilu
\dialx PA
\sn 1
\is oiseau
\ps n
\ge échenilleur Pie
\dn réputé annoncer des nouvelles
\sc Lalage leucopyga simillima
\scf Campéphagidés
\sn 2
\is oiseau
\ps n
\ge fauvette à ventre jaune (Wapipi)
\sc Gerygone flavolateralis flavolateralis
\scf Acanthizidés
\gb Long-tailed Triller
\dt 08/Feb/2025

\lx ul
\dialx PA BO
\sn 1
\is caractéristiques_personnes
\ps v
\ge pauvre ; indigent
\se êgu ul
\dialx BO
\sge un pauvre
\sn 2
\is grammaire_modalité
\ps v
\ge inutile
\ge gaspiller |dialx{BO}
\xv pe-ul
\xn ce n'est rien ; ce n'est pas grave
\xv i ul
\dialx BO
\xn il n'est bon à rien
\xv dili xa i ul
\dialx BO
\xn une mauvaise terre ; une terre laissée à l'abandon
\dt 22/Feb/2025

\lx ula
\hm 1
\dialx GOs BO
\sn 1
\is action
\ps v.t.
\ge chasser ; éloigner (des insectes)
\sn 2
\is action_corps
\ps v.t.
\ge éventer (s')
\xv nu ula-nu
\dialx GOs
\xn je m'évente
\se ba-ula
\dialx GOs
\va ba-ul
\dialx PA BO
\sge éventail
\sn 3
\is feu
\ps v.t.
\ge attiser (en éventant)
\xv i ula yaai
\dialx PA BO
\xn elle attise le feu
\et *ura
\el PSO
\ea Geraghty
\dt 08/Feb/2025

\lx ula
\hm 2
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge grappe de noix de coco
\xv ula-xè ula-nu
\dialx PA
\xn une grappe de noix de coco
\dt 17/Aug/2021

\lx ula yaai
\dialx GOs PA
\is feu
\ps v
\ge attiser le feu
\cf ui
\ce souffler
\dt 24/Mar/2019

\lx ulavwi
\dialx GOs
\is action
\ps v
\ge dégager ; faire place nette ; nettoyer (champ)
\xv e ulavwi dònò
\xn le ciel est totalement dégagé
\dt 29/Jan/2019

\lx uli
\dialx PA WE BO
\va uzi
\dialx GOs
\is action_plantes
\ps v
\ge lisser ; rendre lisse (tige)
\ge biseauter
\xv uli u dom
\dialx PA
\xn tailler en pointe
\dt 20/Aug/2021

\lx ulo
\dialx BO
\is feu
\ps v
\ge brûler ; flamber
\nt mot non vérifié
\dt 03/Feb/2019

\lx ulò
\hm 1
\dialx GOs PA BO
\is insecte
\ps n
\ge sauterelle
\se êgo ulò
\sge larve de sauterelle
\dt 24/Mar/2019

\lx ulò
\hm 2
\dialx GOs
\is poisson
\ps n
\ge poisson-volant
\dt 29/Jan/2019

\lx ulu
\dialx GOs
\is mouvement
\ps v
\ge enfoncer (s')
\ge embourber (s')
\ge descendre verticalement
\xv e ulu a
\dialx GOs
\xn le soleil se couche
\xv ulu loto
\xn la voiture s'est embourbée
\xv xa za trêê-ulu-ò ã-e hèlè-ã
\xn et le couteau s'enfonce sans obstacle
\dt 05/May/2024

\lx umau
\dialx BO
\is action_corps
\ps v
\ge tituber
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx ûne
\dialx GOs
\is préparation_aliments
\ps v
\ge passer sur la flamme pour assouplir
\dn par ex. une feuille de bananier
\xv nu ûne dròò-chaamwa
\xn je passe la feuille de bananier sur la flamme
\dt 08/Feb/2025

\lx u-pwai
\dialx GOs
\va u-pwaim
\dialx PA WEM
\va pwai
\dialx PA
\is nourriture_tabac
\ps v
\ge fumer (tabac)
\xv e u pwai
\dialx GOs
\xn il fume
\cf u
\ce souffler ; fumer
\dt 22/Feb/2025

\lx uramõ
\ph u'ramɔ̃
\dialx GOs
\is religion
\ps n
\ge lutins ; petits génies
\cf jee
\dialx PA
\ce lutins
\dt 10/Jan/2022

\lx urîni
\dialx GOs PA
\is soin
\is action_corps
\ps v
\ge frotter
\ge masser
\ge frictionner (avec des plantes ou des pommades)
\dt 22/Mar/2023

\lx uu
\dialx PA BO
\is action_plantes
\ps v
\ge cueillir (fleur, feuilles, bourgeons)
\xv la uu dòò-cee
\dialx PA
\xn elles cueillent des feuilles
\xv la uu mû-cee
\dialx BO
\xn elles cueillent des fleurs
\dt 22/Feb/2025

\lx ûû
\dialx GO
\va ôô
\is discours_interjection
\ps INTJ
\ge hmm
\dn rythme le discours de quelqu'un
\dt 08/Feb/2025

\lx ûûni
\dialx GOs PA
\is action
\ps v
\ge faire osciller les arbres (vent)
\ge faire tomber (qqch. sur pied) démolir une maison
\xv i ûû-ni cee u dèèn
\dialx PA
\xn le vent a fait tomber l'arbre
\xv i paò khi-ûû-ni-du bwa kaò
\dialx PA
\xn elle le lance (bout de bois) et le fait tomber dans le courant
\xv lha tha-uu-ni la-ã cee
\dialx GOs
\xn ils ont fait tomber ces arbres (en les poussant)
\dt 17/Feb/2025

\lx uva
\dialx GOs BO
\is taro
\ps n
\ge taro d'eau (pied de) (nom générique)
\se dròò-uva
\dialx GOs
\sge feuille de taro d'eau
\se kê-uva
\sge champ de taros ; tarodière
\se kò-uva
\sge la partie inférieure du taro
\dt 24/Feb/2025

\lx uvilu
\dialx PA
\is oiseau
\ps n
\ge petit lève-queue
\sc Rhipidura fuliginosa bulgeri
\scf Rhipiduridés
\gb Grey Fantail
\dt 23/Jan/2022

\lx uvo-ê
\dialx GOs
\va uvo-èm
\dialx PA
\is cultures
\ps n
\ge bouture de canne à sucre
\dt 22/Aug/2021

\lx uvo-uva
\dialx GOs
\is taro
\is cultures
\ps n
\ge bouture de taro d'eau
\dn il s'agit d'un pédoncule de taro muni d'une tige
\ge pied de taro d'eau
\et *upe
\eg plante
\el POc
\dt 24/Feb/2025

\lx uvwa
\ph uβa
\dialx GOs
\is interaction
\ps v ; n
\ge accepter la demande de pardon
\ge nom du don pour la demande de pardon
\an taçuuni
\at refuser la demande de pardon
\dt 19/Aug/2021

\lx uvwi
\ph uβi
\dialx GOs
\va upi
\dialx GO(s)
\va uvi
\dialx PA BO
\is échanges
\ps v ; n
\ge acheter ; payer
\ge prix ; salaire
\xv e u uvwi xo kêê-nu axè-êgu pò-mã na kòlò Pwayili
\xn mon père a acheté vingt mangues à Pwayili
\xv e u uvwi xo kêê-nu na kòlò Pwayili axè-êgu pò-mã
\xn mon père a acheté vingt mangues à Pwayili
\xv nu uvi yaa hii-n
\dialx BO
\xn je le lui ai acheté
\xv pwaalu uvi-n
\dialx BO
\xn c'est cher
\an içu, iyu
\at vendre
\dt 21/Dec/2021

\lx uvwia
\ph uβia
\dialx GOs PA BO
\va upia, uvhia
\dialx PA BO
\is taro
\ps n
\ge taro (de montagne)
\dn nom du tubercule ou du pied de taro
\sc Colocasia esculenta
\scf Aracées
\se uvwia kari
\sge taro de montagne jaune
\se wèè-uvwia ; wèè-uvhia
\sge racines du taro de montagne
\cf kutru, kuru
\ce tubercule de taro d'eau
\dt 27/Aug/2021

\lx uxi
\dialx GOs
\is action_corps
\ps v
\ge percer ; faire des trous ; ronger
\se ba-uxi cee
\sge perceuse à bois
\dt 08/Nov/2021

\lx uxo
\dialx GO
\is plantes
\ps n
\ge Crinum sp.
\sc Crinum sp.
\scf Amaryllidacées
\dt 27/Aug/2021

\lx uza
\dialx GOs
\va ula
\dialx PA BO
\is interaction
\ps v
\ge moquer de (se)
\xv la uza-nu
\dialx GOs
\xn ils se moquent de moi
\xv la uza-je
\dialx GOs
\xn ils se moquent de lui
\xv la ula-je
\dialx BO
\xn ils se moquent de lui
\dt 25/Aug/2021

\lx uzi
\dialx GOs
\va uli
\dialx PA WE BO
\sn 1
\is tressage
\is action_plantes
\ps v
\ge assouplir les fibres en les lissant
\ge lisser (une tige)
\sn 2
\is bois_travail
\is action_outils
\ps v
\ge raboter ; lisser
\xv e uzi cee
\dialx GOs
\xn il rabote le bout de bois
\dt 08/Feb/2025

\lx uzi cii
\dialx GOs
\is action_plantes
\ps v
\ge enlever les fibres
\dn en lissant avec un couteau
\dt 08/Feb/2025

\lx uzi dròò
\dialx GOs
\va uli dòò
\dialx PA
\is action_plantes
\ps v
\ge effeuiller une tige
\dt 20/Aug/2021

\lx va
\hm 1
\dialx GOs
\va za
\dialx PA
\is grammaire_pronom
\ps PRO.1° pers. excl. PL (sujet)
\ge nous (excl.)
\dt 27/Mar/2022

\lx va
\hm 2
\dialx GOs
\is classificateur_possessif_nourriture
\ps n
\ge part de nourriture donnée dans les coutumes
\xv vangi-nu
\xn ma part d'igname
\dt 14/Oct/2021

\lx va ?
\dialx WEM BO
\is grammaire_interrogatif
\ps INT (statique)
\ge où ?
\xv yo êgu va ?
\dialx BO
\xn d'où es-tu ?
\xv ge va la mu-cee ?
\dialx BO
\xn où sont les fleurs ?
\cf iva?
\ce où?
\et *pe
\eg où?
\el POc
\dt 14/May/2024

\lx vaaci
\dialx PA BO WEM
\is mammifères
\ps n
\ge vache ; bétail
\bw vache (FR)
\dt 10/Oct/2021

\lx vajama
\ph va'ɲɟama
\dialx GOs PA BO
\va fhajama
\dialx GA
\is discours_tradition_orale
\ps n ; v
\ge mythe d'origine ; conte
\xv nu zo(ma) vajama-ni nhye zixô
\dialx GOs
\xn je vais raconter ce conte
\ng v.t. |lx{vajama-ni}
\dt 05/Jan/2022

\lx vala
\dialx PA
\is action
\ps v
\ge ajouter
\xv mwã ruma vala-ni mwã je-nã
\dialx PA
\xn nous ajouterons cela
\ng v.t. |lx{vala-ni}
\dt 05/Jan/2022

\lx valèèma
\dialx GOs
\is caractéristiques_objets
\ps v.stat.
\ge lisse
\dt 22/Jan/2018

\lx vali
\dialx GOs PA
\is instrument_ponts
\ps n
\ge pierre constituant un passage/pont
\dn ce pont couvre une conduite d'eau (|lx{de we} conduisant l'eau d'une tarodière à l'autre
\dt 31/Jan/2022

\lx vana
\dialx GOs
\is cultures_champ
\is taro
\ps n
\ge tarodière en terrasse et irriguée
\se thu vana
\sge faire la tarodière en terrasse
\dt 26/Aug/2021

\lx vara
\dialx GOs PA
\is grammaire_distributif
\ps dispersif ; DISTR
\ge séparément ; chacun
\xv lha vara a
\dialx GOs
\xn ils partent chacun de leur côté
\xv mõ vara thiu mwã a trabwa ni mhenõ-trabwa
\dialx GOs
\xn nous nous disperserons pour rejoindre nos demeures
\xv mõ vara pu phò-ã
\dialx GOs
\xn nous avons chacun notre charge
\xv lhi vara a ni dee-li
\dialx GOs
\xn ils partent chacun sur leur chemin
\xv lhi vara phe 1000F xo ãbaa-nu mã nata
\dialx GOs
\xn mon frère et le pasteur ont pris 1000F chacun
\xv ãbaa-nu mã nata, lhi vara phe 1000F
\dialx GOs
\xn mon frère et le pasteur, ils ont pris 1000F chacun
\xv lhi vara uvwi loto ka we-xe
\dialx GOs
\xn ils ont chacun acheté une voiture
\xv bî vara kibaò a-kò bwò mã ãbaa-nu
\dialx GOs
\xn mes frères et moi avons tué trois roussettes chacun
\xv vara Poimenya õã-lò
\dialx GO
\xn leurs mères sont chacune des Poymegna
\dt 22/Mar/2023

\lx varan
\dialx PA
\is maison
\ps n
\ge véranda
\bw véranda (FR)
\dt 25/Aug/2021

\lx varû
\dialx BO
\is action_corps
\ps v
\ge tripoter
\nt selon BM
\dt 27/Mar/2022

\lx vauma
\dialx GOs BO
\is terrain_pierre
\ps n
\ge pierre-ponce
\dt 28/Jan/2019

\lx vaxaròò
\dialx GOs PA BO
\is eau_mer_plante
\ps n
\ge patate de corail
\ge caillou blanc ressemblant à une patate de corail
\cf paa-karòò
\ce pierre-corail
\dt 08/Feb/2025

\lx -ve
\dialx PA BO
\is grammaire_suff_directionnel
\ps DIR (transverse)
\ge en s'éloignant sur un axe transverse
\dn en allant d'une vallée à l'autre, ou pour traverser un cours d'eau
\xv i phe-ve
\dialx PA
\xn emporter (transverse)
\dt 27/Aug/2023

\lx vea
\dialx GOs
\is matière
\ps n
\ge verre ; vitre
\bw verre (FR)
\dt 29/Jan/2019

\lx vee
\dialx GOs WEM
\is discours
\ps n
\ge discussion
\dn pour savoir comment procéder lors d'une coutume
\se mõ-vee
\sge maison pour discuter comment procéder pour la coutume
\dt 08/Feb/2025

\lx vèlè
\dialx GOs PA
\va baalaba
\dialx PA BO
\is maison_objet
\ps n
\ge lit
\xv thrôbo vèlè
\xn drap de lit
\dt 22/Mar/2023

\lx vi
\dialx GOs
\is poisson
\ps n
\ge carangue
\dt 29/Jan/2019

\lx vijang
\dialx PA BO
\sn 1
\is coutumes_objet
\ps n
\ge bouquet de fibres
\dn il est accroché sur un piquet pour marquer un interdit ou montrer qu'une plante est réservée
\xv tha vijang
\dialx PA
\xn attacher un bouquet de fibres
\xv i thu vijang
\dialx BO
\xn attacher un bouquet de fibres
\sn 2
\is coutumes_objet
\ps n
\ge nœud
\dn fait avec une herbe, signale un interdit, protège des magies
\dt 22/Oct/2021

\lx vwo
\hm 1
\dialx GO PA
\va ko, xo
\dialx GO(s)
\is grammaire_conjonction
\ps COORD
\ge et
\xv cabo mwã nhyã we na khazia mõ-je vwo pe-tho-du mwã
\dialx GOs
\xn une source a jailli près de sa maison et elle coule vers le bas
\dt 26/Jan/2022

\lx vwo
\hm 2
\dialx GOs PA
\va xo
\dialx GOs
\va vo
\dialx BO
\is grammaire_IS
\ps FOCUS in situ
\ge marque de sujet
\xv nu nõõli vwo/xo inu
\dialx GOs
\xn c'est moi qui l'ai vu
\xv ti xa na i a ? – Nu a u/vwo inu
\dialx PA
\xn qui est-ce qui est parti ? – C'est moi qui suis parti (xa : indéfini)
\xv ti je i a ? – Nu a u/vwo inu
\dialx PA
\xn qui est parti ? – C'est moi qui suis parti
\xv ti je i a ? – Nu a vo nu
\dialx BO
\xn qui part ? – C'est moi qui pars
\ng |lx{xo} est utilisé avec la même fonction de focus
\dt 22/Feb/2025

\lx vwo
\hm 3
\dialx GOs
\va po, vwö
\dialx GO(s)
\va (p)u
\dialx PA BO
\is grammaire_conjonction
\ps CNJ
\ge que ; pour que ; afin que ; si
\xv e paxeze-nu vwo nu a-mi kòlò-je
\dialx GO
\xn il m’a empêché de venir chez lui
\xv la pe-zage u la khabe nye mwa
\dialx PA
\xn ils s’entraident pour construire cette maison
\dt 22/Dec/2021

\lx -vwo
\dialx GOs PA BO
\va -vo
\dialx BO
\wr A
\is grammaire_suffixe_valence
\ps SUFF.detransitivant
\ge suffixe détransitivant
\xv mo a nhii-vwo
\dialx GOs
\xn nous allons faire la cueillette
\xv phee-vwo !
\dialx PA
\xn servez-vous !
\xv e tii-vwo
\xn il écrit
\xv la na-vwo
\xn ils font des dons ; ils font la coutume
\se hine-vwo
\sge intelligent
\wr B
\is grammaire_dérivation
\ps NMZ
\ge nom déverbal
\xv u pònu lhã mhenõ-vwo ni mee-jèè
\dialx GOs
\xn elle a beaucoup de cicatrices/de plaies sur le visage
\se na-vwo
\sge cadeau (donner) ; coutumes
\se nee-vwo
\sge acte ; action (faire)
\ng |lx{-vwo} vient de |lx{po}
\gt chose
\dt 22/Feb/2025

\lx vwö
\hm 1
\dialx GOs
\va vwo
\dialx GOs
\va (vw)u
\dialx BO PA
\va wi
\dialx BO
\is grammaire_conjonction
\ps CNJ
\ge pour que ; que
\xv e khõbwe vwö mõ a
\dialx GOs
\xn il nous a dit de partir
\xv kô-zo na cö whili-nu vwö nu nõõ-je ?
\dialx GO
\xn serait-il possible que tu m'emmènes, pour que je la voie ?
\xv la phèle po waramã
\dialx PA
\xn ils les roulent (en torons) pour les ceintures
\xv la pe-zage u la khabe nye mwa
\dialx PA
\xn ils s’entraident pour construire cette maison
\dt 22/Feb/2025

\lx vwö
\hm 2
\dialx GOs
\is grammaire_IS
\ps THEM
\ge quant à
\ng forme courte de |lx{novwö}
\dt 05/Jan/2022

\lx vhaa
\dialx GOs PA BO
\va fhaa
\dialx GA
\is discours
\ps v ; n
\ge parler ; parole
\ge voix
\xv vhaa i je
\dialx GO
\xn sa voix
\xv vhaa i la
\dialx PA
\xn leurs voix
\xv fhaa whamã
\dialx GA
\xn la parole des anciens
\xv nu fha cai-co
\dialx GA
\xn je te parle
\xv ge le vhaa
\dialx PA
\xn il y a une nouvelle/une information
\xv la vhaa ni yuanga
\dialx BO
\xn ils parlent yuanga
\xv la vhaa na dòò-cee
\dialx BO
\xn ils discutent de ces feuilles
\se vhaa baaxò
\sge parler juste
\se fhaa ka ku-gòò
\dialx GA
\sge parole véritable
\se khõbwe vhaa-raa
\dialx PA
\sge dire des grossièretés
\se paxa-vhaa
\sge mot
\se pu-fhaa
\dialx GA
\sge cause/origine du discours
\dt 22/Feb/2025

\lx vhaa caaxo
\dialx BO
\is discours
\ps v
\ge parler doucement ; murmurer
\dt 22/Feb/2025

\lx vhaa draalae
\dialx GOs
\va vhaa daleen
\dialx PA
\is discours
\ps n
\ge français
\dt 28/Jan/2019

\lx vhaa-raa
\dialx GOs PA
\is interaction
\ps v
\ge jurer
\dn (lit. parler mauvais)
\ge dire des gros mots ; médire
\dt 08/Feb/2025

\lx vhaa-zo
\dialx GOs
\is discours
\ps v
\ge parler bien ; parler clairement
\an vhaa-raa
\at parler mal
\dt 08/Feb/2025

\lx vhaò
\dialx GOs
\va fhaò
\dialx GA
\is instrument
\ps n
\ge fil de fer (barbelé)
\dt 27/Jan/2019

\lx vhii
\dialx GOs BO
\va vii
\dialx GO(s) BO
\va fhi
\dialx GO(s)
\is fonctions_naturelles
\ps v
\ge péter (grossier)
\cf tabawi (+ poli)
\ce avoir des vents
\dt 25/Jan/2019

\lx vhiliçô
\ph vilidʒõ
\dialx GOs
\is santé
\ps n
\ge bourbouille
\dt 25/Jan/2019

\lx wa
\hm 1
\dialx GOs
\va wal
\dialx PA BO
\va wòl
\dialx WEM
\sn 1
\is cordes
\ps n
\ge corde (général) ; ficelle
\ge liane ; lien
\xv wa-bwa-nu
\dialx GOs
\xn mon serre-tête
\xv wa-bwa-ny
\dialx PA
\xn mon serre-tête
\xv wa-gòò-hii-n
\dialx PA
\xn son brassard
\se wa-rama
\dialx GOs
\sge ceinture de soutien
\se wa-kiò, wa-ki
\dialx GOs
\sge ceinture de soutien ventral
\se gu-wa-ala-xò
\dialx GOs
\sge lacet de chaussure (gu: piquer comme une filoche)
\se pha-wal
\dialx PA
\sge attacher
\ng forme déterminée: |lx{wazi-je} |dialx{GO}, |lx{wali-n} |dialx{PA, BO}
\gt sa corde
\sn 2
\is corps
\ps n
\ge veine ; artère
\ge tendon
\se wa ni bwèèdrò
\dialx GOs
\sge la veine du front (on spécifie toujours la partie du corps où se trouve la veine)
\se wal kamun
\dialx PA
\sge artères
\et *waRoc
\el POc
\dt 21/Mar/2023

\lx wa
\hm 2
\dialx GO
\va wal
\dialx PA BO
\is plantes_partie
\ps n
\ge racine
\cf we-
\ce forme en composition ou détermination
\et *wakaR
\eg racine
\el POc
\dt 08/Oct/2021

\lx wa
\hm 3
\dialx BO [Corne]
\is grammaire_conjonction
\ps PREP ; CNJ
\ge à cause de ; parce que
\xv nu bware wa nyamã
\dialx BO
\xn je suis fatigué à cause de mon travail
\nt mot non vérifié
\dt 24/Mar/2019

\lx wa
\hm 4
\dialx GOs
\va wal
\dialx PA BO
\is fonctions_naturelles
\is musique
\ps v ; n
\ge chant ; chanter
\xv la wa bulu
\dialx GOs
\xn ils chantent ensemble
\xv e wa-zo
\dialx GOs
\xn il chante bien
\xv e waze-zoo-ni wa nye
\dialx GOs
\xn il a bien chanté cette chanson
\ng v.t. |lx{waze}
\dt 06/Jan/2022

\lx wa
\hm 5
\dialx GOs PA
\is grammaire_pronom
\ps PRO.2° pers. PL
\ge vous (plur.)
\dt 26/Aug/2023

\lx wa-
\dialx GOs PA BO
\va wan-
\dialx BO PA
\is classificateur_numérique
\ps CLF.NUM
\ge lot cérémoniel de deux roussettes ou notous 
\xv wa-xè b(w)ò ko ido
\dialx GOs
\xn un lot de trois roussettes (une paire de roussettes et une demi-paire)
\xv wa-tru bwò
\dialx GOs
\xn deux lots de deux roussettes ou notous dans les dons coutumiers
\xv wa-tru bwò ko ido
\dialx GOs
\xn deux lots de deux roussettes plus une (deux paires de roussettes et une demi-paire)
\xv wa-kò bwò
\dialx GOs
\xn trois lots de deux roussettes
\xv wa-pa bwò
\dialx GOs
\xn quatre lots de deux roussettes
\xv wa-truuçi bwò
\dialx GOs
\xn dix lots de deux roussettes
\xv wan-xe, wa-tu, wa-kon, wa-pa, wa-nim
\dialx BO PA
\xn 1, 2 , 3, 4, 5 lots de deux roussettes ou notous
\cf wa(l)
\ce lien
\dt 21/Feb/2025

\lx wã
\dialx GOs WEM
\va whã(ã)
\dialx GOs PA
\va wãme
\dialx GO(s)
\sn 1
\is grammaire_comparaison
\ps v.COMPAR ; SIMIL
\ge comme (être)
\ge faire comme ; faire ainsi
\xv e nee wãã-na
\dialx GOs
\xn il l'a fait ainsi (anaphorique)
\xv ne xo wã
\dialx GOs
\xn fais-le ainsi !
\xv novwö gunè-n, e gun whã nhyô
\dialx PA
\xn quant à son grondement, il retentit comme le tonnerre
\xv yu khò nõõli kõbwe i whãã ili jili thoo-we
\dialx PA
\xn vois un peu comment est l'écoulement de l'eau
\xv wã ai-ne
\xn comme il veut (Dubois)
\xv wã ãgu
\xn en tant qu'homme (Dubois)
\se i wã-na
\sge il (est resté) comme cela (BO, Dubois)
\cf whaya ?
\ce être comment ?
\sn 2
\is discours
\ge dire ainsi ; faire ainsi
\xv e wã mwa xo Kaawo : "ko (= kawa, kavwö) ço nõõli pòi-nu ?"
\dialx GOs
\xn Kaawo fait/dit : "n'as-tu pas vu mon enfant ?"
\xv axe wã xo dòny
\dialx PA
\xn mais la buse fait/dit ainsi
\xv i wã : "kavwu jo nõõli ja pòi-ny?"
\dialx PA
\xn elle fait/dit : "n'as-tu pas vu mon enfant?"
\dt 22/Feb/2025

\lx wa ne hii
\is corps
\dialx GO
\ps n
\ge tendons du bras
\xv kulaçe wa ne hii-nu
\xn mes tendons de bras sont raides/endoloris
\dt 22/Feb/2025

\lx waa
\dialx BO [BM, Corne]
\va wò
\dialx GO(s)
\is action_tête
\ps v
\ge ouvrir la bouche
\dt 26/Jan/2019

\lx wãã
\dialx GOs
\va wã
\is grammaire_direction
\ps PREP.LOC.DIR
\ge vers (direction approximative)
\xv nu weena khõbwe e a-wãã-du ênè
\dialx GOs
\xn je pense qu'il est passé en bas par là (approximativement)
\xv e a-wãã-du êbòli
\xn il est allé vers le bas là-bas [faux: * e a-wãã-du bòli]
\xv e no-wã-du
\xn elle regarde vers le bas
\dt 22/Feb/2025

\lx wa-aazo
\dialx GOs PA
\va wa-aazo ni kò
\dialx PA WEM
\va wa-ayo
\dialx BO
\is corps
\ps n
\ge tendon d'achille
\dn (lit. le tendon du chef)
\dt 09/Feb/2025

\lx wããdri
\dialx GOs
\is poisson
\ps n
\ge brème bleue
\sc Acanthopagrus berda
\scf Sparidés
\dt 27/Aug/2021

\lx waang
\dialx PA WEM BO
\is temps_découpage
\ps n
\ge matin ; aube ; premières lueurs du jour
\xv u waang
\dialx WEM
\xn c'est l'aube
\dt 28/Jan/2019

\lx wââng
\dialx PA WE
\va wãwã
\dialx GO(s)
\is oiseau
\ps n
\ge corbeau
\sc Corvus moneduloides
\scf Corvidés
\dt 27/Aug/2021

\lx waajô
\dialx PA
\is interaction
\ps v
\ge accroché à sa mère (enfant)
\xv i waajô kai jo
\dialx PA
\xn il est collé à toi
\dt 20/Feb/2019

\lx waal
\dialx PA BO
\is religion
\ps n
\ge religion
\dt 20/Feb/2019

\lx wãã-na
\dialx GOs PA BO
\is grammaire_manière
\ps v.COMPAR ; v.SIMIL
\ge faire ainsi ; être ainsi
\xv cö yuu wãã-na
\dialx GO
\xn tu restes comme ça
\xv kavwö nu trõne me ge-le xa thoomwã  na wãã-na
\dialx GO
\xn je n'ai pas entendu dire s'il y a une femme qui est comme cela
\dt 11/Mar/2023

\lx waara
\dialx GOs PA BO
\va waatra
\dialx GO(s)
\is fonctions_naturelles
\ps v
\ge grossir ; grandir ; croître (plantes)
\ge pousser (en longueur et en largeur)
\se pa-waara
\sge faire grandir
\dt 23/Aug/2021

\lx waat
\dialx BO [Corne]
\is plantes
\ps n
\ge Triumfetta rhomboïdea
\sc Triumfetta rhomboïdea
\scf Malvacées
\dt 27/Aug/2021

\lx waawè
\dialx GOs PA BO
\va waapwè
\dialx GO(s) BO (Corne)
\is arbre
\ps n
\ge pin colonnaire
\dn symbole des hommes et de la durée du lignage
\sc Araucaria columnaris
\scf Araucariacées
\cf jeyu
\ce kaori
\dt 24/Dec/2021

\lx wa-bile
\dialx GOs BO PA
\is cordes
\ps v ; n
\ge toron (roulé sur la cuisse) ; fil de filet
\ge faire un toron
\dn roulé sur la cuisse
\dt 08/Feb/2025

\lx wabwa
\dialx GOs BO PA
\va wabwa pwojo
\dialx GO(s)
\is arbre
\ps n
\ge "bois de chou"
\sc Gyrocarpus americanus
\scf Hernandiacées
\dt 27/Aug/2021

\lx wa-bwanu
\dialx PA BO
\is coutumes_objet
\ps n
\ge ceinture faite en fibre d'écorce de banian ou |lx{alamwi}
\dn elle est tressée de trois brins de paille ou de feuilles de pandanus ; on y mettait les pierres et les paquets magiques pour la guerre ; c'est aussi le signe du départ en guerre (Dubois ms.)
\dt 12/Jan/2022

\lx wa-bwèèdrò
\dialx GOs
\is habillement
\va wa-bwa
\dialx GO(s)
\ps n
\ge bandeau ; turban
\dt 23/Jan/2018

\lx waçãmã
\dialx GOs
\is caractéristiques_personnes
\ps v.stat.
\ge nerveux ; pressé
\dt 27/Oct/2023

\lx wacamaa
\dialx GOs
\is plantes
\ps n
\ge liane
\dt 27/Oct/2023

\lx waçuçu
\ph wadʒudʒu, waʒuʒu
\dialx GOs WEM WE
\va waaçu 
\dialx GO(s) WEM WE
\va waayu, waaju
\dialx PA BO
\is interaction
\ps v.stat.
\ge efforcer de (s') ; persister à ; insister
\ge perséverer ; persévérant
\ge obliger
\xv me waaçu vwö me khila xa whaya me tròòli xo mwani
\dialx GOs
\xn nous nous efforçons de chercher comment gagner de l'argent
\xv êgu ka e a-waçuçu
\dialx GOs
\xn une personne persévérante
\xv e za waçuçu vwo a kai-je haxe e za hããxe-je
\dialx GOs
\xn elle se force à le suivre bien qu'elle ait très peur de lui
\xv hã peve waaju khõbwe hã tooli dè-ra mwani
\dialx PA
\xn nous tous nous efforçons de trouver comment gagner de l'argent (lit. le chemin de l'argent)
\se kò-waayu
\dialx PA
\sge persister à
\dt 23/Apr/2024

\lx wa-do
\dialx GOs PA BO
\is armes
\ps n
\ge ligature de la sagaie
\cf wa
\dialx GOs
\ce corde ; lien
\cf wal
\dialx PA BO
\ce corde ; lien
\dt 22/Feb/2025

\lx wadrö
\dialx GOs
\va waado
\dialx BO
\sn 1
\is caractéristiques_corps
\ps v.stat.
\ge édenté ; interstice laissé par des dents tombées
\xv waado-parôô-n
\dialx BO
\xn l'interstice laissé par ses dents manquantes (Dubois)
\sn 2
\is caractéristiques_objets
\ps v.stat.
\ge ébréché (pour une lame de couteau)
\dt 22/Mar/2023

\lx wadrolò
\dialx GOs
\is crustacés
\ps n
\ge carapace nouvelle du crabe
\cf pitrêê, a-pii, pwaji nhyatru
\dt 22/Mar/2023

\lx waga
\dialx BO
\is taro
\ps n
\ge taro d'eau (clone)
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx wãga
\dialx GOs BO
\is position
\ps v
\ge jambes écartées (être)
\xv ti nye thu-menõ wãga ?
\xn qui marche les jambes écartées ?
\se ku-wãga
\sge debout jambes écartées
\se kô-wãga
\sge couché jambes écartées
\se te-wãga
\sge assis jambes écartées
\dt 10/Feb/2019

\lx wanga
\ph waŋa
\dialx GOs
\va whaga-n
\dialx BO
\is fonctions_intellectuelles
\ps n
\ge sens ; signification ; raison
\xv kixa wanga
\dialx GOs
\xn cela n'a aucun sens
\dt 12/Jan/2022

\lx wãge
\dialx GOs BO PA
\is corps
\ps n
\ge poitrine
\xv wãgee-nu
\dialx GOs
\xn ma poitrine
\xv wãgee-n
\dialx PA BO
\xn sa poitrine
\dt 10/Feb/2019

\lx wagiça
\ph wagiʒa
\dialx GOs
\is caractéristiques_objets
\ps v
\ge dur ; résistant (personne, corde)
\cf kulaçe
\ce dur (viande)
\cf piça
\ce dur
\dt 13/Jan/2022

\lx wa-hi
\dialx GOs PA BO
\va wa-yi
\dialx BO
\is habillement
\ps n
\ge bracelet
\dn (lit. lien-bras)
\ge brassard
\xv wa-hii-je
\dialx GO
\xn son bracelet
\xv wa-hii-n
\dialx PA BO
\xn son bracelet
\dt 08/Feb/2025

\lx wakagume
\dialx GOs
\is position
\ps v
\ge renversé ; chaviré
\dn (se dit d'une voiture, d'un bateau)
\dt 09/Feb/2025

\lx wa-kaze
\dialx GOs
\va wa-kale
\dialx GO(s)
\is eau_marée
\ps n
\ge marée haute
\et *Rua(p)
\eg marée
\el POc
\dt 29/Jan/2019

\lx wa-kiò
\dialx GOs PA BO
\va waki, wa-kiò
\dialx PA
\is habillement
\ps n
\ge ceinture
\xv wa-kiò-nu
\dialx GOs
\xn ma ceinture
\xv wa-kiò-n
\dialx PA BO
\xn sa ceinture
\dt 25/Jan/2019

\lx wal
\dialx PA BO
\is fonctions_naturelles
\ps v ; n
\ge chant ; chanter (personne, oiseau)
\ng v.t. |lx{wale}
\dt 19/May/2024

\lx wala
\dialx GOs
\is caractéristiques_objets
\ps v.stat.
\ge large
\an pivwizai
\at étroit
\dt 22/Jan/2018

\lx walaga
\ph wa'laŋga
\dialx GOs
\is caractéristiques_personnes
\ps v
\ge rusé ; malin ; qui joue des tours
\dt 26/Jan/2019

\lx wala-me
\dialx BO
\is grammaire_comparaison
\ps COMPAR
\ge comme ; semblable ; pareil
\nt mot non vérifié (Dubois)
\dt 03/Feb/2019

\lx walei
\dialx GOs BO
\is igname
\ps n
\ge igname
\sc Dioscorea esculenta Lour.
\scf Dioscoréacées
\bw walei (POLYN) (PPN *walei)
\dt 27/Aug/2021

\lx waluvwi
\dialx GOs
\va waluvi
\dialx BO
\is nourriture
\ps n
\ge chair de coco verreuse |dialx{GO}
\ge jus de coco tourné |dialx{BO}
\dt 08/Feb/2025

\lx wa-mãgiça
\ph wamɛ̃ŋgiʒa
\dialx GOs
\is cordes
\ps n
\ge chaîne
\dn (lit. attache dure)
\dt 09/Feb/2025

\lx wã-me
\dialx GOs WEM
\va whã-me, wãã-me
\dialx GO(s)
\va wã-mèèn
\dialx BO
\is grammaire_comparaison
\ps COMPAR
\ge comme ; pareil ; semblable
\xv e nee wã-me nu
\dialx GOs
\xn il fait comme moi
\xv e trûã mwã ã zine, xa nye whã-me ne e tròròvwuu-je
\dialx GOs
\xn le rat ment en prétendant qu'il se réjouit
\xv axa [= ai-nu xa] mõû-nu na wã-me mõû-çö
\dialx GOs
\xn je voudrais une épouse qui soit comme la tienne
\xv e nee wã-me kêê-je
\dialx GOs
\xn il fait comme son père
\xv e ra êgu wãme-ã
\dialx PA
\xn c'est un homme comme nous
\xv i vhaa wã-mèèn Tèè-ma
\dialx BO
\xn il parle comme un chef
\xv i vhaa wã-mèèn
\dialx BO
\xn il parle comme lui
\xv kavu la wã-mè la yu nee ?
\dialx BO
\xn n'est-ce pas ainsi que tu as fait ?
\dt 24/Aug/2023

\lx wã-me da ?
\dialx GOs PA
\va whã-me da?
\dialx GOs PA
\va wã da?
\dialx PA
\is grammaire_interrogatif
\ps INT
\ge comme quoi? ; qui ressemble à quoi?
\dt 16/Feb/2025

\lx wã-me ne
\ph wãme ɳe
\dialx GOs
\va whã-me ne
\dialx GOs PA
\is grammaire_conjonction
\ps v
\ge faire en sorte que
\xv e wã-me ne khõbwe (e) novwö na li tho ilie-bòli, ça e chaaxö ã we-zumee-je
\dialx GOs
\xn elle a fait en sorte que, lorsque les deux (parents) en bas appellent, sa salive réponde
\xv wã-me ne e tia-wãã-le-da
\dialx GO
\xn il fait en sorte de pousser comme cela vers le haut
\dt 22/Feb/2025

\lx wãme ni
\dialx GOs
\va whãme ni
\dialx GOs
\is grammaire_locatif
\ps LOC
\ge environs (aux) de ; vers
\dt 27/Jan/2022

\lx wã-me ti ?
\dialx GOs WEM
\va whã-me ti?
\dialx GOs WEM
\is grammaire_interrogatif
\ps INT.COMPAR
\ge comme qui ?
\dt 16/Feb/2025

\lx wã-mèèn exa
\dialx BO [Corne]
\is grammaire_comparaison
\ps CNJ
\ge comme si
\xv i nee yhe wã-mèèn e xa yo phããde
\dialx BO
\xn il l'a fait comme ce que tu as montré
\dt 11/Mar/2023

\lx wamòn
\dialx PA WEH BO
\va wamò, wamwa
\dialx GO(s)
\sn 1
\is armes
\is outils
\ps n
\ge hache
\ge tamioc
\xv pai-ny wamòn
\dialx BO
\xn ma hache
\sn 2
\is outils
\ps n
\ge herminette
\xv wamòn paa
\dialx BO
\xn herminette (lit. hache pierre)
\dt 15/Sep/2021

\lx wamwa
\dialx GOs WEM
\va wamòn
\dialx PA
\sn 1
\is outils
\is armes
\ps n
\ge hache
\ge tamioc
\sn 2
\is outils
\ps n
\ge herminette
\dt 15/Sep/2021

\lx wã-na
\dialx PA
\is grammaire_modalité
\ps MODAL
\ge peut-être ; et si ?
\ge faillir
\xv wã-na nu thuã
\dialx PA
\xn je me trompe peut-être
\xv ra u wã-na i mòròm
\dialx PA
\xn il a failli se noyer
\xv wã-na mi tee-a ?
\dialx PA
\xn et si on partait d'abord ?
\xv wã-na ti ?
\dialx PA
\xn qui cela peut-il bien être ?
\ng forme courte de |lx{wãme na}
\dt 12/Mar/2023

\lx waniri
\dialx PA BO WE
\is parenté_alliance
\ps n
\ge maternels (clan des)
\cf a-kaalu
\dialx GO
\ce maternels (dans les cérémonies de deuil)
\dt 16/Feb/2025

\lx wa-pwe
\dialx GOs
\is pêche
\ps n
\ge fil de la ligne
\dt 26/Jan/2019

\lx wara
\dialx GO
\va waza
\dialx GOs
\va w(h)ara-n
\dialx BO PA
\is temps
\ps n
\ge moment ; époque ; période
\ge saison
\ge heure ; temps
\xv wara u mõ a
\dialx GOs
\xn c'est le moment de notre départ
\xv wara u xa za e trabwa ni nõ-ma-òli
\dialx GOs
\xn quand il était encore assis à cet endroit là-bas
\xv ni wara da ?
\dialx PA
\xn à quelle époque ?
\xv e wara thu phoê
\dialx PA
\xn c'est l'époque de faire nos cultures
\xv ni wara al
\dialx PA BO
\xn période ensoleillée ; saison sèche
\xv ni wara khabu
\dialx BO
\xn saison froide
\xv u wara hovwo
\dialx BO
\xn c'est l'heure de manger
\xv u ta wara
\dialx BO
\xn le moment est arrivé
\xv ra thu wara-ny
\dialx BO PA
\xn j'ai le temps
\xv ra gaa thu whara-ny
\dialx PA
\xn j'ai encore le temps
\se whara ò thèl, wharaa thèl
\dialx PA
\sge époque du débroussaillage (mai)
\se õ wara ne
\dialx PA
\sge chaque fois que
\dt 22/Feb/2025

\lx waramã
\dialx GOs PA
\va wara
\dialx GO(s)
\is habillement
\ps n
\ge ceinture
\xv waramã i nu
\xn ma ceinture
\dt 05/Nov/2021

\lx warô
\dialx BO PA
\is cultures
\ps n
\ge graine ; semence
\se warô kaè
\sge des graines de pastèque
\xv warô-n
\xn sa graine
\dt 22/Aug/2021

\lx wa-truuçi
\dialx GO
\is classificateur_numérique
\ps CLF.NUM
\ge dix paires (de roussettes ou de notous dans les dons coutumiers)
\dt 22/Feb/2025

\lx wathrã
\dialx GOs
\is coutumes
\ps n
\ge lien coutumier
\xv wathrã-mã
\xn notre lien coutumier
\dt 17/Feb/2025

\lx wa-vwo
\dialx GOs
\is grammaire_modalité
\ps MODAL
\ge peut-être que oui
\dn réponse à une question
\dt 08/Feb/2025

\lx wawa
\dialx GOs PA BO
\is parenté_adresse
\ps n
\ge grand-papa
\dt 18/Aug/2021

\lx wãwã
\dialx GOs
\va wââng
\dialx PA WE
\is oiseau
\ps n
\ge corbeau
\sc Corvus moneduloides
\scf Corvidés
\dt 27/Aug/2021

\lx wa-xè
\dialx GOs PA
\is classificateur_numérique
\ps CLF.NUM
\ge une paire (de roussettes ou notous)
\dn dans les dons coutumiers
\xv wa-xè bwò
\dialx GOs
\xn une paire de roussettes
\dt 08/Feb/2025

\lx waza
\dialx GOs
\va wara
\dialx PA
\sn 1
\is temps
\ps n
\ge moment ; époque ; heure
\sn 2
\is grammaire_conjonction
\ps n.CNJ
\ge quand
\xv ẽgõgò waza-ò nu ẽnõ
\dialx GOs
\xn avant quand j'étais enfant
\xv çö a-mi ni waza-ò ia ?
\dialx GOs
\xn à quelle époque es-tu arrivé ?
\xv waza-ò me ẽnõ gò !
\xn quand nous étions encore enfants
\dt 16/Feb/2025

\lx waza o kole pwa
\dialx GOs
\va waza ò kole pwal
\dialx PA
\is temps_saison
\ps n
\ge saison des pluies, des cyclones, des grandes marées
\dn de mars à avril
\dt 08/Feb/2025

\lx waza o thaa kui
\dialx GOs
\is temps_saison
\ps n
\ge saison de la récolte des ignames
\dt 28/Jan/2019

\lx waza o thòe kui
\dialx GOs
\is temps_saison
\ps n
\ge saison de la plantation des ignames
\dn de juillet à septembre
\dt 08/Feb/2025

\lx wazale kòò
\dialx GOs
\va wè-rali kòò
\dialx BO
\is action_corps
\ps v
\ge faire un croc-en-jambe
\xv i wè-rali kòò-n
\dialx BO
\xn il lui a fait un croc-en-jambe [Corne]
\cf thali
\dt 22/Feb/2025

\lx wazizibu
\dialx GOs
\is mouvement
\ps v
\ge trébucher
\xv e wazizibu bwa paa
\dialx GOs
\xn il a trébuché sur une pierre
\dt 21/Feb/2018

\lx we
\dialx GOs PA BO
\is eau
\ps n
\ge eau
\se kòli we
\sge au bord de l'eau
\se gu we
\sge eau potable
\se gaa-we
\sge cascade
\se we-pwa
\dialx GOs
\va we-pwal
\dialx PA BO
\sge eau de pluie
\se we ni mèè-n
\dialx PA
\sge ses larmes
\et *wai(R)
\el POc
\dt 24/Jan/2022

\lx we
\dialx GOs
\is grammaire_pronom
\ps PRO 2° pers.triel (sujet, OBJ ou POSS)
\ge vous trois
\dt 29/Mar/2022

\lx wè-
\dialx GOs PA BO
\is classificateur_numérique
\ps CLF.NUM (général et des objets longs)
\ge CLF des objets longs
\dn tels que voiture, bateau, arbre couché, poteau, sagaie, corde, doigt
\ge CLF des années, mois
\ge CLF des chants
\xv lhi ra u khabe je do xa we-xe ênè
\dialx PA
\xn ils lui plantent une sagaie ici (accompagné d'un geste)
\xv wè-xè ka
\dialx GOs
\xn un an
\xv wè-tru mhwããnu
\dialx GOs
\xn deux mois
\xv wè-kò ka
\dialx GOs
\xn trois ans
\xv wè-pa ka
\dialx GOs
\xn quatre ans
\dt 09/Feb/2025

\lx we mii
\dialx GOs
\is nourriture
\ps n
\ge vin
\dn (lit. eau rouge)
\dt 08/Feb/2025

\lx we ni mè
\dialx GOs
\va we ni mèè-n
\dialx PA BO
\is corps
\ps n
\ge larmes
\xv we ni mèè-ny
\dialx PA
\xn mes larmes
\dt 24/Jan/2019

\lx we-ãmu
\dialx BO PA
\is nourriture
\ps n
\ge miel
\cf pi ãmu
\ce miel
\dt 24/Mar/2019

\lx we-bumi
\dialx BO [Corne]
\is habillement
\ps n
\ge étoffe d'écorce de banian
\nt non vérifié
\dt 24/Mar/2019

\lx we-bwaxixi
\dialx GOs
\va we-bwaxii
\dialx GO(s)
\is instrument
\ps n
\ge miroir
\cf we-zhido
\dialx GOs
\ce miroir
\dt 10/Jan/2022

\lx we-cabo
\dialx GOs
\va we-cabòl
\dialx PA
\is eau
\ps n
\ge source d'eau
\dt 29/Jan/2019

\lx we-cee
\dialx GOs BO PA
\is plantes_partie
\ps n
\ge sève d'arbre
\cf dixa-cee
\dialx GOs PA
\ce sève d'arbre
\dt 10/Jan/2022

\lx we-cò
\dialx GOs
\is corps
\ps n
\ge sperme
\dt 22/Aug/2021

\lx wèda
\dialx GOs
\va wèdal
\dialx PA BO
\is armes
\ps n
\ge fronde
\xv hi-wèdal
\dialx BO
\xn le doigtier de la fronde
\ng |lx{wèdali-n} |dialx{PA BO}
\gt sa fronde
\dt 05/Jan/2022

\lx wèdò
\dialx BO PA
\va wôdò
\dialx PA
\is coutumes
\ps v ; n
\ge actes coutumiers ; coutumes
\ge usages ; manières ; mœurs
\xv wèdòò-ni bulu ni la wèdòò-la
\dialx PA
\xn faisons nos coutumes pour leur cérémonie coutumière
\xv wèdòò-n
\dialx BO
\xn ses coutumes ; ses manières
\xv wôdòò-n
\dialx PA
\xn ses coutumes ; ses manières
\xv wèdò daaleèn
\dialx BO
\xn les coutumes des européens
\dt 22/Feb/2025

\lx we-ê
\dialx GOs
\va we-èm
\dialx WE
\va cuk
\dialx PA
\is nourriture
\ps n
\ge sucre
\dn (lit. jus de canne à sucre)
\dt 08/Feb/2025

\lx wèè-
\dialx GOs PA
\va wèè-n
\dialx PA BO
\is plantes_partie
\ps n
\ge racine de
\mr forme composée ou déterminée de |lx{wa(l)} racine
\se wèè bumi
\sge racines de banian
\se wèè cee
\dialx GO PA BO
\sge racines d'arbre
\xv wèè-n
\dialx PA
\xn sa racine
\cf wa ; wal
\ce racine
\et *wakaR
\eg racine
\el POc
\dt 10/Jan/2022

\lx weem
\dialx PA BO
\is richesses
\ps n
\ge monnaie
\dn de moindre valeur que |lx{yòò}, cette monnaie est faite de coquillages blancs, offerte attachée à un rameau de niaouli ou de bananier ; l'autre nom de cette monnaie est |lx{hêgi pulo} (selon Dubois : 1 weem de 5 m vaut 100 fr) ; hiérarchie des valeurs : |lx{yòò > weem > yhalo}
\cf pwãmwãnu ; dopweza; yòò
\dt 08/Feb/2025

\lx weena
\ph we:ɳa
\dialx GOs PA BO
\is fonctions_intellectuelles
\ps v
\ge penser (incertain) ; croire (sans être sûr)
\xv nu weena khõbwe e a-wãã-du ênè
\dialx GOs
\xn je pense qu'il est passé par là (en montrant du doigt)
\xv nu weena khõbwe e a-du ênè
\dialx GOs
\xn je pense qu'il est descendu par ici
\xv nu weena khõbwe e a-è ênõli
\dialx GOs
\xn je pense qu'il est passé par là (transverse)
\xv kavwö me wero, pu nye me weena me ezoma la bòzi-me
\dialx GOs
\xn nous n'avons pas fait de bruit, parce que nous pensions qu'ils nous auraient punis à coup sûr
\xv nu pe-weena
\dialx GOs
\xn je pense/ il me semble (incertain)
\ng v.t. |lx{weena-ni}
\dt 22/Feb/2025

\lx wèè-uva
\dialx GOs PA BO
\is taro
\ps n
\ge racines du taro d'eau
\dt 17/Aug/2021

\lx we-hêê-du
\dialx GOs
\va bòyil, böyil
\dialx PA
\is corps
\ps n
\ge moëlle des os
\dt 24/Mar/2019

\lx we-hoogo
\dialx PA
\is topographie
\ps n
\ge versant ; pente de la montagne
\dt 21/Dec/2021

\lx we-imã
\dialx GOs BO PA
\is fonctions_naturelles
\ps n
\ge urine
\xv we imòò-je
\dialx GO
\xn son urine
\dt 07/Nov/2021

\lx we-kae
\dialx GOs
\is ustensile
\ps n
\ge calebasse (servant à porter l'eau)
\dt 26/Jan/2019

\lx we-kênõng
\dialx BO
\is eau
\ps n
\ge tourbillon ; contre-courant
\dt 29/Jan/2019

\lx wèle
\dialx BO [BM, Corne]
\is guerre
\ps v ; n
\ge battre (se) ; bagarre
\xv yo wèle mã ri ?
\dialx BO
\xn avec qui t'es-tu battu ?
\dt 24/Aug/2021

\lx we-mebo, mebo
\dialx GOs
\is nourriture
\ps n
\ge miel
\dt 26/Jan/2019

\lx we-mèèn
\dialx PA
\is eau
\ps n
\ge eau légèrement salée ; eau saumâtre
\cf mèèn
\ce salé (cuisine)
\dt 29/Jan/2019

\lx we-ne
\ph weɳe
\dialx GOs
\va we-nèm
\dialx BO PA
\is eau
\ps n
\ge eau douce
\dt 02/Jan/2022

\lx wène
\dialx BO PA
\is habillement
\ps n
\ge étoffe d'écorce de banian
\dn fait à partir des racines
\nt selon Dubois ms ; non vérifié
\dt 08/Feb/2025

\lx wêne
\dialx GOs PA BO
\va wône
\is interaction
\dialx GOs
\ps v
\ge changer ; échanger ; remplacer
\xv nu wêne mõ-ny
\dialx BO
\xn je déménage
\xv i tee wêne-nu
\dialx PA
\xn il me remplace
\dt 20/Feb/2019

\lx wè-ni
\ph weɳi
\dialx GOs
\va wè-nim
\dialx PA BO
\is classificateur_numérique
\ps NUM.ORD
\ge cinq (objets longs)
\xv nye za u wè-ni
\dialx GOs
\xn cela fait cinq
\se wè-ni-mã-xè
\dialx GOs
\sge six (objets longs)
\dt 31/Jan/2022

\lx weni-do
\dialx BO
\is terrain_pierre
\ps n
\ge serpentine verte
\nt selon Dubois ; non vérifié
\dt 27/Mar/2022

\lx wè-ni-mã-dru
\dialx GOs
\va wè-nim (m)ã-du
\dialx PA BO
\is classificateur_numérique
\ps NUM
\ge sept (choses longues)
\xv wè-nim-(m)a-du kau-ny
\dialx BO
\xn j'ai sept ans
\dt 24/Jan/2022

\lx wè-niza ?
\dialx GOs
\va we-nira ?
\dialx PA BO
\is grammaire_interrogatif
\ps INT
\ge combien (de choses longues, jours, an) ?
\xv wè-niza wô ?
\dialx GOs
\xn combien de bateaux ?
\xv wè-niza ka ?
\dialx GOs
\xn combien d'années ?
\xv we-niza tèèn ?
\dialx PA
\xn combien de jours ?
\dt 11/Mar/2023

\lx we-nõ
\dialx PA
\va weze nõ; wizi nõ
\dialx GOs
\is étapes_vie
\ps v
\ge pendre (se)
\dt 24/Mar/2019

\lx we-nu
\ph weɳu
\dialx GOs BO
\is arbre_cocotier
\ps n
\ge lait de coco ; coco à boire
\cf nu-wee
\ce coco vert
\dt 02/Jan/2022

\lx we-pò-cee
\dialx GOs
\is nourriture
\ps n
\ge jus de fruit
\dt 08/Nov/2021

\lx wepòò
\dialx PA BO
\is richesses
\ps n
\ge ceinture de femme
\dn faite de racine de bourao (lit. |lx{we-pòò}), elle sert de monnaie à bas prix et est offerte roulée (selon Charles Pebu-Polae) ; selon Dubois (ms.), cette ceinture faisait plusieurs fois le tour du bassin
\cf pobil, thabil
\ce ceinture de femme (monnaie)
\dt 10/Jan/2022

\lx we-phölöö
\ph 'we-'pʰωlo:  'we-'vwωlo:
\dialx GOs
\va we-vwölo
\dialx PA
\is eau
\ps n
\ge eau boueuse ; marais
\cf phölöö
\ce sale ; trouble
\dt 22/Feb/2025

\lx we-phwa
\dialx GOs
\va we-vwa
\dialx GO(s) BO PA
\is fonctions_naturelles
\ps n
\ge salive ; bave
\dn (lit. eau-bouche)
\xv we-phwaa-je ; we-vwaa-je
\dialx GOs
\xn sa salive
\xv we-wa-n
\dialx PA
\xn sa salive
\dt 09/Feb/2025

\lx werö
\dialx GOs
\is son
\ps v
\ge faire du bruit
\xv ko (= kavwö) çö werö !
\xn arrête de faire du bruit !
\dt 22/Mar/2023

\lx wè-ru-m(h)õ
\dialx GOs
\va wòruumò
\dialx BO [BM]
\is caractéristiques_personnes
\ps n
\ge ambidextre
\dn (lit. deux gauche)
\dt 09/Feb/2025

\lx we-tiivwo
\dialx GOs
\va we-tiin
\dialx PA
\is matière
\ps n
\ge encre
\dn (lit. eau écrire)
\dt 08/Feb/2025

\lx we-thi
\dialx GOs BO PA
\is corps
\ps n
\ge lait maternel 
\dn (lit. liquide du sein)
\dt 09/Feb/2025

\lx we-tho
\dialx GOs BO
\is eau
\ps n
\ge eau vive
\dn (lit. eau qui coule)
\dt 09/Feb/2025

\lx we-trabwa
\dialx GOs
\va we-tabwa
\dialx BO
\is eau
\ps n
\ge eau morte ; eau stagnante
\dn (lit. eau assise)
\dt 09/Feb/2025

\lx we-tru
\dialx GOs
\is action_eau_liquide_fumée
\ps n
\ge eau qui monte après une forte pluie
\dt 20/Feb/2019

\lx wè-tru
\dialx GOs
\va wè-ru
\dialx PA BO
\is classificateur_numérique
\ps CLF.NUM (général et des objets longs)
\ge deux (objets longs)
\xv truuçi bwa wè-tru balaa-cee
\dialx GOs
\xn douze morceaux de bois
\xv wè-ru tèèn
\dialx PA BO
\xn deux jours
\dt 24/Jan/2022

\lx we-thrõbo
\dialx GOs
\va we-thôbo
\dialx BO
\is eau
\ps n
\ge chute d'eau ; cascade
\ge ouverture d'eau ; prise d'eau
\dn ouverture d'eau des talus externes de tarodière ; elle peut être fermée par de la terre, des pierres et permet de faire passer l'eau de l'étage supérieur à l'étage inférieur de la tarodière (selon Dubois)
\dt 22/Oct/2021

\lx we-vhaa
\dialx GO PA BO
\va we-fha
\dialx GA
\is discours
\ps n
\ge substance de la parole
\dt 12/Jan/2022

\lx wewele
\dialx GOs
\is action_corps
\ps v
\ge secouer (arbre)
\dt 21/Feb/2018

\lx wè-xè
\dialx GOs PA BO
\is classificateur_numérique
\ps CLF.NUM (général et des objets longs)
\ge un (objet long)
\xv wè-xè; wè-tru; etc.
\dialx GOs
\xn un, deux, etc.
\xv wè-xe, wè-ru, wè-kòn, wè-p(h)a, wè-nim, wè-ni(m)-ma-xe, etc.
\dialx PA
\xn un, deux, trois, quatre, cinq, six, etc.
\xv ni wè-xè mhwããnu
\dialx GOs
\xn dans un mois
\xv nu nõõli wõ xa wè-xè
\dialx GOs
\xn j'ai vu un bateau
\xv wè-niza wõ xa çö nõõli ?  – Ca wè-xè nõ wõ ; ca wè-tru wõ xa nu nõõli
\dialx GOs
\xn combien de bateaux as-tu vus ? – Juste un seul bateau ; j'ai vu deux bateaux
\dt 22/Feb/2025

\lx wè-xè hii-je
\dialx GOs
\is caractéristiques_corps
\ps v.stat.
\ge manchot ; qui n'a qu'un seul bras
\dt 26/Mar/2023

\lx wè-xè kòò-je
\dialx GOs
\is caractéristiques_corps
\ps v.stat.
\ge qui n'a qu'une seule jambe
\dt 26/Mar/2023

\lx weya
\dialx BO
\is action_corps
\ps v
\ge fouiller (dans les affaires des autres) ; se mêler (de ce qui ne vous regarde pas)
\dn selon [Corne]
\se a-weya
\sge indiscret
\nt mot non vérifié
\dt 09/Feb/2025

\lx we-yaai
\dialx GOs
\is matière
\ps n
\ge pétrole (de lampe)
\dt 08/Feb/2025

\lx we-za
\dialx GOs
\va we-ya
\dialx BO
\is eau_mer
\ps n
\ge mer ; eau salée
\cf kaze
\dt 08/Oct/2021

\lx weze
\dialx GOs PA
\va wizi
\dialx GOs PA
\va wei
\dialx BO
\is action_corps
\ps v
\ge serrer
\dt 20/Aug/2021

\lx weze nõ
\ph weðe ɳɔ̃
\dialx GOs
\va wizi nõ
\dialx GOs
\va wei nõ
\dialx BO
\is action_corps
\ps v
\ge étrangler (serrer le cou) ; (se) pendre
\dt 20/Aug/2021

\lx we-zume
\dialx GOs
\is fonctions_naturelles
\ps n
\ge crachat
\xv we-zumee-je
\dialx GOs
\xn son crachat
\cf we-phwa ; we-vwa
\ce salive
\dt 01/Jan/2022

\lx we-zhido
\dialx GA
\va we-zedo
\dialx GO(s) BO
\is instrument
\ps n
\ge miroir
\dt 27/Jan/2019

\lx wi
\dialx GOs PA BO
\va ui, we
\dialx BO
\is grammaire_conjonction
\ps CNJ
\ge que ; pour
\xv wi po-ra ?
\xn pourquoi faire ? (lit. pour faire quoi ?)
\xv çö a wi ?
\dialx GOs
\xn tu viens pourquoi faire ?
\xv i nami wi a-du Kuma
\xn il pense qu'il va descendre à Koumac
\dt 10/Nov/2021

\lx wîî
\dialx GOs PA BO
\va wîî-n
\dialx BO
\va wêê-n
\dialx BO [Corne]
\is caractéristiques_personnes
\ps n ; v.stat.
\ge puissance ; force
\ge fort
\xv e wîî
\dialx GOs
\xn il est fort
\xv ge je ni wîî mããni
\dialx GOs
\xn il est dans un sommeil profond
\xv wîî-je
\dialx GOs
\xn sa force
\xv wîî-n
\dialx PA
\xn sa force
\se wîî mãni hubu
\sge la force physique et le charisme/la force spirituelle
\dt 09/Jan/2022

\lx wîî-kale
\dialx BO
\is eau_marée
\ps n
\ge marée étale
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx wili
\hm 1
\dialx PA BO
\va whizi
\dialx GO
\is nourriture
\ps v.t.
\ge manger (la canne à sucre)
\cf whal èm
\dialx PA
\ce manger de la canne à sucre
\cf wha ê
\dialx GO
\ce manger de la canne à sucre
\dt 11/Feb/2025

\lx wili
\hm 2
\dialx PA BO
\va huli
\dialx BO PA
\va wele
\dialx BO
\is déplacement
\ps v
\ge suivre (se) ; suivre qqn.
\xv li ra pe-wili du bwa haavwu
\dialx PA
\xn ils se suivent pour descendre au jardin
\se pe-huli
\dialx PA BO
\sge marcher en file indienne
\dt 29/Mar/2022

\lx wizi
\dialx GOs
\va weze
\dialx PA
\is action_corps
\ps v
\ge étrangler (avec une corde ou une liane)
\xv e wizi nõõ-je
\dialx GOs
\xn il s'est étranglé ; il l'a étranglé
\xv nu wizi nõõ-je
\dialx GOs
\xn je l'ai étranglé
\dt 22/Feb/2025

\lx wõ
\ph wɔ̃
\dialx GOs
\va wony
\ph wɔ̃ɲ
\dialx PA WEM BO
\sn 1
\is navigation
\is déplacement_moyen
\ps n
\ge bateau ; embarcation
\xv wõ-cee wõjo-nu
\dialx GOs
\xn mon bateau en bois
\xv wõjo-nu ca wõ-cee
\dialx GOs
\xn mon bateau est en bois
\xv wõjo-nu
\dialx GOs
\xn mon bateau
\sn 2
\is maison_objet
\ps n
\ge berceau en fibre de cocotier |dialx{WEM PA BO}
\sn 3
\is jeu
\ps n
\ge figure du jeu de ficelle ("le bateau") |dialx{BO}
\et *waŋka(ŋ)
\eg bateau
\el POc
\dt 25/Mar/2022

\lx wò phii-me
\dialx GOs
\is action_tête
\ps v
\ge écarquiller les yeux
\xv e wò phii-me
\xn elle écarquille les yeux
\dt 26/Jan/2019

\lx wo-cee
\dialx GOs
\is cultures_outil
\ps n
\ge épieu
\dt 08/Nov/2021

\lx wõ-cee
\dialx GOs
\is navigation
\ps n
\ge pirogue à balancier
\dt 08/Nov/2021

\lx wõdò
\dialx GOs
\va wèdò, vòdòòn
\dialx BO
\sn 1
\is discours
\ps v ; n
\ge discuter ; palabrer
\ge juger ; jugement
\ge discussions
\xv e wõdoo-ni zòò-ni
\xn il a porté un bon jugement
\se vhaa mãni wõdò
\sge paroles et jugements
\se a-wõdò
\sge celui qui a un bon jugement
\ng v.t. |lx{wõdoo-ni} |dialx{GO}, |lx{wèdoo-ni} |dialx{BO}
\sn 2
\is coutumes
\ps n
\ge actes coutumiers ; us et coutumes ; usages ; manières ; mœurs
\xv wõdò-(w)a êgu
\dialx GOs
\xn les manières des gens
\dt 04/Jul/2024

\lx wôdrî
\dialx GOs
\va wôding
\dialx PA BO
\va wèding
\dialx BO
\is topographie
\ps n
\ge col de montagne
\ge creux ; dépression (terrain)
\xv ni wôdrî Pwagen
\xn au col de Pwagen
\dt 16/Aug/2021

\lx wogama
\dialx BO
\is temps_saison
\ps n
\ge mois de labour et de plants des champs d'ignames
\dn période d'août à septembre (selon Dubois)
\cf pweralo, pwebae
\nt non vérifié
\dt 08/Feb/2025

\lx wõ-gò
\dialx GOs
\is navigation
\ps n
\ge radeau en bambou
\dt 25/Aug/2021

\lx wône
\ph wõɳe
\dialx GOs
\va wêne
\dialx PA
\sn 1
\is action
\ps v
\ge remplacer ; changer (vêtements)
\ge faire de la monnaie
\xv e wône-nu
\dialx GOs
\xn il me remplace
\xv e wône ã a-chomu
\dialx GOs
\xn il a remplacé cet enseignant
\se a-wône-vwo
\dialx GOs
\sge un remplaçant
\se wône hôbwò
\sge changer de vêtement
\se pe-wône
\sge échanger
\sn 2
\is déplacement
\ps v
\ge déplacer ; changer de place
\xv e wône dè
\dialx GOs
\xn il change de route
\xv wône choova
\dialx GOs
\xn changer un cheval de place
\se wône ku
\sge changer de place
\se wône kun-(n)a
\dialx PA
\sge changer de lieu
\dt 01/Jan/2022

\lx wöö
\dialx GOs
\is chasse
\ps n
\ge lacet
\ge piège
\se pwe-wöö
\sge nœud coulant
\dt 22/Mar/2023

\lx wööe
\dialx GOs
\is action
\ps n
\ge serrer
\se pwe-wööe
\sge lasso
\dt 22/Mar/2023

\lx wòòzi
\dialx GOs
\ph wɔ:ði
\va wooli
\dialx PA BO
\va woji
\dialx BO
\is arbre
\ps n
\ge acajou
\sc Semecarpus atra
\scf Anacardiacées
\dt 27/Aug/2021

\lx wõ-pwaala
\ph wõpwa:la
\dialx GOs
\va wô-waala
\dialx GO(s)
\is navigation
\ps n
\ge bateau à voile
\dt 02/Jan/2022

\lx wõ-phu
\dialx GOs
\va avyô
\is déplacement_moyen
\ps n
\ge avion
\dn (lit. bateau volant)
\se mhenõõ phe wõ-phu
\sge aérodrome
\dt 10/Feb/2025

\lx wòvwa
\dialx GOs
\ph wɔβa
\va wòpa
\dialx arch.
\va wovha
\dialx PA
\va woza
\dialx WEM
\is guerre
\ps v ; n
\ge bagarre ; bagarrer (se) ; affronter (s')
\xv li pe-wòvwa
\xn ils se battent
\dt 24/Dec/2021

\lx woxa
\dialx GOs WEM BO
\va voxa
\dialx BO
\is interaction
\ps v
\ge nier
\ng v.t. |lx{woxe}
\gt cacher ; ne pas dire ; tromper
\dt 22/Feb/2025

\lx wòzò
\dialx GOs
\va wòlò
\dialx PA BO
\va wojò
\dialx BO
\sn 1
\is cultures_outil
\ps n
\ge épieu de culture ; bâton à fouir ; "barre à mine"
\xv wòlò-ny wo-cee
\dialx PA
\xn mon épieu en bois (par opposition à la barre à mine)
\sn 2
\is coutumes_objet
\ps n
\ge perche sacrée du champ d'igname
\dn perche destinée à favoriser la récolte et protéger les plantations
\dt 22/Feb/2025

\lx wòzõõ
\dialx GOs
\va wolõ
\dialx PA
\is santé
\ps n
\ge furoncle
\dt 25/Jan/2019

\lx wha
\dialx GOs PA BO
\is plantes
\hm 1
\ps n
\ge figuier sauvage
\sc Ficus habrophylla
\scf Moracées
\se pò-wha
\sge fruit de figuier
\se ci-wha
\sge l'écorce du figuier
\dt 27/Aug/2021

\lx wha
\hm 2
\dialx GOs
\va hua
\dialx PA BO
\is parenté
\ps n
\ge grand-père (maternel ou paternel)
\dn terme de désignation et d'appellation
\ge frère de grand-père
\ge cousin de grand-père
\ge vieux
\xv wha i nu
\dialx GO
\xn mon grand-père
\xv wha i we
\dialx GO
\xn votre grand-père
\xv hua i nu
\dialx PA
\xn mon grand-père
\se wawa
\sge grand-papa (appellation)
\se whamã
\sge vieil homme ; ancêtre
\dt 22/Feb/2025

\lx wha
\hm 3
\dialx GOs
\va whal
\dialx PA
\is nourriture
\ps v ; n
\ge manger (la canne à sucre)
\ge part de canne à sucre
\xv nu wha ê
\dialx GOs
\xn je mange de la canne à sucre
\xv i whal èm
\dialx PA
\xn il mange de la canne à sucre
\ng classificateur possessif: |lx{whala-n} |dialx{PA}, |lx{whaza-je} |dialx{GOs}
\cf whizi ; w(h)ili
\ce manger de la canne à sucre
\dt 12/Jan/2022

\lx wha ê
\dialx GOs
\va whal èm
\dialx PA
\is nourriture
\ps v
\ge manger de la canne à sucre
\xv nu wha ê
\xn je mange de la canne à sucre
\cf whizi, w(h)ili
\ce manger (canne à sucre)
\dt 10/Jan/2022

\lx whaa
\hm 1
\dialx GOs BO
\is caractéristiques_objets
\ps v
\ge grand ; gros
\ge grandir ; croître ; pousser (en long)
\xv e whaa
\xn il grossit
\an pòńõ
\dialx GOs
\at petit
\dt 22/Mar/2023

\lx whaa
\hm 2
\ph wʰa:
\dialx GOs
\va waa
\dialx WEM
\va waang
\ph wa:ŋ
\dialx PA BO
\va waak
\dialx PA
\is temps_découpage
\ps n ; v
\ge matin ; faire jour
\xv e gaa whaa
\dialx GOs
\xn c'est encore très tôt le matin
\xv e mhã whaa gò
\dialx GOs
\xn c'est encore trop tôt
\xv e thau xa gaa waa gò
\dialx WEM
\xn il est arrivé quand il était encore très tôt le matin
\se gaa whaa
\sge encore très tôt le matin
\cf gòòn-a, kaça-huvwo, thrõbwo
\ce midi/zenith ; après-midi ; soir
\dt 22/Feb/2025

\lx whaaça
\ph wʰa:dʒa
\dialx GOs
\va waaya
\dialx PA
\va waiza
\dialx BO
\va whayap
\dialx BO
\is guerre
\ps v ; n
\ge guerre ; lutte
\ge combattre ; lutter (pour avoir qqch.)
\ge assaillir (pour obtenir qqch.) |dialx{GOs}
\se pe-whaaça
\dialx GO
\sge combattre (se); lutter l'un contre l'autre
\se pe-waaya
\dialx PA
\sge s'affronter
\xv bî pe-whaaça mã ã ãbaa-nu
\dialx GO
\xn mon frère et moi nous sommes battus
\dt 17/Feb/2025

\lx whaadrangi
\dialx GOs
\va wadaga
\dialx BO
\is guerre
\ps n
\ge ennemi
\dt 26/Jan/2019

\lx whaa-gò
\dialx GOs
\is temps_découpage
\ps LOC
\ge aube ; matin de bonne heure
\xv e phe-mõgu ne whaa-gò
\xn elle commence le travail tôt le matin
\dt 22/Mar/2023

\lx whai
\hm 1
\dialx GOs
\is poisson
\ps n
\ge mulet (de mer, juvénile)
\cf naxo
\ce mulet noir (de mer)
\cf mene
\ce mulet queue bleue (de mer)
\dt 08/Feb/2025

\lx whai
\hm 2
\dialx GOs
\va wha
\dialx PA
\is cultures
\ps v
\ge récolter (le manioc en l'arrachant)
\xv i wha manyô
\dialx PA
\xn il arrache du manioc
\xv i wha manyô u ri ?
\dialx PA
\xn qui a arraché le manioc ?
\dt 01/Jan/2022

\lx whãi
\dialx GOs
\is danse
\ps n
\ge danse
\dn effectuée pour le deuil du petit chef, dansée par les oncles maternels
\xv cia whãi
\xn danse pour le deuil du petit chef
\dt 08/Feb/2025

\lx whala-
\dialx PA WE
\va whaza
\dialx GO(s)
\is classificateur_possessif_nourriture
\ps CLF.POSS
\ge part de canne à sucre
\xv ai xa whala-m ?
\xn veux-tu une part de canne à sucre pour toi ?
\xv wili whala-m !
\xn mange ta canne à sucre !
\cf w(h)ili, w(h)izi
\ce manger (la canne à sucre)
\dt 31/Jan/2022

\lx whamã
\dialx GOs PA BO
\va huamã
\dialx arch.
\sn 1
\is étapes_vie
\ps n
\ge vieux (les) ; vieil homme ; parents
\xv whamã i nu
\dialx GOs
\xn mes parents (lit. mes vieux)
\xv whamã i ã ẽgõgò
\dialx GOs
\xn nos vieux d'avant (lit. grands-pères de nous avant)
\xv i mhã whamã
\dialx GOs
\xn c'est l'aîné (lit. le plus vieux)
\xv pòi-nu whamã
\dialx GOs
\xn mon aîné
\cf huamã
\dialx PA
\ce ancêtre
\cf thoimwã
\ce vieille femme
\sn 2
\is étapes_vie
\ps v
\ge grandir ; vieillir (animés)
\xv nu pò whamã nai çö
\dialx GOs
\xn je suis un peu plus vieille que toi
\xv e za u whamã ẽnõ-ni
\dialx GOs
\xn cet enfant a bien grandi
\an pò ẽnõ
\at plus jeune
\dt 21/Feb/2025

\lx wha-maama
\dialx WEM WE
\is parenté
\ps n
\ge arrière-arrière-grand-père
\dt 22/Mar/2023

\lx whany
\dialx PA BO
\va wany
\dialx PA BO
\is religion
\is interaction
\ps v ; n
\ge malédiction ; punition ; punir
\ge esprits des vieux du clan
\dt 15/Aug/2021

\lx wha-thraa
\dialx GOs
\va wha-rhaa
\dialx GO(s)
\is parenté
\ps n
\ge arrière-arrière-grand-père
\dt 22/Mar/2023

\lx whau
\dialx GOs
\va whãup
\dialx PA
\is caractéristiques_corps
\ps v
\ge édenté
\dt 23/Jan/2022

\lx whaya ?
\dialx GOs BO PA
\sn 1
\is grammaire_interrogatif
\ps v.INT
\ge comment (être, faire) ? ; de quelle sorte ?
\xv e whaya phãgo(o) ?
\dialx GOs
\xn quelle est sa forme ?
\xv êmwê xa whaya ?
\dialx GOs
\xn c'est un homme comment (physiquement) ?
\xv pwaji xa whaya ? – Pwaji xa baa
\dialx GOs
\xn c'est un crabe comment ? – Un crabe noir
\xv hèlè xa whaya ? – Hèlè xa ca ; hèlè xa khawali
\dialx GOs
\xn c'est un couteau comment ? – Un couteau affûté ; un grand couteau
\xv e whaya mwê-je êmwê-è ?
\dialx GOs
\xn quelle sorte d'homme est-ce ? (lit. comment sont ses manières ?)
\xv lha nee whaya-le kîbi ?
\dialx GOs
\xn comment ont-ils fait le four ?
\xv whaya me teyimwi pwaji ?
\dialx BO
\xn comment attrape-t-on des crabes ?
\xv i hivine (kôbwe) yu nee whaya-le
\dialx BO
\xn il se sait pas comment tu as fait
\xv i mha whaya ?
\xn comment est-il malade ? (Dubois)
\xv ka i whaya-le ?
\xn comment l'a-t-il fait ? (Dubois)
\ng v.t. |lx{whayale}
\sn 2
\is grammaire_interrogatif
\ps INT
\ge combien?
\xv i hova na whaya hinõ-al ?
\dialx PA
\xn à quelle heure rentre-t-il ?
\xv whaya kau-m ?
\dialx PA
\xn quel âge as-tu ? (lit. combien d'années)
\dt 22/Feb/2025

\lx whayu
\dialx GOs PA
\va wayu
\dialx BO
\is action_tête
\ps v
\ge siffler avec les lèvres
\dt 26/Jan/2019

\lx whaza
\dialx GOs
\va whala
\dialx PA WE
\is classificateur_possessif_nourriture
\ps CLF.POSS
\ge part de canne à sucre
\xv whaza-çö ê mwã lhãã
\dialx GOs
\xn ceci est ta part de canne à sucre
\xv whaza-nu ê
\dialx GOs
\xn ma canne à sucre (à manger)
\cf w(h)izi
\ce manger de la canne à sucre
\dt 22/Feb/2025

\lx whili
\ph wʰɨlɨ
\dialx GOs WEM BO PA
\is déplacement
\is action_corps
\ps v
\ge conduire
\ge amener ; emmener
\ge conduire (voiture) |dialx{PA}
\ge prendre par la main (enfant)
\ge chercher (épouse)
\ge mener (travail)
\xv nu whili dè
\dialx GOs
\xn je montre le chemin
\xv nu whili-çö-da-mi
\dialx GOs
\xn je t'ai amené ici en haut
\xv pwawa ne çö whili-mi lã-nã pòi-m ?
\dialx WEM
\xn peux-tu amener tes enfants ici ?
\xv i whili nye nyamã
\dialx PA
\xn il a mené ce travail
\dt 05/Jan/2022

\lx whili thoomwã
\dialx GOs
\va huli thoomwã
\dialx WE
\is coutumes
\ps n
\ge coutume (cérémonie) de mariage
\dn (lit. chercher la femme)
\dt 05/Jan/2022

\lx whizi
\dialx GOs
\va wili, whili
\dialx PA BO WE
\is nourriture
\ps v
\ge manger (canne à sucre)
\ge mâcher de la canne à sucre
\xv a-vwö nu whizi ê
\dialx GOs
\xn j'ai envie de manger de la canne à sucre
\xv whili whala-m
\dialx PA
\xn mange ta canne à sucre
\xv i wili whala-n èm
\dialx PA WE
\xn il mange la canne à sucre
\xv i wili-õgine whala-n
\dialx PA
\xn il a fini de manger sa canne à sucre
\xv i w(h)ili èm
\dialx PA WE
\xn il mange la canne à sucre
\cf hovwo
\ce manger (général)
\cf cèni
\dialx GOs
\ce manger (féculents)
\cf huu, huli
\ce manger (nourriture carnée)
\cf biije
\ce mâcher des écorces ou du magnania
\cf kûûńi
\ce manger (fruits)
\cf wha ê
\dialx GOs
\ce manger de la canne à sucre
\dt 10/Jan/2022

\lx whòi
\dialx GOs PA
\is action_corps
\ps v
\ge fouiller (dans un sac, une poche)
\xv whòi-du ni ke !
\xn fouille dans le sac !
\dt 20/Aug/2021

\lx whun
\dialx BO
\is caractéristiques_personnes
\ps v ; n
\ge silencieux ; silence
\nt selon Corne
\dt 27/Mar/2022

\lx xa
\hm 1
\va ga
\dialx PA
\dialx GOs PA WE BO
\is grammaire_article
\ps INDEF SPECIFIQUE
\ge un(e) certaine
\xv e trilòò kêê-je xa dili
\dialx GOs
\xn il demande de la terre à son père
\xv e na i pòi-je xa mhavwa lai
\dialx GOs
\xn elle donne à son enfant un peu de riz
\xv kixa mwã xa kui
\dialx GOs
\xn il n'y a plus d'igname
\xv kia xa na çö nõõli
\dialx GOs
\xn tu ne regardes rien
\xv no-me çö tròòli xa çö thomã-nu
\dialx GOs
\xn si tu en trouves, tu m'appelles
\xv ai-je vwo nee xa mõ-je
\dialx GOs
\xn il voulait se construire une maison
\xv a(i)-vwö me tròòli xa mhenõ, me khila xa kuru mãni kui
\dialx GOs
\xn nous voulons retrouver des lieux, chercher des taros et des ignames
\xv ai-me vwö ge le xa hegi
\dialx GOs
\xn nous voudrions qu'il y ait des monnaies
\xv kavwu nu vha mwã na ni xa kun mwã
\dialx PA
\xn je ne vais pas parler de n'importe quel clan
\xv kavwö nu tòòli mwã xa cee
\dialx PA
\xn je n'ai pas trouvé de bois du tout
\xv nu phe-mi hovwo vwo ãbaa xa lhã-ã çö pavwange
\dialx GOs
\xn j'ai apporté de la nourriture en complément de ce que tu as préparé
\xv e trõne khõbwe me ge-le xa thoomwã xa Mwani-mi
\dialx GOs
\xn il a entendu dire qu'il y avait une femme qui s'appelait Mwani-mi
\xv kavwö çö nõõli xa thoomwã na khõbwe Mwani-mii ?
\dialx GOs
\xn n'as tu pas vu une certaine jeune-fille qu'on appelle Mwani-mi ?
\xv kavwö çö trõne nye ẽnõ-zoomwã ge je ni xa bwa drau ?
\dialx GOs
\xn n'as tu pas entendu (parler) d'une jeune-fille qui serait quelque part sur une île ?
\xv na khõbwe çö trõne khõbwe ge-le xa thoomwã xa Mwani-mii
\dialx GOs
\xn si jamais tu entends dire qu'il y a une femme du nom de Mwani-mi
\xv me waaçu vwö me khila xa whaya me tròòli xo mwani
\dialx GOs
\xn nous nous efforçons de chercher une façon de gagner de l'argent
\xv a khila xa hèlè na ca
\dialx GOs
\xn va chercher un couteau qui coupe
\xv haivwo xa yaala-la
\dialx PA
\xn ils ont beaucoup de noms
\xv ni xa tèèn
\dialx PA BO
\xn un certain/beau jour
\xv kavwu nu vha mwã na ni xa kun mwã
\dialx PA
\xn je ne vais pas parler d'un clan/endroit quelconque
\xv koèn xa ãbaa wõny
\dialx PA
\xn certains bateaux ont disparu
\xv i khila xa po
\dialx PA
\xn il cherche quelque chose
\xv nu nõõlî xa po
\dialx PA
\xn je vois quelque chose
\xv i tõne xa êgu
\dialx PA
\xn il entend quelqu'un
\xv ti xa êgu na i cabi mwa ?
\dialx PA
\xn qui a frappé à la maison ?
\xv ti xa na çö nõõli?
\dialx GOs
\xn qui regardes-tu?
\xv da xa na çö nõõli?
\dialx GOs
\xn que regardes-tu?
\dt 20/May/2024

\lx xa
\hm 2
\dialx GOs BO PA
\va ga
\dialx PA
\sn 1
\is grammaire_conjonction
\ps COORD
\ge et ; aussi |dialx{PA}
\xv puyòl xo je, xa u minòng doo
\dialx PA
\xn il cuisine et la marmite est prête
\xv i têên xa (i) wal
\dialx PA
\xn il court et (il) chante
\sn 2
\is grammaire_conjonction
\ps COORD
\ge mais |dialx{GOs}
\xv e kòladuu phãgoo-je xa e pu wîî-je
\dialx GOs
\xn il est maigre, mais il a de la force
\xv e khawali-je xa e kòladuu-je
\dialx GOs
\xn il est grand, mais il est maigre
\dt 22/Feb/2025

\lx xa
\hm 3
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge quand (passé)
\xv e uça gò xa jaxa we-tru ka
\dialx GOs
\xn elle est arrivée il y a deux ans
\se dròrò xa waa
\dialx GOs
\sge hier matin
\cf mõnõ na waa
\dialx GOs
\ce demain matin
\dt 03/Feb/2025

\lx xa
\hm 4
\dialx GOs PA BO
\va ka
\dialx arch.
\is grammaire_conjonction
\ps CNJ.REL
\ge que ; qui
\xv êgu xa lha ã-mi mõgu bwa vhaa i ã
\dialx GOs
\xn des gens qui sont venus travailler sur notre langue
\xv êgu ka e a-waçuçu
\dialx GOs
\xn une personne persévérante
\xv nu nõõli wõ xa we-tru
\dialx GOs
\xn j'ai vu deux bateaux
\xv hõbwò xa whaya ? – Hõbwò xa khawali
\dialx GOs
\xn des vêtements (qui sont) comment ? – Des vêtements (qui sont) longs
\xv êmwê xa ti ?
\dialx GOs
\xn quel homme ?
\xv mõgu xa phwee-mwãgi
\dialx GOs
\xn un travail qui ne sert à rien
\xv zixôô-nu ẽnõ êmwê xa a-xe xa yaza-je Jae
\dialx GO
\xn mon conte d'un jeune garçon qui s'appelle Jae
\xv mõ trõne-da gune chiò xa a-du
\dialx GO
\xn nous entendons en haut le bruit du seau qui descend
\xv hèlè xa i uvwi xo Jan
\dialx GOs
\xn le couteau que Jean a acheté
\xv i êgu xa i aa-vhaa
\dialx PA
\xn c'est un bavard
\xv êgu xa lhi a-du-mi nyamã
\dialx PA
\xn personnes qui viennent travailler
\xv ka i no-da-ò je thoomwã ka e tabwa le
\dialx PA
\xn et il voit là une femme qui y est assise
\xv mèni xa whaya ?
\xn un oiseau comment ? de quelle sorte ?
\xv loto xa whaya ?
\xn une voiture de quelle sorte ?
\xv pwaji xa whaya ? – Pwaji xa baa
\dialx GOs
\xn un crabe comment ? – Un crabe noir
\dt 22/Feb/2025

\lx xa
\hm 5
\dialx GOs
\va xaa
\dialx PA
\is grammaire_aspect
\ps ITER ; REV
\ge encore ; de nouveau ; déjà
\xv kaavwö nu xa kuuni pwaamwa-e
\xn je n'ai pas encore fini ce champ
\xv e xa ã-da
\xn il est remonté
\xv e xa thomã-çö iò
\xn il t'a rappelé tout à l'heure
\xv e xa mããni
\xn il s'est rendormi
\xv a we xa a kaze
\xn repartez à la pêche !
\xv e za xa a-da
\xn il est déjà/vraiment remonté
\dt 26/Aug/2023

\lx xaatra
\dialx GOs
\is poisson
\ps n
\ge "aiguillette" ; demi-bec à taches noires
\sc Hemirhamphus far
\scf Hemiramphidés
\dt 22/Feb/2025

\lx xaatròe
\ph 'ɣa:ɽɔe
\dialx GOs
\va xaaròe
\dialx PA
\is poisson
\ps n
\ge "napoléon" (poisson du chef)
\sc Cheilinus undulatus
\scf Labridés
\dt 27/Aug/2021

\lx -xè
\dialx GO
\va -xe
\dialx PA BO
\is grammaire_numéral
\ps NUM
\ge un
\dt 18/Aug/2021

\lx xhii
\dialx GOs
\va khi
\dialx GO(s)
\va khiny
\dialx PA BO WEM WE
\is oiseau
\ps n
\ge hirondelle busière ; langrayen à ventre blanc
\sc Artamus leucorhyncus melanoleucus
\scf Artamidés
\gb White-breasted Woodswallow
\dt 11/Oct/2021

\lx xo
\hm 1
\dialx GOs PA
\va ko, go
\dialx GO(s)
\va (vw)o, ku, (x)u
\dialx GO PA BO
\va (h)u
\dialx BO
\sn 1
\is grammaire_agent
\ps AGT ; sujet
\ge sujet ; agent des verbes actifs
\xv i tabwa xo thoomwã xo puyol
\dialx PA
\xn la femme est assise et fait la cuisine
\xv e gi xo khiny
\dialx PA
\xn l'hirondelle pleure
\xv i phweween xo Kaavo
\dialx PA
\xn Kaavo se retourne
\xv u kõbwe wo Thomaxim ya kòlò Thonòòl
\dialx PA
\xn Thomaxim dit à Thonòòl
\xv la taluang êê-ny xu choval
\dialx PA
\xn les chevaux ont ravagé mes plantations
\xv e õgine mõgu i ã xo ẽnõ-ã
\dialx GOs
\xn cet enfant a fini notre travail
\xv e thuvwu-õgine mõgu i je xo ẽnõ-ã
\dialx GOs
\xn cet enfant à fini tout seul son travail
\xv e khõbwe xo/ko kêê-nu khõbwe e zo na çö cuxi na ni mõlòò-çö
\dialx GOs
\xn mon père dit qu'il faut que tu sois courageux dans ta vie
\xv e alöe ciia xo zine
\dialx GOs
\xn le rat regarde le poulpe
\xv e pa-tha na ni mèni xo Teã Paak
\dialx GOs
\xn Teã Paak a raté l'oiseau
\xv e kibao mèni xo Teã Paak
\dialx GOs
\xn Teã Paak a tué l'oiseau
\xv e pe-thumenõ bwa dè xo ti ?
\dialx GOs
\xn qui marche sur le chemin ?
\xv lhi pe-thumenõ bulu bwa dè xo Kaavo ma Hiixe
\dialx GOs
\xn Kaavo et Hiixe marchent ensemble sur le chemin
\xv e kòròò-nu xo hovwo
\dialx GOs
\xn j'ai avalé de travers ; je me suis étouffé (lit. la nourriture m'a étouffé)
\xv hèlè xa i uvwi xo Jan
\dialx GOs
\xn le couteau que Jean a acheté
\xv e za xa puxãnu-ni mwã xo je nhye pòi-je
\dialx GOs
\xn elle aime toujours sa fille
\xv a-e xo Pwayili
\dialx PA
\xn Pwayili y va (direction transverse)
\sn 2
\is grammaire_IS
\ps FOCUS du sujet
\ge marque d'emphase
\dn du pronom indépendant sujet des verbes actifs
\xv e khõbwe xo je
\dialx GOs
\xn il/elle dit
\xv nu nõõli xo inu
\dialx GOs
\xn c'est moi qui le vois (inanimé)
\xv nu nõõ-je xo inu
\dialx GOs
\xn c'est moi qui le vois
\xv mãni mwa xo lhi, cabòl mwa xa waang
\dialx PA
\xn ils dorment et se lèvent au matin
\xv buròm mwã xo je
\dialx PA
\xn elle se baigne encore ; elle est en train de se baigner
\dt 22/Feb/2025

\lx xo
\hm 2
\dialx GOs PA
\is grammaire_conjonction
\ps CNJ
\ge aussi ; et aussi
\xv inu xo
\xn moi aussi
\xv içö xo
\xn toi aussi
\xv e a ni we xo
\dialx GOs
\xn il va aussi dans l'eau
\xv kavwö nu trõńe, xo kavwö nu nõõ-je
\dialx GOs
\xn je n'en ai ni entendu parler, ni ne l'ai vu(e)
\xv e khawali-je xo kòlaadu-je
\xn il est grand et maigre
\xv e wî xo khawali-je
\xn il est fort et grand
\dt 22/Feb/2025

\lx xo
\hm 3
\dialx GOs BO PA
\va (k)o, vwo
\dialx BO WEM
\sn 1
\is grammaire_préposition
\ps PREP (instrument)
\ge avec (instrumental)
\xv thei xo hèlè
\dialx GOs
\xn couper avec un couteau
\xv i thei u/xo/vwo wamòn
\dialx PA BO
\xn il l’a abattu avec une hache
\xv bu-ɉe ne zage-ɉe ko nyamã
\dialx PA
\xn il refuser de l'aider avec ce travail
\xv e chavwoo-ni mee-je xo chavwo
\dialx GOs
\xn elle se lave le visage avec du savon
\xv hê-kee-nu xa nu taaja xo do
\dialx GOs
\xn j'ai pris cela en piquant à la sagaie
\xv e thuvwu kòòli hii-je xo du-bwò
\dialx GOs
\xn il s'est piqué la main avec l'aiguille
\xv phaa nee xo gò
\dialx GOs
\xn un radeau fait en bambou
\xv e pònu xo we
\dialx GOs
\xn c'est plein d'eau
\xv nu mhã xo khaabu
\dialx GO
\xn je meurs de froid
\xv u mhã mwã xo tuuyòng
\dialx PA
\xn il meurt de froid
\sn 2
\is grammaire_préposition
\ps PREP (bénéficiaire, destinataire)
\ge pour
\xv e zo xo nu ! axe içö, çö zoma po-za mwa ?
\dialx GOs
\xn pour moi, ça va ! mais toi, comment feras-tu donc ?
\sn 3
\is grammaire_préposition
\ps PREP (cause)
\ge à cause de
\xv e böö phwa-je xo pò-mãã
\dialx GOs
\xn sa bouche a eu une réaction cutanée à cause de la mangue
\xv co kîga o da ?
\dialx WEM
\xn tu ris de quoi ?
\sn 4
\is grammaire_préposition
\ps PREP (objet indirect)
\ge à
\xv i nõnõmi vwo Bwode
\dialx BO
\xn il pense à Bondé
\dt 20/Feb/2025

\lx xo
\hm 4
\dialx GOs PA
\va vwo
\dialx GOs
\is grammaire_conjonction
\ps CNJ
\ge que ; pour que
\xv pha-nõnõmi-je xo a-du-mi !
\dialx GOs
\xn rappelle-lui de descendre !
\xv nee xo êgu-zo ! / nee vwo êgu-zo !
\xn fais que ce soit joli !
\dt 22/Mar/2023

\lx -xo
\dialx GOs
\va -vwò
\dialx GOs BO
\is grammaire_suffixe_valence
\ps SUFF
\ge suffixe détransitivant
\dn (+ objet indéfini ou générique)
\xv xa çò kamweli me nee-xo
\dialx GOs
\xn et comment avez-vous deux fait pour le faire ?
\xv e nee-xo wãã-na dròrò
\dialx GOs
\xn il l'a fait comme cela/ainsi hier
\xv nee-xo wãã !
\dialx GOs
\xn fais-le comme cela/ainsi
\xv me waaçu vwö me khila xa whaya me tròòli xo mwani
\dialx GOs
\xn nous nous efforçons de chercher comment gagner de l'argent
\dt 22/Feb/2025

\lx ya
\dialx GOs
\va yhal
\dialx PA
\is discours
\ps n
\ge nom ; mot
\xv da ya çö tii ?
\dialx GOs
\xn quel est le mot que tu as écrit ?
\dt 23/Aug/2021

\lx ya-
\dialx GOs
\is lumière
\ps PREF.sémantique
\ge lumière (en composition)
\se ya-traabwa
\sge lampe à pétrole (lampe assise)
\cf yaai
\ce feu ; lumière
\dt 22/Feb/2025

\lx yaa
\dialx GOs
\va yaal
\dialx WEM WE PA BO
\is maison
\ps v.i
\ge couvrir (un toit)
\dn originellement avec de la paille
\ng v.t. |lx{yaaze} |dialx{GOs}, |lx{yaale} |dialx{BO}
\dt 08/Feb/2025

\lx yaa bweevwu
\dialx GOs
\is maison
\ps v
\ge couvrir de paille racines vers l'extérieur
\dt 01/Jan/2022

\lx yaa de-du
\is maison
\dialx GOs
\ps v
\ge couvrir de paille racines vers l'extérieur
\dt 25/Aug/2021

\lx yaa do mãe
\dialx GOs
\is maison
\ps v
\ge couvrir de paille racines vers l'intérieur
\cf yaa bweevwu, yaa de-du
\ce couvrir de paille racines vers l'extérieur
\dt 25/Aug/2021

\lx yaa-gòò
\dialx GOs
\va yagòòn
\dialx BO
\sn 1
\is maison
\ps n
\ge tapisserie ; couverture (de maison)
\dn en écorce de niaouli ou de palmes de cocotier tressées
\xv yaa-gòò-ra ? – Yaa-gòò mwa
\xn c'est la couverture de quoi ? –  La couverture de la maison
\sn 2
\is maison
\ps v
\ge couvrir le toit
\dn d'écorce de niaouli ou de palmes de cocotier tressées
\dt 24/Feb/2025

\lx yaa-he
\dialx GOs PA
\va yaai-he
\dialx GO PA
\is feu
\ps n
\ge feu allumé par friction
\dt 15/Sep/2021

\lx yaai
\dialx GOs BO
\va yai
\dialx PA
\is feu
\ps n
\ge feu
\xv nu nõõli ɉe yaai-m
\dialx PA
\xn j’ai alors ton feu
\se pha-yaai
\dialx GOs WEM
\sge allumer le feu
\se phai yai
\dialx PA
\sge allumer le feu
\et *api
\el POc
\dt 24/Aug/2023

\lx yaali
\dialx BO
\is caractéristiques_personnes
\ps v
\ge nerveux
\nt selon BM
\dt 26/Mar/2022

\lx yaawa
\dialx GOs PA
\is sentiments
\ps v
\ge triste ; malheureux ; nostalgique
\xv e za yaawa ui/pexa pomõ-je ?
\xn est-il nostalgique de son pays ?
\xv ôô ! e za yaawa !
\xn oui ! il en est nostalgique
\xv e za yaawa ui/pexa lie whamã ?
\xn est-il nostalgique de ses parents ?
\xv e za yaawa ui/pexa li
\xn oui ! il a la nostalgie d'eux
\dt 27/Feb/2023

\lx yaawe
\dialx BO
\is étapes_vie
\ps n
\ge nouveau-né
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx yaa-wòzò
\dialx GOs
\va yaa-wòlò
\dialx WEM WE PA BO
\va ya-wòjò
\dialx BO
\is coutumes
\is cultures_champ
\ps n
\ge champ d'igname du chef ; "massif calendrier"
\dn on défriche en premier le champ du chef
\xv nee yaa-wòzò
\dialx GO
\xn préparer le champ d'igname du chef
\xv whara ò kòòni yaa-wòlò
\dialx PA
\xn l'époque du labour du champ du chef
\cf wòzò
\ce épieu ; barre à mine
\cf yaa
\ce couvrir de paille
\cf thèl
\dialx PA
\ce débrousser
\cf kîni, khîni
\ce brûler
\cf thoè ; thöe
\ce planter
\dt 22/Feb/2025

\lx yaaze
\dialx GOs
\va yaale
\dialx WEM WE PA
\va yaali
\dialx BO
\is maison
\ps v.t.
\ge couvrir (un toit)
\dn originellement avec de la paille
\ng v.i. |lx{yaa} |dialx{GOs}, |lx{yaal} |dialx{BO}
\dt 08/Feb/2025

\lx yabo
\dialx GOs
\is plantes
\ps n
\ge plante à fruits rouges
\dn qui attire les roussettes
\dt 08/Feb/2025

\lx yabwe
\dialx GOs WEM PA BO
\is société_organisation
\ps n
\ge sujet ; serviteur
\xv yabwe i Teã-ma
\xn le serviteur du Grand Chef
\xv yabwe i je
\xn ses sujets
\dt 21/Feb/2025

\lx ya-cõê
\ph jaʒõê
\dialx GOs
\va ya-çôê
\dialx GO(s)
\is lumière
\ps n
\ge lampe tempête
\dn (qui s'accroche; lit. lumière-accrocher)
\dt 26/Mar/2022

\lx yada
\dialx GOs PA BO
\sn 1
\is richesses
\ps n
\ge affaires ; objets ; biens ; choses
\xv yadaa-nu
\dialx GOs
\xn mes biens
\xv yada-ny
\dialx PA BO
\xn mes biens
\xv yada-n jè-na
\dialx BO
\xn c'est à lui
\sn 2
\is coutumes
\ps n
\ge fêtes coutumières |dialx{BO}
\xv yada ki-kui
\dialx PA BO
\xn la fête des nouvelles ignames
\dt 23/Jan/2022

\lx yage
\dialx GOs BO
\is action_corps
\ps v
\ge ramasser (sable, feuilles, etc.) ; enlever
\xv nu yage mwã dra na mwa
\xn j'enlève les cendres de la maison
\dt 22/Dec/2021

\lx yago
\dialx GOs
\is crustacés
\ps n
\ge araignée de mer
\dt 29/Jan/2019

\lx ya-khaa
\dialx GOs PA BO
\is lumière
\ps n
\ge lampe-torche
\dn électrique (lit. lumière-appuyer)
\dt 22/Feb/2025

\lx yala
\dialx GOs PA
\is action_eau_liquide_fumée
\ps v
\ge rincer (vaisselle, un récipient)
\dt 03/Feb/2019

\lx yali
\dialx PA BO [BM]
\is action_eau_liquide_fumée
\ps v
\ge écoper
\ge vider
\ge éclabousser (avec les mains)
\xv i yali we
\dialx BO
\xn il vide/écope l'eau
\dt 20/Aug/2021

\lx yaloxa
\dialx GOs
\is nourriture
\ps v
\ge manger goulûment, trop vite
\dt 26/Jan/2019

\lx yamevwu
\dialx GOs
\va yamepu
\is société_organisation
\ps n
\ge clan
\dt 27/Jan/2019

\lx ya-mwa
\dialx GOs PA BO
\is maison
\ps v
\ge couvrir une maison
\xv ge li ya-mwa
\xn ils sont en train de couvrir la maison
\cf phu
\ce première rangée de paille au bord du toit
\cf yaaze, yaale
\ce couvrir la maison
\dt 25/Aug/2021

\lx ya-nûû
\dialx BO PA
\is lumière
\ps n
\ge torche
\dt 25/Aug/2021

\lx yaò
\hm 1
\dialx GOs
\is eau_mer_plante
\ps n
\ge corail
\dt 29/Jan/2019

\lx yaò
\hm 2
\dialx GOs
\is interaction
\ps v
\ge quémander
\xv e aa-yaò
\xn quémandeur
\dt 27/Jan/2019

\lx yaoli
\dialx PA WEM
\va yauli
\dialx BO
\va hiliçôô
\dialx GOs
\is action_corps
\is jeu
\ps n
\ge balançoire ; balancer (se)
\xv yaoli-ny
\dialx PA
\xn ma balançoire
\xv e pe-yaoli
\dialx WE
\xn il se balance
\dt 15/Aug/2021

\lx ya-pao
\dialx GOs
\is feu
\ps n
\ge allumette
\dn (lit. feu-frapper)
\dt 09/Feb/2025

\lx ya-phwaa
\dialx GOs
\is lumière
\ps n
\ge lampe coleman
\dn fait une lumière vive
\dt 26/Mar/2022

\lx yaro
\dialx BO PA
\va zaro
\dialx GO(s)
\is cultures_outil
\ps n
\ge pelle à fouir les ignames (en bois ou fer)
\ge bêche
\dt 22/Aug/2021

\lx yatre
\dialx GOs
\va yare, yaare
\dialx GO BO
\sn 1
\is action_corps
\ps v
\ge extraire
\ge ôter
\sn 2
\is action
\ps v
\ge sortir (d'un sac, etc.)
\sn 3
\is préparation_aliments
\ps v
\ge sortir (d'une marmite) ; servir
\xv i yare lavian na ni doo
\dialx BO
\xn il sort la viande de la marmite
\cf ii
\ce sortir/servir les aliments (d'une marmite)
\dt 22/Feb/2025

\lx yaweeni
\dialx GOs
\is action_corps
\ps v
\ge étaler (sable)
\dt 23/Jan/2018

\lx yawi
\dialx GOs PA BO
\va yawe
\dialx GO(s)
\va yau
\dialx BO
\is action_corps
\ps v
\ge gratter (se) ; gratter ; griffer
\xv e yawi duu-nu
\dialx GOs
\xn elle me gratte le dos
\xv nu yau i nu
\dialx BO
\xn je me gratte
\dt 24/Mar/2019

\lx yaxi
\dialx BO
\is interaction
\ps v
\ge saluer
\xv ma pe-yaxi
\dialx BO
\xn nous nous saluons
\nt selon BM
\dt 27/Mar/2022

\lx yaza
\ph jaða
\dialx GOs
\va yaaza
\dialx GOs
\va yaala-n, yala-n
\ph ja:la
\dialx PA WEM BO
\va yhaala-n, yara-n
\dialx PA
\is société
\ps n
\ge nom
\xv yaza-je
\dialx GOs
\xn son nom
\xv yhala-n
\dialx PA
\xn son nom
\xv da yhala-n ? ; da yaala-n ?
\dialx PA BO
\xn comment cela se dit-il ? ; comment cela s'appelle-t-il ? ; quel est son nom ?
\xv da yaza-çö ? – Yaza-nu Pwayili
\dialx GO
\xn quel est ton nom ? – Je m'appelle Pwayili
\xv yaaza-nu ce/je Pwayili
\dialx GO
\xn mon nom c'est Pwayili
\xv da yaala-cu ?
\dialx WE
\xn quel est ton nom ?
\se na yhala-n
\sge donner un nom
\et *qacan, *asan
\el POc
\dt 22/Feb/2025

\lx yaza da ?
\dialx GOs
\va yhaala da ?
\dialx PA
\va yaala da ?
\dialx PA
\is grammaire_interrogatif
\ps INT
\ge quel est le sens de ? ; quel est le nom de ?
\xv yaza da "chö" ?
\xn que signifie "chö" ?
\dt 16/Feb/2025

\lx yaze
\dialx GOs
\is action_eau_liquide_fumée
\ps v
\ge asperger ; arroser (avec la main)
\ge projeter
\dn de l'eau, de la boue avec les mains
\cf tãã, pa-tãã
\ce gicler ; faire gicler
\dt 22/Feb/2025

\lx yazoo
\ph yaðo:
\dialx GOs
\va yaloo, yalo
\dialx PA BO
\is action_corps
\ps v
\ge frotter
\ge limer
\ge polir
\ge affûter ; affûté
\ge aiguiser
\ge tranchant
\se ba-yazoo
\sge pierre à affûter
\xv e yazoo hèlè
\xn le couteau est affûté
\xv nu yazoo-ni
\xn je l'ai affûté
\ng v.t. |lx{yazoo-ni}
\gt affûter qqch.
\cf caa
\ce coupant ; aigu
\et *asa(q), *i-asa(q)
\eg râpe(r)
\el POc
\ea Blust
\dt 22/Feb/2025

\lx ye
\dialx PA
\is grammaire_IS
\ps THEM
\ge thématisation
\dt 29/Jan/2019

\lx -ye
\dialx PA BO
\is grammaire_pronom
\ps PRO 3° pers. SG (OBJ ou POSS)
\ge le ; la ; son ; sa ; ses
\dt 29/Jan/2019

\lx yeege
\dialx PA
\is action_corps
\ps v
\ge prendre (sable, terre)
\ge ramasser dans le creux de la main
\xv i yeege òn
\dialx PA
\xn il prend du sable (avec les mains, une pelle)
\dt 22/Feb/2025

\lx yevwa
\dialx GOs PA
\va yepwan, yebwa
\is temps
\ps n
\ge moment où ; quand
\xv kixa nêêbu ni yevwa tòò
\dialx GOs
\xn il n'y a pas de moustique à la saison chaude
\dt 05/Jun/2024

\lx yevwa bwee-ce
\dialx GOs
\is temps_saison
\ps n
\ge saison chaude
\dn de novembre à février
\dt 08/Feb/2025

\lx yevwa kou
\dialx GOs PA
\is temps_saison
\ps n
\ge saison sèche et froide
\dn de mai à août
\dt 22/Feb/2025

\lx yevwa zenô
\dialx GOs
\is temps_saison
\ps n
\ge époque de maturité des ignames
\xv zenô kui
\dialx GOs
\xn l'igname est arrivée à maturité
\dt 05/Jun/2024

\lx yo
\dialx PA BO
\va yu
\dialx GO
\is grammaire_pronom
\ps PRO 2° pers. SG (sujet, OBJ ou POSS)
\ge toi, tu
\dt 29/Jan/2019

\lx yöi
\dialx GOs
\va yöe
\dialx GOs
\is action_corps
\ps v
\ge ramasser (des objets qui traînent)
\dt 23/Mar/2023

\lx yölae
\dialx PA
\is action_corps
\ps v
\ge enlever les branches latérales d'un tronc
\dn avec un couteau, tamioc
\dt 05/Mar/2019

\lx yomaeo
\dialx BO
\is taro
\ps n
\ge taro (clone) de terrain sec
\nt selon Dubois
\dt 27/Mar/2022

\lx yòò
\hm 1
\dialx GOs
\va yhòò
\dialx GO(s)
\va yòòk
\dialx PA BO
\is arbre
\ps n
\ge bois de fer
\dn de plaine ou de montagne
\sc Casuarina equisetefolia L.
\scf Casuarinacées
\dn on rend l'esprit du défunt aux oncles maternels en attachant et en portant les monnaies traditionnelles sur une branche de bois de fer
\gb iron-wood tree
\se yòò-ma
\sge bois de fer (petit et situé en bordure de cours d'eau)
\et *(y)aRu
\eg Casuarina equisetefolia
\el POc
\dt 08/Feb/2025

\lx yòò
\hm 2
\dialx PA BO
\is richesses
\ps n
\ge monnaie
\dn d'après Charles Pebu-Polae, c'est une monnaie de chef, fine et noire, de haute valeur ; elle est offerte attachée à un rameau de bois de fer, d'où le nom ; selon Dubois, un yòò de 50 cm vaut 100 fr ; hiérarchie des valeurs : |lx{yòò > weem > yhalo}
\cf pwãmwãnu; weem; dopweza
\dt 05/Jan/2022

\lx yöö
\dialx GOs PA
\sn 1
\is action_corps_animaux
\ps v
\ge ramper (sur les arbres ou aux tuteurs)
\dn se dit de serpents, de lézards, de lianes
\sn 2
\is action_corps
\ps v
\ge faufiler (se)
\et *kawaR
\el POc
\dt 05/Mar/2019

\lx yu hayu
\dialx GOs
\is caractéristiques_animaux
\ps v
\ge en chaleur (femelle)
\dt 16/Jan/2024

\lx yua
\dialx GOs
\is grammaire_aspect
\ps ASP progressif, continuatif
\ge continuer de ; ne cesser de
\xv axe novwö kaze mwã yua a-da, yua a-da.
\xn la marée monte encore, ne cesse de monter
\xv e cö-da, yua cö-da vwo e trabwa bwa paa
\xn il monte, il monte encore et s'asseoit sur un rocher
\dt 22/Feb/2025

\lx yue
\dialx GOs PA BO
\is parenté
\ps v
\ge adopter ; élever (enfant)
\ge garder (enfant)
\xv kavwo bi ru yue-jo
\dialx PA
\xn nous n’allons pas t’adopter
\dt 21/Mar/2023

\lx yüe
\dialx BO
\is interaction
\ps v
\ge bercer (enfant)
\xv nu yü(e) ẽnõ
\dialx BO
\xn je berce l'enfant
\nt selon BM
\dt 27/Mar/2022

\lx yuu
\dialx GOs
\va yu, yuu
\dialx BO PA
\sn 1
\is habitat
\ps v
\ge demeurer
\ge rester
\ge résider
\xv i yu kòlò-n
\dialx BO
\xn il demeure chez lui
\se a-yuu
\dialx GOs BO PA
\sge habitant
\se bala-yu
\sge compagnon de résidence ; serviteur
\sn 2
\is grammaire_verbe_locatif
\ps v
\ge trouver (se)
\ge être (loc.)
\xv za pe-yuu ni phãgoo-nu
\dialx GO
\xn c'est resté dans mon corps
\dt 22/Feb/2025

\lx yuu bulu
\dialx PA
\is société
\ps v
\ge mariés (être)
\dn (lit. rester ensemble)
\xv li yuu bulu
\xn ils sont en couple
\dt 08/Feb/2025

\lx yhaamwa
\dialx GOs BO
\is fonctions_intellectuelles
\ps v.IMPERS.NEG
\ge ne pas savoir ; on ne sait pas
\xv yhaamwa ! kavwö nu hine me e trõne
\dialx GOs
\xn je n'en sais rien ! je ne sais pas s'il a entendu
\xv yhaamwa me da la lò trõne
\dialx GOs
\xn on ne sait pas ce qu'ils ont entendu
\xv yhaamwa me ezoma lhò uça
\dialx GOs
\xn on ne sait pas quand ils arriveront
\xv e za u nõ-ko xa yhaamwa mwã xa ba-ogine
\dialx GOs
\xn il (surgit) une forêt dont on ne connaît pas le bout
\xv nu a thraabu, novwu içö ca, yhaamwa içö
\dialx GOs
\xn je vais à la pêche, quant à toi, je ne sais pas (ce qu'il en est) pour toi
\xv yhaamwa içö, novwö çö a bwa da ?
\dialx GOs
\xn on ne sait pas pour toi, tu partiras comment ? (lit. sur quoi quel moyen de transport ?)
\dt 22/Feb/2025

\lx yhal
\dialx PA BO
\is discours
\ps v
\ge nommer
\se yhal paxa
\dialx BO
\sge surnom
\se na yhala-n
\dialx PA
\sge donner un nom
\dt 23/Aug/2021

\lx yhala
\dialx BO
\is nourriture
\ps v
\ge chercher de la nourriture ; aller à la pêche ; aller à la chasse [BM]
\dt 26/Jan/2019

\lx yhalo
\dialx PA
\is richesses
\ps n
\ge monnaie kanak
\dn selon Charles Pebu-Polae, cette monnaie est de valeur moindre que |lx{yòò} et |lx{weem}, mais de valeur équivalente à |lx{pwãmwãnu} ; hiérarchie des valeurs : |lx{yòò > weem > yhalo}
\dt 05/Jan/2022

\lx yha-paxa
\is discours
\dialx GOs
\ps n
\ge surnom
\dt 23/Mar/2023

\lx yhò
\dialx GOs PA BO
\is parenté
\ps n
\ge frère/sœur aîné(e)
\ge cousin(e) parallèle et aîné(e) ; cousin croisé de sexe opposé et aîné (enfants de la sœur du père)
\dn terme de désignation ou d'adresse
\dt 15/Sep/2021

\lx za
\hm 1
\dialx GOs
\ph ða
\va zha
\dialx GA
\ph θa
\va zam
\dialx PA
\va yam
\dialx BO
\is ustensile
\ps n
\ge assiette ; plat
\ge corbeille
\se bwa-xaça za
\dialx GOs
\sge le dos de l'assiette
\se nõ za
\dialx GOs
\sge l'intérieur, le creux de l'assiette
\xv zabo-çö
\dialx GO
\xn ton assiette
\xv zabo-nu
\dialx GOs
\xn mon assiette
\xv zabo-ny
\dialx PA
\xn mon assiette
\xv yabo-m
\dialx BO
\xn ton assiette
\ng forme déterminée: |lx{zabo-} |dialx{GOs PA}, |lx{yabo-} |dialx{BO}
\dt 09/Jan/2022

\lx za
\hm 2
\ph ða
\dialx GOs PA
\va zha
\ph θa
\dialx GA
\va ya
\dialx BO
\is nourriture_goût
\ps v.stat.
\ge salé ; trop salé
\dt 28/Aug/2021

\lx za
\hm 3
\ph ða
\dialx GOs
\va ra
\dialx WE WEM
\is grammaire_assertif
\ps FOCUS ; RESTR (antéposé au GN) (za … nye …)
\ge c'est vraiment … que
\xv ô ! za ije nye penõ dròrò
\xn oui ! c'est bien lui qui a volé hier
\xv ô ! za ilò nye lò penõ dròrò
\xn oui ! c'est bien eux qui ont volé hier
\xv hai ! za inu ma ãbaa-nu nye bî a
\xn non! c'est bien ma sœur et moi qui sommes parties
\xv hai ! za inu ma ãbaa-nu hãda nye bî a
\xn non! c'est seulement ma sœur et moi qui sommes parties
\xv hai ! za caaça hãda ma ãbaa-nu nye li a
\xn non! c'est seulement mon père et ma sœur qui sont partis
\xv e za gi dròrò ?
\xn il a vraiment pleuré hier ?
\xv za ije nye gi dròrò
\xn c'est vraiment lui qui a pleuré hier
\xv e ra mòlò ? – ôô xa, e ra mòlò
\dialx WEM
\xn vit-elle encore ? – Oui bien sûr, elle vit encore
\dt 22/Feb/2025

\lx za
\hm 4
\ph ða
\dialx GOs
\va ra
\dialx PA BO
\is grammaire_adverbe
\is grammaire_assertif
\ps ADV INTENS ; assertif (devant le prédicat)
\ge vraiment ; tout à fait
\xv nu za u trêê
\dialx GOs
\xn j'ai vraiment couru
\xv nu za nee xo nu
\dialx GOs
\xn c'est moi qui l'ai fait
\xv la za yaawa dròrò
\dialx GOs
\xn ils étaient vraiment tristes hier
\xv e za mõlò gò ? – Ô, e za mõlò gò
\dialx GOs
\xn elle vit vraiment encore ? – Oui, elle vit toujours
\xv e zaa ne xo je
\dialx GOs
\xn il l'a vraiment fait
\xv e za uvi tiiwo ponga-je
\dialx GOs
\xn il s'est acheté un livre pour lui-même
\xv i khõbwe vwo ra ije
\dialx PA
\xn il parle en son nom propre (pour lui-même)
\dt 22/Feb/2025

\lx za
\hm 5
\dialx PA WE
\is grammaire_pronom
\ps PRO.1° pers. excl. PL
\ge nous (plur. excl.)
\dt 26/Aug/2023

\lx -za
\hm 6
\dialx PA BO
\va -ya
\dialx BO
\is grammaire_pronom
\ps PRO 1° pers. incl. (OBJ ou POSS)
\ge nous ; notre
\dt 22/Feb/2025

\lx za ?
\hm 7
\dialx GOs
\va ra?
\dialx WEM PA BO
\is grammaire_interrogatif
\ps INT (post-verbal ou post-nominal)
\ge quelle sorte (de) ?
\ge quoi ? ; qu'est ce que ?
\xv çö po za ?
\dialx GOs
\xn que fais-tu ? quelle sorte d'activité as-tu ?
\xv çö po ra ?
\dialx WEM
\xn que fais-tu ?
\xv hèlè za ? – Hèlè ba-cooxe layô
\dialx GOs
\xn quelle sorte de couteau? – Un couteau pour couper la viande
\xv pò-cee za ?
\dialx GOs
\xn quelle sorte de fruit est-ce ?
\xv mwa za ? – Mwa dili
\dialx GOs
\xn quelle sorte de maison ? – Une maison en terre
\xv mõ-da ? – Mõ-pe-rooli, mõ-cia
\dialx GOs
\xn une maison pour quoi? qui sert à quoi ? – Une maison de réunion, une maison de danse
\xv cee za nye ?
\dialx GOs
\xn quelle sorte d'arbre est-ce ? comment s'appelle-t-il ?
\xv nõ za ?
\xn quelle sorte de poisson ?
\xv pwaji za ?
\xn quelle sorte de crabe ?
\dt 24/Feb/2025

\lx za mee
\dialx GOs
\va tha-mee
\dialx GO(s)
\is action
\ps v
\ge tailler en pointe ; appointer (un bout de bois)
\xv e za mee-cee
\xn il taille un bout de bois en pointe
\dt 20/Feb/2025

\lx za xa
\dialx GOs
\va xa
\dialx GO(s)
\is grammaire_aspect
\ps ITER
\ge encore ; à nouveau
\xv e za xa thomã-çö iò
\dialx GOs
\xn il t'a déjà/vraiment rappelé tout à l'heure
\xv na za xa bwovwô ã-è Jae : "Kani, nu bwovwô !"
\dialx GOs
\xn quand Jae est à nouveau fatigué : "canard, je suis fatigué"
\dt 21/Oct/2021

\lx zaa
\hm 1
\dialx GOs PA
\va zhaa
\dialx GA
\va yaa
\dialx BO
\is oiseau
\ps n
\ge poule sultane
\dn symbole de l'abondance des cultures ; son opposé est le rat qui saccage les cultures
\sc Porphyrio porphyrio caledonicus
\scf Rallidés
\gb Purple Swamp Hen
\dt 22/Oct/2021

\lx zaa
\hm 2
\dialx GOs PA
\va zhaa
\dialx GA
\is santé
\ps v
\ge engourdi ; avoir des fourmis (dans les membres)
\xv e zaa kòò-nu
\dialx GOs
\xn j'ai la jambe engourdie
\dt 04/Feb/2019

\lx zaa pweza
\dialx PA
\is bananier
\ps v
\ge prélever les rejets d'un bananier |lx{pweza}
\dn pour les replanter; cette banane noble est celle de la chefferie, et entre dans les échanges coutumiers
\dt 08/Feb/2025

\lx zaa phwa
\dialx PA WEM
\is cultures
\ps v
\ge préparer les champs et le trou pour planter les ignames
\dn la terre est retournée et soulevée avec un bâton pour la rendre meuble, puis on prépare la butte et enfin on fait le trou dans la terre
\cf thu phwa
\ce faire des trous
\dt 22/Aug/2021

\lx zaadu
\dialx GOs BO
\is nourriture
\ps v
\ge maigre ; non gras (viande, poisson)
\nt selon Corne
\dt 22/Feb/2025

\lx zaae
\dialx GOs PA
\va zhae
\dialx GA
\is action_plantes
\ps v
\ge faire mûrir (fruits)
\ge conserver pour faire mûrir
\dt 20/Aug/2021

\lx zaalò
\ph ða:lɔ
\dialx GOs PA
\va zhaalo
\dialx GA
\ph θa:lo
\va yaalo
\dialx BO
\is arbre
\ps n
\ge "gommier"
\sc Cordia dichotoma
\scf Borraginacées
\dt 23/Mar/2023

\lx zaalòè
\dialx GOs PA
\is action
\ps v
\ge coller
\se pe-zaalòè
\sge coller ensemble
\dt 23/Mar/2023

\lx zaawane
\hm 1
\ph ða:waɳe
\dialx GOs
\is poisson
\ps n
\ge "aiguillette" (de taille adulte)
\sc Tylosorus crocodilus crocodilus
\scf Belonidés
\dt 27/Aug/2021

\lx zaawane
\hm 2
\ph ða:waɳe
\dialx GOs
\is plantes_processus
\ps v
\ge presque mûr
\dt 23/Jan/2018

\lx zaba
\dialx GOs PA
\va zhaba
\dialx GA
\va yhaba
\dialx BO
\sn 1
\is interaction
\ps v
\ge encourager ; soutenir
\sn 2
\is discours
\ps v
\ge répondre ; donner la réplique
\xv e zaba cai nu
\dialx GO
\xn il m'a répondu
\dt 24/Jan/2022

\lx zabajo
\dialx GOs
\is déplacement
\ps v
\ge passer à toute allure
\ge courir vite
\dt 22/Aug/2021

\lx zabo
\dialx BO
\is feu
\ps n
\ge suie noire de la fumée dans les maisons
\nt selon Corne ; non vérifié
\dt 27/Mar/2022

\lx zabò
\dialx GOs BO PA WEM
\va zhabò
\dialx GA
\sn 1
\is corps
\ps n
\ge côtes
\xv zabò-n
\dialx PA
\xn sa côte
\sn 2
\is maison
\ps n
\ge gaulettes
\dn qui retiennent la couverture du toit
\cf orèi
\ce gaulettes circulaires
\dt 08/Feb/2025

\lx zabò mwa
\dialx GOs
\is maison
\ps n
\ge gaulettes
\dn qui retiennent la couverture du toit faite d'écorce de niaouli et de paille
\dt 08/Feb/2025

\lx zaboriã
\dialx BO
\is société_organisation
\ps n
\ge porte-parole du chef
\nt selon Corne ; non vérifié
\dt 26/Mar/2022

\lx zaçixõõni
\dialx GOs
\is interaction
\ps v
\ge rapporter ; dénoncer
\dt 27/Jan/2019

\lx zagaò
\dialx GOs
\va zagaòl
\dialx PA
\is cultures
\ps v
\ge récolter les ignames ; époque où l'on récolte les ignames
\ge repousse spontanée des plants
\ge glaner (des ignames, bananes, taros)
\dn cela se pratique dans des champs laissés en jachère ou à l'abandon
\xv la zagaò-ni kui
\dialx GOs
\xn ils récoltent les ignames
\ng v.t. |lx{zagaò-ni}
\cf maxuã
\ce glaner de la canne à sucre
\dt 24/Feb/2025

\lx zagawe
\dialx GOs
\va zhagawe
\dialx GA
\is cultures
\ps v
\ge réserver des tubercules (pour les replanter)
\xv la zhagawe kui
\dialx GA
\xn ils réservent des ignames
\xv la p(h)e-zhagawe-ni êê-la kui
\dialx GA
\xn ils réservent leurs plants d'igname
\dt 21/Feb/2019

\lx zage
\dialx GOs PA
\va zhage
\dialx GA
\va yhage
\dialx BO
\is interaction
\ps v
\ge aider
\xv la pe-zage u la khabe nye mwa
\dialx PA
\xn ils s'entraident pour construire cette maison
\xv kavwö yu zagee-nu
\dialx PA
\xn tu ne m'as pas aidé
\se pe-yhage
\dialx BO
\sge s'entraider
\dt 05/Nov/2021

\lx zageeni
\dialx GOs
\va zhageeni
\dialx GA
\is interaction
\ps v
\ge ajouter ; abonder dans le sens de qqn.
\dt 29/Mar/2022

\lx zagi
\ph ðaŋgi
\dialx GOs PA
\va zhagia
\dialx GO(s)
\ph θa:ŋgia
\va yagi
\dialx PA BO
\is corps
\ps n
\ge cerveau ; cervelle
\dt 04/Feb/2019

\lx zagia ciia
\dialx GOs
\is céphalopode
\ps n
\ge encre de poulpe
\dt 23/Jan/2018

\lx zagu
\dialx BO
\is cultures_champ
\ps n
\ge champ de taros défriché, mais non labouré
\nt selon Dubois ; non vérifié
\dt 24/Feb/2025

\lx zai
\dialx GOs BO
\va zhai
\dialx GA
\is musique
\ps v
\ge composer un chant
\xv wa xa zai xo ti ?
\xn c'est un chant composé par qui ?
\se za wal
\dialx BO
\sge trouver le thème d'un chant
\dt 21/Feb/2019

\lx zakèbi
\dialx GOs
\va zaxèbi
\dialx GO(s)
\va zhaxèbi
\dialx GA
\is caractéristiques_personnes
\ps n
\ge habile ; qui a du savoir-faire
\ge habitué à faire qqch.
\xv e kô-zaxèbi
\xn il a du savoir-faire
\dt 22/Feb/2025

\lx zala
\hm 1
\dialx GOs PA
\va yhala
\dialx BO
\is nourriture
\ps v
\ge chercher de la nourriture
\ge glaner
\se a-zala
\dialx GOs
\sge aller chercher de la nourriture
\dt 24/Feb/2025

\lx zala
\hm 2
\ph ðala
\dialx GOs PA
\va zhala
\ph θala
\dialx GA
\is interaction
\ps v ; n
\ge demander à qqn. ; interroger ; question(ner)
\xv e zala-nu khõbwe çö a mõnõ
\dialx GOs
\xn il me demande si tu pars demain
\xv e zala khõbwe la mińõ dröö
\dialx GOs
\xn il demande si les marmites sont prêtes
\cf phaja
\dialx WEM
\ce demander à qqn.
\cf zaba
\dialx GOs
\ce répondre
\dt 21/Feb/2025

\lx zalae
\dialx GOs
\va zhalae
\dialx GA
\is interaction
\ps v
\ge donner de la nourriture
\ge subvenir aux besoins
\ge élever
\dt 08/Feb/2025

\lx zali
\ph ðali
\dialx GOs
\va zhali
\ph θali
\dialx GA
\sn 1
\is action_corps
\ps v
\ge enlever (natte)
\ge soulever (des pierres, herbes)
\xv e zali thrô
\dialx GOs
\xn elle enlève les nattes
\xv e zali dròò-chaamwa
\dialx GOs
\xn elle enlève les palmes de bananier (couvrant le four enterré)
\xv e zali paa
\dialx PA
\xn il soulève/enlève les pierres
\xv e zali paxa
\dialx PA
\xn il enlève les herbes
\sn 2
\is pêche
\ps v
\ge ramasser (filet)
\xv e zali pwiò
\dialx PA
\xn il ramasse le filet
\sn 3
\is cultures
\ps v
\ge retourner (la terre)
\cf zaa phwa
\dialx PA
\ce préparer les champs d'igname
\cf thu phwa
\ce faire des trous (en retournant la terre)
\dt 10/Jan/2022

\lx zamadra
\dialx GOs
\is armes
\va zamada
\dialx PA
\ps n
\ge sparterie de sagaie
\dt 13/Jul/2018

\lx zano
\dialx PA
\is coutumes
\ps n
\ge herbe
\dn terme utilisé uniquement dans le contexte cérémoniel
\xv ni zano
\xn dans l'herbe
\dt 22/Feb/2025

\lx zanyi
\ph ðaɲi
\dialx GOs
\va zhanyi
\ph θaɲi
\dialx GA
\sn 1
\is nourriture
\ps n
\ge sel
\sn 2
\is préparation_aliments
\ps v
\ge saler la nourriture ; mettre du sel
\xv zanyi ni dröö !
\xn met du sel dans la marmite !
\dt 25/Aug/2021

\lx zanyii
\ph ðaɲi:
\dialx GOs
\va zhanyii
\dialx GA
\va hing
\dialx PA
\is nourriture
\ps v
\ge dégoûté ; faire le difficile
\xv êgu xa nu zanyii
\xn quelqu'un qui me dégoûte
\xv e a-zanyii
\xn c'est un maniaque (dégoûté par tout ce qui est sale)
\dt 13/Sep/2021

\lx zaò
\dialx GOs
\va caò
\dialx GO
\is religion
\ps v
\ge invoquer ; parler aux esprits
\dt 23/Mar/2023

\lx zaòl
\dialx PA
\is igname
\ps n
\ge igname
\dn clone à petites racines, planté sur le bord du billon ; ces ignames poussent plus vite que les ignames à racines longues du centre du billon et donnent les premières récoltes (selon Charles Pebu-Polae)
\dt 22/Oct/2021

\lx zara
\dialx BO
\is igname
\ps n
\ge igname blanche, tendre
\nt selon Dubois ; non vérifié
\dt 26/Mar/2022

\lx zaro
\dialx GOs PA
\va zharo
\dialx GA
\va zaatro
\dialx arch. (Haudricourt)
\va yaro
\dialx BO
\sn 1
\is outils
\ps n
\ge fourche (trident) |dialx{GOs}
\sn 2
\is outils
\ps n
\ge pelle à fouir les ignames
\dn en bois ou en fer
\ge bêche |dialx{PA, BO}
\sn 3
\is cultures
\ps v
\ge labourer avec une pelle à fouir
\dt 08/Feb/2025

\lx zatri
\ph ðaɽi
\dialx GOs
\va zari
\dialx GO(s) PA
\va zhari
\dialx GA
\va yari
\dialx BO
\is médecine
\ps n
\ge remède ; médicaments
\xv zatria-je
\dialx GOs
\xn son médicament
\xv i a thu-zari
\dialx PA
\xn elle va chercher des plantes médicinales
\se zari-raa
\sge les mauvais médicaments
\se zari-zo
\sge les bons médicaments
\se zari-alo
\dialx PA
\sge devin ; voyant
\dt 22/Feb/2025

\lx zava
\ph ðava
\dialx GOs
\va izaa
\dialx PA
\is grammaire_pronom
\ps PRO.INDEP 1° pers. excl. PL (sujet)
\ge nous (excl.)
\cf izava
\dt 11/Feb/2025

\lx -zava
\ph ðava
\dialx GOs
\is grammaire_pronom
\ps PRO 1° pers. excl. PL (OBJ ou POSS)
\ge nous ; nos
\dt 29/Jan/2019

\lx zawa
\ph ðawa
\dialx GOs
\is grammaire_pronom
\ps PRO.INDEP 2° pers. PL
\ge vous (plur.)
\dt 27/Mar/2022

\lx -zawa
\ph ðawa
\dialx GO
\is grammaire_pronom
\ps PRO 2° pers. PL (OBJ ou POSS)
\ge vous ; vos
\dt 29/Jan/2019

\lx zawe
\dialx GOs
\is igname
\ps n
\ge igname (violette)
\dt 29/Jan/2019

\lx zawexan
\dialx PA BO
\va yawegan
\dialx BO
\is caractéristiques_objets
\ps n
\ge rouille
\dt 26/Jan/2019

\lx zaxòe
\ph ða'xo.e
\dialx GOs PA
\va zhaxòe
\ph θa'xo.e
\dialx GA
\va zakòe
\dialx GO(s)
\va yaxòe, yagoe
\dialx BO
\sn 1
\is nourriture
\ps v
\ge goûter
\xv la zaxò kui
\dialx PA
\xn ils goûtent les prémisses de l'igname
\sn 2
\is grammaire_modalité
\ps v
\ge essayer ; à l'essai ; à tout hasard
\xv e zaxòe nee
\dialx GOs
\xn il a essayé de le faire
\dt 22/Dec/2021

\lx zee
\ph ðe
\dialx GOs TRE
\is fonctions_naturelles
\ps v ; n
\ge cracher
\se we-zee
\sge crachat
\se paxa-zee
\sge expectorations
\dt 25/Jan/2019

\lx zeede
\dialx GOs
\va zedil
\dialx PA
\is action_tête
\ps v
\ge siffler avec les doigts pour héler qqn.
\dt 29/Mar/2022

\lx zeele
\dialx GOs
\is poisson
\ps n
\ge requin marteau
\sc Sphyrna lewini
\scf Sphyrénidés
\dt 15/Feb/2025

\lx zenô
\ph ðeɳõ
\dialx GOs PA
\va zhenô
\dialx GA
\va yhèno, zeno
\dialx BO PA
\is plantes_processus
\ps v
\ge mûr ; arrivé à maturité
\ge bien formé
\xv zenô kui
\dialx GOs BO
\xn l'igname est arrivée à maturité
\xv hãgana novwö e zenô mwã kui
\dialx GOs
\xn maintenant que l'igname est mûre
\xv e zenô phãgoo-je
\dialx GOs
\xn son corps est arrivé à maturité (d'un enfant)
\cf mii
\ce mûr; rouge
\an aava, aa
\at pas encore mûr (contraire de |lx{zenô})
\dt 05/Jan/2023

\lx zibi
\dialx GOs
\is caractéristiques_personnes
\ps v.stat
\ge vif
\xv êgu xa zibi
\xn une personne vive
\cf hibil
\dialx PA BO
\dt 27/Oct/2023

\lx zido
\dialx GOs
\va zhido
\ph θindo
\dialx GA
\is fonctions_naturelles
\ps v
\ge regarder (se) (dans un miroir)
\xv e zido Kaavwo ni we
\xn Kaavwo se regarde/se mire dans l'eau
\dt 13/Oct/2021

\lx zii
\ph ði:
\dialx GOs PA BO
\is habillement
\ps n
\ge étoffe d'écorce de banian
\ge balassor
\dt 24/Aug/2021

\lx zine
\ph ðiɳe
\dialx GOs
\va zhine
\dialx GA
\va jine
\dialx GO(s)
\is mammifères
\ps n
\ge rat
\cf ciibwin
\dialx PA BO
\ce rat
\dt 10/Jan/2022

\lx ziu
\dialx PA
\is insecte
\ps n
\ge asticot
\dt 27/Aug/2023

\lx zixô
\dialx GOs PA
\va zhixô
\dialx GA
\va zikô, zhikô
\dialx GO(s) arch.
\va hixò, hingõn
\dialx BO [BM]
\is discours_tradition_orale
\ps v ; n
\ge histoire ; fable
\ge raconter une histoire
\xv zixôô-nu
\dialx GOs
\xn ma fable ; mon conte
\xv e zixô cai la pòi-je xo õ ẽnõ-ã
\dialx GOs
\xn la mère de ces enfants raconte une histoire à ses petits
\xv e zixô õ ẽnõ-ã cai la pòi-je
\dialx GOs
\xn la mère de ces enfants raconte une histoire à ses petits
\xv e za zixôô-ni je-nã
\dialx GOs
\xn elle conte cela
\xv zixòò-ny
\dialx PA
\xn ma fable ; mon conte
\xv hixòò-ny
\dialx BO
\xn ma fable ; mon conte
\xv higõõ-ny ã ciibwin ma amãla-ò mèni
\dialx BO
\xn mon conte sur le rat et les autres oiseaux
\ng v.t. |lx{zixô-ni}
\dt 05/Jan/2022

\lx zo
\hm 1
\ph ðo
\dialx GOs PA
\va zho
\dialx GO(s)
\va yo
\dialx BO
\sn 1
\is caractéristiques_personnes
\ps v.stat.
\ge bien ; bon
\xv e wa zo
\dialx GO
\xn il chante bien
\xv i wa zo
\dialx PA
\xn il chante bien
\xv e waze-zoo-ni wa
\dialx GO
\xn il a bien chanté la chanson
\xv kavwö lhi yue-zoo-ni mwã la-ã
\dialx GO
\xn ils ne se sont pas bien occupés d'eux (des animaux)
\xv kine-zoo-ni drube !
\dialx GO
\xn vise bien le cerf !
\xv Haxe me khõbwe-zoo-ni mwa
\dialx GO
\xn et pour que nous disions bien les choses
\xv nee-yoo-ne
\dialx BO
\xn fais-le bien
\xv i khõbwe-yoo-ni
\dialx BO
\xn il a bien parlé
\ng forme transitive en composition: v.t. |lx{-zoo-ni}  |dialx{GOs}, v.t. |lx{-yoo-ni} |dialx{PA}
\sn 2
\is caractéristiques_personnes
\ps v.stat.
\ge propre
\sn 3
\is grammaire_modalité
\ps v
\ge pouvoir ; falloir ; devoir
\xv e zo na çö wa zo
\dialx GO
\xn tu dois bien chanter
\xv axe je-nã, e zo na e a-du ni we-za
\dialx GO
\xn mais voilà, il faudra traverser la mer
\dt 18/Feb/2023

\lx zo
\hm 2
\dialx GO
\va ro, ru
\dialx WEM WE
\is grammaire_temps
\ps FUT
\ge futur
\xv kavwö mi zo nõõ-je
\dialx GO
\xn nous deux ne la reverrons pas
\dt 17/Oct/2021

\lx zo
\hm 3
\dialx GOs
\va zho
\dialx GA
\is richesses
\ps n
\ge biens ; affaires
\xv zoo i je
\xn ses affaires
\dt 26/Aug/2021

\lx zò
\hm 1
\is action
\dialx GOs
\ps v
\ge couper
\ng v.t. |lx{zòi}
\dt 05/Jan/2022

\lx zò
\hm 2
\dialx GOs
\va zhò
\dialx GA
\va zòn
\dialx PA
\is santé
\ps v
\ge gratteux (être)
\dn i.e. atteint par la ciguatera
\se nõ zò
\sge poisson "gratteux"
\xv nu tròòli mã nõ-zò
\dialx GOs
\xn j'ai attrapé la "gratte"/la ciguatera (c.-à-d., la maladie du poisson atteint par la ciguatera)
\dt 22/Feb/2025

\lx zoa
\dialx GOs
\is chasse
\ps n
\ge lacet (chasse)
\dt 23/Jun/2018

\lx zoe
\dialx GOs
\is action_corps
\ps v
\ge attacher (lacet, vêtement)
\dt 23/Jan/2018

\lx zòi
\ph ðɔɨ
\dialx GOs PA BO
\va zhòi
\dialx GO(s)
\sn 1
\is bois_travail
\is action_outils
\ps v.t.
\ge scier
\se ba-zò-cee
\sge scie (à bois) ; tronçonneuse
\sn 2
\is action_corps
\ps v.t.
\ge couper (se)
\ge couper (viande)
\xv e za draa zòi
\dialx GOs
\xn il s'est coupé volontairement
\xv e zòi hii-je xo hèlè
\dialx GOs
\xn il s'est coupé la main avec le couteau
\dt 22/Feb/2025

\lx zòli
\dialx GOs PA
\va zhòli
\dialx GA
\va yòli
\dialx BO
\sn 1
\is préparation_aliments
\ps v.t.
\ge râper (coco)
\ge gratter (des féculents)
\dn par ex. l'igname cuite ou la peau de l'igname, des pommes de terre, du taro
\xv zò-nu
\dialx BO
\xn râper du coco
\se ba-zò-nu
\dialx BO
\sge râpe à coco
\sn 2
\is action_corps
\ps v.t.
\ge récurer
\dn le dos de la marmite avec de la cendre
\sn 3
\is action_corps
\ps v.t.
\ge griffer
\ge écorcher (s') la peau
\xv i yòli-nu ho minòn
\dialx BO
\xn le chat m'a griffé
\ng v.i. |lx{zòl} |dialx{PA}, |lx{zò} |dialx{GOs}
\et *kodi
\el POc
\dt 08/Feb/2025

\lx zoma
\dialx GOs
\va ezoma
\dialx GOs
\is grammaire_temps
\ps FUT
\ge futur
\xv nu zoma nee mõ-çö
\dialx GOs
\xn je construirai ta maison
\cf ezoma
\ce futur proche
\dt 21/Oct/2021

\lx zòn
\dialx PA
\va yòn, yhòn
\dialx BO
\sn 1
\is caractéristiques_objets
\ps v ; n
\ge toxique ; non comestible ; poison
\xv dòò-cee zòn
\dialx PA
\xn feuilles non comestibles, toxiques
\xv i yòn (a) nye nõ
\dialx BO
\xn ce poisson n'est pas comestible/est atteint par la ciguatera
\an dòò-cee hovho
\dialx PA
\at feuilles comestibles
\sn 2
\is santé
\ps v
\ge intoxiqué par la ciguatera
\xv nu zòn
\dialx PA
\xn je suis intoxiqué par la ciguatera
\xv ma zòn
\dialx PA
\xn maladie de la ciguatera
\xv kixa mwã na i zò(ò)n na ni phagòò-ny
\dialx PA
\xn il n'y a rien de toxique dans mon corps
\dt 22/Feb/2025

\lx zòò
\hm 1
\dialx PA
\is grammaire_pronom
\ps PRO 2° pers. PL (sujet, OBJ ou POSS)
\ge vous ; votre (plur.)
\dt 22/Feb/2025

\lx zòò
\hm 2
\dialx GOs
\is caractéristiques_objets
\ps n
\ge difficulté ; embûche
\se kixa zòò
\sge facile
\se pu zòò
\sge difficile
\dt 20/Oct/2021

\lx zòò
\hm 3
\ph ðɔ:
\dialx GOs
\va zhòò
\dialx GO(s)
\va zòòm
\ph ðɔ:m
\dialx PA
\va yhòòm, yòòm, yòò
\dialx BO
\sn 1
\is action_corps
\ps v
\ge nager
\sn 2
\is action_plantes
\ps v
\ge ramper (pour des lianes)
\et *kaRu
\eg nager
\el POc
\et *kakau (*kk > θ, ð)
\eg PNC
\ea Haudricourt
\dt 22/Feb/2025

\lx zòò
\hm 4
\dialx GOs PA BO
\va zhò
\dialx GA
\is cultures
\ps n
\ge rejet (de plante servant à bouturer)
\xv zòò-n
\dialx BO
\xn ses jeunes pousses
\se zòò-ê
\dialx GOs
\sge rejet/bouture de canne à sucre
\dt 23/Mar/2023

\lx zòò-chaamwa
\dialx GOs
\is bananier
\ps n
\ge pousse (ou) rejet de bananier
\ge bouture de bananier
\dt 09/Jan/2022

\lx zòòni
\ph ðɔ:ɳi
\dialx GOs PA
\va zhòòni
\dialx GO(s)
\va yooni
\dialx BO (BM, Corne)
\va yhoonik
\dialx BO (Corne)
\is arbre
\ps n
\ge niaouli
\sc Melaleuca leucadendron
\scf Myrtacées
\dn lors des naissances, on enveloppait le nourrisson dans l'écorce de niaouli pour le protéger et lui donner de la force ; lors des décès, les feuilles et branches enveloppent la monnaie blanche qui autorise l'échange entre deux clans
\se mû-zòòni
\sge fleur de niaouli
\dt 27/Aug/2021

\lx zòò-uva
\dialx GOs PA
\va zò-uva
\dialx GO(s) PA
\va yo-uva
\dialx BO
\is taro
\ps n
\ge jeunes pousses de taro d'eau ; rejets de taro d'eau
\dt 24/Feb/2025

\lx zòòwa
\dialx GOs
\is poisson
\ps n
\ge relégué
\sc Terapon jarbua
\scf Teraponidés
\dt 27/Aug/2021

\lx zovaale
\dialx GOs PA
\va yo-vhaale
\dialx BO [Corne]
\is interaction
\ps v
\ge murmurer ; parler doucement ; parler à voix basse
\ge rapporter ; dénoncer
\dt 15/Aug/2021

\lx zò-xabu
\dialx GOs
\va yo-xabu
\dialx BO
\is caractéristiques_corps
\ps n
\ge chair de poule (froid) 
\dn (lit. réaction cutanée au froid) [Corne]
\cf zò khaabu
\ce réaction cutanée au froid
\dt 08/Feb/2025

\lx zoxãî
\dialx WEM WE
\va zogãî
\dialx PA
\is caractéristiques_personnes
\ps v.stat.
\ge joli ; bien
\xv ra gu zogãî ala-mèè-n
\dialx PA
\xn son visage est très beau
\dt 22/Dec/2021

\lx zòxu
\dialx GOs
\va zhòxu
\dialx GO(s)
\is poisson
\ps n
\ge carpe
\sc Kuhlia sp.
\scf Kuhliidés
\cf thãi
\ce carpe
\dt 27/Aug/2021

\lx zu
\dialx GOs PA
\va zhu
\dialx GO(s)
\va yu
\dialx BO
\is poisson
\ps n
\ge mulet de rivière (noir)
\dn il pond à l'embouchure, puis remonte la rivière
\sc Mugil cephalus
\scf Mugilidés
\et *zau
\eg mulet de rivière
\el PEOc
\dt 27/Aug/2021

\lx zuanga
\ph ðu'aŋa
\dialx GOs PA
\va phwa-zua
\dialx GO(s)
\va zhuanga
\dialx GA
\va yuanga
\dialx BO
\is discours
\ps n
\ge zuanga (nom de la langue)
\dt 23/Aug/2021

\lx zugi
\ph ðuŋgi
\dialx GOs PA
\va zhugi
\ph θuŋgi
\dialx GA
\va yugi
\dialx BO
\is action_corps
\ps v
\ge tordre
\ge courber
\ge ployer
\ge arquer
\ge ramasser (en roulant)
\ge (re)tirer
\ge rembobiner (ligne de pêche)
\ge retrousser (robe, pantalon)
\xv e zugi pwiò
\dialx GO
\xn il tire le filet
\xv e zugi pwe
\dialx GO
\xn il rembobine la ligne (de pêche)
\xv e zugi hõbwòli-je
\dialx GO
\xn elle a retroussé sa robe
\xv e zugi hi-hõbwòli-je
\dialx GO
\xn elle a retroussé les manches de son vêtement
\xv i yugi wal
\dialx BO
\xn il enroule la corde
\dt 22/Dec/2021

\lx zume
\dialx GOs
\va zhume
\dialx GO(s) GA
\va zome
\dialx PA
\va zume-n, yume-n
\dialx BO
\is fonctions_naturelles
\ps v
\ge cracher
\xv zume-du pwa !
\dialx GOs
\xn crache dehors !
\se we-zume
\dialx GOs
\sge crachat
\se we ni zume
\dialx PA
\sge crachat
\se paxa-zume
\sge expectorations
\dt 23/Mar/2023

\lx zuzuu
\dialx GOs
\va zhuzuu
\dialx GO(s)
\is crustacés
\ps n
\ge crabe de sable
\dt 08/Oct/2021
