<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:import href="en-tête.xsl"/>

<xsl:template match="RessourceLexicale">
<xsl:apply-templates select="." mode="thématique"/>
</xsl:template>

<xsl:template match="RessourceLexicale" mode="thématique">
<xsl:call-template name="ajouter_entête_générale"/>
<xsl:call-template name="ajouter_entête_thématique"/>

\begin{document}

\pagenumbering{roman}

<xsl:call-template name="ajouter_premières_pages"/>

\bibliographie

\mainmatter

\begin{dictionnaire}[\textefra{Dictionnaire thématique}]
<xsl:apply-templates select="InformationsLexicographiques" mode="thématique"/>
\end{dictionnaire}

\tabledesmatières

\end{document}

</xsl:template>

<xsl:template match="InformationsLexicographiques" mode="thématique">
    <xsl:apply-templates select="OrdreThématique/Hiérarchie" mode="thématique"/>
</xsl:template>

<xsl:template match="Hiérarchie" mode="thématique">
    <xsl:apply-templates select="Élément" mode="thématique">
        <xsl:with-param name="profondeur" select="1"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:strip-space elements="Enfants"/>

<xsl:template match="Élément" mode="thématique">
    <xsl:param name="profondeur"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test="Identifiant">
            <xsl:variable name="lettrine">
                <xsl:call-template name="section">
                    <xsl:with-param name="profondeur" select="$profondeur"/>
                </xsl:call-template>
                <xsl:text>{</xsl:text>
                <xsl:value-of select="Valeur"/>
                <xsl:text>}</xsl:text>
            </xsl:variable>
            <xsl:variable name="domainesémantique" select="Identifiant"/>
            <xsl:variable name="bloc_entrées">
                <xsl:apply-templates select="/RessourceLexicale/Dictionnaire/EntréesLexicales" mode="thématique">
                    <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
                </xsl:apply-templates>
            </xsl:variable>
            <xsl:if test="$bloc_entrées != ''">
                <xsl:if test="ancestor::Élément[1][not(Identifiant)] and not(preceding-sibling::Élément)">
                    <xsl:text>\vspace{-2ex}</xsl:text> <!-- Le calcul du plus petit espacement vertical de titlesec pour des titres successifs ne voit pas une section cachée dans l’environnement blocthème. -->
                    <xsl:text>&#10;</xsl:text>
                </xsl:if>
                <xsl:text>\begin{blocthème}{</xsl:text>
                <xsl:value-of select="$lettrine"/>
                <xsl:text>}</xsl:text>
                <xsl:text>&#10;</xsl:text>
                <xsl:if test="count(/RessourceLexicale/Dictionnaire/EntréesLexicales/EntréeLexicale[.//DomaineSémantique=$domainesémantique]) = 1 and string-length($bloc_entrées) &lt; 400">
                    <xsl:text>\setcounter{minrows}{3}</xsl:text> <!-- Afin d’éviter une coupure de colonne en plein milieu d’une entrée courte s’il n’y a qu’une entrée dans le bloc thématique. -->
                    <xsl:text>&#10;</xsl:text>
                </xsl:if>
                <xsl:value-of select="$bloc_entrées"/>
                <xsl:text>\end{blocthème}</xsl:text>
                <xsl:text>&#10;</xsl:text>
                <xsl:if test="count(/RessourceLexicale/Dictionnaire/EntréesLexicales/EntréeLexicale[.//DomaineSémantique=$domainesémantique]) = 1 and string-length($bloc_entrées) &lt; 400">
                    <xsl:text>\setcounter{minrows}{1}</xsl:text> <!-- Remise à la valeur par défaut. -->
                <xsl:text>&#10;</xsl:text>
                </xsl:if>
            </xsl:if>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="section">
                <xsl:with-param name="profondeur" select="$profondeur"/>
            </xsl:call-template>
            <xsl:text>{</xsl:text>
            <xsl:value-of select="Valeur"/>
            <xsl:text>}</xsl:text>
            <xsl:text>&#10;</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="Enfants" mode="thématique">
        <xsl:with-param name="profondeur" select="$profondeur + 1"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="EntréesLexicales" mode="thématique">
    <xsl:param name="domainesémantique"/>
    <xsl:apply-templates select="EntréeLexicale[.//DomaineSémantique=$domainesémantique]" mode="thématique">
        <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="EntréeLexicale" mode="thématique">
    <xsl:param name="domainesémantique"/>
    <xsl:text>\begin{entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme" mode="thématique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="NuméroDHomonyme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Lemme/FormePhonétique" mode="thématique"/>
    <xsl:apply-templates select="Lemme/Région" mode="thématique"/>
    <xsl:apply-templates select="Notes[Note[Type='phonétique']]" mode="thématique"/>
    <xsl:apply-templates select="Lemme/Variantes" mode="thématique"/>
    <xsl:apply-templates select="Groupes" mode="thématique">
        <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="ListeDeSens" mode="thématique">
        <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
    </xsl:apply-templates>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{entrée}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Groupes" mode="thématique">
    <xsl:param name="domainesémantique"/>
    <xsl:for-each select="Groupe">
        <xsl:apply-templates select=".[.//DomaineSémantique=$domainesémantique]" mode="thématique">
            <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
        </xsl:apply-templates>
    </xsl:for-each>
</xsl:template>

<xsl:template match="Groupe" mode="thématique">
    <xsl:param name="domainesémantique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{groupe}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="NomDeGroupe"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="PartieDuDiscours" mode="thématique"/>
    <xsl:apply-templates select="ListeDeSens" mode="thématique">
        <xsl:with-param name="domainesémantique" select="$domainesémantique"/>
    </xsl:apply-templates>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{groupe}</xsl:text>
</xsl:template>

<xsl:template match="Sous-entréesLexicales" mode="thématique">
    <xsl:apply-templates select="Sous-entréeLexicale" mode="thématique"/>
</xsl:template>

<xsl:template match="Sous-entréeLexicale" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{sous-entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme" mode="thématique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Lemme/Région" mode="thématique"/>
    <xsl:apply-templates select="Lemme/Variantes" mode="thématique"/>
    <xsl:apply-templates select="ListeDeSens" mode="thématique"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{sous-entrée}</xsl:text>
</xsl:template>

<xsl:template match="FormePhonétique" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\formephonétique{</xsl:text>
        <xsl:call-template name="forme_phonétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Région" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\région{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Variantes" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{variantes}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Variante">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{variantes}</xsl:text>
</xsl:template>

<xsl:template match="Variante" mode="thématique">
    <xsl:text>\variante{</xsl:text>
    <xsl:apply-templates select="Forme" mode="thématique"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="FormePhonétique" mode="thématique"/>
    <xsl:apply-templates select="Région" mode="thématique"/>
</xsl:template>

<xsl:template match="PartieDuDiscours" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\partiedudiscours{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="ListeDeSens" mode="thématique">
    <xsl:param name="domainesémantique"/>
    <xsl:apply-templates select="Sens[$domainesémantique = '' or .//DomaineSémantique=$domainesémantique]" mode="thématique"/>
</xsl:template>

<xsl:template match="Sens" mode="thématique">
    <xsl:apply-templates select="NuméroDeSens" mode="thématique"/>
    <xsl:apply-templates select="PartieDuDiscours" mode="thématique"/>
    <xsl:apply-templates select="Gloses" mode="thématique"/>
    <xsl:apply-templates select="Sous-entréesLexicales" mode="thématique"/>
</xsl:template>

<xsl:template match="NuméroDeSens" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\numérodesens{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="DomainesSémantiques" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\domainesémantique{</xsl:text>
    <xsl:for-each select="DomaineSémantique">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:if test="position() != last()">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="DomaineSémantique" mode="thématique">
    <xsl:variable name="domainesémantique" select="."/>
    <xsl:value-of select="/RessourceLexicale/InformationsLexicographiques/OrdreThématique/Hiérarchie//Élément[Identifiant=$domainesémantique]/Valeur"/>
</xsl:template>

<xsl:template match="Définitions" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{définitions}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Définition[@langue = 'fra']">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{définitions}</xsl:text>
</xsl:template>

<xsl:template match="Définition" mode="thématique">
    <xsl:text>\définition{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Gloses" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{gloses}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Glose[@langue = 'fra']">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{gloses}</xsl:text>
</xsl:template>

<xsl:template match="Glose" mode="thématique">
    <xsl:text>\glose{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomsScientifiques" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{nomsscientifiques}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="NomScientifique">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:if test="position() != last()">
            <xsl:text> ; </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{nomsscientifiques}</xsl:text>
</xsl:template>

<xsl:template match="NomScientifique" mode="thématique">
    <xsl:text>\nomscientifique{</xsl:text>
    <xsl:value-of select="Famille"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Binôme"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="NomsVernaculaires" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{nomsvernaculaires}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="NomVernaculaire">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:if test="position() != last()">
            <xsl:text> ; </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{nomsvernaculaires}</xsl:text>
</xsl:template>

<xsl:template match="NomVernaculaire" mode="thématique">
    <xsl:text>\nomvernaculaire{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Exemples" mode="thématique">
    <xsl:apply-templates select="Exemple" mode="thématique"/>
</xsl:template>

<xsl:template match="Exemple" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exemple}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="Original" mode="thématique"/>
    <xsl:for-each select="Traduction">
        <xsl:apply-templates select="." mode="thématique"/>
        <xsl:if test="not(position() = last())">
            <xsl:text>&#10;</xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exemple}</xsl:text>
</xsl:template>

<xsl:template match="Original" mode="thématique">
    <xsl:text>\original{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="../Région" mode="thématique"/>
</xsl:template>

<xsl:template match="Traduction" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\traduction{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="RelationsSémantiques" mode="thématique">
    <xsl:apply-templates select="RelationSémantique" mode="thématique"/>
</xsl:template>

<xsl:template match="Notes" mode="thématique">
    <xsl:apply-templates select="Note" mode="thématique"/>
</xsl:template>

<xsl:template match="Note[not(Glose) and Type!='phonétique']" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\note{</xsl:text>
    <xsl:apply-templates select="Texte" mode="thématique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Note[Type='phonétique']" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\notephonétique{</xsl:text>
    <xsl:apply-templates select="Texte" mode="thématique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Note[Glose]" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\noteglose{</xsl:text>
    <xsl:apply-templates select="Texte" mode="thématique"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="Glose"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Forme" mode="thématique">
    <xsl:value-of select="replace(., '_', '')"/>
</xsl:template>

<xsl:template match="Texte" mode="thématique">
    <xsl:call-template name="adapter_langue"/>
</xsl:template>

<xsl:template match="lien" mode="thématique">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\lien{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(., '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style" mode="thématique">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style[@type='ph']" mode="thématique">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
        <xsl:call-template name="forme_phonétique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="text()" mode="thématique">
    <xsl:value-of select="replace(., '&quot;(.+?)&quot;', '« $1 »')"/>
</xsl:template>

<xsl:template name="adapter_langue">
    <xsl:text>\p</xsl:text>
    <xsl:value-of select="@langue"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates mode="thématique"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="forme_phonétique">
    <xsl:value-of select="replace(., ':', 'ː')"/>
</xsl:template>

<xsl:template name="lettrine_variante">
    <xsl:param name="lettrine"/>
    <xsl:if test="$lettrine = 'dr' or $lettrine = 'tr' or $lettrine = 'thr'">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>(variante de GOs)</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="section">
    <xsl:param name="profondeur"/>
    <xsl:text>\</xsl:text>
    <xsl:value-of select="string-join(for $compteur in 1 to ($profondeur - 1) return 'sub')"/>
    <xsl:text>section</xsl:text>
</xsl:template>

<xsl:template name="traduire">
    <xsl:param name="expression"/>
    <xsl:choose>
        <xsl:when test="$expression='renvoi'">
            <xsl:text>Cf.</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='synonyme'">
            <xsl:text>Syn.</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='antonyme'">
            <xsl:text>Ant.</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$expression"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
