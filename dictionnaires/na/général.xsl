<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:template match="RessourceLexicale">
% !TEX program = lualatex

\documentclass[type=book]{dictionnaire lexica}
\RequireVersions{
    *{application}{luaTeX} {0000/00/00 v1.18.0}
    *{package}{ctex} {2022/07/14 v2.5.10}
}
\ConfigurerLangue[main]{fra}{}
\ConfigurerLangue{eng}{}
\ConfigurerLangue{}{}[nru]
\ConfigurerLangue{api}{}
<!-- \ConfigurerLangue{cmncn}{} -->
<!-- \ConfigurerLangue{cmncn}{polices={rm={nom={EBGaramond}}}} -->
\ConfigurerLangue{cmncn}{polices={rm={options={Script=CJK, Language=Chinese Simplified, CJKShape=Simplified, AutoFakeBold=1.5, ItalicFont=Noto Serif CJK SC}}}}
\ConfigurerLangue{pinyin}{}
\ConfigurerLangue{bod}{}
\ConfigurerLangue{lat}{}
\RequirePackage{listings}
\RequirePackage{changepage}
\babelpatterns[nru]{}
\addbibresource{bibliographie.bib}
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage{tocloft}
\RenewDocumentCommand \cftsecfont {} {\selectlanguage{nru}}
\RequirePackage[fit]{truncate}
\setmainfont{EBGaramond}
\setfontfamily{\policetitrecmn}{京華老宋体v2.002.ttf}[AutoFakeBold=0.5]
\NewDocumentCommand \pdéf { m } {\textesymbolique{\textcolor{<xsl:value-of select="$couleurnru"/>}{#1}}}
\NewDocumentCommand \pfradéf { m } {\textefra{#1}}
\NewDocumentCommand \pnru { m } {\textenru{\textcolor{<xsl:value-of select="$couleurnru"/>}{#1}}}
\NewDocumentCommand \papi { m } {\texteapi{#1}}
\NewDocumentCommand \pfra { m } {\textefra{\textcolor{<xsl:value-of select="$couleurfra"/>}{#1}}}
\NewDocumentCommand \peng { m } {\texteeng{\textcolor{<xsl:value-of select="$couleureng"/>}{#1}}}
\NewDocumentCommand \pcmn { m } {\textecmncn{\textcolor{<xsl:value-of select="$couleurcmn"/>}{#1}}}
\NewDocumentCommand \pcmnbis { m } {\scriptcjc\textefra #1}
\NewDocumentCommand \pbod { m } {\textebod{\textcolor{<xsl:value-of select="$couleuremprunt"/>}{#1}}}
\NewDocumentCommand \plat { m } {\textelat{\emph{\textcolor{<xsl:value-of select="$couleuremprunt"/>}{#1}}}}
\NewDocumentCommand \pnxq { m } {\textenru{\textcolor{<xsl:value-of select="$couleuremprunt"/>}{#1}}}
\NewDocumentCommand \pxina { m } {\textecmncn{\textcolor{<xsl:value-of select="$couleuremprunt"/>}{#1}}}
\RenewDocumentCommand \vedetteentête { m } {\pnru{#1}}
\RenewDocumentCommand \vedetteentêtecourte { m } {\truncate{5cm}{\pnru{#1}}}
\RenewDocumentCommand \vedette { m } {\pnru{\textbf{#1}}}
\RenewDocumentEnvironment {définitions} { } {\unskip\enskip\pdéf{►}}{\ignorespacesafterend}
\NewDocumentEnvironment {références} {} {\pfradéf{(}\ignorespaces} {\unskip\pfradéf{)}}
\NewDocumentCommand \formedesurface { m } {\unskip\enskip\pfradéf{/}\pnru{#1}\pfradéf{/}}
\NewDocumentCommand \orthographe { m } {\unskip\enskip\papi{\emph{#1}}}
\NewDocumentCommand \orthographeexemple { m } {\papi{\emph{#1}}}
\NewDocumentCommand \locuteur { m } {\pfradéf{\textcolor{<xsl:value-of select="$couleurlocuteur"/>}{#1}}}
\NewDocumentCommand \référence { m } {\locuteur{#1}}
\NewDocumentCommand \doi { m } {\href{https://doi.org/#1}{\pfradéf{\textcolor{<xsl:value-of select="$couleurdoi"/>}{#1}}}}
\NewDocumentCommand \référencetexte { m } {\pfradéf{\textcolor{<xsl:value-of select="$couleurréférencetexte"/>}{#1}}}
\NewDocumentCommand \morphème { m m } {\ciblerelationsémantique{\pnru{#1}}{#2}}
\RenewDocumentCommand \formeclassificateur { m m } {\ciblerelationsémantique{\pnru{#1}}{#2}}

\ExplSyntaxOn
\RenewDocumentCommand \note { m m m m }{
    \bool_if:nF { % Ce cas arrive pour la morphologie
        \tl_if_blank_p:n {#2} &amp;&amp; \tl_if_blank_p:n {#3}
    } {
        \pdéf{◈}
    }
    \tl_if_eq:nnTF {#1} {cmn} {} {~}
    \use:c {p#1} {
        \bool_if:nF {
            \tl_if_blank_p:n {#2} &amp;&amp; \tl_if_blank_p:n {#3}
        } {
            \tl_if_eq:nnTF {#1} {cmn} {（} {(}
        }
        \tl_if_blank:nTF{#2} {} {\typenote{#2}}
        \tl_if_blank:nTF{#3} {} {
            \tl_if_blank:nTF{#2} {} {
                \tl_if_eq:nnTF {#1} {cmn} {——} {~–~}
            }
            \typenote{#3}
        }
        \bool_if:nF {
            \tl_if_blank_p:n {#2} &amp;&amp; \tl_if_blank_p:n {#3}
        } {
            \tl_if_eq:nnTF {#1} {cmn} {）} {)~}
        }
        #4
    }
}
\ExplSyntaxOff
\NewDocumentCommand \typenote { m } {\textbf{#1}}
\RenewDocumentCommand \lien { m m } {\hyperlink{#2}{\pnru{#1}}}
\RenewDocumentCommand \lienbrisé { m } {\pnru{\textcolor{Red}{#1}}}
\RenewDocumentCommand \stylefv { m } {\pnru{#1}}
\RenewDocumentCommand \stylefn { m } {\pcmn{#1}}
\RenewDocumentCommand \stylefi { m } {\plat{#1}}
\NewDocumentCommand \styletib { m } {\pbod{#1}}
\NewDocumentCommand \stylefg { m } {\textsc{\textcolor{<xsl:value-of select="$couleurpartiedudiscours"/>}{#1}}}
\NewDocumentCommand \stylefo { m } {\orthographeexemple{#1}}
<!-- \setlength{\marginparsep}{1.5em} -->
\NewDocumentCommand \phonologie { m } {\textcolor{<xsl:value-of select="$couleurphonologie"/>}{\papi{/#1/}}}
\NewDocumentCommand \phonologiebis { m } {\textcolor{<xsl:value-of select="$couleurphonologie"/>}{\papi{#1}}}
\NewDocumentCommand \phonétique { m } {\textcolor{<xsl:value-of select="$couleurphonétique"/>}{\papi{[#1]}}}
\NewDocumentCommand \phonétiquebis { m } {\textcolor{<xsl:value-of select="$couleurphonétique"/>}{\papi{#1}}}

\NewDocumentCommand \ensemblevide {} {\pfradéf{∅}}

\NewDocumentCommand \bibzh { m } {\textecmncn{#1}}
\NewDocumentCommand \transl { m } {\peng{#1}}

\RenewDocumentCommand {\mkbibnamefamily} { m } {\textsc{\textnohyphenation{#1}}}

\DeclareSourcemap{
   \maps[datatype=bibtex]{
       \map{
            \step[fieldsource=langid, match={chinese}, replace={english}]
        }
    }
}

\colorlet{couleurcoordinateurscitation}{OliveGreen!50!white}

\ExplSyntaxOn
\NewDocumentCommand \coord { m } {
    \regex_match:nnTF { \A [、，] \Z } { #1 } {
        \emph{#1}
    } {
        \textcolor{couleurcoordinateurscitation}{\small{#1}}
    }
}
\ExplSyntaxOff

<!-- \DeclareLanguageMapping{chinese}{cmncn} -->

\DefineBibliographyStrings{french}{%
    andothers = {\emph{et al\adddot}\addspace},
    and = {\emph{et}\addspace}
}

\DefineBibliographyStrings{english}{%
    andothers = {\emph{et al\adddot}\addspace},
    and = {\emph{and}\addspace}
}

<!-- \DefineBibliographyStrings{chinese}{%
    andothers = {\emph{等}},
    and = {\emph{和}}
} -->

\RenewDocumentCommand \finalnamedelim {} {\addspace\textsc{\&amp;}\addspace}
\RenewDocumentCommand \multinamedelim {} {\addspace\textsc{\&amp;}\addspace}
\RenewDocumentCommand \finallistdelim {} {\addspace\textsc{\&amp;}\addspace}
\RenewDocumentCommand \multicitedelim {} {\addcomma\space}

<!-- \DefineBibliographyStrings{chinese}{%
    andothers = {\textcolor{couleurcoordinateurscitation}{等}},
    and = {\textcolor{couleurcoordinateurscitation}{与}}
} -->

<!-- \DeclareDatamodelFields[type=list, datatype=name]{authorzh}
\DeclareDatamodelEntryfields{authorzh} -->

<!-- \step[fieldset=shortauthor, fieldsource=author] -->

<!-- \step[fieldset=shortauthor, fieldsource=authorzh] -->



<!-- \DeclareSourcemap{
    \maps[datatype=bibtex]{
        \map[overwrite]{
            \step[fieldsource=author, match={\regexp{\\bibzh\{(.+?)\}}}, replace={$1}]
            \step[fieldsource=author, match={ and }, replace={ 〇〇〇 }]
            \step[fieldsource=author, match={\regexp{[^\p{Han}]}}, replace={}]
            \step[fieldsource=author, match={ 〇〇〇 }, replace={\\textcolor{couleurcoordinateurscitation}{\\emph{与}}}]
            \step[fieldset=shortauthor, fieldsource=author]
        }
    }
 } -->

\directlua{
    luaotfload.add_fallback(
        "policessecours",
        {
            "LinuxBiolinum:mode=harf;", "LinuxLibertine:mode=harf;"
        }
    )
}

\babelprovide{code}
\babelfont[code]{rm}[Scale=1, RawFeature={fallback=policessecours}]{Gentium Plus}
\babelfont[code]{sf}[Scale=1, RawFeature={fallback=policessecours}]{Gentium Plus}
\babelfont[code]{tt}[Scale=1, RawFeature={fallback=policessecours}]{Gentium Plus}
\ExplSyntaxOn
\tl_put_right:cn {extrascode} {
    \RenewDocumentCommand \CJKttdefault {} {cmncnrm}
}
\ExplSyntaxOff

\NewDocumentCommand \languecode { } {\selectlanguage{code}\scriptlatin}
\NewDocumentCommand \textecode { m } {\foreignlanguage{code}{\scriptlatin#1}}

\lstset{
    backgroundcolor=\color{gray!20!white},
    basicstyle=\footnotesize\linespread{1.05}\ttfamily\scriptlatin\selectlanguage{code}, % \linespread nécessaire sinon le chinois provoque une ligne de fond blanc (https://tex.stackexchange.com/questions/129377/unwanted-white-lines-in-listings-environment).
    columns=fullflexible,
    showstringspaces=false,
    commentstyle=\color{gray}\upshape
}

\lstdefinelanguage{XML}{
    morestring=[s]{"}{"},
    morecomment=[s]{?}{?},
    morecomment=[s]{!--}{--},
    commentstyle=\color{Periwinkle},
    moredelim=[s][\color{black}]{&gt;}{&lt;},
    moredelim=[s][\color{PineGreen}]{\ }{=},
    stringstyle=\color{Orange},
    identifierstyle=\color{Fuchsia}
}

\ExplSyntaxOn
\seq_set_split:Nnn \g_listecaractèrespageimpaire_seq {,} {b, ɕ, d, dz, dʑ, ɖ, ɖʐ, f, g, ɣ, h, j, ʝ, k, kʰ, l, ɭ, ɬ, m, n, ɳ, ɲ, ŋ, p, pʰ, q, qʰ, ɻ, ʁ, s, ʂ, t, tʰ, ts, tsʰ, tɕ, tɕʰ, ʈ, ʈʰ, ʈʂ, ʈʂʰ, w, w̃, z, ʑ, ʐ}
\seq_set_split:Nnn \g_listecaractèrespagepaire_seq {,} {ɑ, *ɑ̃, æ, æ̃, *e, *ɤ, ə, *i, ĩ, o, õ, ɻ̩, ɻ̩̃, u, *ɯ, v̩, *ṽ̩}

\fancypagestyle{dictionnaireaveccaractères}[dictionnaire]{
    \fancyfoot[OC]{\normalsize\pnru{\afficherlettrines{\g_listecaractèrespagepaire_seq}}}
    \fancyfoot[EC]{\normalsize\pnru{\afficherlettrines{\g_listecaractèrespageimpaire_seq}}}
}

\RenewDocumentEnvironment {dictionnaire*} { O{\textefra{Dictionnaire}} } {
    \RenewDocumentCommand \chaptermark { m } {}
    \RenewDocumentCommand \sectionmark { m } {}
    \fancypagestyle{plain}[base]{
        \fancyfoot[OC]{\normalsize\pnru{\afficherlettrines{\g_listecaractèrespagepaire_seq}}}
        \fancyfoot[EC]{\normalsize\pnru{\afficherlettrines{\g_listecaractèrespageimpaire_seq}}}
    }
    \markboth{}{}
    \tl_set:Nn \l_nomdictionnaire_tl {#1}
    \pagestyle{dictionnaireaveccaractères}
    \chapter*{#1}
} {
    \fancypagestyle{plain}[base]{}
    \newpage
}
\ExplSyntaxOff

<!-- \newlength{\espacementverticaltableau}
\setlength{\espacementverticaltableau}{\bigskipamount}
\SetTblrOuter[longtblr]{presep=\espacementverticaltableau, postsep=\espacementverticaltableau}
\SetTblrOuter[tblr]{presep=\espacementverticaltableau, postsep=\espacementverticaltableau} -->

\ExplSyntaxOn
\prop_new:N \compteurs
\NewDocumentCommand \ajoutercompteur { O{\compteurs} m m }
{
  \prop_put:Nnn #1 {#2}{#3}
}
\NewDocumentCommand \obtenircompteur { O{\compteurs} m }
{
  \prop_item:Nn #1 {#2}
}
\ExplSyntaxOff

\ajoutercompteur{total}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale)"/>}
\ajoutercompteur{adj}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^adj$')])"/>}
\ajoutercompteur{adv}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^adv$')])"/>}
\ajoutercompteur{clf}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^clf$')])"/>}
\ajoutercompteur{clitic}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^clitic$')])"/>}
\ajoutercompteur{cnj}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^cnj$')])"/>}
\ajoutercompteur{disc.ptcl}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^disc\.ptcl$')])"/>}
\ajoutercompteur{ideophone}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^ideophone$')])"/>}
\ajoutercompteur{intj}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^intj$')])"/>}
\ajoutercompteur{n}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^n$')])"/>}
\ajoutercompteur{num}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^num$')])"/>}
\ajoutercompteur{postp}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^postp$')])"/>}
\ajoutercompteur{pref}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^pref$')])"/>}
\ajoutercompteur{prep}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^prep$')])"/>}
\ajoutercompteur{pro}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^pro$')])"/>}
\ajoutercompteur{suff}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^suff$')])"/>}
\ajoutercompteur{v}{<xsl:value-of select="count(//EntréesLexicales/EntréeLexicale[matches(PartieDuDiscours, '^v$')])"/>}

<xsl:call-template name="ajouter_préambule_complémentaire"/>

\begin{document}

<xsl:call-template name="ajouter_première_de_couverture"/>

\pagefantôme

\pagenumbering{roman}

\pagestyle{empty}

<xsl:call-template name="ajouter_premières_pages"/>

<xsl:call-template name="ajouter_épigraphe"/>

<xsl:call-template name="ajouter_préface"/>

\begin{introduction}
\input{<xsl:value-of select="$fichierintroduction"/>}
\end{introduction}

\RenewDocumentCommand \mkbibparens { m } {(#1)}
\RenewDocumentCommand \bibopenparen { m } {(}
\RenewDocumentCommand \bibcloseparen { m } {)}

\bibliographie[\nombibliographie]

\mainmatter

\begin{dictionnaire*}[\nomdictionnaire]
<xsl:apply-templates select="InformationsLexicographiques"/>
\end{dictionnaire*}

\backmatter

<xsl:call-template name="ajouter_postface"/>

\tabledesmatières[\nomtabledesmatières]

\cleardoublepage
\pagefantôme

<xsl:call-template name="ajouter_quatrième_de_couverture"/>

\end{document}
</xsl:template>

<xsl:template match="InformationsLexicographiques">
    <xsl:apply-templates select="OrdreLexicographique"/>
</xsl:template>

<xsl:template match="OrdreLexicographique">
    <xsl:apply-templates select="Élément"/>
</xsl:template>

<xsl:template match="Élément">
    <xsl:variable name="lettrine" select="if (Élément) then string-join(Élément, ' – ') else ."/>
    <xsl:variable name="expression_rationnelle_graphèmes">
        <xsl:call-template name="créer_expression_rationnelle_graphèmes">
            <xsl:with-param name="graphèmes" select="."/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bloc_entrées">
        <xsl:apply-templates select="/RessourceLexicale/Dictionnaire/EntréesLexicales">
            <xsl:with-param name="expression_rationnelle_graphèmes" select="$expression_rationnelle_graphèmes"/>
        </xsl:apply-templates>
    </xsl:variable>
    <xsl:if test="$bloc_entrées != ''">
        <xsl:if test="not(position() = 1)">
            <xsl:text>&#10;&#10;</xsl:text>
            <xsl:text>\cleardoublepage</xsl:text>
            <xsl:text>&#10;&#10;</xsl:text>
        </xsl:if>
        <xsl:text>\begin{bloclettrine}{</xsl:text>
        <xsl:value-of select="$lettrine"/>
        <xsl:text>}</xsl:text>
        <xsl:text>&#10;&#10;</xsl:text>
        <xsl:value-of select="$bloc_entrées"/>
        <xsl:text>\end{bloclettrine}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="*[@print='n']">
</xsl:template>

<xsl:template match="EntréesLexicales">
    <xsl:param name="expression_rationnelle_graphèmes"/>
    <xsl:apply-templates select="EntréeLexicale[matches(replace(Lemme/Forme, '[_†=-]', ''), $expression_rationnelle_graphèmes, 'i;j')]"/>
</xsl:template>

<xsl:template match="EntréeLexicale">
    <xsl:text>\begin{entrée}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="Lemme/Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="NuméroDHomonyme"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Lemme/FormeDeSurface"/>
    <xsl:apply-templates select="Lemme/Orthographe"/>
    <xsl:apply-templates select="Lemme/Variantes"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\newline</xsl:text>
    <xsl:apply-templates select="PartieDuDiscours"/>
    <xsl:apply-templates select="Lemme/Ton"/>
    <xsl:apply-templates select="Notes"/>
    <xsl:apply-templates select="ListeDeSens"/>
    <xsl:apply-templates select="Morphologie"/>
    <xsl:apply-templates select="Étymologie"/>
    <xsl:apply-templates select="Comparatisme"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{entrée}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Variantes">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{variantes}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each-group select="Variante" group-by="Forme">
        <xsl:sort select="if (current-grouping-key() = 'ID.') then '0' else '1'"/>
        <xsl:text>\variante{</xsl:text>
        <xsl:choose>
            <xsl:when test="current-grouping-key() = '*'">
                <xsl:text>\ensemblevide</xsl:text>
            </xsl:when>
            <xsl:when test="current-grouping-key() = 'ID.'">
                <xsl:call-template name="traduire">
                    <xsl:with-param name="expression" select="current-grouping-key()"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="convertir_caractères_spéciaux">
                    <xsl:with-param name="expression" select="current-grouping-key()"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>}{</xsl:text>
        <xsl:for-each select="current-group()">
            <xsl:apply-templates select="Locuteur"/>
            <xsl:if test="not(position() = last())">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
        <xsl:apply-templates select="Notes"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:text>\unskip , </xsl:text>
        </xsl:if>
    </xsl:for-each-group>
    <xsl:text>\end{variantes}</xsl:text>
</xsl:template>

<xsl:template match="Locuteur">
    <xsl:text>\locuteur{</xsl:text>
    <xsl:call-template name="remplacer_locuteur">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Ton">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\ton{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="FormeDeSurface">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\formedesurface{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Lemme/Orthographe">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\orthographe{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Exemple/Orthographe">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\orthographeexemple{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Morphologie">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{morphologie}</xsl:text>
    <xsl:if test="Morphème">
        <xsl:text>&#10;</xsl:text>
        <xsl:for-each select="Morphème">
            <xsl:apply-templates select="."/>
            <xsl:if test="not(position() = last())">
                <xsl:text>\séparateurénumération</xsl:text>
            </xsl:if>
        </xsl:for-each>
    </xsl:if>
    <xsl:apply-templates select="Notes"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{morphologie}</xsl:text>
</xsl:template>

<xsl:template match="Morphème">
    <xsl:text>\morphème{</xsl:text>
    <xsl:variable name="expression">
        <xsl:apply-templates select="text()"/>
    </xsl:variable>
    <xsl:value-of select="replace($expression, '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(@identifiant, '#', '\\detokenize{#}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="PartieDuDiscours">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\partiedudiscours{</xsl:text>
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="."/>
        <xsl:with-param name="ajouter_langue" select="false()"/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Notes">
    <xsl:apply-templates select="Note"/>
</xsl:template>

<xsl:template match="Note">
    <xsl:if test="Texte[not(@print = 'n')] and (Texte/@langue = $ordrelanguesnotes or not(Texte/@langue))">
        <xsl:variable name="langue" select="(Texte/@langue, $languedocument)[1]"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\note{</xsl:text>
        <xsl:value-of select="$langue"/>
        <xsl:text>}{</xsl:text>
        <xsl:if test="Type">
            <xsl:call-template name="traduire">
                <xsl:with-param name="expression" select="Type"/>
                <xsl:with-param name="langue" select="$langue"/>
                <xsl:with-param name="ajouter_langue" select="false()"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:text>}{</xsl:text>
        <xsl:if test="Domaine">
            <xsl:call-template name="traduire">
                <xsl:with-param name="expression" select="Domaine"/>
                <xsl:with-param name="langue" select="$langue"/>
                <xsl:with-param name="ajouter_langue" select="false()"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:text>}{</xsl:text>
        <xsl:apply-templates select="Texte"/>
        <xsl:text>}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="Note[Texte = 'PHONO' or Texte = 'PROVERBE']">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\notegénérale{</xsl:text>
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="Texte"/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Classificateurs">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{classificateurs}[</xsl:text>
    <xsl:value-of select="count(Classificateur)"/>
    <xsl:text>]</xsl:text>
    <xsl:for-each select="Classificateur">
        <xsl:apply-templates select="."/>
        <xsl:if test="not(position() = last())">
            <xsl:text>\séparateurénumération</xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{classificateurs}</xsl:text>
</xsl:template>

<xsl:template match="Classificateur">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\classificateur{</xsl:text>
    <xsl:apply-templates select="Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="ChampDApplication[@langue = $languedocument]"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="ChampDApplication">
    <xsl:text>\champclassificateur{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="ListeDeSens">
    <xsl:apply-templates select="Sens"/>
</xsl:template>

<xsl:template match="Sens">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{sens}</xsl:text>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="NuméroDeSens"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="Définitions"/>
    <!-- <xsl:apply-templates select="Gloses"/> -->
    <xsl:apply-templates select="Notes"/>
    <xsl:apply-templates select="Usages"/>
    <xsl:apply-templates select="Exemples"/>
    <xsl:apply-templates select="RelationsSémantiques"/>
    <xsl:apply-templates select="Classificateurs"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{sens}</xsl:text>
</xsl:template>

<xsl:template match="NuméroDeSens">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\numérodesens{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Définitions">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{définitions}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Définition[@langue = $ordrelangues]">
        <xsl:sort select="index-of($ordrelangues, @langue)"/>
        <xsl:apply-templates select="."/>
        <xsl:if test="./@langue = 'cmn'">
            <xsl:apply-templates select="ancestor::Sens/Gloses"/>
        </xsl:if>
        <xsl:if test="not(position() = last())">
            <xsl:text>&#10;</xsl:text>
            <xsl:text>\pdéf{•}</xsl:text>
        </xsl:if>
        <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{définitions}</xsl:text>
</xsl:template>

<xsl:template match="Définition">
    <xsl:text>\définition{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Gloses">
    <xsl:if test="Glose/@langue = 'xina'">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\begin{gloses}</xsl:text>
        <xsl:text>&#10;</xsl:text>
        <xsl:for-each select="Glose[@langue = 'xina']">
            <xsl:apply-templates select="."/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>\end{gloses}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="Glose">
    <xsl:text>\glose{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Exemples">
    <xsl:for-each select="Exemple">
        <xsl:apply-templates select="."/>
        <!-- <xsl:if test="not(position() = last())">
            <xsl:text>&#10;</xsl:text>
            <xsl:text>\unskip{}\,\ignorespaces</xsl:text>
        </xsl:if> -->
    </xsl:for-each>
</xsl:template>

<xsl:template match="Exemple">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exemple}</xsl:text>
    <xsl:if test="Notes/Note[(Texte = 'PHONO' or Texte = 'PROVERBE') and $languedocument = 'cmn']">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\unkern\kern-0.2em</xsl:text>
    </xsl:if>
    <xsl:apply-templates select="Notes/Note[Texte = 'PHONO' or Texte = 'PROVERBE']"/>
    <xsl:apply-templates select="Références"/>
    <xsl:apply-templates select="Original"/>
    <xsl:apply-templates select="Orthographe"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\pdéf{•}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Traduction[@langue = $ordrelangues]">
        <xsl:sort select="index-of($ordrelangues, @langue)"/>
        <xsl:variable name="langue" select="@langue"/>
        <xsl:apply-templates select="."/>
        <xsl:apply-templates select="../Notes/Note[Texte != 'PHONO' and Texte != 'PROVERBE' and Texte/@langue = $langue]"/>
        <xsl:if test="not(position() = last())">
            <xsl:text>&#10;</xsl:text>
            <xsl:text>\pdéf{•}</xsl:text>
            <xsl:text>&#10;</xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:apply-templates select="Notes/Note[Texte != 'PHONO' and Texte != 'PROVERBE' and not(Texte/@langue)]"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exemple}</xsl:text>
</xsl:template>

<xsl:template match="Exemple[not(Original)]">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exemple}</xsl:text>
    <xsl:apply-templates select="Références"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exemple}</xsl:text>
</xsl:template>

<xsl:template match="Références">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{références}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="Référence">
        <xsl:apply-templates select="."/>
        <xsl:if test="not(position() = last())">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{références}</xsl:text>
</xsl:template>

<xsl:template match="Référence[@type='locuteur']">
 <xsl:text>\référence{</xsl:text>
    <xsl:call-template name="remplacer_locuteur">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Référence[@type='doi']">
 <xsl:text>\doi{</xsl:text>
    <xsl:value-of select="replace(., '#', '\\#')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Référence[@type='texte']">
 <xsl:text>\référencetexte{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>


<xsl:template match="Référence">
 <xsl:text>\référence{</xsl:text>
    <xsl:call-template name="remplacer_locuteur">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Usages">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{usages}</xsl:text>
    <xsl:apply-templates select="Usage"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{usages}</xsl:text>
</xsl:template>

<xsl:template match="Usage">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\usage{</xsl:text>
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Original">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\original{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Traduction">
    <xsl:text>\traduction{</xsl:text>
    <xsl:call-template name="adapter_langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="RelationsSémantiques">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{relationssémantiques}</xsl:text>
    <xsl:for-each-group select="RelationSémantique" group-by="Type">
        <xsl:sort select="index-of(('renvoi', 'synonyme', 'antonyme'), current-grouping-key())"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\relationsémantique{</xsl:text>
        <xsl:text>\typerelationsémantique{</xsl:text>
        <xsl:call-template name="traduire">
            <xsl:with-param name="expression" select="current-grouping-key()"/>
        </xsl:call-template>
        <xsl:text>}}{</xsl:text>
        <xsl:for-each select="current-group()/Cible">
            <xsl:text>\ciblerelationsémantique{</xsl:text>
            <xsl:variable name="expression">
                <xsl:apply-templates/>
            </xsl:variable>
            <xsl:value-of select="replace($expression, '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="replace(./@identifiant, '(#|~)', '\\detokenize{$1}')"/>
            <xsl:text>}</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>\séparateurénumération</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:for-each-group>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{relationssémantiques}</xsl:text>
</xsl:template>

<xsl:template match="Étymologie">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{étymologie}</xsl:text>
    <!-- <xsl:apply-templates select="Étymons"/> -->
    <xsl:apply-templates select="Emprunt"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{étymologie}</xsl:text>
</xsl:template>

<xsl:template match="Comparatisme">
    <xsl:apply-templates select="Cognats"/>
    <xsl:apply-templates select="Comparanda"/>
</xsl:template>

<xsl:template match="Emprunt">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\emprunt{</xsl:text>
    <xsl:apply-templates select="Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="Langue"/>
    </xsl:call-template>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Cognats">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{cognats}</xsl:text>
    <xsl:for-each select="Cognat">
        <xsl:apply-templates select="."/>
            <xsl:if test="not(position() = last())">
            <xsl:text>\séparateurénumération</xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{cognats}</xsl:text>
</xsl:template>

<xsl:template match="Cognat">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\cognat{</xsl:text>
    <xsl:apply-templates select="Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="Langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Comparanda">
    <xsl:if test="Comparandum[Forme[not(@print = 'n')]]">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\begin{comparanda}</xsl:text>
        <xsl:for-each select="Comparandum">
            <xsl:apply-templates select="."/>
                <xsl:if test="not(position() = last())">
                <xsl:text>\séparateurénumération</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\end{comparanda}</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template match="Comparandum">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\comparandum{</xsl:text>
    <xsl:apply-templates select="Forme"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="Langue"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Forme">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="Classificateur/Forme">
    <xsl:variable name="expression">
        <xsl:apply-templates select="text()"/>
    </xsl:variable>
    <xsl:text>\formeclassificateur{</xsl:text>
    <xsl:value-of select="replace($expression, '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="replace(@identifiant, '#', '\\detokenize{#}')"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Emprunt/Forme">
    <xsl:call-template name="adapter_langue"/>
</xsl:template>

<xsl:template match="Texte">
    <xsl:call-template name="adapter_langue"/>
</xsl:template>

<xsl:template match="(Cognat|Comparandum)/Langue">
    <xsl:call-template name="traduire">
        <xsl:with-param name="expression" select="."/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="lien">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\lien{</xsl:text>
    <xsl:value-of select="@identifiant"/>
    <xsl:text>}{</xsl:text>
    <xsl:call-template name="convertir_liens"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="style">
    <xsl:text>\style</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="convertir_liens">
    <xsl:param name="expression" select="."/>
    <xsl:value-of select="replace($expression, '\s*(\d+)', ' \\numérodhomonyme{$1}')"/>
</xsl:template>

<xsl:template name="convertir_catégories_tonales">
    <xsl:param name="expression" select="."/>
    <xsl:value-of select="replace(replace(replace($expression, 'α', '\\textsubscript{a}'), 'β', '\\textsubscript{b}'), 'γ', '\\textsubscript{c}')"/>
</xsl:template>

<xsl:template name="convertir_exposants">
    <xsl:param name="expression" select="."/>
    <xsl:value-of select="replace($expression, '(\d)(ers|res|er|re|es|st|nd|rd|th|e)', '$1\\up{$2}')"/>
</xsl:template>

<xsl:template match="text()">
    <xsl:variable name="expression">
        <xsl:call-template name="convertir_caractères_spéciaux">
            <xsl:with-param name="expression" select="."/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="expression">
        <xsl:call-template name="convertir_catégories_tonales">
            <xsl:with-param name="expression" select="$expression"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="expression">
        <xsl:choose>
            <xsl:when test="not(ancestor::Glose)">
                <xsl:call-template name="convertir_exposants">
                    <xsl:with-param name="expression" select="$expression"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$expression"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="$expression"/>
</xsl:template>

<xsl:template name="convertir_caractères_spéciaux">
    <xsl:param name="expression" select="."/>
    <xsl:value-of select="replace(replace(replace($expression, '[#$_&amp;]', '\\$0'), '~', '\\textasciitilde{}'), '[〜∼]', '\\sim{}')"/>
</xsl:template>

<xsl:template name="adapter_langue">
    <xsl:param name="expression" select="."/>
    <xsl:choose>
        <xsl:when test="$expression/@langue">
            <xsl:text>\p</xsl:text>
            <xsl:value-of select="replace($expression/@langue, '\d+', '')"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>\pfradéf</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>{</xsl:text>
        <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="remplacer_locuteur">
    <xsl:param name="expression"/>
    <xsl:choose>
        <xsl:when test="$expression='F4'">
            <xsl:text>La</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='F5'">
            <xsl:text>Gi</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='M18'">
            <xsl:text>Da</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='M21'">
            <xsl:text>Jj</xsl:text>
        </xsl:when>
        <xsl:when test="$expression='M23'">
            <xsl:text>Dd</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$expression"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="traduire">
    <xsl:param name="expression"/>
    <xsl:param name="langue" select="$languedocument"/>
    <xsl:param name="ajouter_langue" select="true()"/>
    <xsl:variable name="traduction" select="$traductions/termes/terme[@expression=$expression]/traduction[@langue=$langue]"/>
    <xsl:variable name="résultat">
        <xsl:choose>
            <xsl:when test="string-length($traduction) > 0">
                <xsl:value-of select="$traduction"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$expression"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:choose>
        <xsl:when test="$ajouter_langue and $expression != $résultat">
            <xsl:text>\p</xsl:text>
            <xsl:value-of select="$langue"/>
            <xsl:text>{</xsl:text>
            <xsl:value-of select="$résultat"/>
            <xsl:text>}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$résultat"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:variable name="traductions">
    <termes>
        <terme expression="adj">
            <traduction langue="cmn">形容词</traduction>
            <traduction langue="fra">adjectif</traduction>
            <traduction langue="eng">adjective</traduction>
        </terme>
        <terme expression="adv">
            <traduction langue="cmn">助词</traduction>
            <traduction langue="fra">adverbe</traduction>
            <traduction langue="eng">adverb</traduction>
        </terme>
        <terme expression="clf">
            <traduction langue="cmn">量词</traduction>
            <traduction langue="fra">classificateur</traduction>
            <traduction langue="eng">classifier</traduction>
        </terme>
        <terme expression="clitic">
            <traduction langue="cmn">附着词</traduction>
            <traduction langue="fra">clitique</traduction>
            <traduction langue="eng">clitic</traduction>
        </terme>
        <terme expression="cnj">
            <traduction langue="cmn">连接词</traduction>
            <traduction langue="fra">conjonction</traduction>
            <traduction langue="eng">conjunction</traduction>
        </terme>
        <terme expression="disc.PTCL">
            <traduction langue="cmn">语气助词</traduction>
            <traduction langue="fra">particule discursive</traduction>
            <traduction langue="eng">discourse particle</traduction>
        </terme>
        <terme expression="ideophone">
            <traduction langue="cmn">状貌词</traduction>
            <traduction langue="fra">idéophone</traduction>
            <traduction langue="eng">ideophone</traduction>
        </terme>
        <terme expression="intj">
            <traduction langue="cmn">感叹词</traduction>
            <traduction langue="fra">interjection</traduction>
            <traduction langue="eng">interjection</traduction>
        </terme>
        <terme expression="lnk">
            <traduction langue="cmn">连词</traduction>
            <traduction langue="fra">lien</traduction>
            <traduction langue="eng">link</traduction>
        </terme>
        <terme expression="n">
            <traduction langue="cmn">名词</traduction>
            <traduction langue="fra">nom</traduction>
            <traduction langue="eng">noun</traduction>
        </terme>
        <terme expression="neg">
            <traduction langue="cmn">否定词</traduction>
            <traduction langue="fra">négation</traduction>
            <traduction langue="eng">negation</traduction>
        </terme>
        <terme expression="num">
            <traduction langue="cmn">数词</traduction>
            <traduction langue="fra">numéral</traduction>
            <traduction langue="eng">numeral</traduction>
        </terme>
        <terme expression="postp">
            <traduction langue="cmn">后置词</traduction>
            <traduction langue="fra">postposition</traduction>
            <traduction langue="eng">postposition</traduction>
        </terme>
        <terme expression="pref">
            <traduction langue="cmn">前缀</traduction>
            <traduction langue="fra">préfixe</traduction>
            <traduction langue="eng">prefix</traduction>
        </terme>
        <terme expression="prep">
            <traduction langue="cmn">介词</traduction>
            <traduction langue="fra">préposition</traduction>
            <traduction langue="eng">preposition</traduction>
        </terme>
        <terme expression="pro">
            <traduction langue="cmn">代词</traduction>
            <traduction langue="fra">pronom</traduction>
            <traduction langue="eng">pronoun</traduction>
        </terme>
        <terme expression="suff">
            <traduction langue="cmn">后缀</traduction>
            <traduction langue="fra">suffixe</traduction>
            <traduction langue="eng">suffix</traduction>
        </terme>
        <terme expression="v">
            <traduction langue="cmn">动词</traduction>
            <traduction langue="fra">verbe</traduction>
            <traduction langue="eng">verb</traduction>
        </terme>
        <terme expression="classifier">
            <traduction langue="cmn">量词</traduction>
            <traduction langue="fra">classificateur</traduction>
            <traduction langue="eng">classifier</traduction>
        </terme>
        <terme expression="PHONO">
            <traduction langue="cmn">音系资料</traduction>
            <traduction langue="fra">élicitation phonologique</traduction>
            <traduction langue="eng">phonological elicitation</traduction>
        </terme>
        <terme expression="PROVERBE">
            <traduction langue="cmn">谚语</traduction>
            <traduction langue="fra">proverbe</traduction>
            <traduction langue="eng">proverb</traduction>
        </terme>
        <terme expression="archaic">
            <traduction langue="cmn">古词</traduction>
            <traduction langue="fra">archaïque</traduction>
            <traduction langue="eng">archaic</traduction>
        </terme>
        <terme expression="renvoi">
            <traduction langue="cmn">参考</traduction>
            <traduction langue="fra">Voir aussi</traduction>
            <traduction langue="eng">See also</traduction>
        </terme>
        <terme expression="synonyme">
            <traduction langue="cmn">同义词</traduction>
            <traduction langue="fra">Synonyme</traduction>
            <traduction langue="eng">Synonym</traduction>
        </terme>
        <terme expression="antonyme">
            <traduction langue="cmn">反义词</traduction>
            <traduction langue="fra">Antonyme</traduction>
            <traduction langue="eng">Antonym</traduction>
        </terme>
        <terme expression="ID.">
            <traduction langue="cmn">同</traduction>
            <traduction langue="fra">idem</traduction>
            <traduction langue="eng">same</traduction>
        </terme>
        <terme expression="sem">
            <traduction langue="cmn">语义</traduction>
            <traduction langue="fra">sémantique</traduction>
            <traduction langue="eng">semantics</traduction>
        </terme>
        <terme expression="synt">
            <traduction langue="cmn">句法</traduction>
            <traduction langue="fra">syntaxe</traduction>
            <traduction langue="eng">syntax</traduction>
        </terme>
        <terme expression="morpho">
            <traduction langue="cmn">组成语素</traduction>
            <traduction langue="fra">composition morphémique</traduction>
            <traduction langue="eng">morphemic make-up</traduction>
        </terme>
        <terme expression="phono">
            <traduction langue="cmn">音系学</traduction>
            <traduction langue="fra">phonologie</traduction>
            <traduction langue="eng">phonology</traduction>
        </terme>
        <terme expression="ton">
            <traduction langue="cmn">声调</traduction>
            <traduction langue="fra">tonologie</traduction>
            <traduction langue="eng">tonology</traduction>
        </terme>
        <terme expression="ety">
            <traduction langue="cmn">词源</traduction>
            <traduction langue="fra">étymologie</traduction>
            <traduction langue="eng">etymology</traduction>
        </terme>
        <terme expression="dialect">
            <traduction langue="cmn">方言学</traduction>
            <traduction langue="fra">dialectologie</traduction>
            <traduction langue="eng">dialectology</traduction>
        </terme>
        <terme expression="usage">
            <traduction langue="cmn">语用</traduction>
            <traduction langue="fra">usage</traduction>
            <traduction langue="eng">usage</traduction>
        </terme>
        <terme expression="hist">
            <traduction langue="cmn">编辑过程历史</traduction>
            <traduction langue="fra">historique</traduction>
            <traduction langue="eng">history</traduction>
        </terme>
        <terme expression="nxq">
            <traduction langue="cmn">纳西语</traduction>
            <traduction langue="fra">naxi</traduction>
            <traduction langue="eng">Naxi</traduction>
        </terme>
        <terme expression="lata1234">
            <traduction langue="cmn">拉塔地摩梭语</traduction>
            <traduction langue="fra">na de Lataddi</traduction>
            <traduction langue="eng">Lataddi Narua</traduction>
        </terme>
        <terme expression="xina1239">
            <traduction langue="cmn">西南官话</traduction>
            <traduction langue="fra">mandarin du Sud-Ouest</traduction>
            <traduction langue="eng">Southwestern Mandarin</traduction>
        </terme>
        <terme expression="cmn">
            <traduction langue="cmn">汉语</traduction>
            <traduction langue="fra">chinois</traduction>
            <traduction langue="eng">Chinese</traduction>
        </terme>
        <terme expression="bod">
            <traduction langue="cmn">藏语</traduction>
            <traduction langue="fra">tibétain</traduction>
            <traduction langue="eng">Tibetan</traduction>
        </terme>
        <terme expression="pmi">
            <traduction langue="cmn">普米语</traduction>
            <traduction langue="fra">pumi</traduction>
            <traduction langue="eng">Pumi</traduction>
        </terme>
        <terme expression="jya">
            <traduction langue="cmn">茶堡话</traduction>
            <traduction langue="fra">japhug</traduction>
            <traduction langue="eng">Japhug</traduction>
        </terme>
    </termes>
</xsl:variable>

<xsl:template name="créer_expression_rationnelle_graphèmes">  <!-- forme : ^(x|y|z) -->
    <xsl:param name="graphèmes"/>
    <xsl:text>^</xsl:text>
    <xsl:call-template name="exclure_graphèmes_chevauchants">
        <xsl:with-param name="graphèmes" select="$graphèmes"/>
    </xsl:call-template>
    <xsl:text>(</xsl:text>
    <xsl:choose>
        <xsl:when test="Élément">
            <xsl:for-each select="$graphèmes/Élément">
                <xsl:value-of select="."/>
                <xsl:if test="not(position() = last())">
                    <xsl:text>|</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$graphèmes"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template name="exclure_graphèmes_chevauchants">  <!-- forme : ^(?!xz|yz)(x|y) (attention : nécessite le drapeau « ;j » dans la fonction « matches » pour que « ?! » soit accepté par Saxon) -->
    <xsl:param name="graphèmes"/>
    <xsl:variable name="graphèmes_chevauchants">
        <xsl:call-template name="trouver_graphèmes_chevauchants">
            <xsl:with-param name="graphèmes" select="$graphèmes"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$graphèmes_chevauchants != ''">
        <xsl:text>(?!</xsl:text>
        <xsl:for-each select="$graphèmes_chevauchants//Graphème">
            <xsl:value-of select="."/>
            <xsl:if test="not(position() = last())">
                <xsl:text>|</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="trouver_graphèmes_chevauchants">  <!-- exemple : x et xz -->
    <xsl:param name="graphèmes"/>
    <xsl:element name="Graphèmes">
        <xsl:for-each select="//OrdreLexicographique//Élément[not(Élément)]">  <!-- chaque graphème utilisé dans l'ordre lexicographique -->
            <xsl:variable name="graphème_comparé" select="."/>
            <xsl:for-each select="$graphèmes/Élément|$graphèmes[not(Élément)]">  <!-- chaque graphème formant la lettrine (formant un bloc d'entrées) -->
                <xsl:if test="starts-with($graphème_comparé, .) and $graphème_comparé != .">
                    <Graphème>
                        <xsl:value-of select="$graphème_comparé"/>
                    </Graphème>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
