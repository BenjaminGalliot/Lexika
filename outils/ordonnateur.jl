using FilePaths

# Exemple d’utilisation
# chemin_source::PosixPath = "dictionnaires/na/na.lex"
# chemin_cible::PosixPath = chemin_source
# balise_à_déplacer::String = "rf"
# balises_adelphes_prioritaires::Vector{String} = ["xv", "xe", "xf", "xn", "xc", "xo"]

chemin_source::PosixPath = ""
chemin_cible::PosixPath = ""
balise_à_déplacer::String = ""
balises_adelphes_prioritaires::Vector{String} = ["", ""]

function lire_source(chemin_accès::AbstractPath)::Vector{String}
    données = String[]
    open(chemin_accès) do fichier
        for (index, ligne) ∈ fichier |> eachline |> enumerate
            if !isempty(ligne)
                if startswith(ligne, "\\")
                    push!(données, strip(ligne) * "\n")
                else
                    données[end] = strip(données[end]) * " " * strip(ligne) * "\n"
                    @warn("Ligne tronquée à la position $index, concaténation.")
                end
            else
                push!(données, "\n")
            end
        end
    end
    return données
end

function écrire_résultat(chemin_accès::AbstractPath, données::Vector{<:AbstractString})
    open(chemin_accès, "w") do fichier
        for ligne ∈ données
            write(fichier, "$ligne")
        end
    end
end

function ordonner_lignes(données::Vector{<:AbstractString}, balise_à_déplacer::String, balises_adelphes_prioritaires::Vector{<:AbstractString}; balise_bloc::String = "lx")::Vector{String}
    lignes_à_déplacer::Vector{Tuple{Int, String}} = []
    nouvelles_données::Vector{String} = []
    compteur_déplacements::Int = 0
    for (position, ligne) ∈ données |> enumerate
        if isempty(lignes_à_déplacer)
            if startswith(ligne, "\\$balise_bloc")
                push!(nouvelles_données, ligne)
            elseif startswith(ligne, "\\$balise_à_déplacer")
                push!(lignes_à_déplacer, (position, ligne))
                @info("Ligne à déplacer trouvée et mise de côté (ligne $position) : $lignes_à_déplacer")
            else
                push!(nouvelles_données, ligne)
            end
        else
            if startswith(ligne, "\\$balise_bloc")
                @error("Les lignes à déplacer n’ont pas trouvé leur bloc adelphique avant l’entrée suivante (ligne $position) : « $lignes_à_déplacer ».")
            elseif startswith(ligne, "\\$balise_à_déplacer")
                if lignes_à_déplacer[end][1] == position - 1
                    push!(lignes_à_déplacer, (position, ligne))
                    @info("Ligne à déplacer consécutive trouvée et mise de côté avec la précédente (ligne $position) : $lignes_à_déplacer")
                else
                    nombre_lignes = length(lignes_à_déplacer)
                    append!(nouvelles_données, [données[2] for données ∈ lignes_à_déplacer])
                    empty!(lignes_à_déplacer)
                    push!(lignes_à_déplacer, (position, ligne))
                    compteur_déplacements += 1
                    @info("Lignes à déplacer ($nombre_lignes) replacées avant la ligne suivante (ligne $position), qui devient la nouvelle ligne à déplacer mise de côté : $ligne")
                end
            elseif startswith.(ligne, ["\\$balise_adelphe" for balise_adelphe ∈ balises_adelphes_prioritaires]) |> any
                push!(nouvelles_données, ligne)
            else
                nombre_lignes = length(lignes_à_déplacer)
                append!(nouvelles_données, [données[2] for données ∈ lignes_à_déplacer])
                empty!(lignes_à_déplacer)
                compteur_déplacements += 1
                push!(nouvelles_données, ligne)
                @info("Lignes à déplacer replacées ($nombre_lignes) avant la ligne suivante (ligne $position) : $ligne")
            end
        end

    end
    @info("Il y a eu en tout $compteur_déplacements déplacements de lignes à balise $balise_à_déplacer.")
    return nouvelles_données
end

données = lire_source(chemin_source)
nouvelles_données = ordonner_lignes(données, balise_à_déplacer, balises_adelphes_prioritaires)
écrire_résultat(chemin_cible, nouvelles_données)