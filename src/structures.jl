"Module regroupant tous les éléments nécessaires pour manipuler les structures linguistiques de jLexika."
module Structures

using Crayons
using DataStructures
using FilePaths
using JSON
using Parameters
using YAML

export ConfigurationLinguistique, Balise, Abstraction, EntitéAbstraite, Entité, EntitéBrute
export lister_abstractions, lister_entités, déboguer
export dossier_module

global dossier_module = Path(@__FILE__) |> dirname |> parent

# Structures de données linguistiques.

abstract type EntitéAbstraite end

"Structure immuable qui stocke le nom de la balise et les informations utiles aux traitements (comme les abstractions, valeurs et caractéristiques…)."
struct Balise
    nom::String
    informations::Dict
end

function Balise(nom::AbstractString, informations::AbstractDict)::Balise
    if informations isa AbstractDict && haskey(informations, "abstractions") &&
        informations["abstractions"] isa Vector{<:AbstractDict} && all([haskey(abstraction, "nom") for abstraction ∈ informations["abstractions"]]) &&
        all([!haskey(abstraction, "caractéristiques") || abstraction["caractéristiques"] isa Vector{<:AbstractDict} for abstraction ∈ informations["abstractions"]]) &&
        all([haskey(caractéristique, "nom") for abstraction ∈ informations["abstractions"] for caractéristique ∈ get(abstraction, "caractéristiques", [])])
        return Balise(nom, informations |> Dict)
    else
        error("Balise « $nom » mal formée : un champ « abstractions » doit être présent et il doit contenir une liste de blocs contenant chacun au moins un champ « nom », éventuellement un champ « caractéristiques » qui doit contenir une liste de blocs contenant chacun au moins un champ « nom ».")
    end
end

function Balise(nom::AbstractString, ::Nothing)::Nothing
    @warn("La balise « $nom » est configurée pour ne pas être utilisée.")
end

"Structure immuable qui stocke le nom de l’abstraction et les informations utiles aux traitements (comme l’entité liée, les arguments/drapeaux, les ascendants et descendants…)."
struct Abstraction
    nom::String
    informations::Dict
end

function Abstraction(nom::AbstractString, informations::AbstractDict)::Abstraction
    if informations isa AbstractDict && haskey(informations, "entité") &&
        informations["entité"] isa AbstractDict && haskey(informations["entité"], "nom") &&
        !haskey(informations, "descendants") || (informations["descendants"] isa Vector{<:AbstractDict} && all([haskey(entité, "nom") for entité ∈ informations["descendants"]]))
        Abstraction(nom, informations |> Dict)
    else
        error("Abstraction « $nom » mal formée : un champ « entité » doit être présent et il doit contenir au moins un champ « nom », éventuellement un champ « descendants » qui doit contenir une liste de blocs contenant chacun au moins un champ « nom ».")
    end
end

function Abstraction(nom::AbstractString, ::Nothing)::Nothing
    @warn("L’abstraction « $nom » est configurée pour ne pas être utilisée.")
end

"Structure muable qui stocke le nom de l’entité, l’abstraction liée, sa valeur et ses caractéristiques, son ascendant et ses descendants, les cas échéant. Hormis le nom et l’abstraction, tous les champs sont facultatifs (une entité intermédiaire peut aisément n’avoir aucune valeur ni caractéristique, tandis que la racine et les feuilles n’ont respectivement aucun ascendant et descendant). Pour des raisons techniques, cette structure est muable (elle est créée avant de savoir quels sont ses ascendant et descendants)."
mutable struct Entité <: EntitéAbstraite
    nom::String
    abstraction::Abstraction
    valeur::Union{String, Nothing}
    caractéristiques::Union{Dict{String, String}, Nothing}
    origine::Union{String, Nothing}
    ascendant::Union{Entité, Nothing}
    descendants::Union{Vector{EntitéAbstraite}, Nothing}
    perceptible::Bool

    Entité(nom, abstraction, valeur=nothing, caractéristiques=nothing, origine=nothing, ascendant=nothing, descendants=nothing, perceptible=true) = new(nom, abstraction, valeur, caractéristiques, origine, ascendant, descendants,perceptible)
end

@with_kw mutable struct EntitéBrute <: EntitéAbstraite
    nom::String
    valeur::Union{String, Nothing} = nothing
    caractéristiques::Union{Dict{String, String}, Nothing} = nothing
    ascendant::EntitéAbstraite
    descendants::Union{Vector{EntitéBrute}, Nothing} = nothing
end

"Structure regroupant toutes les informations immuables (après la première initialisation) servant aux traitements, notamment la liste de toutes les balises et abstsractions (comme structures, non comme noms), ainsi que des généralités."
struct ConfigurationLinguistique
    généralités::Dict
    balises::Dict{String, Balise}
    abstractions::Dict{String, Abstraction}
end

function ConfigurationLinguistique(chemin_accès_configuration::SystemPath, chemin_accès_informations::SystemPath=joinpath(dossier_module, "configuration/informations linguistiques.yml"))::ConfigurationLinguistique
    configuration = lire_configuration(chemin_accès_configuration)
    informations_linguistiques = lire_configuration(chemin_accès_informations)
    généralités = informations_linguistiques["généralités"]
    généralités["informations"] = configuration
    absolutiser_chemin_accès!(généralités["informations"]["chemins"], chemin_accès_configuration)
    données_balises = informations_linguistiques["balises"]
    données_abstractions = informations_linguistiques["abstractions"]
    if haskey(configuration["chemins"], "surconfiguration")
        surconfiguration_linguistique = lire_configuration(configuration["chemins"]["surconfiguration"])
        généralités = merge(généralités, get(surconfiguration_linguistique, "généralités", Dict()))
        données_balises = merge(données_balises, get(surconfiguration_linguistique, "balises", Dict()))
        données_abstractions = merge(données_abstractions, get(surconfiguration_linguistique, "abstractions", Dict()))
    end
    balises = Dict(balise.nom => balise for balise ∈ Balise.(keys(données_balises), values(données_balises)) if !isnothing(balise))
    abstractions = Dict(abstraction.nom => abstraction for abstraction ∈ Abstraction.(keys(données_abstractions), values(données_abstractions)) if !isnothing(abstraction))
    compiler_expressions_rationnelles(généralités)
    groupes_capture_ligne = trouver_groupes_capture(généralités["modèles"]["ligne"])
    if !all(nom -> nom ∈ groupes_capture_ligne, ["balise", "données"])
        error("Il est nécessaire d’avoir les groupes de capture « balise » et « données » dans l’expression rationnelle de ligne.")
    end
    if "métadonnées" ∈ groupes_capture_ligne && "métadonnées" ∉ keys(généralités["modèles"])
        error("Il est nécessaire d’avoir l’expression rationnelle de métadonnées en présence du groups de capture « métadonnées » dans l’expression rationnelle de ligne.")
    end
    return ConfigurationLinguistique(généralités, balises, abstractions)
end

"Fonction compilant les expressions rationnelles (de recherche et de substitution) de la configuration générale."
function compiler_expressions_rationnelles(généralités::AbstractDict)
    for (nom_modèle, modèle) ∈ généralités["modèles"]
        généralités["modèles"][nom_modèle] = modèle .|> Regex
    end
    for (nom_modèle, modèle) ∈ généralités["substitutions"]
        généralités["substitutions"][nom_modèle] = modèle .|> SubstitutionString
    end
end

"Fonction recherchant les groupes de capture d’une expression rationnelle."
function trouver_groupes_capture(expression::Regex; modèle::Regex=r"\(\?<(?<nom>\w+)>")
    bilan = eachmatch(modèle, expression.pattern)
    groupes_capture = vcat(getproperty.(bilan, :captures)...) |> Array{String}
    return groupes_capture
end

Base.broadcastable(balise::Balise) = Ref(balise)
Base.broadcastable(abstraction::Abstraction) = Ref(abstraction)
Base.broadcastable(entité::EntitéAbstraite) = Ref(entité)

"Fonction lisant la configuration générale linguistique (au format YAML)."
function lire_configuration(chemin_accès::SystemPath)::Dict
    if exists(chemin_accès)
        dictionnaire_configuration = open(chemin_accès) do fichier
            try
                YAML.load(fichier; dicttype=OrderedDict{String, Any})
            catch erreur
                if erreur isa YAML.ParserError
                    @error("Fichier YAML ($chemin_accès) mal formé, veuillez vérifier la syntaxe, notamment les espaces.")
                elseif erreur isa AssertionError
                    @error("Fichier YAML ($chemin_accès) vide.")
                end
                error(erreur)
            end
        end
    else
        @error("Fichier YAML ($chemin_accès) inexistant. Si vous n’avez pas besoin de surconfiguration linguistique, veuillez retirer ce paramètre du fichier de configuration.")
    end
    return dictionnaire_configuration
end

function absolutiser_chemin_accès!(informations::AbstractDict, chemin_accès_informations::SystemPath)::Dict
    for (nom_chemin_accès, chemin_accès) ∈ informations
        informations[nom_chemin_accès] = broadcast(chemin -> !isnothing(chemin) ? joinpath(chemin_accès_informations |> parent, chemin) |> abspath : nothing, chemin_accès)
    end
    return informations
end

"Fonction listant une entité et tous ses descendants."
function lister_entités(entité::Entité, compteur::Int=1)::Int
    @info("$compteur --» $entité")
    if !isnothing(entité.descendants)
        for (index, descendant) ∈ enumerate(entité.descendants)
            lister_entités(descendant, compteur + index)
        end
    end
    return compteur
end

"Fonction listant toutes les balises de la configuration."
function lister_balises(configuration::ConfigurationLinguistique)
    for (index, balise) ∈ enumerate(values(configuration.balises))
        @info("$index --» $balise")
        @info(json(balise.informations, 4))
    end
end

"Fonction listant toutes les abstractions de la configuration."
function lister_abstractions(configuration::ConfigurationLinguistique)
    for (index, abstraction) ∈ enumerate(values(configuration.abstractions))
        @info("$index --» $abstraction")
        @info(json(abstraction.informations, 4))
    end
end

"Fonction affichant une configuration linguistique."
function Base.show(io::IO, configuration::ConfigurationLinguistique)
    lister_balises(configuration)
    lister_abstractions(configuration)
    @info(configuration.généralités)
end

"Fonction affichant une balise."
function Base.show(io::IO, balise::Balise)
    print(io, Crayon(foreground = :light_magenta), "$(balise.nom)", Crayon(foreground = :default))
end

"Fonction affichant une abstraction."
function Base.show(io::IO, abstraction::Abstraction)
    print(io, Crayon(foreground = :green), "$(abstraction.nom)", Crayon(foreground = :default))
end

"Fonction affichant une entité."
function Base.show(io::IO, entité::Entité)
    print(io, Crayon(foreground = :cyan), "$(entité.nom)")
    if !isnothing(entité.origine)
        print(io, Crayon(faint=true), " (L.$(entité.origine))")
    end
    print(io, Crayon(foreground = :default, faint=false))
    # if !isnothing(entité.valeur) || !isnothing(entité.caractéristiques)
    #     print(io, " (")
    #     if !isnothing(entité.valeur)
    #         printstyled(io, "$(entité.valeur)", color=:yellow)
    #     end
    #     if !isnothing(entité.caractéristiques)
    #         printstyled(io, "$(entité.caractéristiques)", color=:yellow)
    #     end
    #     print(io, ")")
    # end
    # print(io, " ∵ ")
    # print(io, entité.abstraction)
    # print(io, " ≗ ")
    # print(io, pointer_from_objref(entité))
    # if !isnothing(entité.ascendant)
    #     print(io, " ≺ ")
    #     print(io, pointer_from_objref(entité.ascendant))
    # end
end

"Fonction affichant des informations de débogage sans le formatage spécial de @debug."
function déboguer(message)
    if get(ENV, "JULIA_DEBUG", "") == "all"
        println(message)
    end
end

end
