module Générateur

using ..Structures
using DataStructures
using EzXML
using FilePaths
using Pipe
using Unicode
using ZipFile

export générer_xml, générer_xsd, générer_latex, générer_pdf, effacer_fichiers_temporaires_latex

Base.broadcastable(nœud::EzXML.Node) = Ref(nœud)

function générer_xml(chemin_xml::SystemPath, racine::Entité, chemin_xsd::Union{SystemPath, AbstractString}, remplacements::Dict{Regex, <:SubstitutionString})
    document = XMLDocument()
    attributs = OrderedDict(
        "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:noNamespaceSchemaLocation" => relpath(chemin_xsd, parent(chemin_xml))
    )
    créer_nœud_xml!(document, racine, attributs)
    tampon = IOBuffer()
    prettyprint(tampon, document)
    @pipe tampon |> take! |> String |> replace(_, remplacements...) |> write(chemin_xml, _)
end

function générer_xsd(chemin::SystemPath, racine::Abstraction, abstractions::AbstractDict)
    abstractions_identifiantes = [abstraction.informations["arguments"]["identifiant"]["abstraction"] for abstraction ∈ values(abstractions) if haskey(abstraction.informations["arguments"], "identifiant")]
    document = XMLDocument()
    entête = ElementNode("xs:schema")
    entête["xmlns:xs"] = "http://www.w3.org/2001/XMLSchema"
    setroot!(document, entête)
    créer_nœud_xsd!(entête, racine, abstractions, abstractions_identifiantes)
    tampon = IOBuffer()
    print(tampon, document)
    @pipe tampon |> take! |> String |> indenter_xsd |> write(chemin, _)
end

function créer_nœud_xml!(ascendant::Union{EzXML.Document, EzXML.Node}, entité::Entité, attributs::Union{Nothing, <:AbstractDict}=nothing)
    nœud = entité.nom |> convertir_nom_nœud |> ElementNode
    if !isnothing(attributs)
        for (attribut, valeur) ∈ attributs
            nœud[attribut] = valeur
        end
    end
    if !isnothing(entité.valeur)
        texte = TextNode(entité.valeur)
        link!(nœud, texte)
    end
    if !isnothing(entité.caractéristiques)
        for (nom, valeur) ∈ entité.caractéristiques
            nœud[convertir_nom_attribut(nom)] = valeur
        end
    end
    if !isnothing(entité.descendants)
        créer_nœud_xml!.(nœud, entité.descendants)
    end
    if ascendant isa EzXML.Document
        setroot!(ascendant, nœud)
    else
        link!(ascendant, nœud)
    end
    return nœud
end

function créer_nœud_xml!(ascendant::Union{EzXML.Document, EzXML.Node}, entité::EntitéBrute)
    nœud = entité.nom |> convertir_nom_nœud |> ElementNode
    if !isnothing(entité.valeur)
        texte = TextNode(entité.valeur)
        link!(nœud, texte)
    end
    if !isnothing(entité.caractéristiques)
        for (nom, valeur) ∈ entité.caractéristiques
            nœud[convertir_nom_attribut(nom)] = valeur
        end
    end
    if !isnothing(entité.descendants)
        créer_nœud_xml!.(nœud, entité.descendants)
    end
    if ascendant isa EzXML.Document
        setroot!(ascendant, nœud)
    else
        link!(ascendant, nœud)
    end
    return nœud
end

function créer_nœud_xsd!(nœud_ascendant::Union{EzXML.Document, EzXML.Node}, abstraction::Abstraction, abstractions::AbstractDict{<:AbstractString, Abstraction}, abstractions_identifiantes::AbstractVector{Abstraction}, abstraction_ascendante::Union{Abstraction, Nothing}=nothing)
    nœud_entité = ElementNode("xs:element")
    nœud_entité["name"] = abstraction.informations["entité"]["nom"] |> convertir_nom_nœud
    if !isnothing(abstraction_ascendante)
        arguments = ([descendant for descendant ∈ abstraction_ascendante.informations["descendants"] if descendant["nom"] == abstraction.nom] |> only)["arguments"]
        nœud_entité["minOccurs"] = arguments["impérieux"] ? 1 : 0
        nœud_entité["maxOccurs"] = get(arguments, "cardinalité", "unbounded")
    end
    link!(nœud_ascendant, nœud_entité)
    descendants = get(abstraction.informations, "descendants", nothing)
    if !isnothing(descendants)
        nœud_type = ElementNode("xs:complexType")
        link!(nœud_entité, nœud_type)
        nœud_séquence = ElementNode("xs:sequence")
        link!(nœud_type, nœud_séquence)
        for descendant ∈ descendants
            abstraction_descendante = get(abstractions, descendant["nom"], missing)
            if !ismissing(abstraction_descendante)
                créer_nœud_xsd!(nœud_séquence, abstraction_descendante, abstractions, abstractions_identifiantes, abstraction)
            else
                @warn("Abstraction $(descendant["nom"]) non configurée dans le fichier XSD !")
            end
        end
        if abstraction ∈ abstractions_identifiantes
            nœud_attribut = ElementNode("xs:attribute")
            nœud_attribut["name"] = "identifiant"
            nœud_attribut["use"] = "optional"  # À améliorer ?
            link!(nœud_type, nœud_attribut)
        end
    end
end

function convertir_nom_nœud(nom::AbstractString)::String
    return [uppercasefirst(mot) for mot ∈ split(nom, r"[\s’]")] |> join
end

function convertir_nom_attribut(nom::AbstractString)::String
    return replace(nom, r"\s" => "_")
end

function générer_latex(chemin_source::SystemPath, chemin_style::SystemPath, chemin_cible::SystemPath, informations::AbstractDict; nettoyer::Bool=false)::Bool
    exécutable = détecter_exécutable_xslt()
    arguments = préparer_arguments(informations)
    tampon = IOBuffer()
    réussite = true
    commande = `$exécutable -s:$chemin_source -xsl:$chemin_style -o:$chemin_cible $arguments`
    @info("Commande XSLT : $((commande |> string |> Unicode.graphemes |> collect)[2:end-1] |> join)")
    try
        @pipe commande |> Cmd(_, dir=parent(chemin_cible) |> string) |> pipeline(_; stdout=tampon, stderr=tampon) |> run
    catch exception
        réussite = false
    end
    message = tampon |> take! |> String
    if réussite
        if !isempty(message)
            chemin_journal = joinpath(parent(chemin_cible), filename(chemin_style) * ".log")
            write(chemin_journal, message)
            @warn("Résultat de commande XSL :\n$message\nVoir journal $chemin_journal !")
        else
            @info("Compilation XSL terminée visiblement sans erreur…")
        end
        if nettoyer
            contenu = open(chemin_cible, "r") do fichier
                fichier |> read |> String
            end
            contenu_nettoyé = contenu |> nettoyer_latex
            write(chemin_cible, contenu_nettoyé)
        end
    else
        @error("Erreur de compilation XSL :\n$message")
    end
    return réussite
end

"Fonction générant le fichier PDF par la commande Latexmk (moteur LuaLaTeX par défaut). Notons que du fait d’incessantes mises à jour du cache des polices par le module luaotfload (notamment du fait d’autres projets LuaLaTeX indépendants) qui considérera que les sources auront changé et forcera Latexmk a tout recompiler, la compilation non forcée (par défaut) ignorera les fichiers de cache Lua (luc)."
function générer_pdf(chemin_source::SystemPath, chemin_cible::SystemPath; méthode::Symbol=:lualatex, forcer::Bool=false, versionner_modules::Bool=true)::Bool
    exécutable = détecter_exécutable_latex(méthode)
    arguments = if forcer
        mettre_à_jour_polices_latex()
        `-gg`
    else
        `-e '$hash_calc_ignore_pattern{"luc"} = "^";'`
    end
    # commande = `$exécutable $arguments -output-directory=$(parent(chemin_cible)) -synctex=1 -interaction=batchmode $chemin_source`
    # Bogue avec Bibtex (https://github.com/plk/biber/issues/474). Correctif temporaire proposé par John Collins (Latexmk) par courriel.
    chemin_fichier_latexmkrc = joinpath("gabarits", ".latexmkrc") |> abspath
    write(chemin_fichier_latexmkrc, "\$biber = 'biber %O --logfile tmp.blg %S; mv tmp.blg %B.blg';\n")
    commande = `$exécutable $arguments -output-directory=$(parent(chemin_cible)) -synctex=1 -interaction=batchmode -r $chemin_fichier_latexmkrc $chemin_source`
    # ––––––––––––––––––––––––––––––
    environnement_local = Dict(
        "TEXINPUTS" => "$(joinpath(dossier_module, "gabarits"))//:",
        "BIBINPUTS" => "$(joinpath(dossier_module, "gabarits"))//:$(chemin_cible |> parent |> parent)"
    )
    @info("Commande LaTeX : $(@pipe ["$clef=\"$valeur\"" for (clef, valeur) ∈ environnement_local] |> join(_, " ")) $((commande |> string |> Unicode.graphemes |> collect)[2:end-1] |> join)")
    tampon = IOBuffer()
    réussite = true
    try
        withenv(environnement_local...) do
            @time @pipe commande |> Cmd(_, dir=parent(chemin_source) |> string) |> pipeline(_; stdout=tampon, stderr=tampon) |> run
        end
    catch exception
        @error("Erreur d’exécution de la commande LaTeX : « $exception ».")
        réussite = false
    end
    message_latexmk = tampon |> take! |> String
    if occursin("Latexmk: Nothing to do for", message_latexmk)
        @info("Fichier « $chemin_cible » déjà à jour, aucune compilation.")
        @debug(message_latexmk)
    else
        chemin_journal = splitext(chemin_cible)[1] * ".log"
        contenu_journal = chemin_journal |> string |> readlines
        erreurs = @pipe contenu_journal |> filter(ligne -> match(r"Error|Missing character|There were undefined references|Please rerun LaTeX", ligne) isa RegexMatch, _)
        if !réussite || !isempty(erreurs)
            compteur = counter(erreurs)
            message_erreurs = @pipe ["« $erreur » ($occurrences fois)" for (erreur, occurrences) ∈ compteur] |> join(_, "\n")
            @error("Erreur de compilation LaTeX, il vaudrait peut-être mieux regarder cela de plus près…\n$(!isempty(message_erreurs) ? "$message_erreurs\n" : "")Voir journal $chemin_journal !")
            @warn(message_latexmk)
        else
            @info("Compilation LaTeX terminée visiblement sans erreur…\nVoir journal $chemin_journal !")
            @debug(message_latexmk)
            if filename(chemin_cible) ≠ filename(chemin_source)
                mv(joinpath(parent(chemin_cible), filename(chemin_source) * ".pdf"), chemin_cible; force=true)
            end
        end
        if versionner_modules
            versionner_modules_latex(chemin_source, chemin_cible)
            vérifier_modules_latex(chemin_journal)
        end
    end
    return réussite
end

"""
Fonction pour indenter puisque les indentations par EzXML et LightXML semblent s’arrêter quand l’élément XML commence vers la position 60…
"""
function indenter_xsd(texte::AbstractString; indentation::Int=2)::String
    tampon = IOBuffer()
    profondeur = 0
    séquence_spéciale_en_cours = false
    séquence_spéciale = ""
    taille_texte = length(texte)
    for (position, caractère) ∈ enumerate(texte)
        if caractère == '<' || position == taille_texte
            if !isempty(séquence_spéciale)
                if séquence_spéciale[2] == '/'
                    profondeur -= 1
                    write(tampon, "\n" * " "^(indentation*profondeur))
                elseif séquence_spéciale[prevind(séquence_spéciale, end)] == '/'
                    write(tampon, "\n" * " "^(indentation*profondeur))
                elseif séquence_spéciale[2] ≠ '?'
                    write(tampon, "\n" * " "^(indentation*profondeur))
                    profondeur += 1
                end
                write(tampon, séquence_spéciale)
                séquence_spéciale = ""
            end
            séquence_spéciale *= caractère
            séquence_spéciale_en_cours = true
        elseif caractère == '>'
            séquence_spéciale *= caractère
            séquence_spéciale_en_cours = false
        elseif séquence_spéciale_en_cours
            séquence_spéciale *= caractère
        end
    end
    texte_indenté = tampon |> take! |> String
    return texte_indenté
end

function nettoyer_latex(document::AbstractString)::String
    modèle = r"(?<caractère>[\$\^_#~&])"
    substitution = s"\\\g<caractère>"
    lignes = String[]
    préambule = true
    for ligne ∈ @pipe document |> strip |> split(_, "\n")
        if !préambule
            ligne_modifiée = replace(ligne, modèle => substitution)
            # if ligne_modifiée ≠ ligne
            #     @info("Cette ligne a des caractères qui ont été protégés : $ligne_modifiée")
            # end
            push!(lignes, ligne_modifiée)
        else
            push!(lignes, ligne)
        end
        if occursin("\\begin{document}", ligne)
            préambule = false
        end
    end
    push!(lignes, "")
    document_nettoyé = join(lignes, "\n")
    return document_nettoyé
end

function détecter_exécutable_xslt(exécutable_par_défaut::AbstractString="saxonb-xslt")::Cmd
    saxon_présent = if Sys.isunix()
        @pipe `whereis $exécutable_par_défaut` |> read(_, String) |> split |> filter(!endswith(":"), _) |> !isempty
    elseif Sys.iswindows()
        try
            @pipe `where /T $exécutable_par_défaut` |> split |> first |> parse(Int, _) |> isa(_, Number)
        catch LoadError
            false
        end
    end
    if saxon_présent
        commande = `$exécutable_par_défaut`
    else
        commande = installer_saxon()
    end
    return commande
end

function détecter_exécutable_latex(méthode::Symbol)::Cmd
    latexmk_présent = if Sys.isunix()
        @pipe `whereis latexmk` |> read(_, String) |> split |> filter(!endswith(":"), _) |> !isempty
    elseif Sys.iswindows()
        try
            @pipe `where /T latexmk` |> split |> first |> parse(Int, _) |> isa(_, Number)
        catch LoadError
            false
        end
    end
    commande = latexmk_présent ? `latexmk -f -$méthode` : `$méthode`
    @info("Version de LaTeX : $(@pipe `lualatex --version` |> readchomp |> split(_, "\n")[1])")
    return commande
end

function installer_saxon(lien::AbstractString="https://github.com/Saxonica/Saxon-HE/raw/main/12/Java/SaxonHE12-4J.zip", dossier_cible::SystemPath=joinpath(dossier_module, "exécutables/saxon"); modèle_exécutable::Regex=r"saxon-he-[\d.]+\.jar", modèle_bibliothèque::Regex=r"lib/.+?\.jar", forcer=false)
    if forcer || !isdir(dossier_cible) || dossier_cible |> readdir |> isempty
        lecteur = @pipe lien |> download |> ZipFile.Reader
        exécutable = @pipe lecteur.files |> filter(fichier -> match(modèle_exécutable, fichier.name) |> !isnothing, _) |> only
        bibliothèques = @pipe lecteur.files |> filter(fichier -> match(modèle_bibliothèque, fichier.name) |> !isnothing, _)
        chemins_intérêt = [exécutable, bibliothèques...]
        for chemin ∈ chemins_intérêt
            chemin_cible = joinpath(dossier_cible, chemin.name)
            chemin_cible |> parent |> mkpath
            write(chemin_cible, read(chemin, String))
            chmod(chemin_cible, 0o777)
        end
    else
        @info("Exécutable Saxon déjà présent.")
    end
    nom_exécutable = @pipe dossier_cible |> readdir |> filter(chemin -> match(modèle_exécutable, chemin) |> !isnothing, _) |> only
    chemin_exécutable = joinpath(dossier_cible, nom_exécutable) |> abspath
    commande = `java -jar $chemin_exécutable`
    return commande
end

function préparer_arguments(informations::AbstractDict)::Vector{String}
    résultat = ["$clef=$valeur" for (clef, valeur) ∈ informations]
    return résultat
end

"Fonction effaçant les fichiers LaTeX temporaires et auxiliaires, ce qui forcera par ailleurs Latexmk à recompiler. Attention, le fichier log doit toujours être gardé puisqu’il est lu après par une autre fonction… L’argument -c/-C de Latexmk effacera systématiquement le fichier log, c’est pourquoi cette fonction a été créée."
function effacer_fichiers_temporaires_latex(chemin_fichier_cible::SystemPath; modèle_extension::Regex=r"\.(?!log$|pdf$).+$")
    chemin_dossier = parent(chemin_fichier_cible)
    for fichier ∈ readdir(chemin_dossier)
        chemin_complet = joinpath(chemin_dossier, fichier)
        nom = splitext(fichier)[1]
        modèle = "$nom$(modèle_extension.pattern)" |> Regex
        if match(modèle, fichier) isa RegexMatch
            rm(chemin_complet; force=true)
            @info("Fichier supprimé : $chemin_complet")
        end
    end
end

"Cette fonction met à jour la base de données des polices pour LuaLaTeX. Attention, elle forcera par défaut Latexmk à recompiler puisqu’il aura détecté des changements…"
function mettre_à_jour_polices_latex()
    commande = `luaotfload-tool -u`
    try
        commande |> run
    catch exception
        @warn("Erreur dans la commande de détection des polices : « $(exception.msg) » !")
    end
end

function versionner_modules_latex(chemin_source::SystemPath, chemin_cible::SystemPath)
    chemin_dépendances = splitext(chemin_cible)[1] * ".dep"
    nouveau_chemin_dépendances = joinpath(parent(chemin_source), basename(chemin_dépendances))
    if exists(chemin_dépendances)
        mv(chemin_dépendances, nouveau_chemin_dépendances; force=true)
    elseif !exists(nouveau_chemin_dépendances)
        @warn("Fichier des dépendances introuvable !")
    end
end

function vérifier_modules_latex(chemin_journal::AbstractPath; modèle::Regex=r"Package snapshot Warning:\s+Required version (?<version_requise>.+) of (?<module>.+) and\s+provided version (?<version_fournie>.+) do not match.")
    journal = open(chemin_journal, "r") do fichier
        @pipe fichier |> read |> String
    end
    for bilan ∈ eachmatch(modèle, journal)
        @warn("Le module LaTeX « $(bilan[:module]) » n’est pas à la bonne version (« $(bilan[:version_fournie]) » au lieu de « $(bilan[:version_requise]) »), veuillez vérifier le rendu !")
    end
end

end
