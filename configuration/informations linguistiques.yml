# Une ligne au format Lex (Toolbox, etc.) est composée d’une balise (elle-même composée d'un marqueur, normalement « \\ », et d'un terme court de quelques lettres) et de données (parfois aussi de métadonnées pour ceux qui font des lignes assez complexes). Une balise créera généralement une entité du dictionnaire final, mais pour des besoins de cohérence hiérarchique, il y aura très souvent plus d’entités que de balises (et données associées, par abus de langage) dans le dictionnaire. Comme certaines entités peuvent être créées par plusieurs balises différentes. Par exemple, si toute glose appartient à un sens, une entrée monosémique n’aura pas besoin de préciser l’entité sens, mais une entrée polysémique le fera sûrement, ainsi la relation entre balise et entité n’est pas bijective, ce qui amène à la notion d’abstraction, élément intermédiaire qui permettra le lien conditionnel entre ces deux premiers éléments. L’abstraction, qui créera une seule et unique entité (qui y est liée), ainsi que la balise, sont deux éléments génériques, contrairement à l’entité, qui est spécifique, à savoir que la balise et l’abstraction ne contiennent aucune information liée au fichier source, elles ne servent qu’à créer des liens hiérarchiques et à garder des arguments utiles à la mise en place du dictionnaire (l’entité/abstraction est-elle une tête de bloc, sert-elle d’identifiant, doit-elle subir un traitement particulier, etc. ?).

généralités:
    marqueur de balise: "\\"
    balises de bloc: [_sh, lx]
    modèles:
        ligne: "^\\\\(?<balise>\\w*)\\s*(?<métadonnées><([\\w\\s]+=[\\w\"\\s]+)+>)?\\s*(?<données>.*)"
        nom d’abstraction: "^(?<nom>[\\w\\s’-]+)(?=$|(\\s(?<arguments>.+?))$)"
        texte enrichi: "(?<ensemble>\\|(?<style>fv){(?<texte>.+?)})"
    substitutions:
        texte enrichi: "<style type=\"\\g<style>\">\\g<texte></style>"
    ordre lexicographique:
        symboles: [-, _]
        chiffres: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

balises:
    # _sh:
    #     abstractions:
    #       - nom: format d’entrée

    # Balises liées aux entrées.
    lx:
        abstractions:
          - nom: vedette

    se:
        abstractions:
          - nom: sous-vedette

    a:
        abstractions:
          - nom: variante

    va:
        abstractions:
          - nom: variante

    ve:
        abstractions:
          - nom: variante
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    vn:
        abstractions:
          - nom: variante
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    vr:
        abstractions:
          - nom: variante
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    hm:
        abstractions:
          - nom: numéro d’homonyme

    la:
        abstractions:
          - nom: forme de citation

    lc:
        abstractions:
          - nom: forme de surface

    ph:
        abstractions:
          - nom: forme phonétique

    mr:
        abstractions:
          - nom: morphologie

    ps:
        abstractions:
          - nom: partie du discours
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    pn:
        abstractions:
          - nom: partie du discours
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    nt:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: général

    np:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: phonologie

    ng:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: grammaire

    nd:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: discours

    na:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: anthropologie

    ns:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: sociolinguistique

    nq:
        abstractions:
          - nom: note
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: type de note
            valeur: question

    sg:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: singulier

    pl:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: pluriel

    1s:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: singulier
          - nom: personne grammaticale
            valeur: première

    2s:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: singulier
          - nom: personne grammaticale
            valeur: deuxième

    3s:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: singulier
          - nom: personne grammaticale
            valeur: troisième

    4s:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: singulier
          - nom: personne grammaticale
            valeur: inanimé

    1d:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: duel
          - nom: personne grammaticale
            valeur: première

    2d:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: duel
          - nom: personne grammaticale
            valeur: deuxième

    3d:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: duel
          - nom: personne grammaticale
            valeur: troisième

    4d:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: duel
          - nom: personne grammaticale
            valeur: inanimé

    1p:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: pluriel
          - nom: personne grammaticale
            valeur: première

    2p:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: pluriel
          - nom: personne grammaticale
            valeur: deuxième

    3p:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: pluriel
          - nom: personne grammaticale
            valeur: troisième

    4p:
        abstractions:
          - nom: forme grammaticale
          - nom: nombre grammatical
            valeur: pluriel
          - nom: personne grammaticale
            valeur: inanimé

    re:
        abstractions:
          - nom: renvoi inverse
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    rn:
        abstractions:
          - nom: renvoi inverse
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    rr:
        abstractions:
          - nom: renvoi inverse
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    rd:
        abstractions:
          - nom: réduplication

    th:
        abstractions:
          - nom: thésaurus

    bb:
        abstractions:
          - nom: référence bibliographique

    # Balises liées aux sens.
    sn:
        abstractions:
          - nom: numéro de sens

    sd:
        abstractions:
          - nom: domaine sémantique

    is:
        abstractions:
          - nom: index de domaine sémantique

    lt:
        abstractions:
          - nom: sens littéral
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    gv:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    ge:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    gn:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    gr:
        abstractions:
          - nom: glose
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    dv:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    de:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    dn:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    dr:
        abstractions:
          - nom: définition
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    we:
        abstractions:
          - nom: glose interlinéaire
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    wn:
        abstractions:
          - nom: glose interlinéaire
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    wr:
        abstractions:
          - nom: glose interlinéaire
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    uv:
        abstractions:
          - nom: usage
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    ue:
        abstractions:
          - nom: traduction d’usage
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    un:
        abstractions:
          - nom: traduction d’usage
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    ur:
        abstractions:
          - nom: traduction d’usage
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    xv:
        abstractions:
          - nom: exemple
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    xe:
        abstractions:
          - nom: traduction d’exemple
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    xn:
        abstractions:
          - nom: traduction d’exemple
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    xr:
        abstractions:
          - nom: traduction d’exemple
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    xg:
        abstractions:
          - nom: traduction d’exemple en glose interlinéaire
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    rf:
        abstractions:
          - nom: référence d’exemple

    ev:
        abstractions:
          - nom: information encyclopédique
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    ee:
        abstractions:
          - nom: traduction d’information encyclopédique
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    en:
        abstractions:
          - nom: traduction d’information encyclopédique
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    er:
        abstractions:
          - nom: traduction d’information encyclopédique
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    ov:
        abstractions:
          - nom: restriction sémantique
            caractéristiques:
              - nom: langue
                valeur: source 1
                traitement: code linguistique

    oe:
        abstractions:
          - nom: traduction de restriction sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    on:
        abstractions:
          - nom: traduction de restriction sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    or:
        abstractions:
          - nom: traduction de restriction sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    sc:
        abstractions:
          - nom: nom scientifique binominal
            caractéristiques:
              - nom: langue
                valeur: lat

    sca:
        abstractions:
          - nom: auteur de nom scientifique
            caractéristiques:
              - nom: langue
                valeur: lat

    scf:
        abstractions:
          - nom: famille cladistique
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    # Abstractions liées aux relations sémantiques.
    lf:
        abstractions:
          - nom: type de relation sémantique

    le:
        abstractions:
          - nom: type de relation sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique

    ln:
        abstractions:
          - nom: type de relation sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique

    lr:
        abstractions:
          - nom: type de relation sémantique
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique

    lv:
        abstractions:
          - nom: cible de relation sémantique

    cf:
        abstractions:
          - nom: type de relation sémantique
            valeur: renvoi
          - nom: cible de relation sémantique

    ce:
        abstractions:
          - nom: type de relation sémantique
            valeur: renvoi
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: cible de relation sémantique

    cn:
        abstractions:
          - nom: type de relation sémantique
            valeur: renvoi
            caractéristiques:
              - nom: langue
                valeur: cible 2
                traitement: code linguistique
          - nom: cible de relation sémantique

    cr:
        abstractions:
          - nom: type de relation sémantique
            valeur: renvoi
            caractéristiques:
              - nom: langue
                valeur: cible 3
                traitement: code linguistique
          - nom: cible de relation sémantique

    sy:
        abstractions:
          - nom: type de relation sémantique
            valeur: synonyme
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: cible de relation sémantique

    an:
        abstractions:
          - nom: type de relation sémantique
            valeur: antonyme
            caractéristiques:
              - nom: langue
                valeur: cible 1
                traitement: code linguistique
          - nom: cible de relation sémantique

    mn:
        abstractions:
          - nom: type de relation sémantique
            valeur: entrée principale
          - nom: cible de relation sémantique

    # Balises liées aux paradigmes.
    pd:
        abstractions:
          - nom: forme paradigmatique

    pdl:
        abstractions:
          - nom: catégorie paradigmatique

    pdv:
        abstractions:
          - nom: forme paradigmatique

    # Balises liées aux étymologies.
    et:
        abstractions:
          - nom: étymon

    eg:
        abstractions:
          - nom: glose d’étymon

    el:
        abstractions:
          - nom: langue d’étymon

    es:
        abstractions:
          - nom: source d’étymon

    ec:
        abstractions:
          - nom: commentaire d’étymon

    bw:
        abstractions:
          - nom: emprunt

    # Balises multimédia.
    tb:
        abstractions:
          - nom: tableau

    pc:
        abstractions:
          - nom: image

    # Balises diverses.
    so:
        abstractions:
          - nom: source

    st:
        abstractions:
          - nom: statut

    dt:
        abstractions:
          - nom: date

# Les noms des descendants sont nécessairement des noms d’abstraction.
# Légende des abstractions descendantes :
# * : créer le descendant automatiquement (enchaînement prospectif, par défaut rétrospectif),
# [x] : x occurrences de l’entité (liée à l’abstraction dont le nom figure avant) au maximum dans la descendance,
# <(x, y:ns) : trier l’adelphie selon l’ordre lexicographique (priorité des critères de tri (x puis y, à la profondeur n, dans le sens s)),
# Légende des abstractions :
# ★ : racine du dictionnaire,
# ¶ : bloc indépendant (généralement l’entrée),
# ^ : texte enrichi (XML, notamment),
# =xy : identifiant envoyé à l’abstraction y avec le symbole x,
# @ : cible de lien (par l’identifiant),
# →x : informations tirées directement du chemin de configuration x,
# ≡x : condition d’égalité sur la caractéristique x,
# ❌x : condition d’interdiction d’un ascendant x (et bannissement de lignée).

abstractions:
    # Abstractions générales.
    ressource lexicale:
        arguments: "★"
        entité:
            nom: ressource lexicale
        descendants:
          - nom: informations générales * [1]
          - nom: informations lexicographiques * [1]
          - nom: dictionnaire * [1]

    informations générales:
        entité:
            nom: informations générales
        descendants:
          - nom: langue * [1]
          - nom: code de langue * [1]
          - nom: titres * [1]
          - nom: auteurs * [1]
          - nom: version * [1]
          - nom: licence * [1]
          - nom: format d’entrée [1]

    langue:
        arguments: "→général/langue"
        entité:
            nom: langue

    code de langue:
        arguments: "→général/code de langue"
        entité:
            nom: code de langue

    titres:
        arguments: "→général/titres"
        entité:
            nom: titres
        descendants:
          - nom: titre

    titre:
        caractéristiques:
          - champ: langue
        entité:
            nom: titre

    auteurs:
        arguments: "→général/auteurs"
        entité:
            nom: auteurs
        descendants:
          - nom: auteur

    auteur:
        entité:
            nom: auteur
        descendants:
          - nom: anthroponyme

    anthroponyme:
        caractéristiques:
          - champ: langue
        entité:
            nom: anthroponyme

    version:
        arguments: "→général/version"
        entité:
            nom: version

    licence:
        arguments: "→général/licence"
        entité:
            nom: licence

    informations lexicographiques:
        entité:
            nom: informations lexicographiques
        descendants:
          - nom: ordre lexicographique * [1]

    ordre lexicographique:
        arguments: "→général/ordre lexicographique/graphèmes"
        entité:
            nom: ordre lexicographique

    ordre lexicographique inverse:
        arguments: "→général/ordre lexicographique inverse/graphèmes"
        entité:
            nom: ordre lexicographique inverse

    ordre thématique:
        arguments: "→général/ordre thématique"
        entité:
            nom: ordre thématique

    dictionnaire:
        entité:
            nom: dictionnaire
        descendants:
          - nom: liste d’entrées lexicales * [1]

    liste d’entrées lexicales:
        entité:
            nom: entrées lexicales
        descendants:
          - nom: entrée lexicale <(graphèmes, chiffres, symboles)

    liste de sous-entrées lexicales:
        entité:
            nom: sous-entrées lexicales
        descendants:
          - nom: sous-entrée lexicale

    liste de groupes:
        entité:
            nom: groupes
        descendants:
          - nom: groupe

    # Abstractions générales diverses.
    format d’entrée:
       entité:
           nom: format d’entrée

    # Abstractions liées aux entrées.
    entrée lexicale:
        arguments: "¶"
        entité:
            nom: entrée lexicale
        descendants:
          - nom: numéro d’homonyme [1]
          - nom: lemme [1]
          - nom: morphologie [1]
          - nom: liste de groupes [1]
          - nom: partie du discours [1]
          - nom: liste de notes [1]
          - nom: liste de formes grammaticales [1]
          - nom: liste de paradigmes [1]
          - nom: liste de renvois inverses [1]
          - nom: liste de sens [1]
          - nom: liste de sous-entrées lexicales [1]
          - nom: étymologie [1]
          - nom: multimédia [1]
          - nom: date [1]

    groupe:
        entité:
            nom: groupe
        descendants:
          - nom: nom de groupe [1]
          - nom: partie du discours [1]
          - nom: liste de notes [1]
          - nom: liste de formes grammaticales [1]
          - nom: liste de paradigmes [1]
          - nom: liste de sens [1]
          - nom: liste de sous-entrées lexicales [1]
          - nom: étymologie [1]
          - nom: multimédia [1]

    sous-entrée lexicale:
        entité:
            nom: sous-entrée lexicale
        descendants:
          - nom: sous-lemme [1]
          - nom: partie du discours [1]
          - nom: liste de notes [1]
          - nom: liste de formes grammaticales [1]
          - nom: liste de paradigmes [1]
          - nom: liste de sens [1]
          - nom: multimédia [1]

    numéro d’homonyme:
        arguments: "=ⓗentrée lexicale"
        entité:
            nom: numéro d’homonyme

    lemme:
        entité:
            nom: lemme
        descendants:
          - nom: vedette [1]
          - nom: liste de variantes [1]
          - nom: forme phonétique [1]
          - nom: forme de citation [1]

    nom de groupe:
        arguments: "=ⓖgroupe"
        entité:
            nom: nom de groupe

    sous-lemme:
        entité:
            nom: lemme
        descendants:
          - nom: sous-vedette [1]
          - nom: liste de variantes [1]
          - nom: forme phonétique [1]
          - nom: forme de citation [1]

    vedette:
        arguments: "=ⓔentrée lexicale"
        entité:
            nom: forme

    sous-vedette:
        arguments: "=ⓝsous-entrée lexicale"
        entité:
            nom: forme

    liste de variantes:
        entité:
            nom: variantes
        descendants:
          - nom: variante

    variante:
        entité:
            nom: forme

    forme phonétique:
        entité:
            nom: forme phonétique

    forme de citation:
        entité:
            nom: forme de citation

    morphologie:
        entité:
            nom: morphologie

    partie du discours:
        entité:
            nom: partie du discours

    liste de notes:
        entité:
            nom: notes
        descendants:
          - nom: bloc de note

    bloc de note:
        entité:
            nom: note
        descendants:
          - nom: note [1]
          - nom: type de note [1]

    note:
        arguments: "^"
        entité:
            nom: texte

    type de note:
        entité:
            nom: type

    liste de formes grammaticales:
        entité:
            nom: formes grammaticales
        descendants:
          - nom: bloc de forme grammaticale

    bloc de forme grammaticale:
        entité:
            nom: forme grammaticale
        descendants:
          - nom: forme grammaticale [1]
          - nom: nombre grammatical [1]
          - nom: personne grammaticale [1]

    forme grammaticale:
        entité:
            nom: forme

    nombre grammatical:
        entité:
            nom: nombre grammatical

    personne grammaticale:
        entité:
            nom: personne grammaticale

    liste de renvois inverses:
        entité:
            nom: renvois inverses
        descendants:
          - nom: renvoi inverse

    renvoi inverse:
        entité:
            nom: renvoi inverse

    # Abstractions liées aux sens.
    liste de sens:
        entité:
            nom: liste de sens
        descendants:
          - nom: sens

    sens:
        entité:
            nom: sens
        descendants:
          - nom: numéro de sens [1]
          - nom: liste de domaines sémantiques [1]
          - nom: liste d’index de domaines sémantiques [1]
          - nom: sens littéral [1]
          - nom: liste de définitions [1]
          - nom: liste de gloses [1]
          - nom: liste de gloses interlinéaires [1]
          - nom: liste d’usages [1]
          - nom: liste d’exemples [1]
          - nom: liste de relations sémantiques [1]
          - nom: liste de restrictions sémantiques [1]
          - nom: liste de paradigmes [1]
          - nom: liste de classificateurs [1]
          - nom: liste d’informations encyclopédiques [1]
          - nom: liste de noms scientifiques [1]
          - nom: multimédia [1]

    numéro de sens:
        # arguments: "=ⓢsens ; ❌sous-entrée lexicale"
        arguments: "=ⓢsens"
        entité:
            nom: numéro de sens

    liste de domaines sémantiques:
        entité:
            nom: domaines sémantiques
        descendants:
          - nom: domaine sémantique

    domaine sémantique:
        entité:
            nom: domaine sémantique

    liste d’index de domaines sémantiques:
        entité:
            nom: index sémantiques
        descendants:
          - nom: index de domaine sémantique

    index de domaine sémantique:
        entité:
            nom: index

    sens littéral:
        entité:
            nom: sens littéral

    liste de définitions:
        entité:
            nom: définitions
        descendants:
          - nom: bloc de définition

    bloc de définition:
        entité:
            nom: définition
        descendants:
          - nom: définition

    définition:
        arguments: "^"
        entité:
            nom: texte

    liste de gloses:
        entité:
            nom: gloses
        descendants:
          - nom: glose

    glose:
        arguments: "^"
        entité:
            nom: glose

    liste de gloses interlinéaires:
        entité:
            nom: gloses interlinéaires
        descendants:
          - nom: glose interlinéaire

    glose interlinéaire:
        arguments: "^"
        entité:
            nom: glose interlinéaire

    liste d’usages:
        entité:
            nom: usages
        descendants:
          - nom: bloc d’usage

    bloc d’usage:
        entité:
            nom: usage
        descendants:
          - nom: usage [1]
          - nom: traduction d’usage

    usage:
        arguments: "^"
        entité:
            nom: original

    traduction d’usage:
        arguments: "^"
        entité:
            nom: traduction

    liste d’exemples:
        entité:
            nom: exemples
        descendants:
          - nom: bloc d’exemple

    bloc d’exemple:
        entité:
            nom: exemple
        descendants:
          - nom: exemple [1]
          - nom: traduction d’exemple
          - nom: traduction interlinéaire d’exemple [1]
          - nom: référence d’exemple [1]

    exemple:
        arguments: "^"
        entité:
            nom: original

    traduction d’exemple:
        arguments: "^"
        entité:
            nom: traduction

    traduction interlinéaire d’exemple:
        arguments: "^"
        entité:
            nom: traduction interlinéaire

    référence d’exemple:
        arguments: "^"
        entité:
            nom: référence

    liste d’informations encyclopédiques:
        entité:
            nom: informations encyclopédiques
        descendants:
          - nom: bloc d’information encyclopédique

    bloc d’information encyclopédique:
        entité:
            nom: information encyclopédique
        descendants:
          - nom: information encyclopédique [1]
          - nom: traduction d’information encyclopédique

    information encyclopédique:
        arguments: "^"
        entité:
            nom: original

    traduction d’information encyclopédique:
        arguments: "^"
        entité:
            nom: traduction

    liste de noms scientifiques:
        entité:
            nom: noms scientifiques
        descendants:
          - nom: bloc de nom scientifique

    bloc de nom scientifique:
        entité:
            nom: nom scientifique
        descendants:
          - nom: famille cladistique [1]
          - nom: nom scientifique binominal [1]
          - nom: auteur de nom scientifique [1]  # Avec date incluse.

    famille cladistique:
        entité:
            nom: famille

    nom scientifique binominal:
        entité:
            nom: binôme

    auteur de nom scientifique:
        entité:
            nom: auteur

    liste de restrictions sémantiques:
        entité:
            nom: restrictions sémantiques
        descendants:
          - nom: bloc de restriction sémantique

    bloc de restriction sémantique:
        entité:
            nom: restriction sémantique
        descendants:
          - nom: restriction sémantique [1]
          - nom: traduction de restriction sémantique

    restriction sémantique:
        arguments: "^"
        entité:
            nom: original

    traduction de restriction sémantique:
        arguments: "^"
        entité:
            nom: traduction

    # Abstractions liées aux relations sémantiques.
    liste de relations sémantiques:
        entité:
            nom: relations sémantiques
        descendants:
          - nom: relation sémantique

    relation sémantique:
        entité:
            nom: relation sémantique
        descendants:
          - nom: cible de relation sémantique [1]
          - nom: type de relation sémantique [1]

    cible de relation sémantique:
        arguments: "@(entrée lexicale, sous-entrée lexicale)"
        entité:
            nom: cible

    type de relation sémantique:
        entité:
            nom: type

    # Abstractions liées aux paradigmes.
    liste de paradigmes:
        entité:
            nom: paradigmes
        descendants:
          - nom: paradigme

    paradigme:
        entité:
            nom: paradigme
        descendants:
          - nom: catégorie paradigmatique [1]
          - nom: forme paradigmatique [1]

    catégorie paradigmatique:
        entité:
            nom: catégorie

    forme paradigmatique:
        entité:
            nom: forme

    # Abstractions liées aux classificateurs.
    liste de classificateurs:
        entité:
            nom: classificateurs
        descendants:
          - nom: classificateur

    classificateur:
        entité:
            nom: classificateur
        descendants:
          - nom: forme de classificateur [1]
          - nom: champ d’application de classificateur

    champ d’application de classificateur:
        entité:
            nom: champ d’application

    forme de classificateur:
        entité:
            nom: forme

    # Abstractions multimédia.
    multimédia:
        entité:
            nom: multimédia
        descendants:
          - nom: liste d’images [1]
          - nom: liste d’audios [1]
          - nom: liste de vidéos [1]

    liste d’images:
        entité:
            nom: images
        descendants:
          - nom: image

    liste d’audios:
        entité:
            nom: audios
        descendants:
          - nom: audio

    liste de vidéos:
        entité:
            nom: vidéos
        descendants:
          - nom: vidéo

    image:
        entité:
            nom: audio
        descendants:
          - nom: chemin d’accès image [1]

    audio:
        entité:
            nom: audio
        descendants:
          - nom: chemin d’accès audio [1]

    vidéo:
        entité:
            nom: audio
        descendants:
          - nom: chemin d’accès vidéo [1]

    chemin d’accès image:
        entité:
            nom: chemin d’accès

    chemin d’accès audio:
        entité:
            nom: chemin d’accès

    chemin d’accès vidéo:
        entité:
            nom: chemin d’accès

    # Abstractions liées aux étymologies.
    étymologie:
        entité:
            nom: étymologie
        descendants:
          - nom: bloc d’étymon

    bloc d’étymon:
        entité:
            nom: étymon
        descendants:
          - nom: étymon [1]
          - nom: glose d’étymon [1]
          - nom: langue d’étymon [1]
          - nom: source d’étymon [1]
          - nom: commentaire d’étymon [1]
          - nom: emprunt [1]

    étymon:
        entité:
            nom: forme

    glose d’étymon:
        entité:
            nom: glose

    langue d’étymon:
        entité:
            nom: langue

    source d’étymon:
        entité:
            nom: source

    commentaire d’étymon:
        entité:
            nom: commentaire

    emprunt:
        entité:
            nom: emprunt

    # Abstractions diverses.
    date:
        entité:
            nom: date
