#!/usr/bin/env -S julia --project --color=yes --startup-file=no

using ArgMacros
using Distributed
using Lexika

# ENV["JULIA_DEBUG"] = "all"
# using Logging
# Logging.disable_logging(Logging.Debug-1)

chemins_configurations_défaut = [
    # "dictionnaires/japhug/configuration.yml",
    "dictionnaires/yuanga/configuration.yml",
    # "dictionnaires/na/configuration.yml",
]

travaux = []

function préparer_exécution(chemin_configuration::Union{AbstractString, Nothing}=nothing; créer_latex::Bool=true, créer_pdf::Bool=true, créer_html::Bool=false, taille_échantillon::Union{Nothing, Int}=nothing, date::AbstractString="", forcer_compilation_latex::Bool=false, nettoyer_latex::Bool=false)
    créer_latex = créer_pdf ? true : créer_latex
    push!(travaux, chemin_configuration => (créer_latex=créer_latex, créer_pdf=créer_pdf, créer_html=créer_html, taille_échantillon=taille_échantillon, date=date, forcer_compilation_latex=forcer_compilation_latex, nettoyer_latex=nettoyer_latex))
end

if !isinteractive()
    @inlinearguments begin
        @helpusage "julia --project exécuter.jl chemin/fichier/configuration.yml [--html] [--latex] [--forcer] [--nettoyer] [--taille_échantillon taille] [--date format] chemin/configuration/1 chemin/configuration/2 …"
        @helpdescription "Lance Lexika afin de créer un dictionnaire."
        @argumentflag créer_html "--html"
        @arghelp "Crée un dictionnaire final au format HTML."
        @argumentflag créer_latex "--latex"
        @arghelp "Crée un dictionnaire intermédiaire au format LaTeX."
        @argumentflag créer_pdf "--pdf"
        @arghelp "Crée un dictionnaire final au format PDF."
        @argumentflag forcer_compilation_latex "--forcer"
        @arghelp "Force la compilation LaTeX."
        @argumentflag nettoyer_latex "--nettoyer"
        @arghelp "Supprime les fichiers temporaires LaTeX."
        @argumentoptional Int taille_échantillon "-t" "--taille"
        @arghelp "Crée un dictionnaire partiel avec les x premières entrées."
        @argumentdefault String "" date "-d" "--date"
        @arghelp "Ajoute une date au format voulu (comme yyyy-mm-dd HH:MM:SS) au nom de fichier résultat."
        @positionalleftover String chemins_configurations
        @arghelp "chemins d’accès du ou des fichiers de configuration YAML."
    end
    filter!(chemin -> !startswith(chemin, "--"), chemins_configurations)
    for chemin_configuration ∈ chemins_configurations
        préparer_exécution(chemin_configuration; créer_latex, créer_pdf, créer_html, taille_échantillon, date, forcer_compilation_latex, nettoyer_latex)
    end
else
    for chemin_configuration ∈ chemins_configurations_défaut
        # préparer_exécution(chemin_configuration, forcer_compilation_latex=false, nettoyer_latex=false)
        préparer_exécution(chemin_configuration, créer_pdf=false, forcer_compilation_latex=false, nettoyer_latex=false)
        # préparer_exécution(chemin_configuration; créer_pdf=true, taille_échantillon=100, forcer_compilation_latex=false, nettoyer_latex=false)
        # préparer_exécution(chemin_configuration; taille_échantillon=100, date="yyyy-mm-dd HH:MM:SS", forcer_compilation_latex=false, nettoyer_latex=true)
        # préparer_exécution(chemin_configuration; taille_échantillon=500, marqueur="test $(`git rev-parse HEAD` |> read |> String |> chomp)", forcer_compilation_latex=false, nettoyer_latex=true)
    end
end

if !isinteractive() || length(travaux) < 2
    for travail ∈ travaux |> collect
        générer_dictionnaire(travail.first; travail.second...)
    end
else
    addprocs(length(travaux); exeflags="--threads=auto --color=yes")
    @info("Nombre de travailleurs : $(nworkers())")
    @everywhere begin
        using Lexika
        @info("Nombre de fils : $(Threads.nthreads())")
    end
    @sync @distributed for travail ∈ travaux |> collect
        générer_dictionnaire(travail.first; travail.second...)
    end
end
@info("Génération terminée.")

